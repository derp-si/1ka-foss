<?php

/*
 *  Modul za pregledovanje in urejanje nastavitev naprednih časov po straneh
 *
 */


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class SurveyAdvancedTimestamps {

	var $anketa;				# id ankete

	
	function __construct($anketa){

		// Ce imamo anketo
		if ((int)$anketa > 0){
			$this->anketa = $anketa;
		}
	}
	
	
	public function displaySettings(){
		global $lang;
               
		
		echo '<fieldset><legend>'.$lang['srv_results'].'</legend>';

		$href_csv = 'izvoz.php?m=advanced_timestamps_xls&anketa='.$this->anketa;
		echo ' <span><a href="'.$href_csv.'">Excel izvoz parapodatkov</a></span>';

		echo '</fieldset>';	
    }
    
	// Izvozimo tabelo s parapodatki v xls
	public function exportTable(){
		global $site_path;
    
        ini_set('memory_limit', '4048M');


		// Pridobimo podatke za vse tabele
		$timestamp_data = $this->getTimestampData();
		$page_data = $this->getPageData();
		$question_data = $this->getQuestionData();
		$variable_data = $this->getVariableData();


		$spreadsheet = new Spreadsheet();
		

		// Zavihek strani
		$sheet = $spreadsheet->getActiveSheet();
	
		$sheet->setTitle("Strani");
		$sheet->fromArray(
			$page_data,  // The data to set
			NULL,        // Array values with this value will not be set
			'A1'         // Top left coordinate of the worksheet range where
		);

		$spreadsheet->createSheet();


		// Zavihek vprasanja
		$sheet = $spreadsheet->getSheet(1);

		$sheet->setTitle("Vprašanja");
		$sheet->fromArray(
			$question_data,  // The data to set
			NULL,        // Array values with this value will not be set
			'A1'         // Top left coordinate of the worksheet range where
		);

		$spreadsheet->createSheet();


		// Zavihek itemi
		$sheet = $spreadsheet->getSheet(2);

		$sheet->setTitle("Itemi");
		$sheet->fromArray(
			$variable_data,  // The data to set
			NULL,        // Array values with this value will not be set
			'A1'         // Top left coordinate of the worksheet range where
		);

		$spreadsheet->createSheet();


		// Zavihek casi po straneh
		$sheet = $spreadsheet->getSheet(3);

		$sheet->setTitle("Časi po straneh");
		$sheet->fromArray(
			$timestamp_data,  // The data to set
			NULL,        // Array values with this value will not be set
			'A1'         // Top left coordinate of the worksheet range where
		);


		$writer = new Xlsx($spreadsheet);

		// ob_clean();
		// ob_start();
		$filename = 'advanced_timestamps_'.$this->anketa.'.xlsx';

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header("Content-Disposition: attachment;filename=".$filename);
		header("Cache-Control: max-age=0");
		header("Last-Modified: ".date("D, d M Y H:i:s"));
		header("Cache-Control: cache, must-revalidate");
		header("Pragma: public");

		$writer->save("php://output");
		// ob_end_flush();

		die();
	}



	// Pridobimo parapodatke casov po straneh
	private function getTimestampData(){

		$data = array(0 => array('Stran ID', 'Respondent ID', 'Čas'));

		// Najprej dobimo case na uvodih (gru_id=0)
		$sql = sisplet_query("SELECT at.gru_id, at.usr_id, at.time_edit 
								FROM srv_advanced_timestamps at, srv_user u
                                WHERE u.ank_id='".$this->anketa."' AND at.usr_id=u.id AND gru_id<1
                                ORDER BY at.usr_id ASC, at.id ASC
							");
		while($row = mysqli_fetch_assoc($sql)){
			$data[] = $row;
		}

		// Dobimo se ostale case
		$sql = sisplet_query("SELECT at.gru_id, at.usr_id, at.time_edit 
								FROM srv_advanced_timestamps at, srv_grupa g
                                WHERE g.ank_id='".$this->anketa."' AND at.gru_id=g.id
                                ORDER BY at.usr_id ASC, at.id ASC
							");
		while($row = mysqli_fetch_assoc($sql)){
			$data[] = $row;
		}
		
		return $data;
	}

	// Pridobimo podatke strani v anketi
	private function getPageData(){

		$data = array(0 => array('Stran ID', 'Naslov', 'Vrstni red'));
        
		$sql = sisplet_query("SELECT id, naslov, vrstni_red 
								FROM srv_grupa 
                                WHERE ank_id='".$this->anketa."' 
                                ORDER BY vrstni_red ASC
							");
		while($row = mysqli_fetch_assoc($sql)){
			$data[] = $row;
		}
		
		return $data;
	}

	// Pridobimo podatke vprasanj v anketi
	private function getQuestionData(){

		$question_types = array(
			'1' => 'radio',
			'2' => 'checkbox',
			'3' => 'select',
			'4' => 'text', 
			'21' => 'besedilo*',
			'5' => 'label',
			'6' => 'multigrid',
			'16' => 'multicheckbox',
			'19' => 'multitext',
			'20' => 'multinumber',
			'7' => 'number',
			'22' => 'compute ',
			'25' => 'quota',
			'8' => 'datum ',
			'17' => 'ranking  ',
			'18' => 'vsota',
			'24' => 'grid - multiple',
			'23' => 'iz knjiznice',
			'9' => 'SN-imena',
			'26' => 'Lokacija',
			'27' => 'HeatMap ', 
		);

		$data = array(0 => array('Vprašanje ID', 'Stran ID', 'Naslov', 'Variabla', 'Tip'));
        
		$sql = sisplet_query("SELECT s.id, s.gru_id, s.naslov, s.variable, s.tip 
								FROM srv_spremenljivka s, srv_grupa g
                                WHERE g.ank_id='".$this->anketa."' AND s.gru_id=g.id
                                ORDER BY g.vrstni_red ASC, s.id ASC
							");
		while($row = mysqli_fetch_assoc($sql)){

			$row['tip'] = $question_types[$row['tip']];
			$row['naslov'] = strip_tags($row['naslov']);

			$data[] = $row;
		}
		
		return $data;
	}

	// Pridobimo podatke posameznih itemov v vprasanjih
	private function getVariableData(){

		$data = array(0 => array('Vrednost ID', 'Vprašanje ID, ', 'Naslov', 'Variabla', 'Vrstni red'));
        
		$sql = sisplet_query("SELECT v.id, v.spr_id, v.naslov, v.variable, v.vrstni_red
								FROM srv_vrednost v, srv_spremenljivka s, srv_grupa g
                                WHERE g.ank_id='".$this->anketa."' AND s.gru_id=g.id AND v.spr_id=s.id
                                ORDER BY g.vrstni_red ASC, s.id ASC, v.id ASC
							");
		while($row = mysqli_fetch_assoc($sql)){

			$row['naslov'] = strip_tags($row['naslov']);

			$data[] = $row;
		}
		
		return $data;
	}

}