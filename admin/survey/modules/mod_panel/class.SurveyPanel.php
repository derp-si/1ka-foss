<?php
/**
 *
 *	Modul za povezovanje panela (npr. Valicon, GFK...) z 1ka anketo
 *
 */

class SurveyPanel{

	var $anketa;				# id ankete
	var $db_table = '';	

	
	function __construct($anketa){
		global $site_url;

		// Ce imamo anketo, smo v status->ul evealvacija
		if ((int)$anketa > 0){
			$this->anketa = $anketa;

			# polovimo vrsto tabel (aktivne / neaktivne)
			SurveyInfo :: getInstance()->SurveyInit($this->anketa);
			$this->db_table = SurveyInfo::getInstance()->getSurveyArchiveDBString();
		}
	}
	
	
	// Prikazemo nastavitve pri vklopu naprednega modula
	public function displaySettings(){
		global $lang;
		global $site_url;
		
		$preklici_url = ltrim(str_replace("&s=1","",$_SERVER['REQUEST_URI']),"/");
		$preklici_url = "'". $site_url . $preklici_url . "'";
		
		echo '<fieldset><legend>'.$lang['settings'].'</legend>';
			
		$rowA = SurveyInfo::getInstance()->getSurveyRow();
		$row = $this->getPanelSettings();

		// Url za preusmeritev
		echo '<div class="setting_holder">';
		echo '<p>'.$lang['srv_panel_url'].':</p>';
		echo '<input type="text" class="large w300" name="url" id value="'.$rowA['url'].'" />';
		echo '</div>'; 
		
		// Ime parametra za id respondenta
		echo '<div class="setting_holder">';
		echo '<p>'.$lang['srv_panel_user_id_name'].':</p>';
		echo '<input type="text" class="large w300" name="user_id_name" id="user_id_name" value="'.$row['user_id_name'].'" />';
		echo '</div>'; 
		
		// Ime parametra za status
		echo '<div class="setting_holder">';
		echo '<p" >'.$lang['srv_panel_status_name'].':</p>';
		echo '<input type="text" class="large w300" name="status_name" id="status_name" value="'.$row['status_name'].'" />';
		echo '</div>'; 
		
		// Privzeta vrednost status parametra
		echo '<div class="setting_holder">';
		echo '<p>'.$lang['srv_panel_status_default'].':</p>';
		echo '<input type="text" class="large w300" name="status_default" id="status_default" value="'.$row['status_default'].'" />';
		echo '</div>'; 

		// Primer zacetnega url-ja
		$link = SurveyInfo::getSurveyLink();
		echo '<div class="setting_holder">';
		echo '<p>'.$lang['srv_panel_url1_example'].':</p>';
		echo '<p class="gray">'.$link.'?'.$row['user_id_name'].'=RESPONDENT_PANEL_ID</p>';
		echo '</div>'; 
		
		// Primer končnega url-ja
		echo '<div class="setting_holder">';
		echo '<p>'.$lang['srv_panel_url2_example'].':</p>';
		// Dodaten pogoj, ce imamo slucajno ? ze v osnovnem delu urlja (da vracamo tudi recimo fiksen parameter)
		if(strpos($rowA['url'], "?") !== false){
			echo '<p class="gray">'.$rowA['url'].'&'.$row['user_id_name'].'=RESPONDENT_PANEL_ID&'.$row['status_name'].'=PANEL_STATUS</p>';
		}
		else{
			echo '<p class="gray">'.$rowA['url'].'?'.$row['user_id_name'].'=RESPONDENT_PANEL_ID&'.$row['status_name'].'=PANEL_STATUS</p>';
		}
		echo '</div>'; 
		

        // Ko se uporabnik vrne (zacne od zacetka/nadaljuje kjer je ostal)
        echo '<div class="setting_holder">';
        echo '  <span class="setting_title">' . $lang['srv_cookie_return'] . Help :: display('srv_cookie_return') . ':</span>';

        echo '  <div class="setting_item">';
        echo '      <input type="radio" name="cookie_return" value="0" id="cookie_return_0"' . ($rowA['cookie_return'] == 0 ? ' checked="checked"' : '') . ' />';
        echo '      <label for="cookie_return_0">' . $lang['srv_cookie_return_start'] . '</label>';
        echo '  </div>';
        echo '  <div class="setting_item no-cookie">';
        echo '      <input type="radio" name="cookie_return" value="1" id="cookie_return_1"' . ($rowA['cookie_return'] == 1 ? ' checked="checked"' : '') . ' />';
        echo '      <label for="cookie_return_1">' . $lang['srv_cookie_return_middle'] . '</label>';
        echo '  </div>';
        echo '</div>';

        // Nadaljevanje kasneje
        echo '<div class="setting_holder">';
        echo '  <input type="checkbox" id="continue_later" name="continue_later" value="1" '.($rowA['continue_later'] == 1 ? ' checked="checked"' : '').'>';
        echo '  <label for="continue_later">'.$lang['srv_show_continue_later'].' '.Help::display('srv_continue_later_setting').'</label> ';
        echo '</div>';

		echo '</fieldset>';
		

		// Gumb shrani
		echo '<div class="button_holder">';
		echo '<button class="medium white-blue" onClick="window.location='.$preklici_url.'; return false;">'.$lang['edit1338'].'</button>';				
		echo '<button class="medium blue" onclick="panel_save_settings(); return false;">'.$lang['edit1337'].'</button>';
		echo '</div>';

		echo '<div id="success_save"></div>';	
	}	
	
	// Izvedemo vse potrebno, ko modul aktiviramo (nastavimo parametre za zakljucek, ustvarimo sistemske spremenljivke...)
	public function activatePanel(){
		global $lang;
		
		// Vstavimo vrstico z nastavitvami
		$sql1 = sisplet_query("INSERT INTO srv_panel_settings (ank_id) VALUES ('".$this->anketa."')");

		// Uredimo nastavitve zakljucka, deaktiviramo piskotke in nastavimo nastavitev za nadaljevanje, da nadaljuje kjer je ostal
		$sql2 = sisplet_query("UPDATE srv_anketa SET concl_link='1', cookie='-1', cookie_return='1' WHERE id='".$this->anketa."'");
		if (!$sql2) echo mysqli_error($GLOBALS['connect_db']);
		
		// Ustvarimo sistemsko skrito vprasanje za panel id respondenta
		SurveyRespondents:: getInstance()->Init($this->anketa);
        SurveyRespondents:: checkSystemVariables($variable=array('SID'), $setUserbase=false);
	}	
	
	
	// Vrnemo nastavitve panela
	public function getPanelSettings($what = ''){
		
		if($what != ''){
			$sql = sisplet_query("SELECT ".$what." FROM srv_panel_settings WHERE ank_id='".$this->anketa."'");
			$row = mysqli_fetch_array($sql);
			
			return $row[$what];
		}
		else{
			$sql = sisplet_query("SELECT * FROM srv_panel_settings WHERE ank_id='".$this->anketa."'");
			$row = mysqli_fetch_array($sql);
			
			return $row;
		}
	}
	
	// Vrnemo nastavitev statusa na if-u
	public function getPanelIf($if_id){
		
		$sql = sisplet_query("SELECT value FROM srv_panel_if WHERE ank_id='".$this->anketa."' AND if_id='".$if_id."'");
		
		if(mysqli_num_rows($sql) > 0){
			$row = mysqli_fetch_array($sql);
			
			return $row['value'];
		}
		else{
			return '';
		}
	}
	
	
	public function ajax() {
		
		if(isset($_GET['a']) && $_GET['a'] == 'save_settings'){
			
			// Dobimo staro ime parametra za user id
			$user_id_name_old = $this->getPanelSettings($what='user_id_name');
			
			$user_id_name = isset($_POST['user_id_name']) ? $_POST['user_id_name'] : 'SID';
			if($user_id_name == '')
				$user_id_name = $user_id_name_old;
			
			$status_name = isset($_POST['status_name']) ? $_POST['status_name'] : 'status';
			$status_default = isset($_POST['status_default']) ? $_POST['status_default'] : '0';
			$url = isset($_POST['url']) ? $_POST['url'] : '';
			
			$sql = sisplet_query("UPDATE srv_panel_settings SET user_id_name='".$user_id_name."', status_name='".$status_name."', status_default='".$status_default."' WHERE ank_id='".$this->anketa."'");
			if (!$sql) echo mysqli_error($GLOBALS['connect_db']);
			
			if($url != ''){
				$sql2 = sisplet_query("UPDATE srv_anketa SET url='".$url."' WHERE id='".$this->anketa."'");
				if (!$sql2) echo mysqli_error($GLOBALS['connect_db']);
			}
			
			// Popravimo ime sistemskega vprasanja
			$sqlS = sisplet_query("UPDATE srv_spremenljivka s, srv_grupa g 
									SET s.variable='".$user_id_name."' 
									WHERE s.variable='".$user_id_name_old."' AND s.gru_id=g.id AND g.ank_id='".$this->anketa."'");

            
            // Shranimo kje nadaljuje in ce ima opcijo za nadaljevanje kasneje
            if (isset($_POST['cookie_return']) && isset($_POST['continue_later']))
                $sql = sisplet_query("UPDATE srv_anketa SET cookie_return='".$_POST['cookie_return']."', continue_later='".$_POST['continue_later']."' WHERE id='".$this->anketa."'");               


            // vsilimo refresh podatkov
			SurveyInfo :: getInstance()->resetSurveyData();
			
			$this->displaySettings();
		}

	}
}