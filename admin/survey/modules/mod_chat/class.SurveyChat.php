<?php

/*
 *  Modul za chat z respondenti
 * 
 *	Zaenkrat se uporablja storitev TAWK
 *
 */


class SurveyChat{

	var $anketa;				# id ankete

	
	function __construct($anketa){
		global $site_url;

		// Ce imamo anketo
		if ((int)$anketa > 0){
			$this->anketa = $anketa;
		}
	}
	
	
	// Nastavitve chat-a (na kateri strani se prikaze...)
	public function displaySettings(){
		global $lang;
		global $site_url;
		
		$preklici_url = ltrim(str_replace("&s=1","",$_SERVER['REQUEST_URI']),"/");
		$preklici_url = "'". $site_url . $preklici_url . "'";
    	
		$row = SurveyInfo::getInstance()->getSurveyRow();
		
		
		echo '<fieldset><legend>'.$lang['settings'].'</legend>';
			
		// Koda za embed tawk chat widgeta
		$code = '';
		$sql = sisplet_query("SELECT * FROM srv_chat_settings WHERE ank_id='".$this->anketa."'");
		if(mysqli_num_rows($sql) > 0){
			
			$row = mysqli_fetch_array($sql);
			$code = $row['code'];
		}

		echo '<p class="bottom16">'.$lang['srv_vrsta_survey_note_14_1a'].':</p>';

		echo '<div class="setting_holder">';
		echo '<p class="bold">'.$lang['srv_chat_code'].':</p>';
		echo '<textarea class="wauto" id="chat_code" name="chat_code">'.$code.'</textarea>';
		echo '</div>'; 
		
		// Prikaz vklopa chata
		echo '<p class="top16 bottom16">'.$lang['srv_vrsta_survey_note_14_1c'].'</p>';

		echo '<div class="setting_holder">';
		echo '<span class="bold setting_title">'.$lang['srv_chat_type'].':</span>';

		echo '<div class="setting_item">';
		echo '<input type="radio" name="chat_type" id="chat_type_0" value="0" '.(($row['chat_type'] == 0) ? ' checked="checked" ' : '').' />';
		echo '<label for="chat_type_0">'.$lang['srv_chat_type_0'].'</label>';
		echo '</div>';
		echo '<div class="setting_item">';
		echo '<input type="radio" name="chat_type" id="chat_type_1" value="1" '.(($row['chat_type'] == 1) ? ' checked="checked" ' : '').' />';
		echo '<label for="chat_type_1">'.$lang['srv_chat_type_1'].'</label>';	
		echo '</div>';
		echo '<div class="setting_item">';
		echo '<input type="radio" name="chat_type" id="chat_type_2" value="2" '.(($row['chat_type'] == 2) ? ' checked="checked" ' : '').' />';
		echo '<label for="chat_type_2">'.$lang['srv_chat_type_2'].'</label>';	
		echo '</div>';
					
		echo '</div>';

		echo '</fieldset>';
		
		
		// Gumb shrani
		echo '<div class="button_holder">';
		echo '<button class="medium white-blue" onClick="window.location='.$preklici_url.'; return false;">'.$lang['edit1338'].'</button>';				
		echo '<button class="medium blue" onclick="chat_save_settings(); return false;">'.$lang['edit1337'].'</button>';
		echo '</div>';

		echo '<div id="success_save"></div>';	
	}
	
	
	public function ajax() {
		
		if(isset($_GET['a']) && $_GET['a'] == 'save_settings'){
			
			$code = isset($_POST['code']) ? $_POST['code'] : '';
			$chat_type = isset($_POST['chat_type']) ? $_POST['chat_type'] : '0';
			
			$sql = sisplet_query("INSERT INTO srv_chat_settings 
									(ank_id, code, chat_type) VALUES ('".$this->anketa."', '".$code."', '".$chat_type."') 
									ON DUPLICATE KEY UPDATE code='".$code."', chat_type='".$chat_type."'");
			if (!$sql) echo mysqli_error($GLOBALS['connect_db']);
		}

	}
}