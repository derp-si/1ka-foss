<?php

/*
 *  Modul za kviz
 * 
 *
 */


class SurveyQuiz{

	var $anketa;				# id ankete

	
	function __construct($anketa){
		global $site_url;

		// Ce imamo anketo
		if ((int)$anketa > 0){
			$this->anketa = $anketa;
		}
	}
	
	
	// Nastavitve kviza (prikaz rezultatov, grafa...)
	public function displaySettings(){
		global $lang;
		global $site_url;
        
        $preklici_url = ltrim(str_replace("&s=1","",$_SERVER['REQUEST_URI']),"/");
        $preklici_url = "'". $site_url . $preklici_url . "'";

    			
		echo '<fieldset><legend>'.$lang['settings'].'</legend>';
		
		// Pridobimo trenutne nastavitve
		$settings = $this->getSettings();

		// Prikaz rezultatov v zakljucku

		echo '<div class="setting_holder">';
		echo '<span class="setting_title">'.$lang['srv_quiz_results'].':</span>';

		echo '<div class="setting_item">';
		echo '<input type="radio" name="quiz_results" id="quiz_results_0" value="0" '.(($settings['results'] == 0) ? ' checked="checked" ' : '').' />';
		echo '<label for="quiz_results_0">'.$lang['no1'].'</label>';
		echo '</div>';
		echo '<div class="setting_item">';
		echo '<input type="radio" name="quiz_results" id="quiz_results_1" value="1" '.(($settings['results'] == 1) ? ' checked="checked" ' : '').' />';
		echo '<label for="quiz_results_1">'.$lang['yes'].'</label>';	
		echo '</div>';
					
		echo '</div>';
		
		// Prikaz grafa rezultatov v zakljucku
		echo '<div class="setting_holder">';
		echo '<span class="setting_title">'.$lang['srv_quiz_results_chart'].':</span>';

		echo '<div class="setting_item">';
		echo '<input type="radio" name="quiz_results_chart" id="quiz_results_chart_0" value="0" '.(($settings['results_chart'] == 0) ? ' checked="checked" ' : '').' />';
		echo '<label for="quiz_results_chart_0">'.$lang['no1'].'</label>';
		echo '</div>';
		echo '<div class="setting_item">';
		echo '<input type="radio" name="quiz_results_chart" id="quiz_results_chart_1" value="1" '.(($settings['results_chart'] == 1) ? ' checked="checked" ' : '').' />';
		echo '<label for="quiz_results_chart_1">'.$lang['yes'].'</label>';	
		echo '</div>';
					
		echo '</div>';

		echo '</fieldset>';
		
		// Gumb shrani
		echo '<div class="button_holder">';
        echo '<button class="medium white-blue" onClick="window.location='.$preklici_url.'; return false;">'.$lang['edit1338'].'</button>';             
        echo '<button class="medium blue" onclick="quiz_save_settings(); return false;">'.$lang['edit1337'].'</button>';
        echo '</div>';

		echo '<div id="success_save"></div>';	
	}
	
	
	// Pridobimo trenutne nastavitve kviza za anketo
	public function getSettings(){
		
		$settings = array();
		
		// Default vrednosti
		$settings['results'] = '1';
		$settings['results_chart'] = '0';
		
		$sql = sisplet_query("SELECT * FROM srv_quiz_settings WHERE ank_id='".$this->anketa."'");
		if(mysqli_num_rows($sql) > 0){	
			$row = mysqli_fetch_array($sql);
			
			$settings['results'] = $row['results'];
			$settings['results_chart'] = $row['results_chart'];
		}
		
		return $settings;
	}
	
	
	public function ajax() {
		
		if(isset($_GET['a']) && $_GET['a'] == 'save_settings'){
			
			$results = isset($_POST['results']) ? $_POST['results'] : '';
			$results_chart = isset($_POST['results_chart']) ? $_POST['results_chart'] : '0';
			
			$sql = sisplet_query("INSERT INTO srv_quiz_settings 
									(ank_id, results, results_chart) VALUES ('".$this->anketa."', '".$results."', '".$results_chart."') 
									ON DUPLICATE KEY UPDATE results='".$results."', results_chart='".$results_chart."'");
			if (!$sql) echo mysqli_error($GLOBALS['connect_db']);
		}

	}
}