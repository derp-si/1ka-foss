<?php
/**
 *
 *	Modul za povezovanje panela (npr. Valicon, GFK...) z 1ka anketo
 *
 */

class SurveyEmailAccess{

	var $anketa;				# id ankete
	var $db_table = '';	

	
	function __construct($anketa){
		global $site_url;

		// Ce imamo anketo, smo v status->ul evealvacija
		if ((int)$anketa > 0){
			$this->anketa = $anketa;

			# polovimo vrsto tabel (aktivne / neaktivne)
			SurveyInfo :: getInstance()->SurveyInit($this->anketa);
			$this->db_table = SurveyInfo::getInstance()->getSurveyArchiveDBString();
		}
	}
	
	
	// Prikazemo nastavitve pri vklopu naprednega modula
	public function displaySettings(){
		global $lang;
		global $site_url;
		
		$preklici_url = ltrim(str_replace("&s=1","",$_SERVER['REQUEST_URI']),"/");
		$preklici_url = "'". $site_url . $preklici_url . "'";
		
		$rowA = SurveyInfo::getInstance()->getSurveyRow();
		$row = $this->getEmailAccessSettings();


		echo '<fieldset><legend>'.$lang['settings'].'</legend>';

		// Captcha
        echo '<div class="setting_holder">';
        echo '  <input type="checkbox" id="captcha" name="captcha" value="1" '.($row['captcha'] == 1 ? ' checked="checked"' : '').'>';
        echo '  <label for="captcha">'.$lang['srv_captcha_edit'].'</label> ';
        echo '</div>';

		// Text pogoji uporabe
		$pogoji_warning = $row['terms_text'] == '' ? true : false;
		echo '<div class="setting_holder">';
		echo '	<p '.($pogoji_warning ? 'class="red"' : '').'>'.$lang['user_agreement'] . ($pogoji_warning ? ' (polje obvezno izpolnite)' : '').':</p>';
		echo '	<textarea name="terms_text" class="wauto '.($pogoji_warning ? 'redBorder' : '').'" id="terms_text" placeholder="Dopolnite s Pogoji uporabe osebnih podatkov">'.$row['terms_text'].'</textarea>';
        echo '</div>';

		echo '</fieldset>';
		

		// Gumb shrani
		echo '<div class="button_holder">';
		echo '<button class="medium white-blue" onClick="window.location='.$preklici_url.'; return false;">'.$lang['edit1338'].'</button>';				
		echo '<button class="medium blue" onclick="email_access_save_settings(); return false;">'.$lang['edit1337'].'</button>';
		echo '</div>';

		echo '<div id="success_save"></div>';
	}	

	// Izvedemo vse potrebno, ko modul aktiviramo
	public function activateEmailAccess(){
		global $lang;

		// privzeto besedilo za pogoje uporabe
		$terms_text = '';

		// Vstavimo vrstico z nastavitvami
		$sql1 = sisplet_query("INSERT INTO srv_email_access_settings (ank_id, terms_text) VALUES ('".$this->anketa."', '".$terms_text."')");

		// Ugasnimo belezenje parapodatkov - samo za mju
		if(true){
			SurveySetting::getInstance()->Init($this->anketa);
			
			SurveySetting::getInstance()->setSurveyMiscSetting('survey_ip', '1');
			SurveySetting::getInstance()->setSurveyMiscSetting('survey_browser', '1');
			SurveySetting::getInstance()->setSurveyMiscSetting('survey_referal', '1');
			//SurveySetting::getInstance()->setSurveyMiscSetting('survey_date', '1');
		}
	}	
	
	// Vrnemo nastavitve modula
	public function getEmailAccessSettings($what = ''){
		
		if($what != ''){
			$sql = sisplet_query("SELECT ".$what." FROM srv_email_access_settings WHERE ank_id='".$this->anketa."'");
			$row = mysqli_fetch_array($sql);
			
			return $row[$what];
		}
		else{
			$sql = sisplet_query("SELECT * FROM srv_email_access_settings WHERE ank_id='".$this->anketa."'");
			$row = mysqli_fetch_array($sql);
			
			return $row;
		}
	}
	

	// Zgeneriramo kodo za dostop do ankete in jo posljemo na mail
	public function sendAccessCode($email){
		global $lang;

		$return = array();

		// Dobimo domeno iz emaila
		$parts = explode("@", $email);
		$domain = $parts[1];

		// Preverimo ce je veljaven email
		if(!validEmail($email)){
			$return['error']['email'] = 'Neveljaven email';
		}
		elseif($this->checkEmailBlacklist($domain)){			
			$return['error']['email'] = 'Uporaba email naslova z domene '.$domain.' ni dovoljena.';
		}
		// Preverimo, ce email ze obstaja v bazi
		else{
			$sql = sisplet_query("SELECT * FROM srv_email_access WHERE ank_id='".$this->anketa."' AND email='".$email."' AND status!='0'");
			if(mysqli_num_rows($sql) > 0){
				$return['error']['email'] = 'Na ta email je bila že poslana koda.';
			}
		}

		// Preverimo, ce se je strinjal s pogoji uporabe
		if(isset($_POST['terms']) && $_POST['terms'] != '1'){
			$return['error']['terms'] = 'Strinjanje s pogoji uporabe je obvezno.';
		}

		if(isset($return['error']['email']) || isset($return['error']['terms'])){
			return $return;
		}


		// Zgeneriramo 8 mestno kodo za dostop
		//$chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
		$chars = '0123456789';

		// Preverimo za vsak slučaj, da koda še ne obstaja
		$code_ok = false;		
		while(!$code_ok){
			$code = substr(str_shuffle($chars), 0, 6);
			
			$sql = sisplet_query("SELECT * FROM srv_email_access WHERE ank_id='".$this->anketa."' AND access_code='".$code."'");
			if(mysqli_num_rows($sql) == 0){
				$code_ok = true;
			}
		}

		// Vstavimo podatke v bazo
		$sql = sisplet_query("INSERT INTO srv_email_access
									(ank_id, email, access_code, status)
									values
									('".$this->anketa."', '".$email."', '".$code."', '0')
								ON DUPLICATE KEY UPDATE
									access_code='".$code."'
							");


		// Sestavimo email za posiljanje
		$email_subject = $lang['srv_email_access_email_subject'];

		$email_body = '<p>'.$lang['srv_email_access_email_body0'].'</p>';
		$email_body .= '<p>'.$lang['srv_email_access_email_body1'].'</p>';
		$email_body .= '<span style="font-size:24px; font-weight:600;">'.substr($code, 0, 3).' '.substr($code, 3, 6).'</span>';
		$email_body .= '<p>'.$lang['srv_email_access_email_body2'].' \''.SurveyInfo::getSurveyTitle().'\'.</p>';
		$email_body .= '<p>'.$lang['srv_email_access_email_body3'].'</p>';
		

		// Posljemo email s kodo
		try{
			$MA = new MailAdapter($this->anketa, $type='invitation');
			$MA->addRecipients($email);
			$resultX = $MA->sendMail($email_body, $email_subject);
		}
		catch (Exception $e) {
			$return['error']['email'] = 'Napaka pri pošiljanju kode na email.';
			return $return;
		}

		// Na koncu popravimo status, da je bil email uspesno poslan
		$sql = sisplet_query("UPDATE srv_email_access SET status='1' WHERE ank_id='".$this->anketa."' AND email='".$email."'");

		return true;
	}

	// Preverimo, ce imamo vneseno kodo in ce se ni bila uporabljena - potem ustvarimo cookie in nastavimo status na uporabljeno kodo
	public function checkAccessCode($code){

		// Preverimo kodo
		$sql = sisplet_query("SELECT * FROM srv_email_access WHERE ank_id='".$this->anketa."' AND access_code='".$code."'");
		
		// Koda ne obstaja v bazi
		if(mysqli_num_rows($sql) == 0){
			$return['error'] = 'Vnešena koda ni pravilna!';
			return $return;
		}
		$row = mysqli_fetch_array($sql);

		// Koda še ni bila poslana
		if($row['status'] == '0'){
			$return['error'] = 'Vnešena koda še ni bila aktivirana!';
			return $return;
		}

		// Koda je bila že uporabljena
		if($row['status'] == '2'){
			$return['error'] = 'Vnešena koda je bila že uporabljena!';
			return $return;
		}

		// Vse ok, nastavimo status da je bila koda uporabljena in nastavimo cookie
		$sql = sisplet_query("UPDATE srv_email_access SET status='2' WHERE ank_id='".$this->anketa."' AND access_code='".$code."'");

		return true;
	}

	// Preverimo, ce email obstaja na blacklisti zacasnih emailov
	private function checkEmailBlacklist($domain){
		global $site_path;

		$filename = $site_path."/admin/survey/modules/mod_email_access/disposable_email_blocklist.txt";
		$file = new SplFileObject($filename);
		
		$found = false;

		// Loop through each line in the file
		foreach ($file as $line) {
			$line = trim($line);
			
			// Check if the line matches the email
			if ($line === $domain) {
				$found = true;
				break;
			}
		}

		$file = null; // Close the file

		return $found;
	}



	// Pobrisemo vse kode povezane za anketo (za mju - 7 dni po deaktivaciji ankete)
	private static function deleteSurveyEmailsAndCodes($ank_id){

		if ((int)$ank_id <= 0)
			return;

		$sql = sisplet_query("DELETE * FROM srv_email_access WHERE ank_id='".$ank_id."'");
	}

	
	public function ajax() {
		
		if(isset($_GET['a']) && $_GET['a'] == 'save_settings'){
			
			$captcha = isset($_POST['captcha']) ? $_POST['captcha'] : '0';
			$terms_text = isset($_POST['terms_text']) ? $_POST['terms_text'] : '';
			

			$sql = sisplet_query("UPDATE srv_email_access_settings 
									SET captcha='".$captcha."', terms_text='".$terms_text."'
									WHERE ank_id='".$this->anketa."'
								");
			if (!$sql) echo mysqli_error($GLOBALS['connect_db']);


			$this->displaySettings();
		}

	}
}