
// Prikaze popup v urejanju ankete (spodnja vrstica pod vsakim vprasanjem - 3 gumbi)
function openCommentPopupInline (type, spremenljivka) {
	
    // Vse odprte najprej zapremo
    $('.comment_inline_popup').addClass('displayNone');

    // dodaj komentar
	if (type==1){
		$("#comment_inline_add_"+spremenljivka).removeClass('displayNone');
    }
    // komentarji urednikov
    else if (type==2){
		$("#comment_inline_editor_"+spremenljivka).removeClass('displayNone');
    }
    // komentarji respondentov
    else if (type==3){
		$("#comment_inline_respondent_"+spremenljivka).removeClass('displayNone');
    }
}

// Zapre popupe v urejanju ankete (spodnja vrstica pod vsakim vprasanjem - 3 gumbi)
function closeCommentPopupInline (type, spremenljivka) {
    
    // dodaj komentar
	if (type==1){
		$("#comment_inline_add_"+spremenljivka).addClass('displayNone');
    }
    // komentarji urednikov
    else if (type==2){
		$("#comment_inline_editor_"+spremenljivka).addClass('displayNone');
    }
    // komentarji respondentov
    else if (type==3){
		$("#comment_inline_respondent_"+spremenljivka).addClass('displayNone');
    }
}

// Prikaze popup v urejanju ankete (spodnja vrstica pod vsakim pogojem - 2 gumba)
function openIfCommentPopupInline (type, if_id) {
	
    // Vse odprte najprej zapremo
    $('.comment_inline_popup').addClass('displayNone');

    // dodaj komentar
	if (type==1){
		$("#comment_if_inline_add_"+if_id).removeClass('displayNone');
    }
    // komentarji urednikov
    else if (type==2){
		$("#comment_if_inline_editor_"+if_id).removeClass('displayNone');
    }
}

// Zapre popupe v urejanju ankete (spodnja vrstica pod vsakim pogojem - 2 gumba)
function closeIfCommentPopupInline (type, if_id) {
    
    // dodaj komentar
	if (type==1){
		$("#comment_if_inline_add_"+if_id).addClass('displayNone');
    }
    // komentarji urednikov
    else if (type==2){
		$("#comment_if_inline_editor_"+if_id).addClass('displayNone');
    }
}


function toggleCommentDropdown () {
	$("#comment_dropdown").toggleClass('displayNone');
}

function toggleCommentDropdownTestSurvey () {
	$("#comment_dropdown_test_survey").toggleClass('displayNone');
	$(".open_comment_box").toggleClass('active');
}

function toggleCommentOptions(id) {
	$("#options_"+id).toggleClass("displayNone");
}


var siteurl = '';
if ( typeof srv_site_url !== 'undefined' ) {	// komentarji v izpolnjevanju ankete
	siteurl = srv_site_url + '/admin/survey/';
}

// doda komentar in osvezi oblacek
// type=0 : anketa, type=1 : vprasanje
function add_comment (spremenljivka, type, view, vsebina) {
    
    $('div#survey_comment_'+spremenljivka+'_'+view).load(siteurl+'ajax.php?t=branching&a=comment_manage', {type: type, view: view, spremenljivka: spremenljivka, vsebina: vsebina, anketa: srv_meta_anketa_id, refresh: '1'}, 
        function () {

            // V predogledu izpisemo samo text da je bil uspesno dodan
            if(view == 1 && (type == 0 || type == 4)){
                $('#comment_dropdown_test_survey').html('<div class="success">'+lang['action_note_comment_added']+'</div>');
            }
            // pri opciji Dodaj komentar (urejanje vprasalnika - pod vsakim vprasanjem), skrijemo oblacek po submitu
            else if (view == 0) {
                $(".comment_inline_popup").addClass('displayNone');			
            } 
            else if (view == 4 || view == 5) {
                window.location.reload();
            }
        }
    );
}

// nastavi podanemu linku (a.surveycomment) oblacek za komentarje
function load_comment (__this, show) {
    if (show == 1) {    // opcije za takrat, ko je po defaultu ze na zacetku prikazan - za dodat komentar na anketo
        _when = false;
        _ready = true;
        var corners = ['leftMiddle', 'leftMiddle'];
    	var opposites = ['rightMiddle', 'rightMiddle'];
    } else if (show == 2) {	// opcija za takrat, ko se aktivira preko oblacka, ko se prikaze takoj
        _when = 'click';
        _ready = true;
        var corners = ['topRight', 'topRight'];
    	var opposites = ['bottomLeft', 'bottomLeft'];
    } else {		// default za normalne komentarje, da se odpre na klik
        _when = 'click';
        _ready = false;
        var corners = ['topLeft', 'topRight'];
    	var opposites = ['bottomRight', 'bottomLeft'];
    }

    var width = $(document).width();
    // nastavitve za help

    // preverimo ali prikažemo tip na levo stran
    var position = $(__this).offset(); // position = { left: 42, top: 567 }

    var left = width - position.left;

    var i = (left >= 350) ? 0 : 1;

    if ($(__this).data("qtip")) {
        $(__this).qtip("destroy");
        $('div.qtip').html('');
    }

	// Posebej naslov ce smo v urejanju
	if($(__this).attr('view') == '1' && !show && $('#comment_qtip_title').length > 0 && $(__this).attr('subtype') == undefined){
		var naslov = $('#comment_qtip_title').val();
	}
	// komentar na vprasanje
	else if($(__this).attr('subtype') == 'q_admin_add'){
		var naslov = lang['srv_testiranje_komentar_q_title'];
	}
	// komentar na if
	else if($(__this).attr('subtype') == 'if_admin_add'){
		var naslov = lang['srv_testiranje_komentar_if_title'];
	}
	// komentarji na if
	else if($(__this).attr('subtype') == 'if_admin_all'){
		var naslov = lang['srv_testiranje_komentar_if_all_title'];
	}
	// komentar na blok
	else if($(__this).attr('subtype') == 'blok_admin_add'){
		var naslov = lang['srv_testiranje_komentar_blok_title'];
	}
	// komentarji na blok
	else if($(__this).attr('subtype') == 'blok_admin_all'){
		var naslov = lang['srv_testiranje_komentar_blok_all_title'];
	}
	// komentarji na vprasanja
	else if($(__this).attr('subtype') == 'q_admin_all'){
		var naslov = lang['srv_testiranje_komentar_q_all_title'];
	}
	// komentarji respondentov na vprasanja
	else if($(__this).attr('subtype') == 'q_resp_all'){
		var naslov = lang['srv_testiranje_komentar_q_resp_all_title'];
	}
	else{
		var naslov = lang['srv_testiranje_komentarji_anketa_title2'];
	}

    $(__this).qtip({
        content: {text: '<div class="qtip_comment"><h1>'+naslov+'</h1><div id="survey_comment_'+$(__this).attr('spremenljivka')+'_'+$(__this).attr('view')+'"></div></div>', title: {button: '&#x2715;'}},
        fixed: false, show: {when: _when, ready: _ready, solo: true},hide: {when: 'click'},
        style: {name: 'light', border: {width: 3, radius: 8}, width: 350, tip: {corner: corners[i]}},
        position: {corner: {tooltip: corners[i], target: opposites[i] }, adjust: {screen : true}},
        api: {
            beforeShow: function () {
                // tuki se poklice zato, ker se drugace content: {url: ....} ne refresha, ce zapres in spet odpres oblacek
                _comment = 1;
                $('div#survey_comment_'+$(__this).attr('spremenljivka')+'_'+$(__this).attr('view')).load(siteurl+'ajax.php?t=branching&a=comment_manage', {anketa: srv_meta_anketa_id, type: $(__this).attr('type'), view: $(__this).attr('view'), spremenljivka: $(__this).attr('spremenljivka'), vsebina: '', anketa: srv_meta_anketa_id, refresh: '1'});
            },
            onShow: function () {
            	$('div.qtip').draggable();
            }
        }
    });

}
var _comment = 0;