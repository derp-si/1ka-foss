function diagnosticsChart(place,display) {

	const data = {
		datasets: [{
		  data: display,
		  backgroundColor: [
			'rgb(229,229,229)',
			'rgb(30, 136, 229)',
		  ],
		  borderWidth: 0, //spacing between slices
		}]
	};
	
	  const config = {
		type: 'doughnut',
		data: data,
		options: {
			events: [], //no hover labels,animations,...
			cutout: 30, //slice width
		}
	};

	  const myChart = new Chart(
		document.getElementById(place),
		config
	  );

}