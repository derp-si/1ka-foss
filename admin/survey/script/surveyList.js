/** Izbrise anketo
 * 
 * @param anketa
 * @param confirmtext
 * @return
 */
 function anketa_delete_list (anketa, confirmtext) {
	if (confirm(confirmtext)) {
		$("#anketa_list_"+anketa).slideUp();
        $.post('ajax.php?a=anketa_delete', {anketa: anketa, 'inList': 'true'}, function(response) {
        	if (response == '0') {
        		window.location = 'index.php';
        	}
        });
	}
}

/** doda/odstrani anketo v sistemsko knjiznico in refresa ikono za knjiznico ankete
 * 
 */
function surveyList_knjiznica (anketa) {
	$("ul#surveyList").find("li#anketa_list_"+anketa).find(".sl_lib_glb").load('ajax.php?t=surveyList&a=surveyList_knjiznica', {anketa: anketa});
}

/** navadnega uporabnika obvesti da nima dostopa za dodajanje v sistemsko knjiznico
 * 
 */
function surveyList_knjiznica_noaccess (msg) {
	genericAlertPopup('alert_parameter_msg');
}

/** doda/odstrani anketo v uporabnisko knjiznico in refresa ikono za knjiznico ankete
* 
*/
function surveyList_myknjiznica (anketa) {
	$("ul#surveyList").find("li#anketa_list_"+anketa).find(".sl_lib_usr").load('ajax.php?t=surveyList&a=surveyList_myknjiznica', {anketa: anketa});
    //$('#folders').load('ajax.php?t=folders&a=folders_myknjiznica', {anketa: anketa});
}

function surveyList_myknjiznica_new (anketa) {
	$.post('ajax.php?t=surveyList&a=surveyList_myknjiznica_new', {anketa: anketa}, function() {
		window.location.reload(true);
	});
}

function surveyList_knjiznica_new (anketa) {
	$.post('ajax.php?t=surveyList&a=surveyList_knjiznica_new', {anketa: anketa}, function() {
		window.location.reload(true);
	});
}

// Prikaz vec na tri pikice v tabeli anket
function surveyList_show_more(ank_id){

    if($('#survey_other_box_'+ank_id).hasClass('displayNone')){
        $('.survey_other_box').addClass('displayNone');
        $('#survey_other_box_'+ank_id).removeClass('displayNone');
    }
    else{
        $('.survey_other_box').addClass('displayNone');
    }
    
}



/* MOJE ANKETE - pogled z mapami */

// Preklopimo med prikazom folderjev in navadnim prikazom
function switchFolder(show){
	if(show == 1)
		var show_folders = 0;
	else
		var show_folders = 1;

	$('#survey_list').load('ajax.php?a=surveyList_folders', {show_folders:show_folders}, function(){
        $('body').toggleClass('body_mySurveys_folders');
        $('body').toggleClass('body_mySurveys');
    });
}

// inicializiramo drag/drop anket in folderjev
function mySurvey_folder_init() {

	$('#survey_list .mySurvey_droppable').droppable({
		accept: '#survey_list .mySurvey_draggable', 
		hoverClass: 'folderhover', 
		tolerance: 'pointer',
        drop: function (e, ui) {

			// Drop folderja
			if($(ui.draggable).hasClass('folder_item')){

				var drag_folder_id = $(ui.draggable).attr('folder_id');
				var parent_folder_id = $(this).attr('folder_id');
				
				$('#survey_list').load('ajax.php?t=surveyListFolders&a=mysurvey_folder_drop', {parent_folder_id: parent_folder_id, drag_folder_id: drag_folder_id});
			}
			
			// Drop ankete
			if($(ui.draggable).hasClass('anketa_list')){

				var drag_survey_id = $(ui.draggable).attr('anketa_id');
				var parent_folder_id = $(this).attr('folder_id');
                
				$('#survey_list').load('ajax.php?t=surveyListFolders&a=mysurvey_survey_drop', {parent_folder_id: parent_folder_id, drag_survey_id: drag_survey_id});
			}
        }
    });
	
	$('#survey_list .mySurvey_draggable').draggable({
		revert: 'invalid', 
		opacitiy: '0.9', 
		helper: 'clone',
		cursor: 'move',
		cursorAt: { left: 20 },
		start: function(e, ui){
			$(ui.helper).addClass('mySurvey_draggable_helper');
		}
	});
}

// Razpremo/skrcimo folder
function mySurvey_folder_toggle(folder_id){

    if($('#folder_item_'+folder_id).hasClass('open'))
        var open = 0;
    else
        var open = 1;

    $.post('ajax.php?t=surveyListFolders&a=mysurvey_folder_toggle', {folder_id: folder_id, open: open}, function(){

        if($('#folder_item_'+folder_id).hasClass('open')){
            $('#folder_item_'+folder_id).removeClass('open');
            $('#folder_list_'+folder_id).slideUp();
        }
        else{
            $('#folder_item_'+folder_id).addClass('open');
            $('#folder_list_'+folder_id).slideDown();
        }
    });

    event.stopPropagation();
}

// Aktiviramo folder
function mySurvey_folder_activate(folder_id){

    $('#right_content').load('ajax.php?t=surveyListFolders&a=mysurvey_folder_activate', {folder_id: folder_id}, function(){

        // Pobrisemo aktivni class aktivnega
        $('.folder_item').removeClass('active');
        
        // Dodamo aktivni class novemu
        $('#folder_item_'+folder_id).addClass('active');
	});
}

// Prikazemo opcije za urejanje folderja
function mySurvey_folder_show_edit(element){
    
    if($(element).parent().find('.folder_item_settings').hasClass('displayNone')){
        $('.folder_item_settings').addClass('displayNone');
        $('.dots_ver_folder').removeClass('active');
        $(element).parent().find('.folder_item_settings').removeClass('displayNone');
        $(element).addClass('active');
    }
    else{
        $('.folder_item_settings').addClass('displayNone');
        $('.dots_ver_folder').removeClass('active');
        $(element).parent().find('.folder_item_settings').addClass('displayNone');
        $(element).removeClass('active');
    }

    event.stopPropagation();
}


// Zaprtje dodatnega popupa
function mySurvey_folder_close_popup(){
    $('#fade').fadeOut('slow');
    $("#mySurvey_additional_popup").fadeOut().html();
}

// Ime novega folderja
function mySurvey_folder_add_popup(parent_folder_id){

    $('#fade').fadeTo('slow', 1);
    $("#mySurvey_additional_popup").fadeIn();
    $("#mySurvey_additional_popup").load('ajax.php?t=surveyListFolders&a=mysurvey_folder_add_popup', {folder_id: parent_folder_id});
}

// Dodamo nov folder
function mySurvey_folder_add(parent_folder_id){

    var folder_name = $("#mySurvey_folder_name").val();

    $("#left_content").load('ajax.php?t=surveyListFolders&a=mysurvey_folder_add', {folder_id: parent_folder_id, folder_name: folder_name}, function(){
        mySurvey_folder_close_popup();
    });
}

// Ime obstojecega folderja
function mySurvey_folder_rename_popup(folder_id, folder_name){

    $('#fade').fadeTo('slow', 1);
    $("#mySurvey_additional_popup").fadeIn();
    $("#mySurvey_additional_popup").load('ajax.php?t=surveyListFolders&a=mysurvey_folder_rename_popup', {folder_id: folder_id, folder_name: folder_name});
}

// Preimenujemo obstojeci folder
function mySurvey_folder_rename(folder_id){

    var folder_name = $("#mySurvey_folder_name").val();

    $("#left_content").load('ajax.php?t=surveyListFolders&a=mysurvey_folder_rename', {folder_id: folder_id, folder_name: folder_name}, function(){
        mySurvey_folder_close_popup();
    });
}

// Pobrisemo obstojeci folder
function mySurvey_folder_delete(folder_id){

    $("#survey_list").load('ajax.php?t=surveyListFolders&a=mysurvey_folder_delete', {folder_id: folder_id});
}




/*

// prikazemo/skrijemo ankete v folderju
function toggle_folder (folder) {
	
	var open = 0;
	if($('#folder_content_'+folder).hasClass('closed')){
		open = 1;
	}
	
	$.post('ajax.php?t=surveyList&a=folder_toggle', {folder: folder, open: open}, function(){
		if(open == 1){
			$('#folder_content_'+folder).removeClass('closed');
			//$('#folder_'+folder).find('.arrow2_u').removeClass('arrow2_u').addClass('arrow2_d');
			
            $('#folder_'+folder).removeClass('closed').addClass('open');
		}
		else{
			$('#folder_content_'+folder).addClass('closed');		
			//$('#folder_'+folder).find('.arrow2_d').removeClass('arrow2_d').addClass('arrow2_u');
			
            $('#folder_'+folder).removeClass('open').addClass('closed');
		}
	});
}


// Kopiramo folder
function copy_folder(folder){

	$.post('ajax.php?t=surveyList&a=folder_copy', {folder: folder}, function(){
		window.location.reload();
	});
}

*/



