
// Prikaz popupa za preklop domene
function popupSwitch() {
    
    $('#fade').fadeTo('slow', 1);
    $("#popup_user_access").load('ajax.php?t=domainChange&a=displaySwitchPopup');
    $("#popup_user_access").show();
}

// Prikaz popupa za preklop domene - zapri
function popupSwitch_close() {

	$("#popup_user_access").html().hide();
	$('#fade').fadeOut('slow');
}

// 
function popupSwitch_save(switch_status){
    
    $.post('ajax.php?t=domainChange&a=setSwitchStatus', {switch_status:switch_status}, function (response) {
        
        var data = JSON.parse(response);

        if(data['action'] == 'switch_domain'){
            window.location = data['url'];
        }
    });
}