// Odpremo knjiznico v urejanju ankete
function displayVprasanjeDeletedPopup(){

    $('#fade').fadeTo('slow', 1);
	$('#general_popup').html('').addClass('vprasanje_deleted_popup').fadeIn('slow');

	$("#general_popup").load('ajax.php?t=vprasanjeDeleted&a=displayVprasanjeDeletedPopup', {anketa: srv_meta_anketa_id});
}


// Klik na posamezen item na desni
function selectVprasanjeDeletedItem(item_id){

    if($('#question_item_holder_'+item_id).hasClass('active')){
        $('#question_item_holder_'+item_id).removeClass('active');
        $('#question_item_check_'+item_id).attr("checked", false);
    }
    else{
        $('#question_item_holder_'+item_id).addClass('active');
        $('#question_item_check_'+item_id).attr("checked", true);
    }

    // Prestejemo oznacene iteme
    var count = $('.question_item_check:checkbox:checked').length;
    $('#selected_item_counter').html(count);

    // Nastavimo gumb dodaj kot disabled glede na to ce imamo elemente ali ne
    if(count > 0){
        $("#insert_vprasanje_deleted_button").prop('disabled', false);
    }
    else{
        $("#insert_vprasanje_deleted_button").prop('disabled', true);
    }
}

function deleteVprasanjeDeleted(item_id, item_type){

    $.post('ajax.php?t=vprasanjeDeleted&a=deleteItem', {anketa: srv_meta_anketa_id, item_id: item_id, item_type: item_type}, function(){
        $('#question_item_holder_'+item_id).remove();
    });

    event.stopPropagation();
}

// Restavriramo pobrisano vprasanje nazaj v anketo na konec
function insertVprasanjeDeletedItemsIntoSurvey(){

    var items = [];

    var prev = false;
    var note = '1';

    $('.question_item_check:checkbox:checked').each(function(){
        var id_string = $(this).attr("id");
        var id = id_string.substring(20);

        items.push(id);

        // Preverimo za obvestilo, ce dodajamo vec elementov in ce so razlicni
        if(prev){
            note = '1m';
        }

        prev = true;
    });    

    $("#branching").load('ajax.php?t=vprasanjeDeleted&a=addIntoSurvey', {anketa: srv_meta_anketa_id, items: items}, function(){

        popupClose();        
        actionNotePopup('lib_add_to_survey_'+note, 'success');
    });
}


// Predogled pobrisanega vprasanjaoz. vprasanja iz knjiznice
function previewVprasanje(spr_id){

	$('#vprasanje_preview').load('ajax.php?a=preview_spremenljivka_lib', {anketa: srv_meta_anketa_id, spremenljivka: spr_id, podstran: srv_meta_podstran}).removeClass('displayNone');

    event.stopPropagation();
}

// Predogled pobrisanega vprasanja
function closePreviewVprasanje(){
	$('#vprasanje_preview').addClass('displayNone').empty();
}
