//meta podatki
var srv_meta_anketa_id 	= $("#srv_meta_anketa").val();
var srv_meta_podstran 	= $("#srv_meta_podstran").val();
var srv_meta_grupa 		= $("#srv_meta_grupa").val();
var srv_meta_branching 	= $("#srv_meta_branching").val();

// poklice se iz htmlja ob prikazu librarija (init)
function library () {
	var lib_tab = $("input#lib_tab").val();
	
    // nardimo ankete, vprasanja draggable
    //$('div#libraryInner div.folder_container, div.anketa_vprasanja span').draggable({opacity:'0.5', zIndex:'50', helper: 'clone', appendTo: 'body', distance:5});
    
    // nardimo folderje draggable
    $('div#libraryInner ul.can_edit li.folder.can_drag, div#libraryInner li.anketa.can_drag').draggable({
        revert: 'invalid', 
        opacitiy: '0.5', 
        zIndex: '1', 
        handle: '.movable', 
        distance: 5,
        handle: '.folder_box, folder_container'
    });
	
    // nardimo folderje droppable
    $('div#libraryInner ul.can_edit .folderdrop').droppable({
        hoverClass: 'folderhover', 
        tolerance: 'pointer', 
        drop: function (e, ui) {

	        // premikanje folderjev v librariju
            if ($(ui.draggable).attr('name') == 'folder') {
                $('#libraryInner').load('ajax.php?t=library&a=folder_dropped', {drop: $(ui.draggable).attr('eid'), folder: $(this).attr('eid'), tab: lib_tab});
            }
            // premikanje anket
            else if ($(ui.draggable).attr('name') == 'library') {		
                $('#libraryInner').load('ajax.php?t=library&a=spr_dropped', {spremenljivka: $(ui.draggable).attr('eid'), folder: $(this).attr('eid'), tab: lib_tab});
            }
        }
    });
    
    // preview vprasanja
    //$('div#libraryInner div.folder_container, div.anketa_vprasanja span').bind('mouseover', function (event) {
    $('div#libraryInner div.folder_container.new_spr, div.anketa_vprasanja span.new_spr').bind('mouseover', function (event) {
        var copy = $(event.target).closest('[copy]').attr('copy');
        
        if (copy > 0 && !is_new_spr_dragable) {
        	show_tip_preview_toolbox(0, copy);
        }
	}).bind('mouseout', function (event) {
		$("#tip_preview").hide();
	});
    
}

function library_if_new (copy) {
	$('html, body').animate({scrollTop: $('body').height()+$('#branching').height()});
	if_new(0, 0, 1, '0', copy);
}

// prikaze knjiznico
function display_knjiznica(tab) {
	var lib_prva = $("input#lib_prva").val();

    $('#library_holder').load('ajax.php?t=library&a=display_knjiznica', {tab: tab, prva: lib_prva, anketa: srv_meta_anketa_id});
    
}

// zamenja view knjiznice na prvi strani
function change_knjiznica(tab) {
	var lib_prva = $("input#lib_prva").val();
	$('#library').load('ajax.php?t=library&a=display_knjiznica', {tab: tab, prva: lib_prva});        
}

// doda spremenljivko v knjiznico
/*function knjiznica_dodaj (spremenljivka) {
	var lib_tab = $("input#lib_tab").val();
	var lib_prva = $("input#lib_prva").val();
	$('#branching_vprasanja').load('ajax.php?t=library&a=knjiznica_dodaj', {spremenljivka: spremenljivka, tab: lib_tab, prva: lib_prva, anketa: srv_meta_anketa_id});
}*/

function folder_rename (folder) {
	$('#sp'+folder).load('ajax.php?t=library&a=folder_rename', {folder: folder},
        function () {
            $('#naslov_'+folder).focus();
        }
    );
}
function library_folder_newname (folder) {
	var lib_tab = $("input#lib_tab").val();
    $('#libraryInner').load('ajax.php?t=library&a=folder_newname', {folder: folder, naslov: $('#naslov_'+folder).attr('value'), tab: lib_tab});
}

function library_new_folder (folder, uid) {
	var lib_tab = $("input#lib_tab").val();
    $('#libraryInner').load('ajax.php?t=library&a=new_folder', {folder: folder, uid: uid, tab: lib_tab});
}

function library_delete_folder (folder) {
	var lib_tab = $("input#lib_tab").val();

    $('#libraryInner').load('ajax.php?t=library&a=delete_folder', {folder: folder, tab: lib_tab});
}


// Prikaz vec na tri pikice
function library_show_more(ank_id){

    if($('#library_more_box_'+ank_id).hasClass('displayNone')){
        $('.library_more_box').addClass('displayNone');
        $('#library_more_box_'+ank_id).removeClass('displayNone');
    }
    else{
        $('.library_more_box').addClass('displayNone');
    }   
}

function library_del_anketa (anketa, text) {
    if (confirm(text)) {
    	var lib_tab = $("input#lib_tab").val();
        $('#libraryInner').load('ajax.php?t=library&a=library_del_anketa', {anketa: anketa, tab: lib_tab});
    }
}

function library_del_myanketa (anketa, text) {
    if (confirm(text)) {
    	var lib_tab = $("input#lib_tab").val();
        $('#libraryInner').load('ajax.php?t=library&a=library_del_myanketa', {anketa: anketa, tab: lib_tab});
    }
}

function add_to_my_library () {
	
	$.post('ajax.php?t=library&a=library_add_myanketa', {anketa: srv_meta_anketa_id}, function () {
		window.location.href = 'index.php?anketa='+srv_meta_anketa_id+'&tab=2';
	});
	
}

function anketa_copy (ank_id) {
	var naslov = $("#novaanketa_naslov").val(); 
	$.redirect('ajax.php?t=library&a=anketa_copy_new', {ank_id: ank_id, naslov: naslov});		
}

function anketa_copy_top (ank_id, hierarhija) {
	var hierarhija = hierarhija || 0;
	$.redirect('ajax.php?t=library&a=anketa_copy_new', {
		ank_id: ank_id,
		hierarhija: hierarhija
	});
}

function library_folders_plusminus (folder) {
    
	var lib_tab = $("input#lib_tab").val();
    var sortable_if = document.getElementById('folder_'+folder).style;
    
    if (sortable_if.display != "none") {
        $('#folder_'+folder).slideUp(); 
        $('#li'+folder).find('> .folder_box_holder > .folder_box').removeClass('open');  

        $.post('ajax.php?t=library&a=folder_collapsed', {collapsed: 1, folder: folder, tab: lib_tab});  
    } 
    else {        
        $('#folder_'+folder).slideDown();    
        $('#li'+folder).find('> .folder_box_holder > .folder_box').addClass('open');  

        $.post('ajax.php?t=library&a=folder_collapsed', {collapsed: 0, folder: folder, tab:lib_tab});
    }
}

function library_anketa_plusminus (anketa, _this) {
	
    var disp = document.getElementById('anketa_vprasanja_'+anketa).style;
    
    if (disp.display == "block") {
    	$('#anketa_vprasanja_'+anketa).slideUp();
    	$(_this).find('span').removeClass('minus').addClass('plus');
    
    } else {
        $('#anketa_vprasanja_'+anketa).slideDown();
    	$(_this).find('span').removeClass('plus').addClass('minus');
    }
}

// odstrani blok/if iz knjiznice
function library_if_remove (_if) {
	var lib_tab = $("input#lib_tab").val();
	var lib_prva = $("input#lib_prva").val();
	
    if (confirm(lang['srv_brisiifconfirm'])) {
        $('#libraryInner').load('ajax.php?t=library&a=if_remove', {'if': _if, anketa: srv_meta_anketa_id, tab:lib_tab, prva: lib_prva});
    }
}

// izbrise spremenljivko iz knjiznice
function library_brisi_spremenljivko (spremenljivka, text) {
	var lib_tab = $("input#lib_tab").val();

    if (confirm(text)) {
        $('#libraryInner').load('ajax.php?t=library&a=brisi_spremenljivko', {spremenljivka: spremenljivka, grupa: srv_meta_grupa, anketa: srv_meta_anketa_id, branching: srv_meta_branching, tab:lib_tab});
    }
}

function alert_copy_anketa(ank_id) {
	// prikazemo div z moznostmi za kopiranje ankete (1.prepise, 2.predhodno arhivira obstojeco, 3.preklici)
	$('#fade').fadeTo('slow', 1);
	$('#fullscreen').html('').fadeIn('slow').draggable({delay:100, cancel: 'input, .buttonwrapper'});
	$('#fullscreen').load('ajax.php?t=library&a=alert_copy_anketa', {anketa: srv_meta_anketa_id, ank_id:ank_id});
}
function alert_copy_anketa_cancle() {
	$('#fullscreen').fadeOut('slow').html('');
	$('#fade').fadeOut('slow');
}

function anketa_archive_and_copy(anketa, ank_id) {
	$.redirect('ajax.php?t=library&a=anketa_archive_and_copy', {anketa:anketa, ank_id: ank_id});
}

function anketa_copy_no_archive(anketa, ank_id) {
	$.redirect('ajax.php?t=library&a=anketa_copy', {anketa:srv_meta_anketa_id, ank_id: ank_id });
}

function lib_show_vprasanja() {
	window.location = 'index.php?anketa='+srv_meta_anketa_id;
}

function check_library () {
	
	if ( $('input[name=javne_ankete]:checked').val() == '1' ) {
		
		$('input[name=moje_ankete][value=0]').attr('checked', 'true');
		$('#moje_ankete').addClass('displayNone');
		
	} else {
		$('#moje_ankete').removeClass('displayNone');
	}
	
}



// Odpremo knjiznico v urejanju ankete
function displayLibraryPopup(){

    $('#fade').fadeTo('slow', 1);
	$('#general_popup').html('').addClass('library_popup').fadeIn('slow');

	$("#general_popup").load('ajax.php?t=libraryBranching&a=displayLibraryPopup', {anketa: srv_meta_anketa_id});
}

// Preklop med tabi
function displayLibraryTab(tab){

    if(tab == 1){
        $('#tab_0').removeClass('active');
        $('#tab_1').addClass('active');
    }
    else{
        $('#tab_1').removeClass('active');
        $('#tab_0').addClass('active');
    }

    $('#active_tab').val(tab);

    $("#tab_content").load('ajax.php?t=libraryBranching&a=displayLibraryTabContent', {anketa: srv_meta_anketa_id, tab: tab});
}

// Odpremo folder - na desni prikazemo vprasanja
function openLibraryFolder(folder_id){

    $('.folder_item').removeClass('active');
    $('#folder_item_'+folder_id).addClass('active').addClass('open');

    // Ce poddirektoriji niso vidni jih prikazemo
    if(!$('#folder_list_'+folder_id).is(':visible')){
        $('#folder_list_'+folder_id).slideDown();
    }

    var tab = $('#active_tab').val();

    $("#lib_question_list").load('ajax.php?t=libraryBranching&a=displayLibraryQuestionList', {anketa: srv_meta_anketa_id, tab: tab, folder_id: folder_id});


    // Ugasnemo vse prejšnje checkboxe
    $(".question_item_check").removeClass('active').attr("checked", false);

    // Nastavimo oznacene iteme na 0 in disablamo gumb
    $('#selected_item_counter').html('0');
    $("#insert_library_button").prop('disabled', true);
}

// Odpremo anketo - na desni prikazemo vprasanja
function openLibrarySurvey(folder_id, type){

    $('.folder_item').removeClass('active');
    $('#'+type+'_survey_item_'+folder_id).addClass('active');

    var tab = $('#active_tab').val();

    $("#lib_question_list").load('ajax.php?t=libraryBranching&a=displayLibrarySurveyQuestionList', {anketa: srv_meta_anketa_id, tab: tab, folder_id: folder_id});


    // Ugasnemo vse prejšnje checkboxe
    $(".question_item_check").removeClass('active').attr("checked", false);

    // Nastavimo oznacene iteme na 0 in disablamo gumb
    $('#selected_item_counter').html('0');
    $("#insert_library_button").prop('disabled', true);
}

// Razpremo/skrcimo folder na levi
function expandLibraryFolder(folder_id){

    if($('#folder_list_'+folder_id).is(':visible')){
        $('#folder_list_'+folder_id).slideUp();
        $('#folder_item_'+folder_id).removeClass('open');
    }
    else{
        $('#folder_list_'+folder_id).slideDown();
        $('#folder_item_'+folder_id).addClass('open');
    }

    event.stopPropagation();
}

// Prikazemo opcije za urejanje folderja
function showLibraryFolderEdit(element){
    
    if($(element).parent().find('.folder_item_settings').hasClass('displayNone')){
        $('.folder_item_settings').addClass('displayNone');
        $('.dots_ver_folder').removeClass('active');
        $(element).parent().find('.folder_item_settings').removeClass('displayNone');
        $(element).addClass('active');
    }
    else{
        $('.folder_item_settings').addClass('displayNone');
        $('.dots_ver_folder').removeClass('active');
        $(element).parent().find('.folder_item_settings').addClass('displayNone');
        $(element).removeClass('active');
    }

    event.stopPropagation();
}


// Zaprtje dodatnega popupa
function closeAdditionalPopup(){
    $("#lib_additional_popup").fadeOut().html();
}

// Ime novega folderja
function displayAddFolderPopup(parent_folder_id, uid){

    var tab = $('#active_tab').val();

    $("#lib_additional_popup").fadeIn();
    $("#lib_additional_popup").load('ajax.php?t=libraryBranching&a=addFolderPopup', {anketa: srv_meta_anketa_id, tab: tab, folder_id: parent_folder_id, uid: uid});
}

// Ime obstojecega folderja
function displayRenameFolderPopup(folder_id, folder_name){

    var tab = $('#active_tab').val();

    $("#lib_additional_popup").fadeIn();
    $("#lib_additional_popup").load('ajax.php?t=libraryBranching&a=renameFolderPopup', {anketa: srv_meta_anketa_id, tab: tab, folder_id: folder_id, folder_name: folder_name});
}


// Dodamo nov folder
function addLibraryFolder(parent_folder_id, uid){

    var tab = $('#active_tab').val();
    var folder_name = $("#lib_folder_name").val();

    $("#lib_folder_list").load('ajax.php?t=libraryBranching&a=addFolder', {anketa: srv_meta_anketa_id, tab: tab, folder_id: parent_folder_id, uid: uid, folder_name: folder_name});
}

// Pobrisemo obstojeci folder
function deleteLibraryFolder(folder_id){

    var tab = $('#active_tab').val();

    $("#lib_folder_list").load('ajax.php?t=libraryBranching&a=deleteFolder', {anketa: srv_meta_anketa_id, tab: tab, folder_id: folder_id});
}

// Preimenujemo obstojeci folder
function renameLibraryFolder(folder_id){

    var tab = $('#active_tab').val();
    var folder_name = $("#lib_folder_name").val();

    $("#lib_folder_list").load('ajax.php?t=libraryBranching&a=renameFolder', {anketa: srv_meta_anketa_id, tab: tab, folder_id: folder_id, folder_name: folder_name});
}


// Klik na posamezno anketo - prikazemo/skrijemo vprasanja na desni znotraj ankete (desni tab)
function toggleLibrarySurveyQuestions(folder_id){

    if($('#survey_title_'+folder_id).hasClass('active')){
        $('#survey_title_'+folder_id).removeClass('active');
        $('#survey_questions_'+folder_id).hide();
    }
    else{
        $('#survey_title_'+folder_id).addClass('active');
        $('#survey_questions_'+folder_id).show();
    }
}

// Klik na posamezen item na desni
function selectLibraryItem(item_id){

    if($('#question_item_holder_'+item_id).hasClass('active')){
        $('#question_item_holder_'+item_id).removeClass('active');
        $('#question_item_check_'+item_id).attr("checked", false);
    }
    else{
        $('#question_item_holder_'+item_id).addClass('active');
        $('#question_item_check_'+item_id).attr("checked", true);
    }

    // Prestejemo oznacene iteme
    var count = $('.question_item_check:checkbox:checked').length;
    $('#selected_item_counter').html(count);

    // Nastavimo gumb dodaj kot disabled glede na to ce imamo elemente ali ne
    if(count > 0){
        $("#insert_library_button").prop('disabled', false);
    }
    else{
        $("#insert_library_button").prop('disabled', true);
    }
}

// Prikaz urejanja posameznega itema na desni
function showLibraryItemEdit(element){
    
    if($(element).parent().find('.item_settings').hasClass('displayNone')){
        $('.item_settings').addClass('displayNone');
        $('.dots_ver_item').removeClass('active');
        $(element).parent().find('.item_settings').removeClass('displayNone');
        $(element).addClass('active');
    }
    else{
        $('.item_settings').addClass('displayNone');
        $('.dots_ver_item').removeClass('active');
        $(element).parent().find('.item_settings').addClass('displayNone');
    }

    event.stopPropagation();
}

function deleteLibraryItem(item_id, item_type){

    $.post('ajax.php?t=libraryBranching&a=deleteItem', {anketa: srv_meta_anketa_id, item_id: item_id, item_type: item_type}, function(){
        $('#question_item_holder_'+item_id).remove();
    });

    event.stopPropagation();
}

// Odpremo popup za dodajanje itema v knjiznico v urejanju ankete
function displayRenameLibraryItemPopup(item_id, type){

    var tab = $('#active_tab').val();

    $("#lib_additional_popup").fadeIn();
    $("#lib_additional_popup").load('ajax.php?t=libraryBranching&a=displayRenameLibraryItemPopup', {anketa: srv_meta_anketa_id, tab: tab, item_id: item_id, type: type});

    event.stopPropagation();
}

// Preimenujemo obstojeci element
function renameLibraryItem(item_id, type){

    var tab = $('#active_tab').val();
    var title = $('#lib_element_name').val();
    var folder_id = $('.folder_item.active').attr("folder-id");

    closeAdditionalPopup();
    
    $("#lib_question_list").load('ajax.php?t=libraryBranching&a=renameItem', {anketa: srv_meta_anketa_id, tab: tab, item_id: item_id, type: type, title: title, folder_id: folder_id});
}

// Funkcija za init drag drop vprasanj v knjiznici
function dropLibraryItem(item_id, item_type, folder_id){

    $.post('ajax.php?t=libraryBranching&a=dropItem', {anketa: srv_meta_anketa_id, item_id: item_id, item_type: item_type, folder_id: folder_id}, function(){
        $('#question_item_holder_'+item_id).remove();
    });
}

// Funkcija za init drag drop
function initDragLibraryItem(){

    $('.question_item_info').draggable({
        containment: $('#general_popup'),
        helper: 'clone',
        //distance: 20,
        //snap: theVoteBar,
        //revert: true,
    });

    $(".droppable_folder").droppable({
        accept: ".question_item_info",
        tolerance: "pointer",
        hoverClass: "drag-hover",
        drop: function( event, ui ) {
            var item_id = ui.draggable.attr('item-id');
            var item_type = ui.draggable.attr('item-type');
            var folder_id = $(this).attr("folder-id");

            dropLibraryItem(item_id, item_type, folder_id);
        }
    });   
}

// Skrijemo puscice folderjem brez subfolderjev
function initHideLibraryArrows(){

    $('ul.folder_list').each(function(){

        if($(this).is(':empty')){

            var id_string = $(this).attr("id");
            var id = id_string.substring(12);
            
            $('#folder_item_'+id).addClass('no_arrow');
        }
    });
}

function insertLibraryItemsIntoSurvey(){

    var items = [];

    var multiple = false;
    var different = false;
    var prev = '';
    var subtype = '';

    $('.question_item_check:checkbox:checked').each(function(){
        var id_string = $(this).attr("id");
        var id = id_string.substring(20);

        var type = $(this).attr("item-type");
        subtype = $(this).attr("item-subtype");

        items.push(id + '_' + type);

        // Preverimo za obvestilo, ce dodajamo vec elementov in ce so razlicni
        if(prev != ''){
            multiple = true;
            
            if(prev != subtype){
                different = true;
            }
        }

        prev = subtype;
    });    

    // Pohendlamo obvestilo, ce dodajamo vec elementov in ce so razlicni
    var note = subtype;
    if(different){
        note = 'm';
    }
    else if(multiple){
        note = subtype + 'm';
    }
    else{
        note = subtype;
    }
    console.log(note)

    $("#branching").load('ajax.php?t=libraryBranching&a=addIntoSurvey', {anketa: srv_meta_anketa_id, items: items}, function(){

        popupClose();        
        actionNotePopup('lib_add_to_survey_'+note, 'success');
    });
}


// Odpremo popup za dodajanje itema v knjiznico v urejanju ankete
function displayAddIntoLibraryPopup(item_id, type){

    $('#fade').fadeTo('slow', 1);
	$('#general_popup').html('').fadeIn('slow');

	$("#general_popup").load('ajax.php?t=libraryBranching&a=displayAddIntoLibraryPopup', {anketa: srv_meta_anketa_id, item_id:item_id, type:type});
}

// Dodamo element v knjiznico
function addIntoLibrary(item_id, type){

    var title = $('#lib_item_title').val();
    var folder_id = $('#lib_item_folder').val();

	$.post('ajax.php?t=libraryBranching&a=addIntoLibrary', {anketa: srv_meta_anketa_id, item_id:item_id, type:type, title:title, folder_id:folder_id}, function(){

        popupClose();
        actionNotePopup('lib_add_to_lib_type'+type, 'success');
    });
}
