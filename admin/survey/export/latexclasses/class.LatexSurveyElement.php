<?php

/**
 *
 *	Class ki skrbi za izris posameznega vprasanja za vprašalnik
 *
 *
 */


include('../../vendor/autoload.php');

if (!defined("MAX_STRING_LENGTH")) define("MAX_STRING_LENGTH", 90);
if (!defined("LINE_BREAK_AT")) define("LINE_BREAK_AT", '7 cm');
if (!defined('RADIO_BTN_SIZE')) define("RADIO_BTN_SIZE", 0.13);
if (!defined('CHCK_BTN_SIZE')) define("CHCK_BTN_SIZE", 0.13);
if (!defined('PIC_SIZE_ANS')) define("PIC_SIZE_ANS", "\includegraphics[width=3cm]"); 	//slika dolocene sirine
if (!defined('DROPDOWN_SIZE')) define("DROPDOWN_SIZE", 0.8);
if (!defined('VAS_SIZE')) define("VAS_SIZE", 0.04); //VAS_SIZE

 
class LatexSurveyElement{
	
	public $anketa;				// ID ankete
	public static $spremenljivka;
	public $spremenljivkaParams;
	public $stevilcenje;
	public $showIf = 0;			// izpis if-ov
	public $numbering = 0; 		// ostevillcevanje vprasanj
	public $texNewLine = '\\\\ ';
	//public $texNewLine = '\newline ';
	public $export_format;
	public $fillablePdf;
	public $questionText;
	protected $usr_id = null;			// id userja ki je odgovarjal (na katerega so vezani podatki)
	protected $db_table = '';
	protected $loop_id = null;	// id trenutnega loopa ce jih imamo
	protected $userAnswer = array();
	protected $texBigSkip = '\bigskip';
	//protected $userDataPresent = array();
	//protected $userDataPresent = 0;
	
	protected $skipEmpty = 0; 	// izpusti vprasanja brez odgovora
	protected $skipEmptySub = 0; 	// izpusti podvprasanja brez odgovora
	protected $path2Images;
	protected $path2UploadedImages;
	protected $language;
	protected $prevod;
	protected $admin_type;
	protected $variableName;
	protected $path2UrlImages;
	protected $export_subtype;
	
	
	function __construct($anketa=null, $export_format='', $fillablePdf=null, $usr_id=null, $export_subtype='', $language=null){
		global $site_path, $global_user_id, $admin_type, $lang;
		
		$this->anketa = $anketa;
		$this->path2Images = $site_path.'admin/survey/export/latexclasses/textemp/images/';
		$this->path2UploadedImages = $site_path.'uploadi/editor/';
		$this->path2UrlImages = $site_path.'uploadi/editor/';

		$this->admin_type = $admin_type;
		
/* 		$this->spremenljivka = $spremenljivka;
		$this->stevilcenje = $stevilcenje; */
		if($export_subtype=='q_empty'||$export_subtype=='q_comment'){
			$this->showIf = (int)SurveySetting::getInstance()->getSurveyMiscSetting('export_show_if');
			$this->numbering = (int)SurveySetting::getInstance()->getSurveyMiscSetting('export_numbering');
		}elseif($export_subtype=='q_data'||$export_subtype=='q_data_all'){
			$this->showIf = (int)SurveySetting::getInstance()->getSurveyMiscSetting('export_data_show_if');
			$this->numbering = (int)SurveySetting::getInstance()->getSurveyMiscSetting('export_data_numbering');
			$this->skipEmpty = (int)SurveySetting::getInstance()->getSurveyMiscSetting('export_data_skip_empty');
			$this->skipEmptySub = (int)SurveySetting::getInstance()->getSurveyMiscSetting('export_data_skip_empty_sub');
		}
		
		$this->export_format = $export_format;
		$this->fillablePdf = $fillablePdf;
		
		//$this->usr_id = $_GET['usr_id'];
		$this->usr_id = $usr_id;
		
		if ($this->usr_id  != '') {
			$sqlL = sisplet_query("SELECT language FROM srv_user WHERE id = '$this->usr_id ' AND ank_id='$this->anketa' ");
			$rowL = mysqli_fetch_array($sqlL);
			$this->language = $rowL['language'];			
		}

		//preverjanje, ali je prevod
		if(isset($_GET['language'])){
			//$this->language = $_GET['language'];
			$this->language = isset($_GET['language'])?$_GET['language']:null;
			$this->prevod = 1;
		}else{
			$this->prevod = 0;
		}		
		//preverjanje, ali je prevod - konec

		//if($language!=-1){ //ce ni default jezik, ampak je prevod
		if($this->prevod){ //ce ni default jezik, ampak je prevod
			$this->language = $language;			
		}

		if ( SurveyInfo::getInstance()->SurveyInit($anketa))
		{
			SurveyUserSetting::getInstance()->Init($anketa, $global_user_id);
			
			$this->db_table = SurveyInfo::getInstance()->getSurveyArchiveDBString();
		}		
		else{
			return false;			
		}
		
	}
	
	
	#funkcija, ki pripravi latex kodo za prikazovanje besedila vprasanja ############################################################################
	public function displayQuestionText($spremenljivke=null, $zaporedna=null, $export_subtype='', $preveriSpremenljivko=null, $loop_id=null, $export_data_type=''){		
		$tex = '';
		$userDataPresent = null; //dodal definicijo spremenljivke zaradi intellisense napake
		self::$spremenljivka = $spremenljivke['id'];
		$row = Cache::srv_spremenljivka($spremenljivke['id']);
		$this->spremenljivkaParams = new enkaParameters($row['params']);

		// Ce je spremenljivka v loopu
		$this->loop_id = $loop_id;
		
		#pridobitev podatkov o odgovorih respondenta na trenutno vprasanje
		if($export_subtype!='q_empty'){
			if( in_array($spremenljivke['tip'], array(1, 2, 3)) ){	//ce je radio,checkbox ali roleta
			//if( in_array($spremenljivke['tip'], array(1, 2, 3)) && $spremenljivke['orientation']!=5){
				$userDataPresent = $this->GetUsersData($this->db_table, $spremenljivke['id'], $spremenljivke['tip'], $this->usr_id, $this->loop_id);
			}elseif( in_array($spremenljivke['tip'], array(6, 16, 19, 20)) ){	//ce je multigrid radio, checkbox, besedilo ali stevilo
				$sqlVrednosti = sisplet_query("SELECT id, naslov, naslov2, variable, other, spr_id FROM srv_vrednost WHERE spr_id='".$spremenljivke['id']."' ORDER BY vrstni_red");			
				//pregled vseh moznih vrednosti (kategorij) po $sqlVrednosti
				while ($rowVrednost = mysqli_fetch_assoc($sqlVrednosti)){
					$indeksZaWhile = 1;
					$sqlVsehVrednsti = sisplet_query("SELECT id, naslov FROM srv_grid WHERE spr_id='".$spremenljivke['id']."' ORDER BY 'vrstni_red'");
					while ($rowVsehVrednosti = mysqli_fetch_assoc($sqlVsehVrednsti)){						
						$sqlUserAnswer = $this->GetUsersDataGrid($spremenljivke, $this->db_table, $rowVrednost, $rowVsehVrednosti, $this->usr_id, 0, $this->loop_id);

						$userAnswer = mysqli_fetch_assoc($sqlUserAnswer);
						if(isset($userAnswer['grd_id']) && $rowVsehVrednosti['id'] == $userAnswer['grd_id']){
							$indeksZaWhile++;
						}
						if($indeksZaWhile!=1){
							$userDataPresent = 1;
						}
					}
				}
			//}elseif(in_array($spremenljivke['tip'], array(21, 7, 8, 18, 17, 26, 27))){	//ce je besedilo ali stevilo ali datum ali vsota
			}elseif(in_array($spremenljivke['tip'], array(21, 4, 7, 8, 18, 17, 26, 27))){	//ce je besedilo ali staro besedilo (4) ali stevilo ali datum ali vsota
				$userDataPresent = $this->GetUsersData($this->db_table, $spremenljivke['id'], $spremenljivke['tip'], $this->usr_id, $this->loop_id);
			}elseif($spremenljivke['tip']==24){	//ce je kombinirana tabela
				//GetUsersDataKombinirana($spremenljivke, $db_table, $usr_id)
				$questionText=1;
				$indeksPolja = 0;
				$userDataPresentArray = $this->GetUsersDataKombinirana($spremenljivke, $this->db_table, $this->usr_id, $questionText, $this->loop_id);
				if (is_array($userDataPresentArray)){
					$userDataPresent=0;
					foreach($userDataPresentArray as $key=>$value){
						if($key==$indeksPolja){
							if($value!=''){
								$userDataPresent=1;
							}
							$indeksPolja++;
						}
					}
				}else{
					if($userDataPresent!=0){
						$userDataPresent=1;
					}else{
						$userDataPresent=0;
					}
				}
			}
		}
		#pridobitev podatkov o odgovorih respondenta na trenutno vprasanje - konec ####################################		
				
		if(($export_subtype=='q_empty')||($export_subtype=='q_comment')||(($export_subtype=='q_data'||$export_subtype=='q_data_all')&&($userDataPresent!=0||$preveriSpremenljivko))){	//ce je prazen vprasalnik ali (je vprasalnik poln in (so podatki prisotni ali je potrebno pokazati vprasanje tudi, ce ni podatkov))
			$rowl = $this->srv_language_spremenljivka($spremenljivke);
			if ($rowl!=null && strip_tags($rowl['naslov']) != '') $spremenljivke['naslov'] = $rowl['naslov'];
			if ($rowl!=null && strip_tags($rowl['info']) != '') $spremenljivke['info'] = $rowl['info'];
			
			#Pridobimo tekst vprasanja#################################################################################	
			
			$sqlVrstic = sisplet_query("SELECT count(*) FROM srv_vrednost WHERE spr_id='".$spremenljivke['id']."'");
			$rowVrstic = mysqli_fetch_row($sqlVrstic);
			$visina = round(($rowVrstic[0]+2) * 8);
			
			//$linecount_vprasanja = $this->pdf->getNumLines($spremenljivke['naslov'], $this->pdf->getPageWidth());
			
			//$tex = $spremenljivke['naslov'];		
			######################################### Pridobimo tekst vprasanja - konec
			
			#Stevilcenje vprasanj###############################################################
			$numberingText = ($this->numbering == 1) ? ($spremenljivke['variable']).' - ' : '';			
			######################################### Stevilcenje vprasanj - konec		
			
			//belezenje imena spremenljivke, zaradi GDPR vprasanja
			$this->variableName = $spremenljivke['variable'];
			//belezenje imena spremenljivke, zaradi GDPR vprasanja - konec

			#Izris stevilke in besedila vprasanja ter IF ali BLOK, ce so prisotni ###############################################
			$text = strip_tags($numberingText . $spremenljivke['naslov'], '<a><img><ul><li><ol><br><p>');	//je potrebno spustiti <p>, zaradi GDPR vprasanja			
			
			//$tex = $text." ".$texNewLine;
			if( !in_array($spremenljivke['tip'], array(0, 1, 2, 3, 4, 7, 8, 6, 16, 19, 20, 21, 17, 18, 24, 26, 27)) ){	//ce ni radio, check, roleta, stevilo, datum, multigrid radio, checkbox, besedilo, stevilo, razvrscanje, vsota ali kombinirana tabela, lokacija, ali heatmap
				$tex .= ($this->export_format == 'pdf' ? '\\begin{absolutelynopagebreak} \\noindent ' : ' ');	//ce je pdf uredimo, da med vprasanji ne bo prelomov strani
			}
				
				#Izpis if-ov pri vprasanju#########################################################		
				if($this->showIf == 1){
					
					// TODO: Stara koda za iskanje po branchingu (briši, če je vse ok)
					//$b = new Branching($this->anketa);
					//$parents = $b->get_parents($spremenljivke['id']);
                    //$parents = explode('p_', $parents);
                    //foreach ($parents AS $key => $val) {
                    //    if ( is_numeric(trim($val)) ) {
                    //        $parents[$key] = (int)$val;
                    //    } else {
                    //        unset($parents[$key]);
                    //    }
					//}
					
					/* $b = new Branching($this->anketa);
					$parents = $b->get_parents($spremenljivke['id']);
                    $parents = explode('p_', $parents);
                    foreach ($parents AS $key => $val) {
                        if ( is_numeric(trim($val)) ) {
                            $parents[$key] = (int)$val;
                        } else {
                            unset($parents[$key]);
                        }
                    } 

					foreach ($parents AS $if) {				
						$tex .= $this->displayIf($if);
						$tex .= $this->texNewLine;
					}*/
					###### stara koda, ki je delavala


                    // Po novem izpisemo pred vsakim vprasanjem vse ife znotraj katerih se nahaja
                    Cache::cache_all_srv_branching($this->anketa);
					$parents = Cache::srv_branching($spremenljivke['id'], 0)['parent'];
					if($parents){
						$tex .= ' '.LatexDocument::encodeText($this->displayIf($parents));	
						$tex .= $this->texNewLine;
					}
					#preuredil kodo, da zadeva deluje tako kot ta stara, ki se nahaja nad tem

				}				
				######################################### Izpis if-ov pri vprasanju - konec

			//$tex .= '\textbf{'.$text.'} '.$texNewLine;	//izris besedila vprasanja
			
			if($export_subtype=='q_data'||$export_subtype=='q_data_all'){	//ce je izpis odgovorov
				$text = $this->dataPiping($text);	//pokazi odgovore po zanki				
			}			

			$tex .= ' \noindent '; //dodal pred vsakim tekstom vprasanja, da ni indent-a

			if($spremenljivke['orientation']==0){ //ce je vodoravno ob vprasanju
				if($spremenljivke['tip'] == 21 || $spremenljivke['tip'] == 4){ //ce je besedilo (vodoravno ob vprasanju)
					$tex .= ' \par { ';	//dodaj zacetek odstavka, ki je pomemben za pravile izpis
				}
			}

			$tex .= '\textbf{'.LatexDocument::encodeText($text, null, null, $loop_id).'} ';	//izris besedila vprasanja	//encodeText($text='', $vre_id=0, $naslovStolpca = 0, $img_id=0){
			
			$this->questionText = $text;	//zabelezimo tekst vprasanja, ki ga potrebujemo kasneje
			
			#Izris stevilke in besedila vprasanja ter IF ali BLOK, ce so prisotni - konec ###############################################
			
			#Izris opombe ###############################################################################
			if($spremenljivke['orientation']!=0){	//ce ni vodoravno ob vprasanju,

				//ce imamo opombo, jo izpisi
				if($spremenljivke['info'] != ''){
					$tex .= $this->texNewLine;	
					$tex .= '\vspace{2 mm}';					
					$tex .= ' \noindent \\footnotesize '.LatexDocument::encodeText($spremenljivke['info']).'  \\normalsize ';
				}

				if( !in_array($spremenljivke['tip'], array(4, 6, 16, 19, 20, 21, 7, 8, 18)) ){	//ce ni multigrid radio, checkbox, besedilo, stevilo, datum ali vsota ki ne potrebujejo prazne vrstice zaradi uporabe tabele
					$tex .= $this->texNewLine;
				}

				
				if($export_subtype=='q_data'||$export_subtype=='q_data_all'){	//ce je izpis odgovorov					
					if($export_data_type==0||$export_data_type==2){	//ce je Navaden ali Kratek izvoz					
						if( in_array($spremenljivke['tip'], array(4, 6, 16, 19, 20, 21)) ){	//ce je multigrid radio, checkbox, besedilo ali stevilo in je Navaden ali Kratek izpis
							$tex .= $this->texNewLine;						
						}
					}else{
						if( in_array($spremenljivke['tip'], array(4, 21)) ){	//ce je besedilo
							$tex .= $this->texNewLine;						
						}
					}
				}
			}else{	//ce je vodoravno ob vprasanju				
				//ce imamo opombo, jo izpisi
				if($spremenljivke['info'] != ''){
					//pejdi v novo vrstico
					$tex .= $this->texNewLine;
					$tex .= '\vspace{2 mm}';
					$tex .= ' {\noindent \\footnotesize '.LatexDocument::encodeText($spremenljivke['info']).' \\normalsize } ';
				}

				if($export_subtype=='q_data'||$export_subtype=='q_data_all'){	//ce je izpis odgovorov
					if(!in_array($spremenljivke['tip'], array(8))){	//ce ni datum
						$tex .= $this->texNewLine;	//dodaj prazno vrstico
					}					
				}
			}
			#Izris opombe - konec #########################################################################		
			
			#ce vprasanje nima moznih odgovorov, je potrebno zakljuciti environment (absolutelynopagebreak) pri pdf
			if($rowVrstic[0]==0 && (in_array($spremenljivke['tip'], array(1, 2, 3, 6, 16, 17, 20, 9, 19, 17))) ){
				if($this->export_format == 'pdf'){	//ce je pdf
					if($spremenljivke['orientation']==0 || $spremenljivke['orientation']==2){	//ce sta vodoravni orientaciji						
						$tex .= $this->texNewLine;	//dodaj na koncu vprasanja prazno vrstico
					}
					$tex .= $this->texBigSkip;
					$tex .= $this->texBigSkip;
					$tex .= '\\end{absolutelynopagebreak}';	//zakljucimo environment, da med vprasanji ne bo prelomov strani
				}else{	//ce je rtf
					if($spremenljivke['orientation']==0 || $spremenljivke['orientation']==2){	//ce sta vodoravni orientaciji						
						$tex .= $this->texNewLine;	//dodaj na koncu vprasanja prazno vrstico
					}
					$tex .= $this->texBigSkip;
					$tex .= $this->texBigSkip;
				}
			}
			#ce vprasanje nima moznih odgovorov, je potrebno zakljuciti environment (absolutelynopagebreak) pri pdf - konec			
		}
		return $tex;
	}
	#funkcija, ki pripravi latex kodo za prikazovanje besedila vprasanja - konec ############################################################################
	
	
	#funkcija, ki pripravi latex kodo za prikazovanje moznih odgovorov glede na tip vprasanja################################################################
	public function displayAnswers($spremenljivke=null, $export_subtype='', $preveriSpremenljivko=null, $export_data_type='', $loop_id=null){		

		switch ( $spremenljivke['tip'] )
		{
			case 1: //radio
			case 2: //check
			case 3: //select -> radio
				return RadioCheckboxSelectLatex::getInstance()->export($spremenljivke, $this->export_format, $this->questionText, $this->fillablePdf, $this->texNewLine, $this->getUserId(), $this->db_table, $preveriSpremenljivko, $export_data_type, $export_subtype, $loop_id, $this->language);
			break;
			case 6: //multigrid
			case 16:// multicheckbox
			case 19:// multitext
			case 20:// multinumber		
				return MultiGridLatex::getInstance()->export($spremenljivke, $this->export_format, $this->questionText, $this->fillablePdf, $this->texNewLine, $this->getUserId(), $this->db_table, $export_subtype, $preveriSpremenljivko, $this->skipEmptySub, $export_data_type, $this->skipEmpty, $loop_id, $this->language);
			break;
			case 21: //besedilo
				return BesediloLatex::getInstance()->export($spremenljivke, $this->export_format, $this->questionText, $this->fillablePdf, $this->texNewLine, $this->getUserId(), $this->db_table, $this->anketa, $export_subtype, $preveriSpremenljivko, $export_data_type, $loop_id);
			break;
			case 4: //besedilo staro
				return BesediloLatex::getInstance()->export($spremenljivke, $this->export_format, $this->questionText, $this->fillablePdf, $this->texNewLine, $this->getUserId(), $this->db_table, $this->anketa, $export_subtype, $preveriSpremenljivko, $export_data_type, $loop_id);
			break;
			case 7: //stevilo
				return SteviloLatex::getInstance()->export($spremenljivke, $this->export_format, $this->questionText, $this->fillablePdf, $this->texNewLine, $this->getUserId(), $this->db_table, $export_subtype, $preveriSpremenljivko, $export_data_type, $loop_id);
			break;
			case 8:	//datum
				return DatumLatex::getInstance()->export($spremenljivke, $this->export_format, $this->questionText, $this->fillablePdf, $this->texNewLine, $this->getUserId(), $this->db_table, $export_subtype, $preveriSpremenljivko, $loop_id);
			break;
			case 17: //ranking
				return RazvrscanjeLatex::getInstance()->export($spremenljivke, $this->export_format, $this->questionText, $this->fillablePdf, $this->texNewLine, $this->getUserId(), $this->db_table, $export_subtype, $preveriSpremenljivko, $export_data_type, $loop_id);
			break;
			case 18: //vsota
				return VsotaLatex::getInstance()->export($spremenljivke, $this->export_format, $this->questionText, $this->fillablePdf, $this->texNewLine, $this->getUserId(), $this->db_table, $export_subtype, $preveriSpremenljivko, $loop_id);
			break;
			case 24: // kombinirana tabela
				return GridMultipleLatex::getInstance()->export($spremenljivke, $this->export_format, $this->questionText, $this->fillablePdf, $this->texNewLine, $this->getUserId(), $this->db_table, $export_subtype, $preveriSpremenljivko, $export_data_type, $loop_id, $this->language);
			break;			
			case 26: //lokacija
				return LokacijaLatex::getInstance()->export($spremenljivke, $this->export_format, $this->questionText, $this->fillablePdf, $this->texNewLine, $this->getUserId(), $this->db_table, $export_subtype, $preveriSpremenljivko, $loop_id);
			break;
			case 27: //heatmap
				return HeatmapLatex::getInstance()->export($spremenljivke, $this->export_format, $this->questionText, $this->fillablePdf, $this->texNewLine, $this->getUserId(), $this->db_table, $export_subtype, $preveriSpremenljivko, $loop_id);
			break;
			case 5:	//nagovor
				return NagovorLatex::getInstance()->export($spremenljivke, $this->export_format, $this->questionText, $this->fillablePdf, $this->texNewLine, $export_subtype, $preveriSpremenljivko, $loop_id);
			break;
			case 22: //kalkulacija
				return KalkulacijaLatex::getInstance()->export($spremenljivke, $this->export_format, $this->fillablePdf, $this->texNewLine, $export_subtype, $this->db_table, $this->getUserId(), $loop_id);
			break;
			case 25: //kvota
				return KvotaLatex::getInstance()->export($spremenljivke, $this->export_format, $this->fillablePdf, $this->texNewLine, $export_subtype, $this->db_table, $this->anketa, $this->getUserId(), $loop_id);
			break;
			case 9: //SN-imena
				return SNImenaLatex::getInstance()->export($spremenljivke, $this->export_format, $this->fillablePdf, $this->texNewLine, $export_subtype, $this->db_table, $this->anketa, $this->getUserId(), $loop_id);
			break;
		}
	}
	#funkcija, ki pripravi latex kodo za prikazovanje moznih odgovorov glede na tip vprasanja - konec #######################################################

	/**
	 * prevod za srv_spremenljivka
	 */
	 function srv_language_spremenljivka ($spremenljivka=null) {
		 
		// if ($this->language != -1) {
		if ($this->prevod) {
			$sqll = sisplet_query("SELECT naslov, info FROM srv_language_spremenljivka WHERE ank_id='".$this->anketa."' AND spr_id='".$spremenljivka['id']."' AND lang_id='".$this->language."'");
			$rowl = mysqli_fetch_array($sqll);
			
			return $rowl;
		 }
		
		return false;
	 }
	 
	function displayIf($if=null){
		global $lang;
    	$sql_if = sisplet_query("SELECT tip FROM srv_if WHERE id = '$if'");
    	$row_if = mysqli_fetch_array($sql_if);

        // Blok
		if($row_if['tip'] == 1)
			$output = strtoupper($lang['srv_block']).' ';
		// Loop
		elseif($row_if['tip'] == 2)
			$output = strtoupper($lang['srv_loop']).' ';
		// IF
		else
			$output = 'IF ';
      
		$sql_if = sisplet_query("SELECT number, label FROM srv_if WHERE id = '$if'");
		$row_if = mysqli_fetch_array($sql_if);
		$output .= '('.$row_if['number'].') ';

        $sql = Cache::srv_condition($if);
        
        $bracket = 0;
        $i = 0;
        while ($row = mysqli_fetch_array($sql)) {

            if ($i++ != 0)
                if ($row['conjunction'] == 0)
                    $output .= ' and ';
                else
                    $output .= ' or ';

            if ($row['negation'] == 1)
                $output .= ' NOT ';

            for ($i=1; $i<=$row['left_bracket']; $i++)
				$output .=  ' ( ';

            // obicajne spremenljivke
            if ($row['spr_id'] > 0) {

				$row2 = Cache::srv_spremenljivka($row['spr_id']);
				
                // obicne spremenljivke
                if ($row['vre_id'] == 0) {
                    $row1 = Cache::srv_spremenljivka($row['spr_id']);
                // multigrid
                } elseif ($row['vre_id'] > 0) {
                    $sql1 = sisplet_query("SELECT variable FROM srv_vrednost WHERE id = '$row[vre_id]'");
                    $row1 = mysqli_fetch_array($sql1);
                } else
                    $row1 = null;

                $output .= LatexDocument::encodeText($row1['variable']);
                // radio, checkbox, dropdown in multigrid
                if (($row2['tip'] <= 3 || $row2['tip'] == 6) && ($row['spr_id'] || $row['vre_id'])) {

                    if ($row['operator'] == 0)
                        $output .= ' = ';
                    else
                        $output .= ' != ';

                    $output .= '[';

                    // obicne spremenljivke
                    if ($row['vre_id'] == 0) {
                        $sql2 = sisplet_query("SELECT v.variable as variable FROM srv_condition_vre c, srv_vrednost v WHERE cond_id='$row[id]' AND c.vre_id=v.id");

                        $j = 0;
                        while ($row2 = mysqli_fetch_array($sql2)) {
                            if ($j++ != 0) $output .= ', ';
                            $output .= $row2['variable'];
                        }
                    // multigrid
                    } elseif ($row['vre_id'] > 0) {
                        $sql2 = sisplet_query("SELECT g.variable as variable FROM srv_condition_grid c, srv_grid g WHERE c.cond_id='$row[id]' AND c.grd_id=g.id AND g.spr_id='$row[spr_id]'");

                        $j = 0;
                        while ($row2 = mysqli_fetch_array($sql2)) {
                            if ($j++ != 0) $output .= ', ';
                            $output .= $row2['variable'];
                        }
                    }

                    $output .= ']';

                // textbox in nubmer mata drugacne pogoje in opcije
                } elseif ($row2['tip'] == 4 || $row2['tip'] == 21 || $row2['tip'] == 7 || $row2['tip'] == 22) {

                    if ($row['operator'] == 0)
                        $output .= ' = ';
                    elseif ($row['operator'] == 1)
                        $output .= ' <> ';
                    elseif ($row['operator'] == 2)
                        $output .= ' < ';
                    elseif ($row['operator'] == 3)
                        $output .= ' <= ';
                    elseif ($row['operator'] == 4)
                        $output .= ' > ';
                    elseif ($row['operator'] == 5)
                        $output .= ' >= ';

                    $output .= '\''.$row['text'].'\'';

                }

            // recnum
            } elseif ($row['spr_id'] == -1) {

                $output .= 'mod(recnum, '.$row['modul'].') = '.$row['ostanek'];

			} 

            for ($i=1; $i<=$row['right_bracket']; $i++)
				$output .= ' ) ';
        }
        
        if ($row_if['label'] != '') {
	        $output .= ' (';
	        $output .= ' '.$row_if['label'].' ';
	        $output .= ') ';      		
		}
			
		return $output;
	}
	
	#funkcija, ki skrbi za izbiro radio, checkbox ali ostale simbole, ki so potrebni za izris odgovorov #############################################################
	function getAnswerSymbol($export_subtype=null, $export_format='', $fillablePdf=null, $spremenljivkeTip=null, $spremenljivkeGrids=null, $numOfMissings=null, $data=null, $enota='', $indeksVASIcon=0, $VASNumberRadio='', $spremenljivkeId=null){
		//return;
		$tip = $spremenljivkeTip;
		global $site_path;
		$this->path2Images = $site_path.'admin/survey/export/latexclasses/textemp/images/';
		$this->export_subtype = $export_subtype;
		$numGrids=$spremenljivkeGrids;
		if($tip==21||$tip==4||$tip==8){	//ce je besedilo ali datum,
			$tip=2; //naj se pobere checkbox
		}
		if( ($export_format=='pdf'&&$fillablePdf==0)||$export_format=='rtf'){//ce je navaden pdf ali rtf dokument (brez moznosti izbire ali vnosa v polja)
			if($data){
				$data = LatexDocument::encodeText($data);
			}

			if($tip==1||$tip==6){	//radio ali multigrid z radio
				if($data){	//ce je odgovor respondenta
					if($enota!=11&&$enota!=12){	//ce ni VAS ali slikovni tip
						$radioButtonTex = ($export_format=='pdf'?"{\\radio}" : "\\includegraphics[scale=".RADIO_BTN_SIZE."]{".$this->path2Images."radio2}");	//\radio je newcommand
					}elseif($enota==11){ //drugace, ce je VAS
						if($tip==1){
							$VASNumber = $VASNumberRadio;
						}else{
							$spremenljivkeGrids = $spremenljivkeGrids - 1;
							$VASNumber = $spremenljivkeGrids;
						}
						$indeksVASIcon = $indeksVASIcon - 1;
						$radioButtonTex = [];
						if($VASNumber>1){						
							switch ($VASNumber) {
								case 1:
									$radioButtonTex = "";
									break;
								case 2:
									$arrayVAS = ['vas3checked', 'vas5checked'];
 									foreach($arrayVAS AS $VAS){										
										//$radioButtonTex[] = "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."".$VAS."}";
										$radioButtonTex[] = "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."".$VAS."}";
									}
									break;
								case 3:
									$arrayVAS = ['vas3checked', 'vas4checked', 'vas5checked'];
									foreach($arrayVAS AS $VAS){									   
									   $radioButtonTex[] = "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."".$VAS."}";
								   }
								   break;
								case 4:
									$arrayVAS = ['vas2checked', 'vas3checked', 'vas5checked', 'vas6checked'];
									foreach($arrayVAS AS $VAS){									   
									   $radioButtonTex[] = "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."".$VAS."}";
								   }
								   break;
								case 5:
									$arrayVAS = [ 'vas2checked', 'vas3checked', 'vas4checked', 'vas5checked', 'vas6checked'];
 									/* foreach($arrayVAS AS $VAS){										
										$radioButtonTex[] = "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."".$VAS."}";
									} */
									break;
								case 6:
									$arrayVAS = ['vas1checked', 'vas2checked', 'vas3checked', 'vas5checked', 'vas6checked', 'vas7checked'];
 									foreach($arrayVAS AS $VAS){										
										$radioButtonTex[] = "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."".$VAS."}";
									}
									break;
								case 7:
									$arrayVAS = ['vas1checked', 'vas2checked', 'vas3checked', 'vas4checked', 'vas5checked', 'vas6checked', 'vas7checked'];
 									foreach($arrayVAS AS $VAS){										
										$radioButtonTex[] = "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."".$VAS."}";
									}
									break;
							}
							return $radioButtonTex[$indeksVASIcon];	//$indeksVASIcon
						}
					}elseif($enota==12){ //drugace, ce je slikovni tip
						$prviOdgovorSlikovniTip = 1;
						$radioButtonTex = ICON_SIZE."{".$this->path2Images."".$this->getCustomRadioSymbol($spremenljivkeId, $prviOdgovorSlikovniTip)."}";
					}
				}else{	//ce ni odgovorov respondenta oz. je prazen vprasalnik
					if($enota!=11&&$enota!=12){	//ce ni VAS ali slikovni tip
						$radioButtonTex = ($export_format=='pdf'?"{\Large $\ocircle$}" : "\\includegraphics[scale=".RADIO_BTN_SIZE."]{".$this->path2Images."radio}");
					}elseif($enota==11){ //drugace, ce je VAS
						if($tip == 1){ //ce je radio							
							$VASNumber = $VASNumberRadio;
						}else{	//drugace (ce je tabela z radio)
							$VASNumber = $spremenljivkeGrids;
						}
						if($VASNumber > 1){
							switch ($VASNumber) {
								case 1:
									$radioButtonTex = "";
									break;
								case 2:
									$radioButtonTex = array("\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas3}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas5}");
									break;
								case 3:
								   $radioButtonTex = array("\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas3}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas4}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas5}");
								   break;
								case 4:									
								   $radioButtonTex = array("\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas1}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas3}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas5}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas7}");
								   break;
								case 5:
									$radioButtonTex = array("\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas1}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas3}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas4}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas5}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas7}");											
									break;
								case 6:
									$radioButtonTex = array("\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas1}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas2}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas3}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas5}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas6}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas7}");
									break;
								case 7:
									$radioButtonTex = array("\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas1}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas2}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas3}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas4}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas5}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas6}", "\\includegraphics[scale=".VAS_SIZE."]{".$this->path2Images."vas7}");
									break;
							}
							//echo "polje z VAS simboli </br>";
							return $radioButtonTex;
						}							
							
					}elseif($enota==12){ //ce je slikovni tip
						$prviOdgovorSlikovniTip = 0;
						$radioButtonTex = ICON_SIZE."{".$this->path2Images."".$this->getCustomRadioSymbol($spremenljivkeId, $prviOdgovorSlikovniTip)."}";				
					}
				}
				return $radioButtonTex;
			}else if($tip==2||$tip==16){	//checkbox ali multigrid s checkbox
				if($data){ //ce je odgovor respondenta
					$checkboxTex = ($export_format=='pdf'?'{\Large \CheckedBox}' : "\\includegraphics[scale=".CHCK_BTN_SIZE."]{".$this->path2Images."checkbox2}");
				}else{ //ce ni odgovorov respondenta oz. je prazen vprasalnik
					$checkboxTex = ($export_format=='pdf'?'{\Large \Square}' : "\\includegraphics[scale=".CHCK_BTN_SIZE."]{".$this->path2Images."checkbox}");
				}
				return $checkboxTex;
			}elseif($tip==19||$tip==20){	//multi text ali multi number
				$textboxWidth = 0.4/($numGrids+$numOfMissings);	//sirina praznega textbox-a				
				//priprava latex kode za prazen text box dolocene sirine in visine glede na export format
				if($export_format == 'pdf'){					
					if($data != null){ //ce je odgovor respondenta	
						$TextBoxWithText = ' \\textcolor{crta}{\footnotesize{'.$data.'}} ';
						$emptyTextBox = $TextBoxWithText;						
					}else{ //ce ni odgovorov respondenta oz. je prazen vprasalnik					
						if($this->export_subtype == 'q_empty' || $this->export_subtype == 'q_comments'){
							$emptyTextBox = ' \fbox{\parbox{'.$textboxWidth.'\textwidth}{ \hphantom{\hspace{'.$textboxWidth.'\textwidth}} }} ';
						}else{
							$emptyTextBox = ' ';
						}
					}
				}elseif($export_format == 'rtf'){
					if($data!=0){	//ce je odgovor respondenta	 				
						$TextBoxWithText = ' '.$data.' ';
						$emptyTextBox = $TextBoxWithText;
					}else{ //ce ni odgovorov respondenta oz. je prazen vprasalnik
						if($this->export_subtype == 'q_empty' || $this->export_subtype == 'q_comments'){
							$emptyTextBox =' \rule{'.$textboxWidth.'\textwidth}{.1pt} ';
						}else{
							$emptyTextBox = ' ';
						}						
					}
				}
				return $emptyTextBox;
			}
		}else if($export_format=='pdf'&&$fillablePdf==1){//ce je pdf dokument, kjer je mozno vpisati v polja
			$radioButtonTex ="{\Large $\ocircle$}";
			$checkboxTex ='{\Large \Square}';	
		}		
	}
	#funkcija, ki skrbi za izbiro radio, checkbox ali ostale simbole, ki so potrebni za izris odgovorov - konec #####################################################
	
	#funkcija, ki ureja pretvorbo stevilskega ID vprasanja v "crkovsko" identifikacijo, ker Latex ne podpira imen s stevilkami ######################################
	function UrediOznakoVprasanja($sprId=null){
		$sprId = (string) $sprId;
		$sprIdArray = str_split($sprId);
		$temp='';		
		foreach($sprIdArray as $data){
			$temp .= chr($data+65);
		}		
		return $temp;		
	}
	#funkcija, ki ureja pretvorbo stevilskega ID vprasanja v "crkovsko" identifikacijo, ker Latex ne podpira imen s stevilkami - konec #############################
	
	#funkcija, ki skrbi za pridobitev simbola za slikovni tip ######################################
	function getCustomRadioSymbol($sprId=null, $odgovor=null){
		$customRadioSymbol = '';	
		$findme = 'customRadio=';
		$finishAt = 'customRadioNumber';
		
		$row = Cache::srv_spremenljivka($sprId);
		$this->spremenljivkaParams = new enkaParameters($row['params']);
		$customRadioSymbol = $this->spremenljivkaParams->get('customRadio');
		
		if($odgovor){	//ce je odgovor
			$customRadioSymbol = $customRadioSymbol."Inverted";	//preuredi, da bo razvidna grafika, ko je prisoten odgovor respondenta
		}
		return $customRadioSymbol;
	}
	#funkcija, ki skrbi za pridobitev simbola za slikovni tip - konec ###############################

	 
	 /**
	 * vrne prevod za srv_vrednost
	 * 
	 * @param mixed $vrednost
	 */
	 function srv_language_vrednost ($vre_id=null) {		
		 //if ($this->language != -1) {	

		if ($this->prevod) {
			$sqllString = "SELECT naslov, naslov2 FROM srv_language_vrednost WHERE vre_id='".$vre_id."' AND lang_id='".$this->language."'";
			$sqll = sisplet_query($sqllString);
			$rowl = mysqli_fetch_array($sqll);
			return $rowl;
		 }		 
		 return false;	 
	 }
	 
	 /**
	 * vrne prevod za srv_grid
	 * 
	 * @param mixed $vrednost
	 */
	function srv_language_grid ($grd_id=null, $spr_id=null) {
		 //if ($this->language != -1) {
		if ($this->prevod) {
			$sqllString = "SELECT naslov, podnaslov FROM srv_language_grid WHERE spr_id = '".$spr_id."' AND  grd_id='".$grd_id."' AND lang_id='".$this->language."'";
			$sqll = sisplet_query($sqllString);
			$rowl = mysqli_fetch_array($sqll);			
			return $rowl;
		 }
		
		return false;
	 }
	 function srv_language_grid_old ($spremenljivka, $grid) {
	 	 
		 if ($this->language != -1) {
			$sqll = sisplet_query("SELECT * FROM srv_language_grid WHERE ank_id='".$this->anketa['id']."' AND spr_id='".$spremenljivka."' AND grd_id='".$grid."' AND lang_id='".$this->language."'");
			$rowl = mysqli_fetch_array($sqll);
			
			if ($rowl['naslov'] != '') return $rowl['naslov'];	
		 }
		 
		 return false;		 
	 }
	 
	#funkcija, ki skrbi za filanje obstojecega polja z odgovori z missing odgovori #############################################################
	function AddMissingsToAnswers($vodoravniOdgovori=[], $missingOdgovori=[]){		
		for($m=0;$m<count($missingOdgovori);$m++){
			array_push($vodoravniOdgovori,$missingOdgovori[$m]);
		}
		return $vodoravniOdgovori;
	}	
	#funkcija, ki skrbi za filanje obstojecega polja z odgovori z missing odgovori - konec #####################################################
	
	#funkcija, ki skrbi za izpis latex kode za zacetek tabele ##################################################################################
	#argumenti 1. export_format, 2. parametri tabele, 3. tip tabele za pdf, 4. tip tabele za rtf, 5. sirina pdf tabele (delez sirine strani), 6. sirina rtf tabele (delez sirine strani)
	function StartLatexTable($export_format='', $parameterTabular='', $pdfTable=null, $rtfTable=null, $pdfTableWidth=null, $rtfTableWidth=null){
		$tex = "";
		$tex .= "\keepXColumns";		
 		if($export_format == 'pdf'){
			//$tex .= "\r\n";
			$tex .= "\begin{".$pdfTable."}";
			if($pdfTable=='xltabular'){
				$tex .= '{'.$pdfTableWidth.'\textwidth}';
			}
			$tex .= '{ '.$parameterTabular.' }';
		}elseif($export_format == 'rtf'){
			$tex .= '\begin{'.$rtfTable.'}';
			if($rtfTable=='tabular*'){
				$tex .= '{'.$pdfTableWidth.'\textwidth}';
			}
			//$tex .= '{ '.$parameterTabular.' }';
			$tex .= '{@{}  '.$parameterTabular.' }';	//dodal @{} , da ni indent-a
		}
		
		return $tex;
	}	
	#funkcija, ki skrbi za izpis latex kode za zacetek tabele - konec ##########################################################################
	
	#funkcija, ki skrbi za izpis latex kode za zakljucek tabele ##################################################################################
	#argumenti 1. export_format, 2. tip tabele za pdf, 3. tip tabele za rtf
	function EndLatexTable($export_format='', $pdfTable=null, $rtfTable=null){
		$tex = '';
		$tex .= ($export_format == 'pdf' ? '\end{'.$pdfTable.'}' : '\end{'.$rtfTable.'}');
		return $tex;
	}	
	#funkcija, ki skrbi za izpis latex kode za zakljucek tabele - konec ##########################################################################
	
	#funkcija, ki skrbi za pripravo latex kode za text box (okvirja) (prazen ali z besedilom) dolocene sirine in visine glede na export format  ####################
	#argumenti 1. export_format, 2. visina okvirja, 3. sirina okvirja, 4. besedilo v okvirju, 5. poravnava, 6. obrobe
	function LatexTextBox($export_format='', $textboxHeight=null, $textboxWidth=null, $text='', $textboxAllignment=null, $noBorders=null){
		$tex = '';		
		
		//zacetek okvirja		
		if($export_format == 'pdf'&&$textboxHeight!=0&&$noBorders==0){		
			$tex .= ' \fbox{\parbox['.$textboxAllignment.']['.$textboxHeight.']{'.$textboxWidth.'\textwidth}';
		}elseif((($export_format == 'pdf'&&$textboxHeight==0))&&$noBorders==0){		
			$tex .= ' \fbox{\parbox{'.$textboxWidth.'\textwidth}';
		}elseif( ($export_format == 'rtf'||$export_format == 'pdf')&&$noBorders ){
			$tex .= ' {\parbox{'.$textboxWidth.'\textwidth}';			
		}
		

		if(is_array($text)){
			$text = implode(" ", $text);
		}
		
		if($text==''){	//ce ni teksta, je okvir prazen
			if($export_format == 'pdf'){
				$tex .= '{ \hphantom{\hspace{'.$textboxWidth.'\textwidth}} }}';
			}elseif($export_format == 'rtf'){				
				$tex .= ' \rule{'.$textboxWidth.'\textwidth}{.1pt} ';	//izpisi neprekinjeno crto, ki je premaknjena za hspace in dolga $textboxWidth in debela 0.1pt
			}		
		}else{	//drugace, izpisi besedilo
			if($export_format == 'pdf'){
				$tex .= '{ '.$text.' }}';	//izpis besedila v okvirju				
			}elseif($export_format == 'rtf'){
				$tex .= '{ '.$text.' }';	//izpis besedila v okvirju
			}			
		}
		
		return $tex;
	}
	#funkcija, ki skrbi za pripravo latex kode za text box (okvirja) (prazen ali z besedilom) dolocene sirine in visine glede na export format - konec ############
	
	#funkcija, ki skrbi za pripravo latex U oblike dolocene sirine in visine glede na export format  ####################
	#argumenti 1. export_format, 2. visina okvirja, 3. sirina okvirja
	function LatexUShape($export_format='', $textboxHeight=null, $textboxWidth=null){
		$tex = '';
		if($export_format == 'pdf'&&$textboxHeight!=0){	
			$tex .= '\keepXColumns\begin{xltabular}{0.25\textwidth}{C} ';	//zacetek tabele, ki bo zgledala kot okvir	
			$tex .= ' \begin{tikzpicture} ';			
			//$tex .= ' \draw (0,0) -- (4,0) -- (4,4)  (0,4) -- (0,0);';
			$tex .= ' \draw (0,0) -- ('.$textboxWidth.',0) -- ('.$textboxWidth.','.$textboxHeight.')  (0,'.$textboxHeight.') -- (0,0);';			
			$tex .= ' \end{tikzpicture} ';
			
		}elseif($export_format == 'rtf'||($export_format == 'pdf'&&$textboxHeight==0)){
			$tex .= ' \fbox{\parbox{'.$textboxWidth.'\textwidth}';
		}
		return $tex;
	}
	#funkcija, ki skrbi za pripravo latex U oblike dolocene sirine in visine glede na export format - konec ############
	
	#funkcija, ki skrbi za pripravo latex U oblike s tekstom dolocene sirine in visine glede na export format  ####################
	#argumenti 1. export_format, 2. visina okvirja, 3. sirina okvirja, 4. tekst oz. koda za odgovore v okvirjih
	function LatexTextInUShape($export_format='', $textboxHeight=null, $textboxWidth=null, $text=''){
		//\begin{tikzpicture} \draw (-2,0) -- (2,0) -- (2,1.5 cm) (-2,1.5 cm) -- (-2,0)  node[above right] {\begin{tabular}{c}  \fbox{\parbox{0.2\textwidth}{  \centering  Vpišite besedilo odgovora 11 }}  \\ \fbox{\parbox{0.2\textwidth}{  \centering  Vpišite besedilo odgovora 12 }} \end{tabular} }; \end{tikzpicture}
		$tex = '';
		if($export_format == 'pdf'&&$textboxHeight!=0){

			$tex .= '\keepXColumns\begin{xltabular}{0.25\textwidth}{C} ';	//zacetek tabele, ki bo zgledala kot okvir
			$tex .= ' \begin{tikzpicture} ';			
			
			//\draw (-2,0) -- (2,0) -- (2,1.5 cm) (-2,1.5 cm) -- (-2,0)	
			$tex .= ' \draw (-'.$textboxWidth.',0) -- ('.$textboxWidth.',0) -- ('.$textboxWidth.','.$textboxHeight.' cm)  (-'.$textboxWidth.','.$textboxHeight.' cm) -- (-'.$textboxWidth.',0) node[above right] { ';			
			
			$tex .= '\begin{tabular}{c} ';	//zacetek tabele znotraj skatle, da je lahko vec odgovorov (eden pod drugim) znotraj skatle
			
			$tex .= $text;
			
			$tex .= ' \end{tabular} '; //konec tabele znotraj skatle
			
			
			$tex .= ' }; \end{tikzpicture} ';
			
		}elseif($export_format == 'rtf'||($export_format == 'pdf'&&$textboxHeight==0)){
			$tex .= ' \fbox{\parbox{'.$textboxWidth.'\textwidth}';
		}
		return $tex;
	}
	#funkcija, ki skrbi za pripravo latex U oblike s tekstom  dolocene sirine in visine glede na export format - konec ############	
	
	#funkcija, ki skrbi za pripravo latex okvirja za grid drag and drop  ####################
	#argumenti 1. export_format, 2. visina okvirja, 3. sirina okvirja, 4. tekst naslova okvirja, 5. ali je odgovor prisoten, 6. tekst za odgovore v okvirjih
	function LatexTextGridOfBoxes($export_format='', $textboxHeight=null, $textboxWidth=null, $textNaslovOkvir='', $jeOdgovor=null, $textIzpis=''){		
		$tex = '';
		if($export_format == 'pdf'&&$textboxHeight!=0){
			if($jeOdgovor==0){
				$tex .= '\keepXColumns\begin{xltabular}{0.25\textwidth}{|C|} ';	//zacetek tabele, ki bo zgledala kot okvir
				$tex .= '\hline';	//izris horizontalne obrobe za zacetek tabele
				$tex .= '{\parbox{0.25\textwidth} \centering '; //$tex .= '{\parbox{0.25\textwidth}{\vspace{0.5\baselineskip} \centering ';
				$tex .= $textNaslovOkvir;
				$tex .= '}'; //$tex .= '\vspace{0.5\baselineskip}}}';
				$tex .= $this->texNewLine;
				$tex .= '\hline';	//izris horizontalne obrobe za zakljuciti tabelo
				$tex .= '{\parbox{0.25\textwidth}{\vspace{0.5\baselineskip} \centering ';
				$tex .= '';
				$tex .= '\vspace{0.5\baselineskip}}}';
				$tex .= $this->texNewLine;
				$tex .= '\hline';	//izris horizontalne obrobe za zakljuciti tabelo
				$tex .= '\end{xltabular}'; //konec tabele znotraj skatle
			}else{
				$tex .= '\keepXColumns\begin{xltabular}{0.25\textwidth}{|C|} ';	//zacetek tabele, ki bo zgledala kot okvir
				$tex .= '\hline';	//izris horizontalne obrobe za zacetek tabele
				$tex .= $textNaslovOkvir;				
				$tex .= '\hline';	//izris horizontalne obrobe za zakljuciti tabelo				
				$tex .= $textIzpis;				
				$tex .= '\hline';	//izris horizontalne obrobe za zakljuciti tabelo
				$tex .= '\end{xltabular}'; //konec tabele znotraj skatle			
			}			
		}elseif($export_format == 'rtf'||($export_format == 'pdf'&&$textboxHeight==0)){
			$tex .= ' \fbox{\parbox{'.$textboxWidth.'\textwidth}';
		}
		return $tex;
	}
	#funkcija, ki skrbi za pripravo latex okvirja za grid drag and drop - konec ############


	
	#funkcija, ki skrbi za pravilen izris prve vrstice v tabelah (vrstica z vodoravnimi naslovi multigridov) #############################
	function LatexPrvaVrsticaMultiGrid($steviloStolpcev=null, $enota=null, $trak=null, $customColumnLabelOption=null, $spremenljivke=null, $vodoravniOdgovori=null, $missingOdgovori=null){
		$tex = '';
		for($i = 0; $i < $steviloStolpcev; $i++){
			if ($i != 0){	//ce ni prvi stolpec
				if((($enota==0 && ($trak==0&&$customColumnLabelOption==1)) || ($enota==1 && ($trak==0&&$customColumnLabelOption==1))) ||($enota==0 && $spremenljivke['tip']==16) ||($enota==1 && ($trak==0&&$customColumnLabelOption==1)) || $enota==8 || $enota==3 || $enota==11 || $enota==12 || ($enota==2 && $spremenljivke['tip']==24)){ // ce je (klasicna ali diferencial tabela (brez traku)) ali tabela da/ne ali dvojna tabela ali VAS ali slikovni tip ali roleta/seznam v kombinirani tabeli
					if($i==$steviloStolpcev-1 && $enota==1){	//ce je zadnji stolpec in je diferencial						
						for($m=0;$m<count($missingOdgovori);$m++){
							$tex .= " & ".$missingOdgovori[$m];
						}
						$tex .= " & ";
					}else{
						$tex .= " & ".$vodoravniOdgovori[$i-1];
									
					}
				}elseif($enota == 5){	//maxdiff
					if($i == 1){
						$tex .= ' & ';
					}
					$tex .= $vodoravniOdgovori[$i];
				}
			}elseif($i == 0 && $enota != 5){	//ce je prvi stolpec tabele in ni "maxdiff"
				$tex .= '';
			}elseif($i == 0 && $enota == 5){	//ce je prvi stolpec tabele in "maxdiff"
				$tex .= $vodoravniOdgovori[$i].' & ';
			}elseif( ($i == $steviloStolpcev-1 && $enota != 5) ){	//ce je zadnji stolpec tabele in ni "maxdiff"
				$tex .= ' & ';	
			}
		}
		
		#Nastavitev UPORABA LABEL
		if( $customColumnLabelOption!=1 && $trak==0 && ($enota==0||$enota==1) && $spremenljivke['tip'] == 6 ){	//ce ni potrebno izrisati vseh label vodoravnih odgovorov in je "klasicna tabela" ali "diferencial" (uredi vodoravne labele nad radio buttoni)
			$numGrids = $spremenljivke['grids'];
			if($customColumnLabelOption == 2){	//ce je trenutna moznost prilagajanja "le koncne"
				if(($numGrids%2) == 0){	//ce je parno stevilo, spoji polovico label na vsako skupino label	
					$colParameter1 = $colParameter2 = intval(($numGrids)/2);
				}else if(($numGrids%2) != 0){	//ce ni parno stevilo, spoji prvi skupini label eno celico vec kot pri drugi skupini label
					$colParameter1 = intval(($numGrids)/2 + 0.5);
					$colParameter2 = intval(($numGrids)/2 - 0.5);
				}
				for($i=0; $i<$numGrids; $i++){
					if($i==0){	//ce je prvi stolpec nadnaslovov
						$tex .= ' & \multicolumn{'.$colParameter1.'}{l}{'.$vodoravniOdgovori[$i].'}';
					}elseif( $i==($numGrids-1) ){ //ce je zadnji stolpec nadnaslovov
						$tex .= ' & \multicolumn{'.$colParameter2.'}{r}{'.$vodoravniOdgovori[$i].'}';
					}
				}
			}else if($customColumnLabelOption == 3){	//ce je trenutna moznost prilagajanja "koncne in vmesna"											
				if(($numGrids%3) == 0){	//ce je velikost deljiva s 3, spoji vsako tretjino label
					$colParameter1 = $colParameter2 = $colParameter3 = $numGrids/3;
					$sredina = $numGrids/3;
				}else if(($numGrids%3) == 1){	//ce pri deljenju z 3 je ostanek 1
					$colParameter1 = $colParameter2 = intval($numGrids/3);
					$colParameter3 = intval($numGrids/3)+1;
					$sredina = intval(1 + $numGrids/3);
				}elseif(($numGrids%3) == 2){	//ce pri deljenju z 3 je ostanek 2
					$colParameter1 = $colParameter2 = 1 + intval($numGrids/3);
					$colParameter3 = intval($numGrids/3);
					$sredina = $numGrids%3 + intval($numGrids/3);
				}
				
				for($i=0; $i<$numGrids; $i++){
					if($i==0){	//ce je prvi stolpec nadnaslovov (prva labela)
						$tex .= ' & \multicolumn{'.$colParameter1.'}{l}{'.$vodoravniOdgovori[$i].'}';
					}elseif($i==$sredina){	//ce je sredina (vmesna labela)
						$tex .= ' & \multicolumn{'.$colParameter3.'}{c}{'.$vodoravniOdgovori[$i].'}';
					}elseif( $i==(($numGrids)-1) ){ //ce je zadnji stolpec nadnaslovov (zadnja labela)
						$tex .= ' & \multicolumn{'.$colParameter2.'}{r}{'.$vodoravniOdgovori[$i].'}';
					}
					
				}
			}
		}
		#Nastavitev UPORABA LABEL - KONEC
		//$tex .= '\endhead'; 	//da se naslovna vrstica ponovi na vsaki strani, ce tabela gre na novo stran
		return $tex;
	}
	#funkcija, ki skrbi za pravilen izris prve vrstice v tabelah (vrstica z vodoravnimi naslovi multigridov) - konec #####################
	
	#funkcija, ki skrbi za izris vrstic tabele (z multigrid) ###########################################################
	function LatexVrsticeMultigrid($numRowsSql=null, $export_format='', $enota=null, $simbolTex=null, $navpicniOdgovori=null, $trakStartingNumber=null, $fillablePdf=null, $numColSql=null, $spremenljivke=null, $trak=null, $vodoravniOdgovori=null, $texNewLine='', $navpicniOdgovori2=null, $missingOdgovori=null, $vodoravniOdgovoriTip=null, $vodoravniOdgovoriEnota=null, $vodoravniOdgovoriSprId=null, $data=null, $export_subtype=null, $preveriSpremenljivko=null, $userDataPresent=null, $presirokaKombo = null, $export_data_type=null, $usr_id=null, $loop_id=null, $skipEmpty = null, $skipEmptySub = null){
		//$time_start = microtime(true);
		
		$this->export_subtype = $export_subtype;
		$tex = '';
		global $lang, $site_path;
		$this->path2Images = $site_path.'admin/survey/export/latexclasses/textemp/images/';
		$indeksOdgovorovRespondentMultiNumText = 0;
		
		if(($spremenljivke['enota']==2||$spremenljivke['enota']==6)&&($spremenljivke['tip']!=20&&$spremenljivke['tip']!=19)){	//ce je seznam ali roleta in ni multinumber ALI multitext
			if(count($missingOdgovori)==0){	//ce ni missing vrednosti
				$numColSql = $numColSql + 1;
			}	
		}
		$userAnswerIndex = array();
 		$userAnswerIndex[$spremenljivke['id']] = 0;
		$z = 0;
		$skipRow = false;
		
		//$this->skipEmpty = (int)SurveySetting::getInstance()->getSurveyMiscSetting('export_data_skip_empty'); // izpusti vprasanja brez odgovora
		//$this->skipEmptySub = (int)SurveySetting::getInstance()->getSurveyMiscSetting('export_data_skip_empty_sub'); // izpusti podvprasanja brez odgovora
		$this->skipEmpty = $skipEmpty;
		$this->skipEmptySub = $skipEmptySub;

		if($spremenljivke['tip']==24){	//ce je kombinirana tabela			
			if($presirokaKombo == 1 && count($data) != 0 && (($enota == 2 || $enota == 6) || $export_data_type == 2 && $vodoravniOdgovoriTip[0] == 6)){
				$numColSql = 1 + 1;
			}else{
				$numColSql = count($vodoravniOdgovoriEnota) + 1;
			}
		}
		
		if($trakStartingNumber && $trak){
			$trakStartingNumberTmp = intval($trakStartingNumber); //tmp spremenljivka, ki je potrebna za pravilen izris stevilk, ce imamo trak
		}

			
		//IZRIS PO VRSTICAH
		
		for ($i = 1; $i <= $numRowsSql; $i++){	//za vsako vrstico
			
			if($i == 1 && ($enota == 2 || $enota == 6)&&$spremenljivke['tip']!=24){	//ce je prvi dogovor IN je roleta ALI seznam IN ni kombinirana tabela
				if($export_format == 'rtf'){	//ce je rtf						
					$tex .= ' \hline '; //dodaj crto
				}
			}
			
			// Ce imamo nastavljeno preskakovanje podvprasanj preverimo ce je kaksen odgovor v vrstici
			if($this->skipEmptySub == 1){
				$skipRow = true;
				for($z = $z; $z < $i * ($numColSql - 1); $z++){
					if(is_array($data)){
						if(array_key_exists($z, $data)){
							if($data[$z]){
								$skipRow = false;
								$podatekZaSlikovniTip = $data[$z];	//belezi podatek za slikovni tip, ki pride prav za pravilen izpis izvoza						
							}
						}
					}
				}
			}			
			// Ce imamo nastavljeno preskakovanje podvprasanj preverimo ce je kaksen odgovor v vrstici - konec

			if(!$skipRow || (!$userDataPresent && $this->skipEmpty == 0) || $export_subtype == 'q_empty'){	/* ce je potrebno preskociti vrstico ALI (ni podatkov za prikaz, vendar je potrebno pokazati vprasanja brez odgovorov) ALI je prazen vprasalnik */
				if($i%2 != 0 && $export_format == 'pdf'){
					if($enota == 5){	//ce je maxdiff
						$tex .= "\\rowcolor[gray]{.9}".$this->getAnswerSymbol($this->export_subtype, $export_format, $fillablePdf, $spremenljivke['tip'], $numColSql, 0, $data[$userAnswerIndex[$spremenljivke['id']]]).' & ';	//pobarvaj ozadje vrstice
						//$tex .= "\\rowcolor[gray]{.9}".$simbolTex.' & ';	//pobarvaj ozadje vrstice
						$userAnswerIndex[$spremenljivke['id']]++;
					}else{
						$tex .= "\\rowcolor[gray]{.9}".$navpicniOdgovori[$i-1];	//pobarvaj ozadje vrstice
					}				
				}else{
					if($enota == 5){	//ce je maxdiff
						$tex .= $this->getAnswerSymbol($this->export_subtype, $export_format, $fillablePdf, $spremenljivke['tip'], $numColSql, 0, $data[$userAnswerIndex[$spremenljivke['id']]]).' & ';
						$userAnswerIndex[$spremenljivke['id']]++;
					}else{
						$tex .= $navpicniOdgovori[$i-1];
					}
				}
				
				
				
				//ureditev spremenljivk za pravilen kasnejsi izris seznama ali rolete
				$roletaAliSeznam = 0;	//belezi, ali je tak tip podtabele ali tabele prisoten
				$indeksRoleta = 1;
				$noItem = 1;
				if($spremenljivke['tip']==24){	//ce je kombinirana tabela, uredi enote znotraj te tabele			
					foreach($vodoravniOdgovoriEnota as $enota){
						if($enota == 2 || $enota == 6){	//roleta ali izberite s seznama, uredi ogrodje za itemize, da se bo dalo zadevo pravilno izrisati
							$roletaAliSeznam = 1;
							$indeksRoleta = 1;
							$noItem = 1;							
						}				
					}
					//$numColSql = count($vodoravniOdgovoriEnota) + 1;
				}
				//ureditev spremenljivk za pravilen kasnejsi izris seznama ali rolete - konec
				
				for($j = 1; $j < $numColSql; $j++){	//izris posameznega stolpca v vrstici
					if($spremenljivke['tip']==24){	//ce je kombinirana tabela, uredi enote znotraj te tabele
						if($presirokaKombo == 1 && count($data) != 0 && ($enota == 2 || $enota == 6)){
							$enota = $vodoravniOdgovoriEnota[0];
						}else{
							$enota = $vodoravniOdgovoriEnota[$j-1];
						}
						$sprID = $vodoravniOdgovoriSprId[$j-1];
					}
					
					if($enota==0||$enota==1||$enota==3||$enota==11||$enota==12||$spremenljivke['tip']==19||$spremenljivke['tip']==20){		//klasika ali diferencial ali VAS ali slikovni tip ali multitext ali multinumber
						if(($trak == 1 && $enota != 3 && $spremenljivke['tip'] == 6)||($spremenljivke['tip']==19||$spremenljivke['tip']==20)){	//ce je trak ali multitext ali multinumber
							if($j<=$spremenljivke['grids']){	//ce so stolpci, ki vsebujejo trak s stevilkami ali textbox-e						
								if($spremenljivke['tip']==19||$spremenljivke['tip']==20){ //ce je multitext ali multinumber
									if($export_subtype=='q_data'||$export_subtype=='q_data_all'){	//ce je odgovor respondenta ali vec respondentov
										$tex .= "& ".$simbolTex[$indeksOdgovorovRespondentMultiNumText];
									}elseif($export_subtype=='q_empty'||$export_subtype=='q_comment'){
										$tex .= "& ".$simbolTex;
									}
								}else{
									$tex .= '& '.($trakStartingNumberTmp);	//prikazovanje brez obrob celic
									$trakStartingNumberTmp++;
								}
							}else{	//drugace so missing-i, kjer je potrebno izrisati ustrezen simbol (radio button)									
								if($enota==0&&($spremenljivke['tip']==6||$spremenljivke['tip']==16)){	//ce je klasicna tabela ali multitext ali multinumber
									$tex .= "& ".$simbolTex;
								}elseif($spremenljivke['tip']==19||$spremenljivke['tip']==20){//ce je multitext ali multinumber, izrisi missing simbol kot radio
									if($export_subtype=='q_data'||$export_subtype=='q_data_all'){	//ce je odgovor respondenta ali vec respondentov
										$tex .= "& ".$simbolTex[$indeksOdgovorovRespondentMultiNumText];
									}else{
										$radioButtonTex = ($export_format=='pdf'?"{\Large $\ocircle$}" : "\\includegraphics[scale=".RADIO_BTN_SIZE."]{".$this->path2Images."radio}");
										$tex .= "& ".$radioButtonTex;
									}
									//$tex .= "& ".$radioButtonTex;
									
									//echo "radio button, ko je missing: ".$radioButtonTex."</br>";
								}									
							}
						}else{								
							if($spremenljivke['tip']==24){	//ce je kombinirana tabela, s klasicno podtabelo
								if( is_array($data) ){
									$data_4_izpis = $data[$userAnswerIndex[$spremenljivke['id']]];
								}else{
									$data_4_izpis = null;
								}
								if($export_data_type==0 || $export_data_type==1 || ($export_data_type==2 && $vodoravniOdgovoriTip[$j-1] != 6)){ //ce je razsirjen izvoz ALI je skrcen izvoz IN ni klasicna tabela

									$tex .= "& ".$this->getAnswerSymbol($this->export_subtype, $export_format, $fillablePdf, $vodoravniOdgovoriTip[$j-1], $numColSql, 0, $data_4_izpis);
									
								}else{
									$test = $data_4_izpis;
									if($usr_id){
										$test = Common::getInstance()->dataPiping($test, $usr_id, $loop_id);
									}
									$test = LatexDocument::encodeText($test);
									$tex .= ' & \\textcolor{crta}{\footnotesize{'.$test.'}}';
								}
								/* elseif($export_data_type==2 && $vodoravniOdgovoriTip[$j-1]){ //ce je skrcen izvoz IN 

								} */
								
							}else{	//ce so ostali tipi vprasanj
								if($enota == 12){ //ce je slikovni tip										
									$podatekSlikovniTip = $podatekZaSlikovniTip;										
									if($j <= $podatekSlikovniTip){
										$podatekSlikovniTipTmp = $podatekSlikovniTip;
									}else{
										$podatekSlikovniTipTmp = 0;
									}										
									$tex .= "& ".$this->getAnswerSymbol($this->export_subtype, $export_format, $fillablePdf, $spremenljivke['tip'], $numColSql, 0, 
									$podatekSlikovniTipTmp, $enota, $j, '', $spremenljivke['id']);
								}elseif($enota == 11){ //ce je VAS
									if(array_key_exists($userAnswerIndex[$spremenljivke['id']], $data)){
										$dataUserAnswerIndex = $data[$userAnswerIndex[$spremenljivke['id']]];
									}
									$tex .= "& ".$simbolTex[$j-1];
									//if($i == 1){ //ce je prva iteracija uredi VAS simbole
									//	$tex .= "& ".$simbolTex[$j-1];
									//}else{	//drugace naj bo prazno
									//	$tex .= "& ";
									//}
								}else{ //drugace
									if(array_key_exists($userAnswerIndex[$spremenljivke['id']], $data)){
										$dataUserAnswerIndex = $data[$userAnswerIndex[$spremenljivke['id']]];
									}
									$tex .= "& ".$this->getAnswerSymbol($this->export_subtype, $export_format, $fillablePdf, $spremenljivke['tip'], $numColSql, 0, $dataUserAnswerIndex, $enota, $j, '', $spremenljivke['id']);									
								}
							}
						}
					}elseif($enota==2||$enota==6){	//roleta ali izberite s seznama
						if($export_format == 'pdf'){	//ce je pdf
							$beginItemize = '& \begin{itemize}[leftmargin=*]';	//zacetek itemize, ce je pdf
						}else{
							$beginItemize = '& \begin{itemize}';	//zacetek itemize, ce je rtf
						}
						if($spremenljivke['tip']!=24){ //ce ni kombinirana tabela
							if($j==1){	//ce je prvi mozen odgovor v roleti ali seznamu
								$tex .= $beginItemize;	//zacetek itemize
							}
						}
						
						if($export_subtype=='q_empty'){	//ce je prazen vprasalnik
							if($spremenljivke['tip']==24){	//ce je kombinirana tabela z izberite s seznama (ali roleto)
								$tex .= "& ".$this->getAnswerSymbol($this->export_subtype, $export_format, $fillablePdf, $vodoravniOdgovoriTip[$j-1], $numColSql, 0, $data[$userAnswerIndex[$spremenljivke['id']]]);
							}
							else{	//ce je roleta ali seznam
								if($i==1){	//samo za prvo vrstico izpisi vse mozne odgovore v roleti
									$tex .= '\item[] '.$vodoravniOdgovori[$j-1];	//izris odgovora v roleti kot item
								}elseif($j==1){
									$tex .= '\item[] ';	//prazno vrstico
								}
							}
						}else{	//drugace, ce je vprasalnik z odgovori
							if($spremenljivke['tip'] != 24){	//ce ni kombinirana tabela z izberite s seznama (ali roleto)
								if($data[$userAnswerIndex[$spremenljivke['id']]]==($indeksRoleta)){	//ce je prisoten podatek za doloceni indeks seznama, ga izpisi	
									if($export_data_type==0||$export_data_type==2){	//ce skrcen izvoz
										$tex .= '& \\textcolor{crta}{\footnotesize{'.$vodoravniOdgovori[$j-1].'}}';	//izris odgovora respondenta v roleti ali											
									}else{	//drugace, ce je razsirjen izvoz
										$tex .= '\item[] \\textcolor{crta}{\footnotesize{'.$vodoravniOdgovori[$j-1].'}}';	//izris odgovora respondenta v roleti ali 
									}								
									$noItem = 0;
									
								}else{
									if($export_data_type==0||$export_data_type==2){	//ce skrcen izvoz
										$tex .= ' & '.$vodoravniOdgovori[$j-1];
									}else{	//drugace, ce je razsirjen izvoz
										//echo "tip exp: ".$export_data_type."</br>";
									}
									
								}
							}else{ //ce je kombinirana tabela z izberite s seznama (ali roleto)
								$tex .= ' & \\textcolor{crta}{\footnotesize{'.$data[$userAnswerIndex[$spremenljivke['id']]].'}}';

							}
							
						}
						
						$indeksRoleta++;				
						
						if($spremenljivke['tip']==24&&$sprID!=$vodoravniOdgovoriSprId[$j]){//ce je naslednji ID spremenljivke razlicen od trenutnega ID
							if($presirokaKombo == 0){
								//$tex .= '\end{itemize}';	//zakljucek itemize
							}
							$roletaAliSeznam = 1;
						}
						
					
					}elseif($enota == 4){	//ena moznost proti drugi
						if($data[$userAnswerIndex[$spremenljivke['id']]]==1){
							$simbolTex1=$this->getAnswerSymbol($this->export_subtype, $export_format, $fillablePdf, $spremenljivke['tip'], $numColSql, 0, $data[$userAnswerIndex[$spremenljivke['id']]]);
							$simbolTex2=$simbolTex;							
						}elseif($data[$userAnswerIndex[$spremenljivke['id']]]==2){
							$simbolTex1=$simbolTex;
							$simbolTex2=$this->getAnswerSymbol($this->export_subtype, $export_format, $fillablePdf, $spremenljivke['tip'], $numColSql, 0, 1);
						}elseif($data[$userAnswerIndex[$spremenljivke['id']]]==''){
							$simbolTex1=$simbolTex;
							$simbolTex2=$simbolTex;
							$simbolTex3='';
						}						
						$tex .= '& '.$simbolTex1.' & '.$lang['srv_tip_sample_t6_4_vmes'].' & '.$simbolTex2;
					}elseif($enota == 5){	//maxdiff
						$tex .= $navpicniOdgovori[$i-1].' & '.$this->getAnswerSymbol($this->export_subtype, $export_format, $fillablePdf, $spremenljivke['tip'], $numColSql, 0, $data[$userAnswerIndex[$spremenljivke['id']]]);					
					}elseif($enota == 8){	//tabela da/ne
						$tex .= ' & '.$this->getAnswerSymbol($this->export_subtype, $export_format, $fillablePdf, $spremenljivke['tip'], $numColSql, 0, $data[$userAnswerIndex[$spremenljivke['id']]]);
					}
					
					$indeksOdgovorovRespondentMultiNumText++;

					$userAnswerIndex[$spremenljivke['id']]++;
				}	//IZRIS PO STOLPCIH - KONEC
				
				if($enota==1 || $enota==4){	//ce je "diferencial tabela" ali "ena moznost proti drugi", dodaj se tekst v zadnjem stolpcu tabele
					$tex .= ' & '.$navpicniOdgovori2[$i-1].' ';	//tekst v drugem stolpcu ob symbol
					
					if(($enota==4 && count($missingOdgovori)!=0)||($enota==1 && $trak==1 && count($missingOdgovori)!=0)){	//ce je "ena moznost proti drugi" in so missingi ALI je "diferencial tabela" na traku in so missingi
						for($m=0;$m<count($missingOdgovori);$m++){						
							$tex .= ' & '.$this->getAnswerSymbol($this->export_subtype, $export_format, $fillablePdf, $spremenljivke['tip'], $numColSql, 0, $data[$userAnswerIndex[$spremenljivke['id']]]);						
							$userAnswerIndex[$spremenljivke['id']]++;
							if($enota==4){	//ce je "ena moznost proti drugi"
								$tex .= ' '.$missingOdgovori[$m];	//izpisi se missing odgovor
							}
						}
					}
					
				}
				
				if($enota==5&&count($missingOdgovori)!=0){	//ce je maxdiff in so missingi
					 for($m=0;$m<count($missingOdgovori);$m++){
						//$tex .= ' & '.$simbolTex.' '.$missingOdgovori[$m];
						$tex .= ' & '.$this->getAnswerSymbol($this->export_subtype, $export_format, $fillablePdf, $spremenljivke['tip'], $numColSql, 0, $data[$userAnswerIndex[$spremenljivke['id']]]).' '.$missingOdgovori[$m];
						$userAnswerIndex[$spremenljivke['id']]++;
					}
				}
							
				if(($enota == 2 || $enota == 6)&&$spremenljivke['tip']!=24&&$spremenljivke['tip']!=20&&$spremenljivke['tip']!=19){	//ce je roleta ali seznam in ni kombinirana tabela in ni mutinumber in ni multitekst
					$tex .= '\end{itemize}';	//zakljucek itemize
				}
				
				$tex .= $texNewLine;
				if($spremenljivke['tip']==24){
					//$userAnswerIndex++;
				}
			}else{
				$userAnswerIndex[$spremenljivke['id']]=$z;
			}
			if(($enota == 2 || $enota == 6)&&$spremenljivke['tip']!=24){	//ce je roleta ali seznam in ni kombinirana tabela
				if($export_format == 'rtf'){	//ce je rtf						
					$tex .= ' \hline '; //dodaj crto na koncu vrstice
				}
			}
			//echo "koda vrstice: $tex </br>";
		}
		//IZRIS PO VRSTICAH - KONEC
		//$time_end = microtime(true);
		//	$execution_time = ($time_end - $time_start);
		//	echo '</br><b>Total Execution Time izpisa vrstice</b> '.$execution_time.' sec za vprašanje '.strip_tags($spremenljivke['naslov']).'</br>';
		return $tex;
	}
	#funkcija, ki skrbi za izris vrstic tabele (z multigrid) - konec ###########################################################
		
	function getUserId() {return ($this->usr_id)?$this->usr_id:false;}
		
	#funkcija, ki skrbi za preverjanje obstoja podatkov za vprasanja, ki niso grid ali kombinirana tabela
	function GetUsersData($db_table=null, $spremenljivkeId=null, $spremenljivkeTip=null, $usr_id=null, $loop_id_raw=null){
		$userDataPresent = 0;	//belezi, ali je odgovor respondenta prisoten in je indeks za določena polja, ki shranjujejo podatke o odgovorih respondenta
		$loop_id = $loop_id_raw == null ? " IS NULL" : " = '".$loop_id_raw."'";
		
		// če imamo vnose, pogledamo kaj je odgovoril uporabnik
		if( in_array($spremenljivkeTip, array(21, 4, 7, 8, 18)) ){	//ce je tip besedilo ali besedilo staro (4) ali stevilo ali datum ali vsota
			$sqlUserAnswerString ="SELECT text FROM srv_data_text".$db_table." WHERE spr_id='".$spremenljivkeId."' AND usr_id='".$usr_id."' AND loop_id $loop_id ";
		}elseif($spremenljivkeTip==17){		//ce je razvrscanje
			//$sqlUserAnswer = sisplet_query("SELECT vrstni_red FROM srv_data_rating WHERE spr_id=".$spremenljivke['id']." AND usr_id='".$this->getUserId()."' AND vre_id='".$rowVrednost['id']."' AND loop_id $loop_id");
			//$sqlUserAnswerString = "SELECT vrstni_red FROM srv_data_rating WHERE spr_id='".$spremenljivkeId."' AND usr_id='".$usr_id."' ";
			$sqlUserAnswerString = "SELECT vrstni_red FROM srv_data_rating WHERE spr_id='".$spremenljivkeId."' AND usr_id='$usr_id' AND loop_id $loop_id ";
		}elseif($spremenljivkeTip==26){		//ce je lokacija
			//$sqlUserAnswerString ="SELECT lat, lng, address, text FROM srv_data_map WHERE spr_id='".$spremenljivkeId."' AND usr_id='".$usr_id."' ";
			$sqlUserAnswerString ="SELECT IF(dm.lat > 0, dm.lat, vm.lat) as lat, IF(dm.lng > 0, dm.lng, vm.lng) as lng, IF(dm.address != \"\", dm.address, vm.address) as address, text FROM srv_data_map as dm "
                                . "LEFT JOIN (SELECT lat, lng, address, vre_id FROM srv_vrednost_map) AS vm on vm.vre_id=dm.vre_id "
                                . "WHERE spr_id='".$spremenljivkeId."' AND usr_id='$usr_id' AND loop_id $loop_id ";
		}elseif($spremenljivkeTip==27){		//ce je heatmap
			//$sqlUserAnswerString ="SELECT lat, lng, address, text FROM srv_data_heatmap WHERE spr_id='".$spremenljivkeId."' AND usr_id='".$usr_id."' ";
			$sqlUserAnswerString ="SELECT lat, lng, address, text FROM srv_data_heatmap WHERE spr_id='".$spremenljivkeId."' AND usr_id='$usr_id' AND loop_id $loop_id ";
		}else{	
			//$sqlUserAnswerString =  "SELECT vre_id FROM srv_data_vrednost".$db_table." WHERE spr_id='$spremenljivkeId' AND usr_id=$usr_id";
			$sqlUserAnswerString =  "SELECT vre_id FROM srv_data_vrednost".$db_table." WHERE spr_id='$spremenljivkeId' AND usr_id='$usr_id' AND loop_id $loop_id";
		}
		
		$sqlUserAnswer = sisplet_query($sqlUserAnswerString);
		
		if( in_array($spremenljivkeTip, array(21, 4, 7, 8, 18, 17)) ){//ce je tip besedilo ali stevilo ali datum ali vsota ali razvrscanje
			$rowAnswers = mysqli_fetch_assoc($sqlUserAnswer);
			if($rowAnswers){	//ce je kaj v bazi
				$userDataPresent++;
			}
		}else{
			if($sqlUserAnswer){	//ce je kaj v bazi
				while ($rowAnswers = mysqli_fetch_assoc($sqlUserAnswer)){
					if($spremenljivkeTip==26||$spremenljivkeTip==27){
						//$this->userAnswer = $rowAnswers;
						$this->userAnswer[$userDataPresent] = $rowAnswers;
						$userDataPresent++;
					}else{
						$this->userAnswer[$rowAnswers['vre_id']] = $rowAnswers['vre_id'];
						if($rowAnswers['vre_id']>0){
							$userDataPresent++;
						}
					}				
				}		
			}
		}
		return $userDataPresent;
	}
	#funkcija, ki skrbi za preverjanje obstoja podatkov za vprasanja, ki niso grid ali kombinirana tabela - konec
	
	#funkcija, ki skrbi za preverjanje obstoja podatkov za vprasanja z grid	
	function GetUsersDataGrid($spremenljivke=null, $db_table=null, $rowVrednost=null, $rowVsehVrednosti=null, $usr_id=null, $subtip=null, $loop_id_raw=null, $export_data_type=null){
		$loop_id = $loop_id_raw == null ? " IS NULL" : " = '".$loop_id_raw."'";
		// poiščemo kaj je odgovoril uporabnik: PREVERITI, CE JE POTREBEN STAVEK Z LOOP IN KDAJ JE TO AKTUALNO
		if(($spremenljivke['tip']==16)||($spremenljivke['tip']==6&&$spremenljivke['enota']==3)){	//ce je grid checkbox ali dvojna tabela
			$sqlString = "SELECT grd_id, vre_id FROM srv_data_checkgrid".$db_table." WHERE spr_id = '".$rowVrednost['spr_id']."' AND usr_id = '".$usr_id."' AND vre_id = '".$rowVrednost['id']."' AND grd_id = ".$rowVsehVrednosti['id']." AND loop_id $loop_id";
		}elseif($spremenljivke['tip']==6){	//ce je grid radio
			$sqlString ="SELECT grd_id, vre_id FROM srv_data_grid".$db_table." WHERE spr_id = '".$rowVrednost['spr_id']."' AND usr_id = '".$usr_id."' AND vre_id = '".$rowVrednost['id']."' AND grd_id = ".$rowVsehVrednosti['id']." AND loop_id $loop_id";
		}elseif($spremenljivke['tip']==19||$spremenljivke['tip']==20){	//ce je grid besedila ali stevil
			$sqlString = "SELECT grd_id, text, vre_id FROM srv_data_textgrid".$db_table." where spr_id = '".$rowVrednost['spr_id']."' and usr_id = '".$usr_id."' AND vre_id = '".$rowVrednost['id']."' AND grd_id = ".$rowVsehVrednosti['id'];
		}elseif($spremenljivke['tip']==24){	//ce je kombo			
			if($subtip==6){	//ce je grid radio
				if($rowVrednost['enota'] != 2 && $rowVrednost['enota'] != 6 && ($export_data_type == 1)){	//ce ni roleta in seznam IN je razsirjen izvoz
					$sqlString ="SELECT grd_id FROM srv_data_grid".$db_table." WHERE spr_id = '".$rowVrednost['spr_id']."' AND usr_id = '".$usr_id."' AND vre_id = '".$rowVrednost['id']."' AND grd_id = ".$rowVsehVrednosti['id']." AND loop_id $loop_id";
				}else{	//ce je roleta ali seznam
					$sqlString ="SELECT g.naslov, gdata.grd_id FROM srv_grid g, srv_data_grid".$db_table." gdata WHERE g.id=gdata.grd_id AND g.spr_id = '".$rowVrednost['spr_id']."' AND gdata.usr_id = '".$usr_id."' AND gdata.vre_id = '".$rowVrednost['id']."' AND gdata.loop_id $loop_id";
				}
			}elseif($subtip==16){	//ce je grid checkbox ali dvojna tabela
				$sqlString = "SELECT grd_id FROM srv_data_checkgrid".$db_table." WHERE spr_id = '".$rowVrednost['spr_id']."' AND usr_id = '".$usr_id."' AND vre_id = '".$rowVrednost['id']."' AND grd_id = ".$rowVsehVrednosti['id']." AND loop_id $loop_id";
			}elseif($subtip==19||$subtip==20){	//ce je grid besedila ali stevil
				$sqlString = "SELECT grd_id, text FROM srv_data_textgrid".$db_table." where spr_id = '".$rowVrednost['spr_id']."' and usr_id = '".$usr_id."' AND vre_id = '".$rowVrednost['id']."' AND grd_id = ".$rowVsehVrednosti['id'];
			}
		}
		$sqlUserAnswer = sisplet_query($sqlString);
		return $sqlUserAnswer;
	}
	#funkcija, ki skrbi za preverjanje obstoja podatkov za vprasanja z grid - konec
	
	#funkcija, ki skrbi za preverjanje obstoja podatkov za vprasanja s kombinirano tabelo
	function GetUsersDataKombinirana($spremenljivke=null, $db_table=null, $usr_id=null, $presirokaTabela=0, $loop_id_raw=null, $export_data_type=null){
		$userDataPresent = 0;	//belezi, ali je odgovor respondenta prisoten in je indeks za določena polja, ki shranjujejo podatke o odgovorih respondenta
		$userAnswerSprIds = array();
		$userAnswerSprTip = array();
		$userAnswerSprIdsIndex = 0;
		$orStavek = '';
		//$loop_id = $loop_id_raw == null ? " IS NULL" : " = '".$loop_id_raw."'";
		$loop_id = $loop_id_raw;
		$userAnswers = array();
		
		#za pridobitev stevila vrstic
		$sqlVrednostiKombo = sisplet_query("SELECT id, naslov, naslov2, variable, other FROM srv_vrednost WHERE spr_id='".$spremenljivke['id']."' ORDER BY vrstni_red");		
		$numRowsSql = mysqli_num_rows($sqlVrednostiKombo);
		#za pridobitev stevila vrstic - konec
		
		#za pridobitev stevila stolpcev
		$sqlStVrednostiKombo = sisplet_query("SELECT count(*) FROM srv_grid g, srv_grid_multiple m WHERE m.spr_id=g.spr_id AND m.parent='".$spremenljivke['id']."'");
		$rowStVrednost = mysqli_fetch_array($sqlStVrednostiKombo);	//stevilo stolpcev		
		$numColSql = $rowStVrednost['count(*)'];	//stevilo vseh stolpcev
		#za pridobitev stevila stolpcev - konec

		if($presirokaTabela==0){	//ce tabela ni presiroka
			$sqlSubGrid = sisplet_query("SELECT m.spr_id, s.tip FROM srv_grid_multiple m, srv_spremenljivka s WHERE m.parent='".$spremenljivke['id']."' AND m.spr_id=s.id ORDER BY m.vrstni_red");	//pridobimo spr_id in tip podvprasanj, ki sestavljajo kombinirano tabelo
			
			while($rowSubGrid = mysqli_fetch_array($sqlSubGrid)){
				array_push($userAnswerSprIds, $rowSubGrid['spr_id'] );	//filanje polja s spr_id podvprasanj
				array_push($userAnswerSprTip, $rowSubGrid['tip'] );	//filanje polja s tip podvprasanj
				if($userAnswerSprIdsIndex){
					$orStavek .= ' OR ';
				}
				$orStavek .= "v.spr_id='".$rowSubGrid['spr_id']."' ";
				$userAnswerSprIdsIndex++;
			}
		}else{
			$orStavek = "v.spr_id='".$spremenljivke['id']."' ";
		}	
		
		for($i=1;$i<=$numRowsSql;$i++){
			$sqlVrednostiString = "SELECT v.spr_id, v.naslov, s.tip, v.id, s.enota FROM srv_vrednost v, srv_spremenljivka s WHERE v.spr_id=s.id AND (".$orStavek.") AND v.vrstni_red=".($i).";";
			$sqlVrednosti = sisplet_query($sqlVrednostiString);
			while($rowVrednosti = mysqli_fetch_assoc($sqlVrednosti)){
				$sqlVsehVrednostiString = "SELECT id, naslov FROM srv_grid WHERE spr_id='".$rowVrednosti['spr_id']."' ORDER BY 'vrstni_red'";

				$sqlVsehVrednosti = sisplet_query($sqlVsehVrednostiString);
				
				$roletaZabelezena = 0;

				while ($rowVsehVrednosti = mysqli_fetch_assoc($sqlVsehVrednosti)){
					if($roletaZabelezena == 0){
						$sqlUserAnswer = $this->GetUsersDataGrid($spremenljivke, $db_table, $rowVrednosti, $rowVsehVrednosti, $usr_id, $rowVrednosti['tip'], $loop_id, $export_data_type);
					}										
					$userAnswer = mysqli_fetch_assoc($sqlUserAnswer);
					if($rowVrednosti['tip']==19||$rowVrednosti['tip']==20){							
						$userAnswers[$userDataPresent] = $userAnswer['text'];
					}else{
						if($roletaZabelezena == 0){
							//$userAnswers[$userDataPresent] = $userAnswer['grd_id'];
							$userAnswers[$userDataPresent] = $userAnswer['naslov'];
							//if($rowVrednosti['enota']==2 || $rowVrednosti['enota']==6){ //ce je roleta ali seznam
							if($rowVrednosti['enota']==2 || $rowVrednosti['enota']==6 || ($export_data_type==2 && $rowVrednosti['tip']==6)){ //ce je roleta ali seznam
								$roletaZabelezena = 1;
								$userDataPresent++;
							}else{
								$userAnswers[$userDataPresent] = $userAnswer['grd_id'];
							}
							
						}
					}
					if($roletaZabelezena == 0){ 
						$userDataPresent++;
					}
				}

			}
			
		}
		return $userAnswers;
	}
	#funkcija, ki skrbi za preverjanje obstoja podatkov za vprasanja s kombinirano tabelo - konec
	
	#funkcija, ki skrbi za pridobitev operatorja iz stevilskega podatka ###########################################################
	function GetOperator($operatorNum=null){
		if ($operatorNum == 0){
			$operator = LatexDocument::encodeText('+');
		}elseif ($operatorNum == 1){
			$operator = LatexDocument::encodeText('-');
		}elseif ($operatorNum == 2){
			$operator = LatexDocument::encodeText('*');
		}elseif ($operatorNum == 3){
			$operator = LatexDocument::encodeText('/');
		}
		return $operator;
	}
	#funkcija, ki skrbi za pridobitev operatorja iz stevilskega podatka - konec ###################################################
	
			/**
    * @desc V podanem stringu poisce spremenljivke in jih spajpa z vrednostmi
    */
    function dataPiping ($text='') {
    	Common::getInstance()->Init($this->anketa);
        return Common::getInstance()->dataPiping($text, $this->usr_id, $this->loop_id);
    }
	

}