<?php
/***************************************
 * Description: Priprava Latex kode za Razvrscanje
 *
 * Vprašanje je prisotno:
 * tip 17
 *
 * Autor: Patrik Pucer
 * Datum: 08-09/2017
 *****************************************/


if (!defined('PIC_SIZE')) define("PIC_SIZE", "\includegraphics[width=5cm]"); 	//slika sirine 50mm
if (!defined('ICON_SIZE')) define("ICON_SIZE", "\includegraphics[width=0.5cm]"); 	//za ikone @ slikovni tip
if (!defined('RADIO_BTN_SIZE')) define("RADIO_BTN_SIZE", 0.13);

class RazvrscanjeLatex extends LatexSurveyElement
{	
    public function __construct()
    {
        //parent::getGlobalVariables();
    }

    /************************************************
     * Get instance
     ************************************************/
    private static $_instance;
	protected $texBigSkip = ' \bigskip ';	
	protected $loop_id = null;	// id trenutnega loopa ce jih imamo
	protected $path2ImagesRadio = null;

    public static function getInstance()
    {
        if (self::$_instance)
            return self::$_instance;

        return new RazvrscanjeLatex();
    }
	

	public function export($spremenljivke=null, $export_format='', $questionText='', $fillablePdf=null, $texNewLine='', $usr_id=null, $db_table=null, $export_subtype='', $preveriSpremenljivko=null, $export_data_type=null, $loop_id=null){
		//$time_start = microtime(true);
		global $lang, $site_path;
		// Ce je spremenljivka v loopu
		$this->loop_id = $loop_id;
		$this->path2ImagesRadio = $site_path.'uploadi/editor/';
		
		//preveri, ce je kaj v bazi
		//$userDataPresent = $this->GetUsersData($db_table, $spremenljivke['id'], $spremenljivke['tip'], $usr_id);
		$userDataPresent = $this->GetUsersData($db_table, $spremenljivke['id'], $spremenljivke['tip'], $usr_id, $this->loop_id);
		
		if($userDataPresent||$export_subtype=='q_empty'||$export_subtype=='q_comment'||$preveriSpremenljivko){	//ce je kaj v bazi ali je prazen vprasalnik ali je potrebno pokazati tudi ne odgovorjena vprasanja
			global $lang;
			
			// iz baze preberemo vse moznosti - ko nimamo izpisa z odgovori respondenta			
			//$sqlVrednosti = sisplet_query("SELECT id, naslov, naslov2, variable, other FROM srv_vrednost WHERE spr_id='".$spremenljivke['id']."' ORDER BY vrstni_red");
			$sqlVrednosti = sisplet_query("SELECT id, naslov, naslov2, variable, other FROM srv_vrednost WHERE spr_id='".$spremenljivke['id']."' AND hidden='0' ORDER BY vrstni_red");
			$numRowsSql = mysqli_num_rows($sqlVrednosti);
			
			$tex = '';
			
			//nastavitve iz baze ##########################
			$spremenljivkaParams = new enkaParameters($spremenljivke['params']);		
			$tipRazvrscanja = $spremenljivke['design'];	//0-Prestavljanje, 1-Ostevilcevanje, 2-Premikanje
			$steviloDesnihOkvirjev = $spremenljivke['ranking_k']; //nastavitev Moznosti: 0-Vsi, 1....
			if($steviloDesnihOkvirjev==0){	//ce je 0, je stevilo desnih okvirjev enako stevilo vnesenih odgovorov na levi strani
				$steviloDesnihOkvirjev=$numRowsSql;
			}
			//nastavitve iz baze - konec ####################
					
			$navpicniOdgovori = array();
			$navpicniOdgovori = [];
			
			$odgovoriRespondenta = array();
			$odgovoriRespondenta = [];
			
			$texNewLineAfterTable = $texNewLine." ".$texNewLine." ".$texNewLine;
								
			//pregled vseh moznih vrednosti (kategorij) po $sqlVrednosti
			while ($rowVrednost = mysqli_fetch_assoc($sqlVrednosti)){
				$jeOdgovor = 0;	//belezi, ali je trenutna vrednost odgovora, odgovor respondenta ali ne
				$stringTitleRow = $rowVrednost['naslov']; //odgovori na levi strani
				
				if($userDataPresent){	//ce so prisotni podatki respondenta
					//preverjanje podatkov trenutnega uporabnika ######################################################
					//$sqlUserAnswer = sisplet_query("SELECT vrstni_red FROM srv_data_rating WHERE spr_id=".$spremenljivke['id']." AND usr_id='".$this->getUserId()."' AND vre_id='".$rowVrednost['id']."' AND loop_id $loop_id");
					$sqlUserAnswer = sisplet_query("SELECT vrstni_red FROM srv_data_rating WHERE spr_id=".$spremenljivke['id']." AND usr_id='".$usr_id."' AND vre_id='".$rowVrednost['id']."' ".($loop_id !== null ? " AND loop_id='$loop_id'" : ""));
					
					$userAnswer = mysqli_fetch_assoc($sqlUserAnswer);					
					
					if($userAnswer){	//ce je kaj v bazi
						if($tipRazvrscanja==1){	//ce je Ostevilcevanje
							$odgovorRespondenta = $userAnswer['vrstni_red'];
							array_push($odgovoriRespondenta, $odgovorRespondenta);	//filanje polja z odgovori respondenta (stevilke)
						}
						$jeOdgovor = 1;
					}					
					//preverjanje podatkov trenutnega uporabnika - konec ##############################################
				}
				
				if($jeOdgovor==0||$tipRazvrscanja==1){	//ce ni odgovor respondenta, bo naslov na levi strani; ali je Ostevilcevanje
					array_push($navpicniOdgovori, LatexDocument::encodeText($stringTitleRow, $rowVrednost['id']) );	//filanje polja z navpicnimi odgovori (po vrsticah)
				}
			}
			//pregled vseh moznih vrednosti (kategorij) po $sqlVrednosti - konec
			
			if($userDataPresent&&$tipRazvrscanja!=1){	//ce so prisotni podatki respondenta in ni Ostevilcevanje
				#ureditev polja s podatki trenutnega uporabnika ######################################################
				//$sqlOdgovoriRespondentaString = "SELECT v.naslov, v.id from srv_vrednost v, srv_data_rating r WHERE r.spr_id=v.spr_id AND r.usr_id=".$usr_id." AND r.vre_id=v.id AND r.spr_id=".$spremenljivke['id']." ORDER BY r.vrstni_red";
				$sqlOdgovoriRespondentaString = "SELECT v.naslov, v.id from srv_vrednost v, srv_data_rating r WHERE ".($loop_id !== null ? " r.loop_id='$loop_id' AND " : "")."r.spr_id=v.spr_id AND r.usr_id=".$usr_id." AND r.vre_id=v.id AND r.spr_id=".$spremenljivke['id']." AND hidden='0' ORDER BY r.vrstni_red";
				//echo $sqlOdgovoriRespondentaString."</br>";
				$sqlOdgovoriRespondenta = sisplet_query($sqlOdgovoriRespondentaString);
				//pregled vseh odgovorov respondenta razvrsceni kot morajo biti
				while ($rowOdgovoriRespondenta = mysqli_fetch_assoc($sqlOdgovoriRespondenta)){
					$odgovorRespondenta = LatexDocument::encodeText($rowOdgovoriRespondenta['naslov'], $rowOdgovoriRespondenta['id']);					
					array_push($odgovoriRespondenta, $odgovorRespondenta);	//filanje polja z odgovori respondenta
				}
				
				//pregled vseh odgovorov respondenta razvrsceni kot morajo biti
				#ureditev polja s podatki trenutnega uporabnika - konec ##############################################
			}
			//izris tabel dolocenega razvrscanja
			if($export_data_type==2){	//ce je kratek izpis izvoza
				$tex .= $this->IzrisRazvrscanjaKratko($spremenljivke, $steviloDesnihOkvirjev, $numRowsSql, $navpicniOdgovori, $texNewLine, $texNewLineAfterTable, $export_format, 0, $tipRazvrscanja, $odgovoriRespondenta, $export_subtype);
			}elseif($export_data_type==0||$export_data_type==1){ //ce je navaden ali dolg izpis izvoza
				if($tipRazvrscanja==0||$tipRazvrscanja==2){	//ce je Prestavljanje ali Premikanje
					$tex .= $this->IzrisRazvrscanjaTabele($spremenljivke, $steviloDesnihOkvirjev, $numRowsSql, $navpicniOdgovori, $texNewLine, $texNewLineAfterTable, $export_format, 0, $tipRazvrscanja, $odgovoriRespondenta, $export_subtype);
				}elseif($tipRazvrscanja==1){	//ce je Ostevilcevanje
					$tex .= $this->IzrisRazvrscanja($spremenljivke, $numRowsSql, $navpicniOdgovori, $odgovoriRespondenta, $texNewLine, $export_format, 0);
				}elseif($tipRazvrscanja==3){	//ce je Image hotspot
					$tex .= $this->IzrisRazvrscanjaImageHotSpot($spremenljivke, $export_data_type, $odgovoriRespondenta, $texNewLine);
				}
			}
			//izris tabel dolocenega razvrscanja - konec
						
			
			if($export_data_type!=2){	//ce ni skrcen izpis izvoza
				if($tipRazvrscanja==1){ //ce je Ostevilcevanje
					$tex .= $this->texNewLine;
					$tex .= $this->texNewLine;
				}else{
					$tex .= $this->texBigSkip;
				}
			}
			//echo "latex koda: $tex";
			/* $time_end = microtime(true);
			$execution_time = ($time_end - $time_start);
			echo '<b>Total Execution Time razvrščanje:</b> '.$execution_time.' sec'; */
			return $tex;
		}
	}


	#funkcija, ki skrbi za izris ustreznih tabel za razvrscanje (postavitev: Prestavljanje in Premikanje) ################################
	function IzrisRazvrscanjaTabele($spremenljivke=null, $steviloDesnihOkvirjev=null, $steviloVrstic=null, $navpicniOdgovori=null, $texNewLine='', $texNewLineAfterTable=null, $typeOfDocument=null, $fillablePdf=null, $tipRazvrscanja=null, $odgovoriRespondenta=null, $export_subtype=null){
		global $lang;
		
		//izpis kode tabela
		$tabela = '';
		
		if($tipRazvrscanja==0){	//ce je postavitev Prestavljanje
			#pred zacetkom tabel s kategorijami #######################################################################
			$tabela .= '\setlength{\parindent}{0.1\textwidth} ';			
			//prva vrstica pred tabelo z odgovori
			if($typeOfDocument == 'pdf'){	//ce je pdf				
				$tabela .= '\begin{xltabular}{\textwidth}{l c l} ';	//izris s tabelo xltabular
				$tabela .= $lang['srv_ranking_available_categories'].': & \hspace{0.1\textwidth} & '.$lang['srv_ranking_ranked_categories1'].': '.$texNewLine;
				$tabela .= '\rule{0.4\textwidth}{0.7 pt} &  & \rule{0.4\textwidth}{0.4 pt} \end{xltabular} ';				
			}else{	//ce je rtf				
				$tabela .= '\begin{tabular}{l c l} ';	//izris s tabelo				
				$tabela .= $lang['srv_ranking_available_categories'].': & & '.$lang['srv_ranking_ranked_categories1'].': '.$texNewLine;				
				$tabela .= '\rule{0.4\textwidth}{0.7 pt} &  & \rule{0.4\textwidth}{0.4 pt} \end{tabular} ';				
			}
			//prva vrstica pred tabelo z odgovori - konec
			#pred zacetkom tabel s kategorijami  - konec ###############################################################
			
			$parameterTabularL = 'ccc';	//parameter za celotno tabelo z levimi in desnimi okvirji odgovorov za Prestavljanje
		}
		
		
		$tableCentering = ($typeOfDocument == 'pdf' ? ' \centering ' : '');
		
		if($tipRazvrscanja==2){	//ce je postavitev Premikanje
			$parameterTabularL = 'c';	//parameter za celotno tabelo z levimi in desnimi okvirji odgovorov za Premikanje
			if($typeOfDocument == 'pdf'){
				$tabela .= '\begin{center}';	//naj bo tabela na sredini lista, zacetek obmocja za center
			}
		}
		
		#################################################
		//zacetek tabele
		$tabela .= $this->StartLatexTable($typeOfDocument, $parameterTabularL, 'xltabular', 'tabular', 1, 0.2);
				
		//argumenti za leve okvirje		
		$textboxWidthL = 0.25;		
		$textboxAllignmentL = 'c';
		$indeksZaStevilaL=1;
		$indeksZaStevilaD=1;
		
		//if($tipRazvrscanja==0||($tipRazvrscanja==2&&count($odgovoriRespondenta)==0)){ //ce je Prestavljanje ali Premikanje in ni podatkov respondenta
		if(($tipRazvrscanja==2&&count($odgovoriRespondenta)==0)){ //Premikanje in ni podatkov respondenta
			$steviloOdgovorov=count($navpicniOdgovori);
			$textboxHeightL = 0;
		}elseif($tipRazvrscanja==2&&count($odgovoriRespondenta)!=0){	//ce je postavitev Premikanje in imamo odgovore respondenta
			$steviloOdgovorov=count($odgovoriRespondenta);
			$textboxHeightL = 0;	//ker mora biti prilagojena visina tekstu damo na 0
		}elseif($tipRazvrscanja==0){ //ce je Prestavljanje
			//$steviloOdgovorov=count($navpicniOdgovori)+$steviloDesnihOkvirjev;
			$steviloOdgovorov=$steviloDesnihOkvirjev;
			$textboxHeightL = 0;
		}
		

		//izris notranjosti tabele
		for ($i = 1; $i <= $steviloOdgovorov; $i++){
			if(array_key_exists($i-1, $navpicniOdgovori)){
				$textL = $tableCentering.' '.$navpicniOdgovori[$i-1];	//odgovor znotraj okvirja
			}
			if($tipRazvrscanja==2){	//ce je postavitev Premikanje
				
				$tabela .= $indeksZaStevilaL.'. ';	//pred okvirjem s kategorijo odgovora dodaj stevilko s piko

			}elseif($tipRazvrscanja==0&&$typeOfDocument == 'rtf'){
				//$tabela .= '\begin{tabular}{c} ';	//izris s tabelo brez obrob
			}

			//izpis latex kode za okvir z odgovorom
			if($tipRazvrscanja==0||($tipRazvrscanja==2&&count($odgovoriRespondenta)==0)){ //ce je Prestavljanje ali Premikanje in ni podatkov respondenta
				if(array_key_exists($i-1, $navpicniOdgovori)&&($navpicniOdgovori[$i-1]!='')){	//ce so prisotni odgovori
					$textVOkvirju = $textL;
				}
			}elseif($tipRazvrscanja==2&&count($odgovoriRespondenta)!=0){	//ce je postavitev Premikanje in imamo odgovore respondenta				
				$textVOkvirju = $odgovoriRespondenta[$i-1];
			}

			//echo "text V Okvirju: ".$textVOkvirju."</br>";
			
			//izpis latex kode za okvir z odgovorom
			if($tipRazvrscanja==0&&(array_key_exists($i-1, $navpicniOdgovori)&&$navpicniOdgovori[$i-1]!='')){
				if($typeOfDocument == 'pdf'){	//ce je pdf
					$tabela .= $this->LatexTextBox($typeOfDocument, $textboxHeightL, $textboxWidthL, $textVOkvirju, $textboxAllignmentL, 0);
				}else{
					$tabela .= $textVOkvirju;
				}
			}elseif($tipRazvrscanja==2||$tipRazvrscanja==1){
				if($typeOfDocument == 'pdf'){	//ce je pdf
					$tabela .= $this->LatexTextBox($typeOfDocument, $textboxHeightL, $textboxWidthL, $textVOkvirju, $textboxAllignmentL, 0);
				}else{
					$tabela .= $textVOkvirju;
				}
			} 
			
			if($typeOfDocument == 'pdf'){
				$tabela .= ' \bigskip ';							
			}

			if($tipRazvrscanja==2){ //ce je Premikanje				
				$tabela .= $texNewLine;				
			}

			################
			if($tipRazvrscanja==0){	//ce je postavitev Prestavljanje
				//prazen prostor med levim delom in desnim delom 
				$tabela .= '& \hspace{0.2\textwidth} &';	
				
				//desni del vprasanja
				$textboxWidthDE = 0.25;	//sirina okvirja z vsebino in empty
				$textboxAllignmentDE = 'c';	//allignment desnega okvirja, ki je empty
				if($indeksZaStevilaD <= $steviloDesnihOkvirjev){ //ce se ni preseglo zeleno stevilo desnih okvirjev					
					$tabela .= $indeksZaStevilaD.'. ';
					$odgovorZaIzpis = isset($odgovoriRespondenta[$i-1])?$odgovoriRespondenta[$i-1]:null;
					if($typeOfDocument == 'pdf'){
						//echo "odgovori respondenta na desni: ".$odgovoriRespondenta[$i-1]."</br>";
						if($odgovorZaIzpis){	//ce je odgovor respondenta
							$textboxHeight = 0; //ker mora biti prilagojena visina tekstu damo na 0
						}else{
							$textboxHeight = '0.3cm';
						}
						//izpis latex kode za okvir brez besedila oz. z odgovorom respondenta						
						$tabela .= $this->LatexTextBox($typeOfDocument, $textboxHeight, $textboxWidthDE, $odgovorZaIzpis, $textboxAllignmentDE, 0);
						$tabela .= $texNewLine;					
					}elseif($typeOfDocument == 'rtf'){											
						//izpis latex kode za okvir brez besedila oz. z odgovorom respondenta						
						$tabela .= $this->LatexTextBox($typeOfDocument, 0, $textboxWidthDE, $odgovorZaIzpis, $textboxAllignmentDE, 0);
						$tabela .= $texNewLine;
					}
		
					$indeksZaStevilaD++;
				}else{	//ce se je preseglo stevilo zelenih okvirjev na desni strani, izpisi prazno celico
					$tabela .= ' '.$texNewLine;				
				}				
			}
			################
			$indeksZaStevilaL++;

		}
				
		//$tabela .= ' \bigskip ';
		
		
		//if(count($navpicniOdgovori)==0){	//ce ni odgovorov na desni strani, uredi prazen neviden okvir
		if(count($navpicniOdgovori)==0 && $typeOfDocument == 'pdf'){	//ce ni odgovorov na desni strani, uredi prazen neviden okvir
			$tabela .= $this->LatexTextBox($typeOfDocument, $textboxHeightL, $textboxWidthL, '', $textboxAllignmentL, 1);
		}
			
		//zakljucek tabele
		$tabela .= $this->EndLatexTable($typeOfDocument, 'xltabular', 'tabular');
		#################################################
					
		if($tipRazvrscanja==2){	//ce je postavitev Premikanje
			if($typeOfDocument == 'pdf'){
				$tabela .= '\end{center}';	//naj bo tabela na sredini lista, konec obmocja za center
			}			
		}
		//izpis kode tabela - konec
		
		return $tabela;
	}
	#funkcija, ki skrbi za izris ustreznih tabel za razvrscanje (postavitev: Prestavljanje in Premikanje) - konec ################################
	
	#funkcija, ki skrbi za izris ustreznih tabel za razvrscanje (postavitev: Prestavljanje in Premikanje) ################################

	#funkcija, ki skrbi za izris razvrscanja (postavitev: Ostevilcevanje) ################################
	function IzrisRazvrscanja($spremenljivke=null, $steviloVrstic=null, $navpicniOdgovori=null, $odgovoriRespondenta=null, $texNewLine='', $typeOfDocument=null, $fillablePdf=null){
		$tex = '';
		$textboxWidth = 0.1;
		$textboxHeight = '0.2cm';
		$textboxAllignment = 'c';	//dummy spremenljivka
		$odgovorRespondenta = null;

		if($typeOfDocument == 'rtf'){	//ce je rtf, zacetek tabele, kjer sta dva stolpca (prazen okvir + okvir z odgovorom)
			//$tex .= '\begin{tabular}{l l} ';	//izris z enostolpicno tabelo
			$tex .= '\begin{tabular}{c l} ';	//izris z enostolpicno tabelo
		}		
		for ($i = 1; $i <= $steviloVrstic; $i++){
			$tex .= ' \noindent ';	//da ni premika besedila v desno			
			
			//izpis latex kode za prazen okvir oz. okvirjem z ustreznim stevilskim odgovorom
			//$tex .= $this->LatexTextBox($typeOfDocument, $textboxHeight, $textboxWidth, $odgovoriRespondenta[$i-1], $textboxAllignment, 0);
			if(array_key_exists($i-1, $odgovoriRespondenta)){
				$odgovorRespondenta = $odgovoriRespondenta[$i-1];
			}
			//$tex .= $this->LatexTextBox($typeOfDocument, $textboxHeight, $textboxWidth, '\\textcolor{crta}{'.$odgovoriRespondenta[$i-1].'}', $textboxAllignment, 0);
			$tex .= $this->LatexTextBox($typeOfDocument, $textboxHeight, $textboxWidth, '\\textcolor{crta}{'.$odgovorRespondenta.'}', $textboxAllignment, 0);
			
			if($typeOfDocument == 'rtf'){	//ce je rtf
				$tex .= ' & ';	//meja med stolpcema tabele 1. prazen okvir (okvir s stevilskim odgovorom), 2. navpicni odgovor
			}
			
			//odgovor ob praznem okvirju
			$tex .= ' '.$navpicniOdgovori[$i-1];
			
			//v novo vrstico
			$tex .= $texNewLine;
		}
		
		if($typeOfDocument == 'rtf'){		// ce je rtf, zakljuci tabelo s stolpcema
			$tex .= ' \end{tabular} ';
		}
		return $tex;		
	}	
	#funkcija, ki skrbi za izris razvrscanja (postavitev: Ostevilcevanje) - konec ################################
	
	#funkcija, ki skrbi za izris razvrscanje za kratek izpis izvoza ################################
	function IzrisRazvrscanjaKratko($spremenljivke=null, $steviloDesnihOkvirjev=null, $steviloVrstic=null, $navpicniOdgovori=null, $texNewLine='', $texNewLineAfterTable=null, $typeOfDocument=null, $fillablePdf=null, $tipRazvrscanja=null, $odgovoriRespondenta=null, $export_subtype=null){
		global $lang;
		
		$indeksZaStevila=1;
		$steviloOdgovorov=count($navpicniOdgovori);
		$steviloOdgovorov=count($odgovoriRespondenta);
		$tex = '';
		
		//izpis stevil in odgovorov
		for ($i = 1; $i <= $steviloOdgovorov; $i++){
			$navpicniOdgovor = null;
			if(array_key_exists(($i-1), $navpicniOdgovori)){
				$navpicniOdgovor = $navpicniOdgovori[$i-1];
			}
			if($tipRazvrscanja==0||$tipRazvrscanja==2||$tipRazvrscanja==3){ //ce je Prestavljanje ali Premikanje ali image hotspot
				$tex .= $indeksZaStevila.'. ';	//stevilka pred odgovorom
				$tex .= '\\textcolor{crta}{'.$odgovoriRespondenta[$i-1].'}';	//odgovor
			}elseif($tipRazvrscanja==1){	//ce je Ostevilcevanje 
				$tex .=$navpicniOdgovor.': ';
				$tex .= '\\textcolor{crta}{'.$odgovoriRespondenta[$i-1].'}';	//odgovor				
			}			
			$tex .= '; ';
			$indeksZaStevila++;
		}
		
		$tex .= ' \\\\ ';
		$tex .= ' \\\\ ';
		
		return $tex;
	}
	#funkcija, ki skrbi za izris razvrscanje za kratek izpis izvoza - konec ################################

	#funkcija, ki skrbi za izris razvrscanje za kratek izpis izvoza ################################
	function IzrisRazvrscanjaImageHotSpot($spremenljivke=null, $export_data_type=null, $odgovoriRespondenta=null, $texNewLine=''){
		global $lang;
		$indeksZaStevila=1;
		
		$steviloOdgovorov=count($odgovoriRespondenta);
		$tex = '';
		$imageName = LatexDocument::getImageName('hotspot', $spremenljivke['id'], 'hotspot_image=');
		$imageNameTest = $this->path2ImagesRadio.$imageName.'.png';	//za preveriti, ali obstaja slikovna datoteka na strezniku
		
		//echo("za image hot spot ne grid: ".$imageNameTest."</br>");
		if(filesize($imageNameTest) > 0){
			$image = PIC_SIZE."{".$this->path2ImagesRadio."".$imageName."}";	//priprave slike predefinirane dimenzije			
		}else{
			$image = $lang['srv_pc_unavailable'];
		}
		
		$tex .= $image."".$texNewLine; //izris slike

		//iz baze poberi imena obmocij
		$sqlHotSpotRegions = sisplet_query("SELECT region_name FROM srv_hotspot_regions WHERE spr_id='".$spremenljivke['id']."' ORDER BY vrstni_red");	
		
		//izris imen obmocij po $sqlHotSpotRegions
		$tex .= $lang['srv_export_hotspot_regions_names'].': '.$texNewLine;	//besedilo "Obmocja na sliki"
		while ($rowHotSpotRegions = mysqli_fetch_assoc($sqlHotSpotRegions))
		{
			$tex .= $rowHotSpotRegions['region_name'].''.$texNewLine;
		}
			
		if(count($odgovoriRespondenta)){
			$tex .= $lang['srv_ranking_ranked_categories1'].": ".$texNewLine;
			//izpis odgovorov
			for ($i = 1; $i <= $steviloOdgovorov; $i++){
				$tex .= '\\textcolor{crta}{'.$odgovoriRespondenta[$i-1].'}';	//odgovor		
				$tex .= ' \\\\ ';
				$indeksZaStevila++;
			}
		}
		
		$tex .= ' \\\\ ';
		$tex .= ' \\\\ ';
		
		return $tex;
	}
	#funkcija, ki skrbi za izris razvrscanje za kratek izpis izvoza - konec ################################
	
}