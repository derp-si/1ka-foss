<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CrossRoad
 *
 * @author podkrizniku
 */

class CrossRoad {

    static function MainNavigation($anketa=null){

        // Status za tracking sprememb
        $status = -1;

        // Lokacija v navigaciji
        $first_action = '';
        $second_action = '';
        $third_action = '';

        // Layout strani glede na to kje se nahajamo (imamo levi meni in sirina holderja)
        $layout_menu = '';
        $layout_width = '';
        $hide_header = '';

        # ajaxa se poslje skupaj z ajaxom, da ob updatu vemo kaksen 'a' je bil na originalni strani
        # (drugace se ob updatu z ajaxom informacija o 'a'ju zgubi)
        $get = $_GET['a'];

        if (isset ($_GET['ajaxa']))
            $get = $_GET['ajaxa'];

        if ($get === null || trim($get) == '')
            $get = A_BRANCHING;


        //smo vezani na anketo
        if($anketa != null && $anketa > 0){

            switch ($get) {

                # STATUS
                case A_REPORTI:
                    $first_action = NAVI_STATUS;
                    $second_action = NAVI_STATUS_OSNOVNI;

                    $layout_width = 'wide';

                    $status = 3;

                    break;

                case A_NONRESPONSE_GRAPH:
                case A_USABLE_RESP:
                case A_KAKOVOST_RESP:
                case A_SPEEDER_INDEX:
                case A_TEXT_ANALYSIS:
                case A_GEOIP_LOCATION:
                case A_EDITS_ANALYSIS:
                case A_REMINDER_TRACKING:
                case A_UL_EVALVATION:
                case A_PARA_GRAPH:
                case 'langStatistic':
                case 'AAPOR':
                case A_STATUS_ADVANCED:
                    $first_action = NAVI_STATUS;
                    $second_action = NAVI_STATUS_OSNOVNI;

                    $layout_width = 'wide';
                    if ($_GET['m'] == 'advanced')
                        $layout_width = 'fullwidth';

                    $status = 4;

                    break;


                # UREJANJE ANKETE
                case A_BRANCHING:
                case A_GLASOVANJE:
                    $first_action = NAVI_UREJANJE;
                    $second_action = NAVI_UREJANJE_BRANCHING;

                    $layout_width = 'fullwidth';

                    $status = 0;

                    break;


                # TESTIRANJE
                case A_TESTIRANJE:
                    $first_action = NAVI_TESTIRANJE;
                    $second_action = M_TESTIRANJE_DIAGNOSTIKA;
                    if ($_GET['m'] == M_TESTIRANJE_VNOSI) {
                        $second_action = NAVI_TESTIRANJE_VNOSI;
                    }
                    if ($_GET['m'] == M_TESTIRANJE_PREDVIDENI) {
                        $second_action = NAVI_TESTIRANJE_PREDVIDENI;
                    }
                    if ($_GET['m'] == M_TESTIRANJE_CAS) {
                        $second_action = NAVI_TESTIRANJE_CAS;
                    }

                    $layout_width = 'narrow';

                    if (isset($_GET['m']) && ($_GET['m'] == 'cas' || $_GET['m'] == 'predvidenicas')) {
                        $layout_width = 'wide';

                    }

                    $status = 4;

                    break;

        
                # NASTAVITVE ANKETE
                case A_KOMENTARJI:
                case A_KOMENTARJI_ANKETA:
                    $first_action = NAVI_TESTIRANJE;
                    $second_action = NAVI_TESTIRANJE_KOMENTARJI;

                    $layout_width = 'narrow';
                    if (isset($_GET['a']) && ($_GET['a'] == 'komentarji' || $_GET['a'] == 'komentarji_anketa'))
                        $layout_width = 'wide';

                    $layout_menu = '';

                    $status = 0;

                    break;
                    
                case A_SETTINGS:
                case A_OSNOVNI_PODATKI:
                case A_FORMA:
                case A_COOKIE:
                case A_TRAJANJE:
                case A_DOSTOP:
                case A_MISSING:
                case A_METADATA:
                case A_MOBILESETTINGS:
                case A_TABLESETTINGS:
                case A_JEZIK: # nastavitve jezik
                case A_UREJANJE: # nastavitve komentarjev
                case A_PRIKAZ: # nastavitve komentarjev
                case A_SKUPINE:
                case A_EXPORTSETTINGS:
                case A_GDPR:
                    $first_action = NAVI_UREJANJE;
                    $second_action = NAVI_UREJANJE_ANKETA;

                    $layout_width = 'wide';
                    $layout_menu = 'menu_left';

                    $status = 0;

                    break;

                case A_TEMA: # nastavitve prevajanje
                case 'theme-editor': # nastavitve prevajanje
                case 'edit_css': # nastavitve prevajanje
                    $first_action = NAVI_UREJANJE;
                    $second_action = NAVI_UREJANJE_TEMA;

                    $layout_width = 'wide';
                    if($_GET['t'] == 'upload')
                        $layout_width = 'narrow';

                    $status = 0;

                    break;

                case A_HIERARHIJA:
                    $first_action = NAVI_HIERARHIJA;

                    $layout_width = 'wide';
                    $layout_menu = 'menu_left';

                    break;

                case A_PREVAJANJE: # nastavitve prevajanje
                    $first_action = NAVI_UREJANJE;
                    $second_action = NAVI_UREJANJE_PREVAJANJE;

                    $layout_width = 'fullwidth';

                    $status = 0;

                    break;

                case A_ALERT:
                    $first_action = NAVI_UREJANJE;
                    $second_action = NAVI_UREJANJE_ANKETA;

                    $layout_width = 'wide';
                    $layout_menu = 'menu_left';

                    $status = 0;

                    break;

                case A_NAGOVORI:
                    $first_action = NAVI_UREJANJE;

                    $layout_width = 'wide';
                    $layout_menu = 'menu_left';

                    $status = 0;

                    break;


                # ARHIVI
                case A_ARHIVI:
                    $first_action = ($_GET['m'] == 'data') ? NAVI_RESULTS : NAVI_UREJANJE;  
                    $second_action = NAVI_ARHIVI;

                    if(isset($_GET['m']) && $_GET['m'] == 'survey')
                        $third_action = NAVI_UREJANJE_ARHIVI_EXPORT1;
                    elseif(isset($_GET['m']) && $_GET['m'] == 'survey_data')
                        $third_action = NAVI_UREJANJE_ARHIVI_EXPORT2;
                    elseif(!isset($_GET['m']) || $_GET['m'] != 'data')
                        $third_action = NAVI_UREJANJE_ARHIVI;

                    $layout_width = 'wide';
                    $layout_menu = 'menu_left';


                    $status = 0;

                    break;

                case A_TRACKING:
                    $first_action = NAVI_UREJANJE;
                    $second_action = NAVI_ARHIVI;

                    if(isset($_GET['appendMerge']) && $_GET['appendMerge'] == '1')
                        $third_action = NAVI_UREJANJE_ARHIVI_TRACKING3;
                    elseif($_GET['m'] == 'tracking_data')
                        $third_action = NAVI_UREJANJE_ARHIVI_TRACKING2;
                    else
                        $third_action = NAVI_UREJANJE_ARHIVI_TRACKING1;

                    $layout_width = 'wide';
                    $layout_menu = 'menu_left';

                    $status = 0;

                    break;


                # OBJAVA, VABILA
                case A_VABILA:
                    $first_action = NAVI_OBJAVA;
                    $_GET['m'] == 'settings' ? $second_action = NAVI_OBJAVA_SETTINGS : (isset($_GET['m']) && $_GET['m'] == 'url' ? $second_action = NAVI_OBJAVA_URL : $second_action = '');

                    $layout_width = 'narrow';

                    $status = 5;

                    break;

                case A_EMAIL:
                    $first_action = NAVI_OBJAVA;
                    $second_action = NAVI_OBJAVA;

                    $layout_width = 'wide';

                    $status = 5;

                    break;

                case 'invitations':
                    $first_action = NAVI_OBJAVA;
                    $second_action = (isset($_GET['m']) && $_GET['m'] == 'view_archive') ? NAVI_ARHIVI : 'invitations';  

                    $layout_width = 'wide';
                    
                    if ($_GET['m'] == 'view_archive')
                        $layout_menu = "menu_left";

                    $status = 5;

                    break;

                case A_OTHER_INV:
                    $first_action = NAVI_OBJAVA;
                    $second_action = (isset($_GET['m']) && $_GET['m'] == 'view_archive') ? NAVI_ARHIVI : NAVI_OBJAVA;  

                    $layout_width = 'narrow';
                    
                    if ($_GET['m'] == 'view_archive')
                        $layout_menu = "menu_left";

                    $status = 5;

                    break;


                # ANALIZE, PODATKI
                case A_ANALYSIS:
                    $first_action = NAVI_ANALYSIS;

                    $second_action = NAVI_STATISTIC_ANALYSIS;
                    if ($_GET['m'] == M_ANALYSIS_LINKS) {
                        $second_action = NAVI_ANALYSIS_LINKS;
                    }
                    elseif($_GET['m'] == 'anal_arch'){
                        $second_action = NAVI_ARHIVI;  
                    }
                    
                    if ($_GET['m'] == 'sumarnik') {
                        $third_action = M_ANALYSIS_SUMMARY;
                    }
                    elseif ($_GET['m'] == 'descriptor') {
                        $third_action = M_ANALYSIS_DESCRIPTOR;
                    }
                    elseif ($_GET['m'] == 'frequency') {
                        $third_action = M_ANALYSIS_FREQUENCY;
                    }
                    elseif ($_GET['m'] == 'crosstabs') {
                        $third_action = M_ANALYSIS_CROSSTAB;
                    }
                    elseif ($_GET['m'] == 'multicrosstabs') {
                        $third_action = M_ANALYSIS_MULTICROSSTABS;
                    }
                    elseif ($_GET['m'] == 'means') {
                        $third_action = M_ANALYSIS_MEANS;
                    }
                    elseif ($_GET['m'] == 'ttest') {
                        $third_action = M_ANALYSIS_TTEST;
                    }
                    elseif ($_GET['m'] == 'break') {
                        $third_action = M_ANALYSIS_BREAK;
                    }

        
                    if  ($_GET['m'] == 'charts') {
                        $layout_width = 'narrow';
                    }
                    else{
                        $layout_width = 'wide';
                    }
                    
                    if  ($_GET['m'] == 'anal_arch') {
                        $layout_menu = 'menu_left';
                    }

                    $status = 2;

                    break;

                case A_COLLECT_DATA:
                    $first_action = NAVI_RESULTS;
                    $second_action = NAVI_DATA;

                    $layout_width = 'fullwidth';

                    if ($_GET['m'] == M_COLLECT_DATA_CALCULATION) {
                        $second_action = NAVI_DATA_CALC;
                        $third_action = NAVI_DATA_CALC_CALCULATION;

                        $layout_width = 'wide';
                        $layout_menu = 'menu_left';
                    }
                    elseif($_GET['m'] == M_COLLECT_DATA_CODING){
                        $second_action = NAVI_DATA_CALC;  
                        $third_action = NAVI_DATA_CALC_CODING;  

                        $layout_width = 'wide';
                        $layout_menu = 'menu_left';
                    }
                    elseif($_GET['m'] == M_COLLECT_DATA_CODING_AUTO){
                        $second_action = NAVI_DATA_CALC;  
                        $third_action = NAVI_DATA_CALC_CODING_AUTO;  

                        $layout_width = 'wide';
                        $layout_menu = 'menu_left';
                    }
                    elseif($_GET['m'] == M_COLLECT_DATA_RECODING){
                        $second_action = NAVI_DATA_CALC;  
                        $third_action = NAVI_DATA_CALC_RECODING;  

                        $layout_width = 'wide';
                        $layout_menu = 'menu_left';
                    }

                    elseif(isset($_GET['m']) && ($_GET['m'] == M_COLLECT_DATA_APPEND || $_GET['m'] == 'upload_xls' || $_GET['m'] == 'append_xls' || $_GET['m'] == 'merge_xls')){
                        $second_action = NAVI_DATA_IMPORT;  
                        $third_action = NAVI_DATA_IMPORT_APPEND;  

                        $layout_width = 'wide';
                        $layout_menu = 'menu_left';
                    }
                    elseif(isset($_GET['m']) && $_GET['m'] == M_COLLECT_DATA_MERGE){
                        $second_action = NAVI_DATA_IMPORT;  
                        $third_action = NAVI_DATA_IMPORT_MERGE;  

                        $layout_width = 'wide';
                        $layout_menu = 'menu_left';
                    }
                    elseif  (isset($_GET['m']) && $_GET['m'] == 'quick_edit') {
                        $layout_width = 'wide';
                    }
                    elseif  (isset($_GET['m']) && $_GET['m'] == 'variables') {
                        $layout_width = 'wide';
                    }

                    $status = 4;

                    break;


                # IZVOZI
                case A_COLLECT_DATA_EXPORT:
                    $first_action = NAVI_RESULTS;
                    $second_action = NAVI_DATA_EXPORT;

                    if(isset($_GET['m']) && $_GET['m'] == M_EXPORT_EXCEL){
                        $third_action = NAVI_DATA_EXPORT_EXCEL;  
                    }
                    elseif(isset($_GET['m']) && $_GET['m'] == M_EXPORT_EXCEL_XLS){
                        $third_action = NAVI_DATA_EXPORT_EXCEL_XLS;  
                    }
                    elseif(isset($_GET['m']) && $_GET['m'] == M_EXPORT_SAV){
                        $third_action = NAVI_DATA_EXPORT_SAV;  
                    }
                    elseif(isset($_GET['m']) && $_GET['m'] == M_EXPORT_TXT){
                        $third_action = NAVI_DATA_EXPORT_TXT;  
                    }
                    else{
                        $third_action = NAVI_DATA_EXPORT_SPSS;  
                    }

                    $layout_width = 'wide';
                    $layout_menu = 'menu_left';

                    $status = 4;

                    if (isset($_GET['m']) && $_GET['m'] == A_COLLECT_DATA_EXPORT_ALL) {
                        $first_action = NAVI_RESULTS;
                        $second_action = NAVI_ANALYSIS_LINKS;
                        $third_action = ''; 

                        $status = 2;
                    }

                    break;


                # DODATNE NASTAVITVE
                case A_ADVANCED:
                case A_UPORABNOST:
                case A_HIERARHIJA_SUPERADMIN:
                case A_KVIZ:
                case A_VOTING:
                case A_ADVANCED_PARADATA:
                case A_ADVANCED_TIMESTAMPS:
                case A_JSON_SURVEY_EXPORT:
                case A_VNOS:
                case A_SOCIAL_NETWORK:
                case A_CHAT:
                case A_PANEL:
                case A_EMAIL_ACCESS:
                case A_SLIDESHOW:
                case A_360:
                case A_360_1KA:
                case A_MAZA:
                case A_WPN:
                case 'evoli':
                case 'evoli_teammeter':
                case 'evoli_quality_climate':
                case 'evoli_teamship_meter':
                case 'evoli_organizational_employeeship_meter':
                case 'evoli_employmeter':
                case 'mfdps':
                case 'borza':
                case 'mju':
                case 'excell_matrix':
                case 'fieldwork':
                    $first_action = NAVI_UREJANJE;
                    $second_action = NAVI_UREJANJE_ANKETA;

                    $layout_width = 'wide';
                    $layout_menu = 'menu_left';

                    $status = 0;

                    break;

                case A_TELEPHONE:
                case A_PHONE:
                case T_PHONE:
                    $first_action = NAVI_UREJANJE;
                    $second_action = NAVI_UREJANJE_ANKETA;

                    $layout_width = 'wide';

                    // Anektar (telefonska anketa) nima leve navigacije
                    global $global_user_id;
			        if(!Common::isUserAnketar($anketa, $global_user_id))
                        $layout_menu = 'menu_left';

                    $status = 5;

                    break;

                default:
                    break;
            }

            // shrani tracking za anketo
            if(!isDemoSurvey($anketa) 
                && $third_action != NAVI_UREJANJE_ARHIVI_TRACKING1 
                && $third_action != NAVI_UREJANJE_ARHIVI_TRACKING2 
                && $third_action != NAVI_UREJANJE_ARHIVI_TRACKING3
            ){
                TrackingClass::update($anketa, $status);
            }
        }
        // Nismo vezani na anketo, tracking uporabnika
        else{

            switch ($get) {

                # AKTIVNOST in UPORABNIKI
                case 'diagnostics':

                    // Uporabniki
                    if(isset($_GET['t']) && $_GET['t'] == 'uporabniki'){
                        $layout_width = 'narrow';
                        if(!isset($_GET['m']) || $_GET['m'] == 'all' || $_GET['m'] == ''){
                            $layout_width = 'fullwidth';  
                        } 
                    }
                    // Ativnost
                    else{
                        $layout_width = 'narrow';
                    }

                    break;

                
                # KNJIZNICA
                case 'knjiznica':
                    $layout_width = 'wide';  

                    break;


                # NASTAVITVE
                case 'nastavitve':
                    $layout_width = 'narrow';  

                    break;


                # OBVESTILA
                case 'obvestila':
                    $layout_width = 'narrow';  

                    break;


                # GDPR
                case 'gdpr':
                    if (isset($_GET['m']) && $_GET['m'] == 'gdpr_requests')
                        $layout_width = 'wide';  
                    elseif (isset($_GET['m']) && $_GET['m'] == 'gdpr_requests_all')
                        $layout_width = 'fullwidth';  
                    else
                        $layout_width = 'narrow';  

                    break;


                # MODUL UL EVALVACIJE
                case 'ul_evalvation':
                    $layout_width = 'wide';  

                    break;

                    
                # MOJE ANKETE
                case 'pregledovanje':
                default:
                    $layout_width = 'wide';
                    
                    if ((isset($_GET['a']) && $_GET['a'] == 'ustvari_anketo') || (isset($_GET['b']) && $_GET['b'] == 'new_survey')) {
                        $layout_width = 'fullwidth';
                        $layout_menu = 'menu_left';
                        $hide_header = 'hide_header';
                    }

                    if ($_GET['a'] == 'narocila') {
                        $layout_width = 'narrow';
                    }

                    break;
            }

            // shrani tracking za userja
            TrackingClass::update_user();
        }

        // vrni podatke o navigaciji nazaj v SurveyAdmin
        return array(
            'first_action'  => $first_action, 
            'second_action' => $second_action, 
            'third_action'  => $third_action, 

            'layout_width'  => $layout_width,
            'layout_menu'   => $layout_menu,
            'hide_header'   => $hide_header,
        ); 
    }
}
