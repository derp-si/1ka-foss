<?php

/** Pomožen class
 *
 * Enter description here ...
 * @author veselicg
 *
 */
class SurveyStaticHtml
{
    private $sid = null;


    public function __construct($anketa){
        global $global_user_id;

        $this->sid = $anketa;

        SurveyUserSetting:: getInstance()->Init($anketa, $global_user_id);
    }


    # Nastavitve multicrosstab tabele
    public function displayMulticrosstabSettings(){
        global $lang;
        global $global_user_id;

        $this->table_id = SurveyUserSetting:: getInstance()->getSettings('default_mc_table');

        /*if (isset($this->table_id) && $this->table_id != '')
            $sql = sisplet_query("SELECT name FROM srv_mc_table WHERE id='$this->table_id' AND ank_id='$this->sid' AND usr_id='$global_user_id'");
        else
            $sql = sisplet_query("SELECT name FROM srv_mc_table WHERE ank_id='$this->sid' AND usr_id='$global_user_id' ORDER BY time_created ASC");
        $row = mysqli_fetch_array($sql);*/


        echo '<div class="mc_settings_links">';


        // Seznam tabel
        echo '<div class="table_list">';

        $sql = sisplet_query("SELECT id, name FROM srv_mc_table WHERE ank_id='$this->sid' AND usr_id='$global_user_id' ORDER BY time_created ASC");
        while($row = mysqli_fetch_array($sql)){
            echo '<div class="table_item '.($row['id'] == $this->table_id ? 'active' : '').'">';
            echo '  <span onClick="switch_mc_table(\''.$row['id'].'\');">'.$row['name'].'</span>';
            echo '</div>';
        }

        echo '</div>';


        // Nastavtve, dodajanje tabele
        echo '<div class="table_settings">';

        // Urejanje
        echo '  <span class="faicon edit" onClick="show_mc_tables();"></span>';

        // Dodajanje
        echo '  <span id="mc_tables_plus" class="faicon add" title="'.$lang['srv_multicrosstabs_tables_add'].'"></span>';

        // Nastavitve tabele (popup)
        echo '  <span class="faicon wheel_32" onClick="showMCSettings();" title="'.$lang['srv_multicrosstabs_settings'].'"></span>';

        echo '</div>';


        echo '</div>';
    }

    function displayAnalizaPreview(){
        global $lang;

        $preview_files = array(
            1 => 'sumarnik.svg',
            2 => 'opisne.svg',
            3 => 'frekvence.svg',
            4 => 'tabela.svg',
            5 => 'povprecja.svg',
            6 => 'ttest.svg',
            7 => 'razbitje.svg',
            8 => 'multitabela.svg'
        );


        echo '<div id="srv_analiza_preview_div">';

        for($i=1; $i<9; $i++){

            echo '<div id="srv_analiza_preview_sub_'.$i.'" class="srv_analiza_preview_sub displayNone">';

            echo '<div class="title">';
            echo '  <span class="faicon table_icon"></span>'.$lang['srv_analize_preview_'.$i];
            echo '</div>';
            
            echo '<div class="content">';
            if($i != 7)
                echo '  <img src="../../../public/img/analysis_previews/'.$preview_files[$i].'">';
            echo '</div>';

            echo '</div>';
        }

        echo '</div>';
    }

    public function displayArchiveNavigation()
    {
        global $lang, $admin_type, $global_user_id;
        
        $userAccess = UserAccess::getInstance($global_user_id);

        $d = new Dostop();

        //$sa = new SurveyAdmin();
        SurveyInfo::getInstance()->SurveyInit($this->sid);
        $this->survey_type = SurveyInfo::getInstance()->getSurveyColumn("survey_type");

        $a = isset($_GET['a']) ? $_GET['a'] : '';
        $m = isset($_GET['m']) ? $_GET['m'] : '';
        $appendMerge = isset($_GET['appendMerge']) ? $_GET['appendMerge'] : '';
        
        if(!$userAccess->checkUserAccess('arhivi')){
            echo '<div class="archive user_access_locked locked">';
        }
        //echo '<span class="menu_left-title title '.(!$userAccess->checkUserAccess('arhivi') ? 'user_access_locked' : '').'">'.$lang['srv_arhivi'].'</span>'; //POGLEJ
        echo '<span class="menu_left-title '.(!$userAccess->checkUserAccess('arhivi') ? 'user_access_locked locked' : '').'">'.$lang['srv_arhivi'].'</span>'; //POGLEJ

        //echo '<ul class="menu_left-list '.(!$userAccess->checkUserAccess('arhivi') ? 'user_access_locked' : '').' locked">';
        echo '<ul class="menu_left-list '.(!$userAccess->checkUserAccess('arhivi') ? 'user_access_locked locked' : '').' ">';

        # arhivi vprasalnika
        if ($d->checkDostopSub('edit')) {
            echo '<li' . ($a == A_ARHIVI && $m != 'data' && $m != 'changes' && $m != 'survey' && $m != 'survey_data' && $m != 'testdata' ? ' class="active"' : '') . '>';
            //echo '<a href="index.php?anketa=' . $this->sid . '&a=' . A_ARHIVI . '" title="' . $lang['srv_questionnaire_archives'] . '"><span>' . $lang['srv_questionnaire_archives'] . '</span></a>';
            echo '<a href="index.php?anketa=' . $this->sid . '&a=' . A_ARHIVI . '" title="' . $lang['srv_questionnaire_archives'] . '">' . $lang['srv_questionnaire_archives'] . '</a>';
            echo '</li>';
        }

        # arhivi podatkov
        if ($d->checkDostopSub('edit') && $this->survey_type > 0) {
            echo '<li' . ($a == A_ARHIVI && $m == 'data' ? ' class="active"' : '') . '>';
            echo '<a href="index.php?anketa=' . $this->sid . '&a=' . A_ARHIVI . '&m=data" title="' . $lang['srv_data_archives'] . '">' . $lang['srv_data_archives'] . '</a>';
            echo '</li>';
        }

        # arhivi objave
        if ($d->checkDostopSub('publish')) {
            echo '<li' . ($a == A_INVITATIONS && $m == 'view_archive' ? ' class="active"' : '') . '>';
            echo '<a href="index.php?anketa=' . $this->sid . '&a=' . A_INVITATIONS . '&m=view_archive' . '" title="' . $lang['srv_archive_invitation_menu'] . '">' . $lang['srv_archive_invitation_menu'] . '</a>';
            echo '</li>';
        }

        # arhivi analiz
        if ($d->checkDostopSub('analyse')) {
            echo '<li' . ($a == A_ANALYSIS && $m == M_ANALYSIS_ARCHIVE ? ' class="active"' : '') . '>';
            echo '<a href="index.php?anketa=' . $this->sid . '&a=' . A_ANALYSIS . '&m=' . M_ANALYSIS_ARCHIVE . '" title="' . $lang['srv_archive_analysis_menu'] . '">' . $lang['srv_archive_analysis_menu'] . '</a>';
            echo '</li>';
        }

        # arhivi testnih vnosov
        if ($this->survey_type > 1) {
            echo '<li' . ($a == A_ARHIVI && $m == 'testdata' ? ' class="active"' : '') . '>';
            echo '<a href="index.php?anketa=' . $this->sid . '&a=' . A_ARHIVI . '&m=testdata" title="' . $lang['srv_arhiv_testdata_menu'] . '">' . $lang['srv_arhiv_testdata_menu'] . '</a>';
            echo '</li>';
        }

        echo '</ul>';
        if(!$userAccess->checkUserAccess('arhivi')){
            echo '</div>';
        }

        # uvoz/izvoz ankete ali ankete s podatki
        if ($d->checkDostopSub('edit')) {
            echo '<span class="menu_left-title '.(!$userAccess->checkUserAccess('arhivi') ? 'user_access_locked' : '').'">'.$lang['srv_survey_archives_title'].'</span>';

            echo '<ul class="menu_left-list" id="sub_navi_tracking">';

            // Uvoz/izvoz ankete
            echo '<li ' . ($a == A_ARHIVI && $m == 'survey' && $appendMerge != '1' ? ' class="active"' : ' ') . '>';
            echo '<a href="index.php?anketa=' . $this->sid . '&a=' . A_ARHIVI . '&m=survey" title="' . $lang['srv_survey_archives_ie'] . '">' . $lang['srv_survey_archives_ie'] . '</a>';
            echo '</li>';

            // Uvoz/izvoz ankete in podatkov
            echo '<li ' . ($a == A_ARHIVI && $m == 'survey_data' ? ' class="active"' : '') . '>';
            echo '<a href="index.php?anketa=' . $this->sid . '&a=' . A_ARHIVI . '&m=survey_data" title="' . $lang['srv_survey_archives_ie_data'] . '">' . $lang['srv_survey_archives_ie_data'] . '</a>';
            echo '</li>';

            echo '</ul>';
        }

        # arhivi sprememb
        if ($d->checkDostopSub('edit')) {
            echo '<span class="menu_left-title '.(!$userAccess->checkUserAccess('arhivi') ? 'user_access_locked' : '').'">'.$lang['srv_survey_archives_tracking'].'</span>';

            echo '<ul class="menu_left-list" id="sub_navi_tracking">';

            // Vse spremembe ankete
            echo '<li ' . ($a == A_TRACKING && $m != 'tracking_data' && $appendMerge != '1' ? ' class="active"' : '') . '>';
            echo '<a href="index.php?anketa=' . $this->sid . '&a=' . A_TRACKING . '" title="' . $lang['srv_survey_archives_tracking_survey_menu'] . '">' . $lang['srv_survey_archives_tracking_survey_menu'] . '</a>';
            echo '</li>';

            $hierarhija = false;
            if (SurveyInfo::getInstance()->checkSurveyModule('hierarhija')) {
                $uporabnik = sisplet_query("SELECT type FROM srv_hierarhija_users WHERE anketa_id='".$this->sid."' AND user_id='".$global_user_id."'", "obj");
                if (!empty($uporabnik) && $uporabnik->type == 1)
                    $hierarhija = true;
            }

            if ($hierarhija) {

                // Vsi podatki o gradnji hierarhije, šifrantov in ostalega
                echo '<li ' . ($a == A_TRACKING_HIERARHIJA && $m == 'hierarhija' ? ' class="active"' : '') . '>';
                echo '<a href="index.php?anketa=' . $this->sid . '&a=' . A_TRACKING_HIERARHIJA. '&m=hierarhija" title="' . $lang['srv_survey_archives_tracking_hierarchy_structure'] . '">' . $lang['srv_survey_archives_tracking_hierarchy_structure'] . '</a>';
                echo '</li>';

                // Vse spremembe pri dodajanju udeležencev
                echo '<li ' . ($a == A_TRACKING_HIERARHIJA && $m == 'udelezenci' ? ' class="active"' : '') . '>';
                echo '<a href="index.php?anketa=' . $this->sid . '&a=' . A_TRACKING_HIERARHIJA . '&m=udelezenci" title="' . $lang['srv_survey_archives_tracking_hierarchy_users'] . '">' . $lang['srv_survey_archives_tracking_hierarchy_users'] . '</a>';
                echo '</li>';
            }

            // Spremembe na podatkih
            echo '<li ' . ($a == A_TRACKING && $m == 'tracking_data' ? ' class="active"' : '') . '>';
            echo '<a href="index.php?anketa=' . $this->sid . '&a=' . A_TRACKING . '&m=tracking_data" title="' . $lang['srv_survey_archives_tracking_data_menu'] . '">' . $lang['srv_survey_archives_tracking_data_menu'] . '</a>';
            echo '</li>';

            // Append/Merge (uvozi)
            echo '<li ' . ($a == A_TRACKING && $appendMerge == '1' ? ' class="active"' : '') . '>';
            echo '<a href="index.php?anketa=' . $this->sid . '&a=' . A_TRACKING . '&appendMerge=1" title="' . $lang['srv_survey_archives_tracking_append'] . '">' . $lang['srv_survey_archives_tracking_append'] . '</a>';
            echo '</li>';
        }
        echo '</ul>';

    }

    public function displayTestDataBar($showAnalizeCheckbox = false)
    {
        global $lang;

        $str_testdata = "SELECT count(*) FROM srv_user WHERE ank_id='" . $this->sid . "' AND (testdata='1' OR testdata='2') AND deleted='0'";
        $query_testdata = sisplet_query($str_testdata);
        list($testdata) = mysqli_fetch_row($query_testdata);

        $str_autogen_testdata = "SELECT count(*) FROM srv_user WHERE ank_id='" . $this->sid . "' AND testdata='2' AND deleted='0'";
        $query_autogen_testdata = sisplet_query($str_autogen_testdata);
        list($autogen_testdata) = mysqli_fetch_row($query_autogen_testdata);

        echo '<div class="top_note display_data_test_data_note">';
        
        echo '<div class="title">';
        echo '  <span class="faicon warning"></span> '.$lang['srv_testni_podatki_alert_title'];
        echo '</div>';

        echo $lang['srv_testni_podatki_alert'] . ' <a href="#" onClick="delete_test_data();">'.$lang['srv_delete_testdata'].' ('.$testdata.')</a>.';
        if ($autogen_testdata > 0) {
            echo ' '.$lang['srv_autogen_testni_podatki_alert'].' <a href="index.php?anketa=' . $this->sid . '&a=testiranje&m=testnipodatki&delete_autogen_testdata=1">'.$lang['srv_delete_autogen_testdata'].' ('.$autogen_testdata.')</a>.';
        }

        if ($showAnalizeCheckbox == true && false) {
            session_start();
            $checked = (isset($_SESSION['testData'][$this->sid]['includeTestData']) && $_SESSION['testData'][$this->sid]['includeTestData'] == 'false') ? '' : ' checked="checked"';
            echo '&nbsp;<label><input id="cnx_include_test_data" type="checkbox"' . $checked . ' onchange="surveyAnalisysIncludeTestData();" autocomplete="off">V analizah upoštevaj tudi testne vnose.';
            echo '</label>';
            session_commit();
        }
        echo '</div>';
    }


    /* Nastavitve na vrhu pri analizah in podatkih - NOVO
    *	Podstrani: 	data, export, quick_edit, variables
                    sumarnik, descriptor, frequency, crosstabs, ttest, means, nonresponses,
                    charts,
                    analysis_creport, analysis_links
                    ocena_trajanja, dejanski_casi,
                    komentarji, komentarji_anketa,
                    status,
                    tema, theme_editor
    */
    public function displayTopSettings($podstran){
        global $lang, $admin_type, $site_url, $global_user_id;

        // Ce nimamo podatkov ponekod tega potem ne prikazujemo
        $SDF = SurveyDataFile::get_instance();
        $SDF->init($this->sid);
        $data_file_status = $SDF->getStatus();
  
        if( in_array($data_file_status, array(FILE_STATUS_SRV_DELETED, FILE_STATUS_NO_DATA))
            && in_array($podstran, array(
                'status', 'para_analysis_graph', 'para_graph', 'usable_resp', 'status_advanced',
                'data', 'quick_edit', 'variables', 'export',
                'sumarnik', 'descriptor', 'frequency', 'crosstabs', 'ttest', 'means', 'break', 'multicrosstabs', 'charts', 'analysis_links'
            )) ){

            return;
        }

        //V komentarjih ni tega menija
        if($podstran == "komentarji" || $podstran == "komentarji_anketa") {
            return;
        }

        // Preverimo, ce je funkcionalnost v paketu, ki ga ima uporabnik
        $userAccess = UserAccess::getInstance($global_user_id);
        

        echo '<div id="topSettingsHolder" class="'.$podstran.'">';
        
        $analiza = false;
        if (in_array($podstran, array('sumarnik', 'descriptor', 'frequency', 'crosstabs', 'ttest', 'means', 'break', 'multicrosstabs', 'nonresponses'))) {
            $analiza = true;
        }

        $borderLeft = '';


        echo '<div class="left_options_holder">';

        // Navigacija analiz - ANALIZE
        if ($analiza) {
            echo '<div id="analizaSubNav">';
            $this->displayAnalizaSubNavigation();
            echo '</div>';

            $borderLeft = ' class="borderLeft"';
        }

        // Preklop med porocilom po meri in navadnimi porocili
        if ($podstran == 'analysis_creport' || $podstran == 'analysis_links') {
            echo '<div id="additional_navigation">';

            // Link na navadna porocila
            echo '<span '.($podstran == 'analysis_links' ? ' class="active"' : '').'><a href="index.php?anketa=' . $this->sid . '&a=analysis&m=analysis_links">'.$lang['srv_standard_report'] . '</a></span>';

            // Link na porocilo po meri
            echo '<span '.($podstran == 'analysis_creport' ? ' class="active"' : '').'><a href="index.php?anketa=' . $this->sid . '&a=analysis&m=analysis_creport">'.$lang['srv_custom_report'].'</a></span>';

            echo '</div>';

            $borderLeft = ' class="borderLeft"';
        } 
        // Preklop na vpogled, hitri seznam, spremenljivke (podatki)
        elseif ($podstran == 'data' || $podstran == 'quick_edit' || $podstran == 'variables') {
            echo '<div id="additional_navigation">';

            // Link na osnovno tabelo s podatki
            echo '<span '.($podstran == 'data' ? ' class="active"' : '').'><a href="' . $site_url . 'admin/survey/index.php?anketa=' . $this->sid . '&a=' . A_COLLECT_DATA . '">'.$lang['srv_lnk_data'].'</a></span>';
            
            // Link na vpogled
            echo '<span '.($podstran == 'quick_edit' ? ' class="active"' : '').'><a href="' . $site_url . 'admin/survey/index.php?anketa=' . $this->sid . '&a=' . A_COLLECT_DATA . '&m=quick_edit&quick_view=1">'.$lang['srv_lnk_vpogled'].'</a></span>';

            // Link na spremenljivke
            echo '<span '.($podstran == 'variables' ? ' class="active"' : '').'><a href="' . $site_url . 'admin/survey/index.php?anketa=' . $this->sid . '&a=' . A_COLLECT_DATA . '&m=' . M_COLLECT_DATA_VARIABLE_VIEW . '">'.$lang['srv_lnk_pregled_variabel'].'</a></span>';

            // Link na hitri seznam
            echo '<span><a href="#" onClick="displayDataPrintPreview();">'.$lang['srv_data_print_preview_link'].'</a> '.Help:: display('srv_data_print_preview').'</span>';

            echo '</div>';

            $borderLeft = ' class="borderLeft"';
        }
        // Link nazaj na diagnostiko - Ocenjevanje trajanja
        elseif ($podstran == 'ocena_trajanja') {
            echo '<div id="additional_navigation">';

            echo '<span class="active"><a href="index.php?anketa=' . $this->sid . '&amp;a=' . A_TESTIRANJE . '&amp;m=' . M_TESTIRANJE_PREDVIDENI . '" title="' . $lang['srv_testiranje_diagnostika_base'] . '">'.$lang['srv_testiranje_predvidenicas'].'</a></span>';
            
            echo '<span><a href="index.php?anketa=' . $this->sid . '&amp;a=' . A_TESTIRANJE . '&amp;m=' . M_TESTIRANJE_CAS . '" title="' . $lang['srv_testiranje_diagnostika_base'] . '">'.$lang['srv_testiranje_cas'].'</a></span>';

            echo '</div>';

            $borderLeft = ' class="borderLeft"';
        } 
        // Link nazaj na diagnostiko - Dejanski casi
        elseif ($podstran == 'dejanski_casi') {
            echo '<div id="additional_navigation">';
            
            echo '<span><a href="index.php?anketa=' . $this->sid . '&amp;a=' . A_TESTIRANJE . '&amp;m=' . M_TESTIRANJE_PREDVIDENI . '" title="' . $lang['srv_testiranje_predvidenicas'] . '">'.$lang['srv_testiranje_predvidenicas'].'</a></span>';

            echo '<span class="active"><a href="index.php?anketa=' . $this->sid . '&amp;a=' . A_TESTIRANJE . '&amp;m=' . M_TESTIRANJE_CAS . '" title="' . $lang['srv_testiranje_cas'] . '">'.$lang['srv_testiranje_cas'].'</a></span>';

            echo '</div>';

            $borderLeft = ' class="borderLeft"';
        } 
        elseif ($podstran == 'theme-editor') {
            echo '<div id="additional_navigation">';

			$mobile = (isset($_GET['mobile']) && $_GET['mobile'] == '1') ? '&mobile=1' : '';
			
            echo '<span><a href="index.php?anketa=' . $this->sid . '&amp;a=tema'.$mobile.'" title="' . $lang['srv_themes_select'] . '"><span class="faicon arrow_back"></span></a></span>';
            echo '<span ' . ($_GET['a'] == 'theme-editor' && $_GET['t'] != 'css' && $_GET['t'] != 'upload' ? ' class="active"' : '') . '><a href="index.php?anketa=' . $this->sid . '&amp;a=theme-editor&profile='. $_GET['profile'] . $mobile.'" title="' . $lang['srv_themes_mod'] . '">' . $lang['srv_themes_mod'] . '</a></span>';
            echo '<span ' . ($_GET['a'] == 'theme-editor' && $_GET['t'] == 'css' ? ' class="active"' : '') . '><a href="index.php?anketa=' . $this->sid . '&amp;a=theme-editor&t=css&profile='. $_GET['profile'] . $mobile.'" title="' . $lang['srv_themes_edit'] . '">' . $lang['srv_themes_edit'] . '</a></span>';
            
			// Za mobilno temo zaenkrat nimamo uploada css-ja
			if($mobile == '')
				echo '<span ' . ($_GET['a'] == 'theme-editor' && $_GET['t'] == 'upload' ? ' class="active"' : '') . '><a href="index.php?anketa=' . $this->sid . '&amp;a=theme-editor&t=upload&profile='. $_GET['profile'] . $mobile.'" title="' . $lang['srv_themes_upload_css'] . '">' . $lang['srv_themes_upload_css'] . '</a></span>';

            echo '</div>';

            $borderLeft = ' class="borderLeft"';
        } 
        // Link nazaj na podatke - Spremenljivke
        elseif ($podstran == 'para_analysis_graph') {

            // Info o neodgovorih
            echo '<div id="nonresponse_info">';
            echo $lang['srv_para_graph_text2'];
            echo '</div>';

            echo '<div id="additional_navigation" class="nonresponse">';
            echo '<span ' . (!isset($_GET['m']) || $_GET['m'] == '' ? 'class="active"' : '') . '><a href="index.php?anketa=' . $this->sid . '&a=' . A_NONRESPONSE_GRAPH . '">' . $lang['srv_para_label_variables'] . '</a></span>';
            echo '<span ' . ($_GET['m'] === 'breaks' ? 'class="active"' : '') . '><a href="index.php?anketa=' . $this->sid . '&a=' . A_NONRESPONSE_GRAPH . '&m=breaks">' . $lang['srv_para_label_breaks'] . '</a></span>';
            echo '<span ' . ($_GET['m'] === 'advanced' ? 'class="active"' : '') . '><a href="index.php?anketa=' . $this->sid . '&a=' . A_NONRESPONSE_GRAPH . '&m=advanced">' . $lang['srv_para_label_details'] . '</a></span>';
            echo '</div>';

            $borderLeft = ' class="borderLeft"';
        } 
        elseif ($podstran == 'aapor') {
            echo '<div id="additional_navigation">';

            echo '<span><a href="index.php?anketa=' . $this->sid . '&a=' . NAVI_AAPOR . '&m=aapor1">' . $lang['srv_lnk_AAPOR1'] . '</a></span>';
            echo '<span><a href="index.php?anketa=' . $this->sid . '&a=' . NAVI_AAPOR . '&m=aapor2">' . $lang['srv_lnk_AAPOR2'] . '</a></span>';

            echo '</div>';
        } 
        elseif ($podstran == 'diagnostics') {
            echo '<div id="additional_navigation">';

            echo '<a href="index.php?anketa=' . $this->sid . '&a=' . NAVI_AAPOR . '&m=aapor1"><span>' . $lang['srv_lnk_AAPOR1'] . '</span></a>';
            echo '<div id="toggleDataCheckboxes" ' . $borderLeft . ' onClick="toggleDataCheckboxes(\'paraAnalysisGraph\');"><span class="faicon ' . ($arrow == 1 ? ' fa-angle-up' : 'fa-angle-down') . '"></span> ' . $lang['srv_data_settings_checkboxes'] . '</div>';
            
            echo '</div>';
        }

        // Nastavitve tabele (checkboxi) - PODATKI
        if ($podstran == 'para_analysis_graph') {
            $arrow = (isset($_SESSION['sid_' . $this->sid]['paraAnalysisGraph_settings'])) ? $_SESSION['sid_' . $this->sid]['paraAnalysisGraph_settings'] : 0;
            echo '<div id="toggleDataCheckboxes" ' . $borderLeft . ' onClick="toggleDataCheckboxes(\'paraAnalysisGraph\');"><span class="faicon blue ' . ($arrow == 1 ? ' fa-angle-up' : 'fa-angle-down') . '"></span> ' . $lang['srv_data_settings_checkboxes'] . '</div>';
        }

        // Info o uporabnih enotah
        if ($podstran == 'usable_resp') {
            echo '<div id="usable_info">';
            echo $lang['srv_usableResp_text'];
            echo '</div>';

            $borderLeft = ' class="borderLeft"';
        }

        // Nastavitve tabele za UPORABNOST
        if ($podstran == 'usable_resp') {
            $arrow = (isset($_SESSION['sid_' . $this->sid]['usabilityIcons_settings'])) ? $_SESSION['sid_' . $this->sid]['usabilityIcons_settings'] : 0;
            echo '<div id="toggleDataCheckboxes" ' . $borderLeft . ' onClick="toggleDataCheckboxes(\'usability\');"><span class="faicon ' . ($arrow == 1 ? ' dropup_blue' : 'dropdown_blue') . '"></span> ' . $lang['srv_data_settings_checkboxes'] . '</div>';
        }

        // Radio status (vsi, ustrezni...)
        if ($analiza || in_array($podstran, array('data', 'export', 'charts', 'analysis_creport', 'analysis_links', 'para_graph', 'reminder_tracking', 'heatmap'))) {
            echo '<div id="dataOnlyValid" ' . $borderLeft . '>';
            echo '<span class="dataOnlyValid_label">'.$lang['srv_data_valid_label'].':</span>';
            SurveyStatusProfiles::displayOnlyValidCheckbox();
            echo '</div>';
        }

        if ($podstran == 'reminder_tracking') {
            echo '<div id="additional_navigation">';

            // Link na porocila z recnum
            echo '<a href="index.php?anketa=' . $this->sid . '&a=reminder_tracking&m=recnum"><span>' . $lang['srv_reminder_tracking_report_recnum'] . '</span></a>';

            // Link na porocila s spremenljivkami
            echo '<a href="index.php?anketa=' . $this->sid . '&a=reminder_tracking&m=vars"><span>' . $lang['srv_reminder_tracking_report_vprasanja'] . '</span></a>';

            echo '</div>';
        }

        echo '</div>';


        // Nastavitve na desni
        if ($analiza || in_array($podstran, array('data', 'export', 'charts', 'analysis_creport', 'analysis_links', 'dejanski_casi', 'para_analysis_graph', 'heatmap'))) {

            $active_filter = $this->filteredData($podstran);

            echo '<div id="analiza_right_options_holder" class="right_options_holder">';

            if ($analiza || in_array($podstran, array('charts'))) {

                // Nastavitev stevila odgovorov (odprtih) - po novem prestavljeno ven
                echo '<div id="analiza_right_options3" class="spaceRight">';
                echo $lang['srv_analiza_defAnsCnt_short'] . ': ';
                echo '<select id="numOpenAnswers" name="numOpenAnswers" autocomplete="off" onChange="saveSingleProfileSetting(\'' . SurveyDataSettingProfiles::getCurentProfileId() . '\', \'numOpenAnswers\', this.value); return false;">';
                $lastElement = end(SurveyDataSettingProfiles::$textAnswersMore);
                $cp = SurveyDataSettingProfiles::GetCurentProfileData();
                foreach (SurveyDataSettingProfiles::$textAnswersMore AS $key => $values) {
                    echo '<option' . (isset($cp['numOpenAnswers']) && (int)$cp['numOpenAnswers'] == $values ? ' selected="selected"' : '') . ' value="' . $values . '">';
                    if ($values != $lastElement) {
                        echo $values;
                    } else {
                        echo $lang['srv_all'];
                    }
                    echo '</option>';
                }
                echo '</select>';
                echo '</div>';

                // Nastavitve pri grafih
                if(in_array($podstran, array('charts'))){
                    echo '<div title="' . $lang['settings'] . '" id="analiza_right_options2" class="spaceRight spaceLeft">';
                    echo '<span id="filters_span2" class="faicon wheel_32"></span>';
                    $this->displayAnalizaRightOptions2($podstran);
                    echo '</div>';
                }
                // Nastavitve pri analizah - brez popupa - samo klik na zobnik
                else{
                    echo '<div title="' . $lang['settings'] . '" id="analiza_right_options2" class="spaceRight spaceLeft">';
                    echo '  <span id="filters_span2" class="faicon wheel_32" onClick="dataSettingProfileAction(\'showProfiles\');"></span>';
                    echo '</div>';
                }
            } 
            elseif ($podstran == 'data' || $podstran == 'export') {
                // Ikona za ponovno generiranje datoteke
                echo '<span title="' . $lang['srv_deleteSurveyDataFile_link'] . '" class="faicon refresh" onClick="changeColectDataStatus(); return false;"></span>';
            }

            echo '<div title="' . $lang['filters'] . '" id="analiza_right_options" '.(!$userAccess->checkUserAccess($what='filters') ? 'class="user_access_locked"' : '').'>';
            echo '<span id="filters_span" class="faicon filter pointer"></span>';
            $this->displayAnalizaRightOptions($podstran);
            echo '</div>';
            if($podstran != 'dejanski_casi') 
                echo Help::display('srv_data_filter');

            echo '</div>';
        } 

        echo '</div>';
    }

    public function displayAnalizaSubNavigation(){
        global $lang, $admin_type, $global_user_id;

        $userAccess = UserAccess::getInstance($global_user_id); 
        $current_package = $userAccess->getPackage();
        $user_package = $userAccess->getPackage(); // paket, ki ga ima uporabnik (1, 2 , 3)
        $dostop_styling = "";
        if ($user_package != "-1" && $user_package != "" && !$userAccess->isAnketaOld())
            $dostop_styling = "dostop_".$user_package."ka";

        $_js_links = array();
        for ($i=1; $i<9; $i++) {
            $_js_links[$i] = ' onmouseover="show_anl_prev('.$i.'); return false;" onmouseout="hide_anl_prev(); return false"';
        }

        if ($_GET['m'] != M_ANALYSIS_CHARTS && $_GET['m'] != M_ANALYSIS_LINKS && $_GET['m'] != M_ANALYSIS_CREPORT) {
            
            echo '<span>'.$lang['srv_statistic_menu'].' '.Help::display('srv_menu_statistic').'</span>';
            
            echo '<div id="globalSetingsLinks" class="analiza" >';

            if (SurveyInfo::getInstance()->checkSurveyModule('hierarhija')) {
                echo '<div class="analizaSubNavigation">';

                echo '  <div class="analizaSubNavigation_item '.($_GET['m'] == M_ANALYSIS_MEANS_HIERARHY ? 'highlightLineTab' : 'nonhighlight displayNone').'" '.$_js_links[5].'>';
                echo '      <a href="index.php?anketa=' . $this->sid . '&a=' . A_ANALYSIS . '&m=' . M_ANALYSIS_MEANS_HIERARHY . '" title="' . $lang['srv_means'] . '"><span>' . $lang['srv_means'] . '</span></a>';
                echo '  </div>';

                echo '</div>';
            } 
            else {

                echo '<div class="analizaSubNavigation '.($dostop_styling != "" && $current_package!=3 ? 'user_access_locked' : '').'" onmouseenter="show_menu(); return false;" onmouseleave="hide_menu(); return false">';
                
                # sumarnik
                echo '  <div class="analizaSubNavigation_item '.($_GET['m'] == M_ANALYSIS_SUMMARY ? 'highlightLineTab' : 'nonhighlight displayNone"').'" '.$_js_links[1].'>';
                echo '      <a href="index.php?anketa=' . $this->sid . '&a=' . A_ANALYSIS . '&m=' . M_ANALYSIS_SUMMARY . '" title="' . $lang['srv_sumarnik'] . '"><span>' . $lang['srv_sumarnik'] . '</span></a>';
                echo '  </div>';
                
                # opisne
                echo '  <div class="analizaSubNavigation_item '.($_GET['m'] == M_ANALYSIS_DESCRIPTOR ? 'highlightLineTab' : 'nonhighlight displayNone').'" '.$_js_links[2].'>';
                echo '      <a href="index.php?anketa=' . $this->sid . '&a=' . A_ANALYSIS . '&m=' . M_ANALYSIS_DESCRIPTOR . '" title="' . $lang['srv_descriptor'] . '"><span>' . $lang['srv_descriptor_short'] . '</span></a>';
                echo '  </div>';

                # frekvence
                echo '  <div class="analizaSubNavigation_item '.($_GET['m'] == M_ANALYSIS_FREQUENCY ? 'highlightLineTab' : 'nonhighlight displayNone').'" '.$_js_links[3].'>';
                echo '      <a href="index.php?anketa=' . $this->sid . '&a=' . A_ANALYSIS . '&m=' . M_ANALYSIS_FREQUENCY . '" title="' . $lang['srv_frequency'] . '"><span>' . $lang['srv_frequency'] . '</span></a>';
                echo '  </div>';

                // Pri glasovanju nimamo teh modulov ker imamo samo 1 vprasanje
                if(SurveyInfo::getInstance()->getSurveyColumn("survey_type") != 0){

                    if($dostop_styling != "" && $current_package == 1){
                        //echo '<div class="analysis_locked">';
                        //$analysis_locked_show = $_GET['m'] == M_ANALYSIS_CROSSTAB ? 'displayAnalysis' : 'displayNone';
                        if($_GET['m'] == M_ANALYSIS_CROSSTAB || $_GET['m'] == M_ANALYSIS_MULTICROSSTABS || $_GET['m'] == M_ANALYSIS_MEANS || $_GET['m'] == M_ANALYSIS_TTEST || $_GET['m'] == M_ANALYSIS_BREAK){
                            $analysis_locked_show = 'displayAnalysis';
                        }else{
                            $analysis_locked_show = 'displayNone';
                        }
                        echo '<div class="analysis_locked '.$analysis_locked_show.'">';
                    }

                    # crostabs
                    echo '<div class="analizaSubNavigation_item '.($_GET['m'] == M_ANALYSIS_CROSSTAB ? 'highlightLineTab' : 'nonhighlight displayNone').'" '.$_js_links[4].'>';
                    echo '  <a href="index.php?anketa=' . $this->sid . '&a=' . A_ANALYSIS . '&m=' . M_ANALYSIS_CROSSTAB . '" title="' . $lang['srv_crosstabs'] . '"><span>' . $lang['srv_crosstabs'] . '</span></a>';
                    echo '</div>';

                    if($dostop_styling != "" && $current_package == 2){
                        //echo '<div class="analysis_locked">';
                        if($_GET['m'] == M_ANALYSIS_MULTICROSSTABS || $_GET['m'] == M_ANALYSIS_MEANS || $_GET['m'] == M_ANALYSIS_TTEST || $_GET['m'] == M_ANALYSIS_BREAK){
                            $analysis_locked_show = 'displayAnalysis';
                        }else{
                            $analysis_locked_show = 'displayNone';
                        }
                        echo '<div class="analysis_locked '.$analysis_locked_show.'">';
                    }

                    # multicrostabs
                    echo '<div class="analizaSubNavigation_item '.($_GET['m'] == M_ANALYSIS_MULTICROSSTABS ? 'highlightLineTab' : 'nonhighlight displayNone').'" '.$_js_links[8].'>';
                    echo '  <a href="index.php?anketa=' . $this->sid . '&a=' . A_ANALYSIS . '&m=' . M_ANALYSIS_MULTICROSSTABS . '" title="' . $lang['srv_multicrosstabs'] . '"><span>' . $lang['srv_multicrosstabs'] . '</span></a>';
                    echo '</div>';

                    # povprečaj
                    echo '<div class="analizaSubNavigation_item '.($_GET['m'] == M_ANALYSIS_MEANS ? 'highlightLineTab' : 'nonhighlight displayNone').'" '.$_js_links[5].'>';
                    echo '  <a href="index.php?anketa=' . $this->sid . '&a=' . A_ANALYSIS . '&m=' . M_ANALYSIS_MEANS . '" title="' . $lang['srv_means'] . '"><span>' . $lang['srv_means'] . '</span></a>';
                    echo '</div>';

                    # ttest
                    if ($admin_type == 0) {
                        echo '<div class="analizaSubNavigation_item '.($_GET['m'] == M_ANALYSIS_TTEST ? 'highlightLineTab' : 'nonhighlight displayNone').'" '.$_js_links[6].'>';
                        echo '  <a href="index.php?anketa=' . $this->sid . '&a=' . A_ANALYSIS . '&m=' . M_ANALYSIS_TTEST . '" title="' . $lang['srv_ttest'] . '"><span>' . $lang['srv_ttest'] . '</span></a>';
                        echo '</div>';
                    }

                    # break
                    echo '<div class="analizaSubNavigation_item '.($_GET['m'] == M_ANALYSIS_BREAK ? 'highlightLineTab' : 'nonhighlight displayNone') . $_js_links[7].'>';
                    echo '  <a href="index.php?anketa=' . $this->sid . '&a=' . A_ANALYSIS . '&m=' . M_ANALYSIS_BREAK . '" title="' . $lang['srv_break'] . '"><span>' . $lang['srv_break'] . '</span></a>';
                    echo '</div>';
                    
                    if($dostop_styling != "" && $current_package != 3){
                        echo '</div>';
                    }
                }
                
                echo '</div>';
            }
      
            echo '</div>';


            ?><script>
                <?php $m = $_GET['m']; ?>
                data_m = "<?php echo $m; ?>";
                dostop_styling = "<?php echo $dostop_styling; ?>";
                current_package = "<?php echo $current_package; ?>";

                function show_menu(){
                    if((data_m == 'crosstabs' || data_m == 'multicrosstabs' || data_m == 'means' || data_m == 'ttest' || data_m == 'break')&&(dostop_styling != "" && current_package == 1)){
                        $('.analysis_locked').removeClass('displayAnalysis');
                    }else if((data_m == 'multicrosstabs' || data_m == 'means' || data_m == 'ttest' || data_m == 'break')&&(dostop_styling != "" && current_package == 2)){
                        $('.analysis_locked').removeClass('displayAnalysis');
                    }
                }
                function hide_menu(){
                    if((data_m == 'crosstabs' || data_m == 'multicrosstabs' || data_m == 'means' || data_m == 'ttest' || data_m == 'break')&&(dostop_styling != "" && current_package == 1)){                   
                        $('.analysis_locked').addClass('displayAnalysis');
                    }else if((data_m == 'multicrosstabs' || data_m == 'means' || data_m == 'ttest' || data_m == 'break')&&(dostop_styling != "" && current_package == 2)){
                        $('.analysis_locked').addClass('displayAnalysis');
                    }
                }
                
                function show_anl_prev(tip) {
                    $("#srv_analiza_preview_div").show();

                    if (tip > 0) {
                        // 	skrijemo ostale previev-e
                        $('.srv_analiza_preview_sub').addClass('displayNone');
                        // prikažemo ustrezen predogled
                        $('#srv_analiza_preview_sub_' + tip).removeClass('displayNone');
                    }
                }
                function hide_anl_prev() {
                    $("#srv_analiza_preview_div").hide();
                   
                }
            </script><?php
        }

        $this->displayAnalizaPreview();
    }

    public function displayAnalizaRightOptions($podstran, $onlyLinks = false){
        global $lang, $admin_type, $global_user_id;

        $userAccess = UserAccess::getInstance($global_user_id);     

        $allowShow = array();

        #dovoljenja za prikaz določenih nastavitev
        $allowShow[M_ANALYSIS_SUMMARY] =
        $allowShow[M_ANALYSIS_DESCRIPTOR] =
        $allowShow[M_ANALYSIS_FREQUENCY] =
        $allowShow[M_ANALYSIS_CHARTS] =
        $allowShow[M_ANALYSIS_LINKS] =
        $allowShow[M_ANALYSIS_CREPORT] = array(
            'AS_SETTINGS',
            'AS_SEGMENTS',
            'AS_ZOOM',
            'AS_LOOPS',
            'AS_BREAK',
            'AS_VARIABLES',
            'AS_CONDITIONS',
            'AS_MISSINGS',
            'AS_TIME',
            'AS_STATUS');

        $allowShow[M_ANALYSIS_CROSSTAB] =
        $allowShow[M_ANALYSIS_MULTICROSSTABS] =
        $allowShow[M_ANALYSIS_MEANS_HIERARHY] =
        $allowShow[M_ANALYSIS_MEANS] = array(
            'AS_SETTINGS',
            'AS_ZOOM',
            'AS_LOOPS',
            'AS_CONDITIONS',
            'AS_MISSINGS',
            'AS_TIME',
            'AS_STATUS');
        $allowShow[M_ANALYSIS_TTEST] = array(
            'AS_SETTINGS',
            'AS_CONDITIONS',
            'AS_TIME',
            'AS_STATUS');
        $allowShow[M_ANALYSIS_BREAK] = array(
            'AS_SETTINGS',
            'AS_ZOOM',
            'AS_LOOPS',
            'AS_BREAK',
            'AS_VARIABLES',
            'AS_CONDITIONS',
            'AS_MISSINGS',
            'AS_TIME',
            'AS_STATUS');

        $allowShow[M_ANALYSIS_NONRESPONSES] =
        $allowShow[M_ANALYSIS_PARA] = array(
            'AS_SETTINGS',
            'AS_VARIABLES',
            'AS_CONDITIONS',
            'AS_MISSINGS',
            'AS_TIME',
            'AS_STATUS');

        $allowShow['para_analysis_graph'] = array(
            'AS_VARIABLES',
            'AS_CONDITIONS',
            'AS_MISSINGS',
        );

        session_start();
        $hideAdvanced = (isset($_SESSION['AnalysisAdvancedLinks'][$this->sid]) && $_SESSION['AnalysisAdvancedLinks'][$this->sid] == true) ? true : false;

        if ($podstran == 'data' || $podstran == 'export' || $podstran == 'quick_edit') {
            echo '<div id="div_analiza_filtri_right" class="floatRight">';
            echo '<ul>';

            /* if ($podstran == 'export') {
                echo '<li>';
                echo '  <span id="link_export_setting" onClick="$(\'#fade\').fadeTo(\'slow\', 1);$(\'#div_export_setting_show\').fadeIn(\'slow\'); return false;" title="' . $lang['srv_dsp_link'] . '">' . $lang['srv_dsp_link'] . '</span>';
                echo '</li>';
            } */

            # filter za nastavitve
            # div za filtre statusov
            SurveyStatusProfiles:: DisplayLink(false, false);

            # filter za spremenljivke - variable
            SurveyVariablesProfiles::DisplayLink(false, false);
            #filter za ife - pogoje
            SurveyConditionProfiles::DisplayLink(false);
            # filter za čase
            SurveyTimeProfiles::DisplayLink(false);

            echo '</ul>';
            echo '</div>'; # id="div_analiza_filtri_right" class="floatRight"
        } 
        elseif ($podstran == 'dejanski_casi') {

            SurveyStatusCasi:: Init($this->sid);

            SurveyUserSetting:: getInstance()->Init($this->sid, $global_user_id);

            // nastavitve iz popupa
            $rezanje = SurveyUserSetting::getInstance()->getSettings('rezanje');
            if ($rezanje == '') $rezanje = 0;
            $rezanje_meja_sp = SurveyUserSetting::getInstance()->getSettings('rezanje_meja_sp');
            if ($rezanje_meja_sp == '') $rezanje_meja_sp = 5;
            $rezanje_meja_zg = SurveyUserSetting::getInstance()->getSettings('rezanje_meja_zg');
            if ($rezanje_meja_zg == '') $rezanje_meja_zg = 5;
            $rezanje_predvidena_sp = SurveyUserSetting::getInstance()->getSettings('rezanje_predvidena_sp');
            if ($rezanje_predvidena_sp == '') $rezanje_predvidena_sp = 10;
            $rezanje_predvidena_zg = SurveyUserSetting::getInstance()->getSettings('rezanje_predvidena_zg');
            if ($rezanje_predvidena_zg == '') $rezanje_predvidena_zg = 200;


            echo '<div id="div_analiza_filtri_right" class="casi">';
            echo '<ul>';

            // profili rezanja
            echo '  <li>';
            echo '      <span onclick="vnosi_show_rezanje_casi(); return false;" id="link_rezanje_casi" title="'.$lang['srv_rezanje'].'">';
            echo $lang['srv_rezanje'];
            echo '          <div style="font-size:10px">';
            if ($rezanje == 0)
                echo '(' . $lang['srv_rezanje_meja_sp'] . ': ' . $rezanje_meja_sp . '%, ' . $lang['srv_rezanje_meja_zg'] . ': ' . $rezanje_meja_zg . '%)';
            else
                echo '(' . $rezanje_predvidena_sp . '% ' . $lang['srv_and'] . ' ' . $rezanje_predvidena_zg . '% ' . $lang['srv_rezanje_predvidenega'] . ')';
            echo '          </div>';
            echo '      </span>';
            echo '  </li>';
            

            // profili statusov
            $statusCasi = SurveyStatusCasi::getProfiles();
            $izbranStatusCasi = SurveyStatusCasi::getCurentProfileId();
            echo '  <li>';
            //echo '<label id="link_status_casi" title="' . $lang['srv_statusi'] . '">' . $lang['srv_statusi'] . ': </span>';
            echo '      <span>'.$lang['srv_statusi'].':</span>';
            echo '      <select id="vnosi_current_status_casi" name="vnosi_current_status_casi" onChange="statusCasiAction(\'change\'); return false;">';
            foreach ($statusCasi as $key => $value) {
                echo '		<option' . ($izbranStatusCasi == $value['id'] ? ' selected="selected"' : '') . ' value="' . $value['id'] . '">' . $value['name'] . '</option>';
            }
            echo '      </select>';
            echo '</li>';

            echo '</ul>';
            echo '</div>';
        } 
        else {
            if ($onlyLinks == false) {
                echo '<div id="div_analiza_filtri_right" class="analiza">';
            }

            echo '<ul>';

            if (in_array('AS_SEGMENTS', $allowShow[$podstran])) {
                # zoom
                SurveyZoom::DisplayLink($hideAdvanced);
            }
            if (in_array('AS_ZOOM', $allowShow[$podstran])) {
                # inspect
                $SI = new SurveyInspect($this->sid);
                $SI->DisplayLink($hideAdvanced);
            }
            if (in_array('AS_LOOPS', $allowShow[$podstran])) {
                # filter za zanke
                SurveyZankaProfiles::DisplayLink($hideAdvanced);
            }
            if (in_array('AS_VARIABLES', $allowShow[$podstran])) {
                # div za profile variabel
                SurveyVariablesProfiles::DisplayLink(true, $hideAdvanced);
            }
            if (in_array('AS_CONDITIONS', $allowShow[$podstran])) {
                # filter za  pogoje - ifi
                SurveyConditionProfiles::DisplayLink($hideAdvanced);
            }
            if (in_array('AS_MISSINGS', $allowShow[$podstran])) {
                # profili missingov
                SurveyMissingProfiles::DisplayLink($hideAdvanced);
            }
            if (in_array('AS_TIME', $allowShow[$podstran])) {
                # filter za čase
                SurveyTimeProfiles::DisplayLink($hideAdvanced);
            }
            if (in_array('AS_STATUS', $allowShow[$podstran])) {
                # div za filtre statusov
                SurveyStatusProfiles::DisplayLink($hideAdvanced);
            }
            echo '</ul>';

            if ($onlyLinks == false) {
                echo '</div>';
            }
        }

        // Javascript s katerim povozimo urlje za izvoze, ki niso na voljo v paketu
        $userAccess = UserAccess::getInstance($global_user_id);
        if(!$userAccess->checkUserAccess($what='filters')){
            echo '<script> userAccessFilters(); </script>';
        }
    }

    public function displayAnalizaRightOptions2($podstran, $onlyLinks = false)
    {
        global $lang, $admin_type, $global_user_id;

        $allowShow = array();

        #dovoljenja za prikaz določenih nastavitev
        $allowShow[M_ANALYSIS_SUMMARY] =
        $allowShow[M_ANALYSIS_DESCRIPTOR] =
        $allowShow[M_ANALYSIS_FREQUENCY] =
        $allowShow[M_ANALYSIS_CHARTS] =
        $allowShow[M_ANALYSIS_LINKS] =
        $allowShow[M_ANALYSIS_CREPORT] = array(
            'AS_SETTINGS',
            'AS_SEGMENTS',
            'AS_ZOOM',
            'AS_LOOPS',
            'AS_BREAK',
            'AS_VARIABLES',
            'AS_CONDITIONS',
            'AS_MISSINGS',
            'AS_TIME',
            'AS_STATUS');

        $allowShow[M_ANALYSIS_CROSSTAB] =
        $allowShow[M_ANALYSIS_MULTICROSSTABS] =
        $allowShow[M_ANALYSIS_MEANS_HIERARHY] =
        $allowShow[M_ANALYSIS_MEANS] = array(
            'AS_SETTINGS',
            'AS_ZOOM',
            'AS_LOOPS',
            'AS_CONDITIONS',
            'AS_MISSINGS',
            'AS_TIME',
            'AS_STATUS');
        $allowShow[M_ANALYSIS_TTEST] = array(
            'AS_SETTINGS',
            'AS_CONDITIONS',
            'AS_TIME',
            'AS_STATUS');
        $allowShow[M_ANALYSIS_BREAK] = array(
            'AS_SETTINGS',
            'AS_ZOOM',
            'AS_LOOPS',
            'AS_BREAK',
            'AS_VARIABLES',
            'AS_CONDITIONS',
            'AS_MISSINGS',
            'AS_TIME',
            'AS_STATUS');

        $allowShow[M_ANALYSIS_NONRESPONSES] =
        $allowShow[M_ANALYSIS_PARA] = array(
            'AS_SETTINGS',
            'AS_VARIABLES',
            'AS_CONDITIONS',
            'AS_MISSINGS',
            'AS_TIME',
            'AS_STATUS');

        $allowShow['para_analysis_graph'] = array(
            'AS_VARIABLES',
            'AS_CONDITIONS',
            'AS_MISSINGS',
        );

        session_start();
        $hideAdvanced = (isset($_SESSION['AnalysisAdvancedLinks'][$this->sid]) && $_SESSION['AnalysisAdvancedLinks'][$this->sid] == true) ? true : false;

        if ($onlyLinks == false) {
            echo '<div id="div_analiza_filtri_right2" class="analiza">';
        }

        echo '<ul>';

        if ($podstran == 'charts') {
            // nastavitve za grafe (hq, barva)
            $this->displayChartOptions();
        }

        if (in_array('AS_SETTINGS', $allowShow[$podstran])) {

            # filter za nastavitve
            SurveyDataSettingProfiles::DisplayLink($hideAdvanced);
        }

        echo '</ul>';

        if ($onlyLinks == false) {
            echo '</div>';
        }
    }

    public function displayChartOptions(){
        global $lang, $admin_type;

        SurveyChart::Init($this->sid);
        
        // Nastavitev HQ grafov
        echo '<li>';
        echo '  <input type="checkbox" name="chart_hq" id="chart_hq" onClick="changeChartHq(this)" '.(SurveyChart::$quality == 3 ? ' checked="checked"' : '').'>';
        echo '  <label for="chart_hq">'.$lang['srv_chart_hq'] . '</label>';
        echo '</li>';


        // Nastavitev skina grafov
        $skin = SurveyUserSetting:: getInstance()->getSettings('default_chart_profile_skin');
        $skin = isset($skin) ? $skin : '1ka';

        // ce je custom skin
        if (is_numeric($skin)) {
            $skin = SurveyChart::getCustomSkin($skin);
            $name = $skin['name'];
        } else {
            switch ($skin) {
                // 1ka skin
                case '1ka':
                    $name = $lang['srv_chart_skin_1ka'];
                    break;

                // zivahen skin
                case 'lively':
                    $name = $lang['srv_chart_skin_0'];
                    break;

                // blag skin
                case 'mild':
                    $name = $lang['srv_chart_skin_1'];
                    break;

                // Office skin
                case 'office':
                    $name = $lang['srv_chart_skin_6'];
                    break;

                // Pastel skin
                case 'pastel':
                    $name = $lang['srv_chart_skin_7'];
                    break;

                // zelen skin
                case 'green':
                    $name = $lang['srv_chart_skin_2'];
                    break;

                // moder skin
                case 'blue':
                    $name = $lang['srv_chart_skin_3'];
                    break;

                // rdeč skin
                case 'red':
                    $name = $lang['srv_chart_skin_4'];
                    break;

                // skin za vec kot 5 moznosti
                case 'multi':
                    $name = $lang['srv_chart_skin_5'];
                    break;
            }
        }
        
        $hideAdvanced = (isset($_SESSION['AnalysisAdvancedLinks'][$this->sid]) && $_SESSION['AnalysisAdvancedLinks'][$this->sid] == true) ? true : false;
        if ($hideAdvanced == false) {

            echo '<li>';
            echo '  <span id="link_chart_color" title="' . $lang['srv_chart_skin'] . '">' . $lang['srv_chart_skin'] . ': <span style="font-weight: 500;">' . $name . '</span></span>';
            echo '</li>';
        }
    }


    // Ugotovimo ce so podatki kako filtrirani
    function filteredData($podstran)
    {

        if ($podstran == 'status') {

            if (SurveyTimeProfiles::getCurentProfileId() != STP_DEFAULT_PROFILE)
                return true;
        } else if (in_array($podstran, array('sumarnik', 'descriptor', 'frequency', 'crosstabs', 'ttest', 'means', 'break', 'multicrosstabs', 'nonresponses'))) {

            if (SurveyDataSettingProfiles::getCurentProfileId() != SDS_DEFAULT_PROFILE)
                return true;

            if (SurveyZoom::getCurentProfileId() != 0 && $podstran != 'status')
                return true;

            $SI = new SurveyInspect($this->sid);
            if ($SI->isInspectEnabled() && $podstran != 'status')
                return true;

            if (SurveyVariablesProfiles::getCurentProfileId() != SVP_DEFAULT_PROFILE)
                return true;

            if (SurveyConditionProfiles::getCurentProfileId() != SCP_DEFAULT_PROFILE)
                return true;

            if (SurveyMissingProfiles::getCurentProfileId() != SMP_DEFAULT_PROFILE)
                return true;

            if (SurveyTimeProfiles::getCurentProfileId() != STP_DEFAULT_PROFILE)
                return true;

            if (SurveyStatusProfiles::getCurentProfileId() != SSP_DEFAULT_PROFILE)
                return true;
        } else if (in_array($podstran, array('data', 'export', 'quick_edit'))) {

            $SPM = new SurveyProfileManager($this->sid);
            if ($SPM->getCurentProfileId() != SSP_DEFAULT_PROFILE && (int)$SPM->getCurentProfileId() != 0 && (int)$SPM->getCurentProfileId() != -1)
                return true;

            if (SurveyVariablesProfiles::getCurentProfileId() != SVP_DEFAULT_PROFILE)
                return true;

            if (SurveyConditionProfiles::getCurentProfileId() != SCP_DEFAULT_PROFILE)
                return true;

            if (SurveyTimeProfiles::getCurentProfileId() != STP_DEFAULT_PROFILE)
                return true;

            if (SurveyStatusProfiles::getCurentProfileId() != SSP_DEFAULT_PROFILE)
                return true;
        }

        return false;
    }
}

?>