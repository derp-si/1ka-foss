<?php

/*
    Class, ki skrbi za prikaz in urejanje vprasajckov
*/


class Help {

    /**
    * @desc izpise polje s helpom. 
    * ce smo v editmodu se bo prikazal textbox za urejanje helpa
    * ce smo v navadnem modu se bo prikazal help box
    */
    public static function display ($what, $title='') {
        global $admin_type, $lang;
        
        $help = '';

        $sql = sisplet_query("SELECT help FROM srv_help WHERE what='$what' AND lang='$lang[id]'");
        if(mysqli_num_rows($sql) > 0){
            $row = mysqli_fetch_array($sql);
            $help = $row['help'];
        }

        if ($admin_type == 0 && isset($_COOKIE['edithelp'])) {
            return ' <a href="/" id="help_'.$what.'" lang="'.$lang['id'].'" class="edithelp" onclick="return false;" title_txt="">'.(empty($title) ? '(?)' : '<span class="faicon help2 title20"></span>').'</a>';  
        } 
        elseif ($help != '') {
            return ' <a href="/" id="help_'.$what.'" lang="'.$lang['id'].'" class="help" onclick="return false;" title_txt="">'.(empty($title) ? '(?)' : '<span class="faicon help2 title20"></span>').'</a>';
        }
        
    }
    

    // Prikazemo zavihek z nastavitvami vprasajckov
    public static function displaySettings(){
        global $lang;


        // Vklop / izklop urejanja vprasajckov
        echo '<fieldset>';
        echo '  <legend>' . $lang['help_settings_activate_editing'] . '</legend>';   
        echo '  <p class="bottom16" >' . $lang['help_settings_activate_editing'] . ' '.Help::display('srv_window_help').':</p>';
        self::editToggle();
        echo '</fieldset>';


        // Urejanje vseh vprasajckov
        echo '<fieldset>';
        echo '  <legend>' . $lang['help_settings_list'] . '</legend>';
        self::editVprasajcki();
        echo '</fieldset>';
    }

    // vkljuci izkljuci editiranje helpa
    private static function editToggle () {
        global $lang;
        
        if (isset($_COOKIE['edithelp'])) {
            echo '<div class="button_holder bottom0">';
            echo '<button class="medium blue" onClick="window.location.href=\'ajax.php?t=help&a=edit_off\';return false;">'.$lang['srv_insend'].'</button>';				
            echo '</div>';
        }
        else {
            echo '<div class="button_holder bottom0">';
            echo '<button class="medium blue" onClick="window.location.href=\'ajax.php?t=help&a=edit_on\';return false;">'.$lang['start'].'</button>';				
            echo '</div>';
        }
    }

    // Izpise seznam vprasajckov za urejanje
    private static function editVprasajcki () {
        global $lang;

        $sql = sisplet_query("SELECT *
                                FROM srv_help h 
                                    LEFT JOIN users u
                                    ON u.id=h.last_edit_usr_id
                                WHERE h.lang='".$lang['id']."'
                                ORDER BY h.last_edit_time DESC, h.what ASC
                            ");
        while($row = mysqli_fetch_array($sql)){

            echo '<div class="setting_holder help_edit">';

            echo '<div class="col1">';
            echo $row['what'].' '.self::display($row['what']);

            echo '<div class="last_edit">';
            echo 'Zadnje urejanje:<br>'.date('d.m.Y, H:i', strtotime($row['last_edit_time']));
            if($row['email'] != '')
                echo '<br>('.$row['email'].')';
            echo '</div>';
            
            echo '</div>';

            echo '<div class="col2">';
            echo '  <textarea id="edithelp_mass_'.$row['what'].'">'.$row['help'].'</textarea>';
            echo '</div>';
            
            echo '<div class="col3">';
            echo '  <button class="small blue" onClick="save_help_mass(\''.$row['what'].'\', \''.$lang['id'].'\');">Shrani</button>';
            echo '</div>';

            echo '</div>';
        }
    }

    
    function ajax () {
        
        if ($_GET['a'] == 'edit_on') {
            $this->ajax_edit_on();
        } 
        elseif ($_GET['a'] == 'edit_off') {
            $this->ajax_edit_off();
        } 
        elseif ($_GET['a'] == 'display_edit_help') {
            $this->ajax_display_edit_help();
        } 
        elseif ($_GET['a'] == 'save_help') {
            $this->ajax_save_help();
        } 
        elseif ($_GET['a'] == 'display_help') {
            $this->ajax_display_help();
        }
    }
    
    /**
    * @desc vklopi editiranje helpa (nastavi cooike)
    */
    function ajax_edit_on () {

        setcookie('edithelp', 'on');

        header("Location: index.php?a=nastavitve&m=help_settings");
    }
    
    /**
    * @desc izklopi editiranje helpa (nastavi cooike)
    */
    function ajax_edit_off () {

        setcookie('edithelp', '', time()-3600);

        header("Location: index.php?a=nastavitve&m=help_settings");
    }
    
    /**
    * @desc prikaze formo za urejanje helpa
    */
    function ajax_display_edit_help () {
    	global $lang;
    	
        $l = (int)$_GET['lang'];
        
        $what = substr($_REQUEST['what'], 5);
        
        $sql = sisplet_query("SELECT help FROM srv_help WHERE what = '$what' AND lang='$l'");
        $row = mysqli_fetch_array($sql);

        echo '<textarea id="edithelp_'.$what.'" name="help" style="width:100%; height: 100px">'.$row['help'].'</textarea>';
        echo '<input type="button" value="'.$lang['save'].'" onclick="save_help(\''.$what.'\', \''.$l.'\')" />';
        
    }
    
    /** 
    * @desc shrani help
    */
    function ajax_save_help () {
        global $global_user_id;

    	$l = (int)$_GET['lang'];
    	
        $what = $_REQUEST['what'];
        $help = $_POST['help'];
        
        sisplet_query("REPLACE INTO srv_help (what, lang, help, last_edit_usr_id) VALUES ('".$what."', '".$l."', '".$help."', '".$global_user_id."')");
    }
    
    function ajax_display_help() {
        	
    	$l = (int)$_GET['lang'];
    	
        $what = substr($_REQUEST['what'], 5);
        
        $sql = sisplet_query("SELECT help FROM srv_help WHERE what = '$what' AND lang='$l'");
        $row = mysqli_fetch_array($sql);
		
		echo '<div class="qtip-help">'.nl2br($row['help']).'</div>';
    }
}

?>