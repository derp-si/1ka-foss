<?php

class LibraryBranching {


    var $ank_id;            // Anketa znotraj katere urejamo knjiznico
    
    var $tab = 0;           // tip pove v bazi srv_library_folder za kater tip gre - 0->vprasanja, 1->ankete (vprasanja iz anket)
    var $folder_id = 0;     // Trenutno aktiven folder
    var $expanded_folders = array();     // Razprti folderji

    var $root_folder = array();         // Root folderji (sistemski in uporabnikov)
    var $folder_structure = array();     // Struktura direktorijev uporabnika


    public function __construct ($ank_id=0) {
		
		if($ank_id == 0)
			die();	

        $this->ank_id = $ank_id;

        // Nastavimo vse razprte folderje (vsi parenti ce je child aktiven)
        $this->setExpandedFolders($this->folder_id);
	}
	
    // Loop cez folderje in rekurzivno nastavi razprte (parenti aktivnega)
    private function setExpandedFolders($folder_id){

        $this->expanded_folders[] = $folder_id;

        if($folder_id == '0'){
            return;
        }

        $sqlFolders = sisplet_query("SELECT parent FROM srv_library_folder WHERE id='".$folder_id."' AND tip='".$this->tab."'");
        $rowFolders = mysqli_fetch_array($sqlFolders);

        $this->setExpandedFolders($rowFolders['parent']);
    }


    /**
    * @desc prikaze knjiznico znotraj ankete
    */
    private function displayLibrary() {
        global $admin_type;
		global $global_user_id;
		global $lang;


        // Naslov v oknu
        echo '<h2><span class="faicon library"></span>'.$lang['srv_library_survey_title'].'</h2>';
        echo '<div class="popup_close"><a href="#" onclick="popupClose();">✕</a></div>';

        // Tabi (vprasanja, ankete)
        echo '<div id="lib_tabs" class="lib_tabs">';
        $this->displayTabs();
        echo '</div>';

        // Vsebina
        echo '<div id="tab_content" class="tab_content">';
        $this->displayTabContent();
        echo '</div>';
    }

    // Prikazemo tabe - vprasanja / ankete
    private function displayTabs(){
        global $lang;

        // Tab vprasanja
        echo '<div id="tab_0" class="tab '.($this->tab == 0 ? 'active' : '').'" onClick="displayLibraryTab(\'0\'); return false;">';
        echo $lang['srv_library_survey_tab_questions'];
        echo '</div>';

        // Tab ankete
        echo '<div id="tab_1" class="tab '.($this->tab == 1 ? 'active' : '').'" onClick="displayLibraryTab(\'1\'); return false;">';
        echo $lang['srv_library_survey_tab_surveys'];
        echo '</div>';

        echo '<input type="hidden" name="active_tab" id="active_tab" value="'.$this->tab.'">';
    }

    // Priazemo glavno vsebino glede na aktiven tab
    public function displayTabContent(){
        global $lang;

        // Levi seznam folderjev
        echo '<div id="lib_folder_list" class="content_left">';
        $this->displayFolderList();
        echo '</div>';
        

        // Desna vsebina
        echo '<div class="content_right">';
        
        // Seznam vprasanj
        echo '<div id="lib_question_list" class="question_list">';
        $this->displayQuestionList();
        echo '</div>';
        
        // Item counter
        echo '<div class="selected_items">'.$lang['srv_library_item_counter'].': <span id="selected_item_counter">0</span></div>';

        // Gumbi na dnu
        echo '<div class="button_holder">';
        $this->displayButtons();
        echo '</div>';
        
        echo '</div>';
    }

    // Prikazemo seznam folderjev na levi
    private function displayFolderList(){
		global $global_user_id;
		global $lang;

        // Prikazemo seznam mojih folderjev v knjiznci
        echo '<ul class="folder_list user">';
        $this->displayFolderParent($parent=0, $uid=$global_user_id);
        echo '</ul>';

        // Prikazemo seznam javnih folderjev v knjiznci
        echo '<ul class="folder_list public">';
        $this->displayFolderParent($parent=0, $uid=0);
        echo '</ul>';

        // Prikazemo se skrit div za dodaten popup (rename, add - title)
        echo '<div id="lib_additional_popup" class="divPopUp"></div>';

        // Klik izven "Vec" - zapremo okno
        echo '<script>
            $(document).mouseup(function(e){
                var container = $(".folder_item_settings, .dots_ver");
            
                if (!container.is(e.target) && container.has(e.target).length === 0){
                    $(".folder_item_settings").addClass("displayNone");
                    $(".dots_ver_folder").removeClass("active");
                }

                var container2 = $(".item_settings, .dots_ver");
            
                if (!container.is(e.target) && container.has(e.target).length === 0){
                    $(".item_settings").addClass("displayNone");
                    $(".dots_ver_item").removeClass("active");
                }
            });

            initHideLibraryArrows();
        </script>';
    }

    private function displayFolderParent($parent, $uid) {
        global $lang;
        global $admin_type;
        global $global_user_id;
        global $site_url;


        // Ce gre za root mapo prikazemo z ustreznim jezikom
        if ($parent == 0 && $uid == 0) {
			$language = " AND lang='".$lang['id']."' ";
		}

        // Dobimo vse folderje za parenta
        $sqlFolders = sisplet_query("SELECT id, naslov 
                                FROM srv_library_folder 
                                WHERE uid='".$uid."' AND parent='".$parent."' AND tip='".$this->tab."' ".$language." 
                                ORDER BY naslov
                            ");
        if (!$sqlFolders) 
            echo mysqli_error($GLOBALS['connect_db']);


        // Za prvic ko pride user, da mu dodamo folder
        if (mysqli_num_rows($sqlFolders) == 0 && $uid > 0 && $parent == 0){
            
            // Nastavimo naslov mape
            $naslov = ($this->tab == 0) ? $lang['srv_moja_vprasanja'] : $lang['srv_moje_ankete'];
            
            sisplet_query("INSERT INTO srv_library_folder 
                            (uid, tip, naslov, parent, lang) 
                            VALUES 
                            ('".$uid."', '".$this->tab."', '".$naslov."', '0', '".$lang['id']."')
                        ");
            
            $sqlFolders = sisplet_query("SELECT id, naslov 
                                            FROM srv_library_folder 
                                            WHERE uid='".$uid."' AND parent='".$parent."' AND tip='".$this->tab."'
                                            ORDER BY naslov
                                        ");
            if (!$sqlFolders) 
                echo mysqli_error($GLOBALS['connect_db']);
        }


        // Loop po vseh mapah znotraj parenta
        while ($rowFolders = mysqli_fetch_array($sqlFolders)) {

            // Nastavimo default prvi aktiven folder
            if($this->folder_id == 0 && $parent == 0 && $uid == 0){
                $this->folder_id = $rowFolders['id'];
                $this->setExpandedFolders($this->folder_id);
            }
            
            $this->displayFolderItem($rowFolders['id'], $rowFolders['naslov'], $uid);

            $expanded = in_array($rowFolders['id'], $this->expanded_folders) ? true : false;

            echo '<ul id="folder_list_'.$rowFolders['id'].'" class="folder_list" '.($expanded ? '' : 'style="display:none;"').'>';

            // Rekurzivno izpisemo childe
            $this->displayFolderParent($rowFolders['id'], $uid);

            // Smo tabu ankete - izpisemo se ankete v tem folderju
            if($this->tab == '1'){

                $sqlSurvey = sisplet_query("SELECT a.id, a.naslov FROM srv_anketa a, srv_library_anketa l WHERE a.id=l.ank_id AND l.folder='".$rowFolders['id']."' AND l.uid='".$uid."' ORDER BY naslov ASC");
				if (!$sqlSurvey) echo mysqli_error($GLOBALS['connect_db']);

				if (mysqli_num_rows($sqlSurvey) > 0) {

					while ($rowSurvey = mysqli_fetch_array($sqlSurvey)) {

                        $this->displayFolderItemSurvey($rowSurvey['id'], $rowSurvey['naslov'], $uid);
                    }
                }
            }

            echo '</ul>';
        }
    }

    // Levi item - folder
    private function displayFolderItem($id, $title, $uid) {
        global $lang;
        global $admin_type;

        $expanded = in_array($id, $this->expanded_folders) ? true : false;
        $active = ($this->folder_id == $id) ? true : false;

        echo '<li id="folder_item_'.$id.'" class="folder_item '.($active ? 'active' : '').' '.($expanded ? 'open' : '').' '.(($uid != 0 || $admin_type == 0) ? 'droppable_folder' : '').'" folder-id="'.$id.'" onClick="openLibraryFolder(\''.$id.'\');">';
        

        // Ikoni puscice in mape
        echo '  <span id="folder_arrow_'.$id.'" class="faicon arrow" onClick="expandLibraryFolder(\''.$id.'\');"></span>';
        echo '  <span id="folder_folder_'.$id.'" class="faicon folder"></span>';
        
        // Naslov folderja
        echo '  <span class="folder_item_title">'.$title.'</span>';
        
        // Stevilo itemov v folderju
        //echo '  <span class="folder_item_child_count">'.$child_count.'</span>';
        

        // Urejanje folderja
        if($uid != 0 || $admin_type == 0){
            // Tri pikice za prikaz urejanja folderja
            echo '  <span class="faicon dots_ver dots_ver_folder" onClick="showLibraryFolderEdit(this);"></span>';

            // Skrit div za urejanje folderja
            echo '  <div class="folder_item_settings displayNone">';
            echo '      <ul>';
            echo '          <li onClick="displayAddFolderPopup(\''.$id.'\', \''.$uid.'\');">'.$lang['srv_library_folder_add'].'</li>';
            echo '          <li onClick="displayRenameFolderPopup(\''.$id.'\', \''.$title.'\');">'.$lang['srv_library_folder_rename'].'</li>';
            if($id != $this->root_folder['user'])
                echo '          <li onClick="deleteLibraryFolder(\''.$id.'\');">'.$lang['srv_library_folder_delete'].'</li>';
            echo '      </ul>';
            echo '  </div>';
        }


        echo '</li>';
    }

    // Levi item - anketa
    private function displayFolderItemSurvey($id, $title, $uid) {
        global $lang;

        // Javne ankete
        $type = ($uid == '0') ? 'public' : 'my';

        echo '<li id="'.$type.'_survey_item_'.$id.'" class="folder_item survey_item '.($this->folder_id == $id ? 'active' : '').'" onClick="openLibrarySurvey(\''.$id.'\', \''.$type.'\');">';
        

        // Ikoni puscice in mape
        echo '  <span class="faicon clipboard"></span>';
        
        // Naslov folderja - ankete
        echo '  <span class="folder_item_title">'.$title.'</span>';
                

        echo '</li>';
    }


    // Dodaten popup za ime direktorija pri dodajanju
    private function displayAddFolderPopup($folder_id, $uid){
        global $lang;

        echo '<h2>'.$lang['srv_library_folder_name'].'</h2>';

        echo '<div>';
        echo '  <input type="text" id="lib_folder_name" class="large">';
        echo '</div>';

        echo '<div class="button_holder">';
        echo '  <button class="medium white-blue" onClick="closeAdditionalPopup();">'.$lang['edit1338'].'</button>';
        echo '  <button class="medium blue" onClick="addLibraryFolder(\''.$folder_id.'\', \''.$uid.'\');">'.$lang['srv_library_folder_add'].'</button>';
        echo '</div>';
    }

    // Dodaten popup za ime direktorija pri preimenovanju
    private function displayRenameFolderPopup($folder_id, $folder_name){
        global $lang;

        echo '<h2>'.$lang['srv_library_folder_name'].'</h2>';

        echo '<div>';
        echo '  <input type="text" id="lib_folder_name" class="large" value="'.$folder_name.'">';
        echo '</div>';

        echo '<div class="button_holder">';
        echo '  <button class="medium white-blue" onClick="closeAdditionalPopup();">'.$lang['edit1338'].'</button>';
        echo '  <button class="medium blue" onClick="renameLibraryFolder(\''.$folder_id.'\');">'.$lang['srv_library_folder_rename'].'</button>';
        echo '</div>';
    }


    // Prikazemo seznam vprasanj v folderju
    private function displayQuestionList(){
		global $global_user_id;
		global $lang;

        // Folder ni nastavljen
        if($this->folder_id == 0)
            return;

            
        // Naslov trenutnega folderja
        $sqlCurrentFolder = sisplet_query("SELECT naslov FROM srv_library_folder WHERE id='".$this->folder_id."'");
        $rowCurrentFolder = mysqli_fetch_array($sqlCurrentFolder);
        echo '<div class="folder_title"><span class="faicon folder"></span>'.$rowCurrentFolder['naslov'].'</div>';


        // Seznam vprasanj iz anket v knjiznici
        if($this->tab == 1){

            // Najprej zlistamo vse child folderje
            $sqlFolders = sisplet_query("SELECT id, naslov FROM srv_library_folder WHERE parent='".$this->folder_id."' AND tip='".$this->tab."' ORDER BY naslov");
            if (mysqli_num_rows($sqlFolders) > 0){

                // Loop po vseh mapah znotraj parenta
                while ($rowFolders = mysqli_fetch_array($sqlFolders)) {
                    $this->displayRightFolderItem($rowFolders['id'], $rowFolders['naslov']);
                }
            }

            echo '<div class="spacer"></div>';

            $sqlFolderSurveys = sisplet_query("SELECT a.id, a.naslov FROM srv_anketa a, srv_library_anketa l WHERE a.id=l.ank_id AND l.folder='".$this->folder_id."' ORDER BY naslov ASC");
            while ($rowFolderSurveys = mysqli_fetch_array($sqlFolderSurveys)) {
                
                // Naslov ankete
                echo '<div id="survey_title_'.$rowFolderSurveys['id'].'" class="survey_title" onClick="toggleLibrarySurveyQuestions(\''.$rowFolderSurveys['id'].'\');">';
                echo '  <span class="faicon clipboard"></span>';
                echo '  <span class="title">'.$rowFolderSurveys['naslov'].'</span>';
                echo '</div>';

                
                // Seznam vprasanj v anketi
                echo '<div id="survey_questions_'.$rowFolderSurveys['id'].'" class="survey_questions">';
                
                $sqlQuestions = sisplet_query("SELECT s.id, s.naslov, s.lib_naslov, s.tip FROM srv_spremenljivka s, srv_grupa g WHERE s.gru_id=g.id AND g.ank_id='".$rowFolderSurveys['id']."' ORDER BY g.vrstni_red ASC, s.vrstni_red ASC");
                while ($rowQuestions = mysqli_fetch_array($sqlQuestions)) {

                    $this->displayRightQuestionItem($rowQuestions);     
                }

                echo '</div>';
            }
        }
        // Seznam vprasanj iz knjiznice
        else{

            // Najprej zlistamo vse child folderje
            $sqlFolders = sisplet_query("SELECT id, naslov FROM srv_library_folder WHERE parent='".$this->folder_id."' AND tip='".$this->tab."' ORDER BY naslov");
            if (mysqli_num_rows($sqlFolders) > 0){

                // Loop po vseh mapah znotraj parenta
                while ($rowFolders = mysqli_fetch_array($sqlFolders)) {
                    $this->displayRightFolderItem($rowFolders['id'], $rowFolders['naslov']);
                }
            }

            echo '<div class="spacer"></div>';

            $allow_edit = true;

            // Loop po vseh vprasanjih v folderju
            $sqlFolderQuestions = sisplet_query("SELECT s.id, s.naslov, s.lib_naslov, s.tip, f.uid AS folder_uid 
                                                    FROM srv_spremenljivka s, srv_library_folder f 
                                                    WHERE s.folder='".$this->folder_id."' AND s.gru_id='-1' AND f.id=s.folder
                                                    ORDER BY s.naslov ASC
                                                ");
            while ($rowFolderQuestions = mysqli_fetch_array($sqlFolderQuestions)) {

                $this->displayRightQuestionItem($rowFolderQuestions);     
            }

            // Loop po vseh if-ih/blokih v folderju
            $sqlFolderIfs = sisplet_query("SELECT i.id, i.label, i.tip, f.uid AS folder_uid 
                                            FROM srv_if i, srv_library_folder f 
                                            WHERE i.folder='".$this->folder_id."' AND f.id=i.folder
                                            ORDER BY i.label ASC, i.id ASC
                                        ");
            while ($rowFolderIfs = mysqli_fetch_array($sqlFolderIfs)) {

                $this->displayRightIfItem($rowFolderIfs);     
            }

            // Init drag/drop js
            echo '<script>initDragLibraryItem();</script>';
        }
        
    }

    // Prikazemo seznam vprasanj v anketah iz knjiznice v folderju
    private function displaySurveyQuestionList(){
		global $global_user_id;
		global $lang;

        // Folder ni nastavljen
        if($this->folder_id == 0)
            return;

            
        // Naslov ankete
        SurveyInfo :: getInstance()->SurveyInit($this->folder_id);
        echo '<div class="folder_title"><span class="faicon clipboard"></span>'.SurveyInfo::getSurveyTitle().'</div>';

        $sqlQuestions = sisplet_query("SELECT s.id, s.naslov, s.lib_naslov, s.tip FROM srv_spremenljivka s, srv_grupa g WHERE s.gru_id=g.id AND g.ank_id='".$this->folder_id."' ORDER BY g.vrstni_red ASC, s.vrstni_red ASC");
        while ($rowQuestions = mysqli_fetch_array($sqlQuestions)) {

            $this->displayRightQuestionItem($rowQuestions);     
        }
    }

    // Prikazemo vrstico (item, folder) na desni
    private function displayRightFolderItem($folder_id, $folder_name){
        global $lang;

        echo '<div id="folder_item_holder_'.$folder_id.'" class="folder_item_holder" onClick="openLibraryFolder(\''.$folder_id.'\');">';

        echo '  <div class="folder_item_info">';
        echo '      <span class="faicon folder_empty"></span>';
        echo '      <span class="title">'.$folder_name.'</span>';
        echo '  </div>';

        echo '</div>';
    }

    // Prikazemo 1 vrstico (item, vprasanje) na desni
    private function displayRightQuestionItem($spremenljivka){
        global $lang;
        global $global_user_id;
        global $admin_type;

        echo '<div id="question_item_holder_'.$spremenljivka['id'].'" class="question_item_holder" onClick="selectLibraryItem(\''.$spremenljivka['id'].'\');">';

        echo '  <input type="checkbox" id="question_item_check_'.$spremenljivka['id'].'" item-type="1" item-subtype="1" class="question_item_check" onClick="selectLibraryItem(\''.$spremenljivka['id'].'\');"><label for="question_item_check_'.$spremenljivka['id'].'"></label>';

        echo '  <div id="question_item_info_'.$spremenljivka['id'].'" item-type="1"  item-subtype="1" item-id="'.$spremenljivka['id'].'" class="question_item_info">';
       
        echo '      <span class="faicon list"></span>';

        if($spremenljivka['lib_naslov'] == '')
            echo '      <span class="title">'.substr(strip_tags($spremenljivka['naslov']), 0, 40).'</span>';
        else
            echo '      <span class="title">'.substr(strip_tags($spremenljivka['lib_naslov']), 0, 40).'</span>';
        
        echo '<div class="right_icons">';

        // Preview
        echo '  <span class="faicon monitor" onClick="previewVprasanje(\''.$spremenljivka['id'].'\');"></span>';
        
        // Urejanje Vprasanja
        if($this->tab == '0' && ($spremenljivka['folder_uid'] == $global_user_id || $admin_type == 0)){            
            
            // Tri pikice za prikaz urejanja vprasanja
            echo '  <span class="faicon dots_ver dots_ver_item" onClick="showLibraryItemEdit(this);"></span>';
            
            // Skrit div za urejanje vprasanja
            echo '  <div class="item_settings displayNone">';
            echo '      <ul>';
            echo '          <li onClick="deleteLibraryItem(\''.$spremenljivka['id'].'\', \'1\');">'.$lang['srv_library_delete_q'].'</li>';
            echo '          <li onClick="displayRenameLibraryItemPopup(\''.$spremenljivka['id'].'\', \'1\');">'.$lang['srv_library_rename_q'].'</li>';
            echo '      </ul>';
            echo '  </div>';
        }

        echo '</div>';


        echo '  </div>';

        echo '</div>';
    }

    // Prikazemo 1 vrstico (item, if) na desni
    private function displayRightIfItem($if){
        global $lang;
        global $global_user_id;
        global $admin_type;

        if($if['tip'] == 2){
            $type_string = '<span class="item_type">[LOOP]</span>';
            $icon = 'loop_32';
            $type = 'loop';
            $subtype = '2_2';
        }
        elseif($if['tip'] == 1){
            $type_string = '<span class="item_type">[BLOCK]</span>';
            $icon = 'block_32';
            $type = 'block';
            $subtype = '2_1';
        }
        else{
            $type_string = '<span class="item_type">[IF]</span>';
            $icon = 'if_32';
            $type = 'if';
            $subtype = '2_0';
        }

        $type_string = '';

        echo '<div id="question_item_holder_'.$if['id'].'" class="question_item_holder" onClick="selectLibraryItem(\''.$if['id'].'\');">';

        echo '  <input type="checkbox" id="question_item_check_'.$if['id'].'" item-type="2" item-subtype="'.$subtype.'" class="question_item_check" onClick="selectLibraryItem(\''.$if['id'].'\');"><label for="question_item_check_'.$spremenljivka['id'].'"></label>';

        echo '  <div id="question_item_info_'.$if['id'].'" item-type="2" item-subtype="'.$subtype.'" item-id="'.$if['id'].'" class="question_item_info">';
        
        echo '      <span class="faicon '.$icon.'"></span>';
        echo '      <span class="title">'.$type_string.substr(strip_tags($if['label']), 0, 40).'</span>';
        

        echo '<div class="right_icons">';

        // Urejanje Vprasanja
        if($this->tab == '0' && ($if['folder_uid'] == $global_user_id || $admin_type == 0)){

            // Tri pikice za prikaz urejanja folderja
            echo '  <span class="faicon dots_ver dots_ver_item" onClick="showLibraryItemEdit(this);"></span>';

            // Skrit div za urejanje folderja
            echo '  <div class="item_settings displayNone">';
            echo '      <ul>';
            echo '          <li onClick="deleteLibraryItem(\''.$if['id'].'\', \'2\');">'.$lang['srv_library_delete_'.$type].'</li>';
            echo '          <li onClick="displayRenameLibraryItemPopup(\''.$if['id'].'\', \''.$subtype.'\');">'.$lang['srv_library_rename_'.$type].'</li>';
            echo '      </ul>';
            echo '  </div>';
        }

        echo '</div>';


        echo '  </div>';

        echo '</div>';
    }

    /**
     * Popup pri preimenovanju itema v knjiznici
     */
    private function displayRenameItemPopup($item_id, $type){
        global $lang;
        global $admin_type;
        global $global_user_id;

        // Podtip elementa (vprasanje, blok, if...)
        if($type == '1'){

            // Dobimo naslov
            $sql = sisplet_query("SELECT lib_naslov FROM srv_spremenljivka WHERE id='".$item_id."'");
            $row = mysqli_fetch_array($sql);
            $naslov = substr(strip_tags($row['lib_naslov']), 0, 40);

            $subtype = 'q';
        }
        else{

            // Dobimo  naslov
            $sql = sisplet_query("SELECT label FROM srv_if WHERE id='".$item_id."'");
            $row = mysqli_fetch_array($sql);

            $naslov = substr($row['label'], 0, 40);

            if($type == '2_2')
                $subtype = 'loop';
            elseif($type == '2_1')
                $subtype = 'block';
            else
                $subtype = 'if';
        }
        

        // Naslov okna glede na tip elementa (vprasanje, blok, if...)
        echo '<h2>'.$lang['srv_library_rename_'.$subtype].'</h2>';

        // Vsebina
        echo '<div>';
        echo '  <input type="text" id="lib_element_name" class="large" value="'.$naslov.'" maxlength="40">';
        echo '</div>';

        echo '<div class="button_holder">';
        echo '  <button class="medium white-blue" onClick="closeAdditionalPopup();">'.$lang['edit1338'].'</button>';
        echo '  <button class="medium blue" onClick="renameLibraryItem(\''.$item_id.'\', \''.$type.'\');">'.$lang['srv_library_rename_'.$subtype].'</button>';
        echo '</div>';
    }

    // Dodaten popup za ime direktorija pri preimenovanju
    private function displayRenameQuestionPopup($element_id){
        global $lang;

        echo '<h2>'.$lang['srv_library_folder_name'].'aaa</h2>';

        echo '<div>';
        echo '  <input type="text" id="lib_element_name" class="large" value="'.$folder_name.'">';
        echo '</div>';

        echo '<div class="button_holder">';
        echo '  <button class="medium white-blue" onClick="closeAdditionalPopup();">'.$lang['edit1338'].'</button>';
        echo '  <button class="medium blue" onClick="renameLibraryElement(\''.$element_id.'\', \''.$type.'\');">'.$lang['srv_library_folder_rename'].'</button>';
        echo '</div>';
    }


    // Prikazemo gumbe
    private function displayButtons(){
		global $global_user_id;
		global $lang;

        echo '<button class="medium white-blue" onClick="popupClose();">'.$lang['edit1338'].'</button>';
        echo '<button id="insert_library_button" class="medium blue" disabled="disabled" onClick="insertLibraryItemsIntoSurvey();">'.$lang['srv_library_survey_add'].'</button>';
    }


    /**
     * Popup pri dodajanju itema v knjiznico - potrditev, izbira folderja, urejanje naslova
     *         
     * type        
     * - 0->anketa
     * - 1->vprasanja
     * - 2->if/blok
     */
    private function displayAddIntoLibrary($item_id, $type){
        global $lang;
        global $admin_type;
        global $global_user_id;

        // Podtip elementa (vprasanje, blok, if...)
        if($type == '1'){

            // Dobimo naslov
            $sql = sisplet_query("SELECT naslov, lib_naslov FROM srv_spremenljivka WHERE id='".$item_id."'");
            $row = mysqli_fetch_array($sql);
            $naslov = ($row['lib_naslov'] == '') ? substr(strip_tags($row['naslov']), 0, 40) : substr(strip_tags($row['lib_naslov']), 0, 40);

            $subtype = 'q';
        }
        else{

            // Dobimo  naslov
            $sql = sisplet_query("SELECT label FROM srv_if WHERE id='".$item_id."'");
            $row = mysqli_fetch_array($sql);

            $naslov = substr($row['label'], 0, 40);

            if($type == '2_2')
                $subtype = 'loop';
            elseif($type == '2_1')
                $subtype = 'block';
            else
                $subtype = 'if';
        }
        

        // Naslov okna glede na tip elementa (vprasanje, blok, if...)
        echo '<h2>'.$lang['srv_library_add_to_lib_title_'.$subtype].'</h2>';


        // Vsebina
        echo '<div class="content add_to_lib_content">';

        echo '<label>'.$lang['srv_library_add_to_lib_name_'.$subtype].':</label>';
        echo '<input id="lib_item_title" type="text" value="'.$naslov.'" maxlength="40">';
        
        // Izbira direktorija
        echo '<label>'.$lang['srv_library_add_to_lib_folder'].':</label>';

        $this->getFolderStructure($uid=$global_user_id);
        if($admin_type == 0)
            $this->getFolderStructure($uid='0');

        echo '<select id="lib_item_folder" class="dropdown">';
        foreach($this->folder_structure as $folder){

            echo '<option value="'.$folder['id'].'">';

            for($i=0; $i<$folder['indent']; $i++){
                echo '&nbsp;';
            }

            echo $folder['title'];

            echo '</option>';
        }
        echo '</select>';
 
        echo '</div>';


        echo '<div class="button_holder">';
        echo '  <button class="medium white-blue" onClick="popupClose();">'.$lang['edit1338'].'</button>';
        echo '  <button class="medium blue" onClick="addIntoLibrary(\''.$item_id.'\', \''.$type.'\');">'.$lang['srv_library_add_to_lib'].'</button>';
        echo '</div>';
    }

    private function displayAddIntoLibrarySuccess($item_id, $type, $success){
        global $lang;

        // Podtip elementa (vprasanje, blok, if...)
        if($type == '1'){

            // Dobimo naslov
            $sql = sisplet_query("SELECT lib_naslov FROM srv_spremenljivka WHERE id='".$item_id."'");
            $row = mysqli_fetch_array($sql);
            $naslov = strip_tags($row['lib_naslov']);

            $subtype = 'q';
        }
        else{

            // Dobimo tip (if ali blok) in naslov
            $sql = sisplet_query("SELECT label FROM srv_if WHERE id='".$item_id."'");
            $row = mysqli_fetch_array($sql);

            $naslov = $row['label'];

            if($tip == '2_2')
                $subtype = 'loop';
            elseif($tip == '2_1')
                $subtype = 'block';
            else
                $subtype = 'if';            
        }
        

        // Naslov okna glede na tip elementa (vprasanje, blok, if...)
        echo '<h2>'.$lang['srv_library_add_to_lib_title_'.$subtype].'</h2>';


        // Vsebina
        echo '<div class="content add_to_lib_content">';

        if($success){
            echo $lang['srv_library_add_to_lib_success_'.$subtype];
        }
        else{
            echo $lang['srv_library_add_to_lib_success_err'];
        }

        echo '</div>';


        echo '<div class="button_holder">';
        echo '  <button class="medium blue" onClick="popupClose();">'.$lang['srv_zapri'].'</button>';
        echo '</div>';
    }


    // Dobimo sistemski root folder in root folder uporabnika
    private function setRootFolders(){
        global $global_user_id;
        global $lang;

        // Root folder uporabnika
        $sqlFolder = sisplet_query("SELECT id FROM srv_library_folder WHERE uid='".$global_user_id."' AND parent='0' AND tip='".$this->tab."' AND lang='".$lang['id']."'");

        // Za prvic ko pride user, da mu dodamo folder 
        if (mysqli_num_rows($sqlFolder) == 0){

            // Nastavimo naslov mape
            $naslov = $lang['srv_moja_vprasanja'];
            
            sisplet_query("INSERT INTO srv_library_folder 
                            (uid, tip, naslov, parent, lang) 
                            VALUES 
                            ('".$global_user_id."', '".$this->tab."', '".$naslov."', '0', '".$lang['id']."')
                        ");

            $this->root_folder['user'] = mysqli_insert_id($GLOBALS['connect_db']);
        }
        else{
            $rowFolder = mysqli_fetch_array($sqlFolder);
            $this->root_folder['user'] = $rowFolder['id'];
        }


        // Sistemski root folder
        $sqlFolder = sisplet_query("SELECT id FROM srv_library_folder WHERE uid='0' AND parent='0' AND tip='".$this->tab."' AND lang='".$lang['id']."'");
        $rowFolder = mysqli_fetch_array($sqlFolder);
        
        $this->root_folder['system'] = $rowFolder['id'];
    }

    // Seznam folderjev uporabnika
    private function getFolderStructure($uid, $parent=0, $indent=0){
        global $global_user_id;
        global $admin_type;
        global $lang;


        $sqlFolder = sisplet_query("SELECT id, naslov FROM srv_library_folder WHERE uid='".$uid."' AND parent='".$parent."' AND tip='0' AND lang='".$lang['id']."'");  
        if(mysqli_num_rows($sqlFolder) == 0){
            return;
        }

        while($rowFolder = mysqli_fetch_array($sqlFolder)){

            $this->folder_structure[] = array(
                'id'        =>  $rowFolder['id'],
                'title'     =>  $rowFolder['naslov'],
                'indent'    =>  $indent
            );

            $indent_new = $indent+1;

            $this->getFolderStructure($uid, $rowFolder['id'], $indent_new);
        }       

        return;
    }

  
    /**
    * @desc pohendla ajax klice
    */
    public function ajax () {
        global $lang;
        global $global_user_id;


        $this->tab = (isset($_POST['tab'])) ? (int)$_POST['tab'] : 0;

        // Nastavimo root folder
        $this->setRootFolders();


        // Odpremo popup s knjiznico
    	if ($_GET['a'] == 'displayLibraryPopup') {
            $this->displayLibrary();

            // Div za dodaten popup za preview
            echo '<div id="vprasanje_preview" class="displayNone"></div>';
        } 

        // Preklopimo tab v kniznici
        elseif ($_GET['a'] == 'displayLibraryTabContent') {            
            $this->displayTabContent();
        } 

        // Dodamo folder
        elseif ($_GET['a'] == 'addFolder') {            
            
            $folder_id = $_POST['folder_id'];
            $uid = $_POST['uid'];
            $folder_name = $_POST['folder_name'];

            if($folder_id != '' && $folder_id > 0){
                $s = sisplet_query("INSERT INTO srv_library_folder (uid, tip, naslov, parent, lang) VALUES ('".$uid."', '".$this->tab."', '".$folder_name."', '".$folder_id."', '".$lang['id']."')");
                if (!$s) echo mysqli_error($GLOBALS['connect_db']);
                
                $insert_id = mysqli_insert_id($GLOBALS['connect_db']);

                // Nastavimo za aktivni folder in izrisemo na novo
                $this->folder_id = $insert_id;

                // Posodobimo vse razprte folderje (vsi parenti ce je child aktiven)
                $this->setExpandedFolders($this->folder_id);

                // Prikazemo folderje
                $this->displayFolderList();
            }
        } 

        // Preimenujemo folder
        elseif ($_GET['a'] == 'renameFolder') {            
            
            $folder_id = $_POST['folder_id'];
            $folder_name = $_POST['folder_name'];

            if($folder_id != '' && $folder_id > 0){

                $s = sisplet_query("UPDATE srv_library_folder SET naslov='".$folder_name."' WHERE id='".$folder_id."'");
                if (!$s) echo mysqli_error($GLOBALS['connect_db']);

                // Nastavimo za aktivni folder in izrisemo na novo
                $this->folder_id = $folder_id;

                // Posodobimo vse razprte folderje (vsi parenti ce je child aktiven)
                $this->setExpandedFolders($this->folder_id);

                $this->displayFolderList();
            }            
        } 

        // Brisemo folder
        elseif ($_GET['a'] == 'deleteFolder') {            
            
            $folder_id = $_POST['folder_id'];

            if($folder_id != '' && $folder_id > 0){

                $sql = sisplet_query("SELECT parent FROM srv_library_folder WHERE id = '".$folder_id."'");
                $row = mysqli_fetch_array($sql);

                // Prestavimo vse iteme v parent folder
                if ($this->tab == 0) {
                    sisplet_query("UPDATE srv_spremenljivka SET folder='".$row['parent']."' WHERE folder='".$folder_id."'");
                    sisplet_query("UPDATE srv_if SET folder='".$row['parent']."' WHERE folder='".$folder_id."'");
                } 
                else {
                    sisplet_query("UPDATE srv_library_anketa SET folder='".$row['parent']."' WHERE folder='".$folder_id."'");
                }

                // Prestavimo vse child folderje v parent folder
                sisplet_query("UPDATE srv_library_folder SET parent='".$row['parent']."' WHERE parent='".$folder_id."'");

                // Pobrisemo folder
                sisplet_query("DELETE FROM srv_library_folder WHERE id='".$folder_id."'");

                // Nastavimo parenta za aktivni folder in izrisemo na novo
                $this->folder_id = $row['parent'];

                // Posodobimo vse razprte folderje (vsi parenti ce je child aktiven)
                $this->setExpandedFolders($this->folder_id);

                // Prikazemo fodlderje
                $this->displayFolderList();
            }
        } 

        // Odpremo popup za poimenovanje novega folderja
        elseif ($_GET['a'] == 'addFolderPopup') {     

            $folder_id = $_POST['folder_id'];
            $uid = $_POST['uid'];

            $this->displayAddFolderPopup($folder_id, $uid);
        }

        // Odpremo popup za poimenovanje folderja
        elseif ($_GET['a'] == 'renameFolderPopup') {     

            $folder_id = $_POST['folder_id'];
            $folder_name = $_POST['folder_name'];

            $this->displayRenameFolderPopup($folder_id, $folder_name);
        }

        
        // Odpremo folder z vprasanji v knjiznici
        elseif ($_GET['a'] == 'displayLibraryQuestionList') {

            $this->folder_id = $_POST['folder_id'];

            $this->displayQuestionList();
        } 

        // Odpremo anketo z vprasanji v knjiznici
        elseif ($_GET['a'] == 'displayLibrarySurveyQuestionList') {

            $this->folder_id = $_POST['folder_id'];

            $this->displaySurveyQuestionList();
        } 

        // Dodajanje itema iz knjiznice v vprasalnik
        elseif ($_GET['a'] == 'addIntoSurvey') {
            
            $items = $_POST['items'];
            
            $b = new Branching($this->ank_id);
            $ba = new BranchingAjax($this->ank_id);
            
            foreach(array_reverse($items) as $item){

                $item_array = explode("_", $item);
                $item_id = $item_array[0];
                $item_type = $item_array[1];   

                // Poiscemo zadnji root element v anketi
                /*$sql = sisplet_query("SELECT element_spr, element_if FROM srv_branching WHERE ank_id='$this->anketa' AND parent='$parent' ORDER BY vrstni_red DESC LIMIT 1");
                $row = mysqli_fetch_array($sql);

                // Zdanji element je if oz. blok
                if($row['element_if'] > 0){
                    $endif = 1;
                    $last_spr = 0;
                }
                // Zadnji element je vprasanje
                else{
                    $endif = 0;
                    $last_spr = $row['element_spr'];
                }*/

                // Dodamo if/blok v anketo
                if($item_type == '2'){

                    //$spremenljivka = $last_spr;
                    $copy = $item_id;
                                        
                    $sqln = sisplet_query("SELECT MAX(i.number) AS number FROM srv_if i, srv_branching b WHERE b.ank_id='$this->ank_id' AND b.element_if=i.id");
                    $rown = mysqli_fetch_array($sqln);      
                    $number = $rown['number'] + 1;

                    // Dobimo tip (if ali blok)
                    $sqlt = sisplet_query("SELECT tip FROM srv_if WHERE id='".$item_id."'");
                    $rowt = mysqli_fetch_array($sqlt);
                    $tip = $rowt['tip'];
                    
                    $sql = sisplet_query("INSERT INTO srv_if (id, number, tip) VALUES ('', '$number', '$tip')");
                    if (!$sql) echo mysqli_error($GLOBALS['connect_db']);
                    
                    $if_id = mysqli_insert_id($GLOBALS['connect_db']);

                    $row['parent'] = 0;
					$row['vrstni_red'] = 99999999;

                    $next_element = $b->find_next_element($row['parent'], $row['vrstni_red']);
                    
                    if ($next_element == null) {	// next_element je prazen na koncu ifa, takrat je tudi nov if prazen
                        $next_element['parent'] = 0;
                        $next_element['vrstni_red'] = 99999999;
                        $next_element['element_spr'] = 0;
                        $next_element['element_if'] = 0;
                    }

                    // dodajamo loop - preverimo da ga ne zelimo vgnezditi v drug loop
                    if ($tip == 2) {
                        // preverimo, da ga ne dodamo v ze obstojec loop
                        if ($b->find_loop_parent($next_element['parent']) > 0)
                            $add = false;
                        
                        // preverimo, da ge ne dodamo direktno pred obstojec loop (ker potem objame obstojec loop in dobimo vgnezdenje)
                        if ($next_element['element_if'] > 0)
                            if ($b->find_loop_child($next_element['element_if']) > 0)
                                $add = false;
                    }
                    
                    $b->if_new($endif=1, $next_element['parent'], $if_id, $next_element['vrstni_red'], $next_element['element_spr'], $next_element['element_if'], $copy, $no_content=false, $include_element=false);

                    $ba->check_loop();
                    $b->repare_vrstni_red();
                }
                // Dodamo vprasanje v anketo na konec
                else{             
                    $ba->spremenljivka_new($spremenljivka=0, $if=0, $endif=1, $copy=$item_id, $drop=0);
                    $b->repare_vrstni_red();
                }      
            }

            Common::getInstance()->Init($this->ank_id);
            Common::getInstance()->prestevilci(0, $all=true);

            $b->branching_struktura();
        } 

        // Drop itema v drug folder
        elseif ($_GET['a'] == 'dropItem') {
            
            $item_id = $_POST['item_id'];
            $item_type = $_POST['item_type'];
            $folder_id = $_POST['folder_id'];

            if($item_id != '' && $item_id > 0){

                // if/blok
                if ($item_type == '2') {
                    sisplet_query("UPDATE srv_if SET folder='".$folder_id."' WHERE id='".$item_id."'");
                } 
                // spremenljivka
                else {
                    sisplet_query("UPDATE srv_spremenljivka SET folder='".$folder_id."' WHERE id='".$item_id."'");
                }
            }
        } 

        // Izbris itema iz knjiznice
        elseif ($_GET['a'] == 'deleteItem') {
            
            $item_id = $_POST['item_id'];
            $item_type = $_POST['item_type'];

            if($item_id != '' && $item_id > 0){

                // if/blok
                if ($item_type == '2') {
                    sisplet_query("DELETE FROM srv_if WHERE id='".$item_id."' AND folder!='0'");
                } 
                // spremenljivka
                else {
                    sisplet_query("DELETE FROM srv_spremenljivka WHERE id='".$item_id."'AND folder!='0'");
                }
            }
        } 

        // Odpremo popup za preimenovanje itema
        elseif ($_GET['a'] == 'displayRenameLibraryItemPopup') {

            $item_id = $_POST['item_id'];
            $type = $_POST['type'];

            $this->displayRenameItemPopup($item_id, $type);
        } 

        // Preimenujemo itema
        elseif ($_GET['a'] == 'renameItem') {            
            
            $item_id = $_POST['item_id'];
            $type = $_POST['type'];
            $title = $_POST['title'];

            $this->folder_id = $_POST['folder_id'];

            if($item_id != '' && $item_id > 0){

                if($type == '1'){
                    $s = sisplet_query("UPDATE srv_spremenljivka SET lib_naslov='".$title."' WHERE id='".$item_id."'");
                    if (!$s) echo mysqli_error($GLOBALS['connect_db']);
                }
                elseif($type == '2_0' || $type == '2_1' || $type == '2_2'){
                    $s = sisplet_query("UPDATE srv_if SET label='".$title."' WHERE id='".$item_id."'");
                    if (!$s) echo mysqli_error($GLOBALS['connect_db']);
                }

                $this->displayQuestionList();
            }            
        } 
        

        // Odpremo popup za dodajanje itema v knjiznico
        elseif ($_GET['a'] == 'displayAddIntoLibraryPopup') {

            $item_id = $_POST['item_id'];
            $type = $_POST['type'];

            $this->displayAddIntoLibrary($item_id, $type);
        } 

        // Dodajanje itema v knjiznico
        elseif ($_GET['a'] == 'addIntoLibrary') {
            
            $item_id = $_POST['item_id'];
            $type = $_POST['type'];
            $title = $_POST['title'];
            $folder_id = $_POST['folder_id'];
            
            if($folder_id == '' || $folder_id == 0)
                $folder_id = $this->root_folder['user'];
            
            if ($item_id > 0) {

                $b = new Branching($this->ank_id);

                // v knjiznico dodamo spremenljivko
                if($type == '1') {
                    $id = $b->nova_spremenljivka(-1, 0, 0, $item_id);
                    sisplet_query("UPDATE srv_spremenljivka SET folder='".$folder_id."', lib_naslov='".$title."' WHERE id='".$id."'");            
                    
                }
                // v knjiznico dodamo if/blok
                elseif($type == '2_0' || $type == '2_1' || $type == '2_2') {
                    $id = $b->if_copy(0, $item_id, true);
                    sisplet_query("UPDATE srv_if SET folder='".$folder_id."', label='".$title."' WHERE id='".$id."'");
                } 
            }
        } 
    }
}

?>
