<?php
/** 
 * 
 * 
 * Class za posiljanje sporocil uporabnikom (v mojih anketah)
 * 
 *
 */

 
class Notifications {
		
	function __construct() {
		global $admin_type, $global_user_id;

	}
	
	
	public function display($tab=0){
		global $admin_type, $global_user_id, $lang;

		// Prikazemo poslana sporocila
		if((isset($_GET['t']) && $_GET['t']=='sent') || $tab == 1){

			echo '<div class="notifications_content">';

			// Izpis vseh poslanih sporocil
			if($admin_type == 0){
				echo '<div class="notifications_content_left">';
				
				echo '<div class="title">';
				echo '<div class="title_icon"><span class="faicon inv_sent_1"></span></div>';
				echo '<h2>'.$lang['srv_notifications_sent'].'</h2>';
				echo '</div>';
				
				$this->displaySentMessages();
				echo '</div>';
				
				// obrazec za posiljanje sporocil
				echo '<div class="notifications_content_right">';
				
				echo '<div class="title">';
				echo '<div class="title_icon"><span class="faicon inv_sent_0"></span></div>';
				echo '<h2>'.$lang['srv_inv_message_draft_new'].'</h2>';
				echo '</div>';

				$this->sendMessageForm();
				echo '</div>';

			}
			else
				echo $lang['srv_notifications_admin_alert'];
			
			echo '</div>';
		}
		// Prikazemo prejeta sporocila
		else{

			echo '<div class="title">';
			echo '<div class="title_icon"><span class="faicon fa-inbox"></span></div>';
			echo '<h2>'.$lang['srv_notifications_recieved'].'</h2>';
			echo '</div>';

			echo '<div class="notifications_content">';

			echo '<div class="notifications_content_left">';
			echo '<div class="recieved_list">';
			$this->displayRecievedMessages();
			echo '</div>';
			echo '</div>';
			
			echo '<div class="notifications_content_right">';
			echo '<div id="message" style="display:none;"></div>';
			echo '</div>';


			echo '</div>';

		}
	}
	
	private function displayRecievedMessages($active=0){
		global $admin_type, $global_user_id, $lang;
		
		// Napolnimo array prejetih sporocil
		$recievedMessages = array();
		
		$sql = sisplet_query("SELECT n.id AS id, n.recipient AS recipient, n.viewed AS viewed, m.id AS message_id, m.send_time AS send_time, m.title AS title, m.text AS text
								FROM srv_notifications n, srv_notifications_messages m 
								WHERE n.recipient='".$global_user_id."' AND n.message_id=m.id
								ORDER BY m.send_time DESC");
		while($row = mysqli_fetch_array($sql)){
			$recievedMessages[$row['id']] = $row;
		}	
				
		
		echo '<ul>';
		
		if(count($recievedMessages) > 0){
			foreach($recievedMessages as $message){
				echo '<li class="'.($active > 0 && $message['id'] == $active ? ' active':'').'" onclick="viewMessage(\''.$message['id'].'\'); return false;">';

				echo '<span class="faicon fa-circle '.($message['viewed'] == 0 ? ' unread':'').'"></span>';

				echo '<div class="title_row">'; //sem noter pride še oznaka, če je obvestilo ali sporočilo
				echo '<div class="subject">';
				echo $message['title'];
				echo '</div>';
				echo '<div class="date">';
				echo date('Y-m-d H:i:s', strtotime($message['send_time']));
				echo '</div>';
				echo '</div>';
				
				$text = strip_tags($message['text']);
				echo '<div class="content">';
				echo $text;
				echo '</div>';
				
				echo '</li>';
			}
		}
		
		echo '</ul>';
	}
	
	private function displaySentMessages(){
		global $admin_type, $global_user_id, $lang;

		// Napolnimo array poslanih sporocil
		$sentMessages = array();
		
		$sql = sisplet_query("SELECT * FROM srv_notifications_messages WHERE author='".$global_user_id."' ORDER BY send_time DESC");
		while($row = mysqli_fetch_array($sql)){
			$sentMessages[$row['id']] = $row;
		}
		
		if(count($sentMessages) > 0){

			echo '<div class="sent_list">';
			echo '<ul>';

			foreach($sentMessages as $message_id => $message){
				echo '<li>';

				echo '<div class="title_row">'; //sem noter pride še oznaka, če je obvestilo ali sporočilo
				echo '<div class="subject">';
				echo $message['title'];
				echo '</div>';
				echo '<div class="date">';
				echo date('Y-m-d H:i:s', strtotime($message['send_time']));
				echo '</div>';
				echo '</div>';
				
				$text = strip_tags($message['text']);
				echo '<div class="content">';
				echo $text;
				echo '</div>';
				
				// Gumb da se sporocilo razresi vsem  (kot da so ga pregledali)
				$sqlN = sisplet_query("SELECT message_id FROM srv_notifications WHERE message_id='".$message_id."' AND viewed='0'");
				$count = mysqli_num_rows($sqlN);
				if($count > 0){
					echo '<div class="resolve" onclick="resolveMessages(\''.$message_id.'\'); return false;">';
					echo '<span class="faicon fa-check"></span>';
					echo $lang['srv_notifications_sent_resolve'].' ('.$count.')';
					echo '</div>';
				}
				
				echo '</li>';
			}
			echo '</ul>';
			echo '</div>';
		}
		else //Ni poslanih sporočil
			echo '<p>'.$lang['srv_notifications_sent_none'].'.</p>';		
	}
	
	// Obrazec za posiljanje sporocila
	private function sendMessageForm($note=''){
		global $admin_type, $global_user_id, $lang;
		
		echo '<div class="send_form">';

		//Prejemniki
		echo '<div class="setting_holder">';
		echo '<span class="setting_title">'.$lang['srv_notifications_send_reciever2'].':</span>';

		echo '<div class="setting_item">';
		echo '<input type="checkbox" value="1" name="recipient_all_slo" id="recipient_all_slo" onChange="recipient_toggle(\'slo\');">';
		echo '<label for="recipient_all_slo">'.$lang['srv_notifications_send_all_slo'].'</label>';
		echo '</div>';
		echo '<div class="setting_item">';
		echo '<input type="checkbox" value="1" name="recipient_all_ang" id="recipient_all_ang" onChange="recipient_toggle(\'ang\');">';
		echo '<label for="recipient_all_ang">'.$lang['srv_notifications_send_all_ang'].'</label>';
		echo '</div>';
		echo '<div class="setting_item">';
		echo '<input type="checkbox" value="1" name="recipient_custom" id="recipient_custom" onChange="recipient_toggle(\'custom\');">';
		echo '<label for="recipient_custom">'.$lang['srv_notifications_send_custom'].'</label>';
		echo '<input type="text" class="text large disabled" name="recipient" id="recipient">';
		echo '</div>';
					
		echo '</div>';

        // Naslov sporocila
		echo '<div class="setting_holder">';
		echo $lang['srv_notifications_send_title'].':';
		echo '<input type="text" class="text large" name="title">';
		echo '</div>';

        // Besedilo sporocila (editor)
		echo $lang['srv_notifications_send_text'].':';
		echo '<div class="label">';
		echo '<textarea id="notification" name="notification" autocomplete="off"></textarea>';
		echo '</div>';
		
		// Avtomatsko prikaži obvestilo po prijavi
		echo '<div class="auto_checkbox">';
		echo '<input type="checkbox" value="1" name="force_show" id="force_show">';
		echo '<label for="force_show">'.$lang['srv_notifications_force_show'].'</label>';
		echo '</div>';

		//Error notes
		if($note != ''){
			echo '<p class="top16">'.$note.'</p>';
		}

		// Gumb pošlji
		echo '<div class="button_holder '.($note != '' ? 'top16' : '').'">';
		echo '<button class="medium blue" onclick="sendNotification(); return false;">'.$lang['srv_notifications_send'].'</button>';
		echo '</div>';
		
		echo '</div>';

        // Inicializiramo editor
        echo '<script type="text/javascript">create_editor_notification(\'notification\');</script>';
	}	
	
	
	// Vrnemo stevilo sporocil
	public function countMessages($type='unread'){
		global $global_user_id;
		
		$count = 0;

		switch($type){
			case 'recieved':
				$sql = sisplet_query("SELECT COUNT(*) AS count FROM srv_notifications WHERE recipient='$global_user_id' AND viewed='1'");
				break;
				
			case 'sent':
				$sql = sisplet_query("SELECT COUNT(n.*) AS count FROM srv_notifications n, srv_notifications_messages m WHERE m.author='$global_user_id' AND m.id=n.message_id");
				break;

			case 'unread':
			default:
				$sql = sisplet_query("SELECT COUNT(*) AS count FROM srv_notifications WHERE recipient='$global_user_id' AND viewed='0'");
				break;
		}

		
		if(mysqli_num_rows($sql) > 0){
			$row = mysqli_fetch_array($sql);
			$count = $row['count'];
		}

		return $count;
	}
	
	// Preveri ce avtomatsko prikaze sporocilo po prijavi
	public function checkForceShow(){
		global $global_user_id;
		
		$sql = sisplet_query("SELECT m.force_show FROM srv_notifications n, srv_notifications_messages m WHERE n.message_id=m.id AND n.recipient='$global_user_id' AND n.viewed='0' AND m.force_show='1'");
		
		if(mysqli_num_rows($sql) > 0)
			return true;
		else
			return false;		
	}
	
	
	public function ajax() {
		global $global_user_id;
		
		switch ($_GET['a']) {
			case 'sendNotification':
				$this->ajax_sendNotification();
				break;
			case 'viewMessage':
				$this->ajax_viewMessage();
				break;
			case 'viewUnreadMessages':
				$this->ajax_viewUnreadMessages();
				break;
			case 'viewGDPRMessage':
				$this->ajax_viewGDPRMessage();
				break;
			case 'saveGDPRAgree':
				$this->ajax_saveGDPRAgree();
				break;	
			case 'resolveMessages':
				$this->ajax_resolveMessages();
				break;
			default:
				break;				
		}
	}
	
	public function ajax_sendNotification(){
		global $lang, $global_user_id;
		
		if(isset($_POST['recipient']))
			$recipient = $_POST['recipient'];
		if(isset($_POST['recipient_all_slo']))
			$recipient_all_slo = $_POST['recipient_all_slo'];
		if(isset($_POST['recipient_all_ang']))
			$recipient_all_ang = $_POST['recipient_all_ang'];
		if(isset($_POST['title']))
			$title = $_POST['title'];
		if(isset($_POST['notification']))
			$notification = $_POST['notification'];
		if(isset($_POST['force_show']))
			$force_show = $_POST['force_show'];
		
				
		// Ce posiljamo vsem uporabnikom nimamo maila
		if($recipient_all_slo == 1 || $recipient_all_ang == 1){
			
			// Dodamo novo sporocilo v bazo
			$sql = sisplet_query("INSERT INTO srv_notifications_messages (author, send_time, title, text, force_show) VALUES ('".$global_user_id."', NOW(), '".$title."', '".$notification."', '".$force_show."')");
			$message_id = mysqli_insert_id($GLOBALS['connect_db']);
			
			$note = '';
					
			// Loop cez vse uporabnike v bazi - vsakemu dodamo sporocilo
			if($recipient_all_slo == 1 && $recipient_all_ang == 1)
				$sqlU = sisplet_query("SELECT id, name, surname, email FROM users WHERE status!='0' AND status!='5' AND status!='6'");
			elseif($recipient_all_ang == 1)
				$sqlU = sisplet_query("SELECT id, name, surname, email FROM users WHERE status!='0' AND status!='5' AND status!='6' AND lang='2'");
			else
				$sqlU = sisplet_query("SELECT id, name, surname, email FROM users WHERE status!='0' AND status!='5' AND status!='6' AND lang='1'");
			while($rowU = mysqli_fetch_array($sqlU)){
	
				$sql = sisplet_query("INSERT INTO srv_notifications (message_id, recipient, viewed) VALUES ('".$message_id."', '".$rowU['id']."', '0')");
				if(!$sql) 
					$note .= '<span class="red">'.mysqli_error($GLOBALS['connect_db']).'</span>';	
			}		
		}
		// Posiljamo na specificen mail
		else{		
			// Dobimo user id glede na vnesen mail prejemnika
			$sqlU = sisplet_query("SELECT id, name, surname, email FROM users WHERE email='$recipient'");
			if(mysqli_num_rows($sqlU) > 0){
				$rowU = mysqli_fetch_array($sqlU);
				
				// Dodamo novo sporocilo v bazo
				$sql = sisplet_query("INSERT INTO srv_notifications_messages (author, send_time, title, text, force_show) VALUES ('".$global_user_id."', NOW(), '".$title."', '".$notification."', '".$force_show."')");
				$message_id = mysqli_insert_id($GLOBALS['connect_db']);
				
				// Dodamo novo sporocilo v bazo
				$sql = sisplet_query("INSERT INTO srv_notifications (message_id, recipient, viewed) VALUES ('".$message_id."', '".$rowU['id']."', '0')");
				if(!$sql) 
					$note = '<span class="red">'.mysqli_error($GLOBALS['connect_db']).'</span>';
				else
					$note = '<span class="faicon success blue link-right"></span>'.$lang['srv_notifications_sent_success'].' '.$recipient.' ('.$rowU['name'].' '.$rowU['surname'].').';		
			}
			else{
				$note = '<span class="red">'.$lang['srv_notifications_sent_nouser1'].$recipient.$lang['srv_notifications_sent_nouser1'].'</span>';
			}
		}
		
		echo '<div class="notifications_content">';

		// Refresh vseh poslanih sporocil
		echo '<div class="notifications_content_left">';
				
		echo '<div class="title">';
		echo '<div class="title_icon"><span class="faicon inv_sent_1"></span></div>';
		echo '<h2>'.$lang['srv_notifications_sent'].'</h2>';
		echo '</div>';

		$this->displaySentMessages();
		echo '</div>';
		
		// Refresh obrazca za posiljanje sporocil
		echo '<div class="notifications_content_right">';
				
		echo '<div class="title">';
		echo '<div class="title_icon"><span class="faicon inv_sent_0"></span></div>';
		echo '<h2>'.$lang['srv_inv_message_draft_new'].'</h2>';
		echo '</div>';

		$this->sendMessageForm($note);
		echo '</div>';

		echo '</div>';
	}
	
	public function ajax_resolveMessages(){
		
		if(isset($_POST['id']))
			$message_id = $_POST['id'];
			
		// Oznacimo sporocila kot pregledana
		$sql = sisplet_query("UPDATE srv_notifications SET viewed='1' WHERE message_id='$message_id'");
		
		$this->displaySentMessages();
	}
	
	public function ajax_viewMessage(){
		global $lang, $global_user_id;
		
		if(isset($_POST['id']))
			$id = $_POST['id'];
		
		
		// Oznacimo sporocilo kot pregledano
		$sql = sisplet_query("UPDATE srv_notifications SET viewed='1' WHERE id='$id'");
		
		// Dobimo sporocilo
		$sql = sisplet_query("SELECT n.id AS id, m.title AS title, m.text AS text, m.send_time AS send_time 
								FROM srv_notifications n, srv_notifications_messages m 
								WHERE n.id='$id' AND m.id=n.message_id");
		$row = mysqli_fetch_array($sql);
		
		// Refresh vseh prejetih sporocil
		echo '<div class="title">';
		echo '<div class="title_icon"><span class="faicon fa-inbox"></span></div>';
		echo '<h2>'.$lang['srv_notifications_recieved'].'</h2>';
		echo '</div>';

		echo '<div class="notifications_content">';

		echo '<div class="notifications_content_left">';
		echo '<div class="recieved_list">';
		$this->displayRecievedMessages($active=$id);
		echo '</div>';
		echo '</div>';

		// Prikaz izbranega sporocila
		echo '<div class="notifications_content_right">';
		echo '<div id="message">';

		echo '<div class="subject">';
		echo $row['title'];
		echo '</div>';
		
		echo '<div class="date">';
		echo '('.date('Y-m-d H:i:s', strtotime($row['send_time'])).')';
		echo '</div>';
		
        // Stara sporocila so brez editorja
        $text = (strtotime($row['send_time']) < strtotime('2021-08-26')) ? nl2br($row['text']) : $row['text'];
		echo '<div class="content">';
		echo $text;
		echo '</div>';

		echo '</div>';
		
		echo '<div class="clr"></div>';
		
		echo '</div>'; //div.notifications_content
	}
	
	public function ajax_viewUnreadMessages($active=0){
		global $lang, $global_user_id;	
		
		echo '<h2><span class="faicon envelope-open"></span>'.$lang['srv_notifications_unread'].'</h2>';
		
		echo '<div class="unread_notifications_holder">';
		
		// Loop cez vsa neprebrana sporocila
		$sql = sisplet_query("SELECT n.id AS id, m.title AS title, m.text AS text, m.send_time AS send_time, n.viewed AS viewed 
								FROM srv_notifications n, srv_notifications_messages m 
                                WHERE n.recipient='".$global_user_id."' AND n.viewed='0' AND m.id=n.message_id 
								ORDER BY m.send_time DESC");
		while($row = mysqli_fetch_array($sql)){
			echo '<div class="unread_notification '.($row['viewed'] == 0 ? ' unread':'').' '.($active > 0 && $row['id'] == $active ? ' active':'').'">';
			
			echo '<span class="bold">'.$row['title'].' <span class="italic">('.date('Y-m-d H:i:s', strtotime($row['send_time'])).')</span></span>';
			
            // Stara sporocila so brez editorja
            $text = (strtotime($row['send_time']) < strtotime('2021-08-26')) ? nl2br($row['text']) : $row['text'];
			echo '<br />'.$text;
			
			echo '</div>';
		}
		
		echo '</div>';
		
				
		// Gumb za zapiranje
		echo '<div class="button_holder">';
		echo '  <button class="medium blue" onclick="closeUnreadMessages(); return false;">'.$lang['srv_zapri'].'</button>';
		echo '</div>';
	
	
		// Oznacimo sporocilo kot pregledano
		$sql = sisplet_query("UPDATE srv_notifications SET viewed='1' WHERE recipient='".$global_user_id."' AND viewed='0'");
	}
	
	public function ajax_viewGDPRMessage(){
		global $lang, $global_user_id;	
		
		echo '<h2><span class="faicon envelope-open"></span>'.$lang['srv_notifications_unread'].'</h2>';
        
        /* if(isAAI()){
            echo '<span class="bold">'.$lang['srv_gdpr_notification_title_aai'].'</span>';
        }
        else{
            echo '<span class="bold">'.$lang['srv_gdpr_notification_title'].'</span>';
        }
		
		echo '<br /><br />'; */
		
		echo $lang['srv_gdpr_notification_text1'];
		
		echo '<br /><br />';
		
		echo $lang['srv_gdpr_notification_question'];
		echo '<br /><br />';
		echo '<div class="spaceLeft gdpr_popup_radio"><input type="radio" name="gdpr_agree" id="gdpr_agree_1" value="1" onClick="enableGDPRPopupButton();"> <label for="gdpr_agree_1">'.$lang['srv_gdpr_notification_da'].'</label></div>';
		//echo ' <a href="#" onClick="toggleGDPRMore();"><span class="bold">'.$lang['srv_invitation_nonActivated_more'].'</span></a>';
		echo '<div class="spaceLeft gdpr_popup_radio"><input type="radio" name="gdpr_agree" id="gdpr_agree_0" value="0" onClick="enableGDPRPopupButton();"> <label for="gdpr_agree_0">'.$lang['srv_gdpr_notification_ne'].'</label></div>';

		echo '<div class="italic red gdpr_popup_radio">'.$lang['srv_gdpr_notification_reminder'].'</div>';

		echo '<br />';
		
		echo $lang['srv_gdpr_notification_unsubscribe'];
		
				
		// Gumb za zapiranje
		echo '<div class="button_holder" id="GDPR_popup_button" style="visibility: hidden;">';            
        echo '<button class="medium blue" onclick="saveGDPRMessage(); return false;">'.$lang['save'].'</button>';
        echo '</div>';
	}
	
	public function ajax_saveGDPRAgree(){
		global $global_user_id;
		
		if(isset($_POST['gdpr_agree']))
			$gdpr_agree = $_POST['gdpr_agree'];
			
		// Oznacimo sporocila kot pregledana
		$sql = sisplet_query("UPDATE users SET gdpr_agree='".$gdpr_agree."' WHERE id='".$global_user_id."'");
	}
}
?>