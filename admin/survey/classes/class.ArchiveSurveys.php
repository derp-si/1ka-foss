<?php

/*
    Moznost masovnega izvoza in uvoza vseh anket uporabnika
*/


class ArchiveSurveys{


    public function __construct($anketa = null){
        global $site_path;
        global $site_url;

        define("ARCHIVE_FOLDER", $site_path.'admin/survey/SurveyBackup/');
        define("ARCHIVE_URL", $site_url.'admin/survey/SurveyBackup/');
    }


    public function displayUserSurveys(){
        global $site_path;
        global $global_user_id;

        echo '<form id="survey_archive_form">';

        echo '<ul>';

        // Loop cez vse ankete kjer je user avtor
        $sql = sisplet_query("SELECT id, naslov, akronim FROM srv_anketa WHERE insert_uid='".$global_user_id."' AND active!='-1'");
		while ($row = mysqli_fetch_array($sql)) {

            echo '<li>';
            echo '  <input type="checkbox" name="survey_archive[]" id="survey_archive_'.$row['id'].'" class="survey_archive_checkbox" value="'.$row['id'].'" onChange="archive_surveys_export_counter();">';
            echo '  <label for="survey_archive_'.$row['id'].'">'.$row['naslov'].'</label>'; 
            echo '</li>';
        }

        echo '</ul>';

        echo '</form>';
    }


    // Izvedemo izvoz anket v paket
    private function createSurveysArchive($survey_list, $data=true){
        global $connect_db;
        global $global_user_id;
        global $lang;

        // Nastavimo nastavitve php-ja, da zmore sprocesirati
        //set_time_limit(1800);
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 600);


        // Pobrisemo zip ce obstaja od prej
        if (file_exists(ARCHIVE_FOLDER.'archive_surveys_'.$global_user_id.'.zip')) {
			unlink(ARCHIVE_FOLDER.'archive_surveys_'.$global_user_id.'.zip');
		}


        // Gremo cez seznam anket in ustvarimo .1ka izvoz za vsako posebej in jo dodamo v zip
        $surveys_for_deletion = array();
        foreach($survey_list as $ank_id){	

             // Odpremo oz. ustvarimo zip
            $zip = new ZipArchive();
            $zip_file = ARCHIVE_FOLDER.'archive_surveys_'.$global_user_id.'.zip';

            if ($zip->open($zip_file, ZipArchive::CREATE) !== true) {
                exit('Napaka pri ustvarjanju zip arhiva anket.');
            }

			SurveyCopy::setSrcSurvey($ank_id);
			SurveyCopy::setSrcConectDb($connect_db);
			SurveyCopy::setDestSite(0);
			
			//SurveyCopy::downloadArrayFile($data);
			$export_survey_array = SurveyCopy::getArrayVar($data);
            $export_survey_string =  base64_encode(serialize($export_survey_array));

            // Ustvarimo datoteko za anketo
            $export_survey_file_name =  $export_survey_array['srv_anketa'][0]['naslov'].' '.date("j.n.Y").'.1ka';
            $export_survey_file_content = $export_survey_string;

            // Dodamo vsebino kot datoteka v zip
            $zip->addFromString($export_survey_file_name, $export_survey_file_content);

            // Zapremo zip
            $zip->close();
        }

        return $lang['srv_archive_surveys_export_created'];
    }

    // Izvedemo uvoz paketa anket
    private function importSurveysArchive($zipFile){
        global $lang;
        global $connect_db;
        global $admin_type;

        // Nastavimo nastavitve php-ja, da zmore sprocesirati
        //set_time_limit(1800);
        ini_set('memory_limit', '1024M');
        ini_set('max_input_time', 600);


        $zip = new ZipArchive();

        // Razpakiramo zip paket
        if ($zip->open($zipFile) === true) {

            $success_count = 0;
            $error_surveys = array();
                        
            SurveyCopy::setSrcSurvey(-1);
            SurveyCopy::setSrcConectDb($connect_db);
            SurveyCopy::setDestSite(0);

            // loop cez vse arhivske datoteke v zip paketu
            for ($i = 0; $i < $zip->numFiles; $i++) {

                try{
                    // Dobimo ime datoteke in vsebino
                    $fileName = $zip->getNameIndex($i);
                    $fileContent = $zip->getFromIndex($i);

                    $array = unserialize(base64_decode($fileContent));
                    
                    if (is_array($array) ) {
                        
                        SurveyCopy::setSourceArray($array);
                            
                        // Ce je vse ok ustvarimo kopijo ankete
                        if(SurveyCopy::getErrors() == ''){
                            $new_survey_id = SurveyCopy::doCopy();
                            $success_count++;
                        }
                        else{
                            $error_surveys[] = array('name'=>$fileName, 'error'=>SurveyCopy::getErrors());
                        }   
                    }
                }
                catch (Exception $e) {
                    $error_surveys[] = array('name'=>$fileName, 'error'=>$e->getMessage());
                }

                // Resetiramo parametre classa - errorje in source array
                SurveyCopy::reset();
            }
    
            $zip->close();
    

            // Uspesno uvozene ankete
            echo '<p>'.$lang['srv_archive_surveys_import_success'].': <span class="semi-bold">'.$success_count.'</span></p>';
        
            // Izpisemo se ankete z errorji
            if(count($error_surveys) > 0){

                // Stevilo anket z napako
                echo '<p>'.$lang['srv_archive_surveys_import_error_details'].': <span class="semi-bold red">'.count($error_surveys).'</span></p>';

                // Loop cez vse ankete z napako
                echo '<div class="error_details" style="overflow-y:auto; max-height:50vh;">';
                foreach($error_surveys as $error_survey){
                    echo '<p class="red top16 semi-bold">'.$error_survey['name'].'</p>';

                    // Samo adminu izpišemo dejanski error
                    if($admin_type == 0)
                        echo '<p class="red italic">'.$error_survey['error'].'</p>'; 
                }
                echo '</div>';
            }
        } 
        // Zipa ni bilo mogoce odpreti
        else {
            echo '<p class="red">'.$lang['srv_archive_surveys_import_error_import'].'</p>';
        }
    }


    public function ajax(){
		global $lang;
		global $global_user_id;
		
        // Prikazemo popup s seznamom anket upporabnika za izvoz
		if ($_GET['a'] == 'display_export') {
			
            echo '<div class="popup_close"><a href="#" onClick="popupClose(); return false;">✕</a></div>';
			
            echo '<h2>'.$lang['srv_archive_surveys_export'].'</h2>';

            
            echo '<div class="popup_main archive_surveys">';
            $this->displayUserSurveys();
            echo '</div>';

            // Masovno oznacevanje checkboxov
            echo '<div class="mass_select archive_surveys">';
            echo '  <span id="switch_on"  onClick="archive_surveys_export_toggle(1);" title="'.$lang['srv_select_all'].'"><span class="faicon checkbox-empty" /> '.$lang['srv_select_all'].'</span>';
            echo '  <span id="switch_off" class="displayNone" onClick="archive_surveys_export_toggle(0);" title="'.$lang['srv_deselect_all'].'"><span class="faicon minus_square" /> '.$lang['srv_deselect_all'].'</span>';
            
            echo '  <div class="mass_select_counter">'.$lang['srv_archive_surveys_export_counter'].': <span id="mass_select_counter_number">0</span></div>';
            echo '</div>';


            echo '<div class="button_holder archive_surveys">';
            echo '  <button class="medium white-blue" onClick="popupClose();">'.$lang['srv_zapri'].'</button>';
            echo '  <button class="medium blue" onClick="archive_surveys_export();">'.$lang['srv_archive_surveys_export_create'].'</button>';
            echo '</div>';
		}

        // Ustvarimo zip arhiv za izvoz in ponudimo gumb za download
        elseif($_GET['a'] == 'export'){

            $survey_list = isset($_POST['survey_archive']) ? $_POST['survey_archive'] : array();
            $error = '';

            // Napaka - nimamo oznacene nobene ankete
            if(count($survey_list) == 0){
                $error = $lang['srv_archive_surveys_export_error1'];
            }
            // Napaka - presezen limit anket
            elseif(count($survey_list) > 100){
                $error = $lang['srv_archive_surveys_export_error2'];
            }


            echo '<div class="popup_close"><a href="#" onClick="popupClose(); return false;">✕</a></div>';
			
            echo '<h2>'.$lang['srv_archive_surveys_export'].'</h2>';

            // Imamo opozorila
            if($error != ''){
                echo '<div class="popup_main archive_surveys">';
                echo '<p>'.$error.'</p>';
                echo '</div>';

                echo '<div class="button_holder archive_surveys">';
                echo '  <button class="medium white-blue" onClick="archive_surveys_display_export();">'.$lang['back'].'</button>';
                echo '</div>';

                return;
            }

            // Izvedemo izvoz in prikazemo gumb za download
            try{
                $response = $this->createSurveysArchive($survey_list);
            }
            catch (Exception $e) {
                $error = $e->getMessage();
            }

            // Imamo napake pri pripravljanju paketa
            if($error != ''){
                echo '<div class="popup_main archive_surveys">';
                echo '<p>'.$error.'</p>';
                echo '</div>';

                echo '<div class="button_holder archive_surveys">';
                echo '  <button class="medium white-blue" onClick="archive_surveys_display_export();">'.$lang['back'].'</button>';
                echo '</div>';

                return;
            }


            // Uspesno kreiran paket
            echo '<div class="popup_main archive_surveys">';
            echo $response;
            echo '</div>';
            
            echo '<div class="button_holder archive_surveys">';
            echo '  <button class="medium white-blue" onClick="popupClose();">'.$lang['srv_zapri'].'</button>';
            echo '  <button class="medium blue" onClick="archive_surveys_download_export(\''.ARCHIVE_URL.'archive_surveys_'.$global_user_id.'.zip\');">'.$lang['srv_archive_surveys_export_download'].'</button>';
            echo '</div>';
        }

        // Prikazemo popup za uvoz zip arhiva anket
        if ($_GET['a'] == 'display_import') {
			
            echo '<div class="popup_close"><a href="#" onClick="popupClose(); return false;">✕</a></div>';
			
            echo '<h2>'.$lang['srv_archive_surveys_import'].'</h2>';


            echo '<div class="popup_main archive_surveys">';
            echo '<form id="restore" action="ajax.php?a=archive_restore" method="post" name="restorefrm" enctype="multipart/form-data" >';
            
            echo $lang['srv_archive_surveys_import_text'].'<br><br>';
            
            echo '<input type="file" name="restore" id="archive_surveys_input" onChange="archive_surveys_select_import();" /></label>';

            echo '<p id="archive_surveys_error" class="red top16 displayNone">'.$lang['srv_archive_surveys_import_error'].'</p>';
            echo '<p id="archive_surveys_error2" class="red top16 displayNone">'.$lang['srv_archive_surveys_import_error2'].'</p>';
            echo '<p id="archive_surveys_error3" class="red top16 displayNone">'.$lang['srv_archive_surveys_import_error3'].'</p>';

            echo '</form>';            
            echo '</div>';


            echo '<div class="button_holder archive_surveys">';
            echo '  <button class="medium white-blue" onClick="popupClose();">'.$lang['srv_zapri'].'</button>';
            echo '  <button id="archive_surveys_import_button" class="medium blue displayNone" onClick="archive_surveys_import();">'.$lang['srv_archive_surveys_import_button'].'</button>';
            echo '</div>';
		}

        // Izvedemo uvoz zip arhiva in prikazemo obvestilo (uspesno, napake)
        elseif($_GET['a'] == 'import'){

            echo '<div class="popup_close"><a href="#" onClick="popupClose(); return false;">✕</a></div>';
			
            echo '<h2>'.$lang['srv_archive_surveys_import'].'</h2>';


            echo '<div class="popup_main archive_surveys">';

            // zip file ustrezno nalozen
            if (isset($_FILES['restore']) && $_FILES['restore']['error'] === UPLOAD_ERR_OK) {

                $zipFile = $_FILES['restore']['tmp_name'];

                // Izevedemo uvoz iz zip datoteke
                try{
                    $this->importSurveysArchive($zipFile);
                }
                catch (Exception $e) {
                    echo '<p class="red">'.$lang['srv_archive_surveys_import_error_import'].'</p>';
                    echo '<p class="red italic top8">'.$e->getMessage().'</p>';
                }
            } 
            else {
                echo '<p class="red">'.$lang['srv_archive_surveys_import_error_upload'].'</p>';
            }

            echo '</div>';


            echo '<div class="button_holder archive_surveys">';
            echo '  <button class="medium white-blue" onClick="popupClose();">'.$lang['srv_zapri'].'</button>';
            echo '</div>';
        }
	}
}