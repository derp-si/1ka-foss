<?php

/* 
 * 
 * Komentarji (testiranje->komentarji)
 * 
 */

class SurveyComments {
	

    var $anketa;
    var $db_table = '';
    

    public function __construct($anketa){

        if($anketa == null || $anketa <= 0)
            return 'ID ankete ne obstaja!';

        $this->anketa = $anketa;

        $this->db_table = SurveyInfo::getInstance()->getSurveyArchiveDBString();
    }

    
    // Izpis komentarjev (testiranje -> komentarji)
    public function display_komentarji(){


        // Izpisemo meni (2 zavihka na vrhu)
        $this->display_menu_komentarji();

        // Izpisemo vsebino glede na zavihek
        if($_GET['a'] == 'komentarji'){
            $this->display_komentarji_na_vprasanja();
        } 
        else{
            $this->display_splosni_komentarji();
        }
    }

    // Meni na vrhu (preklop med splosnimi in komentarji na vprasanja)
    private function display_menu_komentarji() {
		global $lang;

		// Prestejemo komentarje (nereseni/vsi)
		$comment_count = $this->testiranje_komentarji_count();

		echo '<div id="komentarji_top">';
		echo '<div id="komentarji_list">';

		// Komentarji na vprasanja
		$question_comment_link = 'window.location.href=\'index.php?anketa=' . $this->anketa . '&amp;a=' . A_KOMENTARJI_ANKETA . '\'; return false;';

		echo '<div class="list_item '.($_GET['a'] == A_KOMENTARJI_ANKETA ? 'active' : '').'" onclick="'.$question_comment_link.'">';
		echo $lang['srv_testiranje_komentarji_anketa_title'];
		echo ' (' . ($comment_count['survey_resp']['unresolved'] + $comment_count['survey_admin']['unresolved']); //nerešeni
		echo '/' . ($comment_count['survey_resp']['all'] + $comment_count['survey_admin']['all']) . ')'; //vsi
		echo '</div>';


		// Komentarji na anketo
		$survey_comment_link = 'window.location.href=\'index.php?anketa=' . $this->anketa . '&amp;a=' . A_KOMENTARJI . '\'; return false;';
		
		echo '<div class="list_item '.($_GET['a'] == A_KOMENTARJI ? ' active' : '').'" onclick="'.$survey_comment_link.'">';
		echo $lang['srv_testiranje_komentarji_title'];
		echo ' (' . $comment_count['question']['unresolved'];
		echo '/' . $comment_count['question']['all'] . ')';
		echo '</div>';


		echo '</div>'; //#komentarji_list

		//Link na nastavitev komentarjev
		$d = new Dostop();

            # nastavitve komentarjev
            if ($d->checkDostopSub('edit')) {
                echo '<div id="analiza_right_options_holder" class="right_options_holder">';

                echo '<div title="' . $lang['settings'] . '" id="analiza_right_options">';
                echo '<a href="index.php?anketa=' . $this->anketa . '&a=urejanje" title="' . $lang['srv_settings_komentarji1'] . '">';
                echo '<span class="faicon wheel_32"></span>';
                echo '</a>';
                echo '</div>';

                echo '</div>';
            }

		echo '</div>';

	}

	// Izpis zavihka SPLOSNI KOMENTARJI
	private function display_splosni_komentarji() {
		global $lang;
		global $site_url;
		global $admin_type;
		global $global_user_id;
		
		$b = new Branching($this->anketa);
		$f = new Forum;
		$d = new Dostop();
		
		SurveySetting::getInstance()->Init($this->anketa);
		$sortpostorder = SurveySetting::getInstance()->getSurveyMiscSetting('sortpostorder');
		$survey_comment = SurveySetting::getInstance()->getSurveyMiscSetting('survey_comment');
		$survey_comment_resp = SurveySetting::getInstance()->getSurveyMiscSetting('survey_comment_resp');
		$comment_count = $this->testiranje_komentarji_count();
		$commentmarks = SurveySetting::getInstance()->getSurveyMiscSetting('commentmarks');
        $commentmarks_who = SurveySetting::getInstance()->getSurveyMiscSetting('commentmarks_who');
		
		$sortpostorder = ($sortpostorder == '') ? 0 : $sortpostorder;
		$commentmarks = ($commentmarks == '') ? 0 : $commentmarks;
		$commentmarks_who = ($commentmarks_who == '') ? 0 : $commentmarks_who;
		
		$rowa = SurveyInfo::getInstance()->getSurveyRow();
		
		
		# VV: privzeto naj bodo samo nerešeni komentarji
		if (!isset($_GET['only_unresolved'])) {
			$_GET['only_unresolved'] = 1;
		}
		
		# samo nerešeni komentarji			
		if ($commentmarks == 0) {
			echo '<div class="">';
			echo '<input type="checkbox" id="only_unresolved" onchange="window.location = \'index.php?anketa='.$this->anketa.'&a=komentarji_anketa&only_unresolved=\'+$(\'#only_unresolved:checked\').val()" value="1" '.($_GET['only_unresolved']==1?'checked':'').' />';
			echo '<label for="only_unresolved">'.$lang['srv_comments_unresolved'].'</label>';
			echo '</div>';

		} 
        else {
			echo $lang['move_show'].': <select id="only_unresolved" class="dropdown" name="" onchange="window.location = \'index.php?anketa='.$this->anketa.'&a=komentarji_anketa&only_unresolved=\'+$(\'#only_unresolved\').val(); " >
								<option value="0"'.($_GET['only_unresolved']==0?' selected="selected"':'').'>'.$lang['all2'].'</option>
								<option value="1"'.($_GET['only_unresolved']==1?' selected="selected"':'').'>'.$lang['srv_comments_unresolved'].'</option>
								<option value="2"'.($_GET['only_unresolved']==2?' selected="selected"':'').'>'.$lang['srv_undecided'].'</option>
								<option value="3"'.($_GET['only_unresolved']==3?' selected="selected"':'').'>'.$lang['srv_todo'].'</option>
								<option value="4"'.($_GET['only_unresolved']==4?' selected="selected"':'').'>'.$lang['srv_done'].'</option>
								<option value="5"'.($_GET['only_unresolved']==5?' selected="selected"':'').'>'.$lang['srv_not_relevant'].'</option>
							</select>';
		}
		
		$only_unresolved = " ";
		switch($_GET['only_unresolved']){
			case 1:
				$only_unresolved = " AND ocena <= '1' ";
				break;
			case 2:
				$only_unresolved = " AND ocena = '0' ";
				break;
			case 3:
				$only_unresolved = " AND ocena = '1' ";
				break;
			case 4:
				$only_unresolved = " AND ocena = '2' ";
				break;
			case 5:
				$only_unresolved = " AND ocena = '3' ";
				break;
			default:
				break;
		}

		echo '<div class="komentarji_anketa_holder">';		
		
		// Splosni komentarji urednikov - levo
		echo '<div class="komentarji_anketa komentarji_ured">';
		
		echo '<div class="comment_type">';

		echo $lang['srv_admin_comment'].':';
		/*Sort
		echo '<span class="faicon pointer blue fa-sort-amount-down"></span>';
		echo '<span class="faicon pointer blue fa-sort-amount-up"></span>';*/
		
		echo '</div>';

		$sql = sisplet_query("SELECT * FROM post WHERE tid='$rowa[thread]' ".$only_unresolved." ");
		$rows = (mysqli_num_rows($sql) == 0) ? 0 : mysqli_num_rows($sql) - 1;
		if ($rows > 0){
			
			$i=0;
			while ($row = mysqli_fetch_array($sql)) {
				
				if (($i != 0 && $sortpostorder==0) || ($i < $rows && $sortpostorder==1)) {
					
					echo '<div class="comment_box '.($row['ocena']==0 ? '' : 'archived').'" id="comment_box_'.$row['id'].'">';

					echo '<div class="by">';

					$datetime = strtotime($row['time']);
					$datetime = date("d.m.Y G:i", $datetime);


					echo '<p>'.$lang['srv_comments_user'].'<span class="user">'.$f->user($row['uid']).'</span> <span class="date">('.$datetime.')</span></p>';

					
					if ($admin_type <= 1 || $rowa['insert_uid']==$global_user_id || $commentmarks_who==0) {
					
						echo '<div class="options">';

						echo '<div class="solve">';
						if ($row['ocena']==0) {
							echo '<p class="archive_comment" onclick="$.post(\'ajax.php?a=comment_ocena\', {type: \'question_comment\', ocena: (\'2\'), id: \''.$row['id'].'\', anketa: \''.$rowa['id'].'\'}, function () {window.location.reload();});"> <span class="faicon fa-check link-right"></span>';
							echo $lang['srv_comments_archive'].'</p>';
						}
						else {
							echo '<p class="unarchive_comment" onclick="$.post(\'ajax.php?a=comment_ocena\', {type: \'question_comment\', ocena: (\'0\'), id: \''.$row['id'].'\', anketa: \''.$rowa['id'].'\'}, function () {window.location.reload();});"> <span class="faicon fa-undo link-right"></span>';
							echo $lang['srv_comments_unarchive'].'</p>';
						}
						echo '</div>';

						echo '<span class="faicon pointer fa-ellipsis-h" id="dots_'.$row['id'].'" onClick="toggleCommentOptions(\''.$row['id'].'\');"></span>';

							echo '<div class="options_dropdown displayNone" id="options_'.$row['id'].'">';
							
                            echo '<div class="line">';
                            echo '  <a href="javascript:comment_on_comment_splosni(\''.$row['id'].'\',\''.$f->user($row['uid']).'\',\''.$datetime.'\');"><span class="faicon fa-reply link-right"></span>'.$lang['srv_comment_comment'].'</a>';
                            echo '</div>';
							
							echo '<div class="line" onClick="survey_comment_delete(\''.$row['id'].'\');">';
							echo '<span class="faicon trash empty link-right"></span>';
							echo $lang['srv_comment_delete'];
							echo '</div>';
							
							echo '</div>';

							//Klik izven razprith nastavitev - zapremo okno
							echo '<script>
							$(document).mouseup(function(e){
								var container = $(".options_dropdown");
							
								if (!container.is(e.target) && container.has(e.target).length === 0){
									$(".options_dropdown").addClass("displayNone");
								}
							});
						</script>';
						
						echo '</div>'; //options
						
					}

					echo '</div>'; //by


					echo '<div class="content">';
					echo $row['vsebina'];
					echo '</div>';

					echo '</div>';

                    echo '<div class="reply_to_text" id="comment_on_comment_splosni_'.$row['id'].'"></div>';
				}

				$i++;
			}
		}
		// Nimamo komentarja
		else{
			// Ce so komentarji aktivirani
			if($survey_comment != ''){
				echo '<p class="bottom16">'.$lang['srv_no_comments_solved'].'</p>';
			}
			else{
				echo '<p class="bottom16">'.$lang['srv_no_comments'].'</p>';
			}
		}

		// Dodajanje novega komentarja

		echo '<div id="comment_toggle_0_4" class="add_comment_toggle pointer" onClick="toggleAddComment(\'0_4\')">';
		echo '<span class="faicon inline_comment link-right"></span>';
		echo $lang['srv_survey_general_comment'];
		echo '</div>';

		echo '<div class="comment_wrapper" id="comment_wrapper_0_4">';
		echo '<div class="new_comment" id="survey_comment_0_4">';
		$ba = new BranchingAjax($this->anketa);
		$ba->add_comment_field(0, '1', '4', false);
		echo '</div>';
		echo '</div>';


		echo '</div>';
		
		
		// Splosni komentarji respondentov - desno
		echo '<div class="komentarji_anketa komentarji_resp">';	
		
		echo '<div class="comment_type">';
		echo $lang['srv_repondent_comment'].':';
		/*Sort
		echo '<span class="faicon pointer blue fa-sort-amount-down"></span>';
		echo '<span class="faicon pointer blue fa-sort-amount-up"></span>';*/
		echo '</div>';
		
		$orderby = '';

		$sql = sisplet_query("SELECT * FROM srv_comment_resp WHERE ank_id='$this->anketa' ".$only_unresolved." ORDER BY comment_time $orderby, id $orderby");
		if (mysqli_num_rows($sql) > 0) {

			while ($row = mysqli_fetch_array($sql)) {

				echo '<div class="comment_box '.($row['ocena']==0 ? '' : 'archived').'" id="comment_box_'.$row['id'].'">';

				$datetime = strtotime($row['comment_time']);
				$datetime = date("d.m.Y G:i", $datetime);

				echo '<div class="by">';

				if($row['usr_id'] == 0){
					echo '<p>Respondent <span class="date">('.$datetime.')</span></p>';
				}
				else{
					$sqlU = sisplet_query("SELECT name FROM users WHERE id='$row[usr_id]'");
					$rowU = mysqli_fetch_array($sqlU);
					
					$user = $rowU['name'];

					echo '<p>'.$lang['srv_comments_user'].'<span class="user">'.$user.'</span> <span class="date">('.$datetime.')</span></p>';

				}

				if ($admin_type <= 1 || $rowa['insert_uid']==$global_user_id || $commentmarks_who==0) {
					
					echo '<div class="options">';

					echo '<div class="solve">';
					if ($row['ocena']==0) {
						echo '<p class="archive_comment" onclick="$.post(\'ajax.php?a=comment_ocena\', {type: \'respondent_survey_comment\', ocena: (\'2\'), id: \''.$row['id'].'\', anketa: \''.$rowa['id'].'\'}, function () {window.location.reload();});"> <span class="faicon fa-check link-right"></span>';
						echo $lang['srv_comments_archive'].'</p>';
					}
					else {
						echo '<p class="unarchive_comment" onclick="$.post(\'ajax.php?a=comment_ocena\', {type: \'respondent_survey_comment\', ocena: (\'0\'), id: \''.$row['id'].'\', anketa: \''.$rowa['id'].'\'}, function () {window.location.reload();});"> <span class="faicon fa-undo link-right"></span>';
						echo $lang['srv_comments_unarchive'].'</p>';
					}
					echo '</div>';

					echo '<span class="faicon pointer fa-ellipsis-h" id="dots_'.$row['id'].'" onClick="toggleCommentOptions(\''.$row['id'].'\');"></span>';

						echo '<div class="options_dropdown displayNone" id="options_'.$row['id'].'">';
						
                        echo '<div class="line">';
                        echo '  <a href="javascript:comment_on_comment_splosni_resp(\''.$row['id'].'\',\''.$f->user($row['uid']).'\',\''.$datetime.'\');"><span class="faicon fa-reply link-right"></span>'.$lang['srv_comment_comment'].'</a>';
                        echo '</div>';
						
						echo '<div class="line" onClick="survey_respondent_comment_delete(\''.$row['id'].'\');">';
						echo '<span class="faicon trash empty link-right"></span>';
						echo $lang['srv_comment_delete'];
						echo '</div>';
						
						echo '</div>';

						//Klik izven razprith nastavitev - zapremo okno
						echo '<script>
						$(document).mouseup(function(e){
							var container = $(".options_dropdown");
						
							if (!container.is(e.target) && container.has(e.target).length === 0){
								$(".options_dropdown").addClass("displayNone");
							}
						});
					</script>';

					echo '</div>'; //options
				}

				echo '</div>'; //by


				echo '<div class="content">';
				echo $row['comment'];
				echo '</div>';
				
				
				echo '</div>';

                echo '<div class="reply_to_text" id="comment_on_comment_splosni_resp_'.$row['id'].'"></div>';
			}
		}
		// Nimamo komentarja
		else{
			// Ce so komentarji aktivirani
			if($survey_comment_resp != ''){
				echo '<p class="bottom16">'.$lang['srv_no_comments_solved'].'</p>';
			}
			else{
				echo '<p class="bottom16">'.$lang['srv_no_comments'].'</p>';
			}
		}
        
		echo '</div>';

		echo '</div>'; // div.komentarji_anketa_holder
	}

    // Izpis zavihka KOMENTARJI NA VPRASANJA
	private function display_komentarji_na_vprasanja() {
		global $lang;
		global $site_url;
		global $admin_type;
		global $global_user_id;
		
		$lang_admin = $lang;
		
		//$sa = new SurveyAdmin(1, $this->anketa);
		include_once('../../main/survey/app/global_function.php');
		new \App\Controllers\SurveyController(true);
		save('forceShowSpremenljivka', true);

		$f = new Forum;
		$c = 0;
		
		$lang = $lang_admin;
		
		$b = new Branching($this->anketa);
		
		$d = new Dostop();
		
		$rowi = SurveyInfo::getInstance()->getSurveyRow();
		
		SurveySetting::getInstance()->Init($this->anketa);
		$question_resp_comment_viewadminonly = SurveySetting::getInstance()->getSurveyMiscSetting('question_resp_comment_viewadminonly');
		$question_comment_viewadminonly = SurveySetting::getInstance()->getSurveyMiscSetting('question_comment_viewadminonly');
		$question_comment_viewauthor = SurveySetting::getInstance()->getSurveyMiscSetting('question_comment_viewauthor');
		$sortpostorder = SurveySetting::getInstance()->getSurveyMiscSetting('sortpostorder');
		$question_note_view = SurveySetting::getInstance()->getSurveyMiscSetting('question_note_view');
		$addfieldposition = SurveySetting::getInstance()->getSurveyMiscSetting('addfieldposition');
		$commentmarks = SurveySetting::getInstance()->getSurveyMiscSetting('commentmarks');
        $commentmarks_who = SurveySetting::getInstance()->getSurveyMiscSetting('commentmarks_who');
		
		$sortpostorder = ($sortpostorder == '') ? 0 : $sortpostorder;
		$commentmarks = ($commentmarks == '') ? 0 : $commentmarks;
		$commentmarks_who = ($commentmarks_who == '') ? 0 : $commentmarks_who;
		$question_comment_viewadminonly = ($question_comment_viewadminonly == '') ? 4 : $question_comment_viewadminonly;
		$question_resp_comment_viewadminonly = ($question_resp_comment_viewadminonly == '') ? 4 : $question_resp_comment_viewadminonly;;
		
		$comment_count = $this->testiranje_komentarji_count();
		
		
		# VV: privzeto naj bodo samo nerešeni komentarji
		if (!isset($_GET['only_unresolved'])) {
			$_GET['only_unresolved'] = 1;
		}
	
		
		$sqlf1 = sisplet_query("SELECT p.id FROM post p WHERE p.tid='$rowi[thread]' AND p.ocena='5'");
		while ($rowf1 = mysqli_fetch_array($sqlf1)) {
			$s = sisplet_query("SELECT * FROM views WHERE pid='$rowf1[id]' AND uid='$global_user_id'");
			if (mysqli_num_rows($s) == 0)
			$show_survey_comment = 1;
		}

		// vsa vprasanja
		echo '<div class="comment_checkboxes_wrap">';

		echo '<div class="">';
		echo '<input type="checkbox" id="all_questions" onchange="window.location = \'index.php?anketa='.$this->anketa.'&a=komentarji'.(isset($_GET['only_unresolved'])?'&only_unresolved='.$_GET['only_unresolved']:'').'&all_questions=\'+$(\'#all_questions:checked\').val()" value="1" '.(isset($_GET['all_questions']) && $_GET['all_questions'] == 1 ? 'checked' : '').' />';
		echo '<label for="all_questions">'.$lang['srv_all_questions'].'</label>';
		echo '</div>';

		# samo nerešeni komentarji			
		if ($commentmarks == 0) {
			echo '<div class="">';
			echo '<input type="checkbox" id="only_unresolved" onchange="window.location = \'index.php?anketa='.$this->anketa.'&a=komentarji'.(isset($_GET['all_questions'])?'&all_questions='.$_GET['all_questions']:'').'&only_unresolved=\'+$(\'#only_unresolved:checked\').val()" value="1" '.($_GET['only_unresolved']==1?'checked':'').' />';
			echo '<label for="only_unresolved">'.$lang['srv_comments_unresolved'].'</label>';
			echo '</div>';

		} else {
			echo $lang['move_show'].': <select id="only_unresolved" class="dropdown" name="" onchange="window.location = \'index.php?anketa='.$this->anketa.'&a=komentarji&only_unresolved=\'+$(\'#only_unresolved\').val(); " >
								<option value="0"'.($_GET['only_unresolved']==0?' selected="selected"':'').'>'.$lang['all2'].'</option>
								<option value="1"'.($_GET['only_unresolved']==1?' selected="selected"':'').'>'.$lang['srv_comments_unresolved'].'</option>
								<option value="2"'.($_GET['only_unresolved']==2?' selected="selected"':'').'>'.$lang['srv_undecided'].'</option>
								<option value="3"'.($_GET['only_unresolved']==3?' selected="selected"':'').'>'.$lang['srv_todo'].'</option>
								<option value="4"'.($_GET['only_unresolved']==4?' selected="selected"':'').'>'.$lang['srv_done'].'</option>
								<option value="5"'.($_GET['only_unresolved']==5?' selected="selected"':'').'>'.$lang['srv_not_relevant'].'</option>
							</select>';
		}
		echo '</div>';
		// echo ' '.Help::display('srv_comments_only_unresolved').'</span>';
		
		
		// Nov nacin kjer se sprehodimo cez branching, ker imamo lahko tudi komentarje na ife in bloke
		Common::getInstance()->Init($this->anketa);
		$branching_array = Common::getBranchingOrder();
		if (count($branching_array) > 0) {
				
			$view = 1;

			$b = new Branching($this->anketa);

			echo '<ul>';
			
			foreach($branching_array AS $element){
			
				// Gre za if ali blok
				if($element['if_id'] > 0){
					$if_id = $element['if_id'];
				
					$sql1 = sisplet_query("SELECT * FROM srv_if WHERE id = '$if_id'");
					$row1 = mysqli_fetch_array($sql1);
					
					$orderby = $sortpostorder == 1 ? 'DESC' : 'ASC' ;
					$tid = $row1['thread'];	
					
					$only_unresolved = " ";
					$only_unresolved2 = " ";
					if ($_GET['only_unresolved'] == 1) $only_unresolved = " AND ocena <= 1 "; 
					if ($_GET['only_unresolved'] == 1) $only_unresolved2 = " AND text2 <= 1 "; 
					
					if ($_GET['only_unresolved'] == 2) $only_unresolved = " AND ocena = 0 "; 
					if ($_GET['only_unresolved'] == 2) $only_unresolved2 = " AND text2 = 0 "; 
					
					if ($_GET['only_unresolved'] == 3) $only_unresolved = " AND ocena = 1 "; 
					if ($_GET['only_unresolved'] == 3) $only_unresolved2 = " AND text2 = 1 "; 
					
					if ($_GET['only_unresolved'] == 4) $only_unresolved = " AND ocena = 2 "; 
					if ($_GET['only_unresolved'] == 4) $only_unresolved2 = " AND text2 = 2 "; 
					
					if ($_GET['only_unresolved'] == 5) $only_unresolved = " AND ocena = 3 "; 
					if ($_GET['only_unresolved'] == 5) $only_unresolved2 = " AND text2 = 3 "; 
					
					
					$tema_vsebuje = substr($lang['srv_forum_intro'],0,10);		// da ne prikazujemo 1. default sporocila
					
					if ($admin_type <= $question_comment_viewadminonly) {	// vidi vse komentarje
						$sqlt = sisplet_query("SELECT * FROM post WHERE vsebina NOT LIKE '%{$tema_vsebuje}%' AND tid='$tid' $only_unresolved ORDER BY time $orderby, id $orderby");
					} elseif ($question_comment_viewauthor==1) {	// vidi samo svoje komentarje
						$sqlt = sisplet_query("SELECT * FROM post WHERE vsebina NOT LIKE '%{$tema_vsebuje}%' AND tid='$tid' $only_unresolved AND uid='$global_user_id' ORDER BY time $orderby, id $orderby");
					} else {												// ne vidi nobenih komentarjev
						$sqlt = sisplet_query("SELECT * FROM post WHERE 1=0");
					}			
								
					if ((isset($_GET['all_questions']) && $_GET['all_questions']=='1') OR (mysqli_num_rows($sqlt) > 0)) {
						$c++;
						
						echo '<li id="branching_'.$if_id.'" class="spr">';
						echo '<div class="spremenljivka_content">';
						
						// Blok
						if($row1['tip'] == 1){
							echo '<div class="spremenljivka_settings">';
							echo '<span class="conditions_display"><strong class="clr_bl">BLOCK</strong> <span class="colorblock">('.$row1['number'].')</span>';
							echo '</div>';
						}
						// If
						else{
							echo '<div class="spremenljivka_settings">';
							$b->conditions_display($if_id);
							echo '</div>';
						}				
						
						echo '</div>';
						echo '</li>';
												
						echo '<div class="question_comment_holder">';
						
						if ($addfieldposition == 1) {
							echo '<div id="survey_comment_'.$if_id.'_'.'4'.'" style="display:none"></div>';
							echo '<p><a href="#" onclick="$(\'#comment_field_'.$if_id.'\').toggle(); return false;">'.$lang['srv_comments_add_comment'].'</a></p>';
							echo '<p id="comment_field_'.$if_id.'" style="display:none">';
							$ba = new BranchingAjax($this->anketa);
							$ba->add_comment_field($if_id, '1', '4', false);
							echo '</p>';
						}

						echo '<div class="question_comment editor">';

						echo '<div class="comment_type">';

						echo $lang['srv_admin_comment'].':';

						/*Sort
						echo '<span class="faicon pointer blue fa-sort-amount-down"></span>';
						echo '<span class="faicon pointer blue fa-sort-amount-up"></span>';*/
						
						echo '</div>';
								
						// komentarji na vprasanje
						if ($row1['thread'] > 0) {
							
							if (mysqli_num_rows($sqlt) > 0) {
								
								$i = 0;
								while ($rowt = mysqli_fetch_array($sqlt)) {

									/*echo '<div class="comment_box '.($rowt['ocena']==0 ? '' : 'archived').'" id="comment_box_'.$rowt['id'].'">';

									echo '<div class="by">';
                                    echo $lang['srv_comments_user'].'<span class="user">'.$f->user($rowt['uid']).'</span>'.$lang['srv_comments_user2'];
                                    echo '</div>';

                                    $datetime = strtotime($rowt['time']);
                                    $datetime = date("d.m.Y G:i", $datetime);

                                    echo '<div class="date">';
                                    echo $f->datetime1($rowt['time']);
                                    echo '</div>';

                                    echo '<div class="content">';
                                    echo $rowt['vsebina'];
                                    echo '</div>';


									echo '<div class="bottom_options">';

									if ($admin_type <= 1 || $rowi['insert_uid']==$global_user_id || $commentmarks_who==0) {
										
										if ($commentmarks == 1) {
											echo '	<select name="ocena" onchange="$.post(\'ajax.php?a=comment_ocena\', {type: \'question_comment\', ocena: this.value, id: \''.$rowt['id'].'\', anketa: \''.$rowi['id'].'\'}, function () {window.location.reload();});">
														<option value="0"'.($rowt['ocena']==0?' selected="selected"':'').'>'.$lang['srv_undecided'].'</option>
														<option value="1"'.($rowt['ocena']==1?' selected="selected"':'').'>'.$lang['srv_todo'].'</option>
														<option value="2"'.($rowt['ocena']==2?' selected="selected"':'').'>'.$lang['srv_done'].'</option>
														<option value="3"'.($rowt['ocena']==3?' selected="selected"':'').'>'.$lang['srv_not_relevant'].'</option>
													</select>';
										} 
                                        else {

											if ($rowt['ocena']==0) {
                                                echo '<p class="archive_comment" onclick="$.post(\'ajax.php?a=comment_ocena\', {type: \'question_comment\', ocena: (\'2\'), id: \''.$rowt['id'].'\', anketa: \''.$rowi['id'].'\'}, function () {window.location.reload();});"> <span class="faicon arhiv link-right"></span>';
                                                echo $lang['srv_comments_archive'].'</p>';

                                                echo '<div class="reply_to">';
                                                echo '  <a href="javascript:comment_on_comment(\''.$rowt['id'].'\',\''.$f->user($rowt['uid']).'\',\''.$f->datetime1($rowt['time']).'\',\''.$if_id.'\');"><span class="faicon fa-reply link-right"></span>'.$lang['srv_comment_comment'].'</a>';
                                                echo '</div>';
											}
											else {

												echo '<p class="archived_comment"><span class="faicon arhiv link-right"></span>'.$lang['srv_comments_archived'].'</p>';
                                                echo '<p class="unarchive_comment" onclick="$.post(\'ajax.php?a=comment_ocena\', {type: \'question_comment\', ocena: (\'0\'), id: \''.$rowt['id'].'\', anketa: \''.$rowi['id'].'\'}, function () {window.location.reload();});"> <span class="faicon inv_sent_0 empty link-right"></span>';
                                                echo $lang['srv_comments_unarchive'].'</p>';

											}
										}
									}

									echo '</div>'; // .bottom_options
									echo '</div>'; // comment_box


									echo '<div class="reply_to_text" id="comment_on_comment_'.$if_id.'"></div>';*/






									echo '<div class="comment_box '.($rowt['ocena']==0 ? '' : 'archived').'" id="comment_box_'.$rowt['id'].'">';

									echo '<div class="by">';

									$datetime = strtotime($rowt['time']);
									$datetime = date("d.m.Y G:i", $datetime);

									echo '<p>'.$lang['srv_comments_user'].'<span class="user">'.$f->user($rowt['uid']).'</span> <span class="date">('.$datetime.')</span></p>';

									
									if ($admin_type <= 1 || $rowi['insert_uid']==$global_user_id || $commentmarks_who==0) {
									
										echo '<div class="options">';
                                        
										echo '<div class="solve">';
										if ($rowt['ocena']==0) {
											echo '<p class="archive_comment" onclick="$.post(\'ajax.php?a=comment_ocena\', {type: \'question_comment\', ocena: (\'2\'), id: \''.$rowt['id'].'\', anketa: \''.$rowi['id'].'\'}, function () {window.location.reload();});"> <span class="faicon fa-check link-right"></span>';
											echo $lang['srv_comments_archive'].'</p>';
										}
										else {
											echo '<p class="unarchive_comment" onclick="$.post(\'ajax.php?a=comment_ocena\', {type: \'question_comment\', ocena: (\'0\'), id: \''.$rowt['id'].'\', anketa: \''.$rowi['id'].'\'}, function () {window.location.reload();});"> <span class="faicon fa-undo link-right"></span>';
											echo $lang['srv_comments_unarchive'].'</p>';
										}
										echo '</div>';

										echo '<span class="faicon pointer fa-ellipsis-h" id="dots_'.$rowt['id'].'" onClick="toggleCommentOptions(\''.$rowt['id'].'\');"></span>';

											echo '<div class="options_dropdown displayNone" id="options_'.$rowt['id'].'">';
											
											echo '<div class="line">';
											echo '<a href="javascript:comment_on_comment(\''.$rowt['id'].'\',\''.$f->user($rowt['uid']).'\',\''.$datetime.'\',\''.$if_id.'\');"><span class="faicon fa-reply link-right"></span>'.$lang['srv_comment_comment'].'</a>';
											echo '</div>';
											
											echo '<div class="line" onClick="comment_delete(\''.$rowt['id'].'\');">';
											echo '<span class="faicon trash empty link-right"></span>';
											echo $lang['srv_comment_delete'];
											echo '</div>';
											
											echo '</div>';

											//Klik izven razprith nastavitev - zapremo okno
											echo '<script>
											$(document).mouseup(function(e){
												var container = $(".options_dropdown");
											
												if (!container.is(e.target) && container.has(e.target).length === 0){
													$(".options_dropdown").addClass("displayNone");
												}
											});
										</script>';


										
										echo '</div>'; //options
                                        
                                    }

									echo '</div>'; //by

									echo '<div class="content">';
									echo $rowt['vsebina'];
									echo '</div>';


									echo '</div>'; //comment_box

									echo '<div class="reply_to_text" id="comment_on_comment_'.$rowt['id'].'"></div>';
								}				
							}
						}

						if ($addfieldposition == '' || $addfieldposition == 0) {

							$if_type = $row1['tip'] == '1' ? '6' : '5';
							$if_type_title = $row1['tip'] == '1' ? $lang['srv_block_comment'] : $lang['srv_if_comment'];

							echo '<div id="comment_toggle_'.$if_id.'_4" class="add_comment_toggle blue pointer" onClick="toggleAddComment(\''.$if_id.'_'.'4'.'\')">';
							echo '<span class="faicon inline_comment link-right"></span>';
                            echo $if_type_title;
                            echo '</div>';

                            echo '<div class="comment_wrapper" id="comment_wrapper_'.$if_id.'_4">';

                            echo '<div class="new_comment" id="survey_comment_'.$if_id.'_4">';
                            $ba = new BranchingAjax($this->anketa);
                            $ba->add_comment_field($if_id,  $if_type, '4', false);
                            echo '</div>';
                            echo '</div>';
						}

						echo '</div>'; #editor
						
						echo '<div class="question_comment resp">'; //prazen, respondenti ne komentirajo na IFe in bloke
						echo '</div>';
												
						echo '</div>'; #question_comment_holder					

					}
				}
				// Gre za navadno vprasanje
				else{
					$spr_id = $element['spr_id'];
				
					$sql1 = sisplet_query("SELECT tip, thread, note FROM srv_spremenljivka WHERE id = '$spr_id'");
					$row1 = mysqli_fetch_array($sql1);
					
					$orderby = $sortpostorder == 1 ? 'DESC' : 'ASC';
					$tid = $row1['thread'];	
					
					$only_unresolved = " ";
					$only_unresolved2 = " ";
					if ($_GET['only_unresolved'] == 1) $only_unresolved = " AND ocena <= 1 "; 
					if ($_GET['only_unresolved'] == 1) $only_unresolved2 = " AND text2 <= 1 "; 
					
					if ($_GET['only_unresolved'] == 2) $only_unresolved = " AND ocena = 0 "; 
					if ($_GET['only_unresolved'] == 2) $only_unresolved2 = " AND text2 = 0 "; 
					
					if ($_GET['only_unresolved'] == 3) $only_unresolved = " AND ocena = 1 "; 
					if ($_GET['only_unresolved'] == 3) $only_unresolved2 = " AND text2 = 1 "; 
					
					if ($_GET['only_unresolved'] == 4) $only_unresolved = " AND ocena = 2 "; 
					if ($_GET['only_unresolved'] == 4) $only_unresolved2 = " AND text2 = 2 "; 
					
					if ($_GET['only_unresolved'] == 5) $only_unresolved = " AND ocena = 3 "; 
					if ($_GET['only_unresolved'] == 5) $only_unresolved2 = " AND text2 = 3 "; 
					
					
					$tema_vsebuje = substr($lang['srv_forum_intro'],0,10);		// da ne prikazujemo 1. default sporocila
					
					if ($admin_type <= $question_comment_viewadminonly) {	// vidi vse komentarje
						$sqlt = sisplet_query("SELECT * FROM post WHERE vsebina NOT LIKE '%{$tema_vsebuje}%' AND tid='$tid' $only_unresolved ORDER BY time $orderby, id $orderby");
					} 
                    elseif ($question_comment_viewauthor==1) {	// vidi samo svoje komentarje
						$sqlt = sisplet_query("SELECT * FROM post WHERE vsebina NOT LIKE '%{$tema_vsebuje}%' AND tid='$tid' $only_unresolved AND uid='$global_user_id' ORDER BY time $orderby, id $orderby");
					} 
                    else {												// ne vidi nobenih komentarjev
						$sqlt = sisplet_query("SELECT * FROM post WHERE 1=0");
					}
				
					$sql2 = sisplet_query("SELECT COUNT(*) AS count FROM srv_data_text".$this->db_table." WHERE spr_id='0' AND vre_id='$spr_id' $only_unresolved2");
					$row2 = mysqli_fetch_array($sql2);

					if ( (isset($_GET['all_questions']) && $_GET['all_questions']=='1') OR ( mysqli_num_rows($sqlt) > 0 || $row2['count'] > 0 || $row1['note'] != '' ) )  {
						$c++;
						
						echo '<li id="branching_'.$spr_id.'" class="spr spr_tip_'.$row1['tip'].'">';
						$b->vprasanje($spr_id);
						echo '</li>';
						
						if ($admin_type <= $question_note_view || $question_note_view == '') {
							
							if ($row1['note'] != '') {
								echo '<div style="float:left; width:100%; margin-left: 1%">';
								echo '<p class="red"><b><a href="#" class="gray" onclick="$(\'.note-'.$spr_id.', .pl, .mn\').toggle(); return false;"><span class="pl">+</span><span class="mn" style="display:none">-</span> '.$lang['srv_note'].'</a></b></p>';
								echo '<p class="note-'.$spr_id.' displayNone">'.nl2br($row1['note']).'</p>';
								echo '</div>';
							}
						}
						
						echo '<div class="question_comment_holder">';
						
						if ($addfieldposition == 1) {
							echo '<div id="survey_comment_'.$spr_id.'_'.'4'.'" style="display:none"></div>';
							echo '<p><a href="#" onclick="$(\'#comment_field_'.$spr_id.'\').toggle(); return false;">'.$lang['srv_comments_add_comment'].'</a></p>';
							echo '<p id="comment_field_'.$spr_id.'" style="display:none">';
							$ba = new BranchingAjax($this->anketa);
							$ba->add_comment_field($spr_id, '1', '4', false);
							echo '</p>';
						}
						
						echo '<div class="question_comment editor">';

						echo '<div class="comment_type">';

						echo $lang['srv_admin_comment'].':';

						/*Sort
						echo '<span class="faicon pointer blue fa-sort-amount-down"></span>';
						echo '<span class="faicon pointer blue fa-sort-amount-up"></span>';*/
						
						echo '</div>';

						// komentarji urednikov
						if ($row1['thread'] > 0) {
							
							if (mysqli_num_rows($sqlt) > 0) {

								$i = 0;
								while ($rowt = mysqli_fetch_array($sqlt)) {


									echo '<div class="comment_box '.($rowt['ocena']==0 ? '' : 'archived').'" id="comment_box_'.$rowt['id'].'">';

									echo '<div class="by">';

									$datetime = strtotime($rowt['time']);
									$datetime = date("d.m.Y G:i", $datetime);

									echo '<p>'.$lang['srv_comments_user'].'<span class="user">'.$f->user($rowt['uid']).'</span> <span class="date">('.$datetime.')</span></p>';

									
									if ($admin_type <= 1 || $rowi['insert_uid']==$global_user_id || $commentmarks_who==0) {
									
										echo '<div class="options">';
                                        
										echo '<div class="solve">';
										if ($rowt['ocena']==0) {
											echo '<p class="archive_comment" onclick="$.post(\'ajax.php?a=comment_ocena\', {type: \'question_comment\', ocena: (\'2\'), id: \''.$rowt['id'].'\', anketa: \''.$rowi['id'].'\'}, function () {window.location.reload();});"> <span class="faicon fa-check link-right"></span>';
											echo $lang['srv_comments_archive'].'</p>';
										}
										else {
											echo '<p class="unarchive_comment" onclick="$.post(\'ajax.php?a=comment_ocena\', {type: \'question_comment\', ocena: (\'0\'), id: \''.$rowt['id'].'\', anketa: \''.$rowi['id'].'\'}, function () {window.location.reload();});"> <span class="faicon fa-undo link-right"></span>';
											echo $lang['srv_comments_unarchive'].'</p>';
										}
										echo '</div>';

										echo '<span class="faicon pointer fa-ellipsis-h" id="dots_'.$rowt['id'].'" onClick="toggleCommentOptions(\''.$rowt['id'].'\');"></span>';

											echo '<div class="options_dropdown displayNone" id="options_'.$rowt['id'].'">';
											
											echo '<div class="line">';
											echo '<a href="javascript:comment_on_comment(\''.$rowt['id'].'\',\''.$f->user($rowt['uid']).'\',\''.$datetime.'\',\''.$spr_id.'\');"><span class="faicon fa-reply link-right"></span>'.$lang['srv_comment_comment'].'</a>';
											echo '</div>';
											
											echo '<div class="line" onClick="comment_delete(\''.$rowt['id'].'\');">';
											echo '<span class="faicon trash empty link-right"></span>';
											echo $lang['srv_comment_delete'];
											echo '</div>';
											
											echo '</div>';

											//Klik izven razprith nastavitev - zapremo okno
											echo '<script>
											$(document).mouseup(function(e){
												var container = $(".options_dropdown");
											
												if (!container.is(e.target) && container.has(e.target).length === 0){
													$(".options_dropdown").addClass("displayNone");
												}
											});
										</script>';


										
										echo '</div>'; //options
                                        
                                    }

									echo '</div>'; //by

									echo '<div class="content">';
									echo $rowt['vsebina'];
									echo '</div>';


									echo '</div>'; //comment_box

									echo '<div class="reply_to_text" id="comment_on_comment_'.$rowt['id'].'"></div>';
								}

							}

						}

						if ($addfieldposition == '' || $addfieldposition == 0) {

							echo '<div id="comment_toggle_'.$spr_id.'_'.'4'.'" class="add_comment_toggle pointer" onClick="toggleAddComment(\''.$spr_id.'_'.'4'.'\')">';
							echo '<span class="faicon inline_comment link-right"></span>';
							echo $lang['srv_question_comment'];
							echo '</div>';

							echo '<div class="comment_wrapper" id="comment_wrapper_'.$spr_id.'_'.'4'.'">';

							echo '<div class="new_comment" id="survey_comment_'.$spr_id.'_'.'4'.'">';
							$ba = new BranchingAjax($this->anketa);
							$ba->add_comment_field($spr_id, '1', '4', false);
							echo '</div>';
							echo '</div>';
						}

						echo '</div>';

						
						echo '<div class="question_comment resp">';
						
						// komentarji respondentov

						echo '<div class="comment_type">';
						echo $lang['srv_repondent_comment'].':';
						/*Sort
						echo '<span class="faicon pointer blue fa-sort-amount-down"></span>';
						echo '<span class="faicon pointer blue fa-sort-amount-up"></span>';*/
						echo '</div>';

						if ($row2['count'] > 0) {
							
							if ($admin_type <= $question_resp_comment_viewadminonly) {

								if ($_GET['only_unresolved'] == 1) $only_unresolved = " AND d.text2 <= 1 "; else $only_unresolved = " ";
								
								$sqlt = sisplet_query("SELECT * FROM srv_data_text".$this->db_table." WHERE spr_id='0' AND vre_id='$spr_id' $only_unresolved2 ORDER BY id $orderby");
								if (!$sqlt) echo mysqli_error($GLOBALS['connect_db']);
								while ($rowt = mysqli_fetch_array($sqlt)) {

									echo '<div class="comment_box  '.($rowt['text2']==0 ? '' : 'archived').'" id="comment_box_'.$rowt['id'].'">';

									// Ce smo slucajno pobrisali testne vnose, nimamo casa vnosa komentarja
									$sqlTime = sisplet_query("SELECT time_edit FROM srv_user WHERE id='".$rowt['usr_id']."'");
									if(mysqli_num_rows($sqlTime) > 0){
										
										$rowTime = mysqli_fetch_array($sqlTime);


										echo '<div class="by">';

										if ( strpos($rowt['text'], '__DATE__') !== false ) {
											$rowt['text'] = str_replace('__DATE__', $f->datetime1($rowTime['time_edit']), $rowt['text']);
											$datetime = '-';
											
										} 
										else {

											$datetime = strtotime($rowTime['time_edit']);
											$datetime = date("d.m.Y G:i", $datetime);
										}

										echo '<p>Respondent <span class="date">('.$datetime.')</span></p>';


										if ($admin_type <= 1 || $rowi['insert_uid']==$global_user_id || $commentmarks_who==0) {
										
											echo '<div class="options">';

											echo '<div class="solve">';
											if ($rowt['text2']==0) {
												echo '<p class="archive_comment" onclick="$.post(\'ajax.php?a=comment_ocena\', {type: \'respondent_comment\', text2: (\'2\'), id: \''.$rowt['id'].'\', anketa: \''.$rowi['id'].'\'}, function () {window.location.reload();});"> <span class="faicon fa-check link-right"></span>';
												echo $lang['srv_comments_archive'].'</p>';
											}
											else {
												echo '<p class="unarchive_comment" onclick="$.post(\'ajax.php?a=comment_ocena\', {type: \'respondent_comment\', text2: (\'0\'), id: \''.$rowt['id'].'\', anketa: \''.$rowi['id'].'\'}, function () {window.location.reload();});"> <span class="faicon fa-undo link-right"></span>';
												echo $lang['srv_comments_unarchive'].'</p>';
											}
											echo '</div>';

											echo '<span class="faicon pointer fa-ellipsis-h" id="dots_'.$rowt['id'].'" onClick="toggleCommentOptions(\''.$rowt['id'].'\');"></span>';

												echo '<div class="options_dropdown displayNone" id="options_'.$rowt['id'].'">';
												
                                                echo '<div class="line">';
                                                echo '  <a href="javascript:comment_on_comment_resp(\''.$rowt['id'].'\',\''.$f->user($rowt['usr_id']).'\',\''.$datetime.'\');"><span class="faicon fa-reply link-right"></span>'.$lang['srv_comment_comment'].'</a>';
                                                echo '</div>';

												echo '<div class="line" onClick="respondent_comment_delete(\''.$rowt['id'].'\');">';
												echo '<span class="faicon trash empty link-right"></span>';
												echo $lang['srv_comment_delete'];
												echo '</div>';
												
												echo '</div>';

												//Klik izven razprith nastavitev - zapremo okno
												echo '<script>
												$(document).mouseup(function(e){
													var container = $(".options_dropdown");
												
													if (!container.is(e.target) && container.has(e.target).length === 0){
														$(".options_dropdown").addClass("displayNone");
													}
												});
											</script>';


											
											echo '</div>'; //options
											
										}

										echo '</div>'; //by

									}
									else{
										if ( strpos($rowt['text'], '__DATE__') !== false ) {
											$rowt['text'] = str_replace('__DATE__', '', $rowt['text']);
										} 
									}

									echo '<div class="content">';
									echo nl2br($rowt['text']);
									echo '</div>';

									echo '</div>';

                                    echo '<div class="reply_to_text" id="comment_on_comment_resp_'.$rowt['id'].'"></div>';
								}
							}
						}
						else {
							echo $lang['srv_no_comments_resp'];
						}

						echo '</div>'; #div.question_comment.resp


						echo '</div>'; #div.question_comment_holder
					}	
				}

			}

			echo '</ul>';
			
			if ($c == 0) {
				echo '<div>';
				echo $lang['srv_no_comments_solved'];
				#echo '<a href="index.php?anketa='.$this->anketa.'&a=urejanje">'.$lang['srv_settings_komentarji'].'</a>';
				echo '</div>';
			}
		}
		else {		
			echo '<div>';
			echo $lang['srv_no_comments'];
			echo '</div>';	
		}		
		
		echo '</div>';	// branching	
		echo '<div id="vprasanje_float_editing"></div>';
	}
	

    // Izpis ikone v navigacijskem meniju
    public function display_comments_icon(){
        global $site_url;
        global $global_user_id;
        global $lang;


        $userAccess = UserAccess::getInstance($global_user_id);

        SurveySetting::getInstance()->Init($this->anketa);
        $survey_comment = SurveySetting::getInstance()->getSurveyMiscSetting('survey_comment');
        $question_comment = SurveySetting::getInstance()->getSurveyMiscSetting('question_comment');
        $question_note_view = SurveySetting::getInstance()->getSurveyMiscSetting('question_note_view');
        $question_resp_comment = SurveySetting::getInstance()->getSurveyMiscSetting('question_resp_comment');


        if ($survey_comment != '' || $question_comment != '' || $question_resp_comment == 1 || $this->testiranje_komentarji_komentarji_na_vprasanje(false) > 0) {
                    
            echo '<li>';
            echo '<div id="quick_comments_link" class="newCss">';

            $comment_count = $this->testiranje_komentarji_count();
            $nereseno = ($comment_count['survey_resp']['unresolved'] + $comment_count['survey_admin']['unresolved']) + $comment_count['question']['unresolved'];
            
            if($userAccess->checkUserAccess('komentarji')){
                if ($nereseno > 0) {
                    
                    if ($nereseno > 99)
                        $nereseno = '99+';

                    echo '<div class="fa-stack has_comment"><span class="faicon comments" id="nereseno" onclick="toggleCommentDropdown(); return false;">';
                    echo '<span class="comment_number">'.$nereseno.'</span>';
                    
                }
                else {
                    echo '<div class="fa-stack" ><span class="faicon comments" id="vkljuceno" onclick="toggleCommentDropdown(); return false;">';
                }
            }
            else{
                if ($nereseno > 0)
                    echo '<div class="fa-stack"><span class="faicon comments icon-orange fa-stack-1x user_access_locked">';
                else
                    echo '<div class="fa-stack"><span class="faicon comments user_access_locked">';
            }

            echo '</span></div>'; //zaključi ikono

            //dropdown na klik ikone
            echo '<div id="comment_dropdown" class="displayNone">';
           
            echo '<div class="comment_dropdown_top">';

            echo '<div class="title">';
            echo '<span class="faicon comment_dropdown"></span>';
            echo $lang['comments'];
            echo '</div>';

            echo '<div class="settings">';
            echo '  <a href="'.$site_url.'admin/survey/index.php?anketa='.$this->anketa.'&a=urejanje">';
            echo '    <span class="faicon wheel_32"></span>';
            echo '  </a>';
            echo '</div>';
            
            echo '</div>';

            $comment_link = 'window.location.href=\'' . $site_url . 'admin/survey/index.php?anketa='.$this->anketa.'&a=komentarji_anketa\'; return false;';
            echo '<div class="links" onclick="'.$comment_link.'">';
            echo $lang['srv_testiranje_komentarji_anketa_overview'];
            echo ' (<span class="red">' . ($comment_count['survey_resp']['unresolved'] + $comment_count['survey_admin']['unresolved']); //nerešeni
            echo '</span>/' . ($comment_count['survey_resp']['all'] + $comment_count['survey_admin']['all']) . ')'; //vsi
            echo '<span class="faicon fa-angle-right"></span>';
            echo '</div>';

            $comment_link = 'window.location.href=\'' . $site_url . 'admin/survey/index.php?anketa='.$this->anketa.'&a=komentarji\'; return false;';
            echo '<div class="links" onclick="'.$comment_link.'">';
            echo $lang['srv_testiranje_komentarji_overview'];
            echo ' (<span class="red">' . $comment_count['question']['unresolved'];
            echo '</span>/' . $comment_count['question']['all'] . ')';
            echo '<span class="faicon fa-angle-right"></span>';
            echo '</div>';

            echo '<div class="first_holder_div" id="survey_comment_0_1">';
            $ba = new BranchingAjax($this->anketa);
            $ba->add_comment_field(0, '0', '1', false, false);
            echo '</div>';

            echo '<div class="button_holder">';
            echo '<button class="small white-blue" onclick="toggleCommentDropdown(); return false;">'.$lang['edit1338'].'</button>';
            echo '<button class="small blue" onclick="add_comment(\'0\', \'0\', \'1\', $(\'#vsebina_0_1\').val()); actionNotePopup(\'comment_added\', \'success\');return false;">'.$lang['srv_comments_new'].'</button>';
            echo '</div>';

            echo '</div>'; //comment_dropdown

            // Klik izven  - zapremo dropdown
            echo '<script>
            $(document).mouseup(function(e){
                var container = $("#comment_dropdown");
            
                if (!container.is(e.target) && container.has(e.target).length === 0){
                    $("#comment_dropdown").addClass("displayNone");
                }
            });
            </script>';

            echo '</div>';
            echo '</li>';
        } 
        // ce ni komentarjev, potem prikazemo link do nastavitev komentarjev
        else {    
            echo '<li>';
            echo '<div id="quick_comments_link" class="newCss">';

            // Preverimo, ce je funkcionalnost v paketu, ki ga ima uporabnik
            
            if($userAccess->checkUserAccess('komentarji')) {
                $comment_link = 'window.location.href=\'' . $site_url . 'admin/survey/ajax.php?anketa='.$this->anketa.'&a=comments_onoff&do=on\'; return false;';
                echo '<div class="fa-stack inactive_comments" onclick="'.$comment_link.'"><span class="faicon comments" title="' . $lang['srv_preview_comments'] . '">';
            }
            else {
               /*  $comment_link = 'window.location.href=\'' . $site_url . 'admin/survey/ajax.php?anketa='.$this->anketa.'&a=urejanje\'; return false;';
                echo '<div class="fa-stack" onclick="'.$comment_link.'"><span class="faicon comments user_access_locked" title="' . $lang['srv_preview_comments'] . '">'; */
                $comment_link = 'window.location.href=\'' . $site_url . 'admin/survey/index.php?anketa='.$this->anketa.'&a=urejanje\'; return false;';
                echo '<div class="fa-stack inactive_comments" onclick="'.$comment_link.'"><span class="faicon comments" title="' . $lang['srv_preview_comments'] . '">';
            }
            echo '</span></div>';

            echo '</div>';
            echo '</li>';
        }
    }


    // Vrstica s komentarji pod posameznim vprasanjem v urejanju ankete
	public function display_vprasanje_komentarji ($spremenljivka) {
		global $lang;
		global $admin_type;
		global $global_user_id;


		SurveySetting::getInstance()->Init($this->anketa);
		$question_comment = SurveySetting::getInstance()->getSurveyMiscSetting('question_comment');
		$question_resp_comment = SurveySetting::getInstance()->getSurveyMiscSetting('question_resp_comment');
		$question_resp_comment_viewadminonly = SurveySetting::getInstance()->getSurveyMiscSetting('question_resp_comment_viewadminonly');

		$question_comment_viewadminonly = SurveySetting::getInstance()->getSurveyMiscSetting('question_comment_viewadminonly');
		$question_comment_viewauthor = SurveySetting::getInstance()->getSurveyMiscSetting('question_comment_viewauthor');

		if (!($admin_type <= $question_comment && $question_comment!='') && !($question_resp_comment==1)) return;
		if ($_GET['a'] == 'komentarji') return; // v komentarjih tut ne izpisujemo te vrstice

        $tema_vsebuje = substr($lang['srv_forum_intro'], 0, 10);		// da ne prikazujemo 1. default sporocila

        $f = new Forum;


		echo '<div id="surveycommentcontainer_'.$spremenljivka.'" class="printHide comment_container_inline">';

        
		// komentarji na vprasanje
		if ($admin_type <= $question_comment && $question_comment != '') {

			// GUMB Dodaj komentar
            echo '<div onClick="openCommentPopupInline(\'1\', \''.$spremenljivka.'\'); return false;" id="surveycomment_'.$spremenljivka.'_0" class="surveycomment noline" type="1" subtype="q_admin_add" spremenljivka="'.$spremenljivka.'" view="0" title="'.$lang['srv_question_comment'].'">';
			echo '  <button class="small-rounded white-gray">';
			echo '      <span class="faicon inline_comment"></span> ';
			echo        $lang['srv_add_comment'];
			echo '  </button>';
			echo '</div>';

            // POPUP za dodaj komentar
			echo '<div class="comment_inline_popup displayNone" id="comment_inline_add_'.$spremenljivka.'">';
			
			echo '<div class="comment_inline_top">';
				echo '<div class="title">';
				echo '<span class="faicon fa-comment"></span>';
				echo $lang['srv_add_comment'];
				echo '</div>';
			echo '<span class="gray" onClick="closeCommentPopupInline(\'1\', \''.$spremenljivka.'\'); return false;">✕</span>';
			echo '</div>';

			echo '<div class="first_holder_div" id="survey_comment_'.$spremenljivka.'_0">';
			$ba = new BranchingAjax($this->anketa);
			$ba->add_comment_field($spremenljivka, '1', '0', false, false);
			echo '</div>';

			echo '<div class="button_holder">';
			echo '  <button class="small white-blue" onclick="closeCommentPopupInline(\'1\', \''.$spremenljivka.'\'); return false;">'.$lang['edit1338'].'</button>';
			echo '  <button class="small blue" onclick="add_comment(\''.$spremenljivka.'\', \'1\', \'0\', $(\'#vsebina_'.$spremenljivka.'_0\').val()); actionNotePopup(\'comment_added\', \'success\'); return false;">'.$lang['srv_comments_new'].'</button>';
			echo '</div>';
			
			echo '</div>'; //comment_inline_popup

			// Klik izven  - zapremo popup
			echo '<script>
			$(document).mouseup(function(e){
				var container = $("#comment_inline_add_'.$spremenljivka.'");

                if (!container.is(e.target) && container.has(e.target).length === 0){
					closeCommentPopupInline(\'1\', \''.$spremenljivka.'\');
				}
			});
			</script>';


			// Poglej komentarje
			if ($admin_type <= $question_comment_viewadminonly || $question_comment_viewauthor==1) {

                // Dobimo seznam komentarjev
                $row = Cache::srv_spremenljivka($spremenljivka);
                if ($row['thread'] == 0) {
                    $post_count = 0;
                } 
                else {
                    if ($admin_type <= $question_comment_viewadminonly) {
                        $sqlP = sisplet_query("SELECT * FROM post WHERE tid='$row[thread]' AND vsebina NOT LIKE '%{$tema_vsebuje}%' ORDER BY time");
                    } 
                    elseif ($question_comment_viewauthor==1) {
                        $sqlP = sisplet_query("SELECT * FROM post WHERE tid='$row[thread]' AND uid='$global_user_id' AND vsebina NOT LIKE '%{$tema_vsebuje}%' ORDER BY time");
                    } 
                    else {
                        $sqlP = sisplet_query("SELECT * FROM post WHERE 1 = 0");
                    }
           
                    $post_count = mysqli_num_rows($sqlP);
                }


                // GUMB za poglej komentarje
                echo '<div onClick="openCommentPopupInline(\'2\', \''.$spremenljivka.'\'); return false;" id="surveycomment_'.$spremenljivka.'_1" class="surveycomment noline" type="1" subtype="q_admin_all" spremenljivka="'.$spremenljivka.'" view="1" title="'.$lang['srv_question_comments'].'">';
				echo '  <button class="small-rounded white-gray" id="comment_add_'.$spremenljivka.'"'.($post_count== 0 ? ' style="display:none;"' : '').'>';
				echo '      <span class="faicon inline_double_comment"></span> ';
				echo        $lang['srv_admin_comment'].($post_count > 0 ? ' ('.$post_count.')':'');
                echo '  </button>';
				echo '</div>';

                // POPUP za poglej komentarje
				echo '<div class="comment_inline_popup displayNone" id="comment_inline_editor_'.$spremenljivka.'">';
			
				echo '<div class="comment_inline_top">';
					echo '<div class="title">';
					echo '<span class="faicon fa-comment"></span>';
					echo $lang['srv_admin_comment'];
					echo '</div>';
				echo '<span class="gray" onClick="closeCommentPopupInline(\'2\', \''.$spremenljivka.'\'); return false;">✕</span>';
				echo '</div>';

                // Izpisemo seznam komentarjev
                if($post_count > 0){
                                        
                    while($rowP = mysqli_fetch_array($sqlP)){

                        echo '<div class="comment_box '.($rowP['ocena']==0 ? '' : 'archived').'" id="comment_box_'.$rowP['id'].'">';

                        echo '<div class="by">';

                        $datetime = strtotime($rowP['time']);
                        $datetime = date("d.m.Y G:i", $datetime);
                        
                        echo '<p><span class="user">'.$f->user($rowP['uid']).'</span> <span class="date">('.$datetime.')</span></p>';

                        // Razresi
                        echo '<div class="solve">';
						if ($rowP['ocena']==0) {
							echo '<p class="archive_comment" onclick="question_comment_solve(\'2\', \''.$rowP['id'].'\');"> <span class="faicon fa-check link-right"></span>';
							echo $lang['srv_comments_archive'].'</p>';
						}
						else {
							echo '<p class="unarchive_comment" onclick="question_comment_solve(\'0\', \''.$rowP['id'].'\');"> <span class="faicon fa-undo link-right"></span>';
							echo $lang['srv_comments_unarchive'].'</p>';
						}
						echo '</div>';

                        echo '</div>'; //by


                        echo '<div class="content">';
                        echo $rowP['vsebina'];
                        echo '</div>';

                        echo '</div>';
                    }
                }

				echo '<div class="button_holder">';
				echo '<button class="small blue" onclick="closeCommentPopupInline(\'2\', \''.$spremenljivka.'\'); return false;">'.$lang['srv_zapri'].'</button>';
				echo '</div>';
				
				echo '</div>'; //comment_inline_popup

				// Klik izven  - zapremo popup
				echo '<script>
				$(document).mouseup(function(e){
					var container = $("#comment_inline_editor_'.$spremenljivka.'");
				
					if (!container.is(e.target) && container.has(e.target).length === 0){
						closeCommentPopupInline(\'2\', \''.$spremenljivka.'\');
					}
				});
				</script>';
			}
		}


		// komentarji respondentov
		if (($question_resp_comment==1) AND ($admin_type <= $question_resp_comment_viewadminonly)) {

            // Dobimo seznam komentarjev respondentov
			$sqlP = sisplet_query("SELECT * FROM srv_data_text".$this->db_table." WHERE spr_id='0' AND vre_id='$spremenljivka' ORDER BY id ASC");
            
            $post_count = mysqli_num_rows($sqlP);

			if ($post_count > 0) {

                // GUMB za komentarji respondentov
				echo '<div onClick="openCommentPopupInline(\'3\', \''.$spremenljivka.'\'); return false;" id="surveycomment_'.$spremenljivka.'_2" class="surveycomment noline" type="2" subtype="q_resp_all" spremenljivka="'.$spremenljivka.'">';
				echo '  <button class="small-rounded white-gray">';
                echo '      <span class="faicon inline_comment icon-orange"></span> ';
				echo        $lang['srv_repondent_comment'].' ('.$post_count.')';
				echo '  </button>';
				echo '</div>';

                // POPUP za komentarji respondentov
				echo '<div class="comment_inline_popup displayNone" id="comment_inline_respondent_'.$spremenljivka.'">';
			
				echo '<div class="comment_inline_top">';
                echo '  <div class="title">';
                echo '      <span class="faicon fa-comment"></span>';
                echo        $lang['srv_repondent_comment'];
                echo '  </div>';
				echo '  <span class="gray" onClick="closeCommentPopupInline(\'3\', \''.$spremenljivka.'\'); return false;">✕</span>';
				echo '</div>';

				// Izpisemo seznam komentarjev                                       
                while($rowP = mysqli_fetch_array($sqlP)){

                    echo '<div class="comment_box  '.($rowP['text2']==0 ? '' : 'archived').'" id="comment_box_'.$rowP['id'].'">';

                    // Ce smo slucajno pobrisali testne vnose, nimamo casa vnosa komentarja
                    $sqlTime = sisplet_query("SELECT time_edit FROM srv_user WHERE id='".$rowP['usr_id']."'");
                    if(mysqli_num_rows($sqlTime) > 0){
                        
                        $rowTime = mysqli_fetch_array($sqlTime);


                        echo '<div class="by">';

                        if ( strpos($rowP['text'], '__DATE__') !== false ) {
                            $rowP['text'] = str_replace('__DATE__', $f->datetime1($rowTime['time_edit']), $rowP['text']);
                            $datetime = '-';
                            
                        } 
                        else {

                            $datetime = strtotime($rowTime['time_edit']);
                            $datetime = date("d.m.Y G:i", $datetime);
                        }

                        echo '<p>Respondent <span class="date">('.$datetime.')</span></p>';

                        // Razresi
                        echo '<div class="solve">';
						if ($rowP['text2']==0) {
							echo '<p class="archive_comment" onclick="question_respondent_comment_solve(\'2\', \''.$rowP['id'].'\');"> <span class="faicon fa-check link-right"></span>';
							echo $lang['srv_comments_archive'].'</p>';
						}
						else {
							echo '<p class="unarchive_comment" onclick="question_respondent_comment_solve(\'0\', \''.$rowP['id'].'\');"> <span class="faicon fa-undo link-right"></span>';
							echo $lang['srv_comments_unarchive'].'</p>';
						}
						echo '</div>';

                        echo '</div>'; //by

                    }
                    else{
                        if ( strpos($rowP['text'], '__DATE__') !== false ) {
                            $rowP['text'] = str_replace('__DATE__', '', $rowP['text']);
                        } 
                    }

                    echo '<div class="content">';
                    echo nl2br($rowP['text']);
                    echo '</div>';

                    echo '</div>';
                }

				echo '<div class="button_holder">';
				echo '  <button class="small blue" onclick="closeCommentPopupInline(\'3\', \''.$spremenljivka.'\'); return false;">'.$lang['srv_zapri'].'</button>';
				echo '</div>';
				
				echo '</div>'; //comment_inline_popup

				// Klik izven  - zapremo popup
				echo '<script>
				$(document).mouseup(function(e){
					var container = $("#comment_inline_respondent_'.$spremenljivka.'");
				
					if (!container.is(e.target) && container.has(e.target).length === 0){
						closeCommentPopupInline(\'3\', \''.$spremenljivka.'\');
					}
				});
				</script>';
			}
			echo '</span>';
		}


		echo '</div>';
	}
	
	// Vrstica s komentarji pod posameznim blokom ali ifom v urejanju ankete
	public function display_if_komentarji ($if_id, $block=0) {
		global $lang;
		global $admin_type;
		global $global_user_id;

		SurveySetting::getInstance()->Init($this->anketa);
		$question_comment = SurveySetting::getInstance()->getSurveyMiscSetting('question_comment');

		$question_comment_viewadminonly = SurveySetting::getInstance()->getSurveyMiscSetting('question_comment_viewadminonly');
		$question_comment_viewauthor = SurveySetting::getInstance()->getSurveyMiscSetting('question_comment_viewauthor');

		if (!($admin_type <= $question_comment && $question_comment!='')) return;
		if ($_GET['a'] == 'komentarji') return; // v komentarjih tut ne izpisujemo te vrstice

		$f = new Forum;
		$tema_vsebuje = substr($lang['srv_forum_intro'], 0, 10);		// da ne prikazujemo 1. default sporocila

		$type = ($block == 1) ? '6' : '5';


		// komentarji na if ali blok
		if ($admin_type <= $question_comment && $question_comment != '') {

			echo '<div class="comment_container_inline">';

			// Dobimo seznam komentarjev
			$row = Cache::srv_if($if_id);
			if ($row['thread'] == 0) {
				$post_count = 0;
			} 
			else {
				if ($admin_type <= $question_comment_viewadminonly) {
					$sqlP = sisplet_query("SELECT * FROM post WHERE tid='$row[thread]' AND vsebina NOT LIKE '%{$tema_vsebuje}%' ORDER BY time");
				} 
				elseif ($question_comment_viewauthor==1) {
					$sqlP = sisplet_query("SELECT * FROM post WHERE tid='$row[thread]' AND uid='$global_user_id' AND vsebina NOT LIKE '%{$tema_vsebuje}%' ORDER BY time");
				} 
				else {
					$sqlP = sisplet_query("SELECT * FROM post WHERE 1 = 0");
				}
	   
				$post_count = mysqli_num_rows($sqlP);
			}

			// Poglej komentarje
			if ($admin_type <= $question_comment_viewadminonly || $question_comment_viewauthor==1) {

				// GUMB za poglej komentarje
				echo '<div onClick="openIfCommentPopupInline(\'2\', \''.$if_id.'\'); return false;" id="surveycomment_'.$if_id.'_1" class="surveycomment noline" type="1" subtype="if_admin_all" spremenljivka="'.$if_id.'" view="1" title="'.$lang['srv_question_comments'].'">';
				echo '  <button class="small-rounded white-gray" id="comment_add_'.$if_id.'"'.($post_count== 0 ? ' style="display:none;"' : '').'>';
				echo '      <span class="faicon inline_double_comment"></span> ';
				echo        $lang['srv_admin_comment'].($post_count > 0 ? ' ('.$post_count.')':'');
				echo '  </button>';
				echo '</div>';

				// POPUP za poglej komentarje
				echo '<div class="comment_inline_popup displayNone" id="comment_if_inline_editor_'.$if_id.'">';

				echo '<div class="comment_inline_top">';
					echo '<div class="title">';
					echo '<span class="faicon fa-comment"></span>';
					echo $lang['srv_admin_comment'];
					echo '</div>';
				echo '<span class="gray" onClick="closeIfCommentPopupInline(\'2\', \''.$if_id.'\'); return false;">✕</span>';
				echo '</div>';

				// Izpisemo seznam komentarjev
				if($post_count > 0){
										
					while($rowP = mysqli_fetch_array($sqlP)){

						echo '<div class="comment_box '.($rowP['ocena']==0 ? '' : 'archived').'" id="comment_box_'.$rowP['id'].'">';

						echo '<div class="by">';

						$datetime = strtotime($rowP['time']);
						$datetime = date("d.m.Y G:i", $datetime);
						
						echo '<p><span class="user">'.$f->user($rowP['uid']).'</span> <span class="date">('.$datetime.')</span></p>';

						// Razresi
						echo '<div class="solve">';
						if ($rowP['ocena']==0) {
							echo '<p class="archive_comment" onclick="question_comment_solve(\'2\', \''.$rowP['id'].'\');"> <span class="faicon fa-check link-right"></span>';
							echo $lang['srv_comments_archive'].'</p>';
						}
						else {
							echo '<p class="unarchive_comment" onclick="question_comment_solve(\'0\', \''.$rowP['id'].'\');"> <span class="faicon fa-undo link-right"></span>';
							echo $lang['srv_comments_unarchive'].'</p>';
						}
						echo '</div>';

						echo '</div>'; //by


						echo '<div class="content">';
						echo $rowP['vsebina'];
						echo '</div>';

						echo '</div>';
					}
				}

				echo '<div class="button_holder">';
				echo '<button class="small blue" onclick="closeIfCommentPopupInline(\'2\', \''.$if_id.'\'); return false;">'.$lang['srv_zapri'].'</button>';
				echo '</div>';

				echo '</div>'; //comment_inline_popup

				// Klik izven  - zapremo popup
				echo '<script>
				$(document).mouseup(function(e){
					var container = $("#comment_if_inline_editor_'.$if_id.'");

					if (!container.is(e.target) && container.has(e.target).length === 0){
						closeIfCommentPopupInline(\'2\', \''.$if_id.'\');
					}
				});
				</script>';
			}
			

			// GUMB Dodaj komentar
            echo '<div onClick="openIfCommentPopupInline(\'1\', \''.$if_id.'\'); return false;" id="surveycomment_'.$if_id.'_0" class="surveycomment noline" type="1" subtype="if_admin_add" spremenljivka="'.$if_id.'" view="0" title="'.$lang['srv_question_comment'].'">';
			echo '  <button class="small-rounded white-gray">';
			echo '      <span class="faicon inline_comment"></span> ';
			echo        $lang['srv_add_comment'];
			echo '  </button>';
			echo '</div>';

            // POPUP za dodaj komentar
			echo '<div class="comment_inline_popup displayNone" id="comment_if_inline_add_'.$if_id.'">';
			
			echo '<div class="comment_inline_top">';
			echo '<div class="title">';
			echo '<span class="faicon fa-comment"></span>';
			echo $lang['srv_add_comment'];
			echo '</div>';
			echo '<span class="gray" onClick="closeIfCommentPopupInline(\'1\', \''.$if_id.'\'); return false;">✕</span>';
			echo '</div>';

			echo '<div class="first_holder_div" id="survey_comment_'.$if_id.'_0">';
			$ba = new BranchingAjax($this->anketa);
			$ba->add_comment_field($if_id, $type, '0', false, false);
			echo '</div>';

			echo '<div class="button_holder">';
			echo '  <button class="small white-blue" onclick="closeIfCommentPopupInline(\'1\', \''.$if_id.'\'); return false;">'.$lang['edit1338'].'</button>';
			echo '  <button class="small blue" onclick="add_comment(\''.$if_id.'\', \''.$type.'\', \'0\', $(\'#vsebina_'.$if_id.'_0\').val()); actionNotePopup(\'comment_added\', \'success\'); return false;">'.$lang['srv_comments_new'].'</button>';
			echo '</div>';
			
			echo '</div>'; //comment_inline_popup

			// Klik izven  - zapremo popup
			echo '<script>
			$(document).mouseup(function(e){
				var container = $("#comment_if_inline_add_'.$if_id.'");

                if (!container.is(e.target) && container.has(e.target).length === 0){
					closeIfCommentPopupInline(\'1\', \''.$if_id.'\');
				}
			});
			</script>';

			echo '</div>';
		}
	}

	
	// $return pove a vrne text (true) ali samo številko (false)
	private function testiranje_komentarji_komentarji_na_vprasanje ($return = true) {
			
		# naenkrat preberemo vse spremenljivke, da ne delamo queryja vsakic posebej
		$spremenljivke = Cache::cache_all_srv_spremenljivka($this->anketa, true);
			
		$spr_id = array();
		$threads = array();
		$rowqr = array();
		$rowqrf = array();
		$rowtqa = array();
		$rowtqu = array();

		if ( is_array($spremenljivke) && count($spremenljivke) > 0 ) {
			foreach ($spremenljivke as $id=>$value) {
				$spr_id[] = $id;
				if ((int)$value['thread'] > 0) {
					$threads[] = $value['thread'];
				}
			}
		}
		if (count($spr_id) > 0) {
			#preštejemo komentarje uporabnikov na vprašanja
			# srv_data_text where spr_id = 0 AND vre_id IN (id-ji spremenljivk)
			$strqr = "SELECT count(*) FROM srv_data_text".$this->db_table." WHERE spr_id=0 AND vre_id IN (".implode(',',$spr_id).")";
			$sqlqr = sisplet_query($strqr);
			list($rowqr) = mysqli_fetch_row($sqlqr);
			
			#končani komentarji respondentov
			#text2 = 2 => končan
			#text2 = 3 => nerelevantno
			$strqrf = "SELECT count(*) FROM srv_data_text".$this->db_table." WHERE spr_id=0 AND vre_id IN (".implode(',',$spr_id).") AND text2 IN (2,3)";
			$sqlqrf = sisplet_query($strqrf);
			list($rowqrf) = mysqli_fetch_row($sqlqrf);
			
			# preštejemo
			if (count($threads) > 0) {
				# vsi komentarji na anketo
				$strta = "SELECT count(*) FROM post WHERE tid IN (".implode(',',$threads).") AND parent > 0";
				$sqlta = sisplet_query($strta);
				list($rowtqa) = mysqli_fetch_row($sqlta);
				# nerešeni komentarji: only_unresolved =>   ocena <= 1
				$strtu = "SELECT count(*) FROM post WHERE tid IN (".implode(',',$threads).") AND parent > 0 AND ocena IN (2,3) ";
				$sqltu = sisplet_query($strtu);
				list($rowtqu) = mysqli_fetch_row($sqltu);
			}
		}
			
		#vsi
		$all = (int)((int)$rowqr + (int)$rowtqa);
		
		# nerešeni
		$unresolved = $all - (int)((int)$rowqrf + (int)$rowtqu);
			
		if ($return)
			// '(<span class="lightRed">'.$unresolved.'</span>/'.$all.')';
			return $unresolved;
		else
			return $unresolved;
		
	}
	
    // Prestejemo vse komentarje
    private function testiranje_komentarji_count () {
		
		$comment_count = array();
		
		$rowi = SurveyInfo::getInstance()->getSurveyRow();
		
		
		// KOMENTARJI NA ANKETO - UREDNIK
		# vsi komentarji na anketo
		$strta = "SELECT count(*) FROM post WHERE tid='".$rowi['thread']."' AND parent > 0";
		$sqlta = sisplet_query($strta);
		list($rowta) = mysqli_fetch_row($sqlta);

		# nerešeni komentarji: only_unresolved =>   ocena <= 1
		$strtu = "SELECT count(*) FROM post WHERE tid='".$rowi['thread']."' AND parent > 0 AND ocena <= 1 ";
		$sqltu = sisplet_query($strtu);
		list($rowtu) = mysqli_fetch_row($sqltu);
		//(int)$rowtu.'/'.(int)$rowta;
		$comment_count['survey_admin']['all'] = (int)$rowta;
		$comment_count['survey_admin']['unresolved'] = (int)$rowtu;
		
		
		// KOMENTARJI NA ANKETO - RESPONDENT
		# vsi komentarji na anketo
		$strta = "SELECT count(*) FROM srv_comment_resp WHERE ank_id='".$this->anketa."'";
		$sqlta = sisplet_query($strta);
		list($rowta) = mysqli_fetch_row($sqlta);

		# nerešeni komentarji: only_unresolved =>   ocena <= 1
		$strtu = "SELECT count(*) FROM srv_comment_resp WHERE ank_id='".$this->anketa."' AND ocena <= 1 ";
		$sqltu = sisplet_query($strtu);
		list($rowtu) = mysqli_fetch_row($sqltu);
		//(int)$rowtu.'/'.(int)$rowta;
		$comment_count['survey_resp']['all'] = (int)$rowta;
		$comment_count['survey_resp']['unresolved'] = (int)$rowtu;

		
		// KOMENTARJI NA VPRASANJE
		# naenkrat preberemo vse spremenljivke, da ne delamo queryja vsakic posebej
		$spremenljivke = Cache::cache_all_srv_spremenljivka($this->anketa, true);	
		$spr_id=array();
		$threads=array();
		if ( is_array($spremenljivke) && count($spremenljivke) > 0 ) {
		
			foreach ($spremenljivke as $id=>$value) {
				$spr_id[] = $id;
				
				if ((int)$value['thread'] > 0) {
					$threads[] = $value['thread'];
				}
			}
		}

		$rowqr = array();
		$rowtqa = array();
		$rowqrf = array();
		$rowtqu = array();
		if (count($spr_id) > 0) {
			#preštejemo komentarje uporabnikov na vprašanja
			# srv_data_text where spr_id = 0 AND vre_id IN (id-ji spremenljivk)
			$strqr = "SELECT count(*) FROM srv_data_text".$this->db_table." WHERE spr_id=0 AND vre_id IN (".implode(',',$spr_id).")";
			$sqlqr = sisplet_query($strqr);
			list($rowqr) = mysqli_fetch_row($sqlqr);
			
			#končani komentarji respondentov
			#text2 = 2 => končan
			#text2 = 3 => nerelevantno
			$strqrf = "SELECT count(*) FROM srv_data_text".$this->db_table." WHERE spr_id=0 AND vre_id IN (".implode(',',$spr_id).") AND text2 IN (2,3)";
			$sqlqrf = sisplet_query($strqrf);
			list($rowqrf) = mysqli_fetch_row($sqlqrf);
			
			# preštejemo
			if (count($threads) > 0) {
				# vsi komentarji na anketo
				$strta = "SELECT count(*) FROM post WHERE tid IN (".implode(',',$threads).") AND parent > 0";
				$sqlta = sisplet_query($strta);
				list($rowtqa) = mysqli_fetch_row($sqlta);
				# nerešeni komentarji: only_unresolved =>   ocena <= 1
				$strtu = "SELECT count(*) FROM post WHERE tid IN (".implode(',',$threads).") AND parent > 0 AND ocena IN (2,3) ";
				$sqltu = sisplet_query($strtu);
				list($rowtqu) = mysqli_fetch_row($sqltu);
			}
		}
			
		#vsi
		$comment_count['question']['all'] = (int)((int)$rowqr + (int)$rowtqa);
		$comment_count['question']['unresolved'] = $comment_count['question']['all'] - (int)((int)$rowqrf + (int)$rowtqu);
		
		// KOMENTARJI NA IF ALI BLOK
		# naenkrat preberemo vse ife in bloke, da ne delamo queryja vsakic posebej
		$ifi = Cache::cache_all_srv_if($this->anketa, true);	
		$if_id = array();
		$threads_if = array();
		if ( is_array($ifi) && count($ifi) > 0 ) {
		
			foreach ($ifi as $id=>$value) {
				$if_id[] = $id;
				
				if ((int)$value['thread'] > 0) {
					$threads_if[] = $value['thread'];
				}
			}
		}

		$rowqr_if = array();
		$rowtqa_if = array();
		$rowqrf_if = array();
		$rowtqu_if = array();
		if (count($if_id) > 0) {
			
			#preštejemo komentarje uporabnikov na vprašanja
			# srv_data_text where if_id = 0 AND vre_id IN (id-ji spremenljivk)
			$strqr = "SELECT count(*) FROM srv_data_text".$this->db_table." WHERE spr_id=0 AND vre_id IN (".implode(',',$if_id).")";
			$sqlqr = sisplet_query($strqr);
			list($rowqr_if) = mysqli_fetch_row($sqlqr);
			
			#končani komentarji respondentov
			#text2 = 2 => končan
			#text2 = 3 => nerelevantno
			$strqrf = "SELECT count(*) FROM srv_data_text".$this->db_table." WHERE spr_id=0 AND vre_id IN (".implode(',',$if_id).") AND text2 IN (2,3)";
			$sqlqrf = sisplet_query($strqrf);
			list($rowqrf_if) = mysqli_fetch_row($sqlqrf);
			
			# preštejemo
			if (count($threads_if) > 0) {
				# vsi komentarji na anketo
				$strta = "SELECT count(*) FROM post WHERE tid IN (".implode(',',$threads_if).") AND parent > 0";
				$sqlta = sisplet_query($strta);
				list($rowtqa_if) = mysqli_fetch_row($sqlta);
				# nerešeni komentarji: only_unresolved =>   ocena <= 1
				$strtu = "SELECT count(*) FROM post WHERE tid IN (".implode(',',$threads_if).") AND parent > 0 AND ocena IN (2,3) ";
				$sqltu = sisplet_query($strtu);
				list($rowtqu_if) = mysqli_fetch_row($sqltu);
			}
		}
		
		$comment_count['question']['all'] += (int)((int)$rowqr_if + (int)$rowtqa_if);
		$comment_count['question']['unresolved'] += ((int)((int)$rowqr_if + (int)$rowtqa_if)) - ((int)((int)$rowqrf_if + (int)$rowtqu_if));
		
		
		return $comment_count;
	}
}

?>