<?php

/**
* 
* Pobrisana vprasanja - pred brisanjem se vprasanje kopira med pobrisane - podobno kot knjiznica
* 
*/

class VprasanjeDeleted {
	
	var $ank_id;                // trenutna anketa

		
	public function __construct ($anketa = 0) {
		
		if (isset ($_GET['anketa']))
			$this->ank_id = $_GET['anketa'];
		elseif (isset ($_POST['anketa'])) 
			$this->ank_id = $_POST['anketa'];
		elseif ($anketa != 0) 
			$this->ank_id = $anketa;
	}
	
    
    // Prikazemo seznam pobrisanih vprasanj uporabnika
    private function displayDeletedVprasanja(){
        global $lang;
        global $global_user_id;
        global $admin_type;


        // Naslov v oknu
        echo '<h2><span class="faicon delete"></span>'.$lang['srv_deleted_vprasanja'].'</h2>';
        echo '<div class="popup_close"><a href="#" onclick="popupClose();">✕</a></div>';


        // Vsebina
        echo '<div id="vprasanje_deleted_content" class="content">';

        
        // Seznam vseh pobrisanih vprasanj
        echo '<div class="question_list">';

        // Loop po vseh pobrisanih vprasanjih v anketi
        $sql = sisplet_query("SELECT sd.*, s.naslov 
                                FROM srv_spremenljivka_deleted sd, srv_spremenljivka s 
                                WHERE sd.ank_id='".$this->ank_id."' AND sd.spr_id=s.id 
                                ORDER BY sd.delete_time DESC
                            ");  

        while($row = mysqli_fetch_array($sql)){
            
            echo '<div id="question_item_holder_'.$row['spr_id'].'" class="question_item_holder" onClick="selectVprasanjeDeletedItem(\''.$row['spr_id'].'\');">';
            
            echo '  <input type="checkbox" id="question_item_check_'.$row['spr_id'].'" class="question_item_check" onClick="selectVprasanjeDeletedItem(\''.$row['spr_id'].'\');"><label for="question_item_check_'.$row['spr_id'].'"></label>';
            
            echo '  <div id="question_item_info_'.$row['spr_id'].'" item-id="'.$row['spr_id'].'" class="question_item_info">';
            
            echo '      <div>';
            echo '          <span class="faicon list"></span>';
            echo '          <span class="title">'.substr(strip_tags($row['naslov']), 0, 40).'</span>';
            echo '          <span class="date">('.date("G:i d.m.Y", strtotime($row['delete_time'])).')</span>';
            echo '      </div>';
            
            echo '      <span class="faicon monitor" title="'.$lang['srv_predogled_spremenljivka'].'" onClick="previewVprasanje(\''.$row['spr_id'].'\');"></span>';
            
            echo '  </div>';
            
            echo '</div>';
        }

        echo '</div>';


        // Item counter
        echo '<div class="selected_items">'.$lang['srv_library_item_counter'].': <span id="selected_item_counter">0</span></div>';


        // Gumbi na dnu
        echo '<div class="button_holder">';
        echo '  <button class="medium white-blue" onClick="popupClose();">'.$lang['edit1338'].'</button>';
        echo '  <button id="insert_vprasanje_deleted_button" class="medium blue" disabled="disabled" onClick="insertVprasanjeDeletedItemsIntoSurvey();">'.$lang['srv_library_survey_add'].'</button>';
        echo '</div>';


        echo '</div>';
    }


    // Pri brisanju najprej skopiramo vprasanje ki ga brisemo
    public function createDeletedVprasanje($spr_id){
        global $global_user_id;

        // Skopiramo spremenljivko 
        $b = new Branching($this->ank_id);
        $new_spr_id = $b->nova_spremenljivka(-3, 0, 0, $spr_id);  

        // Zabelezimo v bazi povezavo z anketo in cas brisanja
        $sql1 = sisplet_query("INSERT INTO srv_spremenljivka_deleted (spr_id, ank_id, delete_time) VALUES ('".$new_spr_id."', '".$this->ank_id."', NOW())");
    }

    // Vrnemo stevilo pobrisanih vprasanj v anketi
    public function countDeletedVprasanja(){

        $sql = sisplet_query("SELECT COUNT(id) AS cnt FROM srv_spremenljivka_deleted WHERE ank_id='".$this->ank_id."'");  
        $row = mysqli_fetch_array($sql);

        return $row['cnt'];
    }


    // Dokoncno brisanje vprasanja
    public static function permanentDeleteVprasanje($spremenljivka){

        if ($spremenljivka > 0) {

            $rowg = Cache::srv_spremenljivka($spremenljivka);

            // pri brisanju multiple grid vprasanja, moramo pobrisate tudi vse child spremenljivke (ker kljuci niso nastavljeni)
            if ($rowg['tip'] == 24) {
                $sqld = sisplet_query("SELECT spr_id FROM srv_grid_multiple WHERE parent='$spremenljivka'");
                while ($rowd = mysqli_fetch_array($sqld)) {
                    sisplet_query("DELETE FROM srv_spremenljivka WHERE id='$rowd[spr_id]'");
                }
            }

            $sql = sisplet_query("DELETE FROM srv_vrednost WHERE spr_id='$spremenljivka'");
            $sql = sisplet_query("DELETE FROM srv_grid WHERE spr_id='$spremenljivka'");
            $sql = sisplet_query("DELETE FROM srv_spremenljivka WHERE id='$spremenljivka'");
        }
    }


    /**
    * @desc pohendla ajax klice
    */
    public function ajax () {
        global $lang;
        global $global_user_id;


        // Odpremo popup s pobrisanimi vprasanji
    	if ($_GET['a'] == 'displayVprasanjeDeletedPopup') {
            $this->displayDeletedVprasanja();

            // Div za dodaten popup za preview
            echo '<div id="vprasanje_preview" class="displayNone"></div>';
        } 

        // Dodajanje pobrisanega vprasanja nazaj v vprasalnik
        elseif ($_GET['a'] == 'addIntoSurvey') {
            
            $items = $_POST['items'];
            
            $b = new Branching($this->ank_id);
            $ba = new BranchingAjax($this->ank_id);
            
            foreach(array_reverse($items) as $item_id){

                $last_spr = $b->find_last_spr();

                // Dodamo vprasanje v anketo           
                $ba->spremenljivka_new($spremenljivka=$last_spr, $if=0, $endif=0, $copy=$item_id, $drop=0);   
            }

            Common::getInstance()->Init($this->ank_id);
            Common::getInstance()->prestevilci(0, $all=true);

            $b->branching_struktura();
        } 


        // Dokoncen izbris pobrisanega vprasanja
        elseif ($_GET['a'] == 'deleteItem') {
            
            $item_id = $_POST['item_id'];

            if($item_id != '' && $item_id > 0){
                sisplet_query("DELETE FROM srv_spremenljivka WHERE id='".$item_id."'AND folder!='0'");
            }
        }
    }
}

?>