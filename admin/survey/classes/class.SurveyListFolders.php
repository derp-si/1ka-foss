<?php

/** 
 *  Moje ankete - nov prikaz direktorijev (19.9.2022)
 */


class SurveyListFolders extends SurveyList{

    
    private $current_folder = array();
    private $surveys_count = array();
			

	// Prikazemo pogled s folderji
	public function getSurveys() {
		global $global_user_id;
		global $lang;			


        // Nastavimo trenutni folder na root
        $this->current_folder = array(
            'id'        => 0,
            'naslov'    => $lang['srv_mySurvey_all_surveys']
        );


        echo '<div class="survey_list_folders">';

        // Levi meni s folderji
        echo '  <div id="left_content" class="left_content">';
        $this->displayLeftContent();
        echo '  </div>';
        
        // Desna vsebina
        echo '  <div id="right_content" class="right_content">';
        $this->displayRightContent();
        echo '  </div>';

        echo '</div>';


        // JS za drag/drop
        echo '<script type="text/javascript">mySurvey_folder_init();</script>';
	}



    // Izpisemo levo stran - seznam folderjev
    private function displayLeftContent(){
        global $lang;

        echo '<div class="title">'.$lang['srv_mySurvey_your_folders'].'</div>';

        // Izpisemo seznam folder s seznamom childov v rootu
        echo '<ul id="folder_list_0" class="folder_list">';
        $this->displayFolderList(0);
        echo '</ul>';

        // Prikazemo se skrit div za dodaten popup (rename, add - title)
        echo '<div id="mySurvey_additional_popup" class="divPopUp"></div>';

        // Klik izven "Vec" - zapremo okno
        echo '<script>
            $(document).mouseup(function(e){
                var container = $(".folder_item_settings, .dots_ver");
            
                if (!container.is(e.target) && container.has(e.target).length === 0){
                    $(".folder_item_settings").addClass("displayNone");
                    $(".dots_ver_folder").removeClass("active");
                }

                var container2 = $(".item_settings, .dots_ver");
            
                if (!container.is(e.target) && container.has(e.target).length === 0){
                    $(".item_settings").addClass("displayNone");
                    $(".dots_ver_item").removeClass("active");
                }
            });

            initHideLibraryArrows();
        </script>';
    } 

    // Izpisemo seznam folderjev za posameznega parenta
    private function displayFolderList($folder_id){
        global $global_user_id;
        
        // Izpisemo folder box
        $folder_open = $this->displayFolder($folder_id);

        // Izpisemo se vse subfolderje
        echo '<ul id="folder_list_'.$folder_id.'" class="folder_list" '.($folder_open ? '' : 'style="display:none;"').'">';

        $sqlChildren = sisplet_query("SELECT id FROM srv_mysurvey_folder WHERE usr_id='".$global_user_id."' AND parent='".$folder_id."' ORDER BY naslov ASC");
        while($rowChildren = mysqli_fetch_array($sqlChildren)){
            $this->displayFolderList($rowChildren['id']);
        }

        echo '</ul>';
    }

    // Izpisemo posamezen folder
    private function displayFolder($folder_id){
        global $lang;
        global $global_user_id;

        // Ce gre za root folder dobimo stevilo anket brez folderja
        if($folder_id == '0'){

            // Prestejemo ankete po folderjih
            $this->countSurveysInFolders();

            // Preverimo, ce ni noben drug folder aktiviran - potem je aktiviran root
            $sql = sisplet_query("SELECT COUNT(id) AS active_count FROM srv_mysurvey_folder WHERE active='1' AND usr_id='".$global_user_id."'");
            $row = mysqli_fetch_array($sql);

            $folder_active = ($row['active_count'] == 0) ? 'active' : '';
            $folder_open = 'open';
        }
        // Ce ne gre za root folder dobimo podatke o folderju
        else{

            // Dobimo podatke folderja
            $sql = sisplet_query("SELECT * FROM srv_mysurvey_folder WHERE id='".$folder_id."'");
            if (!$sql)
                echo mysqli_error($GLOBALS['connect_db']);

            $row = mysqli_fetch_array($sql);

            // Nastavimo, ce je to trenutno odprt folder
            if($row['active'] == '1')
                $this->current_folder = $row;

            $folder_active = ($row['active'] == '1') ? 'active' : '';
            $folder_open = ($row['open'] == '1') ? 'open' : '';
        }


        // Izrisemo folder box
        // Ce je root
        if($folder_id == '0'){
            echo '<li id="folder_item_'.$folder_id.'" class="folder_item '.$folder_open.' '.$folder_active.' mySurvey_droppable" folder_id="'.$folder_id.'" onClick="mySurvey_folder_activate(\''.$folder_id.'\');">';
        
            echo '<span class="faicon folder"></span>';

            echo '<div class="folder_item_title">'.$lang['srv_mySurvey_all_surveys'].'</div>';  
            echo '<span class="folder_item_child_count">'.(isset($this->surveys_count['0']) ? $this->surveys_count['0'] : '0').'</span>';

            // Urejanje folderja
            // Tri pikice za prikaz urejanja folderja
            echo '  <span class="faicon dots_ver dots_ver_folder" onClick="mySurvey_folder_show_edit(this);"></span>';

            // Skrit div za urejanje folderja
            echo '  <div class="folder_item_settings displayNone">';
            echo '      <ul>';
            echo '          <li onClick="mySurvey_folder_add_popup(\''.$folder_id.'\');">'.$lang['srv_mySurvey_create_subfolder'].'</li>';
            echo '      </ul>';
            echo '  </div>';
        }
        else{            
            echo '<li id="folder_item_'.$folder_id.'" class="folder_item '.$folder_open.' '.$folder_active.' mySurvey_draggable mySurvey_droppable" folder_id="'.$folder_id.'" onClick="mySurvey_folder_activate(\''.$folder_id.'\');">';
        
            echo '<span class="faicon arrow" onClick="mySurvey_folder_toggle(\''.$folder_id.'\');"></span>';
            echo '<span class="faicon folder"></span>';

            echo '<div class="folder_item_title">'.$row['naslov'].'</div>';
            echo '<span class="folder_item_child_count">'.(isset($this->surveys_count[$folder_id]) ? $this->surveys_count[$folder_id] : '0').'</span>';

            // Urejanje folderja
            // Tri pikice za prikaz urejanja folderja
            echo '  <span class="faicon dots_ver dots_ver_folder" onClick="mySurvey_folder_show_edit(this);"></span>';

            // Skrit div za urejanje folderja
            echo '  <div class="folder_item_settings displayNone">';
            echo '      <ul>';
            echo '          <li onClick="mySurvey_folder_add_popup(\''.$folder_id.'\');">'.$lang['srv_mySurvey_create_subfolder'].'</li>';
            echo '          <li onClick="mySurvey_folder_rename_popup(\''.$folder_id.'\', \''.$row['naslov'].'\');">'.$lang['srv_mySurvey_rename_folder'].'</li>';
            echo '          <li onClick="mySurvey_folder_delete(\''.$folder_id.'\');">'.$lang['srv_mySurvey_delete_folder'].'</li>';
            echo '      </ul>';
            echo '  </div>';
        }

        echo '</li>';


        // Vrnemo, ce je folder odprt ali ne
        if($folder_open == 'open')
            return true;
        else
            return false;
    }

    // Dodaten popup za ime direktorija pri dodajanju
    private function displayAddFolderPopup($folder_id){
        global $lang;

        echo '<h2>'.$lang['srv_library_folder_name'].'</h2>';

        echo '<div>';
        echo '  <input type="text" id="mySurvey_folder_name" class="large">';
        echo '</div>';

        echo '<div class="button_holder">';
        echo '  <button class="medium white-blue" onClick="mySurvey_folder_close_popup();">'.$lang['edit1338'].'</button>';
        echo '  <button class="medium blue" onClick="mySurvey_folder_add(\''.$folder_id.'\');">'.$lang['srv_mySurvey_create_subfolder'].'</button>';
        echo '</div>';
    }

    // Dodaten popup za ime direktorija pri preimenovanju
    private function displayRenameFolderPopup($folder_id, $folder_name){
        global $lang;

        echo '<h2>'.$lang['srv_library_folder_name'].'</h2>';

        echo '<div>';
        echo '  <input type="text" id="mySurvey_folder_name" class="large" value="'.$folder_name.'">';
        echo '</div>';

        echo '<div class="button_holder">';
        echo '  <button class="medium white-blue" onClick="mySurvey_folder_close_popup();">'.$lang['edit1338'].'</button>';
        echo '  <button class="medium blue" onClick="mySurvey_folder_rename(\''.$folder_id.'\');">'.$lang['srv_mySurvey_rename_folder'].'</button>';
        echo '</div>';
    }



    // Izpisemo desnostran - vsebino odprtega folderja
    private function displayRightContent(){

        // Naslov trenutnega folderja
        echo '<div class="folder_title">';
        $this->displayFolderTitle();
        echo '</div>';


        // Vrstica z gumbi, filtri - niso prikazane pri searchu
        echo '<div class="second_line">';

		if($this->isSearch != 1){	
            $this->displayButtonsRow();
		}
        else{
            // Okno z nastavitvami searcha
            echo '<div id="searchSettings">';
            $this->displaySearchSettings();
            echo '</div>';
        }
        
        echo '</div>';
        

        # koliko imamo strani
        $survey_count = (isset($this->surveys_count[$this->current_folder['id']]) ? $this->surveys_count[$this->current_folder['id']] : 0);
        $this->max_pages = (int)$this->rec_per_page > 0 ? ceil($survey_count / $this->rec_per_page) : 1;
                                
        # katera je trenutna stran
        if (isset($_GET['pageno']) && (int)$_GET['pageno'] > 0) {
            # izbrana stran ne more biti večja, kot pa imamo vseh strani
            $this->pageno = min((int)$_GET['pageno'], $this->max_pages);
        }

        
		// Izris seznama anket      
        echo '<div class="div_sl_new folders">';
        $this->displayNewSurveyList();
        echo '</div>';

        // Paginacija - se na dnu - ni prikazana pri searchu
        if($this->isSearch != 1){            
            echo '<div class="pagination_bottom_holder">';
            echo '  <div class="pagination bottom">';
            $this->displayPagiantion();
            echo '  </div>';
            echo '</div>';
        }


        // Footer
        echo '<footer id="srv_footer" class="folders_footer">';
        $this->displayFooter();
        echo '</footer>';
    }

    // Naslov folderja na vrhu
    private function displayFolderTitle(){

        echo '<span class="faicon folder"></span>';
        echo $this->current_folder['naslov'];
    }

    // Izpisemo vrstico z gombi na vrhu (ustvari novo anketo, filtri...)
    private function displayButtonsRow(){
        global $global_user_id;

        // Gumb za ustvarjanje ankete	 
        if(DomainChange::check1kaDomainChange() && DomainChange::getInstance($global_user_id)->domainChangeLimits()){
            /*echo '<div class="create_buttons">';
            $this->new_anketa_div();
            echo '</div>';*/
        }
        else{
            echo '<div class="create_buttons">';
            $this->new_anketa_div();
            echo '</div>';
        }

        // Filtri na desni
        echo '<div class="filters">';

        // Okno za search po mojeih anketah
        echo '  <div id="searchMySurveys">';
        $this->displaySearch();	
        echo '  </div>';
        
        // Preklop na pogled s folderji (samo ce imamo nov vmesnik)
        //echo '<div id="folderSwitch">';
        $this->displayFolderSwitch();
        //echo '</div>';

        // Gumb za filtriranje
        //echo '<div id="filterButton">';
        $this->displayFilterButton();
        //echo '</div>';
    
        // Gumb za sortiranje
        echo '  <div id="sortButton">';
        $this->displaySortButton();
        echo '  </div>';
        
        echo '</div>';
    }

    // Prikazemo gumb za sortiranje seznama anket
	protected function displaySortButton(){
		global $lang, $site_url;
		
        echo '<span class="faicon sort"></span>';
				
		echo '<div id="sortSettings">';
		
		echo '<ul>';
		
		if($this->sorttype == 2){
			$sort = 1;
			$img_src = 'sort_ascending';
		} 
		else{
			$sort = 2;
			$img_src = 'sort_descending';
		}
		
		echo '<li '.($this->sortby == 1 ? ' class="active"' : '').'><a href="#" onClick="surveyList_goTo(\'1\',\''.($this->sortby != 1 ? '1' : $sort).'\')">'.$lang['sort_by_title'].' <span class="faicon '.($this->sortby != 1 ? 'sort_unsorted' : $img_src).'"></span></a></li>';
		echo '<li '.($this->sortby == 6 ? ' class="active"' : '').'><a href="#" onClick="surveyList_goTo(\'6\',\''.($this->sortby != 6 ? '1' : $sort).'\')">'.$lang['sort_by_qcount'].' <span class="faicon '.($this->sortby != 6 ? 'sort_unsorted' : $img_src).'"></span></a></li>';
		echo '<li '.($this->sortby == 5 ? ' class="active"' : '').'><a href="#" onClick="surveyList_goTo(\'5\',\''.($this->sortby != 5 ? '1' : $sort).'\')">'.$lang['sort_by_answercount'].' <span class="faicon '.($this->sortby != 5 ? 'sort_unsorted' : $img_src).'"></span></a></li>';
		echo '<li '.($this->sortby == 16 ? ' class="active"' : '').'><a href="#" onClick="surveyList_goTo(\'16\',\''.($this->sortby != 16 ? '1' : $sort).'\')">'.$lang['sort_by_insert'].' <span class="faicon '.($this->sortby != 16 ? 'sort_unsorted' : $img_src).'"></span></a></li>';
		echo '<li '.($this->sortby == 14 ? ' class="active"' : '').'><a href="#" onClick="surveyList_goTo(\'14\',\''.($this->sortby != 14 ? '1' : $sort).'\')">'.$lang['sort_by_edit'].' <span class="faicon '.($this->sortby != 14 ? 'sort_unsorted' : $img_src).'"></span></a></li>';
		echo '<li '.($this->sortby == 18 ? ' class="active"' : '').'><a href="#" onClick="surveyList_goTo(\'18\',\''.($this->sortby != 18 ? '1' : $sort).'\')">Status <span class="faicon '.($this->sortby != 18 ? 'sort_unsorted' : $img_src).'"></span></a></li>';
		echo '<li '.($this->sortby == 7 ? ' class="active"' : '').'><a href="#" onClick="surveyList_goTo(\'7\',\''.($this->sortby != 7 ? '1' : $sort).'\')"><li '.($this->sortby == 7 ? ' class="active"' : '').'>'.$lang['sort_by_author'].' <span class="faicon '.($this->sortby != 7 ? 'sort_unsorted' : $img_src).'"></span></a></li>';
		echo '<li '.($this->sortby == 11 ? ' class="active"' : '').' style="border:0;"><a href="#" onClick="surveyList_goTo(\'11\',\''.($this->sortby != 11 ? '1' : $sort).'\')">'.$lang['sort_by_editor'].' <span class="faicon '.($this->sortby != 11 ? 'sort_unsorted' : $img_src).'"></span></a></li>';
		
		echo '</ul>';
		
		echo '</div>';
	}
	
	// Prikazemo gumb za filtriranje seznama anket
	protected function displayFilterButton(){
		global $lang, $site_url, $admin_languages;
		
		echo '<div id="filterButton" '.(($this->user_id || $this->lang_id != 0 || $this->gdpr != 0) ? 'class="active"' : '').'>';
		
		echo '<span class="faicon filter"></span>';
		
		echo '<div id="filterSettings">';		
		echo '<ul>';
               
		# filter po uporabniku
		echo '<li>';
		
		echo '<span class="filter_title">'.$lang['srv_list_author'].'</span>';
		
		// Ce preklapljamo v searchu moramo refreshati celo stran (druga js funkcija)
		$reload = ($this->isSearch == 1) ? '_reload' : '';
		
		echo '<span class="filter_line"><input type="radio" name="filter_mySurveys" id="filter_mySurveys_0" value="0" '.(!$this->user_id ? 'checked="checked"' : '').' onclick="surveyList_user'.$reload.'(\'clr\',\'0\');"> <label for="filter_mySurveys_0">'.$lang['srv_list_all_surveys'].'</label></span>';
		echo '<span class="filter_line"><input type="radio" name="filter_mySurveys" id="filter_mySurveys_1" value="1" '.($this->user_id ? 'checked="checked"' : '').' onclick="surveyList_user'.$reload.'(\'uid\',\''.$this->g_uid.'\');"> <label for="filter_mySurveys_1">'.$lang['srv_list_my_surveys'].'</label></span>';
				
		echo '</li>';
        
        
		# filter po jeziku
		echo '<li>';	
		
		echo '<span class="filter_title">'.$lang['srv_sl_set_language'].'</span>';
		
		echo '<span class="filter_line"><input type="radio" name="filter_language" id="filter_language_0" value="0" '.((int)$this->lang_id == 0 ? 'checked="checked"' : '').' onclick="surveyList_language'.$reload.'(\'0\');"> <label for="filter_language_0">'.$lang['srv_sl_set_language_all'].'</label></span>';
		echo '<span class="filter_line"><input type="radio" name="filter_language" id="filter_language_1" value="1" '.((int)$this->lang_id == 1 ? 'checked="checked"' : '').' onclick="surveyList_language'.$reload.'(\'1\');"> <label for="filter_language_1">'.$admin_languages['1'].'</label></span>';
		echo '<span class="filter_line"><input type="radio" name="filter_language" id="filter_language_2" value="2" '.((int)$this->lang_id == 2 ? 'checked="checked"' : '').' onclick="surveyList_language'.$reload.'(\'2\');"> <label for="filter_language_2">'.$admin_languages['2'].'</label></span>';
		
        echo '</li>';	


        # filter po GDPR anketah
		echo '<li>';	
		
		echo '<span class="filter_title">'.$lang['srv_gdpr'].'</span>';
		
		echo '<span class="filter_line"><input type="radio" name="filter_gdpr" id="filter_gdpr_0" value="0" '.((int)$this->gdpr == 0 ? 'checked="checked"' : '').' onclick="surveyList_gdpr'.$reload.'(\'0\');"> <label for="filter_gdpr_0">'.$lang['srv_list_all_surveys'].'</label></span>';
		echo '<span class="filter_line"><input type="radio" name="filter_gdpr" id="filter_gdpr_1" value="1" '.((int)$this->gdpr == 1 ? 'checked="checked"' : '').' onclick="surveyList_gdpr'.$reload.'(\'1\');"> <label for="filter_gdpr_1">'.$lang['srv_list_gdpr_gdpr'].'</label></span>';
		
        echo '</li>';	
        
		
		echo '</ul>';			
		echo '</div>';
		
		echo '</div>';
	}

    // Izpisemo footer
    private function displayFooter(){
        global $lang;
        global $site_frontend;
        global $mysql_database_name;
        global $admin_type;


        // Leva stran footerja
        echo '<div class="footer_left">';
        
        // Custom footer
        if(AppSettings::getInstance()->getSetting('app_settings-footer_custom') !== false){
            echo '<div class="footer_line">';
            echo AppSettings::getInstance()->getSetting('app_settings-footer_text');
            echo '</div>';
        }
        // Default footer
        else{

            // First left line - links
            echo '<div class="footer_line">';

            echo $lang['srv_footer_links'];
            
            if(isAAI())
                echo '<span class="footer_item"><a href="https://www.1ka.si/d/sl/pomoc/pogosta-vprasanja/pogosta-vprasanja-o-arnes-aai-prijavi-uporabi-orodja-1ka" target="_blank">'.$lang['aa4'].'</a></span>';
            
            echo '</div>';


            // Second left line - version...
            echo '<div class="footer_line">';

            // Verzijo izpišemo samo za admine
            if ($admin_type == 0) {   
                
                // Verzija 1ka
                $sqlVersion = sisplet_query("SELECT value FROM misc WHERE what='version'", "obj");
                if (!empty($sqlVersion)) {
                    echo '<span class="footer_item">'.$lang['srv_footer_1ka_version'].': ' . $sqlVersion->value . '</span>';
                }

                // Verzija Drupal
                if ($site_frontend == 'drupal') {
                    $sqlDrupal = sisplet_query("SELECT value FROM misc WHERE what='drupal version'", "obj");
                    if (!empty($sqlDrupal)) {
                        echo '<span class="footer_item">Drupal: ' . $sqlDrupal->value . '</span>';
                    }
                }
            }
        
            echo '<span class="footer_item">Copyright (©) 2002-'.date('Y').' '.$lang['srv_footer_copyright'].'</span>';

            echo '</div>';
        }

        echo '</div>';


        // Desna stran footerja - report a bug
        echo '<div id="reportabug" class="footer_right">';
        
        // www.1ka.si ima se link na go instrukcije
        if($mysql_database_name == 'real1kasi' || $mysql_database_name == 'test1kasi' || $mysql_database_name == 'test21kasi'){

            echo '<a href="#" onClick="consultingPopupOpen();"><span class="faicon external_link"></span> '.$lang['srv_svetovanje'].'</a>';
            echo '<br>';
        }

        // Posebej report buga za gorenje
        if (Common::checkModule('gorenje')){
            echo '<a href="https://helpdesk.gorenje.com/SubmitSR.jsp" target="_blank"><span class="faicon inline_comment"></span> '.$lang['srv_footer_reportabug'].'</a>';
        }
        else{
            // Slovenski jezik
            if ($lang['id'] == 1){
                echo '<a href="https://www.1ka.si/a/72864?Q1=292032" target="_blank"><span class="faicon inline_comment"></span> '.$lang['srv_footer_reportabug'].'</a>';

                // Zacasen gumb na desni
                echo '<a href="https://www.1ka.si/a/72864?Q1=292032" target="_blank"><div class="right_help_button">'.$lang['srv_footer_reportabug2'].'</div></a>';
            }
            // Angleski jezik
            else{
                echo '<a href="https://www.1ka.si/a/72864?Q1=292032&language=2" target="_blank"><span class="faicon inline_comment"></span> '.$lang['srv_footer_reportabug'].'</a>';

                // Zacasen gumb na desni
                echo '<a href="https://www.1ka.si/a/72864?Q1=292032&language=2" target="_blank"><div class="right_help_button">'.$lang['srv_footer_reportabug2'].'</div></a>';
            }
        }

        echo '</div>';
    }

    // Izposemo tabelo z anketami
    public function displayNewSurveyList($folder=0) {
		global $lang;
		global $site_url;
		global $global_user_id;
				
        
        $ankete = $this->getSurveysAsListNew($folder);
                    
        echo '<input type="hidden" id="sortby" value="'.(isset($_POST['sortby']) ? $_POST['sortby'] : null).'">';
        echo '<input type="hidden" id="sorttype" value="'.(isset($_POST['sorttype']) ? $_POST['sorttype'] : null).'">';
        
        if ($count = count($ankete)) {
        
            // Naslov za search
            if($this->isSearch == 1){
                if($count == 1)
                    $hits = $lang['s_hits_1'];
                elseif($count == 2)
                    $hits = $lang['s_hits_2'];
                elseif($count == 3 || $count == 4)
                    $hits = $lang['s_hits_34'];
                else
                    $hits = $lang['s_hits'];
                
                // Ce imamo vec kot 1000 zadetkov izpisemo samo 1000 in opozorilo
                if($count < 1000)
                    $cnt_text = '<span class="italic">('.$count.' '.$hits.')</span>';
                else
                    $cnt_text = '<span class="italic">('.$lang['s_hits_1000'].')</span>';
                
                echo '<span class="search_title">'.$lang['s_search_mySurvey_title'].' '.$cnt_text.':</span>';
            }
        
            echo '<table id="surveyList_new">';
            
            
            // HEADER VRSTICA
            echo '<tr class="sl_header_new">';
                            
            // Naslov
            echo '<td class="col1">';
            echo $this->createOrderUrlNew(1, $lang['title']);		
            echo '</td>';
            
            // Trajanje - status
            echo '<td class="col2">';
            echo $this->createOrderUrlNew(18, 'Status');
            echo '</td>';
            
            // Ustvarjeno
            echo '<td class="col3">';
            echo $this->createOrderUrlNew(10, $lang['srv_created']);
            echo '</td>';
            
            // Zadnji vnos
            echo '<td class="col4">';
            echo $this->createOrderUrlNew(16, $lang['srv_last_insrt']);
            echo '</td>';
            
            // Sprememba
            echo '<td class="col5">';
            echo $this->createOrderUrlNew(14, $lang['change']);
            echo '</td>';
            
            // Ostalo
            echo '<td class="col6">';
            echo $lang['more2'];
            echo '</td>';

            echo '</tr>';
        
        
            // VRSTICE Z ANKETAMI
            foreach ( $ankete as $anketa ) {
                
                echo '<tr id="anketa_list_'.$anketa['id'].'" class="anketa_list '.($this->show_folders == 1 ? ' mySurvey_draggable' : '').'" anketa_id="'.$anketa['id'].'">';
            

                // Naslov
                echo '<td class="col1">';

                if(strlen($anketa['naslov']) > 60)
                    $text = substr($anketa['naslov'],0,60);
                else
                    $text = $anketa['naslov'];
                
                // Ce gre za search moramo ustrezno pobarvati najden del besede
                $text_searched = $text;
                if($this->isSearch == 1 && $this->searchSettings['stype'] == '0'){
                    foreach($this->searchStringProcessed as $search_word){
                        
                        // Pobarvamo najden niz v naslovu ankete
                        preg_match_all("/$search_word+/i", $text_searched, $matches);
                        if (is_array($matches[0]) && count($matches[0]) >= 1) {

                            // Drugace vcasih pobrise presledek, ne vem kako drugace resit:)
                            $text_searched = str_replace(' ', '&nbsp;', $text_searched);

                            $text_searched = str_replace($matches[0][0], '<span class="red">'.$matches[0][0].'</span>', $text_searched);
                        }
                    }					
                }

                // Zgornja vrstica
                echo '<div class="top_line">';
                echo '  <a href="'.$site_url.'admin/survey/index.php?anketa='.$anketa['id'].'&a='.A_REDIRECTLINK.'" title="'.$text.'">'.$text_searched.'</a>';
                echo '</div>';
                
                // Spodnja vrstica
                echo '<div class="bottom_line">';

                if($anketa['mobile_created'] == '1')
                    echo '<span class="red spaceRight pointer" title="'.$lang['srv_mobile_survey'].'">M</span>';
                
                if($anketa['survey_type'] == 0)
                    echo $lang['srv_vrsta_survey_type_0'];
                elseif($anketa['survey_type'] == 1)
                    echo $lang['srv_vrsta_survey_type_1'];
                elseif(SurveyInfo::checkSurveyModule('hierarhija', $anketa['id']))
                    echo $lang['srv_vrsta_survey_type_10'];
                else
                    echo $lang['srv_vrsta_survey_type_2'];

                echo ', '.$lang['srv_h_sl_stvprasanj'].': '. $anketa['variables'];	
                echo '</div>';

                echo '</td>';
                

                // Trajanje - status
                echo '<td class="col2">';
            
                if ($anketa['active'] == 1) {
                    $now = time();
                    $do = explode('.', $anketa['trajanjedo']);
                    $do = strtotime($do[0].'.'.$do[1].'.20'.$do[2]);
                    $trajanje = $do - $now;
                    $trajanje = floor($trajanje/60/60/24) + 1;
                    
                    if($trajanje >= 0){

                        // Ce je aktivna za vec kot 2000 dni je trajna
                        if($trajanje > 2000){
                            $status_text = $lang['srv_trajna_anketa'];
                        }
                        else{
                            $status_text = $lang['more'].' '.$trajanje.' '.$lang['hour_days'];
                        }
                    }
                    else{
                        $status_text = $lang['srv_trajna_anketa'];
                    }

                    $status_color = 'green';
                } 
                else {
                    $sqlA = sisplet_query("SELECT sid FROM srv_activity WHERE sid='".$anketa['id']."'");
                    
                    if (mysqli_num_rows($sqlA) > 0) {
                        # anketa je zaključena
                        $status_text = $lang['srv_survey_list_closed'];
                        $status_color = 'blue';
                    } 
                    else {
                        # anketa je neaktivna
                        $status_text = $lang['srv_survey_list_inpreparation'];
                        $status_color = 'gray';
                    }	
                }

                echo '  <div class="status_box '.$status_color.'">'.$status_text.'</div>';

                echo '</td>';
                

                // Ustvarjeno
                echo '<td class="col3">';

                // Ce gre za search moramo ustrezno pobarvati najden del besede
                $name_searched = $anketa['i_name'];
                $surname_searched = $anketa['i_surname'];
                if($this->isSearch == 1 && $this->searchSettings['stype'] == '1'){						
                    foreach($this->searchStringProcessed as $search_word){
    
                        // Pobarvamo najden niz v imenu
                        preg_match_all("/$search_word+/i", $name_searched, $matches);
                        if (is_array($matches[0]) && count($matches[0]) >= 1) {
                            foreach ($matches[0] as $match) {
                                $name_searched = str_replace($match, '<span class="red">'.$match.'</span>', $name_searched);
                            }
                        }
                        
                        // Pobarvamo najden niz v priimku
                        preg_match_all("/$search_word+/i", $surname_searched, $matches);
                        if (is_array($matches[0]) && count($matches[0]) >= 1) {
                            foreach ($matches[0] as $match) {
                                $surname_searched = str_replace($match, '<span class="red">'.$match.'</span>', $surname_searched);
                            }
                        }
                    }					
                }

                $i_time = substr($anketa['i_time'], 0, 8);
                $i_time = explode('.', $i_time);

                echo '<div class="top_line">';
                echo $i_time[0].'.'.$i_time[1].'.20'.$i_time[2];
                echo '</div>';
                
                echo '<div class="bottom_line">';
                echo $name_searched.' '.$surname_searched;
                echo '</div>';

                echo '</td>';
                

                // Zadnji vnos
                echo '<td class="col4">';

                echo '<div class="top_line">';
                if($anketa['answers'] > 0){
                    $v_time = substr($anketa['v_time_last'], 0, 8);
                    $v_time = explode('.', $v_time);

                    // Nimamo parapodatkov
                    if($anketa['v_time_last'] == '00.00.00 0:00')
                        echo $lang['srv_survey_list_no_paradata'];
                    else
                        echo $v_time[0].'.'.$v_time[1].'.20'.$v_time[2];
                }
                else{
                    echo '/';
                }
                echo '</div>';

                echo '<div class="bottom_line">';
                echo $lang['srv_info_answers_no'].': '.$anketa['answers'];
                echo '</div>';

                echo '</td>';
                

                // Sprememba
                echo '<td class="col5">';

                echo '<div class="top_line">';
                $e_time = substr($anketa['e_time'], 0, 8);
                $e_time = explode('.', $e_time);
                echo $e_time[0].'.'.$e_time[1].'.20'.$e_time[2];
                echo '</div>';

                echo '<div class="bottom_line">';
                echo '  <span id="edit_user_'.$anketa['id'].'" euid="'.$anketa['edit_uid'].'" onclick="surveyList_user(\'e\',\'#edit_user_'.$anketa['id'].'\');">'.$anketa['e_email'].'</span>';
                echo '</div>';

                echo '</td>';
                

                // Ostalo
                echo '<td class="col6 other_link">';
                
                echo '  <span class="faicon dots" onClick="surveyList_show_more(\''.$anketa['id'].'\');"></span>';
                
                // Hover okno
                echo '  <div class="survey_other_box_holder">';
                echo '      <div id="survey_other_box_'.$anketa['id'].'" class="survey_other_box displayNone"><ul>';

                SurveyInfo::getInstance()->SurveyInit($anketa['id']);
                echo '			<li><a href="' . SurveyInfo::getSurveyLink() . '?preview=on" target="_blank" title="'.$lang['srv_poglejanketo2'].'"><span class="faicon monitor"></span> '.$lang['srv_poglejanketo2'].'</a></li>';
                echo '          <li><a href="index.php?anketa='.$anketa['id'].'&a=testiranje"><span class="faicon test"></span>'.$lang['srv_testiranje'].'</a></li>';
                echo '          <li><a href="index.php?anketa='.$anketa['id'].'&a=vabila"><span class="faicon publish"></span>'.$lang['srv_vabila'].'</a></li>';
                echo '          <li><a href="index.php?anketa='.$anketa['id'].'&a=data"><span class="faicon data"></span>'.$lang['srv_podatki'].'</a></li>';
                echo '          <li><a href="index.php?anketa='.$anketa['id'].'&a=analysis"><span class="faicon chart"></span>'.$lang['srv_analiza'].'</a></li>';

                echo '          <li><div class="line"></li>';

                echo '          <li><input type="checkbox" id="lib_private_'.$anketa['id'].'" '.($anketa['lib_usr'] == 1 ? 'checked="checked"' : '').' onclick="surveyList_myknjiznica_new(\''.$anketa['id'].'\'); return false;"><label for="lib_private_'.$anketa['id'].'">'.$lang['srv_moja_knjiznica'].'</label></li>';
                if ($this->g_adminType == 0){
                    echo '          <li><input type="checkbox" id="lib_public_'.$anketa['id'].'" '.($anketa['lib_glb'] == 1 ? 'checked="checked"' : '').' onclick="surveyList_knjiznica_new(\''.$anketa['id'].'\'); return false;"><label for="lib_public_'.$anketa['id'].'">'.$lang['srv_javna_knjiznica'].'</label></li>';
                }
                
                echo '      </ul></div>';
                echo '  </div>';

                echo '</td>';

                
                echo '</tr>';
            }
        
            echo '</table>';	
        }
        else{
            // Naslov za prazen search
            if($this->isSearch == 1)
                echo '<span class="search_title">'.$lang['s_search_mySurvey_nothing'].'.</span>';
        }			
	}


    // Prestejemo ankete po folderjih
    private function countSurveysInFolders(){
        global $global_user_id;
        global $global_admin_type;

        $query_string = "SELECT a.id AS ank_id, ma.folder AS folder_id
                            FROM srv_anketa a
                            LEFT OUTER JOIN srv_mysurvey_anketa ma
                            ON a.id = ma.ank_id AND ma.usr_id='".$global_user_id."'
                            WHERE a.backup='0' AND a.id>0 AND active>='0' AND a.invisible='0'";

        // Filter za dostop - samo avtor
        if ($this->user_id){
            $query_string .= " AND a.insert_uid='".$global_user_id."'";
        }
        // Filter za dostop - vse anekte z dostopom
        else{
            // posebej za managerje, ki vidijo ankete svojih uporabnikov
			$manage = '';
            if ($global_admin_type == 1 || $global_admin_type == 0) 
                $manage = " OR uid IN (".SurveyCopy::prepareSubquery(sisplet_query("SELECT user FROM srv_dostop_manage WHERE manager='".$global_user_id."' ")).") ";
            
            $query_string .= " AND (a.id IN (".SurveyCopy::prepareSubquery(sisplet_query("SELECT ank_id FROM srv_dostop WHERE uid='".$global_user_id."' $manage"))."))";
        }

        // GDPR filter
        if($this->gdpr == 1){
            $query_string .= " AND EXISTS (SELECT * FROM srv_gdpr_anketa sgdpr WHERE sgdpr.ank_id=a.id) ";
        }

        // Filter za jezik
        if ((int)$this->lang_id > 0) {
            $query_string .= " AND a.lang_admin='".(int)$this->lang_id."'";
        }

        
        $sql = sisplet_query($query_string);
        while($row = mysqli_fetch_array($sql)){
            $folder_id = ($row['folder_id'] != null) ? $row['folder_id'] : 0;

            if(!isset($this->surveys_count[$folder_id]))
                $this->surveys_count[$folder_id] = 1;
            else
                $this->surveys_count[$folder_id]++;
        }
    }

    // Dobimo seznam anket za nov prikaz (starega se naceloma ne uporablja vec)
	private function getSurveysAsListNew() {

		$result = array();

		// ce imas hkrati dostop do ankete (srv_dostop) in preko managerskega dostopa (srv_dostop_manage) se brez DISTINCT podvajajo ankete
		$stringSurveyList = "SELECT DISTINCT sa.id, sa.folder, '1' as del, sa.naslov, sa.active, sa.mobile_created, sa.edit_time, ";
		$stringSurveyList .= 'sal.lib_glb AS lib_glb, ';
		$stringSurveyList .= 'sal.lib_usr AS lib_usr, ';
		$stringSurveyList .= "sa.edit_uid, sal.e_name AS e_name, sal.e_surname AS e_surname, sal.e_email AS e_email, ";
		$stringSurveyList .= "sa.insert_uid, sal.i_name AS i_name, sal.i_surname AS i_surname, sal.i_email AS i_email, ";
		$stringSurveyList .= "date_format(edit_time, '%d.%m.%y %k:%i') AS e_time, ";
		$stringSurveyList .= "date_format(insert_time, '%d.%m.%y %k:%i') AS i_time, ";
		$stringSurveyList .= "date_format(sal.a_first, '%d.%m.%y %k:%i') AS v_time_first, date_format(sal.a_last, '%d.%m.%y %k:%i') AS v_time_last, ";

		$stringSurveyList .= "sal.answers as answers, "; // vedno prestejemo odgovore
		$stringSurveyList .= "sal.variables as variables, ";
		$stringSurveyList .= "date_format(sa.starts, '%d.%m.%y') as trajanjeod, ";
		$stringSurveyList .= "date_format(sa.expire, '%d.%m.%y') as trajanjedo, ";
		$stringSurveyList .= "approp, ";
		$stringSurveyList .= "sd.canEdit, sa.survey_type "; // tega pustim tukaj, da ni problemov z vejico

        
		// Ce searchamo po besedah dodamo se uvod, zakljucek, naslove vprasanj in vrednosti vprasanj
		if($this->isSearch == 1 && $this->searchSettings['stype'] == '2'){
			$stringSurveyList .= ", sa.introduction AS introduction, sa.conclusion AS conclusion ";
			
			$stringSurveyList .= ", sg.id AS sg_id, sg.ank_id AS sg_ank_id ";
			$stringSurveyList .= ", ss.id AS ss_id, ss.naslov AS ss_naslov, ss.gru_id AS ss_gru_id ";
			$stringSurveyList .= ", sv.id AS sv_id, sv.naslov AS sv_naslov, sv.spr_id AS sv_spr_id ";
		}
        
    
		$stringSurveyList .= "FROM srv_anketa sa ";
		$stringSurveyList .= "LEFT OUTER JOIN srv_survey_list AS sal ON sal.id = sa.id " ;
		$stringSurveyList .= "LEFT OUTER JOIN srv_library_anketa AS sla ON sla.ank_id = sa.id " ;
		
		# kdo lahko ureja anketo (briše)
		// tega substringy se ne da dodatno razbit z prepareSubquery, ker selectamo 2 elementa...
		$stringSurveyList .= "LEFT OUTER JOIN (SELECT 1 AS canEdit, ank_id FROM srv_dostop WHERE FIND_IN_SET('edit', dostop ) ='1' AND aktiven = '1' AND uid = '$this->g_uid' OR uid IN (".SurveyCopy::prepareSubquery(sisplet_query("SELECT user FROM srv_dostop_manage WHERE manager = '".$this->g_uid."'")).")) AS sd ON sd.ank_id = sa.id " ;


		if($this->current_folder['id'] > 0)
			$stringSurveyList .= "LEFT OUTER JOIN srv_mysurvey_anketa AS sf ON sf.ank_id = sa.id ";

		// Ce iscemo po kljucnih besedah moramo dodat se tabele srv_grupa, srv_spremenljivka in srv_vrednost
		if($this->isSearch == 1 && $this->searchSettings['stype'] == '2'){
			$stringSurveyList .= "LEFT OUTER JOIN srv_grupa AS sg ON sg.ank_id = sa.id ";
			$stringSurveyList .= "LEFT OUTER JOIN srv_spremenljivka AS ss ON ss.gru_id = sg.id ";
			$stringSurveyList .= "LEFT OUTER JOIN srv_vrednost AS sv ON sv.spr_id = ss.id ";
		}

        // Ce iscemo po imenu ankete moramo dodat se tabele srv_nice_links za lepe linke
		if($this->isSearch == 1 && $this->searchSettings['stype'] == '0'){
			$stringSurveyList .= "LEFT OUTER JOIN srv_nice_links AS nl ON sa.id = nl.ank_id ";
		}
		

		$stringSurveyList .= "WHERE sa.backup='0' AND sa.id>0 AND active >= '0' AND invisible = '0' ";
		
		if($this->current_folder['id'] > 0)
			$stringSurveyList .= "AND sf.usr_id='$this->g_uid' AND sf.folder='".$this->current_folder['id']."' ";
		elseif($this->current_folder['id'] == 0)
			$stringSurveyList .= "AND NOT EXISTS (SELECT * FROM srv_mysurvey_anketa sma WHERE sma.ank_id=sa.id AND sma.usr_id='$this->g_uid') ";
    
            
        // GDPR filter
        if($this->gdpr == 1)
            $stringSurveyList .= "AND EXISTS (SELECT * FROM srv_gdpr_anketa sgdpr WHERE sgdpr.ank_id=sa.id) ";
		elseif($this->gdpr == 2)
			$stringSurveyList .= "AND NOT EXISTS (SELECT * FROM srv_gdpr_anketa sgdpr WHERE sgdpr.ank_id=sa.id) ";

            
		// Ce izvajamo search po anketah
		if($this->isSearch == 1){
			// Filter glede na search
			$stringSurveyList .= $this->getSearchString();

			// Filter glede na jezik ankete
			$stringSurveyList .= $this->getLanguageLimit();
			// Filter glede na dostop do ankete
			$stringSurveyList .= $this->getDostopAnketa();
			// Vrstni red anket
			$stringSurveyList .= $this->getOrderString();	

			// Ce iscemo po kljucnih besedah moramo na koncu grupirat po anketi
			if($this->searchSettings['stype'] == '2'){
				//$stringSurveyList .= " GROUP BY id";
				//$stringSurveyList .= " LIMIT 1000";
			}
			else{
				// Limit anket
				//$stringSurveyList .= $this->getLimitString();
				$stringSurveyList .= " LIMIT 1000";
			}
		}
		else{
			// Filter glede na jezik ankete
			$stringSurveyList .= $this->getLanguageLimit();
			// Filter glede na dostop do ankete
			$stringSurveyList .= $this->getDostopAnketa();
			// Vrstni red anket
			$stringSurveyList .= $this->getOrderString();	
			// Limit anket
			$stringSurveyList .= $this->getLimitString();	
		}

		
		$sqlSurveyList = sisplet_query($stringSurveyList);
		if (!$sqlSurveyList) {
			print_r("ERROR in query:");
			print_r($stringSurveyList);
			echo mysqli_error($GLOBALS['connect_db']);
		}
		
		while ($rowSurveyList = mysqli_fetch_assoc($sqlSurveyList)) {
			$result[$rowSurveyList['id']] = $rowSurveyList;		
		}

		return $result;
	}

	
	/** Funkcija ki kiče podfunkcije za ajax del 
     * 
	 * 
	 */ 
	public function ajax() {
        global $global_user_id;
        global $lang;


        // Aktiviramo folder in prikazemo ankete v tem folderju
		if($_GET['a'] == 'mysurvey_folder_activate'){

			$folder_id = isset($_POST['folder_id']) ? $_POST['folder_id'] : '0';


            // Nastavimo vse folderje na neaktivne
            $sql = sisplet_query("UPDATE srv_mysurvey_folder SET active='0' WHERE usr_id='".$global_user_id."'");
            
            // Nastavimo izbran folder na aktivnega
            if($folder_id > '0')
                $sql = sisplet_query("UPDATE srv_mysurvey_folder SET active='1' WHERE usr_id='".$global_user_id."' AND id='".$folder_id."'");

            // Prestejemo ankete po folderjih
            $this->countSurveysInFolders();

            // Nastavimo current folder
            if($folder_id == '0'){   
                    
                $this->current_folder = array(
                    'id'        => 0,
                    'naslov'    => $lang['srv_mySurvey_all_surveys']
                );
            }
            else{
                // Dobimo podatke folderja
                $sql = sisplet_query("SELECT * FROM srv_mysurvey_folder WHERE id='".$folder_id."'");
                if (!$sql)
                    echo mysqli_error($GLOBALS['connect_db']);

                $row = mysqli_fetch_array($sql);

                $this->current_folder = $row;
            }


            // Na novo prikazemo desno stran
            $this->displayRightContent();

            // JS za drag/drop
            echo '<script type="text/javascript">mySurvey_folder_init();</script>';
		}

        // Razsirimo/skrcimo folder
		if($_GET['a'] == 'mysurvey_folder_toggle'){

			$folder_id = isset($_POST['folder_id']) ? $_POST['folder_id'] : '0';
			$open = isset($_POST['open']) ? $_POST['open'] : '0';

            // Nastavimo izbran folder na odprtega
            $sql = sisplet_query("UPDATE srv_mysurvey_folder SET open='".$open."' WHERE usr_id='".$global_user_id."' AND id='".$folder_id."'");
		}


        // Pokazemo popup za poimenovanje novega subfolderja
		if($_GET['a'] == 'mysurvey_folder_add_popup'){
               
            $folder_id = $_POST['folder_id'];

            $this->displayAddFolderPopup($folder_id);
        }
        
        // Ustvarimo nov subfolder
		if($_GET['a'] == 'mysurvey_folder_add'){

            $folder_id = $_POST['folder_id'];
            $folder_name = $_POST['folder_name'];
			
			// Razpremo parent folder
			$sql = sisplet_query("UPDATE srv_mysurvey_folder SET open='1' WHERE id='".$folder_id."' AND usr_id='$global_user_id'");
			
			$sql = sisplet_query("INSERT INTO srv_mysurvey_folder (usr_id, parent, naslov) VALUES ('".$global_user_id."','".$folder_id."', '".$folder_name."')");

            $new_folder_id = mysqli_insert_id($GLOBALS['connect_db']);
           
            $this->displayLeftContent();

            // JS za drag/drop
            echo '<script type="text/javascript">mySurvey_folder_init();</script>';
        }

        // Pokazemo popup za preimenovanje folderja
		if($_GET['a'] == 'mysurvey_folder_rename_popup'){

            $folder_id = $_POST['folder_id'];
            $folder_name = $_POST['folder_name'];

            $this->displayRenameFolderPopup($folder_id, $folder_name);
        }

        // Preimenujemo folder
		if($_GET['a'] == 'mysurvey_folder_rename'){

            $folder_id = isset($_POST['folder_id']) ? $_POST['folder_id'] : '0';
			$folder_name = isset($_POST['folder_name']) ? $_POST['folder_name'] : '';
			$folder_name = strip_tags($folder_name);

            $sql = sisplet_query("UPDATE srv_mysurvey_folder SET naslov='".$folder_name."' WHERE id='".$folder_id."' AND usr_id='".$global_user_id."'");

            $this->displayLeftContent();

            // JS za drag/drop
            echo '<script type="text/javascript">mySurvey_folder_init();</script>';
        }

        // Pobrisemo folder
		if($_GET['a'] == 'mysurvey_folder_delete'){

            $folder_id = isset($_POST['folder_id']) ? $_POST['folder_id'] : 0;

            if($folder_id > 0){

                // Dobimo parent folderja
                $sqlParent = sisplet_query("SELECT parent, active FROM srv_mysurvey_folder WHERE id='".$folder_id."' AND usr_id='".$global_user_id."'");
                $rowParent = mysqli_fetch_array($sqlParent);

                // Ce je parent root, pobrisemo ankete ki so bile znotraj folderja
                if($rowParent['parent'] == '0'){
                    $sql = sisplet_query("DELETE FROM srv_mysurvey_anketa WHERE folder='".$folder_id."' AND usr_id='".$global_user_id."'");
                }
                // Drugace jih prestavimo v parenta
                else{
                    $sql = sisplet_query("UPDATE srv_mysurvey_anketa SET folder='".$rowParent['parent']."' WHERE folder='".$folder_id."' AND usr_id='".$global_user_id."'");

                    // Ce je bil active, nastavimo parenta za active
                    if($rowParent['active'] == '1'){
                        $sql2 = sisplet_query("UPDATE srv_mysurvey_folder SET active='1' WHERE id='".$rowParent['parent']."' AND usr_id='".$global_user_id."'");
                    }
                }  

                // Nastavimo subfolderjem novega parenta
                $sql3 = sisplet_query("UPDATE srv_mysurvey_folder SET parent='".$rowParent['parent']."' WHERE parent='".$folder_id."' AND usr_id='".$global_user_id."'");
                       
                // Na koncu se pobrisemo prazen folder
                $sql4 = sisplet_query("DELETE FROM srv_mysurvey_folder WHERE id='".$folder_id."' AND usr_id='".$global_user_id."'");
            }

            $this->getSurveys();
		}


        // Drop folderja
		if($_GET['a'] == 'mysurvey_folder_drop'){

            $drag_folder_id = isset($_POST['drag_folder_id']) ? $_POST['drag_folder_id'] : 0;
            $parent_folder_id = isset($_POST['parent_folder_id']) ? $_POST['parent_folder_id'] : 0;
            
            if($drag_folder_id > 0){

                // Preverimo, ce nismo droppali folder v svojega childa - tega nikoli ne pustimo, ker potem vse izgine
                if(!$this->checkFolderIsChild($parent_folder_id, $drag_folder_id)){

                    // Nastavimo folderju novega parenta
                    $sql = sisplet_query("UPDATE srv_mysurvey_folder SET parent='".$parent_folder_id."' WHERE id='".$drag_folder_id."' AND usr_id='".$global_user_id."'");
                
                    // Parenta razpremo
                    $sql = sisplet_query("UPDATE srv_mysurvey_folder SET open='1' WHERE id='".$parent_folder_id."' AND usr_id='".$global_user_id."'");
                }
            }

            $this->getSurveys();
		}

        // Drop ankete
		if($_GET['a'] == 'mysurvey_survey_drop'){

            $drag_survey_id = isset($_POST['drag_survey_id']) ? $_POST['drag_survey_id'] : 0;
            $parent_folder_id = isset($_POST['parent_folder_id']) ? $_POST['parent_folder_id'] : 0;

            if($drag_survey_id > 0){

                // Ce prestavimo v root, samo pobrisemo anketo iz srv_mysurvey_anketa
                if($parent_folder_id == '0'){
                    $sql = sisplet_query("DELETE FROM srv_mysurvey_anketa WHERE ank_id='".$drag_survey_id."' AND usr_id='".$global_user_id."'");
                }
                // Prestavimo anketo v subfolder
                else{
                    $sql = sisplet_query("INSERT INTO srv_mysurvey_anketa 
                                        (ank_id, usr_id, folder) 
                                        VALUES 
                                        ('".$drag_survey_id."', '".$global_user_id."', '".$parent_folder_id."') 
                                        ON DUPLICATE KEY UPDATE 
                                            folder='".$parent_folder_id."'");
                }
            }

            $this->getSurveys();
		}
    }

    // Rekurzivno preverimo, ce je nek folder child drugega folderja
    private function checkFolderIsChild($folder_id, $parent_folder_id){

        // Ce je parent 0, je root in koncamo - folder ni child
        if($folder_id == '0')
            return false;

        $sql = sisplet_query("SELECT parent FROM srv_mysurvey_folder WHERE id='".$folder_id."'");
        $row = mysqli_fetch_array($sql);

        if($row['parent'] == $parent_folder_id){
            return true;
        }
        else{
            return $this->checkFolderIsChild($row['parent'], $parent_folder_id);
        }
    }



}
?>