<?php

/**
* Ta class se vedno kliče iz SurveyAdmin
* 
* Vsebuje naj vse nastavitve v anketi in take zadeve, ki se ne uporabljajo pogosto (in se ne kličejo preko ajaxa) da ne smetijo v SurveyAdmin
* 
* Zaenkrat je še celotna kopija SurveyAdmin, treba je še pobrisat odvečne funkcije
* 
* @var mixed
*/


global $site_path;

if(session_id() == '') {
    session_start();
}

class SurveyAdminSettings {

	var $anketa; // trenutna anketa
	var $grupa; // trenutna grupa
	var $spremenljivka; // trenutna spremenljivka
	var $branching = 0; // pove, ce smo v branchingu
	var $stran;
	var $podstran;
	var $skin = 0;
	var $survey_type; // privzet tip je anketa na vecih straneh

	var $displayLinkIcons = false; // zaradi nenehnih sprememb je trenutno na false, se kasneje lahko doda v nastavitve
	var $displayLinkText = true; // zaradi nenehnih sprememb je trenutno na true, se kasneje lahko doda v nastavitve
	var $setting = null;

	var $db_table = '';
		
	var $icons_always_on = false;	# ali ima uporabnik nastavljeno da so ikone vedno vidne
	var $full_screen_edit = false;	# ali ima uporabnik nastavljeno da ureja vprašanja v fullscreen načinu

	/**
	 * @desc konstruktor
	 */
	function __construct($action = 0, $anketa = 0) {
		global $surveySkin, $site_url, $global_user_id;
		
		if (isset ($surveySkin))
			$this->skin = $surveySkin;
		else
			$this->skin = 0;
			
		if ((isset ($_REQUEST['anketa']) && $_REQUEST['anketa'] > 0) || (isset ($anketa) && $anketa > 0)) {
			$this->anketa = (isset ($anketa) && $anketa > 0) ? $anketa : $_REQUEST['anketa'];
		} 
        else {
			// nekje se uporablja tudi brez IDja ankete!!!
			//die("SAS: SID missing!");
		}

		SurveyInfo::getInstance()->SurveyInit($this->anketa);
		SurveyInfo::getInstance()->resetSurveyData();
		
		$this->db_table = SurveyInfo::getInstance()->getSurveyArchiveDBString();

		$this->survey_type = $this->getSurvey_type($this->anketa);

		$this->stran = isset($_GET['a']) ? $_GET['a'] : null;

	}

	/*Globalne nastavitve
	 * 		Osnovni podatki
	 * 		Respondenti
	 * 		Design
	 * 		Obveščanje
	 * 		Piškotek
	 * 		Trajanje
	 * 		Komentarji
	 * 		Dostop
	 */

	function anketa_nastavitve_global() {//OSTANE
		global $lang;
		global $site_url;
		global $site_path;
		global $admin_type;
		global $global_user_id;

		/* Globalne nastavitve ankete: veljajo za celoto anketo ne glede na uporabnika*/
		$row = SurveyInfo::getInstance()->getSurveyRow();

		$preklici_url = ltrim(str_replace("&s=1","",$_SERVER['REQUEST_URI']),"/");
		$preklici_url = "'". $site_url . $preklici_url . "'";

		echo '<form name="settingsanketa_' . $row['id'] . '" action="ajax.php?a=editanketasettings" method="post" autocomplete="off">' . "\n\r";
		echo '	<input type="hidden" name="anketa" value="' . $this->anketa . '" />' . "\n\r";
		echo '	<input type="hidden" name="grupa" value="' . $this->grupa . '" />' . "\n\r";
		echo '  <input type="hidden" name="location" value="' . $_GET['a'] . '" />' . "\n\r";
		echo '  <input type="hidden" name="submited" value="1" />' . "\n\r";
		/*Osnovni podatki*/

		if ($_GET['a'] == 'osn_pod' || $_GET['a'] == 'nastavitve') {

			/* OSNOVNI PODATKI */
			echo '<fieldset>';
			echo '<legend>' . $lang['srv_osnovniPodatki'] . '</legend>';

			echo '<div class="setting_holder">';
			echo '<label for="anketa_polnoIme">'.$lang['srv_novaanketa_polnoime'].':</label>';
			echo '  <input type="text" class="large" id="anketa_polnoIme" name="naslov" value="' . $row['naslov'] . '" />';
			echo '  <span class="charalimit" id="anketa_polnoIme_chars">' . strlen($row['naslov']) . '/'.ANKETA_NASLOV_MAXLENGTH.'</span>';
			echo '</div>';

			echo '<div class="setting_holder">';
			echo '  <label for="anketa_akronim">'.$lang['srv_novaanketa_kratkoime'].':</label>';
			echo '  <input type="text" class="large" id="anketa_akronim" name="akronim" value="' . $row['akronim'] . '" />';
			echo '  <span class="charalimit" id="anketa_akronim_chars">' . strlen($row['akronim']) . '/'.ANKETA_AKRONIM_MAXLENGTH.'</span>';
			echo '</div>';

			echo '<div class="setting_holder">';
			echo '<input type="hidden" name="opomba" value="0">';
            echo '<input type="checkbox" id="opomba" name="opomba" value="1" '.(isset($row['intro_opomba']) && $row['intro_opomba'] != ""  ? ' checked="checked"' : '').' onChange="toggleAnketaOpomba(); return false;">';
            echo '<label for="opomba">'.$lang['srv_dodaj_opombo'].'</label> ';
			echo '</div>';

			echo '<div id="dodaj-opombo" class="setting_holder '.(isset($row['intro_opomba']) && $row['intro_opomba'] != ""  ? '' : 'displayNone').'">';
			echo '  <label for="anketa_note">'.$lang['srv_note'].':</label>';
			echo '  <textarea id="anketa_note" name="intro_opomba" maxlength="'.ANKETA_NOTE_MAXLENGTH.'">' . $row['intro_opomba'] . '</textarea>';
			echo '  <span class="charalimit" id="anketa_note_chars";">' . strlen($row['intro_opomba']) . '/'.ANKETA_NOTE_MAXLENGTH.'</span>';
			echo '</div>';

			// Ce ima uporabnik mape, lahko izbere v katero mapo se anketa uvrsti
			UserSetting::getInstance()->Init($global_user_id);
			$show_folders = UserSetting::getInstance()->getUserSetting('survey_list_folders');
			
			$selected_folder = 0;
			$sqlFA = sisplet_query("SELECT folder FROM srv_mysurvey_anketa WHERE usr_id='".$global_user_id."' AND ank_id='".$this->anketa."'");
			if(mysqli_num_rows($sqlFA) > 0){
				$rowFA = mysqli_fetch_array($sqlFA);
				$selected_folder = $rowFA['folder'];
			}
			
			$sqlF = sisplet_query("SELECT id, naslov FROM srv_mysurvey_folder WHERE usr_id='".$global_user_id."' ORDER BY naslov ASC");	
			if($show_folders == 1 && mysqli_num_rows($sqlF) > 0){

				echo '<div id="anketa-toggle" class="setting_holder '.($selected_folder != 0 ? '' : 'bottom0').'">';
				echo '<input type="hidden" name="mapa" value="0">';
				echo '<input type="checkbox" id="mapa" name="mapa" value="1" '.($selected_folder != 0 ? ' checked="checked"' : '').' onChange="toggleAnketaMapa(); return false;">';
				echo '<label for="mapa">'.$lang['srv_newSurvey_survey_new_folder'].'</label> ';
				echo '</div>';

				echo '<div id="dodaj-v-mapo"  class="setting_holder '.($selected_folder != 0 ? '' : 'displayNone').'">';
				echo '<label for="anketa_folder">'. $lang['srv_newSurvey_survey_new_folder'] .Help::display('srv_nova_shrani').':</label>';
				echo '<select class="dropdown large w400" name="anketa_folder" id="anketa_folder">';
				echo '<option value="0" '.($selected_folder == 0 ? ' selected="selected"' : '').'>'.$lang['srv_newSurvey_survey_new_folder_def'].'</option>';
				while($rowF = mysqli_fetch_array($sqlF)){
					echo '<option value="'.$rowF['id'].'" '.($rowF['id'] == $selected_folder ? ' selected="selected"' : '').'>'.$rowF['naslov'].'</option>';
				}
				echo '</select>';

				echo '</div>';
			}
			
			echo '</fieldset>';
					
			/* JEZIK */
			echo '<fieldset>';
			echo '<legend>' . $lang['lang'] . Help::display('srv_nastavitve_jezik').'</legend>';
			
			$lang_admin = $row['lang_admin'];
			$lang_resp = $row['lang_resp'];
			$lang_array = array();
			// Preberemo razpoložljive jezikovne datoteke
			if ($dir = opendir('../../lang')) {
				while (($file = readdir($dir)) !== false) {
					if ($file != '.' AND $file != '..') {
						if (is_numeric(substr($file, 0, strpos($file, '.')))) {
							$i = substr($file, 0, strpos($file, '.'));
							if ($i > 0) {
								$file = '../../lang/'.$i.'.php';
								@include($file);
								$lang_array[$i] = $lang['language'];
							}
						}
					}
				}
			}
			
			// nastavimo jezik nazaj
			if ($lang_admin > 0) {
				$file = '../../lang/'.$lang_admin.'.php';
				@include($file);
			}

			echo '<div class="setting_holder">';
			echo '<span class="setting_title">'.$lang['srv_language_admin_survey'].':</span>';

			ksort($lang_array);
			foreach ($lang_array AS $key => $val) {
				if ($key == 1 || $key == 2) {
					echo '<div class="setting_item">';	
					echo '<input type="radio" value="'.$key.'" id="lll_'.$key.'" '.($key==$lang_admin?' checked':'').' name="lang_admin">';
					echo '<label for="lll_'.$key.'">'.$val.'</label>';
					echo '</div>';
				} 
			}
			
			echo '</div>';

			echo '<div class="setting_horizontal_wrapper">';
			echo '<div class="setting_holder">';
			echo '<label for="lang_resp">'.$lang['srv_language_respons_1'].':</label>';

			
			asort($lang_array);
			echo '<select class="dropdown large" id="lang_resp" name="lang_resp">';
			foreach ($lang_array AS $key => $val) {
				echo '<option value="'.$key.'" '.($key==$lang_resp?' selected':'').'>'.$val.'</option>'; 
			}
			echo '</select>';
			echo '</div>';


			// Preverimo, ce je funkcionalnost v paketu, ki ga ima uporabnik
            $userAccess = UserAccess::getInstance($global_user_id);
            if($userAccess->checkUserAccess($what='prevajanje')){

				$lang_link = 'window.location.href=\'index.php?anketa='.$this->anketa.'&a=prevajanje\'; return false;';
				$color = 'white-black';
            }
            else{  
				$lang_link = 'popupUserAccess(\'prevajanje\'); return false;';
				$color = 'yellow user_access_locked';          
            }	

			echo '<div class="button_holder top0 bottom0">';
			echo '<button class="medium '.$color.'" onClick="'.$lang_link.'">'.$lang['srv_multi_lang'].'</button>';				
			echo '</div>';


			echo '</div>';
            
			echo '</fieldset>';

			// Gumb shrani - vmes
			echo '<div class="button_holder">';
			echo '<button class="medium white-blue" onClick="window.location='.$preklici_url.';return false;">'.$lang['edit1338'].'</button>';				
			echo '<button class="medium blue" onclick="document.settingsanketa_' . $row['id'] . '.submit(); return false;">'.$lang['edit1337'].'</button>';
			echo '</div>';
			
					
			/* INTERAKTIVNI ELEMENTI */
			echo '<fieldset>';
			echo '<legend>'.$lang['srv_interaktivni_elementi'].'</legend>';
			
			SurveySetting::getInstance()->Init($this->anketa);
			$survey_privacy = SurveySetting::getInstance()->getSurveyMiscSetting('survey_privacy');
			$survey_hint = SurveySetting::getInstance()->getSurveyMiscSetting('survey_hint'); if ($survey_hint == '') $survey_hint = 1;
			$survey_hide_title = SurveySetting::getInstance()->getSurveyMiscSetting('survey_hide_title');			
			$survey_track_reminders = SurveySetting::getInstance()->getSurveyMiscSetting('survey_track_reminders'); if ($survey_track_reminders == '') $survey_track_reminders = 0;
			$display_backlink = SurveySetting::getInstance()->getSurveyMiscSetting('display_backlink');

			$multiple_pages = false;
			$sqlg = sisplet_query("SELECT id FROM srv_grupa WHERE ank_id = '$this->anketa'");
			if (mysqli_num_rows($sqlg) > 1)
				$multiple_pages = true;

				
			// Indikator napredka
			echo '<div class="setting_holder">';
			echo '<input type="hidden" name="progressbar" value="0">';
            echo '<input type="checkbox" id="progressbar" name="progressbar" value="1" '.($row['progressbar'] == 1 && $multiple_pages ? ' checked="checked"' : '').' '.(!$multiple_pages ? 'disabled' : '').'>';
            echo '<label for="progressbar">'.$lang['srv_te_progressbar'].' '.Help::display('srv_show_progressbar').'</label> ';
			echo '</div>';
			
			// Naslov ankete za uporabnike
			echo '<div class="setting_holder">';
			echo '<input type="hidden" name="survey_hide_title" value="1">';
            echo '<input type="checkbox" id="survey_hide_title" name="survey_hide_title" value="0" '.($survey_hide_title == 0 ? ' checked="checked"' : '').'>';
            echo '<label for="survey_hide_title">'.$lang['srv_show_title'].'</label> ';
			echo '</div>';

			// Nadaljuj kasneje
			if($this->survey_type > 1 && !SurveyInfo::getInstance()->checkSurveyModule('panel')){
                echo '<div class="setting_holder">';
				echo '<input type="hidden" name="continue_later" value="0">';
            	echo '<input type="checkbox" id="continue_later" name="continue_later" value="1" '.($row['continue_later'] == 1 ? ' checked="checked"' : '').'>';
            	echo '<label for="continue_later">'.$lang['srv_show_continue_later'].' '.Help::display('srv_continue_later_setting').'</label> ';
                echo '</div>';
			}

			// Gumb nazaj
			echo '<div class="setting_holder">';
			echo '<input type="hidden" name="display_backlink" value="0">';
			echo '<input type="checkbox" id="display_backlink" name="display_backlink" value="1" '.($display_backlink!=='0'?' checked':'').'>';
			echo '<label for="display_backlink">'.$lang['srv_slideshow_sett_back_button_lbl'].'</label> ';
			echo '</div>';

			// Namig
			echo '<div class="setting_holder">';
			echo '<input type="hidden" name="survey_hint" value="0">';
			echo '<input type="checkbox" id="survey_hint" name="survey_hint" value="1" '.($survey_hint == 1 ? ' checked="checked"' : '').'>';
			echo '<label for="survey_hint">'.$lang['srv_hint'].' '.Help::display('srv_namig_setting').'</label> ';
			echo '</div>';

			/* KNJIZNICA */
			$sqlk = sisplet_query("SELECT * FROM srv_library_anketa WHERE ank_id='$this->anketa' AND uid='$global_user_id'");
			$moje = mysqli_num_rows($sqlk);
			$sqlk = sisplet_query("SELECT * FROM srv_library_anketa WHERE ank_id='$this->anketa' AND uid='0'");
			$javne = mysqli_num_rows($sqlk);
			
			if ($admin_type == 0 || $admin_type == 1) {

				echo '<div class="setting_holder">';
				echo '<input type="hidden" name="javne_ankete" value="0">';
				echo '<input type="checkbox" id="javne_ankete" name="javne_ankete" value="1" '.($javne==1?' checked':'').' onchange="javascript:check_library();">';
				echo '<label for="javne_ankete">'.$lang['srv_javne_ankete'].'</label> ';
				echo '</div>';				
			}
			
			echo '<div class="setting_holder" id="moje_ankete">';
			echo '<input type="hidden" name="moje_ankete" value="0">';
			echo '<input type="checkbox" id="moje_ankete" name="moje_ankete" value="1" '.($moje==1?' checked':'').'>';
			echo '<label for="moje_ankete">'.$lang['srv_moje_ankete'].' '.Help::display('srv_moje_ankete_setting').'</label> ';
			echo '</div>';

			?>
			<script>
			check_library();
			</script>
			<?
			
			// Politika zasebnosti
			echo '<div class="setting_holder">';
			echo '<span class="setting_title">' .$lang['srv_privacy'] .' '.Help::display('srv_privacy_setting').':</span>';
			
			echo '<div class="setting_item">';
			echo '<input type="radio" id="privacy0" name="privacy" value="0"' . ($survey_privacy == 0 ? ' checked="checked"' : '') . '>';
			echo '<label for="privacy0">'.$lang['srv_privacy_0'].'</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" id="privacy1" name="privacy" value="1"' . ($survey_privacy == 1 ? ' checked="checked"' : '') . '>';
			echo '<label for="privacy1">'.$lang['srv_privacy_1'].'</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" id="privacy2" name="privacy" value="2"' . ($survey_privacy == 2 ? ' checked="checked"' : '') . '>';
			echo '<label for="privacy2">'.$lang['srv_privacy_2'].'</label>';
			echo '</div>';

			echo '</div>';
			
			
			// Oozorilo na vprašanja
			echo '<div class="setting_holder">';
			echo '<span class="seting_title">'.$lang['srv_opozorilo_vprasanja'].Help::display('srv_splosnenas_opozorilo').':</span>';
			echo '<div class="setting_item">';
			echo '<span class="blue pointer" onClick="popupAlertAll(\'no\')"><span class="faicon minus_circle link-right"></span>'.$lang['srv_no_reminder_all'].'</span>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<span class="blue pointer" onClick="popupAlertAll(\'soft\')"><span class="faicon link-right fa-stack"><span class="faicon circle empty fa-stack-1x"></span> <span class="faicon plus small fa-stack-1x"></span></span>'.$lang['srv_soft_reminder_all'].'</span>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<span class="blue pointer" onClick="popupAlertAll(\'hard\')"><span class="faicon plus_circle link-right"></span>'.$lang['srv_hard_reminder_all'].'</span>';
			echo '</div>';
			
			echo '</div>';
 
			echo '</fieldset>';
					

			/* ZAKLJUCEK (samo pri formi) */
			if($row['survey_type'] == 1){

				echo '<fieldset>';

				echo '<legend>'.$lang['srv_end_label'].'</legend>';
				if ($row['url'] != '')
					$url = $row['url'];
				else
					$url = $site_url;

                    
                echo '<div class="setting_holder">';

                echo '<span class="setting_title"><label for="anketa' . $row['id'] . '" >' . $lang['srv_concl_link'] . ':</label></span>';
                
                echo '<div class="setting_item">';
                echo '  <input type="radio" id="concl_link0" name="concl_link" value="0" '.($row['concl_link'] == 0 ? ' checked' : '').' onclick="$(\'#srv_concl_link_go\').hide()">';
                echo '  <label for="concl_link0">'.$lang['srv_concl_link_close'].'</label>';		
                echo '</div>';
                echo '<div class="setting_item">';
                echo '  <input type="radio" id="concl_link1" name="concl_link" value="1" '.($row['concl_link'] == 1 ? ' checked' : '').' onclick="$(\'#srv_concl_link_go\').show()">';
                echo '  <label for="concl_link1">'.$lang['srv_concl_link_go'].'</label>';
                echo '</div>';
                echo '<div class="setting_item" id="srv_concl_link_go" '.($row['concl_link'] == 0?' style="display:none"':'').'>';
                echo '  <span class="setting_title"></span>';
                echo '  <label for="anketa' . $row['id'] . '" >' . $lang['srv_url'] . ':</label>';
                echo '  <form>';
                echo '      <input type="text" class="large" name="url" id="url_concl_sett" value="'.$url.'">';
                echo '  </form>';
                echo '</div>';
                
                echo '</div>';
				

				// Prikaz zakljucka
				echo '<div class="setting_holder">';
                
				echo '<span class="seting_title" ><label for="anketa' . $row['id'] . '" >' . $lang['srv_show_concl']. ':</label></span>';

				echo '<div class="setting_item">';
				echo '  <input type="radio" id="show_concl0" name="show_concl" value="0" '.(($row['show_concl'] == 0) ? ' checked="checked" ' : '').' onclick="$(\'#srv_concl_settings\').hide()" />';
				echo '  <label for="show_concl0">'.$lang['no1'].'</label>';
				echo '</div>';
				echo '<div class="setting_item">';
				echo '  <input type="radio" id="show_concl1" name="show_concl" value="1" '.(($row['show_concl'] == 1) ? ' checked="checked" ' : '').' onclick="$(\'#srv_concl_settings\').show()" />';
				echo '  <label for="show_concl1">'.$lang['yes'].'</label>';
				echo '</div>';

				echo '</div>';


				// Besedilo zakljucka
				echo '<div class="setting_holder" id="srv_concl_settings" '.($row['show_concl'] == 0?' style="display:none"':'').'>';
                
				$text = ($row['conclusion'] == '') ? $lang['srv_end'] : $row['conclusion'];
				echo '<label for="anketa' . $row['id'] . '" >' . $lang['text'] . ':</label>';
				echo '<textarea id="conclusion" name="conclusion">' . $text . '</textarea>';

                echo '</div>';


				// Gumb konec
				echo '<div class="setting_holder">';

				echo '<span class="setting_title" ><label for="anketa' . $row['id'] . '" >' . $lang['srv_concl_end_button_show'] . ':</label></span>';

				echo '<div class="setting_item">';
				echo '  <input type="radio" id="concl_end_button0" name="concl_end_button" value="0" '.(($row['concl_end_button'] == 0) ? ' checked="checked" ' : '').' />';
				echo '  <label for="concl_end_button0">'.$lang['no1'].'</label>';
				echo '</div>';
				echo '<div class="setting_item">';
				echo '  <input type="radio" id="concl_end_button1" name="concl_end_button" value="1" '.(($row['concl_end_button'] == 1) ? ' checked="checked" ' : '').' />';		
				echo '  <label for="concl_end_button1">'.$lang['yes'].'</label>';
				echo '</div>';
				
				echo '</div>';


				// Gumb nazaj
				echo '<div class="setting_holder">';

				echo '<span class="setting_title"><label for="anketa' . $row['id'] . '" >' . $lang['srv_concl_back_button_show'] . ':</label></span>';

				echo '<div class="setting_item">';
				echo '  <input type="radio" id="concl_back_button0" name="concl_back_button" value="0" '.(($row['concl_back_button'] == 0) ? ' checked="checked" ' : '').' />';
				echo '  <label for="concl_back_button0">'.$lang['no1'].'</label>';
				echo '</div>';
				echo '<div class="setting_item">';
				echo '  <input id="concl_back_button1" type="radio" name="concl_back_button" value="1" '.(($row['concl_back_button'] == 1) ? ' checked="checked" ' : '').' />';
				echo '  <label for="concl_back_button1">'.$lang['yes'].'</label>';
				echo '</div>';
				
				echo '</div>';

                
				echo '</fieldset>';			
			}
						
			/* STEVILCENJE */
			echo '<fieldset>';
			echo '<legend>' . $lang['srv_nastavitveStevilcenje'] . '</legend>';

			// Izklop prestevilcevanja
			$enumerate = SurveySetting::getInstance()->getSurveyMiscSetting('enumerate'); if ($enumerate == '') $enumerate = 1;
			
			echo '<div class="setting_holder">';
			echo '<input type="hidden" name="enumerate" value="0">';
			echo '<input type="checkbox" id="enumerate" name="enumerate" value="1" '.($enumerate == 1 ? ' checked' : '').'>';
			echo '<label for="enumerate">'.$lang['srv_nastavitvePrestevilcevanje'].'</label> ';
			echo '</div>';

			//način številčenja

			echo '<div class="setting_holder">';
			echo '<span class="setting_title"><label for="anketa_countType">' . $lang['srv_nastavitveStevilcenjeType'] . ':</label></span>';

			echo '<div class="setting_item">';
			echo '<input type="radio" name="countType" value="0" id="countType_0" checked="checked" onclick="saveGlobalSetting(\'countType\')"/>';
			echo '<label for="countType_0">' . $lang['srv_nastavitveStevilcenjeType0'] . '</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="countType" value="1" id="countType_1" ' . ($row['countType'] == 1 ? ' checked="checked"' : '') . ' onclick="saveGlobalSetting(\'countType\')"/>';
			echo '<label for="countType_1">' . $lang['srv_nastavitveStevilcenjeType1'] . '</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="countType" value="2" id="countType_2" ' . ($row['countType'] == 2 ? ' checked="checked"' : '') . ' onclick="saveGlobalSetting(\'countType\')"/>';
			echo '<label for="countType_2">' . $lang['srv_nastavitveStevilcenjeType2'] . '</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="countType" value="3" id="countType_3" ' . ($row['countType'] == 3 ? ' checked="checked"' : '') . ' onclick="saveGlobalSetting(\'countType\')"/>';
			echo '<label for="countType_3">' . $lang['srv_nastavitveStevilcenjeType3'] . '</label>';
			echo '</div>';

			echo '</div>';
			
			echo '</fieldset>';
						
			/* JS TRACKING */
			if ($admin_type == 0 || $admin_type == 1) {
				echo '<fieldset><legend>'.$lang['srv_js_tracking'].'</legend>';
				echo '<div class="setting_holder">';
				echo '<span class="setting_title">'.$lang['js_tracking_note'].'</span>';
				echo '<textarea class="textarea w800" name="js_tracking">'.$row['js_tracking'].'</textarea>';
				echo '</div>';

				echo '</fieldset>';
			}
		}

		/* PISKOTEK */
		if ($_GET['a'] == A_PRIKAZ) {

			echo '<fieldset>';
			echo '<legend>' . $lang['srv_data_valid_units_settings'] . '</legend>';

			echo '<div class="setting_holder">';
			echo '<span class="setting_title" >'.$lang['srv_prikaz_default_valid'].'</span>';

			echo '<div class="setting_item">';
			echo '<input type="radio" id="defValidProfile2" name="defValidProfile" '.($row['defValidProfile']==2?' checked':'').' value="2">';
			echo '<label for="defValidProfile2">'.'(5,6) '.$lang['srv_data_valid_units'].'</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" id="defValidProfile3" name="defValidProfile" '.($row['defValidProfile']==3?' checked':'').' value="3">';
			echo '<label for="defValidProfile3">'.'(6) '.$lang['srv_data_finished_units'].'</label>';
			echo '</div>';
			
			echo '</div>';

            // Pri volitvah ne moremo prikazati datuma respondenta
            /*if(!SurveyInfo::getInstance()->checkSurveyModule('voting')){

				echo '<div class="setting_holder">';
                echo '<span class="setting_title" >'.$lang['srv_prikaz_showItime'].'</span>';

				echo '<div class="setting_item">';
				echo '<input type="radio" id="showItime0" name="showItime" '.((int)$row['showItime']==0?' checked':'').' value="0">';
                echo '<label for="showItime0">'.$lang['no1'].'</label>';
				echo '</div>';
				echo '<div class="setting_item">';
				echo '<input type="radio" id="showItime1" name="showItime" '.((int)$row['showItime']==1?' checked':'').' value="1">';
                echo '<label for="showItime1">'.$lang['yes'].'</label>';
				echo '</div>';
				
				echo '</div>';
            }

			echo '<div class="setting_holder">';
			echo '<span class="setting_title" >'.$lang['srv_prikaz_showLineNumber'].'</span>';

			echo '<div class="setting_item">';
			echo '<input type="radio" id="showLineNumber0" name="showLineNumber" '.((int)$row['showLineNumber']==0?' checked':'').' value="0">';
			echo '<label for="showLineNumber0">'.$lang['no1'].'</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" id="showLineNumber1" name="showLineNumber" '.((int)$row['showLineNumber']==1?' checked':'').' value="1">';
			echo '<label for="showLineNumber1">'.$lang['yes'].'</label>';
			echo '</div>';
			
			echo '</div>';*/

			echo '</fieldset>';

			echo '<form>';

		}

		/*Piskotek*/
		if ($_GET['a'] == 'piskot') {

            // Pri volitvah ne moremo popravljati nastavitev piskotka
            if(SurveyInfo::getInstance()->checkSurveyModule('voting')){
                
                echo '<fieldset>';
                echo '<legend>' . $lang['srv_cookie'] . '</legend>';
                echo '<span class="red">'.$lang['srv_voting_no_cookie'].'</span>';
                echo '</fieldset>';
                
                echo '</form>';

                return;
            }
            
            echo '<fieldset>';

            echo '<legend>' . $lang['settings'] . '</legend>';

            // Piskotkov nimamo, ce imamo vklopljen modul panel
            if(!SurveyInfo::getInstance()->checkSurveyModule('panel')){
                
                echo '<div class="divPopUp_info warning_popup" id="cookie_alert">';
                echo '<h2>'.$lang['srv_cookie_alert_title'].'</h2>';
                echo '<p>'.$lang['srv_cookie_alert_1'].'</p>';
                echo '<p>'.$lang['srv_cookie_alert_2'].'</p>';
                echo '</div>';
                
                
                // Shrani piskotek za X casa
                echo '<div id="cookie_osnovno" class="setting_holder bottom8">';
                echo '<span class="setting_title">' . $lang['srv_cookie'] . Help :: display('srv_cookie') .':</span>';

                echo '<div class="setting_item">';
                echo '<input type="radio" name="cookie" value="-1" id="cookie_-1"' . ($row['cookie'] == -1 ? ' checked="checked"' : '') . ' onclick="checkcookie();" />';
                echo '<label for="cookie_-1">' . $lang['srv_cookie_-1'] . '</label>';
                echo '</div>';
                echo '<div class="setting_item">';
                echo '<input type="radio" name="cookie" value="0" id="cookie_0"' . ($row['cookie'] == 0 ? ' checked="checked"' : '') . ' onclick="checkcookie();" />';
                echo '<label for="cookie_0">' . $lang['srv_cookie_0'] . '</label>';
                echo '</div>';
                echo '<div class="setting_item">';
                echo '<input type="radio" name="cookie" value="1" id="cookie_1"' . ($row['cookie'] == 1 ? ' checked="checked"' : '') . ' onclick="checkcookie();" />';
                echo '<label for="cookie_1">' . $lang['srv_cookie_1'] . '</label>';
                echo '</div>';
                echo '<div class="setting_item">';
                echo '<input type="radio" name="cookie" value="2" id="cookie_2"' . ($row['cookie'] == 2 ? ' checked="checked"' : '') . ' onclick="checkcookie();" />';
                echo '<label for="cookie_2">' . $lang['srv_cookie_2'] . '</label>';
                echo '</div>';
                
                echo '</div>';


                echo '<div id="cookie_settings_wrap">';

                // Ko se uporabnik vrne (zacne od zacetka/nadaljuje kjer je ostal)
    
                echo '<div class="setting_holder indent">';
                echo '<span class="setting_title">' . $lang['srv_cookie_return'] . Help :: display('srv_cookie_return') . ':</span>';
    
                echo '<div class="setting_item">';
                echo '<input type="radio" name="cookie_return" value="0" id="cookie_return_0"' . ($row['cookie_return'] == 0 ? ' checked="checked"' : '') . ' onclick="checkcookie();" />';
                echo '<label for="cookie_return_0">' . $lang['srv_cookie_return_start'] . '</label>';
                echo '</div>';
                echo '<div class="setting_item no-cookie">';
                echo '<input type="radio" name="cookie_return" value="1" id="cookie_return_1"' . ($row['cookie_return'] == 1 ? ' checked="checked"' : '') . ' onclick="checkcookie();" />';
                echo '<label for="cookie_return_1">' . $lang['srv_cookie_return_middle'] . '</label>';
                echo '</div>';
                
                echo '</div>';

                // Ce je zakljucil lahko naknadno ureja svoje odgovore
                echo '<div class="setting_holder bottom16 indent no-cookie no-cookie-return">';
                echo '<span class="setting_title" >' . $lang['srv_return_finished'] . Help :: display('srv_return_finished') . ':</span>';

                echo '<div class="setting_item">';
                echo '<input type="radio" name="return_finished" value="1" id="return_finished_1"' . ($row['return_finished'] == 1 ? ' checked="checked"' : '') . ' />';
                echo '<label for="return_finished_1">' . $lang['srv_return_finished_yes'] . '</label>';
                echo '</div>';
                echo '<div class="setting_item">';
                echo '<input type="radio" name="return_finished" value="0" id="return_finished_0"' . ($row['return_finished'] == 0 ? ' checked="checked"' : '') . ' />';
                echo '<label for="return_finished_0">' . $lang['srv_return_finished_no'] . '</label>';
                echo '</div>';
                
                echo '</div>';
                
                echo '</div>';
            }


			// Nikoli ne more popravljati svojih odgovorov (tudi ce se npr. vrne na prejsnjo stran)
			echo '<div class="setting_holder">';
            echo '<span class="setting_title" >' . $lang['srv_subsequent_answers'] . Help :: display('srv_subsequent_answers') . ':</span>';

			echo '<div class="setting_item">';
			echo '<input type="radio" name="subsequent_answers" value="1" id="subsequent_answers_1"' . ($row['subsequent_answers'] == 1 ? ' checked="checked"' : '') . ' />';
            echo '<label for="subsequent_answers_1">' . $lang['srv_subsequent_answers_yes'] . '</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="subsequent_answers" value="0" id="subsequent_answers_0"' . ($row['subsequent_answers'] == 0 ? ' checked="checked"' : '') . ' />';
            echo '<label for="subsequent_answers_0">' . $lang['srv_subsequent_answers_no'] . '</label>';
			echo '</div>';
			
			echo '</div>';

            
            // Prepoznaj respondenta
			echo '<div class="setting_holder" id="prepoznaj_respondenta">';
            echo '<span class="setting_title">' . $lang['srv_user'] . Help :: display('srv_user_from_cms') . ':</span>';

			echo '<div class="setting_item">';
			echo '<input type="radio" name="user_from_cms" value="1" id="user_1"' . ($row['user_from_cms'] == 1 ? ' checked="checked"' : '') . ' onclick="javascript:checkcookie(); $(\'#user_1_email\').removeAttr(\'disabled\')" />';
            echo '<label for="user_1">' . $lang['srv_respondent'] . '</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="user_from_cms" value="2" id="user_2"' . ($row['user_from_cms'] == 2 ? ' checked="checked"' : '') . ' onclick="javascript:checkcookie(); $(\'#user_1_email\').removeAttr(\'disabled\')" />';
            echo '<label for="user_2">' . $lang['srv_vnasalec'] . '</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="user_from_cms" value="0" id="user_0"' . ($row['user_from_cms'] == 0 ? ' checked="checked"' : '') . ' onclick="javascript:checkcookie(); $(\'#user_1_email\').attr(\'disabled\', true); _user_from_cms(); " />';
            echo '<label for="user_0">' . $lang['no1'] . '</label>';
			echo '</div>';
			
			echo '</div>';
            
            // Ob izpolnjevanju prikazi email
			echo '<div class="setting_holder" id="cms_email">';
            echo '<span class="setting title">' . $lang['srv_user_cms_show'] . ':</span>';

			echo '<div class="setting_item">';
			echo '<input type="checkbox" name="user_from_cms_email" value="1" id="user_1_email"' . ($row['user_from_cms_email'] == 1 ? ' checked="checked"' : '') . ' '.($row['user_from_cms']>0?'':' disabled="true" ').'/>';
            echo '<label for="user_1_email">' . $lang['srv_user_cms_email'] . '</label>';
			echo '</div>';
			
			echo '</div>';

            echo '</fieldset>';
                                
            // Masovno vnasanje - modul Vnos
            echo '<fieldset id="vnos_modul"><legend>'.$lang['srv_vrsta_survey_type_5'].'</legend>';

			echo '<p class="bottom16">'.$lang['srv_vnos_navodila'].'</p>';
			
			echo '<div class="setting_holder">';
            echo '<span class="setting_title">' . $lang['srv_mass_input'] . ':</span>';

			echo '<div class="setting_item">';
			echo '<input type="radio" name="mass_insert" value="1" id="mass_insert_1"' . ($row['mass_insert'] == 1 ? ' checked="checked"' : '') . ' />';
			echo '<label for="mass_insert_1">' . $lang['srv_mass_input_1'] . '</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="mass_insert" value="0" id="mass_insert_0"' . ($row['mass_insert'] == 0 ? ' checked="checked"' : '') . ' />';
			echo '<label for="mass_insert_0">' . $lang['srv_mass_input_0'] . '</label>';
			echo '</div>';
			
            echo '</fieldset>';
                        
            // For modul maza, show all cookie settings
            $isMaza = (SurveyInfo::checkSurveyModule('maza')) ? 1 : 0;
            
            ?> <script>
                
                function checkcookie () {

					if ($('input[name=cookie]:checked').val() == '-1' && $('input[name=user_from_cms]:checked').val() == '0' && <?echo $row['user_base'];?> != 1 && <?echo $isMaza;?> != 1) {
						$('#cookie_settings_wrap').addClass('displayNone');
						$('#cookie_osnovno').removeClass('bottom8');
                    } 
                    else {
						$('#cookie_settings_wrap').removeClass('displayNone');
						$('#cookie_osnovno').addClass('bottom8');
                    }
                    
                    if ( $('input[name=cookie_return]:checked').val() == 1 ) {
                        $('.no-cookie-return').addClass('displayNone');
                    } 
                    else {
                        $('.no-cookie-return').removeClass('displayNone');
                    }
                    
                    if ( $('input[name=user_from_cms]:checked').val() == 0 ) {
                        $('#cms_email').addClass('displayNone');
						$('#prepoznaj_respondenta').addClass('bottom0');
                    } 
                    else {
                        $('#cms_email').removeClass('displayNone');
						$('#prepoznaj_respondenta').removeClass('bottom0');

                    }
                    
                    if ( $('input[name=user_from_cms]:checked').val() == 2 ) {
                        $('#vnos_modul').removeClass('displayNone');
                    } 
                    else {
                        $('#vnos_modul').addClass('displayNone');
                    }
                }

                checkcookie();
                cookie_alert();

            </script> <?

            $stringDostopAvtor = "SELECT count(*) as isAvtor FROM srv_dostop WHERE ank_id = '" . $this->anketa . "' AND (uid='" . $global_user_id . "' OR uid IN (SELECT user FROM srv_dostop_manage WHERE manager='$global_user_id' ))";
            $sqlDostopAvtor = sisplet_query($stringDostopAvtor);
            $rowDostopAvtor = mysqli_fetch_assoc($sqlDostopAvtor);
            $avtorRow = SurveyInfo::getInstance()->getSurveyRow();

            echo '<fieldset>';
            
            echo '<legend>' . $lang['access'] . '</legend>';

			/*echo '<div class="setting_holder">';
            echo '<span class="setting_title" ><label for="odgovarja">' . $lang['srv_izpolnjujejo'] . Help :: display('srv_izpolnjujejo') .': </label></span>';
			echo '<select name="odgovarja" id="odgovarja" onchange="javascript:_odgovarja();" class="dropdown large">';
            echo '<option value="4"' . ($row['odgovarja'] == 4 ? ' selected="selected"' : '') . '>' . $lang['forum_hour_all'] . '</option>';
            echo '<option value="3"' . ($row['odgovarja'] == 3 ? ' selected="selected"' : '') . '>' . $lang['forum_registered'] . '</option>';
            echo '<option value="2"' . ($row['odgovarja'] == 2 ? ' selected="selected"' : '') . '>' . $lang['forum_clan'] . '</option>';
            echo '<option value="1"' . ($row['odgovarja'] == 1 ? ' selected="selected"' : '') . '>' . $lang['forum_manager'] . '</option>';
            echo '<option value="0"' . ($row['odgovarja'] == 0 ? ' selected="selected"' : '') . '>' . $lang['forum_admin'] . '</option>';
            echo '</select>';
			echo '</div>';*/

            echo '<script language="javascript">'."\n";
            echo '  function _user_from_cms() {'."\n";
            echo '    document.settingsanketa_' . $row['id'] . '.odgovarja.value = \'4\''."\n";
            echo '  }'."\n";
            echo '  function _odgovarja() {'."\n";
            echo '    if (document.settingsanketa_' . $row['id'] . '.odgovarja.value != \'4\' && document.settingsanketa_' . $row['id'] . '.user_from_cms[2].checked == true) {'."\n";
            echo '      document.settingsanketa_' . $row['id'] . '.user_from_cms[0].checked = true;'."\n";
            echo '    }'."\n";
            echo '}'."\n";
            echo '</script>'."\n";

			echo '<div class="setting_holder">';
            echo '<span class="setting_title">' . $lang['srv_block_ip'] . Help :: display('srv_block_ip') . ':</span>';

			echo '<div class="setting_item">';
			echo '<input type="radio" name="block_ip" value="0" id="block_ip_0"' . ($row['block_ip'] == 0 ? ' checked="checked"' : '') . ' onChange="$(\'#block_ip_warning\').addClass(\'displayNone\');" />';
            echo '<label for="block_ip_0">' . $lang['no1'] . '</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="block_ip" value="10" id="block_ip_10"' . ($row['block_ip'] == 10 ? ' checked="checked"' : '') . ' onChange="$(\'#block_ip_warning\').removeClass(\'displayNone\');" />';
            echo '<label for="block_ip_10">10 min</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="block_ip" value="20" id="block_ip_20"' . ($row['block_ip'] == 20 ? ' checked="checked"' : '') . ' onChange="$(\'#block_ip_warning\').removeClass(\'displayNone\');" />';
            echo '<label for="block_ip_20">20 min</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="block_ip" value="60" id="block_ip_60"' . ($row['block_ip'] == 60 ? ' checked="checked"' : '') . ' onChange="$(\'#block_ip_warning\').removeClass(\'displayNone\');" />';
            echo '<label for="block_ip_60">60 min</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="block_ip" value="720" id="block_ip_720"' . ($row['block_ip'] == 720 ? ' checked="checked"' : '') . ' onChange="$(\'#block_ip_warning\').removeClass(\'displayNone\');" />';
            echo '<label for="block_ip_720">12 '.$lang['hour_hours2'].'</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="block_ip" value="1440" id="block_ip_1440"' . ($row['block_ip'] == 1440 ? ' checked="checked"' : '') . ' onChange="$(\'#block_ip_warning\').removeClass(\'displayNone\');" />';
            echo '<label for="block_ip_1440">24 '.$lang['hour_hours2'].'</label>';
			echo '</div>';
			
			echo '</div>';

            echo '<p id="block_ip_warning" class="top16 red '.($row['block_ip'] == 0 ? ' displayNone' : '').'">'.$lang['srv_block_ip_warning'].'</p>';

            echo '</fieldset>';
            
            // Preverimo, ce je funkcionalnost v paketu, ki ga ima uporabnik
            global $global_user_id;
            $userAccess = UserAccess::getInstance($global_user_id);

            // dodajanje gesel za anketo
            echo '<fieldset><legend>'.$lang['srv_password'].' '.Help::display('srv_dostop_password').'</legend>';

            if(!$userAccess->checkUserAccess($what='password')){
                $userAccess->displayNoAccess($what='password');
            }
            else{

                echo '<div class="setting_holder" id="password">';
                
                $ss = new SurveySkupine($this->anketa);
                $spr_id = $ss->hasSkupine(2);
                
                echo '<input type="hidden" id="skupine_spr_id" value="'.$spr_id.'"></input>';
                
                // Preprecimo submit na enter
                echo '<script>';
                ?>
                    $('form[name=settingsanketa_'+<?echo $this->anketa;?>+']').on('keyup keypress', function(e) {
                        var keyCode = e.keyCode || e.which;
                        if (keyCode === 13) { 
                            e.preventDefault();
                            return false;
                        }
                    });
                <?
                echo '</script>';
                
                // dodajanje gesel za anketo

				echo '<div class="setting_holder bottom0">';
				echo '<div class="button_holder inline bottom16">';
				echo '<input type="text" class="large" name="skupina" autocomplete="off" onKeyUp="add_skupina_enter(\'2\', event);" />';
				echo '<button type="button" class="medium blue" onclick="add_skupina(\'2\');">'.$lang['add'].'</button>';
				echo '</div>';
				echo '</div>';

				echo '<div>';

				echo '<table id="passwords_table" '.(($spr_id == 0) ? 'class="displayNone"' : '').'>';

					echo '<tr>';
					echo '<th>'.$lang['password'].'</th>';
					echo '<th></th>';
					echo '</tr>';

					if($spr_id > 0){
						$vrednosti = $ss->getVrednosti($spr_id);
						if($vrednosti != 0){
							foreach($vrednosti as $vrednost){
								echo '<tr>';
								echo '<td>'.$vrednost['naslov'].'</td>';
								echo '<td class="right"><button class="table-inline blue caps" type="button" onclick="delete_skupina(\'2\', \''.$vrednost['id'].'\');">'.$lang['srv_password_remove'].'</button></td>';
								echo '</tr>';
							}
						}
					}

				echo '</table>';
				echo '</div>';


				echo '<div class="setting_holder">';
				echo '<a class="noline" href="#" onClick="display_add_passwords_mass();"><span class="faicon file-import flipX link-right"></span>'.$lang['srv_password_add_mass'].'</a>';
				echo '</div>';

                echo '</div>';
            }
			
			echo '</fieldset>';		
		}

		/*Trajanje*/

		if ($_GET['a'] == 'trajanje') {
			echo '<div >';
			echo '<input type="hidden" value="' . $this->anketa . '" name="anketa" >';
			$this->DisplayNastavitveTrajanje();
			$this->DisplayNastavitveMaxGlasov();
			echo '</form>';
			echo '<br/>';
			if (isset($_GET['f'])) {
				switch ($_GET['f']) {
					case 'vabila_settings':
						$url =$site_url . 'admin/survey/index.php?anketa='.$this->anketa.'&a=vabila&m=settings';
						break;
				}
				if (isset($url)) {
					echo '<span class="buttonwrapper floatLeft spaceRight"><a class="ovalbutton ovalbutton_gray" href="'.$url.'"><span>'.$lang['back'] . '</span></a></span>';
				}
			}
			echo '<div class="button_holder">';
			echo '<button class="medium white-blue" onClick="window.location='.$preklici_url.';return false;">'.$lang['edit1338'].'</button>';
			echo '<button class="medium blue" onclick="submitSurveyDuration();return false;" href="#">'.$lang['edit1337'].'</button>';
			echo '</div>';
		}

		/*Respondenti*/

		if ($_GET['a'] == 'resp') {
			$this->respondenti_iz_baze($row);
		}

		/*Komentarji*/

		if ($_GET['a'] == 'urejanje') {

			// tukaj bom dodal še kontrolo na Avtorja ankete, tako da avtor lahko vedno spreminja urejanje (gorazd,1.9.2009)
			$stringDostopAvtor = "SELECT count(*) as isAvtor FROM srv_dostop WHERE ank_id = '" . $this->anketa . "' AND uid='" . $global_user_id . "'";
			$sqlDostopAvtor = sisplet_query($stringDostopAvtor);
			$rowDostopAvtor = mysqli_fetch_assoc($sqlDostopAvtor);
			
			if ($admin_type == 0 || $rowDostopAvtor['isAvtor'] || isDemoSurvey($this->anketa)) {

				SurveySetting::getInstance()->Init($this->anketa);
				
				$survey_comment = SurveySetting::getInstance()->getSurveyMiscSetting('survey_comment');
				$survey_comment_showalways = SurveySetting::getInstance()->getSurveyMiscSetting('survey_comment_showalways');
				$question_comment = SurveySetting::getInstance()->getSurveyMiscSetting('question_comment');
				
				$survey_comment_viewadminonly = SurveySetting::getInstance()->getSurveyMiscSetting('survey_comment_viewadminonly');
				$survey_comment_viewauthor = SurveySetting::getInstance()->getSurveyMiscSetting('survey_comment_viewauthor');
				$question_comment_viewadminonly = SurveySetting::getInstance()->getSurveyMiscSetting('question_comment_viewadminonly');
				$question_comment_viewauthor = SurveySetting::getInstance()->getSurveyMiscSetting('question_comment_viewauthor');
				$question_resp_comment_viewadminonly = SurveySetting::getInstance()->getSurveyMiscSetting('question_resp_comment_viewadminonly');
				$question_resp_comment_inicialke = SurveySetting::getInstance()->getSurveyMiscSetting('question_resp_comment_inicialke');
				$question_resp_comment_inicialke_alert = SurveySetting::getInstance()->getSurveyMiscSetting('question_resp_comment_inicialke_alert');

				$question_resp_comment = SurveySetting::getInstance()->getSurveyMiscSetting('question_resp_comment');
				$srv_qct = SurveySetting :: getInstance()->getSurveyMiscSetting('question_comment_text');
				
				$question_note_view = SurveySetting::getInstance()->getSurveyMiscSetting('question_note_view');
				$question_note_write = SurveySetting::getInstance()->getSurveyMiscSetting('question_note_write');
				
				$question_resp_comment_show_open = SurveySetting::getInstance()->getSurveyMiscSetting('question_resp_comment_show_open');

				$survey_comment_resp = SurveySetting::getInstance()->getSurveyMiscSetting('survey_comment_resp');
				$survey_comment_showalways_resp = SurveySetting::getInstance()->getSurveyMiscSetting('survey_comment_showalways_resp');
				$survey_comment_viewadminonly_resp = SurveySetting::getInstance()->getSurveyMiscSetting('survey_comment_viewadminonly_resp');
				$survey_comment_viewauthor_resp = SurveySetting::getInstance()->getSurveyMiscSetting('survey_comment_viewauthor_resp');			
				
				$sortpostorder = SurveySetting::getInstance()->getSurveyMiscSetting('sortpostorder');
				$addfieldposition = SurveySetting::getInstance()->getSurveyMiscSetting('addfieldposition');
				$commentmarks = SurveySetting::getInstance()->getSurveyMiscSetting('commentmarks');
				$commentmarks_who = SurveySetting::getInstance()->getSurveyMiscSetting('commentmarks_who');
				$comment_history = SurveySetting::getInstance()->getSurveyMiscSetting('comment_history');
				
				$srvlang_srv_question_respondent_comment = SurveySetting::getInstance()->getSurveyMiscSetting('srvlang_'.'srv_question_respondent_comment'.'');
				if ($srvlang_srv_question_respondent_comment == '') $srvlang_srv_question_respondent_comment = $lang['srv_question_respondent_comment'];
				
				$preview_disableif = SurveySetting::getInstance()->getSurveyMiscSetting('preview_disableif');
				$preview_disablealert = SurveySetting::getInstance()->getSurveyMiscSetting('preview_disablealert');
				$preview_displayifs = SurveySetting::getInstance()->getSurveyMiscSetting('preview_displayifs');
				$preview_displayvariables = SurveySetting::getInstance()->getSurveyMiscSetting('preview_displayvariables');
				$preview_hidecomment = SurveySetting::getInstance()->getSurveyMiscSetting('preview_hidecomment');				
				$preview_hide_survey_comment = SurveySetting::getInstance()->getSurveyMiscSetting('preview_hide_survey_comment');				
				$preview_survey_comment_showalways = SurveySetting::getInstance()->getSurveyMiscSetting('preview_survey_comment_showalways');				
				$preview_disable_test_insert = SurveySetting::getInstance()->getSurveyMiscSetting('preview_disable_test_insert');				
				
				if ( isset($_GET['show']) && $_GET['show']=='on_alert' ) {
					echo '<div class="comments_on_alert google_yellow">';
                    
                    echo '  <div class="title"><span class="faicon success"></span>'.$lang['srv_comments_on_alert'].'</div>';

                    echo '  <div class="text">';
                    echo        $lang['srv_comments_on_alert1'].' <a href="ajax.php?anketa='.$this->anketa.'&a=comments_onoff&do=off">'.$lang['srv_here'].'</a>. ';
                    echo        $lang['srv_comments_on_alert2'];
                    echo '      <a href="https://www.1ka.si/d/sl/pomoc/vodic-za-uporabnike/testiranje/komentarji/?from1ka=1" target="_blank">'.$lang['srv_more_info'].'</a>.';
                    echo '  </div>';

                    echo '</div>';
				}
				
				$css_width = '';
				if ($survey_comment != "") {
					$css_width = 'min-height:250px;width:45% !important;';
					$css_width2 = 'width:45% !important;';
				}
				
				$advanced_expanded = isset($_GET['advanced_expanded']) ? $_GET['advanced_expanded'] : '';


				echo '<fieldset class="comments"><legend>'.$lang['comments'].'</legend>';

				echo '<p class="semi-bold caps bottom16">'.$lang['srv_admin_s_comments_editor'].'</p>';
				echo '<div class="setting_holder">';
				echo '<input type="checkbox" name="comments_default" id="comments_admin1" onclick="comments_admin_toggle(\'1\')" admin_on="false" /><label class="semi-bold" for="comments_admin1"> '.$lang['srv_comments_admin_on1'].'</label>';
				echo '<p class="indent">('.$lang['srv_comments_admin_note1'].')</p>';
				echo '</div>';
				echo '<div class="setting_holder">';
				echo '<input type="checkbox" name="comments_default" id="comments_admin2" onclick="comments_admin_toggle(\'2\')" admin_on="false" /><label class="semi-bold" for="comments_admin2"> '.$lang['srv_comments_admin_on2'].'</label>';
				echo '<p class="indent">('.$lang['srv_comments_admin_note2'].')</p>';
				echo '</div>';

				echo '<p class="semi-bold caps bottom16">'.$lang['srv_admin_s_comments_resp'].'</p>';
				echo '<div class="setting_holder">';
				echo '<input type="checkbox" name="comments_resp2" id="comments_resp2" onclick="comments_resp_toggle(\'2\')" resp_on="false" /><label class="semi-bold" for="comments_resp2"> '.$lang['srv_comments_resp_on2'].'</label>';
				echo '<p class="indent">('.$lang['srv_comments_resp_note2'].')</p>';
				echo '</div>';
				echo '<div class="setting_holder">';
				echo '<input type="checkbox" name="comments_default" id="comments_resp" onclick="comments_resp_toggle(\'1\')" resp_on="false" /><label class="semi-bold" for="comments_resp"> '.$lang['srv_comments_resp_on'].'</label>';
				echo '<p class="indent">('.$lang['srv_comments_resp_note'].')</p>';
				echo '</div>';

				echo '<p class="semi-bold caps top16 bottom16">'.$lang['srv_admin_s_comments_links'].'</p>';

				echo '<div class="links">';

				$d = new Dostop();

				echo '<div class="button" title="'.$lang['srv_view_comment'].'" onClick="window.location.href=\'index.php?anketa='.$this->anketa.'&a=komentarji\';">';
				echo '<span class="faicon comments blue"></span>';
				echo $lang['srv_admin_s_comments_view'];
				echo '</div>';

				echo '<div class="button" id="more_comments" onclick="$(\'#komentarji_napredno\').fadeToggle(); $(\'#komentarji_napredno_arrow\').toggleClass(\'plus minus\'); $(this).toggleClass(\'border_blue\'); return false;">';
				echo '<span id="komentarji_napredno_arrow" class="faicon blue '.($advanced_expanded==1 ? 'minus' : 'plus').'"></span>';
				echo $lang['srv_admin_s_comments_set'];
				echo '</div>';
				
				echo '</div>';
				
				echo '</fieldset>';
				
				
				?>
				<script>
					$(function() {
						if ( check_comments_admin(1) ) { 
							$('#comments_admin1').attr('admin_on', 'true');
							$('#comments_admin1').attr('checked', true);
						} else {
							$('#comments_admin1').attr('admin_on', 'false');
							$('#comments_admin1').attr('checked', false);
						}
						if ( check_comments_admin(2) ) {
							$('#comments_admin2').attr('admin_on', 'true');
							$('#comments_admin2').attr('checked', true);
						} else {
							$('#comments_admin2').attr('admin_on', 'false');
							$('#comments_admin2').attr('checked', false);
						}
						
						if ( check_comments_resp(1) ) {
							$('#comments_resp').attr('resp_on', 'true');
							$('#comments_resp').attr('checked', true);
						} else {
							$('#comments_resp').attr('resp_on', 'false');
							$('#comments_resp').attr('checked', false);
						}
						if ( check_comments_resp(2) ) {
							$('#comments_resp2').attr('resp_on', 'true');
							$('#comments_resp2').attr('checked', true);
						} else {
							$('#comments_resp2').attr('resp_on', 'false');
							$('#comments_resp2').attr('checked', false);
						}
						
						if ( ( !check_comments_admin() && !check_comments_admin_off() ) || ( !check_comments_resp() && !check_comments_resp_off() ) ) {
							$('#komentarji_napredno').show();
                            $('#komentarji_napredno_arrow').addClass('minus');
                            $('#komentarji_napredno_arrow').removeClass('plus'); 
                            $('#more_comments').removeClass('border_blue');
						}
						
					});
				</script>
				<?
				
				if ($survey_comment != "") {
					
					echo '<fieldset class="comments" ><legend>'.$lang['srv_admin_s_comments'].'</legend>';
					echo '<textarea name="comment_send"></textarea>';

					echo '<div class="setting_holder">';
					echo '<input type="checkbox" id="srv_c_alert" name="srv_c_alert" checked value="1" />';
					echo '<label for="srv_c_alert"> '.$lang['srv_c_alert'].'</label>';
					echo '</div>';
					echo '<div class="setting_holder">';
					echo '<input type="checkbox" id="srv_c_to_mail" name="srv_c_to_mail" value="1" onchange="$(\'#prejemniki\').toggle();" />';
					echo '<label for="srv_c_to_mail"> '.$lang['srv_c_to_mail'].'</label>';

					echo '<div id="prejemniki" style="display:none">';
					$sqlp = sisplet_query("SELECT u.name, u.surname, u.email FROM srv_dostop d, users u WHERE d.uid=u.id AND ank_id='$this->anketa'");
					while ($rowp = mysqli_fetch_array($sqlp)) {

						echo '<div class="setting_item indent">';
						echo '<input type="checkbox" name="mails[]" value="'.$rowp['email'].'" checked="checked" id="'.$rowp['email'].'" />';
						echo '<label for="'.$rowp['email'].'"> '.$rowp['name'].' '.$rowp['surname'].' ('.$rowp['email'].')</label>';
						echo '</div>';
					}
					echo '</div>';
					echo '</div>';
					
					echo '</fieldset>';
				}
				
				//Komentarji - napredne nastavitve
				echo '<div id="komentarji_napredno" '.($advanced_expanded==1 ? '' : 'style="display:none"').'>';

				echo '<div class="button_holder">';
				echo '  <button class="medium white-blue" onClick="window.location='.$preklici_url.';return false;">'.$lang['edit1338'].'</button>';				
				echo '  <button class="medium blue" onclick="document.settingsanketa_' . $row['id'] . '.submit(); return false;">'.$lang['edit1337'].'</button>';
				echo '</div>';
				
				echo '<fieldset class="comments"><legend>'.$lang['srv_preview_defaults'].'</legend>';


				echo '<div class="setting_holder">';
				echo '  <input type="hidden" name="preview_disableif" value=""><input type="checkbox" value="1" '.($preview_disableif==1?' checked':'').' name="preview_disableif" id="disableif">';
				echo '  <label for="disableif">'.$lang['srv_disableif'].'</label>';
				echo '</div>';

				echo '<div class="setting_holder">';
				echo '  <input type="hidden" name="preview_displayifs" value=""><input type="checkbox" value="1" '.($preview_displayifs==1?' checked':'').' name="preview_displayifs" id="displayifs">';
				echo '  <label for="displayifs">'.$lang['srv_displayifs'].'</label>';
				echo '</div>';

				echo '<div class="setting_holder">';
				echo '  <input type="hidden" name="preview_displayvariables" value=""><input type="checkbox" value="1" '.($preview_displayvariables==1?' checked':'').' name="preview_displayvariables" id="displayvariables">';
				echo '  <label for="displayvariables">'.$lang['srv_displayvariables'].'</label>';
				echo '</div>';

				echo '<div class="setting_holder">';
				echo '  <input type="hidden" name="preview_hidecomment" value=""><input type="checkbox" value="1" '.($preview_hidecomment==1?' checked':'').' name="preview_hidecomment" id="hidecomment">';
				echo '  <label for="hidecomment">'.$lang['srv_preview_comments2'].'</label>';
				echo '</div>';

				echo '<div class="setting_holder">';
				echo '  <input type="hidden" name="preview_hide_survey_comment" value=""><input type="checkbox" value="1" '.($preview_hide_survey_comment==1?' checked':'').' name="preview_hide_survey_comment" id="hidesurveycomment">';
				echo '  <label for="hidesurveycomment">'.$lang['srv_preview_hide_survey_comment'].'</label>';
				echo '</div>';
							
				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_preview_survey_comment_showalways'] .':</span>';

				echo '<div class="setting_item">';
				echo '  <input type="radio" name="preview_survey_comment_showalways" value="0" id="preview_survey_comment_showalways_0" ' . ($preview_survey_comment_showalways == 0 ? ' checked' : '') . '/>';
				echo '  <label for="preview_survey_comment_showalways_0">' . $lang['no'] . '</label> ';
				echo '</div>';

				echo '<div class="setting_item">';
				echo '  <input type="radio" name="preview_survey_comment_showalways" value="1" id="preview_survey_comment_showalways_1" ' . ($preview_survey_comment_showalways == 1 ? ' checked' : '') . '/>';
				echo '  <label for="preview_survey_comment_showalways_1">' . $lang['yes'] . '</label> ';			
				echo '</div>';
							
				echo '</div>';

				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_preview_disable_test_insert'] . ':</span>';

				echo '<div class="setting_item">';
				echo '  <input type="radio" name="preview_disable_test_insert" value="1" id="preview_disable_test_insert_1" ' . ($preview_disable_test_insert == 1 ? ' checked' : '') . '/>';
				echo '  <label for="preview_disable_test_insert_1">' . $lang['no'] . '</label> ';			
				echo '</div>';

				echo '<div class="setting_item">';
				echo '  <input type="radio" name="preview_disable_test_insert" value="0" id="preview_disable_test_insert_0" ' . ($preview_disable_test_insert == 0 ? ' checked' : '') . '/>';
				echo '  <label for="preview_disable_test_insert_0">' . $lang['yes'] . '</label> ';
				echo '</div>';
							
				echo '</div>';
				
				echo '</fieldset>';
				
				echo '<fieldset class="comments"><legend>'.$lang['srv_admin_s_comments'].'<span>'.$lang['srv_admin_s_comments_txt'].'</span></legend>';

				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_comments_write'] . ':</span>';

				echo '<div class="setting_item">';
				echo '<select class="dropdown small" name="survey_comment">';
				echo '<option value=""'.($survey_comment==''?' selected':'').'>'.$lang['srv_nihce'].'</option>';
				//echo '<option value="4"'.($survey_comment==4?' selected':'').'>'.$lang['move_all'].'</option>';
				echo '<option value="3" '.($survey_comment==3 ?' selected':'').'>'.$lang['forum_registered'].'</option>';
				echo '<option value="2" '.($survey_comment==2 ?' selected':'').'>'.$lang['forum_clan'].'</option>';
				echo '<option value="1" '.($survey_comment==1 ?' selected':'').'>'.$lang['forum_manager'].'</option>';
				echo '<option value="0" '.($survey_comment=='0' ?' selected':'').'>'.$lang['forum_admin'].'</option>';
				echo '</select>';
				echo '</div>';
							
				echo '</div>';

				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_comments_view'] . ':</span>';

				echo '<div class="setting_item">';
				echo '<select class="dropdown small" name="survey_comment_viewadminonly">';
				//echo '<option value="4"'.($survey_comment_viewadminonly==4?' selected':'').'>'.$lang['move_all'].'</option>';
				echo '<option value="3" '.($survey_comment_viewadminonly==3 ?' selected':'').'>'.$lang['forum_registered'].'</option>';
				echo '<option value="2" '.($survey_comment_viewadminonly==2 ?' selected':'').'>'.$lang['forum_clan'].'</option>';
				echo '<option value="1" '.($survey_comment_viewadminonly==1 ?' selected':'').'>'.$lang['forum_manager'].'</option>';
				echo '<option value="0" '.($survey_comment_viewadminonly=='0' ?' selected':'').'>'.$lang['forum_admin'].'</option>';
				echo '</select> ';
				echo '</div>';
				echo '<div class="setting_item">';
				echo '<input type="hidden" name="survey_comment_viewauthor" value=""><input type="checkbox" id="survey_comment_viewauthor" name="survey_comment_viewauthor" value="1" '.($survey_comment_viewauthor==1?' checked':'').' />';
				echo '<label for="survey_comment_viewauthor">'.$lang['srv_comments_viewauthor'].'</label>';
				echo '</div>';
							
				echo '</div>';

				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_survey_comment_show'] . ':</span>';

				echo '<div class="setting_item">';
				echo '<input type="radio" name="survey_comment_showalways" value="0" id="survey_comment_showalways_0" ' . ($survey_comment_showalways == 0 ? ' checked' : '') . '/>';
				echo '<label for="survey_comment_showalways_0">' . $lang['no'] . '</label> ';
				echo '</div>';
				echo '<div class="setting_item">';
				echo '<input type="radio" name="survey_comment_showalways" value="1" id="survey_comment_showalways_1" ' . ($survey_comment_showalways == 1 ? ' checked' : '') . '/>';
				echo '<label for="survey_comment_showalways_1">' . $lang['yes'] . '</label> ';
				echo '</div>';
							
				echo '</div>';

				echo '</fieldset>';
				
				echo '<fieldset class="comments">';
				echo '<legend>' . $lang['srv_admin_q_notes'] . '<span>'.$lang['srv_admin_q_notes_txt'].'</span></legend>';

				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_q_note_view'] . ':</span>';

				echo '<div class="setting_item">';
				echo '<select class="dropdown small" name="question_note_view">';
				echo '<option value=""'.($question_note_view==''?' selected':'').'>'.$lang['move_all'].'</option>';
				echo '<option value="3" '.($question_note_view==3 ?' selected':'').'>'.$lang['forum_registered'].'</option>';
				echo '<option value="2" '.($question_note_view==2 ?' selected':'').'>'.$lang['forum_clan'].'</option>';
				echo '<option value="1" '.($question_note_view==1 ?' selected':'').'>'.$lang['forum_manager'].'</option>';
				echo '<option value="0" '.($question_note_view=='0' ?' selected':'').'>'.$lang['forum_admin'].'</option>';
				echo '</select>';
				echo '</div>';
							
				echo '</div>';

				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_q_note_write'] . ':</span>';

				echo '<div class="setting_item">';
				echo '<select class="dropdown small" name="question_note_write">';
				echo '<option value=""'.($question_note_write==''?' selected':'').'>'.$lang['move_all'].'</option>';
				echo '<option value="3" '.($question_note_write==3 ?' selected':'').'>'.$lang['forum_registered'].'</option>';
				echo '<option value="2" '.($question_note_write==2 ?' selected':'').'>'.$lang['forum_clan'].'</option>';
				echo '<option value="1" '.($question_note_write==1 ?' selected':'').'>'.$lang['forum_manager'].'</option>';
				echo '<option value="0" '.($question_note_write=='0' ?' selected':'').'>'.$lang['forum_admin'].'</option>';
				echo '</select>';
				echo '</div>';
							
				echo '</div>';
				
				echo '</fieldset>';
			
			
				echo '<fieldset class="comments">';
				echo '<legend>' . $lang['srv_admin_q_comments'] . '<span>'.$lang['srv_admin_q_comments_txt'].'</span></legend>';


				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_comments_write'] . ':</span>';

				echo '<div class="setting_item">';
				echo '<select class="dropdown small" name="question_comment">';
				echo '<option value=""'.($question_comment==''?' selected':'').'>'.$lang['srv_nihce'].'</option>';
				echo '<option value="4"'.($question_comment==4?' selected':'').'>'.$lang['move_all'].'</option>';
				echo '<option value="3" '.($question_comment==3 ?' selected':'').'>'.$lang['forum_registered'].'</option>';
				echo '<option value="2" '.($question_comment==2 ?' selected':'').'>'.$lang['forum_clan'].'</option>';
				echo '<option value="1" '.($question_comment==1 ?' selected':'').'>'.$lang['forum_manager'].'</option>';
				echo '<option value="0" '.($question_comment=='0' ?' selected':'').'>'.$lang['forum_admin'].'</option>';
				echo '</select>';
				echo '</div>';
							
				echo '</div>';

				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_comments_view'] . ':</span>';

				echo '<div class="setting_item">';
				echo '<select class="dropdown small" name="question_comment_viewadminonly">';
				echo '<option value="4"'.($question_comment_viewadminonly==4 || $question_comment_viewadminonly==''?' selected':'').'>'.$lang['move_all'].'</option>';
				echo '<option value="3" '.($question_comment_viewadminonly==3 ?' selected':'').'>'.$lang['forum_registered'].'</option>';
				echo '<option value="2" '.($question_comment_viewadminonly==2 ?' selected':'').'>'.$lang['forum_clan'].'</option>';
				echo '<option value="1" '.($question_comment_viewadminonly==1 ?' selected':'').'>'.$lang['forum_manager'].'</option>';
				echo '<option value="0" '.($question_comment_viewadminonly=='0' ?' selected':'').'>'.$lang['forum_admin'].'</option>';
				echo '</select>';
				echo '</div>';
				echo '<div class="setting_item">';
				echo '<input type="hidden" name="question_comment_viewauthor" value=""><input type="checkbox" id="question_comment_viewauthor" name="question_comment_viewauthor" value="1" '.($question_comment_viewauthor==1?' checked':'').' />';
				echo '<label for="question_comment_viewauthor">'.$lang['srv_comments_viewauthor'].'</label>';
				echo '</div>';
							
				echo '</div>';

				echo '</fieldset>';

				echo '<fieldset class="comments">';
				echo '<legend>' . $lang['srv_comments_respondents'] . '</legend>';

				echo '<p class="bottom16">'.$lang['srv_comments_respondents_txt'].'</p>';

				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_q_comment'] . ':</span>';

				echo '<div class="setting_item">';
				echo '<input type="radio" name="question_resp_comment" value="0" id="question_resp_comment_0" ' . ($question_resp_comment == 0 ? ' checked' : '') . '/>';
				echo '<label for="question_resp_comment_0">' . $lang['no'] . '</label> ';
				echo '</div>';
				echo '<div class="setting_item">';
				echo '<input type="radio" name="question_resp_comment" value="1" id="question_resp_comment_1" ' . ($question_resp_comment == 1 ? ' checked' : '') . '/>';
				echo '<label for="question_resp_comment_1">' . $lang['yes'] . '</label> ';
				echo '</div>';
							
				echo '</div>';

				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_comments_view'] . ':</span>';

				echo '<div class="setting_item">';
				echo '<select class="dropdown small" name="question_resp_comment_viewadminonly">';
				//echo '<option value=""'.($question_resp_comment_viewadminonly==''?' selected':'').'>'.$lang['srv_nihce'].'</option>';
				echo '<option value="4"'.($question_resp_comment_viewadminonly==4 || $question_resp_comment_viewadminonly==''?' selected':'').'>'.$lang['move_all'].'</option>';
				echo '<option value="3" '.($question_resp_comment_viewadminonly==3 ?' selected':'').'>'.$lang['forum_registered'].'</option>';
				echo '<option value="2" '.($question_resp_comment_viewadminonly==2 ?' selected':'').'>'.$lang['forum_clan'].'</option>';
				echo '<option value="1" '.($question_resp_comment_viewadminonly==1 ?' selected':'').'>'.$lang['forum_manager'].'</option>';
				echo '<option value="0" '.($question_resp_comment_viewadminonly=='0' ?' selected':'').'>'.$lang['forum_admin'].'</option>';
				echo '</select>';
				echo '</div>';
							
				echo '</div>';


				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_comments_show_open'] . ':</span>';

				echo '<div class="setting_item">';
				echo '<input type="radio" name="question_resp_comment_show_open" value="" id="question_resp_comment_show_open_0" ' . ($question_resp_comment_show_open == '' ? ' checked' : '') . '/>';
				echo '<label for="question_resp_comment_show_open_0">' . $lang['forma_settings_open'] . '</label> ';
				echo '</div>';
				echo '<div class="setting_item">';
				echo '<input type="radio" name="question_resp_comment_show_open" value="1" id="question_resp_comment_show_open_1" ' . ($question_resp_comment_show_open == '1' ? ' checked' : '') . '/>';
				echo '<label for="question_resp_comment_show_open_1">' . $lang['forma_settings_closed'] . '</label> ';
				echo '</div>';
							
				echo '</div>';

				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' .$lang['text'].' "'. $lang['srv_question_respondent_comment'] . '":</span>';

				echo '<div class="setting_item">';
				echo '<input type="text" class="text small" name="srvlang_srv_question_respondent_comment" value="'.$srvlang_srv_question_respondent_comment.'" style="width:300px">';
				echo '<input type="hidden" name="extra_translations" value="1">';echo '</div>';
							
				echo '</div>';

				echo '<p class="bottom16">'.$lang['srv_resp_s_comments_txt'].'</p>';

				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_comments_write'] . ':</span>';

				echo '<div class="setting_item">';
				echo '<select class="dropdown small" name="survey_comment_resp">';
				echo '<option value=""'.($survey_comment_resp==''?' selected':'').'>'.$lang['srv_nihce'].'</option>';
				echo '<option value="4"'.($survey_comment_resp==4?' selected':'').'>'.$lang['move_all'].'</option>';
				echo '<option value="3" '.($survey_comment_resp==3 ?' selected':'').'>'.$lang['forum_registered'].'</option>';
				echo '<option value="2" '.($survey_comment_resp==2 ?' selected':'').'>'.$lang['forum_clan'].'</option>';
				echo '<option value="1" '.($survey_comment_resp==1 ?' selected':'').'>'.$lang['forum_manager'].'</option>';
				echo '<option value="0" '.($survey_comment_resp=='0' ?' selected':'').'>'.$lang['forum_admin'].'</option>';
				echo '</select>';
				echo '</div>';
							
				echo '</div>';

				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_comments_view'] . ':</span>';

				echo '<div class="setting_item">';
				echo '<select class="dropdown small" nname="survey_comment_viewadminonly_resp">';
				echo '<option value="4"'.($survey_comment_viewadminonly_resp==4?' selected':'').'>'.$lang['move_all'].'</option>';
				echo '<option value="3" '.($survey_comment_viewadminonly_resp==3 ?' selected':'').'>'.$lang['forum_registered'].'</option>';
				echo '<option value="2" '.($survey_comment_viewadminonly_resp==2 ?' selected':'').'>'.$lang['forum_clan'].'</option>';
				echo '<option value="1" '.($survey_comment_viewadminonly_resp==1 ?' selected':'').'>'.$lang['forum_manager'].'</option>';
				echo '<option value="0" '.($survey_comment_viewadminonly_resp=='0' ?' selected':'').'>'.$lang['forum_admin'].'</option>';
				echo '</select> ';
				echo '</div>';
				echo '<div class="setting_item">';
				echo '<input type="hidden" name="survey_comment_viewauthor_resp" value=""><input type="checkbox" id="survey_comment_viewauthor_resp" name="survey_comment_viewauthor_resp" value="1" '.($survey_comment_viewauthor_resp==1?' checked':'').' />';
				echo '<label for="survey_comment_viewauthor_resp">'.$lang['srv_comments_viewauthor'].'</label>';
				echo '</div>';
							
				echo '</div>';

				echo '<p class="semi-bold bottom16">'.$lang['srv_extra_settings'].'</p>';

				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_q_inicialke'] . ':</span>';

				echo '<div class="setting_item">';
				echo '<input type="radio" name="question_resp_comment_inicialke" value="0" id="question_resp_comment_inicialke_0" ' . ($question_resp_comment_inicialke == 0 ? ' checked' : '') . '/>';
				echo '<label for="question_resp_comment_inicialke_0">' . $lang['no'] . '</label> ';
				echo '</div>';
				echo '<div class="setting_item">';
				echo '<input type="radio" name="question_resp_comment_inicialke" value="1" id="question_resp_comment_inicialke_1" ' . ($question_resp_comment_inicialke == 1 ? ' checked' : '') . '/>';
				echo '<label for="question_resp_comment_inicialke_1">' . $lang['yes'] . '</label> ';
				echo '</div>';
							
				echo '</div>';

				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_q_inicialke_alert'] . ':</span>';

				echo '<div class="setting_item">';
				echo '<input type="radio" name="question_resp_comment_inicialke_alert" value="0" id="question_resp_comment_inicialke_alert_0" ' . ($question_resp_comment_inicialke_alert == 0 ? ' checked' : '') . '/>';
				echo '<label for="question_resp_comment_inicialke_alert_0">' . $lang['srv_reminder_off2'] . '</label> ';
				echo '</div>';
				echo '<div class="setting_item">';
				echo '<input type="radio" name="question_resp_comment_inicialke_alert" value="1" id="question_resp_comment_inicialke_alert_1" ' . ($question_resp_comment_inicialke_alert == 1 ? ' checked' : '') . '/>';
				echo '<label for="question_resp_comment_inicialke_alert_1">' . $lang['srv_reminder_soft2'] . '</label> ';
				echo '</div>';
				echo '<div class="setting_item">';
				echo '<input type="radio" name="question_resp_comment_inicialke_alert" value="2" id="question_resp_comment_inicialke_alert_2" ' . ($question_resp_comment_inicialke_alert == 2 ? ' checked' : '') . '/>';
				echo '<label for="question_resp_comment_inicialke_alert_2">' . $lang['srv_reminder_hard2'] . '</label> ';
				echo '</div>';
							
				echo '</div>';

				echo '</fieldset>';
				
				echo '<fieldset class="comments">';
				echo '<legend>' . $lang['srv_settings_komentarji'] . '</legend>';

				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['orderby'] . ':</span>';

				echo '<div class="setting_item">';
				echo '<input type="radio" name="sortpostorder" value="0" id="sortpostorder_0" ' . ($sortpostorder == 0 ? ' checked' : '') . '/>';
				echo '<label for="sortpostorder_0">' . $lang['forum_asc'] . '</label> ';
				echo '</div>';
				echo '<div class="setting_item">';
				echo '<input type="radio" name="sortpostorder" value="1" id="sortpostorder_1" ' . ($sortpostorder == 1 ? ' checked' : '') . '/>';
				echo '<label for="sortpostorder_1">' . $lang['forum_desc'] . '</label> ';
				echo '</div>';
							
				echo '</div>';

				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_add_comment'] . ':</span>';

				echo '<div class="setting_item">';
				echo '<input type="radio" name="addfieldposition" value="0" id="addfieldposition_0" ' . ($addfieldposition == 0 ? ' checked' : '') . '/>';
				echo '<label for="addfieldposition_0">' . $lang['srv_polozaj_bottom'] . '</label> ';
				echo '</div>';
				echo '<div class="setting_item">';
				echo '<input type="radio" name="addfieldposition" value="1" id="addfieldposition_1" ' . ($addfieldposition == 1 ? ' checked' : '') . '/>';
				echo '<label for="addfieldposition_1">' . $lang['srv_polozaj_top'] . '</label> ';
				echo '</div>';
							
				echo '</div>';

				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_comments_marks'] . ':</span>';

				echo '<div class="setting_item">';
				echo '<input type="radio" name="commentmarks" value="0" id="commentmarks_0" ' . ($commentmarks == 0 ? ' checked' : '') . '/>';
				echo '<label for="commentmarks_0">' . $lang['srv_comments_marks_0'] . '</label> ';
				echo '</div>';
				echo '<div class="setting_item">';
				echo '<input type="radio" name="commentmarks" value="1" id="commentmarks_1" ' . ($commentmarks == 1 ? ' checked' : '') . '/>';
				echo '<label for="commentmarks_1">' . $lang['srv_comments_marks_1'] . '</label> ';
				echo '</div>';
							
				echo '</div>';

				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_comments_marks_who'] . ':</span>';

				echo '<div class="setting_item">';
				echo '<input type="radio" name="commentmarks_who" value="0" id="commentmarks_who_0" ' . ($commentmarks_who == 0 ? ' checked' : '') . '/>';
				echo '<label for="commentmarks_who_0">' . $lang['srv_comments_marks_who_1'] . '</label> ';
				echo '</div>';
				echo '<div class="setting_item">';
				echo '<input type="radio" name="commentmarks_who" value="1" id="commentmarks_who_1" ' . ($commentmarks_who == 1 ? ' checked' : '') . '/>';
				echo '<label for="commentmarks_who_1">' . $lang['srv_comments_marks_who_0'] . '</label> ';			
				echo '</div>';
							
				echo '</div>';

				echo '<div class="setting_holder horizontal">';
				echo '<span class="setting_title">' . $lang['srv_comment_history'] . ':</span>';

				echo '<div class="setting_item">';
				echo '<select class="dropdown small" name="comment_history">';
				echo '<option value="0" '.($comment_history=='0' || $comment_history=='' ? ' selected':'').'>'.$lang['srv_comment_history_1'].'</option>';
				echo '<option value="1" '.($comment_history==1 ?' selected':'').'>'.$lang['srv_comment_history_0'].'</option>';
				echo '<option value="2" '.($comment_history==2 ?' selected':'').'>'.$lang['srv_comment_history_2'].'</option>';
				echo '</select>';
				echo '</div>';
							
				echo '</div>';

				echo '</fieldset>';
				
				echo '<p class="delete_comments"><a class="noline" href="#" onClick="delete_test_data();"><span class="faicon trash empty link-right"></span>'.$lang['srv_delete_comments3'].'</a> ('.$lang['srv_delete_comments_txt2'].')</p>';
				
				echo '</div>';	

			}
		}

		/*Dostop*/
		if ($_GET['a'] == 'dostop') {

			// tukaj bom dodal še kontrolo na Avtorja ankete, tako da avtor lahko vedno spreminja dostop (gorazd,1.9.2009)
			$stringDostopAvtor = "SELECT count(*) as isAvtor FROM srv_dostop WHERE ank_id = '" . $this->anketa . "' AND (uid='" . $global_user_id . "' OR uid IN (SELECT user FROM srv_dostop_manage WHERE manager='$global_user_id' ))";
			$sqlDostopAvtor = sisplet_query($stringDostopAvtor);
			$rowDostopAvtor = mysqli_fetch_assoc($sqlDostopAvtor);
			
			if ($admin_type <= $row['dostop'] || $rowDostopAvtor['isAvtor'] > 0) {
				
				echo '<fieldset><legend>' . $lang['srv_dostop_users'] . '' . Help :: display('srv_dostop_users'). '</legend>';

                //Dodajanje dostopa - prikaži vse za admine - potrebuje prenovo
				/**if($admin_type == 0 || $admin_type == 1){
					echo '<div class="setting_holder">';
				    echo '<span id="dostop_active_show_1"><a href="#" onClick="dostopActiveShowAll(\'true\'); return false;">'.$lang['srv_dostop_show_all'].'</a></span>';
				    echo '<span id="dostop_active_show_2" class="displayNone"><a href="#" onClick="dostopActiveShowAll(\'false\'); return false;">'.$lang['srv_dostop_hide_all'].'</a></span>';
					echo '</div>';
				}**/

				echo '<div id="dostop_users_list">';
				$this->display_dostop_users(0);
				echo '</div>';

				echo '<div class="button_holder below">';
				echo '<button class="medium blue" onclick="document.settingsanketa_' . $row['id'] . '.submit(); return false;">'.$lang['edit1337'].'</button>';
				echo '</div>';
				
				echo '</fieldset>';
							
				
				// Dodajanje uproabnikov preko e-maila
				echo '<fieldset><legend>'.$lang['srv_dostop_addusers'].'</legend>';
				echo '<div id="addmail">';
                     
                $this->display_add_survey_dostop();

				echo '</div>';			
				echo '</fieldset>';	
			}
		}

		/*Jezik*/
		if ($_GET['a'] == 'jezik') {
			global $admin_lang;
			
			$lang_admin = $row['lang_admin'];
			$lang_resp = $row['lang_resp'];
			
			$admin_lang = $lang;
			
			// ce ni default jezik, damo za osnovnega, default jezik
			global $resp_lang;
			$file = '../../lang/'.$row['lang_resp'].'.php';
			include($file);
			$resp_lang = $lang;
			//$lang_admin = $lang_resp;
			
			// nazaj na administrativnega
			$file = '../../lang/'.$lang_admin.'.php';
			include($file);
			
			
			echo '<fieldset><legend>'.$lang['srv_extra_translations'].' ';
			
			$lang_id = '';
			if ($row['multilang'] == 1) {
				echo ' <select class="dropdown medium w130 inline" name="lang_id" onchange="window.location.href=\'index.php?anketa='.$this->anketa.'&a=jezik&lang_id=\'+this.value;">';
				
				$lang_id = (int)$_GET['lang_id'];
				if ($lang_id > 0)
					$lang_resp = $lang_id;
				
				$p = new Prevajanje($this->anketa);
				$p->dostop();
				$langs = $p->get_all_translation_langs();
				
				echo '<option value="" '.($lang_id==''?' selected':'').'>'.$resp_lang['language'].'</option>';
				
				foreach ($langs  AS $k => $l) {
					echo '<option value="'.$k.'" '.($lang_id==$k?' selected':'').'>'.$l.'</option>';
				}
				
				echo '</select>';
			}
			
			echo '</legend>';
			
			
			echo '<div class="standardne_besede">';
			echo '<input type="hidden" name="extra_translations" value="1" />';			// da vemo, da nastavljamo ta besedila

			// Tabela standardnih besed
			echo '<table id="standard_words_table">';
			echo '<tr>';
			
			echo '<th>'.($lang_id > 0 ? $lang['srv_language_respons_1'].': '.$resp_lang['language']:$lang['srv_language_admin'].': '.$lang['language']).'</th>';
			
			echo '<th>'.$lang['srv_std_second'].': ';
			$file = '../../lang/'.$lang_resp.'.php';
			include($file);
			echo $lang['language'].'</th>';

			// nazaj na administrativni jezik
			$file = '../../lang/'.$lang_admin.'.php';
			include($file);
			
			echo '<th><div class="standardna-beseda-th">'.$lang['srv_std_translation'].'<div class="icon_holder"><a href="'.SurveyInfo::getSurveyLink().'&preview=on&language='.$lang_resp.'" target="_blank"><span class="faicon preview"></span></a></div></div></th>';
			echo '</tr>';
			
			// jezik nastavimo na nastavitev za respondente, ker ta text dejansko nastavljamo
			$file = '../../lang/'.$lang_resp.'.php';
			include($file);

			// Pri gumbih ne prikazujemo editorja
			$this->extra_translation('srv_nextpage');
			$this->extra_translation('srv_nextpage_uvod');
			$this->extra_translation('srv_prevpage');
			$this->extra_translation('srv_lastpage');
			$this->extra_translation('srv_forma_send');
			$this->extra_translation('srv_potrdi');
			$this->extra_translation('srv_konec');
			
			$this->extra_translation('srv_remind_sum_hard', 1);
			$this->extra_translation('srv_remind_sum_soft', 1);
			$this->extra_translation('srv_remind_num_hard', 1);
			$this->extra_translation('srv_remind_num_soft', 1);
			$this->extra_translation('srv_remind_hard', 1);
			$this->extra_translation('srv_remind_soft', 1);
			$this->extra_translation('srv_remind_hard_-99', 1);
			$this->extra_translation('srv_remind_soft_-99', 1);
			$this->extra_translation('srv_remind_hard_-98', 1);
			$this->extra_translation('srv_remind_soft_-98', 1);
			$this->extra_translation('srv_remind_hard_-97', 1);
			$this->extra_translation('srv_remind_soft_-97', 1);
			$this->extra_translation('srv_remind_hard_multi', 1);
			$this->extra_translation('srv_remind_soft_multi', 1);
			$this->extra_translation('srv_remind_captcha_hard', 1);
			$this->extra_translation('srv_remind_captcha_soft', 1);
			$this->extra_translation('srv_remind_email_hard', 1);
			$this->extra_translation('srv_remind_email_soft', 1);
			$this->extra_translation('srv_alert_number_exists', 1);
			$this->extra_translation('srv_alert_number_toobig', 1);
			
			$this->extra_translation('srv_ranking_avaliable_categories', 1);
			$this->extra_translation('srv_ranking_ranked_categories', 1);
			$this->extra_translation('srv_question_respondent_comment', 1);
			$this->extra_translation('srv_continue_later', 1);
			$this->extra_translation('srv_continue_later_txt', 1);
			$this->extra_translation('srv_continue_later_email', 1);
			$this->extra_translation('srv_dropdown_select', 1);
			$this->extra_translation('srv_wrongcode', 1);
			$this->extra_translation('user_bye_textA', 1);
			
			$this->extra_translation('srv_survey_non_active', 1);
			$this->extra_translation('srv_survey_deleted', 1);
			$this->extra_translation('srv_survey_non_active_notActivated', 1);
			$this->extra_translation('srv_survey_non_active_notStarted', 1);
			$this->extra_translation('srv_survey_non_active_expired', 1);
			$this->extra_translation('srv_survey_non_active_voteLimit', 1);

			// nastavimo jezik nazaj
			$file = '../../lang/'.$lang_admin.'.php';
			include($file);

			echo '</table>';

			echo '<div>';

			// Gumb za ponastavitev prevoda v bazi pobriše že nastavljene prevode za izbran jezik
			echo '<div class="button_holder top16 bottom16">';
			echo '	<button class="medium white-black" type="button" onclick="ponastavi_prevod(\''.$lang_id.'\')">'.$lang['reset_translation'].'</button>';
			echo '</div>';
			
			// Ta del se v 2. fazi zamenja z gumbom "Večjezične ankete"
			echo '<div class="setting_holder">';

			echo '<p class="bottom8">'.$lang['srv_language_link2'].':';
			// Preverimo, ce je funkcionalnost v paketu, ki ga ima uporabnik
            $userAccess = UserAccess::getInstance($global_user_id);
            if($userAccess->checkUserAccess($what='prevajanje')){
                echo '&nbsp;<a href="index.php?anketa='.$this->anketa.'&a=prevajanje" title="'.$lang['srv_language_link'].'"><span class="bold">'.$lang['srv_language_link'].'</span></a></p>';
            }
            else{            
                echo '&nbsp;<a href="#" onClick=popupUserAccess(\'prevajanje\'); return false;" title="'.$lang['srv_language_link'].'" class="user_access_locked"><span class="bold">'.$lang['srv_language_link'].'</span></a></p>';
            }			
			echo '<p>'.$lang['srv_language_mySurveys'].'.</p>';

			echo '</div>';

			echo '</fieldset>';

			echo '<div class="button_holder">';
			echo '<button class="medium white-blue" onClick="window.location='.$preklici_url.'; return false;">'.$lang['edit1338'].'</button>';             
			echo '<button class="medium blue" onclick="document.settingsanketa_' . $row['id'] . '.submit(); return false;">'.$lang['edit1337'].'</button>';
			echo '</div>';
			
		}
		/*Forma*/
		if ($_GET['a'] == 'forma') {				
		}

		/*Nastavitve prikaza za mobilnike*/
		if ($_GET['a'] == 'mobile_settings') {

			SurveySetting::getInstance()->Init($this->anketa);		
			echo '<fieldset>';
			
			echo '<legend>'.$lang['srv_mobile_settings_title'].Help::display('srv_mobilne_tabele').'</legend>';
		
			// Prikaz slik pri mobilnikih (default da)
			$mobile_friendly = SurveySetting::getInstance()->getSurveyMiscSetting('mobile_friendly');

			echo '<div class="setting_holder '.($mobile_friendly!=='1' ? 'bottom0':'').'" id="mobile_osnovno">';

            echo '<input type="hidden" name="mobile_friendly" value="0">';
            echo '<input type="checkbox" id="mobile_friendly" name="mobile_friendly" value="1" '.($mobile_friendly == 1 ? ' checked="checked"' : '').' onChange="$(\'#mobile_settings_other\').toggleClass(\'displayNone\');$(\'#mobile_osnovno\').toggleClass(\'bottom0\');">';
            echo '<label for="mobile_friendly">'. $lang['srv_settings_mobile_friendly'] .'</label> ';

			echo '</div>';

			$display = ($mobile_friendly == 1) ? '' : 'displayNone';
			
			echo '<div class="'.$display.'" id="mobile_settings_other">';
			
			// Prikaz slik pri mobilnikih (default da)
			$hide_mobile_img = SurveySetting::getInstance()->getSurveyMiscSetting('hide_mobile_img');
			echo '<div class="setting_holder">';

            echo '<input type="hidden" name="hide_mobile_img" value="1">';
            echo '<input type="checkbox" id="hide_mobile_img" name="hide_mobile_img" value="0" '.($hide_mobile_img!= 1 ? ' checked="checked"' : '').'>';
            echo '<label for="hide_mobile_img">'. $lang['srv_settings_mobile_img'] .'</label> ';

			echo '</div>';

			// Prilagoditev tabel pri mobilnikih
			/*$mobile_tables = SurveySetting::getInstance()->getSurveyMiscSetting('mobile_tables');
			echo '<div class="setting_holder">';

			echo '<span class="setting_title" >'.$lang['srv_settings_mobile_tables'].':</span>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="mobile_tables" id="mobile_tables_1" '.($mobile_tables==='1'?' checked':'').' value="1">';
			echo '<label for="mobile_tables_1">'.$lang['yes'].'</label> ';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="mobile_tables" id="mobile_tables_2" '.($mobile_tables==='2'?' checked':'').' value="2">';
			echo '<label for="mobile_tables_2">'.$lang['srv_settings_mobile_tables_slide'].'</label> ';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="mobile_tables" id="mobile_tables_0" '.($mobile_tables==='0'?' checked':'').' value="0">';
			echo '<label for="mobile_tables_0">'.$lang['no'].'</label> ';			
			echo '</div>';

			echo '</div>';*/

			echo '</div>';			

			echo '</fieldset>';
		}

        /*Nastavitve prikaza za tabele*/
		if ($_GET['a'] == 'table_settings') {

			SurveySetting::getInstance()->Init($this->anketa);	

			echo '<fieldset>';
			
			echo '<legend>'.$lang['srv_table_settings_title'].'</legend>';
		

            // Prilagoditev tabel na pc-ju
			$pc_tables = SurveySetting::getInstance()->getSurveyMiscSetting('pc_tables');

			echo '<div class="setting_holder">';

			echo '<span class="setting_title" >'.$lang['srv_table_settings_pc_tables'].':</span>';

            echo '<div class="setting_item">';
			echo '<input type="radio" name="pc_tables" id="pc_tables_0" '.($pc_tables==='0'?' checked':'').' value="0">';
			echo '<label for="pc_tables_0">'.$lang['srv_settings_mobile_tables_0'].'</label> ';			
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="pc_tables" id="pc_tables_1" '.($pc_tables==='1'?' checked':'').' value="1">';
			echo '<label for="pc_tables_1">'.$lang['srv_settings_mobile_tables_1'].'</label> ';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="pc_tables" id="pc_tables_2" '.($pc_tables==='2'?' checked':'').' value="2">';
			echo '<label for="pc_tables_2">'.$lang['srv_settings_mobile_tables_2'].'</label> ';
			echo '</div>';
            echo '<div class="setting_item">';
			echo '<input type="radio" name="pc_tables" id="pc_tables_3" '.($pc_tables==='3'?' checked':'').' value="3">';
			echo '<label for="pc_tables_3">'.$lang['srv_settings_mobile_tables_3'].'</label> ';
			echo '</div>';
			
			echo '</div>';


			// Prilagoditev tabel pri mobilnikih
			$mobile_tables = SurveySetting::getInstance()->getSurveyMiscSetting('mobile_tables');

			echo '<div class="setting_holder">';

			echo '<span class="setting_title" >'.$lang['srv_table_settings_mobile_tables'].':</span>';
            
            echo '<div class="setting_item">';
			echo '<input type="radio" name="mobile_tables" id="mobile_tables_0" '.($mobile_tables==='0'?' checked':'').' value="0">';
			echo '<label for="mobile_tables_0">'.$lang['srv_settings_mobile_tables_0'].'</label> ';			
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="mobile_tables" id="mobile_tables_1" '.($mobile_tables==='1'?' checked':'').' value="1">';
			echo '<label for="mobile_tables_1">'.$lang['srv_settings_mobile_tables_1'].'</label> ';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="mobile_tables" id="mobile_tables_2" '.($mobile_tables==='2'?' checked':'').' value="2">';
			echo '<label for="mobile_tables_2">'.$lang['srv_settings_mobile_tables_2'].'</label> ';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="mobile_tables" id="mobile_tables_3" '.($mobile_tables==='3'?' checked':'').' value="3">';
			echo '<label for="mobile_tables_3">'.$lang['srv_settings_mobile_tables_3'].'</label> ';
			echo '</div>';

			echo '</div>';


			echo '</fieldset>';
		}
		
		/*Metapodatki (Parapodatki)*/
		if ($_GET['a'] == 'metadata') {

			SurveySetting::getInstance()->Init($this->anketa);
			$ip = SurveySetting::getInstance()->getSurveyMiscSetting('survey_ip');
			$ip_show = SurveySetting::getInstance()->getSurveyMiscSetting('survey_show_ip');
			$browser = SurveySetting::getInstance()->getSurveyMiscSetting('survey_browser');
			$referal = SurveySetting::getInstance()->getSurveyMiscSetting('survey_referal');
			$date = SurveySetting::getInstance()->getSurveyMiscSetting('survey_date');

			$ip = $ip == '' ? 0 : $ip;
			$ip_show = $ip_show == '' ? 0 : $ip_show;
			$browser = $browser == '' ? 0 : $browser;
			$referal = $referal == '' ? 0 : $referal;
			$date = $date == '' ? 0 : $date;


			echo '<fieldset>';
			echo '<legend>'.$lang['srv_sledenje'].'</legend>';
				
            // Preverimo ce je vklopljen modul za volitve - potem ne pustimo nobenih preklopov
            $voting_disabled = '';
            if(SurveyInfo::getInstance()->checkSurveyModule('voting')){
                $voting_disabled = ' disabled';

                echo '<p class="red">'.$lang['srv_voting_warning_paradata'].'</p><br>';	
            }
			
			echo '<div class="setting_holder">';
			echo '<p>'.$lang['srv_metadata_desc'].'</p>';
			echo '</div>';
			
			echo '<div class="setting_holder">';
			echo '<input type="hidden" name="survey_browser" value="1">';
            echo '<input type="checkbox" id="survey_browser" name="survey_browser" value="0" '.($browser==0? ' checked':'').' '.$voting_disabled.'>';
            echo '<label for="survey_browser">'. $lang['srv_sledenje_browser'] .'</label> ';
			echo '</div>';

			echo '<div class="setting_holder">';
			echo '<input type="hidden" name="survey_referal" value="1">';
            echo '<input type="checkbox" id="survey_referal" name="survey_referal" value="0" '.($referal==0? ' checked':'').' '.$voting_disabled.'>';
            echo '<label for="survey_referal">'. $lang['srv_sledenje_referal'] .'</label> ';
			echo '</div>';

			echo '<div class="setting_holder">';
			echo '<input type="hidden" name="survey_date" value="1">';
            echo '<input type="checkbox" id="survey_date" name="survey_date" value="0" '.($date==0? ' checked':'').' '.$voting_disabled.'>';
            echo '<label for="survey_date">'. $lang['srv_sledenje_date'] .'</label> ';
			echo '</div>';

			echo '</fieldset>';
	
			echo '<fieldset>';

			echo '<legend>'.$lang['srv_sledenje_ip_title'].'</legend>';

			//Shranjuj IP naslov respondenta
			echo '<div class="setting_holder '.(($ip == 0) ? '' : ' bottom0').'" id="save_IP">';
			echo '<input type="hidden" name="survey_ip" value="1">';
            echo '<input type="checkbox" id="survey_ip" name="survey_ip" value="0" '.($ip==0? ' checked':'').' '.$voting_disabled.' onChange="toggleParapodatki();">';
            echo '<label for="survey_ip">'. $lang['srv_sledenje_ip'] .'</label> ';
			echo '</div>';

			echo '<div class=" '.(($ip == 1 && $ip_show == 0) ? 'displayNone' : '').'" id="save_IP_warning">';
			echo '<p class="red">'.$lang['srv_sledenje_ip_alert'].'</p><br>';
			echo '</div>';

			//Poveži IP v bazi s podatki
			if ($admin_type == 0 || $admin_type == 1){
				echo '<div class="'.(($ip == 0) ? '' : ' displayNone').'" id="connect_IP">';

				echo '<div class="setting_holder">';
				echo '<input type="hidden" name="survey_show_ip" value="0">';
				echo '<input type="checkbox" id="survey_show_ip" name="survey_show_ip" value="1" '.($ip_show==1? ' checked':'').' '.$voting_disabled.' onChange="toggleParapodatki();">';
				echo '<label for="survey_show_ip">'. $lang['srv_show_ip'] .'</label> ';
				echo '</div>';

				echo '<div class=" '.(($ip_show == 1) ? '' : ' displayNone').'" id="connect_IP_warning">';
				echo '<p class="red">'.$lang['srv_show_ip_alert'].'</p>';
				echo '</div>';

				echo '</div>';
			}
	
			echo '</fieldset>';
				
			
			// Povezovanje identifikatorjev s podatki - samo za admine in ce so vklopljena email vabila
			if ($admin_type == 0 && SurveyInfo::getInstance()->checkSurveyModule('email')) {
				echo '<fieldset>';
				echo '<legend>'.$lang['srv_sledenje_identifikatorji_title'].' '.Help::display('srv_email_with_data').'</legend>';

				echo '<div class="setting_holder">';
				echo '<span class="setting_title">'.$lang['srv_sledenje_identifikatorji'].':</span>';

				echo '<div class="setting_item">';
				echo '<input type="radio" name="show_email" id="show_email_0" value="0"'.($row['show_email']==0?' checked':'').' '.$voting_disabled.' onClick="toggleParapodatki();">';
                echo '<label for="show_email_0">'.$lang['no'].'</label>';
				echo '</div>';
				echo '<div class="setting_item">';
				echo '<input type="radio" name="show_email" id="show_email_1" value="1"'.($row['show_email']==1?' checked':'').' '.$voting_disabled.' onClick="toggleParapodatki();">';
                echo '<label for="show_email_1">'.$lang['yes'].'</label>';
				echo '</div>';
				
				echo '</div>';
				
				echo '<div class="setting_holder '.(($row['show_email'] == 1) ? '' : ' displayNone').'" id="connect_identifier_warning">';
				echo '<p class="red">'.$lang['srv_show_mail_with_data3'].'.</p>';
				echo '</div>';
					
				echo '</fieldset>';
			}
		}
		
		/* Nastavitve pdf/rtf izvozov */
		if ($_GET['a'] == 'export_settings') {

			SurveySetting::getInstance()->Init($this->anketa);
		
			// Nastavitve za izpis vprasalnika
			echo '<fieldset>';	
			echo '<legend>'.$lang['srv_export_survey_settings'].'</legend>';
			
			// Številčenje vprašanj (default da)
			$export_numbering = SurveySetting::getInstance()->getSurveyMiscSetting('export_numbering');
			echo '<div class="setting_holder">';
			echo '<input type="hidden" name="export_numbering" value="0">';
            echo '<input type="checkbox" id="export_numbering" name="export_numbering" value="1" '.($export_numbering==='1'?' checked':'').'>';
            echo '<label for="export_numbering">'. $lang['srv_nastavitveStevilcenje'] .'</label> ';
			echo '</div>';

			// Prikaz pogojev (default da)
			$export_show_if = SurveySetting::getInstance()->getSurveyMiscSetting('export_show_if');
			echo '<div class="setting_holder">';
			echo '<input type="hidden" name="export_show_if" value="0">';
            echo '<input type="checkbox" id="export_show_if" name="export_show_if" value="1" '.($export_show_if==='1'?' checked':'').'>';
            echo '<label for="export_show_if">'. $lang['srv_export_if'] .'</label> ';
			echo '</div>';

			// Prikazi uvoda (default ne)
			$export_show_intro = SurveySetting::getInstance()->getSurveyMiscSetting('export_show_intro');
			echo '<div class="setting_holder">';
			echo '<input type="hidden" name="exporexport_show_introt_show_if" value="0">';
            echo '<input type="checkbox" id="export_show_intro" name="export_show_intro" value="1" '.($export_show_intro==='1'?' checked':'').'>';
            echo '<label for="export_show_intro">'. $lang['srv_export_intro'] .'</label> ';
			echo '</div>';

			echo '</fieldset>';
			
			
			// Nastavitve za izpis odgovorov respondentov
			echo '<fieldset>';	
			echo '<legend>'.$lang['srv_export_results_settings'].'</legend>';


			// Številčenje vprašanj (default da)
			$export_data_numbering = SurveySetting::getInstance()->getSurveyMiscSetting('export_data_numbering');
			echo '<div class="setting_holder">';
			echo '<input type="hidden" name="export_data_numbering" value="0">';
            echo '<input type="checkbox" id="export_data_numbering" name="export_data_numbering" value="1" '.($export_data_numbering==='1'?' checked':'').'>';
            echo '<label for="export_data_numbering">'. $lang['srv_nastavitveStevilcenje'] .'</label> ';
			echo '</div>';

			
			// Prikaz recnuma (default da)
			$export_data_show_recnum = SurveySetting::getInstance()->getSurveyMiscSetting('export_data_show_recnum');
			echo '<div class="setting_holder">';
			echo '<input type="hidden" name="export_data_show_recnum" value="0">';
            echo '<input type="checkbox" id="export_data_show_recnum" name="export_data_show_recnum" value="1" '.($export_data_show_recnum==='1'?' checked':'').'>';
            echo '<label for="export_data_show_recnum">'. $lang['srv_export_show_recnum'] .'</label> ';
			echo '</div>';
			

			// Prikaz pogojev (default da)
			$export_data_show_if = SurveySetting::getInstance()->getSurveyMiscSetting('export_data_show_if');
			echo '<div class="setting_holder">';
			echo '<input type="hidden" name="export_data_show_if" value="0">';
            echo '<input type="checkbox" id="export_data_show_if" name="export_data_show_if" value="1" '.($export_data_show_if==='1'?' checked':'').'>';
            echo '<label for="export_data_show_if">'. $lang['srv_export_if'] .'</label> ';
			echo '</div>';

			
			// Page break med posameznimi respondenti (default ne)
			$export_data_PB = SurveySetting::getInstance()->getSurveyMiscSetting('export_data_PB');
			echo '<div class="setting_holder">';
			echo '<input type="hidden" name="export_data_PB" value="0">';
            echo '<input type="checkbox" id="export_data_PB" name="export_data_PB" value="1" '.($export_data_PB==='1'?' checked':'').'>';
            echo '<label for="export_data_PB">'. $lang['srv_export_pagebreak'] .'</label> ';
			echo '</div>';

			
			// Izpusti vprasanja brez odgovora (default ne)
			$export_data_skip_empty = SurveySetting::getInstance()->getSurveyMiscSetting('export_data_skip_empty');
			echo '<div class="setting_holder">';
			echo '<input type="hidden" name="export_data_skip_empty" value="0">';
            echo '<input type="checkbox" id="export_data_skip_empty" name="export_data_skip_empty" value="1" '.($export_data_skip_empty==='1'?' checked':'').'>';
            echo '<label for="export_data_skip_empty">'. $lang['srv_export_skip_empty'] .'</label> ';
			echo '</div>';
			

			// Izpusti podvprasanja brez odgovora (default ne)
			$export_data_skip_empty_sub = SurveySetting::getInstance()->getSurveyMiscSetting('export_data_skip_empty_sub');
			echo '<div class="setting_holder">';
			echo '<input type="hidden" name="export_data_skip_empty_sub" value="0">';
            echo '<input type="checkbox" id="export_data_skip_empty_sub" name="export_data_skip_empty_sub" value="1" '.($export_data_skip_empty_sub==='1'?' checked':'').'>';
            echo '<label for="export_data_skip_empty_sub">'. $lang['srv_export_skip_empty_sub'] .'</label> ';
			echo '</div>';

			// Tip izvoza (1->dolg oz. razsirjen, 2->kratek oz. skrcen)
			echo '<div class="setting_holder">';
			$export_data_type = SurveySetting::getInstance()->getSurveyMiscSetting('export_data_type');
			echo '<label for="export_data_type">'.$lang['srv_displaydata_type'];
			echo Help :: display('displaydata_pdftype');
			echo ':</label>';

			echo '<div class="setting_item">';
			echo '<input type="radio" name="export_data_type" id="export_data_type2" '.((int)$export_data_type == 2 ? ' checked':'').' value="2">';
			echo '<label for="export_data_type2">'.$lang['srv_displaydata_type2'].'</label> ';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="export_data_type" id="export_data_type1" '.((int)$export_data_type == 1 ?' checked':'').' value="1">';
			echo '<label for="export_data_type1">'.$lang['srv_displaydata_type1'].'</label> ';
			echo '</div>';

			echo '</div>';
				
			echo '</fieldset>';
		}
		
		/* Nastavitve GDPR */
		if ($_GET['a'] == A_GDPR) {

			$gdpr = new GDPR();
			$gdpr->displayGDPRSurvey($this->anketa);
		}
		
		
		if ($_GET['a'] != 'jezik' && $_GET['a'] != 'trajanje' && $_GET['a'] != A_GDPR && $_GET['a'] != 'dostop') {
			
			echo '<div class="button_holder">';
			echo '<button class="medium white-blue" onClick="window.location='.$preklici_url.';return false;">'.$lang['edit1338'].'</button>';
			echo '<button class="medium blue" onclick="document.settingsanketa_' . $row['id'] . '.submit(); return false;">'.$lang['edit1337'].'</button>';
			echo '</div>';
		}
		
		if (isset($_GET['s']) && $_GET['s'] == '1') {
			echo '<div id="success_save"></div>';
			echo '<script type="text/javascript">$(document).ready(function() {actionNotePopup(\'success_save\', \'success\');});</script>';
		}
		
		echo '</form>';
	}
	
	function anketa_nastavitve_mail() {
		global $lang;
		global $site_url;
		global $site_path;
		global $admin_type;
		global $global_user_id;

		/* Globalne nastavitve ankete: veljajo za celoto anketo ne glede na uporabnika*/
		$row = SurveyInfo::getInstance()->getSurveyRow();

		$http_referer = parse_url($_SERVER['HTTP_REFERER']); //If yes, parse referrer
		$referer_url = $http_referer['query'];
		$show_back_button = false;
		if(preg_match('/anketa='.$this->anketa.'&a='.A_INVITATIONS.'/', $referer_url) || $_GET['show_back'] == 'true')
			$show_back_button = true;
		
		echo '<fieldset><legend>'.$lang['srv_email_setting_title'].'</legend>';
		echo '<form name="settingsanketa_' . $row['id'] . '" action="ajax.php?a=editanketasettings&m='.A_MAILING. ($show_back_button ? '&show_back=true' : '').'" method="post" autocomplete="off">' . "\n\r";
		echo '	<input type="hidden" name="anketa" value="' . $this->anketa . '" />' . "\n\r";
				echo '  <input type="hidden" name="location" value="' . $_GET['a'] . '" />' . "\n\r";
		echo '  <input type="hidden" name="submited" value="1" />' . "\n\r";
		
		$MA = new MailAdapter($this->anketa);
		
		echo '<span class="bold">'.$lang['srv_email_setting_select_server'].'</span>&nbsp;';
		echo '<label><input type="radio" name="SMTPMailMode" value="0" '.($MA->is1KA() ? 'checked ="checked" ' : '').' onclick="$(\'#send_mail_mode1, #send_mail_mode2\').hide();$(\'#send_mail_mode0\').show();">';
        echo $lang['srv_email_setting_adapter0']. ' </label>';
        // Google smtp je viden samo starim, kjer je ze vklopljen
        if($MA->isGoogle()){
		    echo '<label><input type="radio" name="SMTPMailMode" value="1" '.($MA->isGoogle() ? 'checked ="checked" ' : '').' onclick="$(\'#send_mail_mode0, #send_mail_mode2\').hide(); $(\'#send_mail_mode1\').show();">';
            echo $lang['srv_email_setting_adapter1'].' </label>';
        }
		echo '<label><input type="radio" name="SMTPMailMode" value="2" '.($MA->isSMTP() ? 'checked ="checked" ' : '').' onclick="$(\'#send_mail_mode0, #send_mail_mode1\').hide(); $(\'#send_mail_mode2\').show();">';
		echo $lang['srv_email_setting_adapter2'].' </label>';
		echo Help :: display('srv_mail_mode');

		#1ka mail system
		$enkaSettings = $MA->get1KASettings();
		echo '<br>';
		echo '<br>';
		echo '<span class="bold">'.$lang['srv_email_setting_settings'].'</span><br>';
		echo '<div id="send_mail_mode0" '.(!$MA->is1KA() ? ' class="displayNone"' : '').'>';
		# from
		echo '<p><label>'.$lang['srv_email_setting_from'].'<span>'.$enkaSettings['SMTPFrom'].'</span><input type="hidden" name="SMTPFrom0" value="'.$enkaSettings['SMTPFrom'].'"></label>';
		echo '</p>';
		# replyTo
		echo '<p><label>'.$lang['srv_email_setting_reply'].Help::display('srv_obvescanje_odgovorZa').'<input type="text" name="SMTPReplyTo0" value="'.$enkaSettings['SMTPReplyTo'].'" ></label>';
		echo '</p>';
		echo '</div>';
		
		#GMAIL - Google
		$enkaSettings = $MA->getGoogleSettings();
		echo '<div id="send_mail_mode1" '.(!$MA->isGoogle() ? ' class="displayNone"' : '').'>';
		# from
		echo '<p><label>'.$lang['srv_email_setting_from'].'<input type="text" name="SMTPFrom1" value="'.$enkaSettings['SMTPFrom'].'"></label>';
		echo '</p>';
		# replyTo
		echo '<p><label>'.$lang['srv_email_setting_reply'].Help::display('srv_obvescanje_odgovorZa').'<input type="text" name="SMTPReplyTo1" value="'.$enkaSettings['SMTPReplyTo'].'" ></label>';
		echo '</p>';
		#Password
		echo '<p><label>'.$lang['srv_email_setting_password'].'<input type="password" name="SMTPPassword1" placeholder="'.$lang['srv_email_setting_password_placeholder'].'"></label>';
		echo '</p>';
		echo '</div>';

		#SMTP
		$enkaSettings = $MA->getSMTPSettings();
		echo '<div id="send_mail_mode2" '.(!$MA->isSMTP() ? ' class="displayNone"' : '').'>';
		# from - NICE
		echo '<p><label>'.$lang['srv_email_setting_from_nice'].'<input type="text" name="SMTPFromNice2" value="'.$enkaSettings['SMTPFromNice'].'"></label>';
		echo '</p>';
		# from
		echo '<p><label>'.$lang['srv_email_setting_from'].'<input type="text" name="SMTPFrom2" value="'.$enkaSettings['SMTPFrom'].'"></label>';
		echo '</p>';
		# replyTo
		echo '<p><label>'.$lang['srv_email_setting_reply'].Help::display('srv_obvescanje_odgovorZa').'<input type="text" name="SMTPReplyTo2" value="'.$enkaSettings['SMTPReplyTo'].'" ></label>';
		echo '</p>';
		#Username
		echo '<p><label>'.$lang['srv_email_setting_username'].'<input type="text" name="SMTPUsername2" value="'.$enkaSettings['SMTPUsername'].'" ></label>';
		echo '</p>';
		#Password
		echo '<p><label>'.$lang['srv_email_setting_password'].'<input type="password" name="SMTPPassword2" placeholder="'.$lang['srv_email_setting_password_placeholder'].'"></label>';
		echo '</p>';
		#autentikacija
		echo '<p>';
		echo $lang['srv_email_setting_autentication'];
		echo '<label><input type="radio" name="SMTPAuth2" value="0" '.((int)$enkaSettings['SMTPAuth'] != 1 ? 'checked ="checked" ' : '').'>';
		echo $lang['srv_email_setting_no'].'</label>';
		echo '<label><input type="radio" name="SMTPAuth2" value="1" '.((int)$enkaSettings['SMTPAuth'] == 1 ? 'checked ="checked" ' : '').'>';
		echo $lang['srv_email_setting_yes'].'</label>';
		echo '</p>';
		#Varnost SMTPSecure
		echo '<p>';
		echo $lang['srv_email_setting_encryption'];
		echo '<input type="radio" name="SMTPSecure2" value="0" '.((int)$enkaSettings['SMTPSecure'] == 0 ? 'checked ="checked" ' : '').'>';
		echo $lang['srv_email_setting_encryption_none'].'</label>';
		echo '<label><input type="radio" name="SMTPSecure2" value="ssl" '.($enkaSettings['SMTPSecure'] == 'ssl' ? 'checked ="checked" ' : '').'>';
		echo $lang['srv_email_setting_encryption_ssl'].'</label>';
		echo '<label><input type="radio" name="SMTPSecure2" value="tls" '.($enkaSettings['SMTPSecure'] == 'tls' ? 'checked ="checked" ' : '').'>';
		echo $lang['srv_email_setting_encryption_tls'].'</label>';
		echo '</p>';
		#port
		echo '<p><label>'.$lang['srv_email_setting_port'].'<input type="number" min="0" max="65535" name="SMTPPort2" value="'.(int)$enkaSettings['SMTPPort'].'" ></label>';
		echo $lang['srv_email_setting_port_note'];
		echo '</p>';
		#host
		echo '<p><label>'.$lang['srv_email_setting_host'].'<input type="text" name="SMTPHost2" value="'.$enkaSettings['SMTPHost'].'" ></label>';
		echo '</p>';
		echo '</div>';
		
		echo '</form>';
		echo '</fieldset>';
		
		echo '<span id="send_mail_mode_test"  class="floatLeft spaceRight"><div class="buttonwrapper"><a class="ovalbutton ovalbutton_green" href="#" onclick="showTestSurveySMTP(); return false;"><span>';
		echo $lang['srv_email_setting_btn_test'].'</span></a></div></span>';
		echo '<span class="floatLeft spaceRight" ><div class="buttonwrapper"><a class="ovalbutton ovalbutton_orange btn_savesettings" href="#" onclick="document.settingsanketa_' . $row['id'] . '.submit(); return false;"><span>';
		echo $lang['srv_email_setting_btn_save'] . '</span></a></div></span>';
		
		if (preg_match('/anketa='.$this->anketa.'&a='.A_INVITATIONS.'/', $referer_url) || $show_back_button) {
				
			//echo '<div class="floatLeft spaceRight buttonwrapper"><a class="ovalbutton ovalbutton_gray" href="'.$_SERVER['HTTP_REFERER'].'"><span>';
			echo '<div class="floatLeft spaceRight buttonwrapper"><a class="ovalbutton ovalbutton_gray" href="'.$site_url.'admin/survey/index.php?anketa='.$this->anketa.'&a=invitations"><span>';
			echo $lang['srv_back_to_email'] . '</span></a></div>';
			echo '</div>';
		}
		
		if (isset($_GET['s']) && $_GET['s'] == '1') {
			echo '<div id="success_save"></div>';
			echo '<script type="text/javascript">$(document).ready(function() {actionNotePopup(\'success_save\', \'success\');});</script>';
		}
	}

	/**
	* funkcija, ki prikaze polja za nastavitev ekstra prevodov
	* 
	*/
	function extra_translation ($text, $editor = 0) {
		global $lang;
		global $admin_lang;
		global $resp_lang;
		
		$lang_id = isset($_GET['lang_id']) ? (int)$_GET['lang_id'] : '';
		if ($lang_id > 0)
			$lang_id = '_'.$lang_id;
		else
			$lang_id = '';
			
		$value = SurveySetting::getInstance()->getSurveyMiscSetting('srvlang_'.$text.$lang_id);
		
		if ($value == '') $value = $lang[$text];

        $onclick = 'onclick="inline_jezik_edit(\'srvlang_'.$text.$lang_id.'\');"';

		// Popravimo text za naslednjo stran na uvodu
		$next_uvod = '';
		if($text == 'srv_nextpage_uvod')
			$next_uvod = $lang_id == '' ? $admin_lang['srv_nextpage_uvod_desc'] : $resp_lang['srv_nextpage_uvod_desc'];
		
		echo '<tr>';
		echo '<td>'.($lang_id==''?$admin_lang[$text]:$resp_lang[$text]).' '.($text == 'srv_nextpage_uvod' ? ' ('.$next_uvod.')' : '').'</td> ';
		echo '<td>'.$lang[$text].'</td>';
		echo '<td><div class="standardna-beseda"><div contentEditable="true" class="standardna-beseda-urejanje" name="srvlang_'.$text.$lang_id.'" id="srvlang_'.$text.$lang_id.'">'.$value.'</div>';

		if($editor == 1) 
			echo '<span class="faicon edit-vprasanje sb-edit blue"'.$onclick.' style="display:none;"></span>';

		echo '<textarea name="srvlang_'.$text.$lang_id.'" id="polje_srvlang_'.$text.$lang_id.'"  style="display:none;">'.$value.'</textarea>';
		echo '</div></td>';
        
		echo '</tr>';
	}
	
	function anketa_nice_links () {
		global $lang;
		
		echo '<div id="anketa_edit">';

		echo '<fieldset><legend>'.$lang['srv_nice_url'].'</legend>';
		
		$sql = sisplet_query("SELECT l.link, a.id, a.naslov FROM srv_nice_links l, srv_anketa a WHERE a.id=l.ank_id ORDER BY l.link ASC");
		
		$table_exists = 0;
        while ($row = mysqli_fetch_array($sql)) {   
            if ($table_exists == 0) {
                echo '<p class="bottom16">'.$lang['srv_nice_url_note'].'</p>';
				echo '<div class="table-horizontal-scroll-wrapper1">';
				echo '<div class="table-horizontal-scroll-wrapper2">';
                echo '<table>';
                echo '<tr>';
                echo '<th>'.$lang['srv_nice_url_extension'].'</th>';
                echo '<th>'.$lang['srv_nice_url_surveyname'].'</th>';
                echo '</tr>';

                $table_exists = 1;
            }

            echo '<tr>';
            echo '<td>'.$row['link'].'</td>';
            echo '<td><a href="index.php?anketa='.$row['id'].'&a=vabila">'.$row['naslov'].'</a></td>';
            echo '</tr>';
        }

        if ($table_exists == 1){
			echo '</table>';
			echo '</div>';
			echo '</div>';
		}

		echo '</fieldset>'; 
		echo '</div>';		
	}
	
	/**
	* prikaze ankete z administrativnim dostopom za pomoč
	* 
	*/
	function anketa_admin () {
		global $lang;
		
		echo '<div id="anketa_edit">';
		
		$sql = sisplet_query("SELECT srv_anketa.id, srv_anketa.naslov, users.email FROM srv_anketa, users WHERE users.id=srv_anketa.insert_uid AND dostop_admin >= DATE(NOW()) ORDER BY edit_time DESC");
		if (!$sql) echo mysqli_error($GLOBALS['connect_db']);
		
		while ($row = mysqli_fetch_array($sql)) {		
			echo '<p><strong style="display:inline-block; width:300px;"><a href="index.php?anketa='.$row['id'].'">'.$row['naslov'].'</a></strong> <span style="display:inline-block; width:300px;">('.$row['email'].')</span></p>';	
		}
		
		echo '</div>';	
	}
	
	/**
	* prikaze izbrisanje ankete
	* 
	*/
	function anketa_deleted () {
		global $lang;
		
		echo '<div id="anketa_edit">';

		echo '<fieldset><legend>'.$lang['srv_anketa_deleted'].'</legend>';
		
		$sql = sisplet_query("SELECT srv_anketa.id, srv_anketa.naslov, users.email FROM srv_anketa, users WHERE users.id=srv_anketa.insert_uid AND active='-1' ORDER BY edit_time DESC");
		if (!$sql) echo mysqli_error($GLOBALS['connect_db']);


		$table_exists = 0;
		while ($row = mysqli_fetch_array($sql)) {	
			if ($table_exists == 0)	{
				echo '<p class="bottom16">'.$lang['srv_anketa_deleted_note'].'</p>';
				echo '<div class="table-horizontal-scroll-wrapper1">';
				echo '<div class="table-horizontal-scroll-wrapper2">';
				echo '<table>';
				echo '<tr>';
				echo '<th>'.$lang['srv_data_deleted_surveyname'].'</th>';
				echo '<th class="nowrap">'.$lang['srv_data_deleted_user'].'</th>';
				echo '<th></th>';
				echo '</tr>';

				$table_exists = 1;
			}

			echo '<tr>';
			echo '<td><a href="index.php?anketa='.$row['id'].'">'.$row['naslov'].'</a></td>';
			echo '<td>'.$row['email'].'</td>';
			echo '<td><button class="table white-black caps" type="button" onclick="anketa_restore(\''.$row['id'].'\'); return false;">'.$lang['srv_anketa_deleted_restore'].'</button></td>';
			echo '</tr>';
		}

		if ($table_exists == 1){
			echo '</table>';
			echo '</div>';
			echo '</div>';
		}
			
		
		echo '</fieldset>';	
		echo '</div>';		
	}
	
	/**
	* prikaze izbrisanje podatke
	* 
	*/
	function data_deleted () {
		global $lang;
		
		echo '<div id="anketa_edit">';

		echo '<fieldset><legend>'.$lang['srv_data_deleted'].'</legend>';

		$sql = sisplet_query("SELECT a.id, a.naslov, users.email, COUNT(u.id) AS deleted FROM srv_anketa a, srv_user u, users WHERE u.deleted='1' AND u.ank_id=a.id AND users.id=a.insert_uid GROUP BY a.id ORDER BY edit_time DESC");
		if (!$sql) echo mysqli_error($GLOBALS['connect_db']);
		
		$table_exists = 0;
		while ($row = mysqli_fetch_array($sql)) {	
			if ($table_exists == 0)	{
				echo '<p class="bottom16">'.$lang['srv_data_deleted_note'].'</p>';
				echo '<table>';
				echo '<tr>';
				echo '<th>'.$lang['srv_data_deleted_surveyname'].'</th>';
				echo '<th class="nowrap">'.$lang['srv_data_deleted_user'].'</th>';
				echo '<th class="nowrap">'.$lang['srv_data_deleted'].'</th>';
				echo '<th></th>';
				echo '</tr>';

				$table_exists = 1;
			}

			echo '<tr>';
			echo '<td><a href="index.php?anketa='.$row['id'].'">'.$row['naslov'].'</a></td>';
			echo '<td>'.$row['email'].'</td>';
			echo '<td class="right">'.$row['deleted'].'</td>';
			echo '<td><button class="table white-black caps" type="button" onclick="data_restore(\''.$row['id'].'\'); return false;">'.$lang['srv_data_deleted_restore'].'</button></td>';
			echo '</tr>';
		}

		if ($table_exists == 1)
			echo '</table>';	
		
		echo '</fieldset>';	
		echo '</div>';	
	}

	// online urejanje CSS datoteke 
	function anketa_editcss() {
		$st = new SurveyTheme($this->anketa);
		$st->edit_css();
	}
	
	function anketa_vabila() {
		global $lang;	
		if ($_GET['a'] == A_VABILA) {
			$this->anketa_vabila_nastavitve();
		}
		elseif ($_GET['a'] == A_OTHER_INV) {
			$this->anketa_vabila_drugo();
		}
	}
	
	function anketa_vabila_nastavitve() {
		global $lang, $site_url, $global_user_id;
		
		$d = new Dostop();

		# Opozorilo o napakah
		$this->survey_errors();

		# Opozorilo - anketa ni aktivna
		$this->activation_error();

		// Delite povezavo do ankete
		echo '<div class="title">'.$lang['srv_publication_survey_links'].'</div>';

		//echo '<p class="bottom16">'.$lang['srv_publication_survey_links_note'].'</p>';

		
		//Lep URL - dodajanje
		// Preverimo, ce je funkcionalnost v paketu, ki ga ima uporabnik
        $userAccess = UserAccess::getInstance($global_user_id);
        if(!$userAccess->checkUserAccess($what='nice_url')){

            echo '<div class="button_holder">';

            echo '  <div class="copy_survey_url_holder">';
            echo '      <div class="input_box">';
            echo '          <span class="faicon link-chain link-right"></span><span class="italic"></span>';
            echo '          <a href="'.SurveyInfo::getRawSurveyLink().'">'.SurveyInfo::getRawSurveyLink().'</a>';
            echo '      </div>';
            echo '      <button type="button" class="medium blue" onclick="CopyToClipboard(\''.SurveyInfo::getRawSurveyLink().'\'); actionNotePopup(\'link_copied\', \'success\');">
				<span class="faicon copy link-right"></span>'.$lang['srv_diagnostika_testiranje_copy_url'].'</button>';
            echo '  </div>';
            
            /* echo '  <button type="button" class="medium yellow">'.$lang['srv_nice_url_add'].'</button>'; */
			echo '  <button type="button" class="medium yellow niceurl">'.$lang['srv_nice_url_add'].'<span class="faicon lock_close"></span></button>';
			
            echo '</div>';
        }
        else{

            echo '<div class="button_holder">';

            echo '  <div class="copy_survey_url_holder">';
            echo '      <div class="input_box">';
            echo '          <span class="faicon link-chain link-right"></span><span class="italic"></span>';
            echo '          <a href="'.SurveyInfo::getRawSurveyLink().'">'.SurveyInfo::getRawSurveyLink().'</a>';
            echo '      </div>';
            echo '      <button type="button" class="medium blue" onclick="CopyToClipboard(\''.SurveyInfo::getRawSurveyLink().'\'); actionNotePopup(\'link_copied\', \'success\');">
				<span class="faicon copy"></span>'.$lang['srv_diagnostika_testiranje_copy_url'].'</button>';
            echo '  </div>';
    
            // Gumb za dodajanje lepega linka
            echo '  <button id="nice_url_button" type="button" class="medium white-blue niceurl" onclick="publishPopupOpen(\'add_url\');">'.$lang['srv_nice_url_add'].'</button>';

			echo '</div>';

			
			if (isset($_GET['error'])) {

				$error = $_GET['error'];

				/* ?><script type="text/javascript">
					toggleNiceURL('show');
				</script><?php */

				echo '<div id="nice_url_error">';
                
                // Prekratek lep url
                if(strlen($error) <= 2)
                    echo '<p class="red"><span class="bold">'.$error.'</span> '.$lang['srv_nice_url_short'].'</p>';
                // Predolg lep url
                elseif(strlen($error) > 20)
                    echo '<p class="red"><span class="bold">'.$error.'</span> '.$lang['srv_nice_url_long'].'</p>';
                // Ze obstaja
                else
                    echo '<p class="red"><span class="bold">'.$error.'</span> '.$lang['srv_nice_url_taken'].'</p>';
				
				echo '</div>';
			}

        }

		// Zlistamo nice URL, povezave za skupine in jezike
		$this->niceUrlSettings();
		
		// Spodnji kvadratki
		// Več načinov deljenja ankete
		echo '<div class="divider_line"></div>';

		echo '<div class="more">';

		// Vabila
		echo '<div class="more_block '.(!$userAccess->checkUserAccess('arhivi') ? 'user_access_locked' : '').'"  onclick="window.location.href=\'index.php?anketa=' . $this->anketa . '&a=invitations\'">';
			echo '<div class="more_title">';
			echo '<div class="more_title_icon"><span class="faicon inv_sent_1 empty"></span></div>';
			echo $lang['srv_publication_invitations'];
			if(!$userAccess->checkUserAccess('arhivi')) echo '<span class="faicon lock_close"></span>';
			echo '</div>';
		echo '</div>';

		// Drugi nacini
		echo '<div class="more_block" onclick="window.location.href=\'index.php?anketa=' . $this->anketa . '&a=other_inv\'">';
			echo '<div class="more_title">';
			echo '<div class="more_title_icon"><span class="faicon qrcode"></span></div>';
			echo $lang['srv_publication_survey_more'];
			echo '</div>';
		echo '</div>';

		echo '</div>';
		echo '</div>';

		
		echo '</div>';
	}

	# Opozorilo - anketa ni aktivna
	private function activation_error(){
		global $lang;

		$row = SurveyInfo::getInstance()->getSurveyRow();

		if ($row['active']==0) {
			echo '<div class="top_note one_liner">';
			//echo '  <div class="title">';
			//echo '      <span class="faicon warning"></span> '.$lang['srv_publication_survey_noactive'];
			//echo '  </div>';
			//echo    '<p class="bottom16">'.$lang['srv_publication_survey_noactive_note'].'</p>';


			echo '<div class="top_note_activate">';
			
			//echo '<p class="semi-bold">'.$lang['srv_anketa_noactive2'].'</p>';
			echo '<span class="faicon warning"></span><p>'.$lang['srv_publication_survey_noactive_note3'].'</p>';

			//Toggle kopiran iz headerja
			if (SurveyInfo::getSurveyColumn('active') == 1) {
				
				# anketa je aktivna
				# V kolikor gre za hierarhijo in uporabnik ni administrator hierarhije
				if (SurveyInfo::getInstance()->checkSurveyModule('hierarhija')){
					if ($hierarhija_type == 1) {
						echo '<a href="index.php?anketa=' . $this->anketa . '&amp;a=' . A_HIERARHIJA_SUPERADMIN . '&amp;m=' . M_ADMIN_AKTIVACIJA . '" class="srv_ico" title="' . $lang['srv_anketa_noactive'] . '">';
					} 
					else{
						echo '<a href="#" class="srv_ico" title="' . $lang['srv_anketa_active'] . '" style="cursor:text !important;">';
					}
				}
				else {
					echo '<a href="#" class="srv_ico" onclick="anketa_active(\'' . $this->anketa . '\',\'' . $row['active'] . '\'); return false;" title="' . $lang['srv_anketa_active'] . '">';
				}

				echo '<div id="srv_active" class="switch_anketa anketa_on"></div>';
				
				echo '</a>';
			} 
			else {
				$anketa_active = "anketa_active('" . $this->anketa . "','" . $row['active'] . "'); ";

				//Preden anketo aktiviramo preverimo, če gre tudi za izgradnjo hierarhije in če anketa še ni bila aktivirana
				if (SurveyInfo::getInstance()->checkSurveyModule('hierarhija')){
					if ($hierarhija_type == 1) {
						echo '<a href="index.php?anketa=' . $this->anketa . '&amp;a=' . A_HIERARHIJA_SUPERADMIN . '&amp;m=' . M_ADMIN_AKTIVACIJA . '" class="srv_ico" title="' . $lang['srv_anketa_noactive'] . '">';
					} else{
						echo '<a href="#" class="srv_ico" title="' . $lang['srv_anketa_noactive'] . '">';
					}
				}
				else {
					echo '<a href="#" class="srv_ico" onclick="' . $anketa_active . ' return false;" title="' . $lang['srv_anketa_noactive'] . '">';
				}

				echo '<div id="srv_inactive" class="switch_anketa anketa_off"></div>';

				echo '</a>';
			}

			echo '</div>';

			//echo '<p class="top16">'.$lang['srv_publication_survey_noactive_note2'].'</p>';

			echo '</div>';
		}
	}

	# Opozorilo o napakah
	private function survey_errors(){
		global $lang;

		$sd = new SurveyDiagnostics($this->anketa);
		$sd->doDiagnostics();
		$diagnostic = $sd->getDiagnostic();
		if (is_array($diagnostic) && count($diagnostic) > 0) {

			echo '<div class="top_note" id="publication_warnings">';
			echo '  <div class="title small">';
			echo '      <span class="faicon warning"></span> '.$lang['srv_warning'];
			echo '<div class="note_hide"><a href="#" onClick="top_note_hide(\'publication_warnings\'); return false;"">✕</a></div>';
			echo '  </div>';
			echo    $lang['srv_publication_survey_warnings'].'\'<a href="index.php?anketa=' . $this->anketa . '&amp;a='.A_TESTIRANJE.'">'.$lang['srv_testiranje'].'</a>\'.';
			echo '</div>';
		}
	}

	function anketa_vabila_drugo() {
		global $lang, $site_url, $global_user_id;
		
		$d = new Dostop();

		# Opozorilo o napakah
		$this->survey_errors();

		# Opozorilo - anketa ni aktivna
		$this->activation_error();

		// Preverimo, ce je funkcionalnost v paketu, ki ga ima uporabnik
        $userAccess = UserAccess::getInstance($global_user_id);
		
		// Več načinov deljenja ankete
		echo '<div class="title">'.$lang['srv_publication_survey_more'].'</div>';

		echo '<div class="more squares">';

		//QR koda
		echo '<div class="more_block square" onclick="publishPopupOpen(\'QR\');">';
			echo '<div class="more_title_icon"><span class="faicon fa-qrcode"></span></div>';
			echo '<div class="more_title">';
			echo $lang['srv_anketa_href_friends'];
			echo '</div>';
			//echo $lang['srv_anketa_href_friends_text'];
		echo '</div>';

		//jazvem
		echo '<div class="more_block square" onclick="publishPopupOpen(\'jazvem\');">';
			echo '<div class="more_title_icon jazvem"></div>';
			echo '<div class="more_title">';
			echo $lang['srv_anketa_href_jazvem'];
			echo '</div>';
			//echo $lang['srv_anketa_href_friends_text'];
		echo '</div>';

		echo '</div>';

		echo '<div class="divider_line"></div>';

		// Več načinov deljenja ankete
		echo '<div class="title">'.$lang['srv_publication_survey_integration'].'</div>';

		echo '<div class="more squares">';

		//HTML koda
		echo '<div class="more_block square" onclick="publishPopupOpen(\'html\');">';
			echo '<div class="more_title_icon"><span class="fa-brands html5"></span></div>';
			echo '<div class="more_title">';
			echo $lang['srv_anketa_href'];
			echo '</div>';
			//echo $lang['srv_anketa_href_text'];
		echo '</div>';
		
		//HTML koda +  števec
		echo '<div class="more_block square" onclick="publishPopupOpen(\'htmlcount\');">';
			echo '<div class="more_title_icon"><span class="fa-brands html5"></span><span class="circle-icon icon-123"></span></div>';
			echo '<div class="more_title">';
			echo $lang['srv_anketa_href_count'];
			echo '</div>';
			//echo $lang['srv_anketa_href_count_text'];
		echo '</div>';

		// iFrame - brez js
			echo '<div class="more_block square" onclick="publishPopupOpen(\'iframenojs\');">';
			echo '<div class="more_title_icon"><span class="faicon fa-window-maximize empty"></span></div>';
			echo '<div class="more_title">';
			echo $lang['srv_anketa_href_inojs'];
			echo '</div>';
			//echo $lang['srv_anketa_href_inojs_text'];
		echo '</div>';

		// iFrame - js
		echo '<div class="more_block square" onclick="publishPopupOpen(\'iframejs\');">';
			echo '<div class="more_title_icon"><span class="faicon fa-window-maximize empty"></span><span class="circle-icon icon-JS"></span></div>';
			echo '<div class="more_title">';
			echo $lang['srv_anketa_href_ijs'];
			echo '</div>';
			//echo $lang['srv_anketa_href_ijs_text'];
		echo '</div>';
		


		echo '</div>';
		echo '</div>';

	}
	
	function niceUrlSettings() {
		global $lang, $site_url, $global_user_id;
		
		$p = new Prevajanje($this->anketa);
		$p->dostop();
		$lang_array = $p->get_all_translation_langs();

		$row = SurveyInfo::getInstance()->getSurveyRow();

		$p->include_lang($p->lang_resp);
		$base_lang_resp = $lang['language'];
		$p->include_base_lang();
		
		$link = SurveyInfo::getSurveyLink();
		$preview_disableif = SurveySetting::getInstance()->getSurveyMiscSetting('preview_disableif');
		$preview_disablealert = SurveySetting::getInstance()->getSurveyMiscSetting('preview_disablealert');
		$preview_displayifs = SurveySetting::getInstance()->getSurveyMiscSetting('preview_displayifs');
		$preview_displayvariables = SurveySetting::getInstance()->getSurveyMiscSetting('preview_displayvariables');
		$preview_hidecomment = SurveySetting::getInstance()->getSurveyMiscSetting('preview_hidecomment');
		$preview_options = ''.($preview_disableif==1?'&disableif=1':'').($preview_disablealert==1?'&disablealert=1':'').($preview_displayifs==1?'&displayifs=1':'').($preview_displayvariables==1?'&displayvariables=1':'').($preview_hidecomment==1?'&hidecomment=1':'').'';
        
		echo '<div class="publish_url_holder">';
		
		//Zlistamo vse lepe url-je
		$title = 0;
        $sqll = sisplet_query("SELECT id, link FROM srv_nice_links WHERE ank_id = '$this->anketa' ORDER BY id desc");
        while ($rowl = mysqli_fetch_assoc($sqll)) {

			if ($title == 0) {
				echo '<div class="divider_line"></div>';

			    echo '<div class="title">'.$lang['srv_publication_survey_niceurl'].'</div>';
			    $title = 1;
			}

            $link_nice = $site_url . $rowl['link'];

			echo '  <div class="niceurl">';
			echo '      <div class="input_box">';
            echo '          <span class="faicon link-chain link-right"></span>';
            echo '          <a href="'.$link_nice.'">'.$site_url.'<span class="semi-bold">'.$rowl['link'].'</span></a>';
            echo '      </div>';

			echo '		<a class="input_box as_button onlyicon" href="ajax.php?a=nice_url_remove&anketa='.$this->anketa.'&nice_url='.$rowl['id'].'" title="'.$lang['srv_copy_remove'].'"><span class="faicon trash empty blue"></span></a>';

			echo '      <div class="input_box as_button" onclick="CopyToClipboard(\''.$link_nice.'\'); actionNotePopup(\'link_copied\', \'success\'); return false;" title="'.$lang['srv_diagnostika_testiranje_copy_url'].'">';
            echo '          <span class="faicon copy blue no_margin"></span>'.$lang['srv_diagnostika_testiranje_copy_url'];
            echo '      </div>';

			echo '	</div>';
        }
		

        // Povezave za skupine in jezike
        // Navaden url
		
		$ss = new SurveySkupine($this->anketa);
		$spr_id = $ss->hasSkupine();

		$vrednosti = $ss->getVrednosti($spr_id);

        if (count($lang_array) > 0 || $spr_id > 0){
			echo '<div class="divider_line"></div>';

		    echo '<div class="title">'.$lang['srv_publication_survey_grouplang'].'</div>';

            if (count($lang_array) > 0){
				echo '<div class="lang_wrapper">';
				echo '	<div class="lang_title"><span class="semi-bold">'.$base_lang_resp.'</span>'.$lang['srv_nice_url_default_lang'].'</div>';
				echo '	<hr class="lang_line">';
				echo '</div>';

				echo '  <div class="niceurl">';
				echo '		<div class="group_name" title="'.$lang['srv_nice_url_no_group'].'">'.$lang['srv_nice_url_no_group'].'</div>';
				echo '      <div class="input_box">';
				echo '          <span class="faicon link-chain link-right"></span>';
				echo '          <a href="'.$link.'">'.$link.'</a>';
				echo '      </div>';
	
				echo '      <div class="input_box as_button" onclick="CopyToClipboard(\''.$link.'\'); actionNotePopup(\'link_copied\', \'success\'); return false;" title="'.$lang['srv_diagnostika_testiranje_copy_url'].'">';
				echo '          <span class="faicon copy blue no_margin"></span>'.$lang['srv_diagnostika_testiranje_copy_url'];
				echo '      </div>';
				echo '	</div>';

            }
        }

		// Imamo vec linkov za skupine
		if($spr_id > 0){		

			foreach($vrednosti as $vrednost){

				$link_skupine = isset($vrednost['nice_url']) ? $vrednost['nice_url'] : $vrednost['url'];

				echo '  <div class="niceurl">';
				echo '		<div class="group_name" title="'.$vrednost['naslov'].'">'.$vrednost['naslov'].'</div>';
				echo '      <div class="input_box">';
				echo '          <span class="faicon link-chain link-right"></span>';
				echo '          <a href="'.$link_skupine.'">'.$link_skupine.'</a>';
				echo '      </div>';
	
				echo '      <div class="input_box as_button" onclick="CopyToClipboard(\''.$link_skupine.'\'); actionNotePopup(\'link_copied\', \'success\'); return false;" title="'.$lang['srv_diagnostika_testiranje_copy_url'].'">';
				echo '          <span class="faicon copy blue no_margin"></span>'.$lang['srv_diagnostika_testiranje_copy_url'];
				echo '      </div>';
				echo '	</div>';
			}
		}
		
		// Imamo vec linkov za jezike
		if (count($lang_array) > 0) {

			foreach ($lang_array AS $lang_id => $lang_name) {

				echo '<div class="lang_wrapper">';
				echo '	<div class="lang_title"><span class="semi-bold">'.$lang_name.'</span></div>';
				echo '	<hr class="lang_line">';
				echo '</div>';

				echo '  <div class="niceurl">';
				echo '		<div class="group_name" title="'.$lang['srv_nice_url_no_group'].'">'.$lang['srv_nice_url_no_group'].'</div>';
				echo '      <div class="input_box">';
				echo '          <span class="faicon link-chain link-right"></span>';
				echo '          <a href="'.$link.'?language='.$lang_id.'">'.$link.'?language='.$lang_id.'</a>';
				echo '      </div>';
	
				echo '      <div class="input_box as_button" onclick="CopyToClipboard(\''.$link.'?language='.$lang_id.'\'); actionNotePopup(\'link_copied\', \'success\'); return false;" title="'.$lang['srv_diagnostika_testiranje_copy_url'].'">';
				echo '          <span class="faicon copy blue no_margin"></span>'.$lang['srv_diagnostika_testiranje_copy_url'];
				echo '      </div>';
				echo '	</div>';
				
				if($spr_id > 0){
					foreach($vrednosti as $vrednost){

						$link_skupine = isset($vrednost['nice_url']) ? $vrednost['nice_url'] : $vrednost['url'];

						echo '  <div class="niceurl">';
						echo '		<div class="group_name" title="'.$vrednost['naslov'].'">'.$vrednost['naslov'].'</div>';
						echo '      <div class="input_box">';
						echo '          <span class="faicon link-chain link-right"></span>';
						echo '          <a href="'.$link_skupine.'&language='.$lang_id.'">'.$link_skupine.'&language='.$lang_id.'</a>';
						echo '      </div>';
			
						echo '      <div class="input_box as_button" onclick="CopyToClipboard(\''.$link_skupine.'&language='.$lang_id.'\'); actionNotePopup(\'link_copied\', \'success\'); return false;" title="'.$lang['srv_diagnostika_testiranje_copy_url'].'">';
						echo '          <span class="faicon copy blue no_margin"></span>'.$lang['srv_diagnostika_testiranje_copy_url'];
						echo '      </div>';
						echo '	</div>';
					}
				}
			}
		}
	}
	
	function displayInvSurveyPopup() {
		global $lang;
		
		echo '<fieldset>';
		echo '<legend>'.$lang['srv_popup_title'].':</legend>';
		
		echo '<p><span onclick="$(\'#popup\').toggle(); $(\'#popup textarea\').click();" class="as_link">'.$lang['srv_embed_js'].Help :: display('srv_popup_js').'</span></p>';
		echo '<p id="popup" '.($_GET['js']!='open'?'style="display:none"':'').'><textarea id="pop" style="width: 99%; height:80px" onclick="this.select();" readonly>'.$this->getPopup().'</textarea></p>';
		
		echo '</fieldset>';
		
	}
	/**
	* nastavitve za obveščanje na email
	* 
	*/
	function email_nastavitve ($show_fieldset = true) {
		global $lang;
		global $site_url;
		global $site_path;
		global $admin_type;
		global $global_user_id;

		$row = SurveyInfo::getInstance()->getSurveyRow();
		
#		echo '<div id="anketa_edit">';
		
		// če ni aktivna damo opozorilo
			
		echo '<form name="settingsanketa_' . $row['id'] . '" action="ajax.php?a=editanketasettings&m=vabila" method="post" autocomplete="off">' . "\n\r";
		echo '<input type="hidden" name="submited" value="1" />' . "\n\r";
		echo '<div id="userCodeSettings1">';
		$this->respondenti_iz_baze($row,$show_fieldset);
		echo '</div>';
		
		if ($admin_type == 0) {
			if ($show_fieldset) {
				echo '<fieldset><legend>'.$lang['srv_show_mail_with_data'].'</legend>';
			} else {
				echo '<p class="strong">4.'.$lang['srv_show_mail_with_data'].'</p>';
			}
			echo '<span>'.$lang['srv_show_mail_with_data2'].': </span>';
			
			echo '<input type="radio" name="show_email"'.($row['show_email']==0?' checked':'').' value="0">'.$lang['no'].' ';
			echo '<input type="radio" name="show_email"'.($row['show_email']==1?' checked':'').' value="1">'.$lang['yes'].' ';
			echo '<p>* '.$lang['srv_show_mail_with_data3'].'</p>';
			if ($show_fieldset) {
				echo '</fieldset>';
			}
		}
		
		echo '</form>';
		
		echo '<span class="floatLeft spaceRight"><div class="buttonwrapper"><a class="ovalbutton ovalbutton_orange btn_savesettings" href="#" onclick="document.settingsanketa_' . $row['id'] . '.submit(); return false;"><span>';
		//			echo '<img src="icons/icons/disk.png" alt="" vartical-align="middle" />';
		echo $lang['edit1337'] . '</span></a></div></span>';
		if (isset($_GET['s']) && $_GET['s'] == '1') {
			echo '<div id="success_save"></div>';
			echo '<script type="text/javascript">$(document).ready(function() {actionNotePopup(\'success_save\', \'success\');});</script>';
		}
		
		#echo '</div>';
		
	}
	
	/**
	 * vrne kodo ankete, ki se jo uporabi za embed
	 *
	 */
	function getEmbed ($js = true) {
		global $site_url;
		
		
		$link = SurveyInfo::getSurveyLink();
		SurveyInfo::getInstance()->SurveyInit($this->anketa);
		$row = SurveyInfo::getInstance()->getSurveyRow();
		
		$iframe = '<iframe id="1ka" src="'.$link.'?e=1" height="500px" width="100%" scrolling="auto" frameborder="0"></iframe>';
		$javascript = '<script type="text/javascript">function r(){var a=window.location.hash.replace("#","");if(a.length==0)return;document.getElementById("1ka").style.height=a+"px";window.location.hash=""};window.setInterval("r()",100);'
		.'</script>';

		if ($js)
		return htmlentities($iframe.$javascript, ENT_QUOTES);
		else
		return htmlentities($iframe, ENT_QUOTES);
	}
	
	/**
	 * vrne kodo ankete, ki se jo uporabi za popup embed 
	 *
	 */
	function getPopup () {
		global $site_url;
		
		$link = SurveyInfo::getSurveyLink().'&popup=1';
		SurveyInfo::getInstance()->SurveyInit($this->anketa);
		$row = SurveyInfo::getInstance()->getSurveyRow();
		
		$javascript = '<script type="text/javascript">window.onload=function() {var body=document.getElementsByTagName("body")[0];var div=document.createElement("div");var iframe=document.createElement("iframe");div.setAttribute("id","popup_div");div.setAttribute("style","position:fixed; top:0; right:0; bottom:0; left:0; background:#000; opacity:0.5");iframe.setAttribute("id","popup_iframe");iframe.setAttribute("src","'.$link.'");iframe.setAttribute("style","position: fixed; top:10%; left:50%; margin-left:-400px; background:#fff; height:80%; width:800px;");iframe.setAttribute("scrolling","auto");iframe.setAttribute("frameborder","0");body.appendChild(div);body.appendChild(iframe)}</script>';
		// Dodatek ce bomo delali se naprej popup embed (cross domain problem)
		//if(window.addEventListener){window.addEventListener("message", function(e){if(e.data == "closePopup"){document.getElementById("popup_iframe").remove();document.getElementById("popup_div").remove();}});}

		return htmlentities($javascript, ENT_QUOTES);
	}
	
	/**
	 * @desc prika?e nastavitve alerta za formo
	 */
	function alert_nastavitve() {//OSTANE
		global $lang;
		global $site_url;
		global $admin_type;
		
		$anketa = $this->anketa;

		$preklici_url = ltrim(str_replace("&s=1","",$_SERVER['REQUEST_URI']),"/");
		$preklici_url = "'". $site_url . $preklici_url . "'";
		
		/* moznosti:
		 * 	'complete'		-> obvsetilo o izpolnjeni anketi (respondent, respondent iz cms, avtor + dostop, dodatn-emaili)
		 *  'delete'		-> obvestilo o izbrisani anketi (avtor + dostop, dodatni -emaili)
		 *  'active'		-> obvestilo o aktivnosti, neaktivnosti ankete (avtor + dostop, dodatni -emaili)
		 *  'expire'		-> obvestilo o izteku ankete (avtor + dostop, dodatni -emaili)
		 *  'email_server'	-> nastavitve mail streznika
		 */
		if ( isset($_GET['m']) && $_GET['m'] != "") {
			$tab = $_GET['m'];
        } 
        else {
			$tab = $_GET['m'] = 'complete';
        }
        
		// preberemo nastavitve alertov
		$sqlAlert = sisplet_query("SELECT * FROM srv_alert WHERE ank_id = '$anketa'");
		if (!$sqlAlert)
		    echo mysqli_error($GLOBALS['connect_db']);
        
        if (mysqli_num_rows($sqlAlert) > 0) {
			$rowAlert = mysqli_fetch_array($sqlAlert);
        } 
        else {
			SurveyAlert::getInstance()->Init($anketa, $global_user_id);
			$rowAlert = SurveyAlert::setDefaultAlertBeforeExpire();
		}
		
		$days = $rowAlert['expire_days'];
		$sqlS = sisplet_query("SELECT id, expire, survey_type, insert_uid, DATE_SUB(expire,INTERVAL $days DAY) as newdate FROM srv_anketa WHERE id = '$anketa'");
		if (!$sqlS)	echo mysqli_error($GLOBALS['connect_db']);
		$rowS = mysqli_fetch_array($sqlS);
		$rowAlert['newdate'] = $rowS['newdate'];

		$sqlu = sisplet_query("SELECT email FROM users WHERE id = '$rowS[insert_uid]'");
		$rowu = mysqli_fetch_array($sqlu);
		$MailReply = $rowu['email'];
		
		$custom_alert = array();
		$sql_custom_alert = sisplet_query("SELECT uid, type FROM srv_alert_custom WHERE ank_id = '$this->anketa'");
		while ($row_custom_alert = mysqli_fetch_array($sql_custom_alert)) {
			$custom_alert[$row_custom_alert['type']][$row_custom_alert['uid']] = 1;
		}
		
		
		if ($tab == 'complete') {
			
			//Obveščanje o izpolnjeni anketi
			echo '  <form name="alertanketa_' . $anketa . '" action="ajax.php?a=editanketaalert&m='.$tab.'" method="post" autocomplete="off">' . "\n";
			echo '    <input type="hidden" name="anketa" value="' . $anketa . '" />' . "\n";
			echo '    <input type="hidden" name="location" value="' . $_GET['a'] . '" />' . "\n";
			echo '    <input type="hidden" name="m" value="' . $_GET['m'] . '" />' . "\n";
			echo '    <input type="hidden" name="submited" value="1" />' . "\n";
			
			
			echo '    <fieldset>';
			echo '    <legend>' . $lang['srv_alert_completed_2'] . '</legend>';

			echo '<div class="setting_holder alert_prejemniki">';
            echo '<span class="setting_title">'.$lang['srv_alert_completed_prejemniki'].'</span>';

			// respondent - ne prikazemo ce gre za glasovanje oz. volitve
            if($rowS['survey_type'] != 0 && !SurveyInfo::getInstance()->checkSurveyModule('voting')){
				echo '<div class="setting_item" id="label_alert_finish_respondent">';
                
                //echo '<input type="checkbox" name="alert_finish_respondent" id="alert_finish_respondent" value="1" onChange="change_alert_respondent(\'finish_respondent\', $(this)); $(\'form[name=alertanketa_' . $anketa . ']\').submit(); return false;" ' . ($rowAlert['finish_respondent'] == 1 ? ' checked' : '') . '>';
                $this->display_alert_label('finish_respondent',($rowAlert['finish_respondent'] == 1));
                
                echo '</div>';

                // Ce imamo vec prevodov omogocimo za vsak prevod svoj email
                $this->display_alert_label('finish_respondent_language',($rowAlert['finish_respondent'] == 1));
				
            }

			// respondent iz cms ne prikazemo ce gre za volitve
            if(!SurveyInfo::getInstance()->checkSurveyModule('voting')){
				echo '<div class="setting_item" id="label_alert_finish_respondent_cms">';

                //echo '<input type="checkbox" name="alert_finish_respondent_cms" id="alert_finish_respondent_cms" value="1" onChange="change_alert_respondent(\'finish_respondent_cms\', $(this)); change_alert_instruction($(this)); $(\'form[name=alertanketa_' . $anketa . ']\').submit(); return false;" ' . ($rowAlert['finish_respondent_cms'] == 1 ? ' checked' : '') . '>';
                $this->display_alert_label('finish_respondent_cms',($rowAlert['finish_respondent_cms'] == 1));
			
                echo '</div>';
            }

			//Avtor ankete oz. souredniki
            echo '<div class="setting_item" id="label_alert_finish_author">';
            echo '<input type="checkbox" name="alert_finish_author" id="alert_finish_author" value="1" onChange="change_alert_respondent(\'finish_author\', $(this));return false;"' . ($rowAlert['finish_author'] == 1 ? ' checked' : '') . '>';
            $this->display_alert_label('finish_author',($rowAlert['finish_author'] == 1));
            echo '</div>';

			//Posebej navedeni maili
            echo '<div class="setting_item">';
            echo '<input type="checkbox" name="alert_finish_other"  id="alert_finish_other"  value="1"' . (($rowAlert['finish_other'] == 1 || ($rowAlert['finish_other_emails'] && $rowAlert['finish_other'] != 0)) ? ' checked' : '') . ' onchange="toggleStatusAlertOtherCheckbox(\'finish_other\'); if ( ! $(this).attr(\'checked\') ) { $(\'form[name=alertanketa_' . $anketa . ']\').submit(); }"><label for="alert_finish_other">' . $lang['email_prejemniki'] .'</label>';
            echo ' <a href="#" onclick="alert_custom(\'other\', \'0\'); return false;" title="'.$lang['srv_alert_custom'].'"><span class="faicon text_file empty"></span></a>';
            echo ' <a href="#" onclick="alert_edit_if(\'4\'); return false;"><span class="faicon if_add" '.($rowAlert['finish_other_if']==0?'style=""':'').'></span></a> ';
            if ($rowAlert['finish_other_if']>0) { if ($b==null) $b = new Branching($this->anketa); $b->conditions_display($rowAlert['finish_other_if']); }
			echo '</div>';
            
            echo '<div class="setting_item">';

            echo '<div class="setting_holder alert_other_emails '.($rowAlert['finish_other'] == 0 ? 'displayNone' : '' ).'" id="alert_holder_finish_other_emails">';
            echo '<label for="alert_finish_other_emails">' . $lang['email_prejemniki_one_per_line'] . ':</label>';
            echo '<textarea class="alert_other_emails" name="alert_finish_other_emails" id="alert_finish_other_emails">' . $rowAlert['finish_other_emails'] . '</textarea>';
            echo '</div>';
            echo '</div>';

			echo '</div>';

			echo '<div id="obvescanje_sidebyside_holder">';
            echo '<div id="obvescanje_sidebyside_left">';

            echo '<p class="bold caps bottom16">'.$lang['message_content'].'</p>';

			echo '<div class="setting_holder">';
            echo '<label for="alert_finish_subject">' . $lang['subject'] . ': </label>';
            echo '<input type="text" class="large wauto" id="alert_finish_subject" name="alert_finish_subject" value="' . ($rowAlert['finish_subject'] ? $rowAlert['finish_subject'] : $lang['srv_alert_finish_subject']) . '"/>';
            echo '</div>';

			echo '<div class="setting_holder">';
            echo '<label for="reply_to">' . $lang['srv_replay_to'] . Help::display('srv_obvescanje_odgovorZa').': </label>';
            echo '<input type="text" class="large wauto" id="reply_to" name="reply_to" value="' . ($rowAlert['reply_to'] ? $rowAlert['reply_to'] : $MailReply) . '"/>';
            echo '</div>';
			
			if ($rowAlert['finish_text'] != '') {
				$text = $rowAlert['finish_text'];
            } 
            else {
                // Podpis
                $signature = Common::getEmailSignature();

				$text = nl2br($lang['srv_alert_finish_text'].$signature);
			}

			// prikaze editor za ne-spremenljivko (za karkoli druzga pac)
			echo '<div class="setting_holder">';

            echo '<label class="bottom4" for="alert_finish_text">' . $lang['message'] . ':</label>';
            echo '<textarea name="alert_finish_text" id="alert_finish_text">' . $text . '</textarea>';

            echo '</div>';
            echo '</div>';

			//Navodila na desni
            echo '<div id="obvescanje_sidebyside_right">';
            echo '<div class="message_instructions">';
            echo '<p class="bold caps bottom16">'.$lang['srv_alert_instruction1'].'</p>';
            echo '<p class="bold bottom16">'.$lang['srv_alert_instruction1a'].':</p>';

            echo '<div class="izpolnjena_spremenljivke">';
			
			// ta se skriva, potreben respondent iz CMS, da dobi NAME
			echo '<span id="alert_respondent_cms_instruction" class="'.( $rowAlert['finish_respondent_cms'] == 1 ? '' : 'displayNone').'">'.$lang['srv_alert_instruction2'].'</span>';
			echo '<p>'.$lang['srv_alert_instruction_survey'].'</p>';
			echo '<p>'.$lang['srv_alert_instruction_date'].'</p>';
			echo '<p>'.$lang['srv_alert_instruction_site'].'</p>';
			echo '<p>'.$lang['srv_alert_instruction_url'].'</p>';
			echo '<p>'.$lang['srv_alert_instruction_pdf'].'</p>';
			echo '<p>'.$lang['srv_alert_instruction_rtf'].'</p>';

			$row = SurveyInfo::getInstance()->getSurveyRow();
			# če imamo prepoznavanje uporabnik iz CMS, potem ponudimo tudi META_REFERAL_URL
			if ($rowAlert['finish_respondent_cms'] == 1)
			echo '<p>'.$lang['srv_alert_instruction_meta_referer_url'].'</p>';

            echo '</div>';

            echo '<div class="izpolnjena_spremenljivke">';

			$sqlSistemske = sisplet_query("SELECT s.id, s.naslov, s.variable FROM srv_spremenljivka s, srv_grupa g WHERE s.sistem='1' AND s.gru_id=g.id AND g.ank_id='$anketa' ORDER BY g.vrstni_red, s.vrstni_red");
			$prefix = "";
			
			while ($rowSistemske = mysqli_fetch_assoc($sqlSistemske)) {
				$prefix = $prefix . $rowSistemske['variable'] . ', ';
			}
			if($prefix != "") {	
				echo '<p class="bold bottom16">'.$lang['srv_alert_instruction_available'].'</p>';
				echo '<p class="bottom16">'.rtrim($prefix, ", ").'</p>';
				echo '<p class="bold bottom16">'.$lang['srv_alert_instruction_system'].'</p>';
				echo '<p>'.$lang['srv_alert_instruction_sample'].'</p>';
			}
			else{ // ni sistemskih spremenljivk
				echo '<p class="bold">'.$lang['srv_alert_no_sys_var'].'</p>';
			}

            echo '</div>';

            
			echo '</div>';
            echo '</div>';

            echo '</div>';

            echo '</fieldset>';
			
			echo '<div class="button_holder">';
            echo '<button class="medium white-blue" onClick="window.location='.$preklici_url.';return false;">'.$lang['edit1338'].'</button>';              
            echo '<button class="medium blue" onclick="document.alertanketa_' . $rowS['id'] . '.submit(); return false;">'.$lang['edit1337'].'</button>';
            echo '</div>';
			
			if (isset($_GET['s']) && $_GET['s'] == '1') {
				echo '<div id="success_save"></div>';
				echo '<script type="text/javascript">$(document).ready(function() {actionNotePopup(\'success_save\', \'success\');});</script>';
			}
			echo '  </form>';
		} else if ($tab == 'expired') {
			global $site_path, $global_user_id;

			//Obveščanje o izteku ankete
			echo '  <form name="alertanketa_' . $anketa . '" action="ajax.php?a=editanketaalert&m='.$tab.'" method="post" autocomplete="off">' . "\n";
			echo '    <input type="hidden" name="anketa" value="' . $anketa . '" />' . "\n";
			echo '    <input type="hidden" name="location" value="' . $_GET['a'] . '" />' . "\n";
			echo '    <input type="hidden" name="m" value="' . $_GET['m'] . '" />' . "\n";
			
			echo '    <fieldset>';
			echo '    <legend>' . $lang['srv_alert_expired_2'] . '</legend>';
				
			echo '<div class="setting_holder">';
			echo '<label for="alert_expire_days">'.$lang['srv_alert_expire_days'].':</label>';
			echo '<input type="text" class="large w200" id="alert_expire_days" name="alert_expire_days" value="'.$rowAlert['expire_days'].'" size="3" >';
			echo '</div>';

			echo '<div class="setting_holder">';
			echo '<p class="bold">'. $lang['srv_alert_expire_expire_at'] .'</p>';
			echo $rowS['expire'].$lang['at'].'00:00';
			echo '</div>';

			echo '<div class="setting_holder">';
			echo '<p class="bold">'. $lang['srv_alert_expire_note_at'] .'</p>';
			echo '<p><span id="calc_alert_expire">'.$rowAlert['newdate'].'</span><span>'.$lang['at'].'01:00</span></p>';
			echo '</div>';

			echo '<div class="setting_holder">';
			echo '<span class="setting_title">'.$lang['srv_alert_expire_prejemniki'].'</span>';

			echo '<div class="setting_item" id="label_alert_expire_author">';
			echo '<input type="checkbox" name="alert_expire_author" id="alert_expire_author" value="1" onChange="change_alert_respondent(\'expire_author\', $(this));return false;"' . ($rowAlert['expire_author'] == 1 ? ' checked' : '') . '>';
			$this->display_alert_label('expire_author',($rowAlert['expire_author'] == 1));
			echo '</div>';

			echo '<div class="setting_item">';
			echo '<input type="checkbox" name="alert_expire_other"  id="alert_expire_other"  value="1"' . (($rowAlert['expire_other'] == 1 || ($rowAlert['expire_other_emails'] && $rowAlert['expire_other'] != 0)) ? ' checked' : '') . ' onchange="toggleStatusAlertOtherCheckbox(\'expire_other\');"><label for="alert_expire_other">' . $lang['email_prejemniki'] .'</label>';
			echo '</div>';
			
			echo '<div class="setting_item">';

			echo '<div class="setting_holder alert_other_emails '.($rowAlert['expire_other'] == 0 ? 'displayNone' : '' ).'" id="alert_holder_expire_other_emails">';
			echo '<label for="alert_expire_other_emails">' . $lang['email_prejemniki_one_per_line'] . ':</label>';
			echo '<textarea class="alert_other_emails" name="alert_expire_other_emails" id="alert_expire_other_emails" >' . $rowAlert['expire_other_emails'] . '</textarea>';
			echo '</div>';
			echo '</div>';

			echo '</div>';

			echo '<div id="obvescanje_sidebyside_holder">';
            echo '<div id="obvescanje_sidebyside_left">';

            echo '<p class="bold caps bottom16">'.$lang['message_content'].'</p>';

            echo '<div class="setting_holder">';
            echo '<label for="subject">' . $lang['subject'] . ': </label>';
            echo '<input type="text" class="large wauto" id="alert_delete_subject" id="alert_expire_subject" name="alert_expire_subject" value="' . ($rowAlert['expire_subject'] ? $rowAlert['expire_subject'] : $lang['srv_alert_expire_subject']) . '"/>';
            echo '</div>';

			if ($rowAlert['expire_text'] != ''){
                $text = $rowAlert['expire_text'];
            }
			else{
                // Podpis
                $signature = Common::getEmailSignature();
           
                $text = nl2br($lang['srv_alert_expire_text'].$signature);
            }

			// prikaze editor za ne-spremenljivko (za karkoli druzga pac)
            echo '<div class="setting_holder">';

            echo '<label class="bottom4" for="alert_expire_text">' . $lang['message'] . ':</label>';
            echo '<textarea name="alert_expire_text" id="alert_expire_text">' . $text . '</textarea>';

            echo '</div>';
            echo '</div>';

			//Navodila na desni
            echo '<div id="obvescanje_sidebyside_right">';
            echo '<div class="message_instructions">';
            echo '<p class="bold caps bottom16">'.$lang['srv_alert_instruction1'].'</p>';
            echo '<p class="bold bottom16">'.$lang['srv_alert_instruction1a'].':</p>';
            echo '<p class="bottom16">'.$lang['srv_alert_instruction4'].'</p>';
            echo '</div>';
            echo '</div>';

            echo '</div>';

            echo '</fieldset>';

			echo '<div class="button_holder">';
            echo '<button class="medium white-blue" onClick="window.location='.$preklici_url.';return false;">'.$lang['edit1338'].'</button>';              
            echo '<button class="medium blue" onclick="document.alertanketa_' . $rowS['id'] . '.submit(); return false;">'.$lang['edit1337'].'</button>';
            echo '</div>';

			if (isset($_GET['s']) && $_GET['s'] == '1') {
				echo '<div id="success_save"></div>';
				echo '<script type="text/javascript">$(document).ready(function() {actionNotePopup(\'success_save\', \'success\');});</script>';
			}
			echo '</form>';
		}

		// Obveščanje o spremembi aktivnosti ankete
		else if ($tab == 'active') {
			echo '  <form name="alertanketa_' . $anketa . '" action="ajax.php?a=editanketaalert&m='.$tab.'" method="post" autocomplete="off">' . "\n";
			echo '    <input type="hidden" name="anketa" value="' . $anketa . '" />' . "\n";
			echo '    <input type="hidden" name="location" value="' . $_GET['a'] . '" />' . "\n";
			echo '    <input type="hidden" name="m" value="' . $_GET['m'] . '" />' . "\n";

			echo '<fieldset>';
			echo '<legend>' . $lang['srv_alert_active_2'] . '</legend>';

			echo '<div class="setting_holder">';
            echo '<span class="setting_title">'.$lang['srv_alert_active_prejemniki'].'</span>';

            echo '<div class="setting_item" id="label_alert_active_author">';
            echo '<input type="checkbox" name="alert_active_author" id="alert_active_author" value="1" onChange="change_alert_respondent(\'active_author\', $(this));return false;"' . ($rowAlert['active_author'] == 1 ? ' checked' : '') . '>';
            $this->display_alert_label('active_author',($rowAlert['active_author'] == 1));
            echo '</div>';

			echo '<div class="setting_item">';
            echo '<input type="checkbox" name="alert_active_other"  id="alert_active_other"  value="1"' . (($rowAlert['active_other'] == 1 || ($rowAlert['active_other_emails'] && $rowAlert['active_other'] != 0)) ? ' checked' : '') . ' onchange="toggleStatusAlertOtherCheckbox(\'active_other\');"><label for="alert_active_other">' . $lang['email_prejemniki'] .'</label>';
            echo '</div>';
            
            echo '<div class="setting_item">';

            echo '<div class="setting_holder alert_other_emails '.($rowAlert['active_other'] == 0 ? 'displayNone' : '' ).'" id="alert_holder_active_other_emails">';
            echo '<label for="alert_active_other_emails">' . $lang['email_prejemniki_one_per_line'] . ':</label>';
            echo '<textarea class="alert_other_emails" name="alert_active_other_emails" id="alert_active_other_emails">' . $rowAlert['active_other_emails'] . '</textarea>';
            echo '</div>';
            echo '</div>';

            echo '</div>';

			echo '<div id="obvescanje_sidebyside_holder">';
            echo '<div id="obvescanje_sidebyside_left">';

            echo '<p class="bold caps bottom16">'.$lang['message_content'].'</p>';
            echo '<p class="bold bottom16">'.$lang['srv_alert_oblika_deactivate_note'].'</p>';

			echo '<div class="setting_holder">';
            echo '<label for="subject">' . $lang['subject'] . ': </label>';
            echo '<input type="text" class="large wauto" id="alert_active_subject0" name="alert_active_subject0" value="' . ($rowAlert['active_subject0'] ? $rowAlert['active_subject0'] : $lang['srv_alert_active_subject0']) . '"/>';
            echo '</div>';

			if ($rowAlert['active_text0'] != '') {
				$text0 = $rowAlert['active_text0'];
            } 
            else {
                // Podpis
                $signature = Common::getEmailSignature();

				$text0 = nl2br($lang['srv_alert_active_text0'].$signature);
			}

			// prikaze editor za ne-spremenljivko (za karkoli druzga pac)
            echo '<div class="setting_holder">';

            echo '<label class="bottom4" for="alert_active_text0">' . $lang['message'] . ':</label>';
            echo '<textarea name="alert_active_text0" id="alert_active_text0">' . $text0 . '</textarea>';

            echo '</div>';

			echo '<p class="bold top32 bottom16">'.$lang['srv_alert_oblika_activate_note'].'</p>';

			echo '<div class="setting_holder">';
            echo '<label for="subject">' . $lang['subject'] . ': </label>';
            echo '<input type="text" class="large wauto" id="alert_active_subject1" name="alert_active_subject1" value="' . ($rowAlert['active_subject1'] ? $rowAlert['active_subject1'] : $lang['srv_alert_active_subject1']) . '"/>';
            echo '</div>';

			if ($rowAlert['active_text1'] != '') {
				$text1 = $rowAlert['active_text1'];
            } 
            else {
                // Podpis
                $signature = Common::getEmailSignature();

				$text1 = nl2br($lang['srv_alert_active_text1'].$signature);
			}

			echo '<div class="setting_holder">';

            echo '<label class="bottom4" for="alert_active_text1">' . $lang['message'] . ':</label>';
            echo '<textarea name="alert_active_text1" id="alert_active_text1">' . $text1 . '</textarea>';

            echo '</div>';
            echo '</div>';
			
            //Navodila na desni
            echo '<div id="obvescanje_sidebyside_right">';
            echo '<div class="message_instructions">';
            echo '<p class="bold caps bottom16">'.$lang['srv_alert_instruction1'].'</p>';
            echo '<p class="bold bottom16">'.$lang['srv_alert_instruction1a'].':</p>';
            echo '<p class="bottom16">'.$lang['srv_alert_instruction5'].'</p>';
            echo '</div>';
            echo '</div>';

            echo '</div>';

            echo '</fieldset>';
			
			echo '<div class="button_holder">';
			echo '<button class="medium white-blue" onClick="window.location='.$preklici_url.';return false;">'.$lang['edit1338'].'</button>';				
			echo '<button class="medium blue" onclick="document.alertanketa_' . $rowS['id'] . '.submit(); return false;">'.$lang['edit1337'].'</button>';
			echo '</div>';
			
			if (isset($_GET['s']) && $_GET['s'] == '1') {
				echo '<div id="success_save"></div>';
				echo '<script type="text/javascript">$(document).ready(function() {actionNotePopup(\'success_save\', \'success\');});</script>';
			}
			echo '</form>';
		}
		
		else if ($tab == 'delete') {
			echo '  <form name="alertanketa_' . $anketa . '" action="ajax.php?a=editanketaalert&m='.$tab.'" method="post" autocomplete="off">' . "\n";
			echo '    <input type="hidden" name="anketa" value="' . $anketa . '" />' . "\n";
			echo '    <input type="hidden" name="location" value="' . $_GET['a'] . '" />' . "\n";
			echo '    <input type="hidden" name="m" value="' . $_GET['m'] . '" />' . "\n";

			//Obveščanje o izbrisu ankete
			echo '<fieldset>';
			echo '<legend>' . $lang['srv_alert_surveydelete'] . '</legend>';

			echo '<div class="setting_holder">';
			echo '<span class="setting_title">'.$lang['srv_alert_surveydelete_text'].'</span>';

			echo '<div class="setting_item" id="label_alert_delete_author">';
			echo '<input type="checkbox" name="alert_delete_author" id="alert_delete_author" value="1" onChange="change_alert_respondent(\'delete_author\', $(this));return false;"' . ($rowAlert['delete_author'] == 1 ? ' checked' : '') . '>';
			$this->display_alert_label('delete_author',($rowAlert['delete_author'] == 1));
			echo '</div>';

			echo '<div class="setting_item">';
			echo '<input type="checkbox" name="alert_delete_other"  id="alert_delete_other"  value="1"' . (($rowAlert['delete_other'] == 1 || ($rowAlert['delete_other_emails'] && $rowAlert['delete_other'] != 0)) ? ' checked' : '') . ' onchange="toggleStatusAlertOtherCheckbox(\'delete_other\');"><label for="alert_delete_other">' . $lang['email_prejemniki'] .'</label>';
			echo '</div>';
			
			echo '<div class="setting_item">';

			echo '<div class="setting_holder alert_other_emails '.($rowAlert['delete_other'] == 0 ? 'displayNone' : '' ).'" id="alert_holder_delete_other_emails">';
			echo '<label for="alert_delete_other_emails">' . $lang['email_prejemniki_one_per_line'] . ':</label>';
			echo '<textarea class="alert_other_emails" name="alert_delete_other_emails" id="alert_delete_other_emails">' . $rowAlert['delete_other_emails'] . '</textarea>';
			echo '</div>';
			echo '</div>';

			echo '</div>';

			echo '<div id="obvescanje_sidebyside_holder">';
			echo '<div id="obvescanje_sidebyside_left">';

			echo '<p class="bold caps bottom16">'.$lang['message_content'].'</p>';

			echo '<div class="setting_holder">';
			echo '<label for="subject">' . $lang['subject'] . ': </label>';
			echo '<input type="text" class="large wauto" id="alert_delete_subject" name="alert_delete_subject" value="' . ($rowAlert['delete_subject'] ? $rowAlert['delete_subject'] : $lang['srv_alert_delete_subject']) . '"/>';
			echo '</div>';

			if ($rowAlert['delete_text'] != '') {
				$text = $rowAlert['delete_text'];
            } 
            else {
                // Podpis
                $signature = Common::getEmailSignature();

				$text = nl2br($lang['srv_alert_delete_text'].$signature);
			}

			// prikaze editor za ne-spremenljivko (za karkoli druzga pac)
			echo '<div class="setting_holder">';

			echo '<label class="bottom4" for="alert_delete_text">' . $lang['message'] . ':</label>';
			echo '<textarea name="alert_delete_text" id="alert_delete_text">' . $text . '</textarea>';

			echo '</div>';
			echo '</div>';

			//Navodila na desni
			echo '<div id="obvescanje_sidebyside_right">';
			echo '<div class="message_instructions">';
			echo '<p class="bold caps bottom16">'.$lang['srv_alert_instruction1'].'</p>';
			echo '<p class="bold bottom16">'.$lang['srv_alert_instruction1a'].':</p>';
			echo '<p class="bottom16">'.$lang['srv_alert_instruction5a'].'</p>';
			echo '</div>';
			echo '</div>';

			echo '</div>';

			echo '</fieldset>';
			
			echo '<div class="button_holder">';
			echo '<button class="medium white-blue" onClick="window.location='.$preklici_url.';return false;">'.$lang['edit1338'].'</button>';				
			echo '<button class="medium blue" onclick="document.alertanketa_' . $rowS['id'] . '.submit(); return false;">'.$lang['edit1337'].'</button>';
			echo '</div>';

			if (isset($_GET['s']) && $_GET['s'] == '1') {
				echo '<div id="success_save"></div>';
				echo '<script type="text/javascript">$(document).ready(function() {actionNotePopup(\'success_save\', \'success\');});</script>';
			}
			echo '</form>';

		} else if ($tab == 'email_server') {
			
			// Gorenje tega nima
			if (!Common::checkModule('gorenje') && $admin_type == '0')
				$this->viewServerSettings();
			
			
		} else {
			print_r($tab);
		}


		?>
		<script type="text/javascript">
			alleditors_remove ();
		if ($("#alert_finish_text").length)
			create_editor("alert_finish_text", false);
		if ($("#alert_expire_text").length)
			create_editor("alert_expire_text", false);
		if ($("#alert_active_text0").length)
			create_editor("alert_active_text0", false);
		if ($("#alert_active_text1").length)
			create_editor("alert_active_text1", false);
		if ($("#alert_delete_text").length)
			create_editor("alert_delete_text", false);

		$("#alert_expire_days").bind("keyup", function(e) {
			oldVal = this.value;
			newVal = this.value;
            if (this.value.match(/[^0-9 ]/g)) {
                newVal = this.value.replace(/[^0-9 ]/g, '');
                this.value = newVal; 
            };
            if (oldVal == newVal) // da ne postamo za vsako malenkost :)
              recalc_alert_expire(newVal);
        });
		
		</script>
		<?php
	}
	
	function viewServerSettings(){
		global $lang;
		global $admin_type;
		global $global_user_id;
		global $mysql_database_name;
	
		echo '<fieldset id="email_streznik_anketa">';
		echo '<legend>'.$lang['srv_user_base_email_server_settings'].'</legend>';
	
		$row = SurveyInfo::getInstance()->getSurveyRow();
		
		// Opozorilo, ce imamo vklopljena vabila, da gre za iste nastavitve
		$isEmail = (int)SurveyInfo::getInstance()->checkSurveyModule('email');
		if($isEmail)
			echo '<p class="bottom16">'.$lang['srv_email_server_settings_warning'].'</p>';
	
	
		echo '<form name="settingsanketa_' . $row['id'] . '" action="ajax.php?a=editanketasettings&m=email_server" method="post" autocomplete="off">' . "\n\r";
		echo '	<input type="hidden" name="anketa" value="' . $this->anketa . '" />' . "\n\r";
		echo '  <input type="hidden" name="location" value="' . $_GET['a'] . '" />' . "\n\r";
		echo '  <input type="hidden" name="m" value="' . $_GET['m'] . '" />' . "\n";
		echo '  <input type="hidden" name="submited" value="1" />' . "\n\r";

		
		$MA = new MailAdapter($this->anketa);
		
		// Dostop za posiljanje mailov preko 1ka serverja
		$enabled1ka = ( $MA->is1KA() || (($admin_type == 0) && ($mysql_database_name == 'www1kasi' || $mysql_database_name == 'test1kasi' || $mysql_database_name == 'real1kasi' || $mysql_database_name == '1kaarnessi')) ) ? true : false;
		

		echo '<div class="setting_holder">';
		echo '<span class="setting_title">'.$lang['srv_email_setting_select_server'].'</span>';

		echo '<div class="setting_item">';
		echo '<input type="radio" id="SMTPMailMode0" name="SMTPMailMode" value="0" '.($MA->is1KA() ? 'checked ="checked" ' : '').' '.($enabled1ka ? '' : ' disabled="disabled"').' onclick="$(\'#send_mail_mode1, #send_mail_mode2\').hide();$(\'#send_mail_mode0\').show();">';
		echo '<label for="SMTPMailMode0">'.$lang['srv_email_setting_adapter0']. ' </label>';
		echo '</div>';
       
        // Google smtp je viden samo starim, kjer je ze vklopljen
		if($MA->isGoogle()){
			echo '<div class="setting_item">';
			echo '<input type="radio" name="SMTPMailMode" value="1" '.($MA->isGoogle() ? 'checked ="checked" ' : '').' onclick="$(\'#send_mail_mode0, #send_mail_mode2\').hide(); $(\'#send_mail_mode1\').show();">';
		    echo '<label>'.$lang['srv_email_setting_adapter1'].' </label>';
			echo '</div>';
		}

		echo '<div class="setting_item">';
		echo '<input type="radio" name="SMTPMailMode" value="2" '.($MA->isSMTP() ? 'checked ="checked" ' : '').' onclick="$(\'#send_mail_mode0, #send_mail_mode1\').hide(); $(\'#send_mail_mode2\').show();">';
		echo '<label>'.$lang['srv_email_setting_adapter2'].' </label>';
		echo Help :: display('srv_mail_mode');
		echo '</div>';
					
		echo '</div>';

		
		#1KA
		$enkaSettings = $MA->get1KASettings($raziskave=true);
		echo '<div id="send_mail_mode0" '.(!$MA->is1KA() ? ' class="displayNone"' : '').'>';
		echo '<span class="bold">'.$lang['srv_email_setting_settings'].'</span>';
		echo '<br />';	
		# from
		echo '<p><label>'.$lang['srv_email_setting_from'].'<span>'.$enkaSettings['SMTPFrom'].'</span><input type="hidden" name="SMTPFrom0" value="'.$enkaSettings['SMTPFrom'].'"></label>';
		echo '</p>';
		# replyTo
		echo '<p><label>'.$lang['srv_email_setting_reply'].Help::display('srv_obvescanje_odgovorZa').'<input type="text" name="SMTPReplyTo0" value="'.$enkaSettings['SMTPReplyTo'].'" ></label>';
		echo '</p>';
		echo '</div>';
		
		#GMAIL - Google
		$enkaSettings = $MA->getGoogleSettings();
		echo '<div id="send_mail_mode1" '.(!$MA->isGoogle() ? ' class="displayNone"' : '').'>';
		echo '<span class="italic">'.$lang['srv_email_setting_adapter1_note'].'</span><br />';
		echo '<br /><span class="bold">'.$lang['srv_email_setting_settings'].'</span><br />';
		# from
		echo '<p><label>'.$lang['srv_email_setting_from'].'<input type="text" name="SMTPFrom1" value="'.$enkaSettings['SMTPFrom'].'"></label>';
		echo '</p>';
		# replyTo
		echo '<p><label>'.$lang['srv_email_setting_reply'].Help::display('srv_obvescanje_odgovorZa').'<input type="text" name="SMTPReplyTo1" value="'.$enkaSettings['SMTPReplyTo'].'" ></label>';
		echo '</p>';
		#Password
		echo '<p><label>'.$lang['srv_email_setting_password'].'<input type="password" name="SMTPPassword1" placeholder="'.$lang['srv_email_setting_password_placeholder'].'"></label>';
		echo '</p>';
		echo '</div>';

		#SMTP
		$enkaSettings = $MA->getSMTPSettings();
		echo '<div id="send_mail_mode2" '.(!$MA->isSMTP() ? ' class="displayNone"' : '').'>';
		//echo '<span class="italic">'.$lang['srv_email_setting_adapter2_note'].'</span><br />';
		echo '<p class="semi-bold bottom16">'.$lang['srv_email_setting_settings'].'</p>';
		
		echo '<div class="setting_holder horizontal">';

		# from - NICE
		echo '<div class="setting_holder">';
		echo '<span class="setting_title">'.$lang['srv_email_setting_from_nice'].'</span>';
		echo '<input type="text" class="text large" name="SMTPFromNice2" value="'.$enkaSettings['SMTPFromNice'].'">';
		echo '</div>';

		# from
		echo '<div class="setting_holder">';
		echo '<span class="setting_title">'.$lang['srv_email_setting_from'].'</span>';
		echo '<input type="text" class="text large" name="SMTPFrom2" value="'.$enkaSettings['SMTPFrom'].'">';
		echo '</div>';

		echo '</div>';
		
		echo '<div class="setting_holder horizontal">';

		# replyTo
		echo '<div class="setting_holder">';
		echo '<span class="setting_title">'.$lang['srv_email_setting_reply'].Help::display('srv_obvescanje_odgovorZa').'</span>';
		echo '<input type="text" class="text large" name="SMTPReplyTo2" value="'.$enkaSettings['SMTPReplyTo'].'" >';
		echo '</div>';

		echo '</div>';

		echo '<div class="setting_holder horizontal">';

		#Username
		echo '<div class="setting_holder">';
		echo '<span class="setting_title">'.$lang['srv_email_setting_username'].'</span>';
		echo '<input type="text" class="text large" name="SMTPUsername2" value="'.$enkaSettings['SMTPUsername'].'" >';
		echo '</div>';

		#Password
		echo '<div class="setting_holder">';
		echo '<span class="setting_title">'.$lang['srv_email_setting_password'].'</span>';
		echo '<input type="password" class="text large" name="SMTPPassword2" placeholder="'.$lang['srv_email_setting_password_placeholder'].'">';
		echo '</div>';
		
		echo '</div>';

		echo '<div class="setting_holder horizontal">';

		#autentikacija
		echo '<div class="setting_holder">';
		echo '<span class="setting_title">'.$lang['srv_email_setting_autentication'].'</span>';
		
		echo '<div class="fold">';
	
		echo '<div class="setting_item">';
		echo '<input type="radio" id="SMTPAuth20" name="SMTPAuth2" value="0" '.((int)$enkaSettings['SMTPAuth'] != 1 ? 'checked ="checked" ' : '').'>';
		echo '<label for="SMTPAuth20">'. $lang['srv_email_setting_no'].'</label>';
		echo '</div>';

		echo '<div class="setting_item">';
		echo '<input type="radio" id="SMTPAuth21" name="SMTPAuth2" value="1" '.((int)$enkaSettings['SMTPAuth'] == 1 ? 'checked ="checked" ' : '').'>';
		echo '<label for="SMTPAuth21">'. $lang['srv_email_setting_yes'].'</label>';
		echo '</div>';
		
		echo '</div>';
		echo '</div>';

		#Varnost SMTPSecure
		echo '<div class="setting_holder">';
		echo '<span class="setting_title">'.$lang['srv_email_setting_encryption'].'</span>';
		echo '<div class="fold">';
	
		echo '<div class="setting_item">';
		echo '<input type="radio" name="SMTPSecure20" value="0" '.((int)$enkaSettings['SMTPSecure'] == 0 ? 'checked ="checked" ' : '').'>';
		echo '<label for="SMTPSecure20">'. $lang['srv_email_setting_encryption_none'].'</label>';
		echo '</div>';

		echo '<div class="setting_item">';
		echo '<input type="radio" name="SMTPSecure21" value="ssl" '.($enkaSettings['SMTPSecure'] == 'ssl' ? 'checked ="checked" ' : '').'>';
		echo '<label for="SMTPSecure21">'. $lang['srv_email_setting_encryption_ssl'].'</label>';
		echo '</div>';

		echo '<div class="setting_item">';
		echo '<input type="radio" name="SMTPSecure22" value="tls" '.($enkaSettings['SMTPSecure'] == 'tls' ? 'checked ="checked" ' : '').'>';
		echo '<label for="SMTPSecure22">'. $lang['srv_email_setting_encryption_tls'].'</label>';
		echo '</div>';
		
		echo '</div>';
		echo '</div>';

		echo '</div>';

		echo '<div class="setting_holder horizontal">';

		#port
		echo '<div class="setting_holder">';
		echo '<span class="setting_title">'.$lang['srv_email_setting_port'].$lang['srv_email_setting_port_note'].'</span>';
		echo '<input type="number" class="text large min="0" max="65535" name="SMTPPort2" value="'.(int)$enkaSettings['SMTPPort'].'">';
		echo '</div>';

		#host
		echo '<div class="setting_holder">';
		echo '<span class="setting_title">'.$lang['srv_email_setting_host'].'</span>';
		echo '<input type="text" class="text large" name="SMTPHost2" value="'.$enkaSettings['SMTPHost'].'" >';
		echo '</div>';
		
		echo '</div>';

		echo '<div class="setting_holder horizontal">';

		#delay
		echo '<div class="setting_holder">';
		echo '<span class="setting_title">'.$lang['srv_email_setting_smtp_delay'].' '.Help::display('srv_inv_delay').':</span>';
		echo '<select class="dropdown large" name="SMTPDelay2">'
		/*. '<option value="0" '.($enkaSettings['SMTPDelay']=="0"?'selected="selected"':'') .'>0 </option>'
		. '<option value="10000" '.($enkaSettings['SMTPDelay']=="10000"?'selected="selected"':'') .'>0.01 sec (max 100 / sec)</option>'
		. '<option value="20000" '.($enkaSettings['SMTPDelay']=="20000"?'selected="selected"':'') .'>0.02 sec (max 50 / sec)</option>'
		. '<option value="50000" '.($enkaSettings['SMTPDelay']=="50000"?'selected="selected"':'') .'>0.05 sec (max 20 / sec)</option>'
		. '<option value="100000" '.($enkaSettings['SMTPDelay']=="100000"?'selected="selected"':'') .'>0.1 sec (max 10 / sec)</option>'
		. '<option value="200000" '.($enkaSettings['SMTPDelay']=="200000"?'selected="selected"':'') .'>0.2 sec (max 5 / sec)</option>'*/
		. '<option value="500000" '.($enkaSettings['SMTPDelay']=="500000"?'selected="selected"':'') .'>0.5 sec (max 2 / sec)</option>'
		. '<option value="1000000" '.($enkaSettings['SMTPDelay']=="1000000"?'selected="selected"':'') .'>1 sec (max 1 / sec)</option>'
		. '<option value="2000000" '.($enkaSettings['SMTPDelay']=="2000000"?'selected="selected"':'') .'>2 sec (max 30 / min)</option>'
		. '<option value="4000000" '.($enkaSettings['SMTPDelay']=="4000000"?'selected="selected"':'') .'>4 sec (max 15 / min)</option>'
		. '<option value="5000000" '.($enkaSettings['SMTPDelay']=="5000000"?'selected="selected"':'') .'>5 sec (max 12 / min)</option>'
		. '<option value="10000000" '.($enkaSettings['SMTPDelay']=="10000000"?'selected="selected"':'') .'>10 sec (max 6 / min)</option>'
		. '<option value="20000000" '.($enkaSettings['SMTPDelay']=="20000000"?'selected="selected"':'') .'>20 sec (max 3 / min)</option>'
		. '<option value="30000000" '.($enkaSettings['SMTPDelay']=="30000000"?'selected="selected"':'') .'>30 sec (max 2 / min)</option>'
		. '</select>';
		echo '</div>';


		echo '</div>';

		
		echo '</form>';
		
		echo '</fieldset>';


		echo '<div class="button_holder">';
		// Gumb preveri nastavitve
        echo '<button class="medium white-black" onClick="showTestSurveySMTP(); return false;">'.$lang['srv_email_setting_btn_test'].'</button>';              
		// Gumb shrani
        echo '<button class="medium blue" onclick="document.settingsanketa_' . $row['id'] . '.submit(); return false;">'.$lang['srv_email_setting_btn_save'].'</button>';
        echo '</div>';
		
		
		if (isset($_GET['s']) && $_GET['s'] == '1') {
			echo '<script type="text/javascript">$(document).ready(function() {actionNotePopup(\'success_save\', \'success\');});</script>';
		}
	}
	

	/**
	 * @desc poslje maile userjem iz baze
	 */
	function usermailing() {//OSTANE
		global $lang;
		global $site_url;
		global $site_path;
		global $global_user_id;
		
		// preverimo aktivnost
		//$sql = sisplet_query("SELECT active FROM srv_anketa WHERE id = '$this->anketa'");
		//$row = mysqli_fetch_array($sql);
		$row = SurveyInfo::getInstance()->getSurveyRow();
		// ce ne postamo ali ce anketa ni aktivna
		if (!isset ($_POST['mailto']) || ($row['active'] != 1)) {
			echo '<div id="" style="float:left; width:50%;">';
			echo '<div id="anketa_aktivacija_note" class="div_error">';
			$this->anketa_aktivacija_note();
			echo'</div>';

			echo '<fieldset style="padding:10px; border:1px solid gray;"><legend>' . $lang['srv_mailing'] . ':</legend>';
			echo '<form name="frm_mailto_preview" id="frm_mailto_preview" action="index.php?anketa=' . $this->anketa . '&a=email&m=usermailing" method="post" autocomplete="off">';
			echo '<p><input name="mailto" value="all" type="radio" checked="">' . $lang['srv_mailing_all'] . '</p>';
			echo '<p><input name="mailto" value="norsp" type="radio">' . $lang['srv_mailing_nonrsp'] . '</p>';
			echo '<p><input name="mailto" value="rsp" type="radio">' . $lang['srv_mailing_rsp'] . '</p>';

			echo '<p><input name="mailto" id="radio_mailto_status" value="status" type="radio">'.$lang['srv_mailing_all_with_status'].':</p>';

			echo '<p><div style="padding-left:150px"><input name="mailto_status[]" value="0" type="checkbox">0 - ' . $lang['srv_userstatus_0'] . '</div></p>';
			echo '<p><div style="padding-left:150px"><input name="mailto_status[]" value="1" type="checkbox">1 - ' . $lang['srv_userstatus_1'] . '</div></p>';
			echo '<p><div style="padding-left:150px"><input name="mailto_status[]" value="2" type="checkbox">2 - ' . $lang['srv_userstatus_2'] . '</div></p>';
			echo '<p><div style="padding-left:150px"><input name="mailto_status[]" value="3" type="checkbox">3 - ' . $lang['srv_userstatus_3'] . '</div></p>';
			echo '<p><div style="padding-left:150px"><input name="mailto_status[]" value="4" type="checkbox">4 - ' . $lang['srv_userstatus_4'] . '</div></p>';
			echo '<p><div style="padding-left:150px"><input name="mailto_status[]" value="5" type="checkbox">5 - ' . $lang['srv_userstatus_5'] . '</div></p>';
			echo '<p><div style="padding-left:150px"><input name="mailto_status[]" value="6" type="checkbox">6 - ' . $lang['srv_userstatus_6'] . '</div></p>';

			echo '<script type="text/javascript">';
			echo '$(document).ready(function() {';
			echo '$(\'[name="mailto_status[]"]\').bind("click", function () {change_mailto_status();});';
			echo '$(\'[name="mailto"]\').bind("click", function(el) { change_mailto_radio(); });';
			echo '});';
			echo '</script>';

			echo '<div id="btn_mailto_preview_holder">';
			$this->displayBtnMailtoPreview($row);
			echo '</div>';
			//            echo '<input type="submit">';

			echo '</form>';
			echo '</fieldset>';
			echo '</div>';
			echo '<div id="mailto_right" style="float:left; width:50%;">';
			$sa = new SurveyAdmin(1, $this->anketa);
			$sa->show_mailto_users('all', null);
			echo '</div>';

		} else { // pošljemo emaile

			$errorMsg = null;
			//v odvisnosti od statusa polovimo emaile
			$mailto_radio = $_POST['mailto'];
			$mailto_status = (isset ($_POST['mailto_status']) && count($_POST['mailto_status']) > 0) ? implode(",", $_POST['mailto_status']) : null;
			$sa = new SurveyAdmin(1, $this->anketa);
			$arrayMailtoSqlString = $sa->getMailtoSqlString($mailto_radio, $mailto_status);
			$errorMsg = $arrayMailtoSqlString['errorMsg'];
			$sqlString = $arrayMailtoSqlString['sqlString'];

			// preberemo tekst za trenutno anketo
			$subject = "";
			$text = "";
			$sql_userbase_email = sisplet_query("SELECT * FROM srv_userbase_setting WHERE ank_id = '$this->anketa'");
			if (mysqli_num_rows($sql_userbase_email) > 0) {
				// anketa že ima nastavljen text
				$row_userbase_email = mysqli_fetch_array($sql_userbase_email);
			} else {
				// anketa še nima nastavljenega teksta, preberemo privzetega (id=1) iz tabele srv_userbase_invitations
				$sql_userbase_invitations = sisplet_query("SELECT * FROM srv_userbase_invitations WHERE id = 1");
				$row_userbase_email = mysqli_fetch_array($sql_userbase_invitations);
			}
			
			if ($row_userbase_email['replyto'] == '') {
				$sqluu = sisplet_query("SELECT email FROM users WHERE id = '$global_user_id'");
				$rowuu = mysqli_fetch_array($sqluu);
				$row_userbase_email['replyto'] = $rowuu['email'];
			}

			// poiščemo sistemske spremenljivke iz vsebine
			preg_match_all( "/#(.*?)#/s", $row_userbase_email['text'], $sisVars);
			// poiščemo sistemske spremenljivke iz vsebine
			$sisVars =$sisVars[1]; 

			// Poiščemo še sistemske spremenljivke iz ankete
			$sqlSistemske = sisplet_query("SELECT s.id, s.naslov, s.variable FROM srv_spremenljivka s, srv_grupa g WHERE s.sistem='1' AND s.gru_id=g.id AND g.ank_id='".$this->anketa."' ORDER BY g.vrstni_red, s.vrstni_red");
			if (mysqli_num_rows($sqlSistemske) > 0) {
				while ($rowSistemske = mysqli_fetch_assoc($sqlSistemske)) {
					if (!isset($sisVars[strtoupper($rowSistemske['variable'])]))
						$sisVars[] = strtoupper($rowSistemske['variable']);
				}
			}

			$sql_userbase = sisplet_query("SELECT MAX(b.tip) AS tip FROM srv_userbase b, srv_user u WHERE b.usr_id=u.id AND u.ank_id='$this->anketa'");
			if (!$sql_userbase)
			echo mysqli_error($GLOBALS['connect_db']);
			$row_userbase = mysqli_fetch_array($sql_userbase);
			$tip = $row_userbase['tip'] + 1;

			if ($errorMsg == null) {

				$sql = sisplet_query($sqlString);
				// preprečimo izisovanje warningov

				$warnings_msg = "";

				$send_success = array ();
				$send_errors = array ();
				//				ob_start();
				//				$htmlContent = ob_get_contents();

				//				ob_start();
				
				$cnt = 0;
				
				while ($row = mysqli_fetch_array($sql)) {

					// dodamo sistemske spremenljivke in poiščemo njihove vrednosti
					$userSysData = array();
					foreach ( $sisVars as $sysVar ) {

						$sqlUser = sisplet_query("SELECT d.text FROM srv_data_text".$this->db_table." d, srv_spremenljivka s , srv_grupa g
							                                    WHERE d.spr_id=s.id AND d.usr_id='" . $row['id'] . "' AND
							                                    s.variable = '".strtolower($sysVar)."' AND g.ank_id='" . $this->anketa . "' AND s.sistem = 1 AND s.gru_id=g.id
							                                    ");
						if (!$sqlUser)
						echo mysqli_error($GLOBALS['connect_db']);
						$rowUser = mysqli_fetch_assoc($sqlUser);
						if ($rowUser['text'] != null)
						$userSysData[strtolower($sysVar)] = $rowUser['text'];
					}
					$email = $userSysData['email'];

					if (trim($email) != '' && $email != null) {

						// shranimo komu in kdaj je kdo poslal mail
						sisplet_query("INSERT INTO srv_userbase (usr_id, tip, datetime, admin_id) VALUES ('$row[id]', '$tip', NOW(), '" . $this->uid() . "')");

						$url = SurveyInfo::getSurveyLink() . '?code=' . $row['pass'] . '';
						if (trim($row['pass']) != '') {
							$unsubscribe = $site_url . 'admin/survey/unsubscribe.php?anketa=' . $this->anketa . '&code=' . $row['pass'] . '';
						} else {
							$unsubscribe = $site_url . 'admin/survey/unsubscribe.php?anketa=' . $this->anketa . '&email=' . trim($email) . '&uid='.$row['id'];
						}
						

						// zamenjamo sistemske vrednosti
						$content = $row_userbase_email['text'];
						// za staro verzijo
						$content = str_replace('[URL]', '#URL#', $content);
						$content = str_replace('[CODE]', '#CODE#', $content);
						$content = str_replace(array (
							'#URL#',
							'#CODE#',
						), array (
							'<a href="' . $url . '">' . $url . '</a>',
						$row['pass'],
						), $content);
						$content = str_replace('#UNSUBSCRIBE#', '<a href="'.$unsubscribe.'">'.$lang['user_bye_hl'].'</a>', $content);

						// poiščemo prestale variable katere je potrebno zamenjati v vsebini
						preg_match_all( "/#(.*?)#/s", $content, $toReplace);
						foreach ($toReplace[0] as $key => $seed) {
							$content = str_replace($toReplace[0][$key], $userSysData[strtolower($toReplace[1][$key])],$content);
						}

						$subject = $row_userbase_email['subject'];

						try
						{
							$MA = new MailAdapter($this->anketa, $type='alert');
							$MA->addRecipients($email);
							if ($cnt++ == 0)
							{ # en mail pošljemo tudi na enklikanketa
								$MA->addRecipients('enklikanketa@gmail.com');
							}
							$resultX = $MA->sendMail(stripslashes($content), $subject);
							
						}
						catch (Exception $e)
						{
						}

						if ($resultX) {
							$status = 1; // poslalo ok
							$send_success[] = $email;
						} else {
							$status = 2; // ni poslalo
							$send_errors[] = $email;
						}

						// nastavimo status
						sisplet_query("INSERT INTO srv_userstatus (usr_id, tip, status, datetime) VALUES ('$row[id]', '$tip', '$status', NOW())");
						# laststatus updejtamo samo če je bil pred tem status 0 - email še ni poslan ali 2 -  napaka pri pošiljanju maila
						sisplet_query("UPDATE srv_user SET last_status = '$status' WHERE id = '$row[id]' AND last_status IN (0,2)");
						
					}
					// počistimo warninge
					//					ob_end_clean();

					//					echo $htmlContent;

				}
				echo '<b>Spodnje sporočilo:</b><br/><br/>' . $row_userbase_email['subject'] . ',<br/> ' . $row_userbase_email['text'] . '<br/>';
				if (count($send_success) > 0) {
					echo '<b>je bilo uspešno poslano na naslednje naslove:<br/></b>';
					foreach ($send_success as $email) {
						echo $email . ",<br/>";
					}
				}
				if (count($send_errors) > 0) {
					echo '<br/><b>ni bilo uspešno poslano. Pri pošiljanju na naslednje naslove je prišlo do napake:<br/></b>';
					foreach ($send_errors as $email) {
						echo $email . ",<br/>";
					}
				}
				//echo '<br/>Done';
				// izpipemo warninge na koncu

			} else {
				echo '<div id="div_error" class="red"><img src="icons/icons/error.png" alt="" vartical-align="middle" />' . $errorMsg . '</div>';
			}
		}
	}

	/**
	 * @desc prikaze tab Socialna omrezja
	 */
	function SN_Settings() {
		global $lang;
		global $site_url;
		global $site_path;

		echo '<p class="top16 bottom16">'.$lang['srv_social_settings_text1'].'</p>';
		echo '<p class="bottom16">'.$lang['srv_social_settings_text2'].$lang['srv_social_settings_text3'].$lang['srv_social_settings_text4'].'</p>';
		echo '<p>'.$lang['srv_social_settings_text5'].'<a href="index.php?anketa='.$this->anketa.'">'.$lang['srv_social_settings_text6'].'</a>.</p>';
		
		// Omrežja so sestavljane vprašanja. Začnejo se iz generatorja imen (name generator). S tem respondent (ego) navede objekte, prijatelje - alterje. , s katerim pridobimo imena pzanke in vprašanj. Druga kompnenta je zanka, ki za vse alterje določenega ega sproži enaka vprašanja. Dretja komponenta so vprašanja. Primer omrežja je tukaj, http://www.1ka.si/a/3510, vprašalnik pa najdemo v knjižnjic med  Primerov 1KA anket http://www.1ka.si/admin/survey/index.php?a=knjiznica
	}

	/**
	 * @desc prikaze tab arhivi
	 */
	function arhivi() {
		global $lang;
		
		echo '<fieldset>';
		echo '<legend >' . $lang['srv_questionnaire_archives'] . '</legend>';

		echo '<p class="bottom16">'.$lang['srv_questionnaire_archives_text'].'</p>';

		echo '<div class="setting_holder">';
		echo $lang['srv_questionnaire_archives_note'].':';
		echo '<input class="large w400" name="intro_opomba" id="intro_opomba" type="text"/>';
		echo '</div>';

		echo '<div class="button_holder bottom16">';
		echo '<button class="medium blue" type="button" href="index.php?anketa=' . $this->anketa . '&a=backup_create" onclick="create_archive_survey(\'' . $this->anketa .'\'); return false;">'.$lang['srv_backup_button'].'</button>';
		echo '</div>';
		
		// Seznam ustvarjenih arhivov
		$sql = sisplet_query("SELECT a.id, a.naslov, a.intro_opomba, a.insert_time, a.edit_time, CONCAT(i.name, ' ', i.surname) AS insert_name, CONCAT(e.name, ' ', e.surname) AS edit_name FROM srv_anketa a, users i, users e WHERE a.insert_uid=i.id AND a.edit_uid=e.id AND a.backup = '$this->anketa' AND a.active>='0' ORDER BY a.insert_time DESC");
		if (!$sql) echo mysqli_error($GLOBALS['connect_db']);
		
		if (mysqli_num_rows($sql) > 0) {
			echo '<p class="bold">' . $lang['srv_backup_list'] . ':</p>';

			echo '<table id="survey_archives_table">';

			echo '<tr>';
			echo '<th>'.$lang['srv_questionnaire_archives_date'].'</th>';
			echo '<th>'.$lang['srv_questionnaire_archives_by'].'</th>';
			echo '<th>'.$lang['srv_questionnaire_archives_note2'].'</th>';
			echo '<th></th>';
			echo '</tr>';

			while ($row = mysqli_fetch_array($sql)) {
				echo '<tr>';
				echo '<td>'.datetime($row['insert_time']).'</td>';
				echo '<td>'.$row['insert_name'].'</td>';

				if ($row['intro_opomba']=='')
					$opomba = "/";
				else if (strlen($row['intro_opomba']) > 40)
					$opomba = substr($row['intro_opomba'], 0, 37).'...';
				else
					$opomba = $row['intro_opomba'];

				echo '<td>'.$opomba.'</td>';
				
				//Ikone
				echo '<td class="right">';
				echo '<div class="icon_holder multiple">';
				echo '<a class="faicon" href="#" onclick="javascript:anketa_delete(\'' . $row['id'] . '\', \'' . $lang['srv_anketadeleteconfirm'] . '\'); return false;"><span class="faicon trash empty"></span></a>';
				echo '<button class="table-inline white-black caps" type="button" onclick="window.location.href=\'index.php?anketa=' . $row['id'] . '&a=backup_restore\'">'.$lang['srv_questionnaire_archives_activate'].'</button>';
				echo '</div>';
				echo '</td>';
				
				echo '</tr>';
			}
	
			echo '</table>';

		}
		
		
		
		echo '</fieldset>';
	}

	function arhivi_data() {
		global $lang;
		
		echo '<fieldset>';
		echo '<legend>' . $lang['srv_arhiv_data'] . '</legend>';

		echo '<p class="bottom16">'.$lang['srv_backup_data_text'].'</p>';

		echo '<div class="button_holder bottom16">';
		echo '<button class="medium blue" type="button" href="ajax.php?anketa=' . $this->anketa . '&a=backup_data" onclick="create_archive_survey_data(\'' . $this->anketa .'\'); return false;">'.$lang['srv_backup_data_label'].'</button>';
		echo '</div>';
		
		// Seznam ustvarjenih arhivov - 2. faza: kdo je arhiviral, opomba, možnost izbrisa
		$backups = array();
		if ($handle = opendir( dirname(__FILE__) . '/SurveyBackup/' )) {
		    while (false !== ($entry = readdir($handle))) {
		        if ($entry != "." && $entry != "..") {
		            $file = explode('-', $entry);
		            if ($file[0] == $this->anketa) {
						$backups[] = $entry;
		            }
		        }
		    }
		    closedir($handle);
		}	
		if (count($backups) > 0) {
			echo '<p class="bold">' . $lang['srv_backup_data_list'] . ':</p>';

			echo '<table id="survey_data_archives_table">';

			echo '<tr>';
			echo '<th>'.$lang['srv_questionnaire_archives_date'].'</th>';
			echo '<th></th>';
			echo '</tr>';

			foreach ($backups AS $file) {
				$e = explode('-', $file);
				$e[2] = str_replace('.1ka', '', $e[2]);
				$e[2] = str_replace('.', ':', $e[2]);
				$e[2] = substr($e[2], 0, -3);

				echo '<tr>';
				echo '<td>'.$e[1].' '.$e[2].'</td>';


				//Ikone
				echo '<td class="right">';
				echo '<div class="icon_holder multiple">';
				//echo '<a class="faicon" href="#" onclick="delete_funkcija_todo"><span class="faicon trash empty"></span></a>';
				echo '<button class="table-inline white-black caps" type="button" onclick="window.location.href=\'ajax.php?anketa='.$this->anketa.'&a=backup_restore&filename='.$file.'\'">'.$lang['srv_questionnaire_archives_activate'].'</button>';
				echo '</div>';
				echo '</td>';
				
				echo '</tr>';

			}
			echo '</table>';
		}
		
		echo '</fieldset>';
	}
	
	function arhivi_testdata() {
		global $lang;
		
		echo '<fieldset>';
		echo '<legend>'.$lang['srv_arhiv_testdata'].'</legend>';

        $link = SurveyInfo::getSurveyLink();
		$surveyName = SurveyInfo::getInstance()->getSurveyTitle();

		$preview_disableif = SurveySetting::getInstance()->getSurveyMiscSetting('preview_disableif');
        $preview_disablealert = SurveySetting::getInstance()->getSurveyMiscSetting('preview_disablealert');
        $preview_displayifs = SurveySetting::getInstance()->getSurveyMiscSetting('preview_displayifs');
        $preview_displayvariables = SurveySetting::getInstance()->getSurveyMiscSetting('preview_displayvariables');
        $preview_hidecomment = SurveySetting::getInstance()->getSurveyMiscSetting('preview_hidecomment');
        $preview_options = '' . ($preview_disableif == 1 ? '&disableif=1' : '') . ($preview_disablealert == 1 ? '&disablealert=1' : '') . ($preview_displayifs == 1 ? '&displayifs=1' : '') . ($preview_displayvariables == 1 ? '&displayvariables=1' : '') . ($preview_hidecomment == 1 ? '&hidecomment=1' : '') . '';

		echo '<p class="bottom16">'.$lang['srv_archive_test_data_text1'].'<a href="index.php?anketa=' . $this->anketa . '&a=testiranje&m=testnipodatki">'.$lang['srv_archive_test_data_text1a'].'</a>'.$lang['srv_archive_test_data_text2'].'<a href="' . $link . '&preview=on&testdata=on' . $preview_options . '" title="" target="_blank">'.$lang['srv_archive_test_data_text2a'].'</a>'.$lang['srv_archive_test_data_text3'].'</p>';

		//Nimamo nobenih vnosov
        $str_testdata_any = "SELECT count(*) as cnt, time_insert as time FROM srv_user WHERE ank_id='".$this->anketa."' AND (testdata='1' OR testdata='2') GROUP BY day(time_insert)";
        $query_testdata_any = sisplet_query($str_testdata_any);
        list($testdata_any) = mysqli_fetch_row($query_testdata_any);
        if ((int)$testdata_any == 0) {
            echo '<p class="top16">'.$lang['srv_archive_test_data_none1']. $surveyName . $lang['srv_archive_test_data_none2'] . $lang['srv_archive_test_data_none3'].'.</p>';
        }
        else { // Imamo vnose, izpišemo eno ali obe tabeli
			
			// Avtomatski vnosi
			$str_testdata = "SELECT count(*) as cnt, time_insert as time FROM srv_user WHERE ank_id='".$this->anketa."' AND (testdata='2') GROUP BY day(time_insert)";
			$query_testdata = sisplet_query($str_testdata);
			$table_exists1 = 0;
			while ($testdata = mysqli_fetch_array($query_testdata)) {
				// Izpišemo naslov in začetek tabele
				if($table_exists1==0 && $testdata['cnt']!=0) {
					echo '<p class="bold">'.$lang['srv_archive_test_data_timeline_auto'].$surveyName.':</p>';
					echo '<table id="testni_vnosi">';
					echo '<tr>';
					echo '<th>'.$lang['srv_archive_test_data_date'].'</th>';
					echo '<th class="right">'.$lang['srv_archive_test_data_count'].'</th>';
					echo '</tr>';

					$table_exists1=1;
				}

				echo '<tr>';
				echo '<td>'.date("d.m.Y",strtotime($testdata['time'])).'</td>';
				echo '<td class="right">'.$testdata['cnt'].'</td>';
				echo '</tr>';
			}

			if($table_exists1==1) {
				echo '</table>';
			}
			
			// Ročni vnosi
			$str_testdata = "SELECT count(*) as cnt, time_insert as time FROM srv_user WHERE ank_id='".$this->anketa."' AND (testdata='1') GROUP BY day(time_insert)";
			$query_testdata = sisplet_query($str_testdata);
			$table_exists2 = 0;
			while ($testdata = mysqli_fetch_array($query_testdata)) {
				// Izpišemo naslov in začetek tabele
				if($table_exists2==0 && $testdata['cnt']!=0) {
					echo '<p class="bold top16">'.$lang['srv_archive_test_data_timeline_manual'].$surveyName.':</p>';
					echo '<table id="testni_vnosi">';
					echo '<tr>';
					echo '<th>'.$lang['srv_archive_test_data_date'].'</th>';
					echo '<th class="right">'.$lang['srv_archive_test_data_count'].'</th>';
					echo '</tr>';

					$table_exists2=1;
				}

				echo '<tr>';
				echo '<td>'.date("d.m.Y",strtotime($testdata['time'])).'</td>';
				echo '<td class="right">'.$testdata['cnt'].'</td>';
				echo '</tr>';
			}

			if($table_exists2==1) {
				echo '</table>';
			}
		}
		echo '</fieldset>';
	}
	
	function arhivi_survey() {
		global $lang;
		
		// Izvoz samo ankete
		if($_GET['m'] == 'survey'){
			echo '<fieldset>';
			echo '<legend>'.$lang['srv_survey_archives_ie_title'].Help::display('srv_arhiv_vprasalnik').'</legend>';
			
			echo '<p>'.$lang['srv_survey_archives_note_survey'].'</p>';

			echo '<div class="button_holder top16 bottom0">';
			echo '<button class="medium blue" onClick="window.location.href=\'ajax.php?a=archive_download&anketa='.$this->anketa.'\';" >'.$lang['srv_survey_archives_export'].'</button>';              
			echo '</div>';

			echo '</fieldset>';
		}
		
		// Izvoz ankete s podatki
		else{
			echo '<fieldset>';
			echo '<legend>'.$lang['srv_survey_archives_ie_data_title'].Help::display('srv_arhiv_podatki').'</legend>';

			echo '<p>'.$lang['srv_survey_archives_note_survey_data'].'</p>';

			echo '<div class="button_holder top16 bottom0">';
			echo '<button class="medium blue" onClick="window.location.href=\'ajax.php?a=archive_download&anketa='.$this->anketa.'&data=true\';" >'.$lang['srv_survey_archives_export'].'</button>';              
			echo '</div>';

			echo '</fieldset>';	
		}
	}
	
	// Preveri ce gre za prvo popravljanje podatkov in ce da, potem ustvari arhiv podatkov
	function checkFirstDataChange($inserted=false){
		global $connect_db;

		$sql = sisplet_query('SELECT count(*) AS cnt FROM srv_tracking'.$this->db_table.' WHERE ank_id=\''.$this->anketa.'\' 
								AND (`get` LIKE \'%edit_data%\'
									OR (`get` LIKE \'%a: "data", m: "quick_edit"%\' AND `get` LIKE \'%post: "1"%\')
									OR (`get` LIKE \'%a: "dataCopyRow"%\')
									OR (`get` LIKE \'%a: "dataDeleteMultipleRow"%\')
									OR (`get` LIKE \'%a: "dataDeleteRow"%\')
									OR (`get` LIKE \'%urejanje: "1"%\' AND status=\'4\'))
							ORDER BY datetime DESC');
		$row = mysqli_fetch_array($sql);

		// Naredimo arhiv podatkov
		if($row['cnt'] == 0 || ($inserted && $row['cnt'] == 1)){
			SurveyCopy::setSrcSurvey($this->anketa);
			SurveyCopy::setSrcConectDb($connect_db);
			SurveyCopy::saveArrayFile($data=true);
		}
	}

	/**
	 * @desc skopira anketo 
	 */
	function anketa_copy($anketa = 0) {//OSTANE
		
		// stara kopija kode je v classu class.SurveyCopy.php na dnu :)
		
		global $connect_db;

		if ($anketa > 0)
			$this->anketa = $anketa;
			
		$site = isset($_GET['site']) ? $_GET['site'] : 0;

		SurveyCopy :: setSrcSurvey($this->anketa);
		SurveyCopy :: setSrcConectDb($connect_db);
		SurveyCopy :: setDestSite($site);

		$new_anketa_id = SurveyCopy :: doCopy();

		
		$napake = SurveyCopy :: getErrors();
		if (is_countable($napake) && count($napake) > 0)
			print_r($napake);

		if (!$new_anketa_id)
			die("Can not create new survey!");

		if (!$site || $site == 0)
			return $new_anketa_id;
		elseif ($site != -1)
			header("Refresh:1; url=index.php?anketa=$this->anketa&a=arhivi");
	}

	/**
	 * @desc kreira backup (skopira celotno anketo v novo)
	 */
	function backup_create($NoRedirect = false) {//OSTANE

		$anketa = $this->anketa_copy();

		sisplet_query("UPDATE srv_anketa SET backup='$this->anketa', active='0', naslov = CONCAT( naslov, ' ', DAY(NOW()), '.', MONTH(NOW()), '.', YEAR(NOW()) ), intro_opomba='{$_POST['intro_opomba']}' WHERE id='$anketa'");
		// vsilimo refresh podatkov
		SurveyInfo :: getInstance()->resetSurveyData();

                if ($NoRedirect == false) {
                    header("Refresh:1; url=index.php?anketa=$this->anketa&a=arhivi");
                    //header("Location: index.php?anketa=$this->anketa&a=arhivi");
                }
	}

	/**
	 * @desc kreira backup in da obvestilo o uspešnosti (skopira celotno anketo v novo)
	 */
	function backup_create_popup() {//OSTANE
		global $lang;
		$anketa = $this->anketa_copy();

		sisplet_query("UPDATE srv_anketa SET backup='$this->anketa', active='0', naslov = CONCAT( naslov, ' ', DAY(NOW()), '.', MONTH(NOW()), '.', YEAR(NOW()) ) WHERE id='$anketa'");
		// vsilimo refresh podatkov
		SurveyInfo :: getInstance()->resetSurveyData();

		if ($anketa > 0 || true) {
			echo $lang['srv_backup_create_popup_ok'];
		}
        echo '<span class="buttonwrapper floatRight spaceRight" title="'.$lang['srv_backup_create_popup_view'].'"><a class="ovalbutton ovalbutton_orange" href="#" onclick="archivePopupView(); return false;"><span>'.$lang['srv_backup_create_popup_view'].'</span></a></span>';
        echo '<span class="buttonwrapper floatRight spaceRight" title="'.$lang['srv_backup_create_popup_close'].'"><a class="ovalbutton ovalbutton_gray" href="#" onclick="archivePopupClose(); return false;"><span>'.$lang['srv_backup_create_popup_close'].'</span></a></span>';
		
	}

	/**
	 * @desc prenese arhivsko anketo v folderje
	 */
	function backup_restore() {//OSTANE

		$row = SurveyInfo::getInstance()->getSurveyRow();

		$active = 0;
		$backup = 0;

		$sql = sisplet_query("UPDATE srv_anketa SET active='$active', backup='$backup' WHERE id = '$this->anketa'");
		
		// vsilimo refresh podatkov
		SurveyInfo :: getInstance()->resetSurveyData();

		header("Location: index.php?anketa=$this->anketa");
	}
	
	/**
	* prikaze tab z opcijami za vnos
	*/
	function vnos () {
		global $lang;
				
		$row = SurveyInfo::getInstance()->getSurveyRow();
		
		echo '<fieldset><legend>'.$lang['settings'].'</legend>';
		
		echo '<p>'.$lang['srv_vnos_navodila'].'</p>';
		
		/*echo '</fieldset>';
		
		echo '<br />';
		
		echo '<fieldset>';
		echo '<legend>' . $lang['srv_cookie'] . '</legend>';*/
		
		//prepoznaj uporabnika iz sispleta
		echo '<form name="settingsanketa_' . $row['id'] . '" action="ajax.php?a=editanketasettings" method="post" autocomplete="off">' . "\n\r";
		echo '	<input type="hidden" name="anketa" value="' . $this->anketa . '" />' . "\n\r";
		echo '	<input type="hidden" name="grupa" value="' . $this->grupa . '" />' . "\n\r";
		echo '  <input type="hidden" name="location" value="vnos" />' . "\n\r";
		echo '  <input type="hidden" name="submited" value="1" />' . "\n\r";
				
		echo '<span class="nastavitveSpan3 bold" ><label>' . $lang['srv_mass_input'] . ':</label></span>';
		echo '            <input type="radio" name="mass_insert" value="1" id="mass_insert_1"' . ($row['mass_insert'] == 1 ? ' checked="checked"' : '') . ' /><label for="mass_insert_1">' . $lang['srv_mass_input_1'] . '</label>' . "\n\r";
		echo '            <input type="radio" name="mass_insert" value="0" id="mass_insert_0"' . ($row['mass_insert'] == 0 ? ' checked="checked"' : '') . ' /><label for="mass_insert_0">' . $lang['srv_mass_input_0'] . '</label>' . "\n\r";
		
		echo '<br />';
		echo '<br />';
		
		echo '</form>';		
		echo '</fieldset>';
		
		echo '<br />';
		
		echo '<span class="floatLeft spaceRight">';
		echo '<div class="buttonwrapper">';
		echo '<a class="ovalbutton ovalbutton_orange btn_savesettings" onclick="document.settingsanketa_'.$row['id'].'.submit(); return false;" href="#">';
		echo '<span>'.$lang['edit1337'].'</span>';
		echo '</a>';
		echo '</div>';
		echo '</span>';
		
		if (isset($_GET['s']) && $_GET['s'] == '1') {
			echo '<div id="success_save"></div>';
			echo '<script type="text/javascript">$(document).ready(function() {actionNotePopup(\'success_save\', \'success\');});</script>';
		}
	}
	
	/**
	 * @desc prikaze tab za uporabnost - nastavitve
	 */
	function uporabnost() {
		global $site_url;
		global $lang;

        $preklici_url = ltrim(str_replace("&s=1","",$_SERVER['REQUEST_URI']),"/");
        $preklici_url = "'". $site_url . $preklici_url . "'";


		SurveySetting::getInstance()->Init($this->anketa);

		if (count($_POST) > 0 && (isset($_POST['uporabnost_link']) || isset($_POST['uporabnost_razdeli']))) {
			$uporabnost_link = $_POST['uporabnost_link'];
			$uporabnost = $_POST['uporabnost'];
			sisplet_query("UPDATE srv_anketa SET uporabnost_link = '$uporabnost_link' WHERE id = '$this->anketa'");

			$sqlg = sisplet_query("SELECT id FROM srv_grupa WHERE ank_id = '$this->anketa'");
			while ($rowg = mysqli_fetch_array($sqlg)) {
				if ( isset($_POST['uporabnost_link_'.$rowg['id']])) {

					SurveySetting::getInstance()->setSurveyMiscSetting('uporabnost_link_'.$rowg['id'], $_POST['uporabnost_link_'.$rowg['id']]);
				}
			}

			if (isset($_POST['uporabnost_razdeli'])) {
				SurveySetting::getInstance()->setSurveyMiscSetting('uporabnost_razdeli', $_POST['uporabnost_razdeli']);
			}

			// vsilimo refresh podatkov
			SurveyInfo :: getInstance()->resetSurveyData();
		}

		$row = SurveyInfo::getInstance()->getSurveyRow();

		echo '<fieldset><legend>'.$lang['settings'].'</legend>';
		echo '<form action="index.php?anketa=' . $this->anketa . '&a=uporabnost" name="settingsanketa_'.$this->anketa.'" method="post">';

		if ($row['uporabnost_link'] == '')
			$row['uporabnost_link'] = 'http://';

        echo '<p class="bottom16">'.$lang['srv_vrsta_survey_note_4_3b'].'</p>';

		echo '<div class="setting_holder">';
		echo '<p>'.$lang['srv_uporabnost_link'].'</p>';
		echo '<input type="text" class="large w400" name="uporabnost_link" value="' . $row['uporabnost_link'] . '">';
		echo '</div>';

		$uporabnost_razdeli = SurveySetting::getInstance()->getSurveyMiscSetting('uporabnost_razdeli');

		echo '<div class="setting_holder">';
		echo '<span class="setting_title">'.$lang['srv_uporabnost_razdeli'].':</span>';

		echo '<div class="setting_item">';
		echo '<input type="radio" id="uporabnost_razdeli_0" name="uporabnost_razdeli" value="0" '.($uporabnost_razdeli!=1?' checked':'').'>';
		echo '<label for="uporabnost_razdeli_0">'.$lang['srv_vodoravno'].'</label>';
		echo '</div>';
		echo '<div class="setting_item">';
		echo '<input type="radio" id="uporabnost_razdeli_1" name="uporabnost_razdeli" value="1" '.($uporabnost_razdeli==1?' checked':'').'>';
		echo '<label for="uporabnost_razdeli_1">'.$lang['srv_navpicno'].' ('.$lang['srv_razdeli_dodatno'].'</label>';
		echo '</div>';
					
		echo '</div>';

		echo '<p class="top16">'.$lang['srv_uporabnost_warning'].'</p>';


		echo '</fieldset>';

		echo '<fieldset><legend>'.$lang['srv_uporabnost_nadaljne'].'</legend>';

		echo '<p class="bottom16">'.$lang['srv_uporabnost_link_stran'].'</p>';

		$sqlg = sisplet_query("SELECT id, naslov FROM srv_grupa WHERE ank_id = '$this->anketa' ORDER BY vrstni_red ASC");
		while ($rowg = mysqli_fetch_array($sqlg)) {
			$link = SurveySetting::getInstance()->getSurveyMiscSetting('uporabnost_link_'.$rowg['id']);
			if ($link == '')
				$link = 'http://';
			
			echo '<div class="setting_holder">';
			echo '<p>'.$rowg['naslov'].':</p>';
			echo '<input type="text" class="large w400" name="uporabnost_link_'.$rowg['id'].'" value="'.$link.'">';
			echo '</div>';
		}

		echo '</fieldset>';

		// Gumb shrani
		echo '<div class="button_holder">';
        echo '<button class="medium white-blue" onClick="window.location='.$preklici_url.'; return false;">'.$lang['edit1338'].'</button>';             
        echo '<button class="medium blue" onclick="document.settingsanketa_'.$this->anketa.'.submit(); return false;">'.$lang['edit1337'].'</button>';
        echo '</div>';

		echo '</form>';
	}
    
	/**
	 * @desc prikaze vnose v anketo
	 */
	function displayIzvozi() {
		global $lang, $site_url, $global_user_id;
		
		$sdf = SurveyDataFile::get_instance();
		$sdf->init($this->anketa);
        $sdf->prepareFiles(); 

		$headFileName   = $sdf->getHeaderFileName();
		$dataFileName 	= $sdf->getDataFileName();
		$dataFileStatus = $sdf->getStatus();
		
		if ($dataFileStatus== FILE_STATUS_NO_DATA 
			|| $dataFileStatus == FILE_STATUS_NO_FILE
			|| $dataFileStatus == FILE_STATUS_SRV_DELETED){
				echo '<fieldset><legend>'.$lang['srv_no_data'].'</legend>';
				Common::noDataAlert();
				echo '</fieldset>';
				return false;
    	}
    	/* if ($_GET['m'] == 'excel') {
   			echo '<form id="export_excel_form" target="_blank" action="ajax.php?t=export&anketa='.$this->anketa.'&a=doexport&m=excel" method="POST">';
    	} */ if($_GET['m'] == 'excel_xls') {
   			echo '<form id="export_excel_xls_form" target="_blank" action="ajax.php?t=export&anketa='.$this->anketa.'&a=doexport&m=excel_xls" method="POST">';
    	} else if($_GET['m'] == 'txt') {
   			echo '<form id="export_excel_form" target="_blank" action="ajax.php?t=export&anketa='.$this->anketa.'&a=doexport&m=excel" method="POST">';
    	} else if($_GET['m'] == 'spss') {
   			echo '<form id="export_spss_form" target="_blank" action="ajax.php?t=export&anketa='.$this->anketa.'&a=doexport&m=spss" method="POST">';
    	} else if($_GET['m'] == 'sav') {
   			echo '<form id="export_sav_form" target="_blank" action="ajax.php?t=export&anketa='.$this->anketa.'&a=doexport&m=sav" method="POST">';
		}
    				   
		$row = SurveyInfo::getInstance()->getSurveyRow();
		
		//sedaj se prikaze staticno ob vsakem izvozu
		/* if($_GET['m'] != 'export_PDF'){

			if(session_id() == '')
				session_start();
			
			echo '<div id="div_export_setting_show" class="divPopUp PopUpNarrow">';

			echo '<h2>'.$lang['settings'].'</h2>';
			echo '<div class="popup_close"><a href="#" onClick="$(\'#div_export_setting_show\').fadeOut(\'slow\'); $(\'#fade\').fadeOut(\'slow\');return false;">✕</a></div>';


			echo '<div class="setting_holder">';

			// Izvozi identifikatorje
			echo '<div class="setting_item">';
			echo '<input type="radio" name="exportSetting" id="hiddenSystem" value="2"'.
				((isset($_SESSION['exportHiddenSystem']) && $_SESSION['exportHiddenSystem'] == true) ? ' checked="checked"' : '') .
				' onchange="exportChangeCheckbox(\'exportHiddenSystem\');"/><label for="hiddenSystem">'.$lang['srv_export_hidden_system'];
			echo Help::display('exportSettings').'</label>';
			echo '</div>';

			// Izvozi podatke
			echo '<div class="setting_item">';
			echo '<input type="radio" name="exportSetting" id="onlyData" value="0"'.
				( (isset($_SESSION['exportOnlyData']) && $_SESSION['exportOnlyData'] == true) || (!isset($_SESSION['exportHiddenSystem']) && !isset($_SESSION['exportOnlyData']) && !isset($_SESSION['exportFullMeta'])) ? ' checked="checked"' : '') .
				' onchange="exportChangeCheckbox(\'exportOnlyData\');"/><label for="onlyData">'.$lang['srv_export_only_data']
				.'</label>';
			echo '</div>';

			// Izvozi podatke in parapodatke
			echo '<div class="setting_item">';
			echo '<input type="radio" name="exportSetting" id="fullMeta" value="1"'.
				((isset($_SESSION['exportFullMeta']) && $_SESSION['exportFullMeta'] == true) ? ' checked="checked"' : '') .
				' onchange="exportChangeCheckbox(\'exportFullMeta\');"/><label for="fullMeta">'.$lang['srv_export_full_meta'];
			echo Help::display('srv_export_full_meta').'</label>';
			echo '</div>';
						
			echo '</div>';

			echo '<div class="button_holder">';
			echo '<button class="medium blue" onClick="$(\'#div_export_setting_show\').fadeOut(\'slow\'); $(\'#fade\').fadeOut(\'slow\');return false;">'.$lang['srv_zapri'].'</button>';
			echo '</div>';

			echo '</div>';

		} */
		
		# v odvisnosti od $_GET['m'] prikazemo podstran
		if ($_GET['m'] == M_EXPORT_EXCEL_XLS) {
		
			echo '<fieldset><legend>'.$lang['srv_lnk_excel_xls'].'</legend>';

			$this->exportSettingsData();
			echo '<br>';

			/* echo $lang['srv_izvoz_Excel_xls_labels'];
			echo '<div class="setting_holder">';
	
			// Izvozi podatke kot stevilske vrednosti
			echo '<div class="setting_item">';
			echo '<input type="radio" name="exportLabelsSetting" id="exportCodes" value="0" checked="checked"/>
				<label for="fullMeta">'.$lang['srv_izvoz_Excel_xls_labels_codes'].'</label>';
			echo '</div>';

			// Izvozi podatke kot labele
			echo '<div class="setting_item">';
			echo '<input type="radio" name="exportLabelsSetting" id="exportLabels" value="1"/>
				<label for="onlyData">'.$lang['srv_izvoz_Excel_xls_labels_labels'].'</label>';
			echo '</div>';
		
			echo '</div>';

			echo '<br>'; */

			echo $lang['srv_izvoz_Excel_xls_note1'];
			echo '<div class="button_holder intext">';
			echo '<button class="medium blue" onclick="doExcelXlsExport(); return false;">'.$lang['srv_izvoz_Excel_xls_file'].'</button>';              
			echo '</div>';

			$this->filterNote();

			echo '<p>'.$lang['srv_izvoz_Excel_xls_prirocnik'].'</p>';	
			echo '</fieldset>';
		} 
		elseif ($_GET['m'] == M_EXPORT_TXT) {

			echo '<fieldset><legend>'.$lang['srv_lnk_excel'].'</legend>';

			$this->exportSettingsData();
			echo '<br>';
		
			echo '<div class="setting_holder">';
			echo '<span class="setting_title">'.$lang['srv_expor_excel_cell_delimiter'].Help::display('srv_izvozCSV_locitveni').':</span>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="export_delimit" id="export_delimit_semicolon" value="0" onchange="excelExportChangeDelimit(1); return false;" checked="checked"/>';
			echo '<label for="export_delimit_semicolon">'.$lang['srv_expor_excel_cell_delimiter1'].'</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="export_delimit" id="export_delimit_coma" value="1" onchange="excelExportChangeDelimit(2); return false;"/>';
			echo '<label for="export_delimit_coma">'.$lang['srv_expor_excel_cell_delimiter2'].'</label>';
			echo '</div>';
			echo '</div>';

			echo '<br>';

			echo '<div class="setting_holder">';
			echo '<span class="setting_title">'.$lang['srv_export_texts'].Help::display('srv_izvozCSV_tekst').':</span>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="export_headers" id="export_header_yes" value="1" checked="checked"/>';
			echo '<label for="export_header_yes">'.$lang['yes'].'</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="export_headers" id="export_header_no" value="0" />';
			echo '<label for="export_header_no">'.$lang['no'].'</label>';
			echo '</div>';
			echo '</div>';

			echo '<br>';

			echo '<div class="setting_holder">';
			echo '<span class="setting_title">'.$lang['srv_izvoz_txt_format'].'</span>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="export_format" id="export_format_csv" value="1" checked="checked"/>';
			echo '<label for="export_format_csv">'.$lang['srv_izvoz_txt_format_csv'].'</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="export_format" id="export_format_txt" value="0" />';
			echo '<label for="export_format_txt">'.$lang['srv_izvoz_txt_format_txt'].'</label>';
			echo '</div>';
			echo '</div>';

			echo '<br>';

			echo $lang['srv_izvoz_Excel_note_a'];
			echo '<div class="button_holder intext">';
			echo '<button class="medium blue" onclick="doExcelExport(); return false;">'.$lang['srv_izvoz_Excel_file'].'</button>';              
            echo '</div>';

			$this->filterNote();

			echo '<p>'.$lang['srv_izvoz_Excel_note_2'].'</p>';
			
		} 
		elseif ($_GET['m'] == M_EXPORT_SPSS) {		
		
			echo '<fieldset><legend>'.$lang['srv_lnk_spss'].'</legend>';	
			
			$this->exportSettingsData();
			echo '<br>';

			echo '<p class="bottom16">'.$lang['srv_izvoz_SPSS_faq'].'</p>';
			echo '<p class="bottom16">'.$lang['srv_izvoz_SPSS_note'].'</p>';

				
			echo '<p class="bold">'.$lang['srv_izvoz_step1'].'</p>';
			echo '<div class="export_indent">';
			echo '<p>'.$lang['srv_izvoz_step1_SPSS'].'</p>';
			echo '<div class="button_holder intext">';
			echo '<button class="medium blue" onclick="doSpssExport(\'yes\'); return false;">'.$lang['srv_izvoz_step1_SPSS_file'].'</button>';              
			echo '</div>';
			echo '</div>';

			echo '<p class="bold">'.$lang['srv_izvoz_step2'].'</p>';
			echo '<div class="export_indent">';
			echo '<p>'.$lang['srv_izvoz_step2a_SPSS'].'</p>';
			echo '<div class="button_holder intext">';
			echo '<button class="medium blue" onclick="doSpssExport(\'no\'); return false;">'.$lang['srv_izvoz_step2_SPSS_file'].'</button>';              
			echo '</div>';
			echo '<p class="top16">'.$lang['srv_izvoz_step2b_SPSS'].'</p>';
			echo '</div>';

			echo '<p class="bold">'.$lang['srv_izvoz_step3'].'</p>';
			echo '<div class="export_indent">';
			echo '<ul>';
			echo '<li>'.$lang['srv_izvoz_step3a_SPSS'].'</li>';
			echo '<li>'.$lang['srv_izvoz_step3b_SPSS'].'</li>';
			echo '<li class="no_bullet top16">'.$lang['srv_izvoz_step3c_SPSS'].'</li>';
			echo '</ul>';
			echo '</div>';

			echo '<p class="bold">'.$lang['srv_izvoz_step4'].'</p>';
			echo '<div class="export_indent">';
			echo '<p>'.$lang['srv_izvoz_step4_SPSS'].'</p>';
			echo '</div>';

			echo '<p class="bold">'.$lang['srv_izvoz_step5'].'</p>';
			echo '<div class="export_indent">';
			echo '<p>'.$lang['srv_izvoz_step5_SPSS'].'</p>';
			echo '</div>';

			$this->filterNote();

			echo '</fieldset>';
		} 
		/* elseif ($_GET['m'] == M_EXPORT_SAV) {	
		
			echo '<fieldset><legend>'.$lang['srv_lnk_sav'].'</legend>';	
			echo $lang['srv_izvoz_SAV_note1'];
			echo '<div class="button_holder intext">';
            echo '<button class="medium blue" onclick="doSAVExport(); return false;">'.$lang['srv_izvoz_SAV_file'].'</button>';              
            echo '</div>';
			echo '<p class="top16">'.$lang['srv_izvoz_SAV_note2a'].'<a href="index.php?anketa=' . $this->anketa . '&a=export&m=spss">'.$lang['srv_izvoz_SAV_note2b'].'</a>'.$lang['srv_izvoz_SAV_note2c'].'</p>';

			echo '</fieldset>';
		}  */
		elseif ($_GET['m'] == 'export_PDF') {
			
			$pageBreak = isset($_GET['pageBreak']) ? $_GET['pageBreak'] : 0;
			$type = isset($_GET['type']) ? $_GET['type'] : 0;
			$if = isset($_GET['if']) ? $_GET['if'] : 0;
			$font = isset($_GET['font']) ? $_GET['font'] : 12;
			
			echo '<span class="red bold">'.$lang['srv_export_questionnare_0'].'</span>';
			
			echo '<p>';
			echo $lang['srv_export_questionnare_1'].'<br /><br />';
			echo $lang['srv_export_questionnare_2'];
			echo '</p>';
			
			# PDF in RTF izvoz rezultatov
			echo '<fieldset>';
			echo '<legend >' . $lang['srv_lnk_PDF/RTF'] . '</legend>';
			echo '<a href="'.makeEncodedIzvozUrlString('izvoz.php?a=pdf_results&anketa=' . $this->anketa . '&pageBreak='.$pageBreak.'&type='.$type.'&if='.$if.'&font='.$font).'" target="_blank">' .
			'<span class="faicon pdf"></span>&nbsp;PDF - (Adobe Acrobat)</a>';
			echo '<br/>';

			echo '<a href="'.makeEncodedIzvozUrlString('izvoz.php?a=rtf_results&anketa=' . $this->anketa . '&pageBreak='.$pageBreak.'&type='.$type.'&if='.$if.'&font='.$font).'" target="_blank">';
			echo '<span class="faicon rtf"></span>&nbsp;DOC - (Microsoft Word)</a>';
			echo '</fieldset>';
			
			//vsak resp na svoji strani
			echo '<fieldset>';
			echo '<legend >' . $lang['settings'] . '</legend>';
			echo '<span class="nastavitveSpan1" ><label>' . $lang['srv_export_pagebreak'] . ':</label></span>';
			echo '<input type="radio" name="export_pagebreak" value="1" id="export_pagebreak_1" onclick="vnos_redirect(\''.$site_url.'admin/survey/index.php?anketa='.$this->anketa.'&a=export&m=export_PDF&pageBreak=1&type='.$type.'&if='.$if.'&font='.$font.'\');" '.($pageBreak == 1 ? ' checked' : '').' /><label>' . $lang['yes'] . '</label>';
			echo '<input type="radio" name="export_pagebreak" value="0" id="export_pagebreak_0" onclick="vnos_redirect(\''.$site_url.'admin/survey/index.php?anketa='.$this->anketa.'&a=export&m=export_PDF&pageBreak=0&type='.$type.'&if='.$if.'&font='.$font.'\');" '.($pageBreak == 0 ? ' checked' : '').' /><label>' . $lang['no1'] . '</label>';
			
			//dolg/kratek izpis vprasanj v pdf
			echo '<br />';
			echo '<span class="nastavitveSpan1" ><label>' . $lang['srv_displaydata_type'] . ':</label></span>';
			echo '<input type="radio" name="type" value="0" id="type_0" onclick="vnos_redirect(\''.$site_url.'admin/survey/index.php?anketa='.$this->anketa.'&a=export&m=export_PDF&pageBreak='.$pageBreak.'&type=0&if='.$if.'&font='.$font.'\');" '.($type == 0 ? ' checked' : '').' /><label>' . $lang['srv_displaydata_type0'] . '</label>';
			echo '<input type="radio" name="type" value="1" id="type_1" onclick="vnos_redirect(\''.$site_url.'admin/survey/index.php?anketa='.$this->anketa.'&a=export&m=export_PDF&pageBreak='.$pageBreak.'&type=1&if='.$if.'&font='.$font.'\');" '.($type == 1 ? ' checked' : '').' /><label>' . $lang['srv_displaydata_type1'] . '</label>';
			echo '<input type="radio" name="type" value="2" id="type_2" onclick="vnos_redirect(\''.$site_url.'admin/survey/index.php?anketa='.$this->anketa.'&a=export&m=export_PDF&pageBreak='.$pageBreak.'&type=2&if='.$if.'&font='.$font.'\');" '.($type == 2 ? ' checked' : '').' /><label>' . $lang['srv_displaydata_type2'] . '</label>';
			
			//prikaz if-ov
			echo '<br />';
			echo '<span class="nastavitveSpan1" ><label>' . $lang['srv_export_if'] . ':</label></span>';
			echo '<input type="radio" name="if" value="1" id="if_1" onclick="vnos_redirect(\''.$site_url.'admin/survey/index.php?anketa='.$this->anketa.'&a=export&m=export_PDF&pageBreak='.$pageBreak.'&type='.$type.'&if=1&font='.$font.'\');" '.($if == 1 ? ' checked' : '').' /><label>' . $lang['yes'] . '</label>';
			echo '<input type="radio" name="if" value="0" id="if_0" onclick="vnos_redirect(\''.$site_url.'admin/survey/index.php?anketa='.$this->anketa.'&a=export&m=export_PDF&pageBreak='.$pageBreak.'&type='.$type.'&if=0&font='.$font.'\');" '.($if == 0 ? ' checked' : '').' /><label>' . $lang['no1'] . '</label>';

			//velikost fonta
			echo '<br />';
			echo '<span class="nastavitveSpan1" ><label>' . $lang['srv_export_font'] . ':</label></span>';
			echo '<select name="font" onchange="vnos_redirect(\''.$site_url.'admin/survey/index.php?anketa='.$this->anketa.'&a=export&m=export_PDF&pageBreak='.$pageBreak.'&type='.$type.'&if='.$if.'&font=\'+this.value);">';
			for($i=8; $i<16; $i+=2){
				echo '<option value="'.$i.'" '.($i==$font ? ' selected' : '').'>'.$i.'</option>';
			}
			echo '</select>';
			//echo '<input type="radio" name="font" value="1" id="font_1" onclick="vnos_redirect(\''.$site_url.'admin/survey/index.php?anketa='.$this->anketa.'&a=export&m=export_PDF&pageBreak='.$pageBreak.'&type='.$type.'&if='.$if.'&font='.$font.'\');" '.($type == 1 ? ' checked' : '').' /><label>' . $lang['yes'] . '</label>';
			
			echo '</fieldset>';
		}
			
		echo '</form>';	
	}
	/**
	 * @desc prikaze vnose v anketo
	 */
	/*function displayVnosi() {//OSTANE
		global $lang;
		$row = SurveyInfo::getInstance()->getSurveyRow();
	 	if ($_GET['m'] == 'SN_izvozi') {
			echo '<div id="anketa_edit" style="min-height: 160px;">' . "\n\r";

			//Excel izvozi
			echo '<fieldset class="izvozi">';
			echo '<legend>EXCEL IZVOZI</legend>';
			echo '<div class="floatLeft" style="width:400px;">';
			echo '<p><a href="exportexcel.php?anketa=' . $this->anketa . '"><span>' . $lang['srv_export'] . ' EXCEL za EGE' . '</span></a></p> ' . "\n\r";
			echo '<p><a href="exportexcel.php?anketa=' . $this->anketa . '&tip=SN"><span>' . $lang['srv_export'] . ' EXCEL za ALTERJE' . '</span></a></p> ' . "\n\r";
			echo '</div>';

			echo '<div class="floatLeft"  style="width:auto">';
			echo '<div id="div_error">';
			//			echo '<img src="icons/icons/error.png" alt="" vartical-align="middle" />';
			echo $lang['srv_izvoz_Excel_note'] . '</div>';
			echo '</div>';
			echo ' </fieldset>';
			echo ' <br/>';

			//SPSS izvozi
			echo '<fieldset class="izvozi">';
			echo '<legend>SPSS IZVOZI ZA EGE</legend>';
			//			echo '<p>Ker se pri nekaterih SPSS verzijah pri izvozu podatkov pojavljajo tezave, je treba izvoz datoteke s podatki opraviti v EXCELu, nato pa s spodnjimi SPSS datotekami s strukturami (sintaksami) podatke uvoziti v SPSS iz EXCELa.</p>';
			echo '<div class="floatLeft" style="width:400px;">';
			echo '<p>' . $lang['srv_export'] . ' SPSS: <a href="exportspss.php?anketa=' . $this->anketa . '">' . $lang['srv_structure'] . '</a> ' . $lang['srv_and'] . '
			                     <a href="exportspss.php?anketa=' . $this->anketa . '&amp;podatki=yes">' . $lang['srv_data'] . '</a> </p>
			            <p>' . $lang['srv_notext'] . ':
			                <a href="exportspss.php?anketa=' . $this->anketa . '&amp;notext=yes">' . $lang['srv_structure'] . '</a> ' . $lang['srv_and'] . '
			                <a href="exportspss.php?anketa=' . $this->anketa . '&amp;notext=yes&amp;podatki=yes">' . $lang['srv_data'] . '</a>
			            </p>' . "\n\r";
			echo '<p>' . $lang['srv_metapodatki'] . ' SPSS: <a href="exportspss.php?anketa=' . $this->anketa . '&amp;meta=yes">' . $lang['srv_structure'] . '</a> ' . $lang['srv_and'] . '
				                     <a href="exportspss.php?anketa=' . $this->anketa . '&amp;podatki=yes&amp;meta=yes">' . $lang['srv_data'] . '</a> </p>';
			echo '</div>';
			echo '<div class="floatLeft"  style="width:800px">';
			echo '<div id="div_error">';
			//			echo '<img src="icons/icons/error.png" alt="" vartical-align="middle" />';
			echo $lang['srv_izvoz_SPSS_note'] . '</div>';
			echo '</div>';
			echo ' </fieldset>';

			// EGO
			echo ' <br/>';
			echo '<fieldset class="izvozi">';
			echo '<legend>SPSS IZVOZI ZA ALTERJE</legend>';
			echo '</fieldset>';
		} else { // data iz baze
			echo '<div id="anketa_edit">' . "\n\r";
			$this->displayData();
			echo ' </div>';
		}
	}
*/

	// nastavitve glede katere podatke se naj izvozi
	private function exportSettingsData(){
		global $lang; 
		global $global_user_id;

		//pridobi nastavitve za export ankete
		//kateri podatki so v export se sedaj veze na naketo (0-onlydata, 1-data in paradata, 2-identifikatorji)
		$sql_export_mode = sisplet_query("SELECT value FROM srv_user_setting_for_survey ".
			"WHERE sid = '" . $this->anketa . "' AND what='default_export_data_mode' AND uid='$global_user_id'");
		$export_mode = 0;
		if($sql_export_mode){
			$export_mode = mysqli_fetch_assoc($sql_export_mode);
			$export_mode = isset($export_mode['value']) ? $export_mode['value'] : 0;
		}

		echo $lang['srv_izvoz_settings_data'];

		echo '<div class="setting_holder">';

		// Izvozi podatke
		echo '<div class="setting_item">';
		echo '<input type="radio" name="exportSetting" id="onlyData" value="0"'.
			( $export_mode == 0 ? ' checked="checked"' : '') .
			' onchange="exportChangeCheckbox();"/><label for="onlyData">'.$lang['srv_export_only_data']
			.'</label>';
		echo '</div>';

		// Izvozi podatke in parapodatke
		echo '<div class="setting_item">';
		echo '<input type="radio" name="exportSetting" id="fullMeta" value="1"'.
			( $export_mode == 1 ? ' checked="checked"' : '') .
			' onchange="exportChangeCheckbox();"/><label for="fullMeta">'.$lang['srv_export_full_meta'];
		echo Help::display('srv_export_full_meta').'</label>';
		echo '</div>';

		//pridobi header, da vidimo, ce imamo identifikatorje
		$SDF = SurveyDataFile::get_instance();
        $SDF->init($this->anketa);
		$headFileName = $SDF->getHeaderFileName();
		$HEADERS = unserialize(file_get_contents($headFileName));

		// Izvozi identifikatorje, ce obstajajo
		if((isset($HEADERS['_settings']['count_system_data_variables']) && (int)$HEADERS['_settings']['count_system_data_variables'] > 0)){
			echo '<div class="setting_item">';
			echo '<input type="radio" name="exportSetting" id="hiddenSystem" value="2"'.
				( $export_mode == 2 ? ' checked="checked"' : '') .
				' onchange="exportChangeCheckbox();"/><label for="hiddenSystem">'.$lang['srv_export_hidden_system'];
			echo Help::display('exportSettings').'</label>';
			echo '</div>';
		}
				
		echo '</div>';
	}

	private function filterNote(){
		global $lang;
		echo '<br><p>'.$lang['srv_izvoz_filter_note'].'</p><br>';
	}

	/**
	 * @desc prikaze podatke v tabeli
	 */
	function displayData() {
		global $lang;
		global $site_url;

		//include_once ('DisplaySurveyData.php');
		$dsd = new DisplaySurveyData($this->anketa);
		$dsd->display();
	}

	/**
	 * @desc Vrne ID trenutnega uporabnika (ce ni prijavljen vrne 0)
	 */
	function uid() {
		global $global_user_id;

		return $global_user_id;
	}

	/**
	 * @desc Vrne vse uporabnike iz baze
	 */
	static function db_select_users() {
		return sisplet_query("SELECT name, surname, id, email FROM users ORDER BY name ASC");
	}

	/**
	 * @desc Vrne vse nepobrisane uporabnike iz baze
	 */
	private static function db_select_users_forLevel($anketa = null) {
		global $global_user_id, $admin_type;

		// tip admina:  0=>admin, 1=>manager, 2=>clan, 3=>user
		switch ( $admin_type ) {

            // admin vidi vse
			case 0: 
				return sisplet_query("SELECT name, surname, id, email FROM users WHERE status!='0' ORDER BY name ASC");
				break;

            // manager vidi ljudi pod sabo
			case 1: 	 
                if ($anketa === null)
                    return sisplet_query("SELECT a.name, a.surname, a.id, a.email FROM users a, srv_dostop_manage m WHERE a.status!='0' AND m.manager='" .$global_user_id ."' AND m.user=a.id");
                else
                    return sisplet_query("SELECT a.name, a.surname, a.id, a.email FROM users a, srv_dostop_manage m WHERE a.status!='0' AND m.manager='" .$global_user_id ."' AND m.user=a.id UNION SELECT u.name, u.surname, u.id, u.email FROM users u, srv_dostop d WHERE d.ank_id='$anketa' AND d.uid=u.id");
                break;
			 	
			case 2:
			case 3:
				// TODO // clani in userji lahko vidijo samo tiste ki so jim poslali maile in so se registrirali	
				// ce smo v urejanju nastavitve ankete vidijo vse, ki so dodeljeni anketi, da jim lahko nastavijo
				if ($anketa === null)
					return sisplet_query("SELECT name, surname, id, email FROM users WHERE 1 = 0");
				else
					return sisplet_query("SELECT u.name, u.surname, u.id, u.email FROM users u, srv_dostop d WHERE u.status!='0' AND d.ank_id='$anketa' AND d.uid=u.id");
				break;
        }
        
		return null;
	}

	function display_dostop_users($show_all=0){
		global $global_user_id, $admin_type, $lang;
		
		$avtorRow = SurveyInfo::getInstance()->getSurveyRow();

		echo '<p class="bold">'.$lang['srv_dostop_users_list'].'</p>';
		
		// Prikazemo samo userje ki lahko urejajo anketo
		if($show_all == 0){

			echo '	<input type="hidden" name="dostop_edit" value="1" />' . "\n";

			echo '<table>';
			echo '<tr>';
			echo '<th>';
			//echo '<input type="checkbox" id="user_access_check_all" name="user_access_check_all"></input><label class="empty" for="user_access_check_all"></label>';
			echo '</th>';
			echo '<th>' . $lang['srv_dostop_users_username'] . '</th>';
			echo '<th>' . $lang['srv_dostop_users_email'] . '</th>';
			echo '<th></th>';
			echo '</tr>';
			
			$sql1 = sisplet_query("SELECT u.name, u.surname, u.id, u.email FROM users u, srv_dostop d WHERE d.ank_id='$this->anketa' AND d.uid=u.id");
			while ($row1 = mysqli_fetch_array($sql1)) {

				// Da ga ne pocistimo, ce je disablan (sam sebe ne more odstranit in avtorja se ne sme odstranit)
				if($avtorRow['insert_uid'] == $row1['id'] || $global_user_id == $row1['id'])
					echo '	<input type="hidden" name="uid[]" value="' . $row1['id'] . '" />' . "\n";

				echo '<tr id="div_for_uid_' . $row1['id'] . '" name="dostop_active_uid">';
				
                echo '  <td><input type="checkbox" name="uid[]" value="' . $row1['id'] . '" id="uid_' . $row1['id'] . '" checked="checked" '.($avtorRow['insert_uid'] == $row1['id'] || $global_user_id == $row1['id'] ? ' disabled="disabled"' : '').' autocomplete="off"/><label class="empty" for="uid_' . $row1['id'] . '" title="' . $row1['email'] . '"></label></td>';
				echo '  <td>'.$row1['name'] . ($avtorRow['insert_uid'] == $row1['id'] ? ' (' . $lang['author'] . ')' : '') .'</td>';
				echo '  <td>'.$row1['email'].'</td>';

                // Avtor ne rabi ikone za urejanje
                echo '  <td>';
                if($avtorRow['insert_uid'] != $row1['id'])
                    echo '      <div class="icon_holder multiple"><a><span class="faicon edit" onclick="javascript:anketa_user_dostop(\''.$row1['id'].'\');"></span></a>';
                echo '  </td>';
				
                echo '</tr>';
			}

			echo '</table>';

			//echo '<a class="noline undertable" href="#" onClick="________IZBRIS DOSTOPA ZA VSE OZNAČENE____________"><span class="faicon trash empty link-right"></span>'.$lang['srv_dostop_users_removeselected'].'</a>';

		}
		// Prikazemo vse userje, ki jih lahko uporabnik dodaja
		else{
			$sql1 = $this->db_select_users_forLevel($this->anketa);
			if ( mysqli_num_rows($sql1) > 0 ) {
				
				echo '<span id="dostop_active_show_1"><a href="#" onClick="dostopActiveShowAll(\'true\'); return false;">'.$lang['srv_dostop_show_all'].'</a></span>';
				echo '<span id="dostop_active_show_2" class="displayNone"><a href="#" onClick="dostopActiveShowAll(\'false\'); return false;">'.$lang['srv_dostop_hide_all'].'</a></span>';

				echo '	<input type="hidden" name="dostop_edit" value="1" />' . "\n";

				while ($row1 = mysqli_fetch_array($sql1)) {
					$sql2 = sisplet_query("SELECT ank_id, uid FROM srv_dostop WHERE ank_id='$this->anketa' AND uid='$row1[id]'");
					
					$checked = (mysqli_num_rows($sql2) > 0) ? ' checked="checked"' : '';
					
					// Da ga ne pocistimo ce je disablan (sam sebe ne more odstranit in avtorja se ne sme odstranit)
					if($avtorRow['insert_uid'] == $row1['id'] || $global_user_id == $row1['id'])
						echo '	<input type="hidden" name="uid[]" value="' . $row1['id'] . '" />' . "\n";

					echo '<div id="div_for_uid_' . $row1['id'] . '" name="dostop_active_uid" class="floatLeft dostop_for_uid'.$_css_hidden.'">' . "\n";
					echo '<label nowrap for="uid_' . $row1['id'] . '" title="' . $row1['email'] . '">';
                                        echo '<input type="checkbox" name="uid[]" value="' . $row1['id'] . '" id="uid_' . $row1['id'] . '" '.$checked.' '.($avtorRow['insert_uid'] == $row1['id'] || $global_user_id == $row1['id'] ? ' disabled="disabled"' : '').' autocomplete="off"/>' . "\n";
                                        echo $row1['name'] . ($avtorRow['insert_uid'] == $row1['id'] ? ' (' . $lang['author'] . ')' : '') . '</label>';
					if ($checked != '')
						echo ' <span class="faicon edit small icon-as_link" onclick="javascript:anketa_user_dostop(\''.$row1['id'].'\');"></span>';
					echo '</div>' . "\n";
				}
			}
		}
	}
	
    // Dodajanje uredniskega dostopa do ankete
    public function display_add_survey_dostop(){
        global $lang;
        global $admin_type;

        echo '<p class="red bottom16">';

        // Admini in managerji lahko dodajo dostop komurkoli
        if($admin_type == 0 || $admin_type == 1){
            echo $lang['srv_dostop_adduserstxt_admin'].' '.AppSettings::getInstance()->getSetting('app_settings-app_name').'. ';
        }
        // Ostali uporabniki lahko dodajo dostop samo ze registriranim uporabnikom
        else{
            echo $lang['srv_dostop_adduserstxt'].' '.AppSettings::getInstance()->getSetting('app_settings-app_name').'! ';
        }

        // AAI ima poseben link na help
        if(isAAI()){
            echo $lang['srv_dostop_adduserstxt_aai_more'];
        }
        
        echo '</p>';

        echo '<input type="hidden" name="aktiven" value="1" >';

		echo '<div class="setting_holder">';
        echo '<label for="addusers">'.$lang['srv_dostop_adduserstxt_email'].'</label>';
        echo '<textarea name="addusers" id="addusers"></textarea>';
        echo '</div>';
        
        echo '<p class="bottom16">'.$lang['srv_dostop_adduserstxt_end'].'</p>';

		//Opcijsko sporočilo
		echo '<div class="setting_holder">';
		echo '<div class="setting_item">';
		echo '<input type="checkbox" id="addusers_note_checkbox" onClick="dostopNoteToggle();">';
        echo '<label for="addusers_note_checkbox">'.$lang['srv_dostop_adduserstxt_note'].'</label>';
        echo '</div>';
		echo '<div class="setting_item">';
        echo '<textarea class="displayNone" name="addusers_note" id="addusers_note">'.$lang['srv_dostop_adduserstxt_note_text'].'</textarea>';
        echo '</div>';
        echo '</div>';

        // Gumb za dodajanje in posiljanje
		echo '<div class="button_holder below">';
		echo '<button class="medium blue" href="#" onclick="dostopAddAccess(); return false;">'.$lang['srv_dostop_addusers_button'].'</button>';
		echo '</div>';
	}


	/**
	 * @desc Vrne podatke o uporabniku
	 */
	static function db_select_user($uid) {
		return sisplet_query("SELECT name, surname, id, email FROM users WHERE id='$uid'");
	}

	/** Preveri ali uporabnik ustreza minimalni zahtevi statusa
	 * 
	 * @param $minimum_role_request minimalna zahteva (lahko podamo kot array posamezno)
	 * @return true/false
	 */
	function user_role_cehck($minimum_role_request = U_ROLE_ADMIN) {
		global $admin_type;

		if (is_array($minimum_role_request) && count($minimum_role_request) > 0) { // ce podamo kot array preverimo za vsak zapis posebej
			foreach ($minimum_role_request as $role) {
				if ($admin_type == $role)
				return true;
			}
		} else {
			if ($admin_type <= $minimum_role_request)
			return true;
		}
		return false;
	}
	var $getSurvey_type = null;
	function getSurvey_type($sid) {
		if ($this->getSurvey_type != null)
		return $this->getSurvey_type;

		// polovimo tip ankete
		$str_survey_type = sisplet_query("SELECT survey_type FROM srv_anketa WHERE id = '" . $sid . "'");
		$row_survey_type = mysqli_fetch_assoc($str_survey_type);
		$this->getSurvey_type = isset($row_survey_type['survey_type']) ? $row_survey_type['survey_type'] : null;
		return $this->getSurvey_type;
	}
	
	/**
	* TODO ???
	* 
	* @param mixed $what
	* @param mixed $isChecked
	* $forma - pri hitirh nastavitvah forme prikazemo nekje krajsi text
	*/
	function display_alert_label($what, $isChecked = false, $forma = false) {
		global $lang, $global_user_id;
		
		$custom_alert = array();
		$sql_custom_alert = sisplet_query("SELECT uid, type FROM srv_alert_custom WHERE ank_id = '$this->anketa'");
		while ($row_custom_alert = mysqli_fetch_array($sql_custom_alert)) {
			$custom_alert[$row_custom_alert['type']][$row_custom_alert['uid']] = 1;
		}
		
		switch ($what) {
			case 'finish_respondent_language': 	// respondent ki je zakljucil anketo v drugem jeziku (mu omogocimo nastavljanje custom maila za obvescanje)
				if ($isChecked) {
			
					$p = new Prevajanje($this->anketa);
					$p->dostop();
					$jeziki = $p->get_all_translation_langs();
					if(!empty($jeziki)){
					
						$row = SurveyInfo::getInstance()->getSurveyRow();
						
						foreach($jeziki as $key => $jezik){
							echo '<div class="setting_item respondent_jeziki">';
							echo '<label>'.$lang['srv_alert_respondent'].' - '.$jezik.'</label>';
							echo '<a href="#" onclick="alert_custom(\'respondent_lang_'.$key.'\', \'0\'); return false;" title="'.$lang['srv_alert_custom'].'"><span class="faicon text_file empty"></span></a>';
							echo '</div>';
						}					
					}
				}
				break;
			
			case 'finish_respondent': // respondent ki je zakljucil anketo

				if ($isChecked) {
					// preverimo, ali obstaja sistemska spremenljivka email, če ne jo dodamo
					$sqlEmail = sisplet_query("SELECT s.sistem, s.variable, s.id FROM srv_spremenljivka s, srv_grupa g WHERE s.sistem='1' AND s.variable='email' AND s.gru_id=g.id AND g.ank_id='$this->anketa'");
					$sqlIme = sisplet_query("SELECT s.sistem, s.variable, s.id FROM srv_spremenljivka s, srv_grupa g WHERE s.sistem='1' AND s.variable='ime' AND s.gru_id=g.id AND g.ank_id='$this->anketa'");
					// ce sta dodani obe sistemski spremenljivki, je vse ok
					$email_ok = $ime_ok = false;
					if ( mysqli_num_rows($sqlEmail) > 0 && mysqli_num_rows($sqlIme) > 0) {
						$email_ok = $ime_ok = true;
					}
					else {

						// manjka ena ali obe potrebni sistemski spremenljivki
						// email je nujen, zato ga dodamo avtomatsko
						if ( mysqli_num_rows($sqlEmail) == 0 ) {
							//dodamo email
							$sa = new SurveyAdmin(1, $this->anketa);
							if (in_array('email',$sa->alert_add_necessary_sysvar( array('email') , false))) {
								$email_ok = true;
							}
							// email v tem primeru spremenimo, da je viden, ker gre za alert
							$sqlEmail = sisplet_query("SELECT s.id FROM srv_spremenljivka s, srv_grupa g WHERE s.sistem='1' AND s.variable='email' AND s.gru_id=g.id AND g.ank_id='$this->anketa'");
							$rowEmail = mysqli_fetch_array($sqlEmail);
							sisplet_query("UPDATE srv_spremenljivka SET visible='1' WHERE id = '$rowEmail[id]'");
							// emailu po novem nastavimo preverjanje pravilnosti emaila in mehko opozorilo na to preverjanje
							$v = new Vprasanje($this->anketa);
							$v->spremenljivka = $rowEmail['id'];
							$v->set_email($reminder=1);
							
						}
						else {
							// email je ze dodan, damo tekst za ok
							$email_ok = true;
						}

						// preverimo še za ime
						if ( mysqli_num_rows($sqlIme) == 0 ) {
						}
						else{
							$ime_ok = true;
						}
					}
					echo '<input type="checkbox" name="alert_'.$what.'" id="alert_'.$what.'" value="1" onChange="change_alert_respondent(\''.$what.'\', $(this)); $(\'form[name=alertanketa_' . $this->anketa . ']\').submit(); return false;"' . (($isChecked) ? ' checked' : '') . '>';
					echo '<label for="alert_'.$what.'">'.$lang['srv_alert_respondent'].'</label>';
						
					if ($email_ok && $ime_ok) {
						echo ' <a href="#" onclick="alert_custom(\'respondent\', \'0\'); return false;" title="'.$lang['srv_alert_custom'].'"><span class="faicon text_file empty"></span></a>';
						$sql1 = sisplet_query("SELECT finish_respondent_if  FROM srv_alert WHERE ank_id='$this->anketa'");
						$row1 = mysqli_fetch_array($sql1);
						echo ' <a href="#" onclick="alert_edit_if(\'2\'); return false;"><span class="faicon if_add" '.($row1['finish_respondent_if']==0?'style=""':'').'></span></a> ';
						if ($row1['finish_respondent_if']>0) { if ($b==null) $b = new Branching($this->anketa); $b->conditions_display($row1['finish_respondent_if']); }
						
						echo '<span class="gray">'.$lang['srv_alert_respondent_note_ok_email_ime'].'&nbsp<span class="faicon fa-check-circle"></span></span>';
					
					} else {
						if ($ime_ok) {
							// pomeni da email ni ok! napaka
							echo '<span class="red">'.$lang['srv_alert_respondent_note_notok_email'].'</span>';
						} else {
							// email je ok, ime ni, uporabnika vprasamo, ali hoce se ime
							echo '<a href="#" onclick="alert_custom(\'respondent\', \'0\'); return false;" title="'.$lang['srv_alert_custom'].'"><span class="faicon text_file empty"></span></a>';
							$sql1 = sisplet_query("SELECT finish_respondent_if  FROM srv_alert WHERE ank_id='$this->anketa'");
							$row1 = mysqli_fetch_array($sql1);
							echo ' <a href="#" onclick="alert_edit_if(\'2\'); return false;"><span class="faicon if_add" '.($row1['finish_respondent_if']==0?'style=""':'').'></span></a>';
							if ($row1['finish_respondent_if']>0) { if ($b==null) $b = new Branching($this->anketa); $b->conditions_display($row1['finish_respondent_if']); }
							
							echo '<p class="gray top8">'.$lang['srv_alert_respondent_note_ok_email'].'</p>';
							
							if(!$forma){
								echo '<p class="top8"><span class="gray">'.$lang['srv_alert_respondent_note_ime'].'</span>';
								echo '<a class="noline" href="#" onClick="alert_add_necessary_sysvar(\'finish_respondent\', $(this)); return false;"><span class="faicon add respondent_iconwithlink"></span> '.$lang['srv_alert_respondent_note_link'].'</a></p>';
							}
						}
					}

					// preverimo sistemske nastavitve in spremenljivke ime

				} else { 
					echo '<input type="checkbox" name="alert_'.$what.'" id="alert_'.$what.'" value="1" onChange="change_alert_respondent(\''.$what.'\', $(this)); $(\'form[name=alertanketa_' . $this->anketa . ']\').submit(); return false;"' . (($isChecked) ? ' checked' : '') . '>';
					echo '<label for="alert_'.$what.'">'.$lang['srv_alert_respondent'].'</label>';
				}
				break;

			case 'finish_respondent_cms': // respondent prepoznan iz CMS ko je izpolnil anketo
				
                //respondent iz cms
                echo '<input type="checkbox" name="alert_'.$what.'" id="alert_'.$what.'" value="1" onChange="change_alert_respondent(\''.$what.'\', $(this)); change_alert_instruction($(this)); $(\'form[name=alertanketa_' . $this->anketa . ']\').submit(); return false;" ' . (($isChecked) ? ' checked' : '') . '>';
				echo '<label for="alert_finish_respondent_cms">'.$lang['srv_alert_respondent_cms'].'</label>';
					
				if ($isChecked) {
					// preverimo sistemske nastavitve in spremenljivke
					//$sqlCMS = sisplet_query("SELECT user_from_cms FROM srv_anketa WHERE id='$this->anketa'");
					//$rowCMS = mysqli_fetch_assoc($sqlCMS);
					$rowCMS = SurveyInfo::getInstance()->getSurveyRow();
					if ($rowCMS['user_from_cms'] > 0) {
						echo ' <a href="#" onclick="alert_custom(\'respondent_cms\', \'0\'); return false;" title="'.$lang['srv_alert_custom'].'"><span class="faicon text_file empty"></span></a>';
						
						$sql1 = sisplet_query("SELECT finish_respondent_cms_if  FROM srv_alert WHERE ank_id='$this->anketa'");
						$row1 = mysqli_fetch_array($sql1);
						echo ' <a href="#" onclick="alert_edit_if(\'3\'); return false;"><span class="faicon if_add" '.($row1['finish_respondent_cms_if']==0?'style=""':'').'></span></a> ';
						if ($row1['finish_respondent_cms_if']>0) { if ($b==null) $b = new Branching($this->anketa); $b->conditions_display($row1['finish_respondent_cms_if']); }
						
						echo '<span class="gray">'.$lang['srv_alert_respondent_cms_note_ok'].'&nbsp<span class="faicon fa-check-circle"></span></span>';

					} else {
						echo '<p class="top8"><span class="red">'.$lang['srv_alert_respondent_cms_note'].'</spam>';
						echo '<a class="noline" href="#" onClick="alert_change_user_from_cms(\'finish_respondent_cms\', $(this)); return false;"><span class="faicon add respondent_iconwithlink"></span> '.$lang['srv_alert_respondent_cms_note_link'].'</a></p>';
					}
				}
				break;
					
			case 'finish_author': // obveščanje o izpolnjeni anketi
			case 'expire_author': // obveščanje o poteku ankete
			case 'active_author': // obveščanje o aktivaciji/deaktivaciej ankete
			case 'delete_author': // obveščanje o izbrisu ankete

				// preberemo nastavitve alertov
				$sqlAlert = sisplet_query("SELECT * FROM srv_alert WHERE ank_id = '$this->anketa'");
				if (!$sqlAlert)
					echo mysqli_error($GLOBALS['connect_db']);
				
				if (mysqli_num_rows($sqlAlert) > 0) {
					$rowAlert = mysqli_fetch_array($sqlAlert);
				} 
				else {
					SurveyAlert::getInstance()->Init($this->anketa, $global_user_id);
					$rowAlert = SurveyAlert::setDefaultAlertBeforeExpire();
				}

				// avtor ankete
				if($forma) { 
					echo '<input type="checkbox" name="alert_'.$what.'" id="alert_'.$what.'" value="1" onChange="change_alert_respondent(\''.$what.'\', $(this));return false;"' . (($isChecked) ? ' checked' : '') . '>';
					echo '<label for="alert_'.$what.'">'.$lang['srv_alert_author2'].'</label>';
				}
				else {
					echo '<input type="checkbox" name="alert_'.$what.'" id="alert_'.$what.'" value="1" onChange="change_alert_respondent(\''.$what.'\', $(this));return false;"' . (($isChecked) ? ' checked' : '') . '>';
					echo '<label for="alert_'.$what.'">'.$lang['srv_alert_author'].'</label>';
				}
				if ($isChecked) {
					//$sql = sisplet_query("SELECT insert_uid, edit_uid FROM srv_anketa WHERE id='$this->anketa'");
					//$row = mysqli_fetch_assoc($sql);
					$b = null;
					
					$row = SurveyInfo::getInstance()->getSurveyRow();
					
					if ($what == 'finish_author')
						$db_field = 'alert_complete';
					else if ($what == 'expire_author')
						$db_field = 'alert_expire';
					else if ($what == 'active_author')
						$db_field = 'alert_active';
					else if ($what == 'delete_author')
						$db_field = 'alert_delete';
					
					// polovimo avtorja - novo kjer se ga lahko tudi izklopi (zaenkrat samo pri koncani anketi)
					if($what == 'finish_author'){
						$sqlAuthor = $this->db_select_user($row['insert_uid']);
						$rowAuthor = mysqli_fetch_array($sqlAuthor);
						$sql1 = sisplet_query("SELECT *, uid AS id FROM srv_dostop WHERE ank_id='$this->anketa' AND uid='".$row['insert_uid']."'");
						$row1 = mysqli_fetch_array($sql1);
						
						// Ce smo ravno z ajaxom vklopili obvescanje avtorja, ga tudi aktiviramo
						if(isset($_POST['checked']) && isset($_POST['what']) && $_POST['what']=='finish_author'){
							$checked = ($_POST['checked'] == true) ? ' checked="checked" ' : '';
						}
						else{
							$checked = ($row1[$db_field] == '1') ? ' checked="checked" ' : '';
						}

						echo '<div class="alert_authors">';
						echo '<div class="setting_item">';
						echo '<input type="checkbox" name="alert_'.$what.'_uid[]" value="' . $row['insert_uid'] . '" id="alert_'.$what.'_uid_' . $row['insert_uid'] . '"' . $checked . ' autocomplete="off"/>';
						echo '<label for="alert_'.$what.'_uid_' . $row['insert_uid'] . '" title="' . $rowAuthor['email'] . '">' . $rowAuthor['name'] . ' (' . $lang['author'] . ': '.$rowAuthor['email']. ')' . '</label>';

						if ($what == 'finish_author') {
							echo '<span>';
							echo ' <a href="#" onclick="alert_custom(\'author\', \''.$row['insert_uid'].'\'); return false;" title="'.$lang['srv_alert_custom'].'"><span class="faicon text_file empty"></span></a>';
							echo ' <a href="#" onclick="alert_edit_if(\'1\', \''.$row1['id'].'\'); return false;"><span class="faicon if_add" '.($row1['alert_complete_if']==0?'style=""':'').'></span></a> ';
							if ($row1['alert_complete_if']>0) { if ($b==null) $b = new Branching($this->anketa); $b->conditions_display($row1['alert_complete_if']); }
							echo '</span>';
						}
						echo '</div>';

						echo '</div>';
					}
					// polovimo avtorja	pri ostalih obvestilih
					else{				
						$sqlAuthor = $this->db_select_user($row['insert_uid']);
						$rowAuthor = mysqli_fetch_array($sqlAuthor);
						$sql1 = sisplet_query("SELECT *, uid AS id FROM srv_dostop WHERE ank_id='$this->anketa' AND uid='".$row['insert_uid']."'");
						$row1 = mysqli_fetch_array($sql1);
						
						echo '<div class="alert_authors">';
						echo '<div class="setting_item">';
						echo '<input type="checkbox" name="alert_'.$what.'_uid[]" value="' . $row['insert_uid'] . '" id="alert_'.$what.'_uid_' . $row['insert_uid'] . '" checked="checked" disabled="disabled" autocomplete="off"/>';
						echo '<label for="alert_'.$what.'_uid_' . $row['insert_uid'] . '" title="' . $rowAuthor['email'] . '">' . $rowAuthor['name'] . ' (' . $lang['author'] . ': '.$rowAuthor['email']. ')' . '</label>';
						
						if ($what == 'finish_author') {
							echo '<span>';
							echo ' <a href="#" onclick="alert_custom(\'author\', \''.$row['insert_uid'].'\'); return false;" title="'.$lang['srv_alert_custom'].'"><span class="faicon text_file empty"></span></a>';
							echo ' <a href="#" onclick="alert_edit_if(\'1\', \''.$row1['id'].'\'); return false;"><span class="faicon if_add" '.($row1['alert_complete_if']==0?'style=""':'').'></span></a> ';
							if ($row1['alert_complete_if']>0) { if ($b==null) $b = new Branching($this->anketa); $b->conditions_display($row1['alert_complete_if']); }
							echo '</span>';
						}
						echo '</div>';
						
						echo '</div>';
					}
					

					// polovimo ostale userje ki imajo dostop
					$sql1 = sisplet_query("SELECT u.id, u.name, u.surname, u.email, dostop.".$db_field.", dostop.alert_complete_if FROM users as u "
					." RIGHT JOIN (SELECT sd.uid, sd.".$db_field.", sd.alert_complete_if FROM srv_dostop as sd WHERE sd.ank_id='".$this->anketa."') AS dostop ON u.id = dostop.uid WHERE u.id != '".$row['insert_uid']."'");
					while ($row1 = mysqli_fetch_assoc($sql1)) {
						if ($row1['id']) { // se zgodi da je prazno za metauserje
							// avtor je vedno chekiran
							$checked = ( $row1[$db_field] == '1') ? ' checked="checked"' : '';
							echo '<div class="alert_authors">';
							echo '<div class="setting_item">';
							echo '<input type="checkbox" name="alert_'.$what.'_uid[]" value="' . $row1['id'] . '" id="alert_'.$what.'_uid_' . $row1['id'] . '"' . $checked . ' autocomplete="off"/>';
							echo '<label for="alert_'.$what.'_uid_' . $row1['id'] . '" title="' . $row1['email'] . '">' . $row1['name'] . ' ('.$row1['email'].')</label>';
						
							if ($what == 'finish_author') {
								echo '<span>';
								echo ' <a href="#" onclick="alert_custom(\'author\', \''.$row1['id'].'\'); return false;" title="'.$lang['srv_alert_custom'].'"><span class="faicon text_file empty"></span></a>';
								echo ' <a href="#" onclick="alert_edit_if(\'1\', \''.$row1['id'].'\'); return false;"><span class="faicon if_add" '.($row1['alert_complete_if']==0?'style=""':'').'></span></a> ';
								if ($row1['alert_complete_if']>0) { if ($b==null) $b = new Branching($this->anketa); $b->conditions_display($row1['alert_complete_if']); }
								echo '</span>';
							}
							echo '</div>';
							
							echo '</div>';
						}
					}
				}
				break;
		}

	}

	
	/**
	* TODO ???
	* 
	* @param mixed $row
	*/
	function showUserCodeSettings($row = null) {
		global $lang;

		if ($row == null) {
			$row = SurveyInfo::getInstance()->getSurveyRow();
		}

		$disabled = true;
		$disabled2 = false;
		if (SurveyInfo::getInstance()->checkSurveyModule('email') || SurveyInfo::getInstance()->checkSurveyModule('phone')){
			$disabled = false;
		}

		if ($row['usercode_skip'] == 1) {
			$disabled2 = true;
		}

		#echo '<span class="nastavitveSpan" >&nbsp;</span>';
		echo '<span ' . ($disabled ? 'class="gray"' : '') . '>' . $lang['usercode_skip'] . Help::display('usercode_skip') . ':';
		echo '<input type="radio" name="usercode_skip" value="0" id="usercode_skip_0"' . ($row['usercode_skip'] == 0 ? ' checked="checked"' : '') . ($disabled ? ' disabled="disabled"' : '') . ' onChange="handleUserCodeSkipSetting();"/><label for="usercode_skip_0">' . $lang['no1'] . '</label>' . "\n\r";
		echo '<input type="radio" name="usercode_skip" value="1" id="usercode_skip_1"' . ($row['usercode_skip'] == 1 ? ' checked="checked"' : '') . ($disabled ? ' disabled="disabled"' : '') . ' onChange="handleUserCodeSkipSetting();"/><label for="usercode_skip_1">' . $lang['yes'] . '</label>' . "\n\r";
		echo '<input type="radio" name="usercode_skip" value="2" id="usercode_skip_2"' . ($row['usercode_skip'] == 2 ? ' checked="checked"' : '') . ($disabled ? ' disabled="disabled"' : '') . ' onChange="handleUserCodeSkipSetting();"/><label for="usercode_skip_2">' . $lang['srv_setting_onlyAuthor'] . '</label>' . "\n\r";
		echo '</span>';
		echo '<br />';
		echo '<br/>';
		#echo '<span class="nastavitveSpan" >&nbsp;</span>';
		echo '<span ' . ($disabled /*|| $disabled2*/ ? 'class="gray"' : '') . '>' . $lang['usercode_required'] . help::display('usercode_required') .  ': ';
		echo '<input type="radio" name="usercode_required" value="0" id="usercode_required_0"' . ($row['usercode_required'] == 0 ? ' checked="checked"' : '') . ($disabled /*|| $disabled2*/ ? ' disabled="disabled"' : '') . ' onChange="handleUserCodeRequiredSetting();"/><label for="usercode_required_0">' . $lang['no1'] . '</label>' . "\n\r";
		echo '<input type="radio" name="usercode_required" value="1" id="usercode_required_1"' . ($row['usercode_required'] == 1 ? ' checked="checked"' : '') . ($disabled /*|| $disabled2*/ ? ' disabled="disabled"' : '') . ' onChange="handleUserCodeRequiredSetting();"/><label for="usercode_required_1">' . $lang['yes'] . '</label>' . "\n\r";
		echo '</span>';
		echo '<br/>';
		echo '<div id="div_usercode_text"'.(/*$row['usercode_skip'] == 1 || */$row['usercode_required'] == 0 ? ' class="displayNone"' : '').'>';
		$nagovorText = ($row['usercode_text'] && $row['usercode_text'] != null && $row['usercode_text'] != "") ? $row['usercode_text'] : $lang['srv_basecode'];
		#echo '<span class="nastavitveSpan2" >&nbsp;</span>';
		echo '<span ' . ($disabled ? 'class="gray"' : '') . '>' . $lang['usercode_text'] . ': ';
		echo '            <textarea name="usercode_text" ' . ($disabled ? ' disabled="disabled"' : '') . '>' . $nagovorText . '</textarea>' . "\n\r";
		echo '</span>';
		echo '</div>';
	}
	/**
	* TODO ???
	* 
	* @param mixed $row
	*/
	function respondenti_iz_baze($row = null, $show_fieldset=true) {
		global $lang;
		global $admin_type;
		
		if ($row == null) {
			$row = SurveyInfo::getInstance()->getSurveyRow();
		}
		
		/* aktivnost vec ni pogoj za vklop email vabil:
		 * -         omogočiti aktiviranje emial zavihka, četudi je anketa neaktivna (preprečiti pa pošijanje emailov če je ankete neaktivna)
		*/

		if ($admin_type <= 1) {
			$_cssDisabled = '';
			$_disabled = '';
		} else {
			$_cssDisabled = ' gray';
			$_disabled = ' disabled="disabled"';
		}
		
		echo '<input type="hidden" name="anketa" value="' . $this->anketa . '" />' . "\n\r";
		echo '<input type="hidden" name="grupa" value="' . $this->grupa . '" />' . "\n\r";
		echo '<input type="hidden" name="location" value="' . $_GET['a'] . '" />' . "\n\r";

		if ($show_fieldset) {
			echo '<fieldset>';
			echo '<legend class="'.$_cssDisabled.'">' . $lang['srv_user_base_vabila'] . '</legend>';
		} else {
			echo '<p class="strong">3. ' . $lang['srv_user_base_vabila'] . '</p>';
		}

		if ($_cssDisabled == '' && $_disabled == '') {
			echo '<span class="'.$_cssDisabled.'" ><label>' . $lang['srv_user_base_email'] . ':</label></span>';
			echo '            <input type="radio" name="email" value="1" id="email_1"' . (SurveyInfo::getInstance()->checkSurveyModule('email') ? ' checked="checked"' : '') . ' onChange="//handleUserCodeSetting();" '.$_disabled.'/><label for="email_1" class="'.$_cssDisabled.'">' . $lang['yes'] . '</label>' . "\n\r";
			echo '            <input type="radio" name="email" value="0" id="email_0"' . (!SurveyInfo::getInstance()->checkSurveyModule('email') ? ' checked="checked"' : '') . ' onChange="//handleUserCodeSetting();" '.$_disabled.'/><label for="email_0" class="'.$_cssDisabled.'">' . $lang['no1'] . '</label>' . "\n\r";
			echo '<br/>';
		}

		// dodatne nastavitve za pošiljanje kode pri izpolnjevanju ankete
		if ($_GET['a'] == 'vabila' || $_GET['a'] == 'email' )  {
			echo '<div id="userCodeSettings">';
			$this->showUserCodeSettings($row);
			echo '</div>';
		}
		
		if ($admin_type > 1)
			echo ''.$lang['srv_user_base_user_note'].'';
		if ($show_fieldset) {
			echo '</fieldset>';
		}
	}
	/**
	* TODO ???
	* 
	*/
	function anketa_aktivacija_note() {
		global $lang;
		$row = SurveyInfo::getInstance()->getSurveyRow();
		if ($row['active'] == 0) {
			echo $lang['srv_url_survey_not_active'];
			echo '	<span id="vabila_anketa_aktivacija" class="link_no_decoration">' . "\n\r";
			echo '		<a href="#" onclick="anketa_active(\'' . $this->anketa . '\',\'' . $row['active'] . '\'); return false;" title="' . $lang['srv_anketa_noactive'] . '">';
			echo '      <span class="faicon star icon-orange_very_dark"></span>';
			echo '      <span >' . $lang['srv_anketa_setActive'] . '</span>';
			echo '      </a>' . "\n\r";
			echo '	</span>' . "\n\r";
		} else {
			echo $lang['srv_url_intro_active'];
			echo '	<span id="vabila_anketa_aktivacija" class="link_no_decoration">' . "\n\r";
			echo '		<a href="#" onclick="anketa_active(\'' . $this->anketa . '\',\'' . $row['active'] . '\'); return false;" title="' . $lang['srv_anketa_active'] . '">';
			echo '      <span class="faicon star_on"></span>';
			echo '      <span >' . $lang['srv_anketa_setNoActive'] . '</span>';
			echo '      </a>' . "\n\r";
			echo '	</span>' . "\n\r";
		}
	}
	function anketa_diagnostika_note($diagnostics,$show_link = false) {
		global $lang;

		$diagnostics->printNote($show_link);
	}
	
	
	/**
	 * @desc prikaze dropdown z nastavitvami ankete (globalne, za celo 1ko) -- Prva stran -> Nastavitve -> Sistemske nastavitve
     * Sistemske nastavitve: mora biti admin da ima dostop
	 */
	function anketa_nastavitve_system() {
		global $lang;
		global $site_url;
		global $site_path;
		global $admin_type;
		global $global_user_id;

		$preklici_url = ltrim(str_replace("&s=1","",$_SERVER['REQUEST_URI']),"/");
		$preklici_url = "'". $site_url . $preklici_url . "'";

        // Ni admin - nima pravic
        if ($admin_type != 0) {

            echo '<div id="anketa_edit">';
            echo $lang['srv_settingsSystemNoRights'];
		    echo '</div>';	

            return;
        }


		echo '<div id="anketa_edit">';

        echo '<form name="settingsanketa" action="ajax.php?a=editanketasettings&m=system" method="post" autocomplete="off">';

        echo '  <input type="hidden" name="location" value="' . $_GET['a'] . '" />';
        echo '  <input type="hidden" name="submited" value="1" />';


        // SISTEMSKE NASTAVITVE (prej v settings_optional.php)
        echo '<fieldset><legend>'.$lang['as_basic'].'</legend>';
        
		echo '<div class="osnovne_nastavitve">';

        AppSettings::getInstance()->displaySettingsGroup('basic');

        // Kdo lahko ureja ankete
        echo '<div class="osnovne_nastavitve_single">';

        echo '<label>' . $lang['SurveyDostop'] . ':</label>';

        $result = sisplet_query("SELECT value FROM misc WHERE what='SurveyDostop'");
        list ($SurveyDostop) = mysqli_fetch_row($result);

        echo '<select class="dropdown" name="SurveyDostop">';
        echo '	<option value="0" '.($SurveyDostop=='0'?"SELECTED":"").'>'.$lang['forum_admin'].'</option>';
        echo '	<option value="1" '.($SurveyDostop=='1'?"SELECTED":"").'>'.$lang['forum_manager'].'</option>';
        echo '	<option value="2" '.($SurveyDostop=='2'?"SELECTED":"").'>'.$lang['forum_clan'].'</option>';
        echo '	<option value="3" '.($SurveyDostop=='3'?"SELECTED":"").'>'.$lang['forum_registered'].'</option>';
        echo '</select>';

        echo '</div>';

        // Default trajanje piskotka
        echo '<div class="osnovne_nastavitve_single">';
        echo '<label>' . $lang['SurveyCookie'] . ':</label>';

        $result = sisplet_query("SELECT value FROM misc WHERE what='SurveyCookie'");
        list ($SurveyCookie) = mysqli_fetch_row($result);

        echo '<select class="dropdown" name="SurveyCookie">';
        echo '	<option value="-1" '.($SurveyCookie=='-1'?"SELECTED":"").'>'.$lang['without'].'</option>';
        echo '	<option value="0" '.($SurveyCookie=='0'?"SELECTED":"").'>'.$lang['srv_cookie_0'].'</option>';
        echo '	<option value="1" '.($SurveyCookie=='1'?"SELECTED":"").'>'.$lang['srv_cookie_1'].'</option>';
        echo '	<option value="2" '.($SurveyCookie=='2'?"SELECTED":"").'>'.$lang['srv_cookie_2'].'</option>';
        echo '</select>';

        echo '</div>';

        echo '</div>';

        echo '</fieldset>';


        // INFO
        echo '<fieldset><legend>'.$lang['as_info'].'</legend>';
		echo '<div class="osnovne_nastavitve">';
        AppSettings::getInstance()->displaySettingsGroup('info');
        echo '</div>';
        echo '</fieldset>';


        // OMEJITVE
        echo '<fieldset><legend>'.$lang['as_limits'].'</legend>';
		echo '<div class="osnovne_nastavitve">';
        AppSettings::getInstance()->displaySettingsGroup('limits');
        echo '</div>';
        echo '</fieldset>';
        

        // SMTP NASTAVITVE
        echo '<fieldset><legend>'.$lang['as_smtp'].'</legend>';
		echo '<div class="osnovne_nastavitve">';
        AppSettings::getInstance()->displaySettingsGroup('smtp');
        echo '</div>';
        echo '</fieldset>';
        

        // MODULI
        echo '<fieldset><legend>'.$lang['as_modules'].'</legend>';
		echo '<div class="osnovne_nastavitve">';
        AppSettings::getInstance()->displaySettingsGroup('modules');
        echo '</div>';
        echo '</fieldset>';

        
        /* Missingi - zakomentirano, prenova po redizajnu
        $smv = new SurveyMissingValues();
        $smv->SystemFilters();
		*/
        
        
        // save gumb
		echo '<div class="button_holder">';
		echo '<button class="medium white-blue" onClick="window.location='.$preklici_url.';return false;">'.$lang['edit1338'].'</button>';				
		echo '<button class="medium blue" onclick="document.settingsanketa.submit();">'.$lang['edit1337'].'</button>';
		echo '</div>';
        
        // div za prikaz uspešnosti shranjevanja
        if (isset($_GET['s']) && $_GET['s'] == '1') {
            echo '<div id="success_save"></div>';
            echo '<script type="text/javascript">$(document).ready(function() {actionNotePopup(\'success_save\', \'success\');});</script>';
        }


		echo '</div>';		
	}

	/**
	* nastavitve predvidenih casov za komponente ankete in vprasanj iz katerih se nato racuna celotna dolzina vprasanj in ankete
	* 
	*/
	function anketa_nastavitve_predvidenicasi () {
		global $lang;
		global $site_url;
        
        $preklici_url = ltrim(str_replace("&s=1","",$_SERVER['REQUEST_URI']),"/");
        $preklici_url = "'". $site_url . $preklici_url . "'";

		echo '<div id="anketa_edit">';
		
		echo '<form name="timinganketa" method="post" action="ajax.php?a=editanketasettings&m=predvidenicasi">';
		
		echo '<fieldset><legend>'.$lang['srv_testiranje_predvidenicas'].'</legend>';

		echo '<p class="bottom16">'.$lang['srv_predvidenicasi_help'].'</p>';

		echo '<div class="setting_holder">';
		echo '<label>'.$lang['srv_timing_page'].'</label>';
		echo '<span><input type="text" class="text large w200" name="timing_stran" value="'.GlobalMisc::getMisc('timing_stran').'" />&nbsp&nbsps</span>';
		echo '</div>';

		echo '<p class="top16 bottom16">'.$lang['srv_predvidenicasi_note'].'</p>';

		echo '<div class="table-horizontal-scroll-wrapper1">';
		echo '<div class="table-horizontal-scroll-wrapper2">';

		echo '<table id="timinganketa_table">';
		echo '<tr>';
		echo '<th>'.$lang['srv_question_type'].'</th>';
		echo '<th>'.$lang['srv_timing_vprasanje'].'</th>';
		echo '<th>'.$lang['srv_timing_kategorija'].'</th>';
		echo '<th>'.$lang['srv_timing_kategorija_max'].'</th>';
		echo '</tr>';
		
		for ($tip=1; $tip<= 21; $tip++) {
			
			
			if ($tip <= 8 || $tip >= 16) {
				echo '<tr>';
				echo '<td class="left">'.$lang['srv_vprasanje_tip_'.$tip].'</td>';
				echo '<td class="nowrap"><input type="text" name="timing_vprasanje_'.$tip.'" value="'.GlobalMisc::getMisc('timing_vprasanje_'.$tip).'" />&nbsp&nbsps</td>';
				
				if ($tip<=3 || $tip==6 || $tip==16 || $tip==17 || $tip==18 || $tip==19 || $tip==20)
					echo '<td class="nowrap"><input type="text" name="timing_kategorija_'.$tip.'" value="'.GlobalMisc::getMisc('timing_kategorija_'.$tip).'" />&nbsp&nbsps</td>';
				else
					echo '<td></td>';
					
				if ($tip==3){
					$kategorija_max = GlobalMisc::getMisc('timing_kategorija_max_'.$tip);
					echo '<td class="nowrap"><input type="text" name="timing_kategorija_max_'.$tip.'" value="'.GlobalMisc::getMisc('timing_kategorija_max_'.$tip).'" />&nbsp&nbsps</td>';
				}
				else
					echo '<td></td>';
			
				echo '</tr>';

			}
		}
		echo '</table>';

		echo '</div>';
		echo '</div>';

		echo '</fieldset>';

		echo '<div class="button_holder">';
        echo '<button class="medium white-blue" onClick="window.location='.$preklici_url.'; return false;">'.$lang['edit1338'].'</button>';             
        echo '<button class="medium blue" onclick="document.timinganketa.submit();">'.$lang['edit1337'].'</button>';
        echo '</div>';
		
		echo '</form>';
		
		echo '</div>';
		
	}
	function tabTestiranje () {
        global $lang;
        
        // predvideni casi
		if ($_GET['m'] == 'predvidenicas') {	
			$this->testiranje_predvidenicas();
        } 
        // testni podatki
        elseif ($_GET['m'] == 'testnipodatki') {			
			$this->testiranje_testnipodatki();		
        } 
        // cas
        elseif ($_GET['m'] == M_TESTIRANJE_CAS) {		
			$this->testiranje_cas();;
        } 
        // cas
        elseif ($_GET['m'] == 'cas') {	
			$this->testiranje_cas();			
		}
	}
	
	/**
	* izracuna predvidene case po straneh glede na število in dolžino vprašanj
	* 
	*/
	function testiranje_predvidenicas($samo_izracunaj_skupini_cas=0) {
		global $lang;

		$expected_time = array();
        $expected_time_block = array();
        $block_labels_by_number = array();
		$expected_vprasanja = array();
		$verjetnost = array();
        $verjetnost_block = array();
                
        //from php 7.2 this helps to round numbers calculated in bcmod() - without it, it always rounds down to int
        bcscale(1);
		
		$sql = sisplet_query("SELECT introduction FROM srv_anketa WHERE id = '$this->anketa'");
		$row = mysqli_fetch_array($sql);
		$introduction = isset($row['introduction']) ? $row['introduction'] : $lang['srv_intro'];
		$expected_vprasanja[0][0] = strlen(strip_tags($introduction)) * GlobalMisc::getMisc('timing_vprasanje_5') / 100;
		
		$expected_vprasanja[0][1] = 1;
		$expected_vprasanja[0][2] = $lang['srv_vprasanje_tip_5'];
		$expected_vprasanja[0][3] = $lang['srv_intro_label'];
		
		$expected_time[0][0] = $expected_vprasanja[0][0] + GlobalMisc::getMisc('timing_stran');
		$expected_time[0][1] = $expected_time[0][0];
		
        $block_spr_data = $this->get_block_data_by_spr_id();
		
		$sql = sisplet_query("SELECT id, naslov, vrstni_red FROM srv_grupa g WHERE g.ank_id='$this->anketa' ORDER BY g.vrstni_red ASC");
		while ($row = mysqli_fetch_array($sql)) {
			
			$expected_time[$row['vrstni_red']][0] = 0;
			$expected_time[$row['vrstni_red']][1] = 0;
			
			$sql1 = sisplet_query("SELECT id, naslov FROM srv_spremenljivka WHERE gru_id='$row[id]' AND visible = '1'");
			while ($row1 = mysqli_fetch_array($sql1)) {
				
				$expected_vprasanja[$row1['id']][0] = $this->vprasanje_predvideni_cas($row1['id']);
				$expected_vprasanja[$row1['id']][1] = $this->vprasanje_verjetnost($row1['id']);
				$expected_vprasanja[$row1['id']][2] = strip_tags($row1['naslov']);
				$expected_vprasanja[$row1['id']][3] = strip_tags($row['naslov']);

				if(isset($block_spr_data[$row1['id']]['label']))
					$expected_vprasanja[$row1['id']][4] = strip_tags($block_spr_data[$row1['id']]['label']);
				else
					$expected_vprasanja[$row1['id']][4] = '';
                                    
				$expected_time[$row['vrstni_red']][0] += $expected_vprasanja[$row1['id']][0] * $expected_vprasanja[$row1['id']][1];	// dejanski
				$expected_time[$row['vrstni_red']][1] += $expected_vprasanja[$row1['id']][0];		// bruto - z vsemi vprasanji
				
				if (isset($verjetnost[$row['vrstni_red']]) && $expected_vprasanja[$row1['id']][1] > $verjetnost[$row['vrstni_red']])
					$verjetnost[$row['vrstni_red']] = $expected_vprasanja[$row1['id']][1];
			}
			
			if(isset($verjetnost[$row['vrstni_red']]))
				$expected_time[$row['vrstni_red']][0] += GlobalMisc::getMisc('timing_stran') * $verjetnost[$row['vrstni_red']];		// pri dejanskem trajanju strani upostevamo verjetnost najverjetnejsega vprasanja na strani (stran se pojavi z najvisjo verjetnostjo vseh vprasanj na strani)
			
				$expected_time[$row['vrstni_red']][1] += GlobalMisc::getMisc('timing_stran');
		}
                
        $sql = sisplet_query("SELECT * FROM srv_if as bl LEFT JOIN srv_branching as br ON br.parent = bl.id  WHERE bl.enabled='0' AND bl.tip='1' AND br.ank_id = '$this->anketa' ORDER BY bl.number ASC, br.vrstni_red ASC");
        $last_block_st = -1;
		while ($row = mysqli_fetch_array($sql)) {

            $new_block = $last_block_st != $row['number'];

            if($new_block){
                $last_block_st = $row['number'];
                $label = $row['label'] ? $row['label']: $lang['srv_blok'].' '.$last_block_st;
                $block_labels_by_number[$last_block_st] = $label;
                $expected_time_block[$last_block_st-1][0] = 0;
                $expected_time_block[$last_block_st-1][1] = 0;
            }
    
            if(!$row['element_if']){
                $expected_time_block[$last_block_st-1][0] += $expected_vprasanja[$row['element_spr']][0] * $expected_vprasanja[$row['element_spr']][1];	// dejanski
                $expected_time_block[$last_block_st-1][1] += $expected_vprasanja[$row['element_spr']][0];		// bruto - z vsemi vprasanji
                
                if ( !isset($verjetnost_block[$last_block_st-1]) || $expected_vprasanja[$row['element_spr']][1] > $verjetnost_block[$last_block_st-1])
                    $verjetnost_block[$last_block_st-1] = $expected_vprasanja[$row['element_spr']][1];
            }
            else{
                $sql1 = sisplet_query("SELECT * FROM srv_branching WHERE parent='".$row['element_if']."' ORDER BY vrstni_red ASC");
                while ($row1 = mysqli_fetch_array($sql1)) {
					if(array_key_exists($last_block_st, $expected_time_block)){
						$expected_time_block[$last_block_st-1][0] += $expected_vprasanja[$row1['element_spr']][0] * $expected_vprasanja[$row1['element_spr']][1];	// dejanski
						$expected_time_block[$last_block_st-1][1] += $expected_vprasanja[$row1['element_spr']][0];		// bruto - z vsemi vprasanji
						$expected_vprasanja[$row1['element_spr']][4] = $label;

						if ( !isset($verjetnost_block[$last_block_st-1]) || $expected_vprasanja[$row1['element_spr']][1] > $verjetnost_block[$last_block_st-1])
							$verjetnost_block[$last_block_st-1] = $expected_vprasanja[$row1['element_spr']][1];
					}
                }
            }
                    
            if($new_block){
                $expected_time_block[$last_block_st-1][0] += GlobalMisc::getMisc('timing_stran') * $verjetnost_block[$last_block_st-1];		// pri dejanskem trajanju strani upostevamo verjetnost najverjetnejsega vprasanja na strani (stran se pojavi z najvisjo verjetnostjo vseh vprasanj na strani)
                $expected_time_block[$last_block_st-1][1] += GlobalMisc::getMisc('timing_stran');
            }
		}

		// izpis za strani
		$max = 0;
		$total = 0;
		$total_brez_pogojev = 0;
		foreach ($expected_time AS $key => $val) {

			if ($val[1] > $max) 
                $max = $val[1];

			$total += $val[0];
			$total_brez_pogojev += $val[1];
		}
		if ($max == 0) return;
		
		if ($samo_izracunaj_skupini_cas == 2) {
			return $total;
		}
		
		$skupni_cas = (bcdiv($total, 60, 0)>0?bcdiv($total, 60, 0).'<span class="time_text">min</span> ':'').''.round(bcmod($total, 60), 0).'<span class="time_text">s</span>';
		$skupni_cas_brez_pogojev = (bcdiv($total_brez_pogojev, 60, 0)>0?bcdiv($total_brez_pogojev, 60, 0).'<span class="time_text">min</span> ':'').''.round(bcmod($total_brez_pogojev, 60), 0).'<span class="time_text">s</span>';
		
		if ($samo_izracunaj_skupini_cas == 1)
			return $skupni_cas;
		
		
		echo '<fieldset><legend>'.$lang['srv_total_trajanje'].'</legend>';
		echo '<p class="semi-bold">'.$lang['srv_dejansko_trajanje'].':</p>';
		echo '<p>'.$skupni_cas.'</p>';
		echo '</fieldset>';
		
		echo '<fieldset><legend>'.$lang['srv_casi_po_straneh'].'</legend>';
		echo '<table>';

		echo '<tr>';
		echo '<th>'.$lang['srv_casi_po_straneh_stran'].'</th>';
		echo '<th colspan="2">'.$lang['srv_neto_t_cas'].' / '.$lang['srv_bruto_t_cas'].'</th>';
		echo '</tr>';

		
		foreach ($expected_time AS $vrstni_red => $time) {
			$sql = sisplet_query("SELECT naslov FROM srv_grupa WHERE vrstni_red='$vrstni_red' AND ank_id = '$this->anketa'");
			$row = mysqli_fetch_array($sql);
			$naslov = isset($row['naslov']) && $row['naslov'] != '' ? $row['naslov'] : $lang['srv_intro_label'];
			
			echo '<tr>';
			echo '<td>'.$naslov.'</td>';

			echo '<td class="right time">';
			echo ($time[0]<60?round($time[0],1).'<span class="time_text">s</span> ':round($time[0]/60,1).'<span class="time_text">min</span> ').'/ <span class="gray">'.($time[1]<60?round($time[1],1).'<span class="time_text">s</span> ':round($time[1]/60,1).'<span class="time_text">min</span> ').'</span>';
			echo '</td>';

			echo '<td class="graph_cell">';
			echo '<div class="graph_cell">';

			echo '<div class="graph_full" style="width: '.($time[0]/$max*85).'%"></div>';
			if ((($time[1]-$time[0])/$max*85) > 0)
				echo '  <div class="graph_empty" style="width: '.(($time[1]-$time[0])/$max*85).'%"></div>';
			
			echo '</div>';
			echo '</td>';
			
			echo '</tr>';
		}
		echo '<tr class="semi-bold"><td>'.$lang['srv_casi_po_vprasanjih_skupaj'].'</td><td class="right time">'.$skupni_cas.' / <span class="gray">'.$skupni_cas_brez_pogojev.'</span></td><td></td></tr>';
		echo '</table>';
		
		echo '<div class="tabela_trajanje_legenda">';
		echo '<div class="graph_full legend"></div><span class="legend"> '.$lang['srv_neto_t_cas'].'</span>';
		echo '<div class="graph_empty legend"></div><span class="legend"> '.$lang['srv_bruto_t_cas'].'</span>';
		echo '</div>';
		
		echo '</fieldset>';
				
		// izpis za vprasanja
		$max = 0;
		$bruto_total = 0;
		$neto_total = 0;
		foreach ($expected_vprasanja AS $vpr) {
			if ($vpr[0] > $max) $max = $vpr[0];
			$bruto_total += $vpr[0];
			$neto_total += $vpr[0] * $vpr[1];
		}
		
		$prevstran = false;
		
		echo '<fieldset><legend>'.$lang['srv_casi_po_vprasanjih_strani'].'</legend>';
		
		echo '<table>';
	
		echo '<tr>';
		echo '<th>'.$lang['srv_casi_po_straneh_stran'].'/'.$lang['srv_casi_po_vprasanjih_vprasanje'].'</th>';
		echo '<th>'.$lang['srv_bruto_v_cas'].'</th>';
		echo '<th>'.$lang['srv_verjetnost_pojavitve'].'</th>';
		echo '<th colspan="2">'.$lang['srv_neto_v_cas'].'/'.$lang['srv_bruto_v_cas_long'].'</th>';
		echo '</tr>';

		foreach ($expected_vprasanja AS $vprasanje) {
			
			if (!$prevstran || $prevstran != $vprasanje[3]) {
				echo '<tr><td class="semi-bold" colspan="5">'.$vprasanje[3].'</td></tr>';
				$prevstran = $vprasanje[3];
			}
			
			$bruto = $vprasanje[0];
			$verjetnost = $vprasanje[1];
			$neto = $bruto * $verjetnost;
			
			echo '<tr>';
			echo '<td><span title="'.$vprasanje[2].'">'.skrajsaj($vprasanje[2], 30).'</span></td>';
			echo '<td class="right">'.round($bruto, 1).'s</td>';
			echo '<td class="center">'.round($verjetnost*100, 2).'%</td>';

			echo '<td class="right time">';
			echo round($neto, 1).'s / <span class="gray">'.round($bruto, 1).'s</span>';
			echo '</td>';
			
			echo '<td class="graph_cell">';
			echo '<div class="graph_cell">';

			echo '<div class="graph_full" style="width: '.($neto/$max*85).'%"></div>';
			if (($bruto-$neto)/$max*85 > 0)
				echo '  <div class="graph_empty" style="width: '.(($bruto-$neto)/$max*85).'%"></div>';
			
			echo '</div>';
			echo '</td>';
			
			echo '</tr>';
			
		}
		echo '<tr class="semi-bold"><td>'.$lang['srv_casi_po_vprasanjih_skupaj'].'</td><td class="right">'.(bcdiv($bruto_total, 60, 0)>0?bcdiv($bruto_total, 60, 0).'<span class="time_text">min</span> ':'').''.round(bcmod($bruto_total, 60), 0).'<span class="time_text">s</span></td><td></td><td class="right time">'.(bcdiv($neto_total, 60, 0)>0?bcdiv($neto_total, 60, 0).'<span class="time_text">min</span> ':'').''.round(bcmod($neto_total, 60), 0).'<span class="time_text">s</span> / '.(bcdiv($bruto_total, 60, 0)>0?bcdiv($bruto_total, 60, 0).'<span class="time_text">min</span> ':'').''.round(bcmod($bruto_total, 60), 0).'<span class="time_text">s</span></td><td></td></tr>';
		echo '</table>';

		echo '<div class="tabela_trajanje_legenda">';
		echo '<div class="graph_full legend"></div><span class="legend"> '.$lang['srv_neto_v_cas'].'</span>';
		echo '<div class="graph_empty legend"></div><span class="legend"> '.$lang['srv_bruto_v_cas_long'].'</span>';
		echo '</div>';

		echo '</fieldset>';
                
		//CASI PO BLOKIH
		if($block_spr_data){
			// izpis za bloke
			$maxb = 0;
			$totalb = 0;
			foreach ($expected_time_block AS $key => $val) {
					if ($val[1] > $maxb) $maxb = $val[1];
					$totalb += $val[0];
			}
		
			echo '<fieldset><legend>'.$lang['srv_casi_po_blokih'].'</legend>';
			echo '<table>';

			echo '<tr>';
			echo '<th>'.$lang['srv_casi_po_blokih_blok'].'</th>';
			echo '<th colspan="2">'.$lang['srv_neto_t_cas'].' / '.$lang['srv_bruto_t_cas'].'</th>';
			echo '</tr>';

			foreach ($expected_time_block AS $vrstni_red => $time) {
					echo '<tr>';
					echo '<td class="nowrap">'.$block_labels_by_number[$vrstni_red+1].'</td>';

					echo '<td class="right time">';
					echo ($time[0]<60?round($time[0],1).'<span class="time_text">s</span> ':round($time[0]/60,1).'<span class="time_text">min</span> ').'<span class="gray">/ '.($time[1]<60?round($time[1],1).'<span class="time_text">s</span> ':round($time[1]/60,1).'<span class="time_text">min</span> ').'</span>';
					echo '</td>';

					echo '<td class="graph_cell">';
					echo '<div class="graph_cell">';
					echo '  <div class="graph_full" style="width: '.($time[0]/$maxb*85).'%"></div>';
					if ((($time[1]-$time[0])/$maxb*85) > 0)
							echo '  <div class="graph_empty" style="width: '.(($time[1]-$time[0])/$maxb*85).'%"></div>';
					echo '</div>';
					echo '</td>';

					echo '</tr>';
			}
			echo '<tr class="semi-bold"><td>'.$lang['srv_casi_po_vprasanjih_skupaj'].'</td><td class="right time">'.(bcdiv($totalb, 60, 0)>0?bcdiv($totalb, 60, 0).'<span class="time_text">min</span> ':'').''.round(bcmod($totalb, 60), 0).'<span class="time_text">s</span></td><td></td><td></td><td></td></tr>';

			echo '</table>';

			echo '<div class="tabela_trajanje_legenda">';
			echo '<div class="graph_full legend"></div><span class="legend"> '.$lang['srv_neto_t_cas'].'</span>';
			echo '<div class="graph_empty legend"></div><span class="legend"> '.$lang['srv_bruto_t_cas'].'</span>';
			echo '</div>';

			echo '</fieldset>';
		
		
			// izpis za vprasanja po blokih
			$max = 0;
			$bruto_total = 0;
			$neto_total = 0;
			foreach ($expected_vprasanja AS $vpr) {
				if($vpr[4]){
					if ($vpr[0] > $max) $max = $vpr[0];
					$bruto_total += $vpr[0];
					$neto_total += $vpr[0] * $vpr[1];
				}
			}

			$prevstran = false;

			echo '<fieldset><legend>'.$lang['srv_casi_po_vprasanjih_bloki'].'</legend>';

			echo '<table>';

			echo '<tr>';
			echo '<th>'.$lang['srv_casi_po_straneh_stran'].'/'.$lang['srv_casi_po_vprasanjih_vprasanje'].'</th>';
			echo '<th>'.$lang['srv_bruto_v_cas'].'</th>';
			echo '<th>'.$lang['srv_verjetnost_pojavitve'].'</th>';
			echo '<th colspan="2">'.$lang['srv_neto_v_cas'].'/'.$lang['srv_bruto_v_cas_long'].'</th>';
			echo '</tr>';

			foreach ($expected_vprasanja AS $vprasanje) {
				
				if($vprasanje[4]){
					if (!$prevstran || $prevstran != $vprasanje[4]) {
							echo '<tr><td class="semi-bold" colspan="5">'.$vprasanje[4].'</td></tr>';
							$prevstran = $vprasanje[4];
					}
				
					$bruto = $vprasanje[0];
					$verjetnost = $vprasanje[1];
					$neto = $bruto * $verjetnost;
					
					echo '<tr>';
					echo '<td><span title="'.$vprasanje[2].'">'.skrajsaj($vprasanje[2], 30).'</span></td>';
					echo '<td class="right">'.round($bruto, 1).'s</td>';
					echo '<td class="center">'.round($verjetnost*100, 2).'%</td>';

					echo '<td class="right time">';
					echo round($neto, 1).'s / <span class="gray">'.round($bruto, 1).'s</span>';
					echo '</td>';
					
					echo '<td class="graph_cell">';
					echo '<div class="graph_cell">';

					echo '<div class="graph_full" style="width: '.($neto/$max*85).'%"></div>';
					if (($bruto-$neto)/$max*85 > 0)
						echo '  <div class="graph_empty" style="width: '.(($bruto-$neto)/$max*85).'%"></div>';
					
					echo '</div>';
					echo '</td>';
					
					echo '</tr>';
				}
				
			}
			echo '<tr class="semi-bold"><td>'.$lang['srv_casi_po_vprasanjih_skupaj'].'</td><td class="right">'.(bcdiv($bruto_total, 60, 0)>0?bcdiv($bruto_total, 60, 0).'<span class="time_text">min</span> ':'').''.round(bcmod($bruto_total, 60), 0).'<span class="time_text">s</span></td><td></td><td class="right time">'.(bcdiv($neto_total, 60, 0)>0?bcdiv($neto_total, 60, 0).'<span class="time_text">min</span> ':'').''.round(bcmod($neto_total, 60), 0).'<span class="time_text">s</span> / '.(bcdiv($bruto_total, 60, 0)>0?bcdiv($bruto_total, 60, 0).'<span class="time_text">min</span> ':'').''.round(bcmod($bruto_total, 60), 0).'<span class="time_text">s</span></td><td></td></tr>';
			echo '</table>';

			echo '<div class="tabela_trajanje_legenda">';
			echo '<div class="graph_full legend"></div><span class="legend"> '.$lang['srv_neto_v_cas'].'</span>';
			echo '<div class="graph_empty legend"></div><span class="legend"> '.$lang['srv_bruto_v_cas_long'].'</span>';
			echo '</div>';

			echo '</fieldset>';
		}
	}
        
        /**
         * Dobi podatke o bloku za vsako spremenljivko, ali false, ce ni blokov
         */
        function get_block_data_by_spr_id(){
            global $lang;
            $data = array();
            $block_query = sisplet_query("SELECT * FROM srv_if as bl LEFT JOIN srv_branching as br ON br.parent = bl.id  WHERE bl.enabled='0' AND bl.tip='1' AND br.ank_id = '$this->anketa' ORDER BY br.vrstni_red ASC", 'array');
            if($block_query){
                foreach ($block_query as $row) {
                    $label = $row['label'] ? $row['label']: $lang['srv_blok'].' '.$row['number'];
                    $data[$row['element_spr']] = array('label' => $label);
                }
                return $data;
            }
            return false;
        }
	
	/**
	* oceni predvideni cas za vprasanje
	* 
	* @param mixed $spremenljivka
	*/
	function vprasanje_predvideni_cas ($spremenljivka) {
		
		$sql1 = sisplet_query("SELECT id, naslov, tip FROM srv_spremenljivka WHERE id = '$spremenljivka'");
		$row1 = mysqli_fetch_array($sql1);
		
		$expected_time = strlen(strip_tags($row1['naslov'])) * GlobalMisc::getMisc('timing_vprasanje_'.$row1['tip']) / 100;
		
		// vprasanja, ki imajo tudi kategorije/vrednosti
		if ($row1['tip'] <= 3 || $row1['tip'] == 6 || $row1['tip'] == 16 || $row1['tip'] == 17 || $row1['tip'] == 18 || $row1['tip'] == 19 || $row1['tip'] == 20) {

			$sql2 = sisplet_query("SELECT naslov FROM srv_vrednost WHERE spr_id='$row1[id]'");

			//for those types we have max time option
			if($row1['tip'] == 3){
				$expected_time_temp = 0;

				while ($row2 = mysqli_fetch_array($sql2)) {	
					$expected_time_temp += strlen(strip_tags($row2['naslov'])) * GlobalMisc::getMisc('timing_kategorija_'.$row1['tip']) / 100;
				}
				//if time is greater than max time, use max time
				$max_time = GlobalMisc::getMisc('timing_kategorija_max_'.$row1['tip']);
				$expected_time += (isset($expected_time_temp) && $max_time > $expected_time_temp) ? $expected_time_temp : $max_time;
			}
			//types that doesnt have max time option
			else{
				while ($row2 = mysqli_fetch_array($sql2)) {	
					$expected_time += strlen(strip_tags($row2['naslov'])) * GlobalMisc::getMisc('timing_kategorija_'.$row1['tip']) / 100;
				}
			}
		}
		
		return $expected_time;	
	}
	
	/**
	* oceni verjetnost prikaza vprasanja glede na pogoje, ki so mu nastavljeni
	* 
	* @param mixed $spremenljivka
	*/
	function vprasanje_verjetnost ($spremenljivka) {
		
		$sql = sisplet_query("SELECT parent FROM srv_branching WHERE element_spr = '$spremenljivka'");
		$row = mysqli_fetch_array($sql);
		
		if ($row['parent'] == 0) return 1;	// vprasanje se vedno prikaze
		
		//echo $this->if_verjetnost($row['parent']).'<hr>';
		return $this->if_verjetnost($row['parent']);	
	}
	
	/**
	* oceni verjetnost da bo pogoj (if) izpolnjen
	* 
	* @param mixed $if
	*/
	function if_verjetnost ($if) {
		
		$sql = sisplet_query("SELECT parent FROM srv_branching WHERE element_if = '$if'");
		if (mysqli_num_rows($sql) == 0) return 0;
		$row = mysqli_fetch_array($sql);
		
		// izracunamo se verjetnost parentov
		if ($row['parent'] > 0){
			
			// dodaten pogoj da nismo v deadlocku (zaradi bugov se znata v branchingu pojavit ifa, ki imata drug drugega za parenta)
			$sqlX = sisplet_query("SELECT parent, element_if FROM srv_branching WHERE parent='".$if."' AND element_if='".$row['parent']."'");
			if(mysqli_num_rows($sqlX) > 0){	
				return 0;
			}
			
			$parent = $this->if_verjetnost($row['parent']);
		}
		else
			$parent = 1;
		
		$sql = sisplet_query("SELECT tip FROM srv_if WHERE id = '$if'");
		$row = mysqli_fetch_array($sql);
		
		if ($row['tip'] == 1) return 1 * $parent;	// blok je vedno 'izpolnjen'
		
		$eval = ' $total = ';
		$i = 0;
		// racunanje verjetnosti za podani if
		$sql = sisplet_query("SELECT * FROM srv_condition WHERE if_id = '$if' ORDER BY vrstni_red ASC");
		while ($row = mysqli_fetch_array($sql)) {
			
			$value = '';
			if(($value = $this->condition_verjetnost($row['id'])) !== false){
				
				if ($i++ != 0){
	                if ($row['conjunction'] == 0)
	                    $eval .= ' * ';
	                else
	                    $eval .= ' + ';
				}
				
	            for ($i=1; $i<=$row['left_bracket']; $i++)
	                $eval .= ' ( ';

	            $eval .= $value;

	            for ($i=1; $i<=$row['right_bracket']; $i++)
	                $eval .= ' ) ';
			}
		}
		$eval .= ';';

		if($eval != ' $total = ;')
			@eval($eval); //echo '--'.$eval.'--';
		else
			$total = 1;
			
		if ($total > 1) return 1 * $parent; else return $total * $parent;
	}
	
	/**
	* vrne verjetnost, da je izpolnjen condition (ena vrstica v IFu)
	* 
	* @param mixed $condition
	*/
	function condition_verjetnost ($condition) {
		
		$sql = sisplet_query("SELECT * FROM srv_condition WHERE id = '$condition'");
		if (mysqli_num_rows($sql) == 0) return 0;
		$row = mysqli_fetch_array($sql);
		
		// obicne spremenljivke
		if ($row['spr_id'] > 0) {
            $row2 = Cache::srv_spremenljivka($row['spr_id']);
            
            // radio, checkbox, dropdown in multigrid
            if ($row2['tip'] <= 3 || $row2['tip'] == 6) {
                // obicne spremenljivke
                if ($row['vre_id'] == 0) {
                	
					$sql1 = sisplet_query("SELECT id FROM srv_vrednost WHERE spr_id='$row[spr_id]'");
					$vse_vrednosti = mysqli_num_rows($sql1);

					$sql1 = sisplet_query("SELECT * FROM srv_condition_vre WHERE cond_id = '$condition'");
					$izbrane_vrednosti = mysqli_num_rows($sql1);
					
   					if ($vse_vrednosti > 0)
                   		$p = $izbrane_vrednosti / $vse_vrednosti;
                   	else
                   		$p = 0;
                        
                    if ($row['operator'] == 0)
                    	return $p;
                    else
                    	return 1 - $p;
                    	
                // multigrid
                } elseif ($row['vre_id'] > 0) {
					
					$sql1 = sisplet_query("SELECT id FROM srv_grid WHERE spr_id='$row[spr_id]'");
					$vse_vrednosti = mysqli_num_rows($sql1);

					$sql1 = sisplet_query("SELECT * FROM srv_condition_grid WHERE cond_id = '$condition'");
					$izbrane_vrednosti = mysqli_num_rows($sql1);
					
   					if ($vse_vrednosti > 0)
                   		$p = $izbrane_vrednosti / $vse_vrednosti;
                   	else
                   		$p = 0;
					
					if ($row['operator'] == 0)
                    	return $p;
                    else
                    	return 1 - $p;
                }

            // number in text
            } else {
				return 0.5;
            }
		
		// recnum
        } elseif ($row['spr_id'] == -1) {

            return 1 / $row['modul'];
            
        // calculations
        } elseif ($row['spr_id'] == -2) {

            return 0.5;
			
        // quotas
        } elseif ($row['spr_id'] == -3) {

            return 1;
        }
		
		return false;
	}
		
	/**
	* prikazuje povprecne case po straneh ipd....
	* 
	*/
	function testiranje_cas($samo_izracunaj_skupini_cas=0) {
		global $lang;
		global $global_user_id;
		global $admin_type;
		
		SurveyUserSetting :: getInstance()->Init($this->anketa, $global_user_id);

		// nastavitve iz popupa
		$rezanje = SurveyUserSetting::getInstance()->getSettings('rezanje');	if ($rezanje == '') $rezanje = 0;
		$rezanje_meja_sp = SurveyUserSetting::getInstance()->getSettings('rezanje_meja_sp');	if ($rezanje_meja_sp == '') $rezanje_meja_sp = 5;
		$rezanje_meja_zg = SurveyUserSetting::getInstance()->getSettings('rezanje_meja_zg');	if ($rezanje_meja_zg == '') $rezanje_meja_zg = 5;
		$rezanje_predvidena_sp = SurveyUserSetting::getInstance()->getSettings('rezanje_predvidena_sp');	if ($rezanje_predvidena_sp == '') $rezanje_predvidena_sp = 10;
		$rezanje_predvidena_zg = SurveyUserSetting::getInstance()->getSettings('rezanje_predvidena_zg');	if ($rezanje_predvidena_zg == '') $rezanje_predvidena_zg = 200;
		$rezanje_preskocene = SurveyUserSetting::getInstance()->getSettings('rezanje_preskocene');	if ($rezanje_preskocene == '') $rezanje_preskocene = 1;
		
		$get_predvideni = isset($_GET['predvideni']) ? $_GET['predvideni'] : '';
		$get_prikazi01 = isset($_GET['prikazi01']) ? $_GET['prikazi01'] : '';
		$get_pages = isset($_GET['pages']) ? $_GET['pages'] : '';


		/* ++ Predvideni casi    */
		if ($get_predvideni == 1 || $rezanje == 1) {
			$expected_time = array();
			$expected_vprasanja = array();
			$verjetnost = array();
			
			$sql = sisplet_query("SELECT introduction FROM srv_anketa WHERE id = '$this->anketa'");
			$row = mysqli_fetch_array($sql);
		
			// nagovor racunamo kot da gre za labelo
			$expected_vprasanja[0][0] = strlen(strip_tags($row['introduction'])) * GlobalMisc::getMisc('timing_vprasanje_5') / 100;
			$expected_vprasanja[0][1] = 1;
			$expected_vprasanja[0][2] = $lang['srv_vprasanje_tip_5'];
			$expected_vprasanja[0][3] = $lang['srv_intro_label'];
			
			$expected_time[0][0] = $expected_vprasanja[0][0] + GlobalMisc::getMisc('timing_stran');
			$expected_time[0][1] = $expected_time[0][0];
			
			
			$sql = sisplet_query("SELECT id, naslov, vrstni_red FROM srv_grupa g WHERE g.ank_id='$this->anketa' ORDER BY g.vrstni_red ASC");
			while ($row = mysqli_fetch_array($sql)) {
				
				$expected_time[$row['vrstni_red']][0] = 0;
				$expected_time[$row['vrstni_red']][1] = 0;
				
				$sql1 = sisplet_query("SELECT id, naslov FROM srv_spremenljivka WHERE gru_id='$row[id]' AND visible='1'");
				while ($row1 = mysqli_fetch_array($sql1)) {
					
					$expected_vprasanja[$row1['id']][0] = $this->vprasanje_predvideni_cas($row1['id']);
					$expected_vprasanja[$row1['id']][1] = $this->vprasanje_verjetnost($row1['id']);
					$expected_vprasanja[$row1['id']][2] = strip_tags($row1['naslov']);
					$expected_vprasanja[$row1['id']][3] = strip_tags($row['naslov']);
					
					$expected_time[$row['vrstni_red']][0] += $expected_vprasanja[$row1['id']][0] * $expected_vprasanja[$row1['id']][1];	// dejanski
					$expected_time[$row['vrstni_red']][1] += $expected_vprasanja[$row1['id']][0];		// bruto - z vsemi vprasanji
					
					if (!isset($verjetnost[$row['vrstni_red']]) || $expected_vprasanja[$row1['id']][1] > $verjetnost[$row['vrstni_red']])
						$verjetnost[$row['vrstni_red']] = $expected_vprasanja[$row1['id']][1];
				}
				
				$expected_time[$row['vrstni_red']][0] += GlobalMisc::getMisc('timing_stran') * $verjetnost[$row['vrstni_red']];		// pri dejanskem trajanju strani upostevamo verjetnost najverjetnejsega vprasanja na strani (stran se pojavi z najvisjo verjetnostjo vseh vprasanj na strani)
				$expected_time[$row['vrstni_red']][1] += GlobalMisc::getMisc('timing_stran');
				
			}
		}
		/* -- Predvideni casi    */
		
		// statusi		
		SurveyStatusCasi :: Init($this->anketa);
        $izbranStatusCasi = SurveyStatusCasi :: getCurentProfileId();
		$statusArray = SurveyStatusCasi::getStatusArray($izbranStatusCasi);
		
		$status = '';
		foreach ($statusArray AS $key => $val) {
			if ($key == 'statusnull' && $val == 1) $status .= ($status!=''?',':'') . '-1';
			if ($key == 'status0'    && $val == 1) $status .= ($status!=''?',':'') . '0';
			if ($key == 'status1'    && $val == 1) $status .= ($status!=''?',':'') . '1';
			if ($key == 'status2'    && $val == 1) $status .= ($status!=''?',':'') . '2';
			if ($key == 'status3'    && $val == 1) $status .= ($status!=''?',':'') . '3';
			if ($key == 'status4'    && $val == 1) $status .= ($status!=''?',':'') . '4';
			if ($key == 'status5'    && $val == 1) $status .= ($status!=''?',':'') . '5';
			if ($key == 'status6'    && $val == 1) $status .= ($status!=''?',':'') . '6';
			if ($key == 'statuslurker' && $val == 1) $lurker = ""; else $lurker = " AND lurker='0' ";
		}
		
		
        
		// preberemo vse timestampe za strani v anketi
		$sql = sisplet_query("SELECT ug.usr_id, UNIX_TIMESTAMP(ug.time_edit) AS time_edit_u, g.vrstni_red FROM srv_user_grupa".$this->db_table." ug, srv_grupa g, srv_user u WHERE ug.usr_id=u.id AND u.last_status IN ($status) $lurker AND ug.gru_id=g.id AND g.ank_id='$this->anketa' ORDER BY usr_id, gru_id");
		if (!$sql) echo mysqli_error($GLOBALS['connect_db']);
		if (mysqli_num_rows($sql) == 0) {

			if ($samo_izracunaj_skupini_cas == 1) {
				return null;
			} 
			elseif ($samo_izracunaj_skupini_cas == 2) {
				return array(null, null);
			} 
			else {
				echo '<div style="padding: 5px;" class="clr">'.$lang['srv_analiza_no_entry'].'</div>';
			}
		}
		$user_grupa = array();
		while ($row = mysqli_fetch_array($sql)) {
			
			$user_grupa[$row['usr_id']][$row['vrstni_red']] = $row['time_edit_u'];
			
		}
		
		if (mysqli_num_rows($sql) == 0) {
			die();
		}
		
		
		// cache mysql-a
		$usrs_id = array();
		foreach ($user_grupa AS $usr_id => $val) {
			$usrs_id[] = $usr_id;
		}
		
		$cache_row = array();
		$sql_c = sisplet_query("SELECT id, recnum, time_insert, UNIX_TIMESTAMP(time_insert) AS time_insert_u FROM srv_user WHERE id IN (".implode(',', $usrs_id).")");
		if (!$sql_c) echo mysqli_error($GLOBALS['connect_db']);
		while ($row_c = mysqli_fetch_array($sql_c)) {
			$cache_row[ $row_c['id'] ] = $row_c;
		}
		
		$cache_row1 = array();
		$sql1_c = sisplet_query("SELECT usr_id, time_edit, UNIX_TIMESTAMP(time_edit) AS time_edit_u FROM srv_user_grupa".$this->db_table." WHERE usr_id IN (".implode(',', $usrs_id).") AND gru_id = '0'");
		if (!$sql1_c) echo mysqli_error($GLOBALS['connect_db']);
		while ($row1_c = mysqli_fetch_array($sql1_c)) {
			$cache_row1[ $row1_c['usr_id'] ] = $row1_c;
		}
		
		// izracunamo razlike v casih, da dobimo za vsakega userja koliko casa je bil na posamezni strani
		$casi = array();
		foreach ($user_grupa AS $usr_id => $val) {
			
            $row = isset($cache_row[$usr_id]) ? $cache_row[$usr_id] : array();
			$row1 = isset($cache_row1[$usr_id]) ? $cache_row1[$usr_id] : array();
			
			$prev = (isset($row1['time_edit']) && $row1['time_edit'] != '' ? $row1['time_edit_u'] : $row['time_insert_u']);
			
			// nagovor
			if (isset($row1['time_edit']) && $row1['time_edit'] != '') $casi[0][$usr_id] = abs($row1['time_edit_u'] - $row['time_insert_u']);
			
			if (isset($row['recnum']) && $row['recnum'] > 0) {	// zapisi brez recnuma ne pridejo v poštev, ker nimajo pravih časov
				foreach ($val AS $vrstni_red => $time_edit) {

					$casi[$vrstni_red][$usr_id] = abs($time_edit - $prev);	

					$prev = $time_edit;	
				}
			}
		}
		
		// porezemo zgornjih in spodnjih 5% casov vsake strani
		$spodnja = $rezanje_meja_sp / 100;
		$zgornja = $rezanje_meja_zg / 100;
		
		// REZANJE
		foreach ($casi AS $vrstni_red => $val1) {
			
			asort($casi[$vrstni_red]);
			
			$len = count($casi[$vrstni_red]);
			$odrezi_sp = (int) round ( $len * $spodnja , 0);
			$odrezi_zg = (int) round ( $len * $zgornja , 0);
			
			$i = 1;
			foreach ($casi[$vrstni_red] AS $key => $val2) {
				
				if ($rezanje == 0) {	// rezanje po zgornji in spodnji meji
					if ($i <= $odrezi_sp || $i > $len-$odrezi_zg) {
						unset($casi[$vrstni_red][$key]);
					}
				
				} else {				// rezanje glede na 10% in 200% predvidenih vrednosti
					if ($val2 < $expected_time[$vrstni_red][0]*$rezanje_predvidena_sp/100 || $val2 > $expected_time[$vrstni_red][0]*$rezanje_predvidena_zg/100) {
						unset($casi[$vrstni_red][$key]);
					}
				}
				
				$i++;
			}

		}
				
		
		// izracunamo povprecne case
		$sql = sisplet_query("SELECT MAX(vrstni_red) AS max FROM srv_grupa WHERE ank_id = '$this->anketa'");
		$row = mysqli_fetch_array($sql);
		
		$count = array();
		$povprecni_casi = array();
		$user_casi = array();

		$max_time_on_page = 0;

		// Na zacetku nastavimo vse povprecne case po straneh na 0
		for ($i=0; $i<=$row['max']; $i++){
			$povprecni_casi[$i] = 0;
		}

		// Loop po vseh straneh
		foreach ($casi AS $vrstni_red => $uporabniki_na_strani) {
			
			// pogledamo za preskocene strani - samo ce nimamo nastavljeno, da se uposteva preskocene kot 0s
			$preskocene = array();
			if ($rezanje_preskocene == 0) {
				$sqlp = sisplet_query("SELECT ug.usr_id FROM srv_user_grupa".$this->db_table." ug, srv_grupa g WHERE g.id=ug.gru_id AND g.vrstni_red='$vrstni_red' AND ug.preskocena='1'");
				while ($rowp = mysqli_fetch_array($sqlp)) {
					array_push($preskocene, $rowp['usr_id']);
				}
			}
			
			// Loop po vseh uporabnikih na strani (cas, ki ga je prezivel na trenutni strani)
			foreach ($uporabniki_na_strani AS $usr_id => $time) {

				// Racunanje casov - eliminiramo preskocene samo ce imamo to nastavitev vklopljeno
				//if (!in_array($usr_id, $preskocene) || $rezanje_preskocene==1) {
					
					// neto je kao povprecno trajanje strani in uposteva tudi 0s, 1s ce se je preskocilo
					if(isset($povprecni_casi[$vrstni_red]))
						$povprecni_casi[$vrstni_red] += $time;						
					else
						$povprecni_casi[$vrstni_red] = $time;						
					
					// Pristejemo cas userja
					if(isset($user_casi[$usr_id]))
						$user_casi[$usr_id] += $time;
					else
						$user_casi[$usr_id] = $time;
				//}

				// Zabelezimo najdaljsi obisk strani
				if ($time > $max_time_on_page) 
					$max_time_on_page = $time;
			}

			// Izracunamo povprecje na strani
			if (count($uporabniki_na_strani) > 0){
				$povprecni_casi[$vrstni_red] = $povprecni_casi[$vrstni_red] / count($uporabniki_na_strani);
			}

			$count[$vrstni_red] = count($casi[$vrstni_red]);
		}
		
		// Loop po povprecnih casih in dobimo najvecji povprecni cas in celoten cas
		$max = 0;
		$total_povprecni_cas = 0;
		$total_predvideni_cas = 0;
		foreach ($povprecni_casi AS $key => $val) {

			// Shranimo vecji povprecni cas
			if ($val > $max) 
				$max = $val;

			// Pristejemo povprecni cas strani celotnemu povprecnemu casu
			$total_povprecni_cas += $val;
		}

		if ($get_predvideni == 1) {
			if ($rezanje_preskocene == 1) {
				foreach ($expected_time AS $key => $val) {
					if ($val[0] > $max) $max = $val[0];
					$total_predvideni_cas += $val[0];
				}
			} else {
				foreach ($expected_time AS $key => $val) {
					if ($val[1] > $max) $max = $val[1];
					$total_predvideni_cas += $val[1];
				}
			}
		}

		
		// Izracunamo mediano, min in max cas na anketi
		$total_median_cas = 0;
		$total_min_cas = 0;
		$total_max_cas = 0;

		// Sortiramo case uporabnikov po velikosti
		sort($user_casi, SORT_NUMERIC);
		$user_count = count($user_casi);

		// Dobimo index srednjega elementa
		$user_median_index = floor($user_count / 2);

		// Liho stevilo userjev - dobimo mediano - vrednost srednjega
		if ($user_count % 2 === 1) {    
			$total_median_cas = $user_casi[$user_median_index];
		} 
		// Sodo stevilo - izracunamo mediano iz srednjih dveh
		else {    
			if(isset($user_casi[$user_median_index-1]))               
				$total_median_cas = ($user_casi[$user_median_index-1] + $user_casi[$user_median_index]) / 2;
			else
				$total_median_cas = $user_casi[$user_median_index];
		}

		// Zabelezimo min in max cas uporabnika na anketi
		$total_min_cas = $user_casi[0];
		$total_max_cas = $user_casi[$user_count-1];


		if ($max == 0) 
			return;
		
			
		// izpis
		$skupni_cas = (bcdiv($total_povprecni_cas, 60, 0) > 0 ? bcdiv($total_povprecni_cas, 60, 0).'<span class="time_text">min</span> ':'').''.round(bcmod($total_povprecni_cas, 60), 0).'<span class="time_text">s</span>';
		$predviden_cas = (bcdiv($total_predvideni_cas, 60, 0)>0?bcdiv($total_predvideni_cas, 60, 0).'<span class="time_text">min</span> ':'').''.round(bcmod($total_predvideni_cas, 60), 0);
		
		$mediana = (bcdiv($total_median_cas, 60, 0) > 0 ? bcdiv($total_median_cas, 60, 0).'<span class="time_text">min</span> ':'').''.round(bcmod($total_median_cas, 60), 0).'<span class="time_text">s</span>';
		$minimum = (bcdiv($total_min_cas, 60, 0) > 0 ? bcdiv($total_min_cas, 60, 0).'<span class="time_text">min</span> ':'').''.round(bcmod($total_min_cas, 60), 0).'<span class="time_text">s</span>';
		$maximum = (bcdiv($total_max_cas, 60, 0) > 0 ? bcdiv($total_max_cas, 60, 0).'<span class="time_text">min</span> ':'').''.round(bcmod($total_max_cas, 60), 0).'<span class="time_text">s</span>';
		$numerus = $user_count;


		// Ce izipsujemo samo dejanski cas na nekem drugem mestu
		if ($samo_izracunaj_skupini_cas > 0){

			// Vracamo array tudi z mediano
			if($samo_izracunaj_skupini_cas == 2)
				return array($skupni_cas, $mediana);
			else
				return $skupni_cas;
		}


		// Prvi segment - dejansko trajanje, mediana...
		echo '<fieldset><legend>'.$lang['srv_total_trajanje'].'</legend>';
		echo '	<p class="semi-bold">'.$lang['srv_dejansko_trajanje_real'].':</p>';
		echo '	<p>'.$skupni_cas.'</p>';

		echo '	<p class="semi-bold top16">'.$lang['median'].':</p>';
		echo '	<p>'.$mediana.'</p>';

		echo '	<p class="semi-bold top8">Min:</p>';
		echo '	<p>'.$minimum.'</p>';

		echo '	<p class="semi-bold top8">Max:</p>';
		echo '	<p>'.$maximum.'</p>';

		echo '	<p class="semi-bold top8">'.$lang['numerus'].':</p>';
		echo '	<p>'.$numerus.'</p>';

		echo '</fieldset>';


		echo '<fieldset><legend>'.$lang['srv_dejanski_casi'].'</legend>';

		echo '<p class="bottom16"><input type="checkbox" name="predvideni" id="predvideni" value="1" onclick="vnos_redirect(\'index.php?anketa='.$this->anketa.'&a=testiranje&m=cas&predvideni='.($get_predvideni == 1 ? '0' : '1').'&pages='.$get_pages.'&prikazi01='.$get_prikazi01.'\');" '.($get_predvideni == 1 ? 'checked' : '').' /><label for="predvideni">'.$lang['srv_vkljuci_predvidene'].'</label></p>';

		echo '<table>';
		echo '<tr>';
		echo '<th>'.$lang['srv_casi_po_straneh_stran'].'</th>';
		echo '<th colspan="2">'.$lang['srv_neto_v_cas'];
		
		if	($get_predvideni == 1) {
			echo ' / '.$lang['srv_testiranje_predvidenicas_1'];
		}
		echo '</th>';
		echo '<th>'.$lang['srv_stevilo_enot'].'</th>';
		echo '</tr>';
		
		foreach ($povprecni_casi AS $vrstni_red => $time) {
			
			$naslov = $lang['srv_intro_label'];

			$sql = sisplet_query("SELECT naslov FROM srv_grupa WHERE vrstni_red='$vrstni_red' AND ank_id = '$this->anketa'");
			if(mysqli_num_rows($sql) > 0){
				$row = mysqli_fetch_array($sql);

				if($row['naslov'] != '')
					$naslov = $row['naslov'];
			}
						
			echo '<tr>';
			echo '<td>'.$naslov.'</td>';
			echo '<td class="right time">'.($time<60 ? round($time,1).'<span class="time_text">s</span> ' : round($time/60,1).'<span class="time_text">min</span> ');
			if	($get_predvideni == 1) {
				if ($rezanje_preskocene == 1)
					$time = $expected_time[$vrstni_red][0];
				else
					$time = $expected_time[$vrstni_red][1];

				echo '<span class="gray"> / '.($time<60 ? round($time,1).'<span class="time_text">s</span> ' : round($time/60,1).'<span class="time_text">min</span> ').'</span>';
			}
			echo '</td>';
			
			echo '<td class="graph_cell">';
			echo '<div class="graph_cell">';
			echo '<div class="graph_full" style="width: '.($time/$max*85).'%"></div>';

			if ($get_predvideni == 1) {
				if ($rezanje_preskocene == 1)
					$time = $expected_time[$vrstni_red][0];
				else
					$time = $expected_time[$vrstni_red][1];

				echo '<div class="graph_'.($rezanje_preskocene==1?'empty':'red').'" style="width: '.($time/$max*85).'%"></div>';	
			}
				
			echo '</div>';
			echo '</td>';
			
			echo '<td class="right">'.(isset($count[$vrstni_red]) ? $count[$vrstni_red] : 0).'</td>';
			
			echo '</tr>';
		}
		
		echo '<tr class="semi-bold"><td>'.$lang['srv_anl_suma1'].'<td class="right time">'.$skupni_cas;
		if	($get_predvideni == 1) {
			if ($rezanje_preskocene == 1)
				$time = $expected_time[$vrstni_red][0];
			else
				$time = $expected_time[$vrstni_red][1];

			echo '<span class="gray"> / '.$predviden_cas.'s</span>';
		}
		echo '</td><td></td><td></td>';
		
		echo '</td></tr>';
		
		echo '</table>';

		echo '<div class="tabela_trajanje_legenda">';
		echo '<div class="graph_full legend"></div><span class="legend"> '.$lang['srv_neto_t_cas'].'</span>';
		if ($get_predvideni == 1)
			echo '<div class="graph_empty legend"></div><span class="legend"> '.$lang['srv_testiranje_predvidenicas_1'].'</span>';
		echo '</div>';
		
		echo '</fieldset>';
	
		
		// pridejo prevelike tabele
		if ($max_time_on_page > 1000 && $admin_type > 0) 
			return; 
		
	
		// izpis histograma casov za vsako stran
		
		echo '<fieldset style="position:relative;"><legend>'.$lang['srv_frekvencna_porazdelitev'].'</legend>';

		echo '<div class="setting_holder bottom16">';

		echo '<div class="setting_item">';
		echo '<input type="checkbox" name="pages" id="pages" value="1" onclick="vnos_redirect(\'index.php?anketa='.$this->anketa.'&a=testiranje&m=cas&predvideni='.$get_predvideni.'&prikazi01='.$get_prikazi01.'&pages='.($get_pages == 1 ? '0' : '1').'\');" '.($get_pages == 1 ? 'checked' : '').' /><label for="pages">'.$lang['srv_show_pages'].'</label>';
		echo '</div>';
		if ($rezanje_preskocene == 1) {
			echo '<div class="setting_item">';
			echo ' <input type="checkbox" name="prikazi01" id="prikazi01" value="1" onclick="vnos_redirect(\'index.php?anketa='.$this->anketa.'&a=testiranje&m=cas&predvideni='.$get_predvideni.'&pages='.$get_pages.'&prikazi01='.($get_prikazi01 == 1 ? '0' : '1').'\');" '.($get_prikazi01 == 1 ? 'checked' : '').' /><label for="prikazi01">'.$lang['srv_prikazi01'].'</label>';
			echo '</div>';
		}
					
		echo '</div>';
		

		// Zdruzimo vse case po straneh na en histogram
		if ($get_pages != '1') {

			// Dobimo case resevanja celotne ankete - po respondentih
			$skupni_casi = array();

			// Loop po posameznih straneh - sestejemo case po straneh na skupni cas na anketi
			foreach ($casi AS $stran => $casi_na_strani) {

				// Loop po vseh casih na strani
				foreach ($casi_na_strani AS $usr_id => $cas_na_strani) {

					if (isset($skupni_casi[$usr_id]))
						$skupni_casi[$usr_id] += $cas_na_strani;
					else
						$skupni_casi[$usr_id] = $cas_na_strani;
				}
			}

			// Nastavimo se min cas in max cas na anketi
			$max_cas_na_anketi = 0;
			$min_cas_na_anketi = 0;
			foreach ($skupni_casi AS $usr_id => $cas_na_anketi){

				if ($cas_na_anketi > $max_cas_na_anketi) 
					$max_cas_na_anketi = $cas_na_anketi;

				if ($cas_na_anketi < $min_cas_na_anketi || $min_cas_na_anketi == 0) 
					$min_cas_na_anketi = $cas_na_anketi;
			}

			// Pretvorimo v minute
			foreach ($skupni_casi AS $usr_id => $cas_na_anketi) {
				$skupni_casi[$usr_id] = (int) round($cas_na_anketi / 60, 0);
			}
			$max_cas_na_anketi = (int) round($max_cas_na_anketi / 60, 0);
			$min_cas_na_anketi = (int) round($min_cas_na_anketi / 60, 0);

			if($get_prikazi01 == 1)
				$min_cas_na_anketi = 0;
			else
				$min_cas_na_anketi = 2;

			// Pripravimo podatke za histogram
			$histogram_data = array();
			
			// Napolnimo vrednosti 0 na vsakem koraku (minuti)
			for ($t=0; $t<=$max_cas_na_anketi; $t++) 
				$histogram_data[$t] = 0;

			// Napolnimo vrednosti v histogram in dobimo najvecjo vrednost
			$histogram_max_data = 0;
			foreach ($skupni_casi AS $usr_id => $cas_na_anketi) {
				$histogram_data[$cas_na_anketi]++;

				if($histogram_data[$cas_na_anketi] > $histogram_max_data)
					$histogram_max_data = $histogram_data[$cas_na_anketi];
			}


			// Izrisemo histogram - ce imamo kaksno vrednost nad 0
			if ($histogram_max_data != 0) {

				echo '<table class="casi_histogram">';
				
				// Sivi stolpci
				echo '<tr>';				
				
				for ($t=$min_cas_na_anketi; $t<=$max_cas_na_anketi; $t++) {

					if(isset($histogram_data[$t])){
						echo '<td class="histogram">';
						echo '	<div class="histogram" style="height: '.($histogram_data[$t]/$histogram_max_data*150).'px;" title="'.$histogram_data[$t].' ('.$t.'min)"></div>';
						echo '</td>';				
					}
				}		
						
				echo '</tr>';
				

				// Spodnja os
				echo '<tr>';
				
				// Korak 1 min
				if ($max_cas_na_anketi <= 20) {
					for ($t=$min_cas_na_anketi; $t<=$max_cas_na_anketi; $t++) {
						echo '<td class="histogram_time">'.$t.'min'.'</td>';
					}
				} 
				// Korak 10 min
				else {
					$colspan = 10 - $min_cas_na_anketi;
					echo '<td class="histogram_time" colspan="'.$colspan.'">'.$min_cas_na_anketi.'min</td>';
					
					for ($t=10; $t<=$max_cas_na_anketi; $t+=10) {

						// Pri zadnji labeli odstejemo span
						if(($t + 10) > count($histogram_data))
							$colspan = count($histogram_data) - $t;
						else
							$colspan = 10;

						echo '<td class="histogram_time" colspan="'.$colspan.'">'.$t.'</td>';
					}
				}
				
				echo '</tr>';
				
				echo '</table>';
			}
		}
		// Casi po straneh - 1 histogram na stran
		else{

			foreach ($casi AS $stran => $casi_na_strani) {

				// Izpisemo naslov posamezne strani
				$naslov = $lang['srv_intro_label'];

				$sql = sisplet_query("SELECT naslov FROM srv_grupa WHERE vrstni_red='$stran' AND ank_id='$this->anketa'");
				if(mysqli_num_rows($sql) > 0){
					$row = mysqli_fetch_array($sql);

					if($row['naslov'] != '')
						$naslov = $row['naslov'];
				}

				echo '<p class="bold blue bottom16">'.$naslov.'</p>';
	
	
				// Dobimo podatke za histogram
				// Nastavimo min cas in max cas na anketi
				$max_cas_na_strani = 0;
				$min_cas_na_strani = 0;
				foreach ($casi_na_strani AS $usr_id => $cas_na_strani) {
					
					if ($cas_na_strani > $max_cas_na_strani) 
						$max_cas_na_strani = $cas_na_strani;

					if ($cas_na_strani < $min_cas_na_strani || $min_cas_na_strani == 0) 
						$min_cas_na_strani = $cas_na_strani;
				}

				// Pretvorimo v minute
				$max_cas_na_strani = (int) round($max_cas_na_strani / 60, 0);
				$min_cas_na_strani = (int) round($min_cas_na_strani / 60, 0);

				if($get_prikazi01 == 1)
					$min_cas_na_strani = 0;

				// Pripravimo podatke za histogram
				$histogram_data = array();
				
				// Napolnimo vrednosti 0 na vsakem koraku (minuti)
				for ($t=0; $t<=$max_cas_na_strani; $t++) 
					$histogram_data[$t] = 0;

				// Napolnimo vrednosti v histogram in dobimo najvecjo vrednost
				$histogram_max_data = 0;
				foreach ($casi_na_strani AS $usr_id => $cas_na_strani) {

					// Pretvorimo v minute
					$cas_na_strani = (int) round($cas_na_strani / 60, 0);

					$histogram_data[$cas_na_strani]++;

					if($histogram_data[$cas_na_strani] > $histogram_max_data)
						$histogram_max_data = $histogram_data[$cas_na_strani];
				}


				// Izrisemo histogram za stran - ce imamo kaksno vrednost nad 0
				if ($histogram_max_data != 0) {

					echo '<table class="casi_histogram">';
					
					// Sivi stolpci
					echo '<tr>';				
					
					for ($t=$min_cas_na_strani; $t<=$max_cas_na_strani; $t++) {

						if(isset($histogram_data[$t])){
							echo '<td class="histogram">';
							echo '	<div class="histogram" style="height: '.($histogram_data[$t]/$histogram_max_data*150).'px;" title="'.$histogram_data[$t].' ('.$t.'min)"></div>';
							echo '</td>';				
						}
					}		
							
					echo '</tr>';
					

					// Spodnja os
					echo '<tr>';
					
					// Korak 1 min
					if ($max_cas_na_strani <= 20) {
						for ($t=$min_cas_na_strani; $t<=$max_cas_na_strani; $t++) {
							echo '<td class="histogram_time">'.$t.'min'.'</td>';
						}
					} 
					// Korak 10 min
					else {
						if ($min_cas_na_strani == 0)
							echo '<td class="histogram_time" colspan="10">0'.'min'.'</td>';
						else
							echo '<td class="histogram_time" colspan="8">2'.'min'.'</td>';
						
						for ($t=10; $t<=$max_cas_na_strani; $t+=10) {

							$colspan = 10;
							if(($t + 10) > count($histogram_data))
								$colspan = $t + 10 - count($histogram_data);
							if($min_cas_na_strani != 0)
								$colspan -= 2;

							echo '<td class="histogram_time" colspan="'.$colspan.'">'.$t.'</td>';
						}
					}
					
					echo '</tr>';
					
					echo '</table>';
				}
			}

		}
		
		echo '</fieldset>';	
	}
	
	/**
	* opcije za rezanje
	*/
	function show_rezanje_casi () {
		global $lang;
		global $global_user_id;
		
		SurveyUserSetting :: getInstance()->Init($this->anketa, $global_user_id);

		$rezanje = SurveyUserSetting::getInstance()->getSettings('rezanje');	if ($rezanje == '') $rezanje = 0;
		$rezanje_meja_sp = SurveyUserSetting::getInstance()->getSettings('rezanje_meja_sp');	if ($rezanje_meja_sp == '') $rezanje_meja_sp = 5;
		$rezanje_meja_zg = SurveyUserSetting::getInstance()->getSettings('rezanje_meja_zg');	if ($rezanje_meja_zg == '') $rezanje_meja_zg = 5;
		$rezanje_predvidena_sp = SurveyUserSetting::getInstance()->getSettings('rezanje_predvidena_sp');	if ($rezanje_predvidena_sp == '') $rezanje_predvidena_sp = 10;
		$rezanje_predvidena_zg = SurveyUserSetting::getInstance()->getSettings('rezanje_predvidena_zg');	if ($rezanje_predvidena_zg == '') $rezanje_predvidena_zg = 200;
		$rezanje_preskocene = SurveyUserSetting::getInstance()->getSettings('rezanje_preskocene');	if ($rezanje_preskocene == '') $rezanje_preskocene = 1;
		
		
        echo '<h2>'.$lang['srv_rezanje'].'</h2>';
		echo '<div class="popup_close"><a href="#" onClick="popupClose(); return false;">✕</a></div>';


        echo '<div class="popup_main">';
        
        echo '  <div class="setting_line">';
		echo '      <input type="radio" name="rezanje" id="rezanje_0" value="0"'.($rezanje==0?' checked':'').'>';
        echo '      <label for="rezanje_0">'.$lang['srv_rezanje_0'].':</label>';
        echo '  </div>';

        echo '  <div class="setting_line">';
        echo '      <label for="rezanje_meja_sp">'.$lang['srv_rezanje_meja_sp'].':</label>';
        echo '      <select name="rezanje_meja_sp" id="rezanje_meja_sp">';
		echo '          <option value="0"'.($rezanje_meja_sp==0?' selected':'').'>0%</option>';
		echo '          <option value="1"'.($rezanje_meja_sp==1?' selected':'').'>1%</option>';
		echo '          <option value="3"'.($rezanje_meja_sp==3?' selected':'').'>3%</option>';
		echo '          <option value="5"'.($rezanje_meja_sp==5?' selected':'').'>5%</option>';
		echo '          <option value="10"'.($rezanje_meja_sp==10?' selected':'').'>10%</option>';
		echo '          <option value="20"'.($rezanje_meja_sp==20?' selected':'').'>20%</option>';
		echo '      </select>';        
        echo '  </div>';

        echo '  <div class="setting_line">';
        echo '      <label for="rezanje_meja_sp">'.$lang['srv_rezanje_meja_zg'].':</label>';
        echo '      <select name="rezanje_meja_zg" id="rezanje_meja_zg">';
        echo '          <option value="0"'.($rezanje_meja_zg==0?' selected':'').'>0%</option>';
		echo '          <option value="1"'.($rezanje_meja_zg==1?' selected':'').'>1%</option>';
		echo '          <option value="3"'.($rezanje_meja_zg==3?' selected':'').'>3%</option>';
		echo '          <option value="5"'.($rezanje_meja_zg==5?' selected':'').'>5%</option>';
		echo '          <option value="10"'.($rezanje_meja_zg==10?' selected':'').'>10%</option>';
		echo '          <option value="20"'.($rezanje_meja_zg==20?' selected':'').'>20%</option>';
		echo '      </select>';        
        echo '  </div>';

        echo '  <div class="setting_line">';
		echo '      <input type="radio" name="rezanje" id="rezanje_1" value="1"'.($rezanje==1?' checked':'').'>';
        echo '      <label for="rezanje_1">'.$lang['srv_rezanje_1'].':</label>';
        echo '  </div>';

        echo '  <div class="setting_line">';
        echo '      <label for="rezanje_predvidena_sp">'.$lang['srv_rezanje_meja_sp'].':</label>';
        echo '      <select name="rezanje_predvidena_sp" id="rezanje_predvidena_sp">';
        echo '          <option value="0"'.($rezanje_predvidena_sp==0?' selected':'').'>0%</option>';
		echo '          <option value="1"'.($rezanje_predvidena_sp==1?' selected':'').'>1%</option>';
		echo '          <option value="3"'.($rezanje_predvidena_sp==3?' selected':'').'>3%</option>';
		echo '          <option value="5"'.($rezanje_predvidena_sp==5?' selected':'').'>5%</option>';
		echo '          <option value="10"'.($rezanje_predvidena_sp==10?' selected':'').'>10%</option>';
		echo '          <option value="20"'.($rezanje_predvidena_sp==20?' selected':'').'>20%</option>';
		echo '      </select> '.$lang['srv_rezanje_predvidenega'];
        echo '  </div>';

        echo '  <div class="setting_line">';
        echo '      <label for="rezanje_meja_sp">'.$lang['srv_rezanje_meja_zg'].':</label>';
        echo '      <select name="rezanje_predvidena_zg" id="rezanje_predvidena_zg">';
		echo '          <option value="100"'.($rezanje_predvidena_zg==100?' selected':'').'>100%</option>';
		echo '          <option value="150"'.($rezanje_predvidena_zg==150?' selected':'').'>150%</option>';
		echo '          <option value="200"'.($rezanje_predvidena_zg==200?' selected':'').'>200%</option>';
		echo '          <option value="300"'.($rezanje_predvidena_zg==300?' selected':'').'>300%</option>';
		echo '          <option value="500"'.($rezanje_predvidena_zg==500?' selected':'').'>500%</option>';
		echo '          <option value="1000"'.($rezanje_predvidena_zg==1000?' selected':'').'>1000%</option>';
		echo '      </select> '.$lang['srv_rezanje_predvidenega'];      
        echo '  </div>';

        echo '  <div class="setting_line top8">';
		echo '      <input type="checkbox" name="rezanje_preskocene" id="rezanje_preskocene" value="1"'.($rezanje_preskocene==1?' checked':'').' onchange="javascript: if (this.checked == 1) { $(\'#preskocene_txt\').hide(); } else { $(\'#preskocene_txt\').show(); }">';
        echo '      <label for="rezanje_preskocene">'.$lang['srv_rezanje_preskocene'].':</label>';
        echo '  </div>';
        
        echo '  <div class="bottom16">';
		echo '      <span id="preskocene_txt" '.($rezanje_preskocene==1? 'style="display:none"' : '').'>'.$lang['srv_rezanje_preskocene_txt'].'</span>';
        echo '  </div>';
        
        echo '</div>';


		echo '<div class="button_holder">';
		echo '  <button class="medium white-blue" onclick="statusCasiAction(\'cancle\'); return false;">'.$lang['srv_close_profile'].'</button>';
        echo '  <button class="medium blue" onclick="statusCasiAction(\'run_rezanje\'); return false;">'.$lang['srv_save_run_profile'].'</button>';
        echo '</div>';		
	}
	
	/**
	* shrani nastavitve
	* 
	*/
	function save_rezanje_casi () {
		global $global_user_id;
		
		SurveyUserSetting::getInstance()->Init($this->anketa, $global_user_id);

		SurveyUserSetting::getInstance()->saveSettings('rezanje', $_POST['rezanje']);
		SurveyUserSetting::getInstance()->saveSettings('rezanje_meja_sp', $_POST['rezanje_meja_sp']);
		SurveyUserSetting::getInstance()->saveSettings('rezanje_meja_zg', $_POST['rezanje_meja_zg']);
		SurveyUserSetting::getInstance()->saveSettings('rezanje_predvidena_sp', $_POST['rezanje_predvidena_sp']);
		SurveyUserSetting::getInstance()->saveSettings('rezanje_predvidena_zg', $_POST['rezanje_predvidena_zg']);
		SurveyUserSetting::getInstance()->saveSettings('rezanje_preskocene', $_POST['rezanje_preskocene']);
		
	}
	
	function string_format($cnt,$lang_root) {
		global $lang;
		
		$txt = '';
		//if ($cnt > 0) $txt .= '<span class="red">';
		
		if (isset($lang[$lang_root.'_'.$cnt])) {
			$txt .= $cnt.' '.$lang[$lang_root.'_'.$cnt];
		} else {
			$txt .= $cnt.' '.$lang[$lang_root.'_more'];
		}
		
		//if ($cnt > 0) $txt .= '</span>';
		
		return $txt;
	}
	
	/**
	* odsteje dva datuma, $d1 - D2
	* 
	*/
	function diff ($d1, $d2) {
		//echo $d1.' '.$d2;
		$d1 = (is_string($d1) ? strtotime($d1) : $d1);
        $d2 = (is_string($d2) ? strtotime($d2) : $d2);
		//echo ' ('.$d1.' '.$d2.')<br>';
		$diff_secs = abs($d1 - $d2);
        
        return $diff_secs;
	}
	
	private $usr_id;
	/**
	* vnese izbrano stevilo testnih podatkov
	* 
	*/
	function testiranje_testnipodatki () {
		global $lang;
		
		if (isset($_POST['stevilo_vnosov']) && $_POST['stevilo_vnosov'] > 0) {
			
			// Nastavitev da vstavljamo samo veljavne vnose
			$only_valid = isset($_POST['only_valid']) ? $_POST['only_valid'] : 0;
			
			if(session_id() == '') {session_start();}
			$_SESSION['progressBar'][$this->anketa]['status'] = 'ok';
			$_SESSION['progressBar'][$this->anketa]['total'] = (int)$_POST['stevilo_vnosov'];
			$_SESSION['progressBar'][$this->anketa]['current'] = 0;
			session_commit();
			
			SurveyInfo::getInstance()->SurveyInit($this->anketa);
			$rowa = SurveyInfo::getInstance()->getSurveyRow();
			if ($rowa['survey_type'] < 2) return;	// samo za anketo na več straneh in branching...
			
			$sql = sisplet_query("SELECT MAX(recnum) AS recnum FROM srv_user WHERE ank_id = '$this->anketa' AND preview='0'");
	        $row = mysqli_fetch_array($sql);
	        $recnum = $row['recnum'] + 1;
	        
			//$sql = sisplet_query("SELECT s.id, s.tip, s.size, s.ranking_k, s.design FROM srv_spremenljivka s, srv_grupa g WHERE s.gru_id=g.id AND g.ank_id='$this->anketa' AND s.visible='1' ORDER BY g.vrstni_red, s.vrstni_red");
			$sql = sisplet_query("SELECT s.id, s.tip, s.size, s.ranking_k, s.design, s.cela FROM srv_spremenljivka s, srv_grupa g WHERE s.gru_id=g.id AND g.ank_id='$this->anketa' AND s.visible='1' ORDER BY g.vrstni_red, s.vrstni_red");
	        
	        # zabeležimo id-je za arhiv testnih vnosov
	        
			$arrayTestni = array();
			for ($i=1; $i<=$_POST['stevilo_vnosov']; $i++) {
				
                session_start();
				$_SESSION['progressBar'][$this->anketa]['current'] = $i;
				session_commit();
				
				// izberemo random hash, ki se ni v bazi (to more bit, ker je index na fieldu cookie)
				do {
					$rand = md5(mt_rand(1, mt_getrandmax()).'@'.$_SERVER['REMOTE_ADDR']);
					$sql1 = sisplet_query("SELECT id FROM srv_user WHERE cookie = '$rand'");
				} while (mysqli_num_rows($sql1) > 0);
				
				$sql2 = sisplet_query("INSERT INTO srv_user (ank_id, preview, testdata, cookie, user_id, ip, time_insert, recnum, referer, last_status, lurker) VALUES ('$this->anketa', '0', '2', '$rand', '0', '$_SERVER[REMOTE_ADDR]', NOW(), '$recnum', '$_SERVER[HTTP_REFERER]', '6', '0')");
				if (!$sql2) echo mysqli_error($GLOBALS['connect_db']);
				$this->usr_id = mysqli_insert_id($GLOBALS['connect_db']);
				
				// Survey v zanki vsakič kreiramo znova zaradi IFov !!!
				include_once('../../main/survey/app/global_function.php');
				new \App\Controllers\SurveyController(true);
				save('usr_id', $this->usr_id);
				$s = \App\Controllers\CheckController::getInstance();

				$arrayTestni[] = $this->usr_id;
				mysqli_data_seek($sql, 0);
				while ($row = mysqli_fetch_array($sql)) {
					
					$srv_data_vrednost = "";
					$srv_data_grid = "";
					$srv_data_checkgrid = "";
					$srv_data_text = "";
					$srv_data_textgrid = "";
					$srv_data_rating = "";
                    $srv_data_map = "";
                    $srv_data_heatmap = "";
					
					if ($row['tip'] != 5) {
	
						// radio ali select
						if ( ($row['tip']==1 || $row['tip']==3) ) {
							
							$sql1 = sisplet_query("SELECT id, other FROM srv_vrednost WHERE spr_id='$row[id]'");
							
							// Ce imamo samo veljavne vedno oznacimo enega
							if($only_valid == 1)
								$rand = rand(1, mysqli_num_rows($sql1));
							else
								$rand = rand(0, mysqli_num_rows($sql1));
							
							if ($rand > 0) {
								for ($j=1; $j<=$rand; $j++)
									$row1 = mysqli_fetch_array($sql1);
								$vrednost = $row1['id'];
							} else {
								$vrednost = 0;
							}
							
	                        if ($s->checkSpremenljivka($row['id'], $isTestData=true)) {
							    if (isset($vrednost) && $vrednost > 0) {
							    	$srv_data_vrednost .= "('$row[id]', '$vrednost', '$this->usr_id'),";
	                                if ($row1['other'] == 1)
	                                    $srv_data_text .= "('$row[id]', '$vrednost', '".$this->randomString()."', '', '$this->usr_id'),";
	                            }
	                        } else {
	                            $srv_data_vrednost .= "('$row[id]', '-2', '$this->usr_id'),";
	                        }

	                    // checkbox
						} elseif ($row['tip'] == 2) {
							
							
	                        if ($s->checkSpremenljivka($row['id'], $isTestData=true)) {
	                        	
	                        	unset($vrednost);
	                        	$sql1 = sisplet_query("SELECT id, other FROM srv_vrednost WHERE spr_id='$row[id]'");
								
								$randX = 0;
								if($only_valid == 1)
									$randX = rand(1, mysqli_num_rows($sql1));
								
								$j=1;
								while ($row1 = mysqli_fetch_array($sql1)) {
									$rand = rand(-1, 1);
									
									if ($rand > 0){
										$vrednost[$row1['id']] = $row1['id'];
									}
									
									// Ce imamo samo veljavne vedno oznacimo enega
									if($randX == $j && $only_valid == 1){
										$vrednost[$row1['id']] = $row1['id'];
									}
									
									$j++;
								}
								
							    if (isset($vrednost)) {
								    foreach ($vrednost AS $key => $val) {
									    if ($val > 0) {
										    $srv_data_vrednost .= "('$row[id]', '$val', '$this->usr_id'),";
	                                        if (isset($row1['other']) && $row1['other'] == 1)
	                                            $srv_data_text .= "('$row[id]', '$val', '".$this->randomString()."', '', '$this->usr_id'),";
	                                    }
	                                }
							    }
	                        } else {
	                            $srv_data_vrednost .= "('$row[id]', '-2', '$this->usr_id'),";
	                        }					
						
	                        
						// multigrid
						} elseif ($row['tip'] == 6) {

							$sql1 = sisplet_query("SELECT id, other FROM srv_vrednost WHERE spr_id = '$row[id]'");
							while ($row1 = mysqli_fetch_array($sql1)) {

	                            if ($s->checkSpremenljivka($row['id'], $isTestData=true)) {
								    
								    $sql2 = sisplet_query("SELECT id FROM srv_grid WHERE spr_id='$row[id]'");
									
									// Ce imamo samo veljavne vedno oznacimo enega
									if($only_valid == 1)
										$rand = rand(1, mysqli_num_rows($sql2));
									else
										$rand = rand(0, mysqli_num_rows($sql2));
								    
								    if ($rand > 0) {
										for ($j=1; $j<=$rand; $j++)
											$row2 = mysqli_fetch_array($sql2);
										$grid_id = $row2['id'];
								    } else {
										$grid_id = 0;
								    }
								    
								    if ($grid_id > 0) {
	                                    $srv_data_grid .= "('$row[id]', '$row1[id]', '$this->usr_id', '$grid_id'),";
	                                }
									
							        if ($row1['other'] == 1 && $grid_id > 0)
	                                    $srv_data_text .= "('$row[id]', '$row1[id]', '".$this->randomString()."', '', '$this->usr_id'),";

	                            } else {
	                                $srv_data_vrednost .= "('$row[id]', '-2', '$this->usr_id'),";
	                            }
	                        }
	                    
						// multicheckbox
						} elseif ($row['tip'] == 16) {

							$sql1 = sisplet_query("SELECT id, other FROM srv_vrednost WHERE spr_id = '$row[id]'");
							
							while ($row1 = mysqli_fetch_array($sql1)) {
								$sql2 = sisplet_query("SELECT id FROM srv_grid WHERE spr_id = '$row[id]' ORDER BY vrstni_red");
								
								$randX = 0;
								if($only_valid == 1)
									$randX = rand(1, mysqli_num_rows($sql2));
								
								$j=1;
								while ($row2 = mysqli_fetch_array($sql2)) {
									if ($s->checkSpremenljivka($row['id'], $isTestData=true)) {
																			
										$rand = rand(-1, 1);
									    
									    if ($rand > 0) {
											$grid_id = $row2['id'];
									    } else {
											$grid_id = 0;
									    }
										
										// Ce imamo samo veljavne vedno oznacimo enega
										if($randX == $j && $only_valid == 1){
											$grid_id = $row2['id'];
										}
										
										$j++;
										
										if ($grid_id > 0) {
											$srv_data_checkgrid .= "('$row[id]', '$row1[id]', '$this->usr_id', '$grid_id'),";
										}
										
										if ($row1['other'] == 1 && $grid_id > 0)
											$srv_data_text .= "('$row[id]', '$row1[id]', '".$this->randomString()."', '', '$this->usr_id'),";
											
									} else {
										$srv_data_vrednost .= "('$row[id]', '-2', '$this->usr_id'),";
									}
								}												
							}
						}
	                	
	                	// multitext
						elseif ($row['tip'] == 19) {

							$sql1 = sisplet_query("SELECT id, other FROM srv_vrednost WHERE spr_id = '$row[id]'");
							
							while ($row1 = mysqli_fetch_array($sql1)) {				
								$sql2 = sisplet_query("SELECT id FROM srv_grid WHERE spr_id = '$row[id]' ORDER BY vrstni_red");
								
								while ($row2 = mysqli_fetch_array($sql2)) {                          												
									if ($s->checkSpremenljivka($row['id'], $isTestData=true)) {
									
										// Ce imamo samo veljavne imamo vedno vrednost
										if($only_valid == 1)
											$value = $this->randomString();
										else
											$value = rand(0,1)==0 ? $this->randomString() : '';
										
										$grid_id = $row2['id'];

										if ($value != '') {
											$srv_data_textgrid .= "('$row[id]', '$row1[id]', '$this->usr_id', '$grid_id', '$value'),";
										}
										
										// vsebino text polja vnesemo v vsakem primeru
										if ($row1['other'] == 1 && $value != '')
											$srv_data_text .= "('$row[id]', '$row1[id]', '".$this->randomString()."', '', '$this->usr_id'),";				
									}								
																
									else {
										$srv_data_vrednost .= "('$row[id]', '-2', '$this->usr_id'),";
									}
								}												
							}
						}
                                                
						//Lokacija
						elseif($row['tip'] == 26){
							//choose location
							$sql1 = sisplet_query("SELECT id FROM srv_vrednost WHERE spr_id = '$row[id]'", 'array');

							//so vrednosti, se pravi je choose
							if($sql1){
								foreach($sql1 as $row1){	
									if ($s->checkSpremenljivka($row['id'], $isTestData=true)) {
											// Ce imamo samo veljavne imamo vedno vrednost
											if($only_valid == 1)
													$vrednost = $this->randomString();
											else
													$vrednost = rand(0,1)==0 ? $this->randomString() : '';

											$srv_data_map .= "(" . $this->usr_id . ", '$row[id]', '$row1[id]', ". $this->anketa . ", '', '', '', '".
													($vrednost != '' ? $vrednost : '-1')."', ''),";
									} 
									else {
											$srv_data_vrednost .= "('$row[id]', '-2', '$this->usr_id'),";
									}
								}
							}
							//niso vrednosti, se pravi je moja ali multi lokacija
							else{
									if ($s->checkSpremenljivka($row['id'], $isTestData=true)) {
										
										$make_input = ($only_valid == 1 ? true : rand(0,1)==0);
										if($make_input){
										
												$lat = floatval(mt_rand(454000, 466500)/10000);
												$lng = floatval(mt_rand(136000, 163900)/10000);

												// Ce imamo samo veljavne imamo vedno vrednost
												if($only_valid == 1)
														$vrednost = $this->randomString();
												else
														$vrednost = rand(0,1)==0 ? $this->randomString() : '';

												$srv_data_map .= "(" . $this->usr_id . ", '$row[id]', '$row1[id]', ". $this->anketa . ", '$lat', '$lng', '[N/A]', '".
														($vrednost != '' ? $vrednost : '-1')."', ''),";
										}
									} 
									else {
											$srv_data_vrednost .= "('$row[id]', '-2', '$this->usr_id'),";
									}
							}
						}
						//Heatmap
						elseif($row['tip'] == 27){
							//choose location
							$sql1 = sisplet_query("SELECT id FROM srv_vrednost WHERE spr_id = '$row[id]'", 'array');

							//so vrednosti, se pravi je choose
							if($sql1){
								foreach($sql1 as $row1){	
									if ($s->checkSpremenljivka($row['id'], $isTestData=true)) {
											// Ce imamo samo veljavne imamo vedno vrednost
											if($only_valid == 1)
													$vrednost = $this->randomString();
											else
													$vrednost = rand(0,1)==0 ? $this->randomString() : '';

											$srv_data_heatmap .= "(" . $this->usr_id . ", '$row[id]', '$row1[id]', ". $this->anketa . ", '', '', '', '".
													($vrednost != '' ? $vrednost : '-1')."', ''),";
									} 
									else {
											$srv_data_vrednost .= "('$row[id]', '-2', '$this->usr_id'),";
									}
								}
							}
							//niso vrednosti, se pravi je moja ali multi lokacija
							else{
								if ($s->checkSpremenljivka($row['id'], $isTestData=true)) {
									
									$make_input = ($only_valid == 1 ? true : rand(0,1)==0);
									if($make_input){
									
											$lat = floatval(mt_rand(454000, 466500)/10000);
											$lng = floatval(mt_rand(136000, 163900)/10000);

											// Ce imamo samo veljavne imamo vedno vrednost
											if($only_valid == 1)
													$vrednost = $this->randomString();
											else
													$vrednost = rand(0,1)==0 ? $this->randomString() : '';

											$srv_data_heatmap .= "(" . $this->usr_id . ", '$row[id]', '$row1[id]', ". $this->anketa . ", '$lat', '$lng', '[N/A]', '".
													($vrednost != '' ? $vrednost : '-1')."', ''),";
									}
								} 
								else {
										$srv_data_vrednost .= "('$row[id]', '-2', '$this->usr_id'),";
								}
							}
						}
                                                
						// multinumber
						elseif ($row['tip'] == 20) {

							$sql1 = sisplet_query("SELECT id, other FROM srv_vrednost WHERE spr_id = '$row[id]'");
							
							while ($row1 = mysqli_fetch_array($sql1)) {				
								$sql2 = sisplet_query("SELECT id FROM srv_grid WHERE spr_id = '$row[id]' ORDER BY vrstni_red");
								
								while ($row2 = mysqli_fetch_array($sql2)) {                          												
									if ($s->checkSpremenljivka($row['id'], $isTestData=true)) {
									
										// Ce imamo samo veljavne imamo vedno vrednost
										if($only_valid == 1)
											$value = $this->randomNumber();
										else
											$value = rand(0,1)==0 ? $this->randomNumber() : '';
										
										$grid_id = $row2['id'];

										if ($value != '') {
											$srv_data_textgrid .= "('$row[id]', '$row1[id]', '$this->usr_id', '$grid_id', '$value'),";
										}
										
										// vsebino text polja vnesemo v vsakem primeru
										if ($row1['other'] == 1 && $value != '')
											$srv_data_text .= "('$row[id]', '$row1[id]', '".$this->randomString()."', '', '$this->usr_id'),";				
									}								
																
									else {
									 	$srv_data_vrednost .= "('$row[id]', '-2', '$this->usr_id'),";
									}
								}												
							}
						}
						
						// textbox
						elseif ($row['tip'] == 4) {
							
							if ($s->checkSpremenljivka($row['id'], $isTestData=true)) {
								$vrednost = rand(0,1)==0 ? $this->randomString() : '';
								if (isset($vrednost) && $vrednost != '')
									$srv_data_text .= "('$row[id]', '', '$vrednost', '', '$this->usr_id'),";
							} else {
		                        $srv_data_vrednost .= "('$row[id]', '-2', '$this->usr_id'),";
		                    }
						}
						
						// textbox*
						elseif ($row['tip'] == 21) {

	                        $sql1 = sisplet_query("SELECT id FROM srv_vrednost WHERE spr_id = '$row[id]'");
							while ($row1 = mysqli_fetch_array($sql1)) {	
							
								if ($s->checkSpremenljivka($row['id'], $isTestData=true)) {
									
									// Ce imamo samo veljavne imamo vedno vrednost
									if($only_valid == 1)
										$vrednost = $this->randomString();
									else
										$vrednost = rand(0,1)==0 ? $this->randomString() : '';
									
									if (isset($vrednost) && $vrednost != '')
										$srv_data_text .= "('$row[id]', '$row1[id]', '$vrednost', '', '$this->usr_id'),";
								} 
								
								else {
									$srv_data_vrednost .= "('$row[id]', '-2', '$this->usr_id'),";
								}
							}
						} 
						
						// number
						elseif ($row['tip'] == 7) {						
							#######	za ureditev avtomatskega vnosa glede na dolzino stevila
							if($row['ranking_k']==0){	//ce je stevilo
								$newLength = $row['cela'];								
 							}elseif($row['ranking_k']==1){	//ce je drsnik							
								$rowParams = Cache::srv_spremenljivka($row['id']);
								$spremenljivkaParams = new enkaParameters($rowParams['params']);
								$slider_MaxNumLabel = ($spremenljivkaParams->get('slider_MaxNumLabel') ? $spremenljivkaParams->get('slider_MaxNumLabel') : 100);
								$newLength = strlen((string)$slider_MaxNumLabel)-1;
							}
							####### za ureditev avtomatskega vnosa glede na dolzino stevila - konec
							
							if ($s->checkSpremenljivka($row['id'], $isTestData=true)) {

								$ij=1;
								
								unset($vrednost);
								
								// Ce imamo samo veljavne imamo vedno vrednost
								if($only_valid == 1){
									//if ($row['size'] >= 1) $vrednost[0] = $this->randomNumber();
									if ($row['size'] >= 1) $vrednost[0] = $this->randomNumber($newLength);
									//if ($row['size'] >= 2) $vrednost[1] = $this->randomNumber(); 
									if ($row['size'] >= 2) $vrednost[1] = $this->randomNumber($newLength); 
								}	
								elseif (rand(0,1) == 0) {
									//if ($row['size'] >= 1) $vrednost[0] = $this->randomNumber();
									if ($row['size'] >= 1) $vrednost[0] = $this->randomNumber($newLength);
									//if ($row['size'] >= 2) $vrednost[1] = $this->randomNumber(); 
									if ($row['size'] >= 2) $vrednost[1] = $this->randomNumber($newLength); 
								}
								
								if (isset($vrednost)){

									$text = '';
									$text2 = '';
									
									foreach ($vrednost AS $key => $val) {
										if($ij==1){
											if ($val != '')
												$text = $val;
										}
										
										else{
											if ($val != '')
												$text2 = $val;
										}

										$ij++;
									}
									
									$srv_data_text .= "('$row[id]', '', '$text', '$text2', '$this->usr_id'),";
								}
							}
							else {
								$srv_data_vrednost .= "('$row[id]', '-2', '$this->usr_id'),";
							}
						} 
						
						// compute
						elseif ($row['tip'] == 22) {

							if (true){
								
								$val = $s->checkCalculation(-$row['id']); 	// za spremenljivke je v srv_calculation, v cnd_id zapisan id spremenljivke kot minus (plus je za kalkulacije v ifih)

								if ($val != '')
									$srv_data_text .= "('$row[id]', '', '$val', '', '$this->usr_id'),";
							}
						}
					
						// 8_datum
						elseif ($row['tip'] == 8) {

	                        if ($s->checkSpremenljivka($row['id'], $isTestData=true)) {
							    
								// Ce imamo samo veljavne imamo vedno vrednost
								if($only_valid == 1)
									$vrednost = $this->randomDate();
								else
									$vrednost = rand(0,1)==0 ? $this->randomDate() : '';
							    
							    if (isset($vrednost) && $vrednost != '')
								    $srv_data_text .= "('$row[id]', '', '$vrednost', '', '$this->usr_id'),";
								
	                        } else {
	                            $srv_data_vrednost .= "('$row[id]', '-2', '$this->usr_id'),";
	                        }

						}
						
						// ranking
						elseif ($row['tip'] == 17) {
	                        if ($s->checkSpremenljivka($row['id'], $isTestData=true)) {
								
								//cifre
								if($row['design'] == 1 or true){	// tukaj se pac vse generira tukaj
									
									$sql1 = sisplet_query("SELECT id FROM srv_vrednost WHERE spr_id = '$row[id]' AND vrstni_red>0 ORDER BY vrstni_red");
									$rows = mysqli_num_rows($sql1);
									if ($row['ranking_k'] > 0)	$rows = $row['ranking_k'];
									unset($array);
									
									// Ce imamo samo veljavne imamo vedno vrednosti
									if($only_valid == 1){
										if (rand(0,1) == 0) $rows = floor($rows/2);
										$array = range(1, $rows);
										shuffle($array);
									}
									elseif (rand(0,1) == 0) {
										if (rand(0,1) == 0) $rows = floor($rows/2);
										$array = range(1, $rows);
										shuffle($array);
									}
									
									while($row1 = mysqli_fetch_array($sql1)){
									
										if (isset($array) && count($array) > 0) {
											
											$vrednost = array_pop($array);
											if (isset($vrednost) && $vrednost != '')
												$srv_data_rating .= "('$row[id]', '$row1[id]', '$this->usr_id', '$vrednost'),";
										}						
									}					
								}
								
								//n==k (sortable)
								else if($row['design'] == 2){							
								}							
								//n>k
								else if($row['design'] == 0){							
								}
													
							}
							else
								$srv_data_vrednost .= "('$row[id]', '-2', '$this->usr_id'),";
						}
						
						// vsota
						elseif ($row['tip'] == 18) {
						
							$sql1 = sisplet_query("SELECT id FROM srv_vrednost WHERE spr_id = '$row[id]' AND vrstni_red>0 ORDER BY vrstni_red");
							while($row1 = mysqli_fetch_array($sql1)){
							
								if ($s->checkSpremenljivka($row['id'], $isTestData=true)){
									
									unset($vrednost);
									
									// Ce imamo samo veljavne imamo vedno vrednosti
									if($only_valid == 1)
										$vrednost = $this->randomNumber();
									elseif(rand(0,1) == 0) 
										$vrednost = $this->randomNumber();
									
									if (isset($vrednost) && $vrednost != '')
										$srv_data_text .= "('$row[id]', '$row1[id]', '$vrednost', '', '$this->usr_id'),";
								}
								else
									$srv_data_vrednost .= "('$row[id]', '-2', '$this->usr_id'),";
							}					
						}
						
						// Kombinirana tabela
						elseif($row['tip'] == 24){
						
							// Loop cez podtabele kombinirane dabele
							$sqlC = sisplet_query("SELECT s.id, s.tip FROM srv_grid_multiple m, srv_spremenljivka s WHERE m.parent='$row[id]' AND m.spr_id=s.id ORDER BY m.vrstni_red");
							while ($rowC = mysqli_fetch_array($sqlC)) {
								
								// multigrid
								if ($rowC['tip'] == 6) {

									$sql1 = sisplet_query("SELECT id, other FROM srv_vrednost WHERE spr_id = '$rowC[id]'");
									while ($row1 = mysqli_fetch_array($sql1)) {

										if ($s->checkSpremenljivka($rowC['id'], $isTestData=true)) {
											
											$sql2 = sisplet_query("SELECT id FROM srv_grid WHERE spr_id='$rowC[id]'");
											
											// Ce imamo samo veljavne vedno oznacimo enega
											if($only_valid == 1)
												$rand = rand(1, mysqli_num_rows($sql2));
											else
												$rand = rand(0, mysqli_num_rows($sql2));
											
											if ($rand > 0) {
												for ($j=1; $j<=$rand; $j++)
													$row2 = mysqli_fetch_array($sql2);
												$grid_id = $row2['id'];
											} else {
												$grid_id = 0;
											}
											
											if ($grid_id > 0) {
												$srv_data_grid .= "('$rowC[id]', '$row1[id]', '$this->usr_id', '$grid_id'),";
											}
											
											if ($row1['other'] == 1 && $grid_id > 0)
												$srv_data_text .= "('$rowC[id]', '$row1[id]', '".$this->randomString()."', '', '$this->usr_id'),";

										} else {
											$srv_data_vrednost .= "('$rowC[id]', '-2', '$this->usr_id'),";
										}
									}
								
								// multicheckbox
								} elseif ($rowC['tip'] == 16) {

									$sql1 = sisplet_query("SELECT id, other FROM srv_vrednost WHERE spr_id = '$rowC[id]'");
									
									while ($row1 = mysqli_fetch_array($sql1)) {
										$sql2 = sisplet_query("SELECT id FROM srv_grid WHERE spr_id = '$rowC[id]' ORDER BY vrstni_red");
										
										$randX = 0;
										if($only_valid == 1)
											$randX = rand(1, mysqli_num_rows($sql2));
										
										$j=1;
										while ($row2 = mysqli_fetch_array($sql2)) {
											if ($s->checkSpremenljivka($rowC['id'], $isTestData=true)) {
																					
												$rand = rand(-1, 1);
												
												if ($rand > 0) {
													$grid_id = $row2['id'];
												} else {
													$grid_id = 0;
												}
												
												// Ce imamo samo veljavne vedno oznacimo enega
												if($randX == $j && $only_valid == 1){
													$grid_id = $row2['id'];
												}
												
												$j++;
												
												if ($grid_id > 0) {
													$srv_data_checkgrid .= "('$rowC[id]', '$row1[id]', '$this->usr_id', '$grid_id'),";
												}
												
												if ($row1['other'] == 1 && $grid_id > 0)
													$srv_data_text .= "('$rowC[id]', '$row1[id]', '".$this->randomString()."', '', '$this->usr_id'),";
													
											} else {
												$srv_data_vrednost .= "('$rowC[id]', '-2', '$this->usr_id'),";
											}
										}												
									}
									
								}
								
								// multitext
								elseif ($rowC['tip'] == 19) {

									$sql1 = sisplet_query("SELECT id, other FROM srv_vrednost WHERE spr_id = '$rowC[id]'");
									
									while ($row1 = mysqli_fetch_array($sql1)) {				
										$sql2 = sisplet_query("SELECT id FROM srv_grid WHERE spr_id = '$rowC[id]' ORDER BY vrstni_red");
										
										while ($row2 = mysqli_fetch_array($sql2)) {                          												
											if ($s->checkSpremenljivka($row['id'], $isTestData=true)) {
											
												// Ce imamo samo veljavne imamo vedno vrednost
												if($only_valid == 1)
													$value = $this->randomString();
												else
													$value = rand(0,1)==0 ? $this->randomString() : '';
												
												$grid_id = $row2['id'];

												if ($value != '') {
													$srv_data_textgrid .= "('$rowC[id]', '$row1[id]', '$this->usr_id', '$grid_id', '$value'),";
												}

												// vsebino text polja vnesemo v vsakem primeru
												if ($row1['other'] == 1 && $value != '')
													$srv_data_text .= "('$rowC[id]', '$row1[id]', '".$this->randomString()."', '', '$this->usr_id'),";				
											}								
																		
											else {
												$srv_data_vrednost .= "('$rowC[id]', '-2', '$this->usr_id'),";
											}
										}												
									}
								}
								
								// multinumber
								elseif ($rowC['tip'] == 20) {

									$sql1 = sisplet_query("SELECT id, other FROM srv_vrednost WHERE spr_id = '$rowC[id]'");
									
									while ($row1 = mysqli_fetch_array($sql1)) {				
										$sql2 = sisplet_query("SELECT id FROM srv_grid WHERE spr_id = '$rowC[id]' ORDER BY vrstni_red");
										
										while ($row2 = mysqli_fetch_array($sql2)) {                          												
											if ($s->checkSpremenljivka($rowC['id'], $isTestData=true)) {
											
												// Ce imamo samo veljavne imamo vedno vrednost
												if($only_valid == 1)
													$value = $this->randomNumber();
												else
													$value = rand(0,1)==0 ? $this->randomNumber() : '';
												
												$grid_id = $row2['id'];

												if ($value != '') {
													$srv_data_textgrid .= "('$rowC[id]', '$row1[id]', '$this->usr_id', '$grid_id', '$value'),";
												}
												
												// vsebino text polja vnesemo v vsakem primeru
												if ($row1['other'] == 1 && $value != '')
													$srv_data_text .= "('$rowC[id]', '$row1[id]', '".$this->randomString()."', '', '$this->usr_id'),";				
											}								
																		
											else {
												$srv_data_vrednost .= "('$rowC[id]', '-2', '$this->usr_id'),";
											}
										}												
									}
								}
								
							}
						}
					}
					
					// vprasanja shranjujemo sproti, zaradi IFov !!!
					
					// odrezemo zadnjo vejico, ker smo jo dodajali kar povsod
					$srv_data_grid = substr($srv_data_grid, 0, -1);
					$srv_data_vrednost = substr($srv_data_vrednost, 0, -1);
					$srv_data_text = substr($srv_data_text, 0, -1);
					$srv_data_checkgrid = substr($srv_data_checkgrid, 0, -1);
					$srv_data_textgrid = substr($srv_data_textgrid, 0, -1);
					$srv_data_rating = substr($srv_data_rating, 0, -1);
                    $srv_data_map = substr($srv_data_map, 0, -1);
                    $srv_data_heatmap = substr($srv_data_heatmap, 0, -1);
								
					if ($srv_data_grid != '') {		$sq = sisplet_query("INSERT INTO srv_data_grid".$this->db_table." (spr_id, vre_id, usr_id, grd_id) VALUES $srv_data_grid");		if (!$sq) echo 'err011: '.mysqli_error($GLOBALS['connect_db']); }
					if ($srv_data_vrednost != '') {	$sq = sisplet_query("INSERT INTO srv_data_vrednost".$this->db_table." (spr_id, vre_id, usr_id) VALUES $srv_data_vrednost");		if (!$sq) echo 'err012: '.mysqli_error($GLOBALS['connect_db']); }
					if ($srv_data_text != '') {		$sq = sisplet_query("INSERT INTO srv_data_text".$this->db_table." (spr_id, vre_id, text, text2, usr_id) VALUES $srv_data_text");						if (!$sq) echo 'err013: '.mysqli_error($GLOBALS['connect_db']); }
					if ($srv_data_checkgrid != ''){ $sq = sisplet_query("INSERT INTO srv_data_checkgrid".$this->db_table." (spr_id, vre_id, usr_id, grd_id) VALUES $srv_data_checkgrid");				if (!$sq) echo 'err014: '.mysqli_error($GLOBALS['connect_db']); }
					if ($srv_data_textgrid != '') {	$sq = sisplet_query("INSERT INTO srv_data_textgrid".$this->db_table." (spr_id, vre_id, usr_id, grd_id, text) VALUES $srv_data_textgrid");			if (!$sq) echo 'err015: '.mysqli_error($GLOBALS['connect_db']); }
					if ($srv_data_rating != '') {	$sq = sisplet_query("INSERT INTO srv_data_rating (spr_id, vre_id, usr_id, vrstni_red) VALUES $srv_data_rating");					if (!$sq) echo 'err016: '.mysqli_error($GLOBALS['connect_db']); }
                    if ($srv_data_map != '') {	$sq = sisplet_query("INSERT INTO srv_data_map (usr_id, spr_id, vre_id, ank_id, lat, lng, address, text, vrstni_red) VALUES $srv_data_map");					if (!$sq) echo 'err016: '.mysqli_error($GLOBALS['connect_db']); }                                        
					if ($srv_data_heatmap != '') {	$sq = sisplet_query("INSERT INTO srv_data_heatmap (usr_id, spr_id, vre_id, ank_id, lat, lng, address, text, vrstni_red) VALUES $srv_data_heatmap");					if (!$sq) echo 'err017: '.mysqli_error($GLOBALS['connect_db']); }                                        
				}
				
				$recnum++;
			}
			# zabeležimo kdaj so bili dodani testni vnosi
			if (count($arrayTestni)) {
				global $global_user_id;
				$ins_date = date ("Y-m-d H:m:s");
				
				$insert_qry = "INSERT INTO srv_testdata_archive (ank_id, add_date, add_uid, usr_id) VALUES ";
				$prefix = '';
				foreach ($arrayTestni AS $at_user_id) {
					$insert_qry .= $prefix."('".$this->anketa."', '$ins_date', '$global_user_id', '$at_user_id')";
					$prefix = ', ';
				}
				
				sisplet_query($insert_qry);
			}
			
			if(session_id() == '') {session_start();}
			$_SESSION['progressBar'][$this->anketa]['status'] = 'end';
			session_commit();
			unset($_SESSION['progressBar'][$this->anketa]);	// ce getCollectTimer ne prebere vec 'end' (se prehitro refresha), se tukaj odstranimo sejo
			
			header("Location: index.php?anketa=$this->anketa&a=testiranje&m=testnipodatki");
			
		} elseif ((isset($_GET['delete_testdata']) && $_GET['delete_testdata'] == 1) || (isset($_GET['delete_autogen_testdata']) && $_GET['delete_autogen_testdata'] == 1)) {
			
			if(isset($_GET['delete_autogen_testdata']) && $_GET['delete_autogen_testdata'] == 1)
				sisplet_query("DELETE FROM srv_user WHERE ank_id='$this->anketa' AND testdata='2'");
			else
				sisplet_query("DELETE FROM srv_user WHERE ank_id='$this->anketa' AND (testdata='1' OR testdata='2')");

			#datoteki z podatki moramo zgenerirati na novo
			sisplet_query("UPDATE srv_data_files SET head_file_time='0000-00-00', data_file_time='0000-00-00' WHERE sid='$this->anketa'");
			sisplet_query("COMMIT");
			
			header("Location: ".$_SERVER['HTTP_REFERER']);
			
		// izpis podatkov
		} else {
			
			$sql = sisplet_query("SELECT COUNT(*) AS count FROM srv_user WHERE ank_id='$this->anketa' AND (testdata='1' OR testdata='2')");
			$row = mysqli_fetch_array($sql);
			$total_rows = $row['count'];
	
			echo '<form name="" action="ajax.php?anketa='.$this->anketa.'&a=testiranje&m=testnipodatki" method="post" onsubmit="init_progressBar(true);">';
			
			echo '<fieldset><legend>'.$lang['srv_testiranje_vnosi'].'</legend>';
			echo '<p class="bottom16">'.$lang['srv_testni_nagovor'].'</p>';

			echo '<div class="setting_holder">';
			echo $lang['srv_stevilo_vnosov'].':';
			echo '<div class="max_wrap">';
			echo '<input class="text large" type="text" name="stevilo_vnosov" value="1" onkeyup="max_stevilo_vnosov();">';
			echo '<span class="gray">(max. 1000)</span>';
			echo '</div>';
			echo '</div>';

			echo '<div class="setting_holder">';
			echo '<input type="hidden" name="only_valid" id="only_valid_0" value="0" />';
			echo '<input type="checkbox" name="only_valid" id="only_valid_1" value="1">';
			echo '<label for="only_valid_1">'.$lang['srv_testni_samo_veljavni'].'</label>';
			echo '</div>';

			echo '<div class="button_holder">';
			echo '<button class="medium blue" type="submit">'.$lang['srv_dodaj_vnose'].'</button>';
			echo '</div>';

			echo '</form>';
			
			echo '<p class="bottom16">'.$lang['srv_testni_nakonec'].'</p>';

			echo '<a class="noline" href="#" onClick="delete_test_data();"><span class="faicon trash empty link-right"></span>'.$lang['srv_delete_testdata'].'</a> ('.$total_rows.')';
			
		
			if ($total_rows > 0) {

				echo '<table class="table_header">';
				echo '<tbody>';
					echo '<tr>';
						echo '<td> Labela </td>';
						echo '<td> Veljavni odgovori (Grafični prikaz) </td>';
					echo '</tr>';
				echo '</tbody>';	
				echo '</table>';
				

				$prevpage = 0;


				$sql = sisplet_query("SELECT s.id, s.gru_id, s.tip, s.naslov, g.naslov AS pagename FROM srv_spremenljivka s, srv_grupa g WHERE s.gru_id=g.id AND s.visible='1' AND g.ank_id='$this->anketa' ORDER BY g.vrstni_red, s.vrstni_red");
				while ($row = mysqli_fetch_array($sql)) {
					
					// labela in compute ne upostevamo
					if ($row['tip'] != 5 && $row['tip'] != 22) {
					
						if ($prevpage == 0 || $row['gru_id'] != $prevpage) {
							
							if ($prevpage > 0) {
								echo '</table>';
								echo '</fieldset>';
							}

							echo '<fieldset class="page_num_fieldset">';
							echo '<legend class="page_num">'.$row['pagename'].'</legend>';
							// echo '<span class="dotted_line"> - </span>';
							echo '<table class="avt_vnosi_table">';
							
							$prevpage = $row['gru_id'];
						}
						
						echo '<tr><td title="'.strip_tags($row['naslov']).'">'.skrajsaj(strip_tags($row['naslov']),50).'</td>';
						
						// radio ali select, checkbox, textbox, textbox*, number, datum
						if ( ($row['tip']==1 || $row['tip']==3 || $row['tip']==2 || $row['tip']==4 || $row['tip']==21 || $row['tip']==7 || $row['tip']==8) ) {
							
							$sqlc = sisplet_query("SELECT COUNT(*) AS count FROM srv_data_vrednost".$this->db_table." dv, srv_user u WHERE dv.usr_id=u.id AND (u.testdata='1' OR u.testdata='2') AND spr_id='$row[id]' AND vre_id='-2'");
							$rowc = mysqli_fetch_array($sqlc);
							$p = round(($total_rows-$rowc['count'])/$total_rows*100,2);
	                        
						// multigrid
						} elseif ($row['tip'] == 6) {

							$sql1 = sisplet_query("SELECT id FROM srv_vrednost WHERE spr_id = '$row[id]'");
							while ($row1 = mysqli_fetch_array($sql1)) {

	                            $sqlc = sisplet_query("SELECT COUNT(*) AS count FROM srv_data_grid".$this->db_table." dg, srv_user u WHERE dg.usr_id=u.id AND (u.testdata='1' OR u.testdata='2') AND spr_id='$row[id]' AND vre_id='$row1[id]' AND grd_id='-2'");
								$rowc = mysqli_fetch_array($sqlc);
								$p = round(($total_rows-$rowc['count'])/$total_rows*100,2);	
	                        }
	                    
						// multicheckbox
						} elseif ($row['tip'] == 16) {

							$sql1 = sisplet_query("SELECT id FROM srv_vrednost WHERE spr_id = '$row[id]'");
							
							while ($row1 = mysqli_fetch_array($sql1)) {
								$sql2 = sisplet_query("SELECT id FROM srv_grid WHERE spr_id = '$row[id]' ORDER BY vrstni_red");
								
								while ($row2 = mysqli_fetch_array($sql2)) {
									
									$sqlc = sisplet_query("SELECT COUNT(*) AS count FROM srv_data_checkgrid".$this->db_table." cg, srv_user u WHERE cg.usr_id=u.id AND (u.testdata='1' OR u.testdata='2') AND spr_id='$row[id]' AND vre_id='$row1[id]' AND grd_id='-2'");
									$rowc = mysqli_fetch_array($sqlc);
									$p = round(($total_rows-$rowc['count'])/$total_rows*100,2);
								}												
							}
							
						}
	                	
	                	// multitext, multinumber
						elseif ($row['tip'] == 19 || $row['tip'] == 20) {

							$sql1 = sisplet_query("SELECT id FROM srv_vrednost WHERE spr_id = '$row[id]'");
							
							while ($row1 = mysqli_fetch_array($sql1)) {				
								$sql2 = sisplet_query("SELECT id FROM srv_grid WHERE spr_id = '$row[id]' ORDER BY vrstni_red");
								
								while ($row2 = mysqli_fetch_array($sql2)) {                          												
									
									$sqlc = sisplet_query("SELECT COUNT(*) AS count FROM srv_data_textgrid".$this->db_table." tg, srv_user u WHERE tg.usr_id=u.id AND (u.testdata='1' OR u.testdata='2') AND spr_id='$row[id]' AND vre_id='$row1[id]' AND grd_id='0' AND text='-2'");
									$rowc = mysqli_fetch_array($sqlc);
									$p = round(($total_rows-$rowc['count'])/$total_rows*100,2);
								}												
							}
							
						}
						
						// ranking, vsota
						elseif ($row['tip'] == 17 || $row['tip'] == 18) {
						
								$sql1 = sisplet_query("SELECT id FROM srv_vrednost WHERE spr_id = '$row[id]' AND vrstni_red>0 ORDER BY vrstni_red");
								while($row1 = mysqli_fetch_array($sql1)){
								
									$sqlc = sisplet_query("SELECT COUNT(*) AS count FROM srv_data_text".$this->db_table." dt, srv_user u WHERE dt.usr_id=u.id AND (u.testdata='1' OR u.testdata='2') AND spr_id='$row[id]' AND vre_id='$row1[id]' AND text='-2'");
									$rowc = mysqli_fetch_array($sqlc);
									$p = round(($total_rows-$rowc['count'])/$total_rows*100,2);
								}							
						}
						
						echo '<td class="graph_cell">';
						echo '<div class="graph_cell">';
						echo '  <div class="graph_full" style="width: '.($p*0.9).'%"></div>';
						echo '  <span>'.$p.'% ('.($total_rows-$rowc['count']).')</span>';
						echo '</div>';
						echo '</td>';
						
						echo '</tr>';
						
					}
				}
				
				echo '</table>';
				echo '</fieldset>';

				echo '</fieldset>';
			}		
		}
	}
	
	/**
	* zgenerira random string za vpis v tekstovno polje
	* 
	*/
	function randomString ($length = 10, $chars = 'abcdefghijklmnopqrstuvwxyz') {
	    // Length of character list
	    $chars_length = (strlen($chars) - 1);

	    // Start our string
	    $string = $chars[mt_rand(0, $chars_length)];
	   
	    // Generate random string
	    for ($i = 1; $i < $length; $i = strlen($string))
	    {
	        // Grab a random character from our list
	        $r = $chars[mt_rand(0, $chars_length)];
	       
	        // Make sure the same two characters don't appear next to each other
	        if ($r != $string[$i - 1]) $string .=  $r;
	    }
	   
	    // Return the string
	    return $string;
	}
	
	function randomNumber ($length = 4, $chars = '0123456789') {
		return $this->randomString($length, $chars);
	}
	
	function randomDate ($startDate = '01.01.1950', $endDate = '') {
		if ($endDate == '') $endDate = date("d.m.Y");
	    $days = round((strtotime($endDate) - strtotime($startDate)) / (60 * 60 * 24));
	    $n = rand(0,$days);
	    return date("d.m.Y",strtotime("$startDate + $n days"));   
	}
	
	function displayBtnMailtoPreview($row) {
		global $lang;
		echo '<div class="floatLeft"><div class="buttonwrapper">';
		echo '<a class="ovalbutton ovalbutton_orange" href="#" onclick="'.( ($row['active'] != 1) ? 'genericAlertPopup(\'srv_anketa_noactive2\'); ' : 'preview_mailto_email(); ').'return false;">';
		echo '<span>';
		//'<img src="icons/icons/accept.png" alt="" vartical-align="middle" />'
		echo  $lang['srv_mailto_preview'] . '</span></a></div></div>';

	}
	
	function DisplayNastavitveTrajanje() {	
		global $lang;
		global $site_url;
		
		# vsilimo refresh podatkov
		SurveyInfo :: getInstance()->resetSurveyData();
		$row = SurveyInfo::getInstance()->getSurveyRow();

		$starts =  date('d.m.Y',strtotime($row['starts']));
		$_expire = explode('-',$row['expire']);
		$expire =  $_expire[2].'.'.$_expire[1].'.'.$_expire[0];
		

        // Preklop domene na oneclicksurvey.com - ne pustimo spremembe aktivnosti
        if(DomainChange::check1kaDomainChange() && DomainChange::getInstance($global_user_id)->domainChangeLimits()){
            return;
        }

		echo '<fieldset>';
		echo '<legend>'.$lang['srv_activate_duration_2'].'</legend>';
		echo '<div class="setting_holder" id="start">';
		echo '<span class="setting_title">' . $lang['srv_activate_duration_manual_from'].'</span>';
		echo '<div class="icon_holder">';
		echo '<input id="startsManual1" type="text" class="large" readonly="true" name="durationStarts" value="' . $starts . '" autocomplete="off"/>';
		echo '</div>';
		echo '</div>';
		
		echo '<div class="setting_holder" id="expire">';
		echo '<span class="setting_title">' . $lang['srv_activate_duration_manual_to'].'</span>';
		echo '<div class="icon_holder">';
		echo '<input id="expireManual1" type="text" class="large '.($row['expire'] == PERMANENT_DATE ? ' disabled' : '').'" readonly="true" name="durationExpire" value="' . $expire . '" autocomplete="off"/>';
		echo '</div>';
		echo '</div>';
		
		echo '<div class="setting_holder">';
		echo '<input id="expirePermanent" type="checkbox" name="expirePermanent" value="1"'.($row['expire'] == PERMANENT_DATE ? ' checked="checked"' : '').' autocomplete="off" onchange="setExpirePermanent();"/>';
		echo '<label for="expirePermanent">' . $lang['srv_trajna_anketa'].'</label>';
		echo '</div>';
		
		echo '</fieldset>';
		
		echo '
				<script type="text/javascript">
				$(document).ready(function () {
                    datepicker("#startsManual1");
                    datepicker("#expireManual1");';
					
		if($row['expire'] == PERMANENT_DATE)
			echo '$("#expire").find(".ui-datepicker-trigger").addClass("disabled");';

		echo '		});
			</script>';
	}
	
	function DisplayNastavitveMaxGlasov() {
		global $lang;
		global $site_url;
		global $site_path;
		global $admin_type;
		global $global_user_id;
		
		$row = SurveyInfo::getInstance()->getSurveyRow();
		
		//Nastavitev max stevila glasov
		echo '<fieldset>';
		echo '<legend>'.$lang['srv_vote_quotas'].' '.Help::display('srv_activity_quotas').'</legend>';
		
		echo '<div class="setting_holder '.($row['vote_limit'] == 0 ? 'bottom0' : '').'" id="vote_limit">';
		
		echo '<span class="setting_title">' . $lang['srv_vote_quotas_2'] . ':</span>';
		echo '<div class="setting_item">';
		echo '<input type="radio" name="vote_limit" value="0" id="vote_limit_0"' . ($row['vote_limit'] == 0 ? ' checked="checked"' : '') . ' onClick="voteCountStatus(0)" />';
		echo '<label for="vote_limit_0">' . $lang['srv_vote_quotas_val0'] . '</label>';
		echo '</div>';
		echo '<div class="setting_item">';
		echo '<input type="radio" name="vote_limit" value="2" id="vote_limit_2"' . ($row['vote_limit'] == 2 ? ' checked="checked"' : '') . ' onClick="voteCountStatus(2)" />';
		echo '<label for="vote_limit_2">' . $lang['srv_vote_quotas_val2'] . ' '.Help::display('srv_activity_quotas_valid').'</label>';
		echo '</div>';
		echo '<div class="setting_item">';
		echo '<input type="radio" name="vote_limit" value="1" id="vote_limit_1"' . ($row['vote_limit'] == 1 ? ' checked="checked"' : '') . ' onClick="voteCountStatus(1)" />';
		echo '<label for="vote_limit_1">' . $lang['srv_vote_quotas_val1'] . '</label>';
		echo '</div>';
		
		echo '</div>';


		// Opozorilo, da je limit odgovorov presezen
		echo '<div id="vote_limit_all_warning" class="bottom16 '.($row['vote_limit'] == 0 || $row['vote_limit'] == 2 ? 'displayNone' : '').'">';
		echo '	<div class="setting_holder red">'.$lang['srv_vote_quotas_all_warning'].'</div>';
		echo '</div>';


		// Opozorilo, da je limit odgovorov presezen
		echo '<div id="vote_limit_warning" class="'.($row['vote_limit'] == 0 ? 'displayNone' : '').'">';

		echo '<div class="setting_holder">';
		echo '<span class="setting_title">'.$lang['srv_vote_count'].':</span>';
		echo '<input type="text" class="large" id="vote_count" name="vote_count" value="' . $row['vote_count'] . '"/></label>';
		echo '</div>';

		echo '<p class="top16">'.$lang['srv_survey_voteLimit_warning'].':</p>';
		
		$srv_survey_non_active_voteLimit = SurveySetting::getInstance()->getSurveyMiscSetting('srvlang_srv_survey_non_active_voteLimit');
		if ($srv_survey_non_active_voteLimit == '') $srv_survey_non_active_voteLimit = $lang['srv_survey_non_active_voteLimit'];				
		echo '<p><span class="italic">'.$srv_survey_non_active_voteLimit.' </span><a href="'.$site_url.'admin/survey/index.php?anketa='.$this->anketa.'&a=jezik"><span class="faicon edit"></span></a><p>';
				
		echo '</div>';


		echo '</fieldset>';
	}
	

	/**
	 * Uporabnik (administrator, manager) lahko dodajata nove uporabnike in jim dodelita dostop
	 */
	public function dodeljeniUporabniki(){
	    global $admin_type;
	    global $lang;
	    global $global_user_id;

		// managerji in admini lahko dodajajo uporabnike, ki jih nato managirajo
        if( !($admin_type == 1 || $admin_type == 0) )
            return '';

		$error = isset($_GET['error']) ? $_GET['error'] : '';


        // MANAGER lahko samo doda novega uporabnika pod sebe
        if($admin_type == 1){

            echo '<fieldset><legend>'.$lang['n_users_list_add'].'</legend>';

            echo '<div class="setting_holder">';
            echo '  <span class="setting_title">'.$lang['srv_users_add_new_type2'].'</span>';           
            echo '</div>';


            // Na virtualkah imajo managerji omejitev st. dodeljenih uporabnikov - ZAENKRAT JE TO ONEMOGOCENO, KASNEJE SE LAHKO OMEJI NA PAKET
            /*if(false && isVirtual() && $admin_type == 1){

                // Limit st. dodeljenih uporabnikov
                $managed_accounts_limit = 5;

                // Prestejemo dodeljene uporabnike
                $sql = sisplet_query("SELECT u.email
                                        FROM srv_dostop_manage m, users u
                                        WHERE m.manager='".$global_user_id."' AND u.id=m.user AND u.email NOT LIKE ('D3LMD-%') AND u.email NOT LIKE ('UNSU8MD-%')
                                    ");

                $managed_accounts_count = mysqli_num_rows($sql);


                echo '<p class="bold bottom16">';
                echo $lang['srv_users_add_assigned_max_1'].' <span class="red bold">'.$managed_accounts_limit.'</span> '.$lang['srv_users_add_assigned_max_2'].'</p>';

                // Manager na virtualkah ima omejitev koliko uporabnikov lahko pregleduje
                if($managed_accounts_count >= $managed_accounts_limit){
                    echo '<p>';
                    echo $lang['srv_users_add_assigned_max_reached'];
                    echo '</p>';
                    
                    return;
                }
                elseif($managed_accounts_count > 0){             
                    echo '<p>';
                    echo $lang['srv_users_add_assigned_current'].' <span class="red bold">'.$managed_accounts_count.' '.$lang['of'].' '.$managed_accounts_limit.'</span></p>';
                }
            }*/


            // Dodajanje novih uporabnikov pod pregled - ustvari racun, doda uporabnika pod pregled in mu poslje mail
            echo '<div id="add_user_option_2" class="add_user_option">';
            echo '<form class="manager_add_user" name="manager_add_user" action="ajax.php?t=dostop&a=manager_add_user" method="post">';
            
            echo '<div class="setting_holder horizontal">';
            echo '  <label>'.$lang['name'].':</label>';
            echo '  <input type="text" class="text large" id="name" name="name">';
            echo '</div>';

            echo '<div class="setting_holder">';
            echo '  <label>'.$lang['surname'].':</label>';
            echo '  <input type="text" class="text large" id="surname" name="surname">';
            echo '</div>';

            echo '<div class="setting_holder">';
            echo '  <label>'.$lang['email'].':</label>';
            echo '  <input type="email" class="text large" id="email" name="email"> '.(empty($_GET['add']) && $error=='email'?'<span class="red">'.$lang['srv_added_false'].'</span>':'');
            echo '</div>';

            echo '<div class="setting_holder horizontal">';
            echo '  <label>'.$lang['password'].':</label>';
            echo '  <input type="password" class="text large" id="password" name="password"> '.(empty($_GET['add']) && $error=='pass'?'<span class="red">'.$lang['pass_doesnt_match'].'</span>':'');
            echo '</div>';

            echo '<div class="setting_holder">';
            echo '  <label>'.$lang['password_confirm'].':</label>';
            echo '  <input type="password" class="text large" id="password2" name="password2">';
            echo '</div>';

            echo '<div class="button_holder top16 bottom0">';
            echo '  <button class="medium blue" onClick="document.manager_add_user.submit();">'.$lang['create_add'].'</button>';
            echo '</div>';

            echo '</form>';
            echo '</div>'; //new_user_option


            // Managerji lahko dodajajo samo uporabnike z dolocenimi emaili (če jim domeno posebej nastavi admin)
            // TODO: trenutno onemogočimo dodaja ostalih uporabnikov za managerje. Ko bo stvar vezana na domeno se jim bo omogočilo dodajanje samo domenskih
            /*elseif(false && $admin_type == 1){

                UserSetting :: getInstance()->Init($global_user_id);
                $emails = UserSetting :: getInstance()->getUserSetting('manage_domain');

                echo '<br><form class="manager_add_user" action="ajax.php?t=dostop&a=admin_add_user" method="post">';
                echo '<h3><b>'.sprintf($lang['srv_manager_add_user3'], $emails).'<br />'.$lang['srv_manager_add_user4'].'</b></h3>';
                echo '<p><input name="uemail" value="" style="width: 500px;" id="manager-email"><span id="manager-email-obvestilo"></span></p>';
                echo '<p><button type="submit" id="manager-email-submit" style="display:none;">'.$lang['add'].'</button></p>';

                echo '</form>';
            }*/
        
            echo '</fieldset>';
        }
        // ADMIN lahko tudi doda uporabnika brez pregleda in doda obstojecega pod pregled
        else{

            echo '<fieldset><legend>'.$lang['n_users_list_add'].'</legend>';

            echo '<div class="setting_holder">';
            echo '<span class="setting_title">'.$lang['srv_users_add_new_type'].'</span>';
        
            // Dodajanje novega uporabnika v sistem
            echo '<div class="setting_item">';
            echo '  <input type="radio" id="user_add_type_1" name="user_add_type" value="1" checked="checked" onClick="toggleAddUser(this.value);">';
            echo '  <label for="user_add_type_1">'. $lang['srv_users_add_new_type1'] .'</label> ';
            echo '</div>';

            // Dodajanje novega uporabnika v sistem in pod svoj pregled
            echo '<div class="setting_item">';
            echo '  <input type="radio" id="user_add_type_2" name="user_add_type" value="2" onClick="toggleAddUser(this.value);">';
            echo '  <label for="user_add_type_2">'. $lang['srv_users_add_new_type2'] .'</label> ';
            echo '</div>';

            // Dodajanje obstoječega uporabnika pod pregled
            echo '<div class="setting_item">';
            echo '  <input type="radio" id="user_add_type_3" name="user_add_type" value="3" onClick="toggleAddUser(this.value);">';
            echo '  <label for="user_add_type_3">'. $lang['srv_users_add_new_type3'] .'</label> ';
            echo '</div>';
            
            echo '</div>';


            // Dodajanje novih uporabnikov - ustvari racun in mu poslje mail
            echo '<div id="add_user_option_1" class="add_user_option">';
            echo '<form class="add_new_user" name="add_new_user" action="ajax.php?t=dostop&a=add_new_user" method="post">';
            
            echo '<div class="setting_holder horizontal">';
            echo '  <label>'.$lang['name'].':</label>';
            echo '  <input type="text" class="text large" id="name" name="name">';
            echo '</div>';

            echo '<div class="setting_holder">';
            echo '  <label>'.$lang['surname'].':</label>';
            echo '  <input type="text" class="text large" id="surname" name="surname">';
            echo '</div>';

            echo '<div class="setting_holder">';
            echo '  <label>'.$lang['email'].':</label>';
            echo '  <input type="email" class="text large" id="email" name="email"> '.(empty($_GET['add']) && $error=='email'?'<span class="red">'.$lang['srv_added_false'].'</span>':'');
            echo '</div>';

            echo '<div class="setting_holder horizontal">';
            echo '  <label>'.$lang['password'].':</label>';
            echo '  <input type="password" class="text large" id="password" name="password"> '.(empty($_GET['add']) && $error=='pass'?'<span class="red">'.$lang['pass_doesnt_match'].'</span>':'');
            echo '</div>';

            echo '<div class="setting_holder">';
            echo '  <label>'.$lang['password_confirm'].':</label>';
            echo '  <input type="password" class="text large" id="password2" name="password2">';
            echo '</div>';

            echo '<div class="setting_holder">';
            echo '  <label>'.$lang['lang'].':</label>';
            echo '  <input type="radio" id="jezik_1" name="jezik" value="1" '.($lang['id'] == '1' ? 'checked="checked"' : '').'><label for="jezik_1">'.$lang['srv_diagnostics_filter_lang_slo'].'</label>';
            echo '  <input type="radio" id="jezik_2" name="jezik" value="2" '.($lang['id'] == '2' ? 'checked="checked"' : '').'><label for="jezik_2">'.$lang['srv_diagnostics_filter_lang_ang'].'</label>';
            echo '</div>';

            echo '<div class="button_holder top16 bottom0">';
            echo '  <button class="medium blue" onClick="document.add_new_user.submit();">'.$lang['create'].'</button>';
            echo '</div>';

            echo '</form>';
            echo '</div>';


            // Dodajanje novih uporabnikov pod pregled - ustvari racun, doda uporabnika pod pregled in mu poslje mail
            echo '<div id="add_user_option_2" class="add_user_option displayNone">';
            echo '<form class="manager_add_user" name="manager_add_user" action="ajax.php?t=dostop&a=manager_add_user" method="post">';
            
            echo '<div class="setting_holder horizontal">';
            echo '<div class="setting_holder">';
            echo '<span class="setting_title">'.$lang['name'].':</span>';
            echo '<input type="text" class="text large" id="name" name="name">';
            echo '</div>';

            echo '<div class="setting_holder">';
            echo '<span class="setting_title">'.$lang['surname'].':</span>';
            echo '<input type="text" class="text large" id="surname" name="surname">';
            echo '</div>';
            echo '</div>';

            echo '<div class="setting_holder">';
            echo '<p>'.$lang['email'].':</p>';
            echo '<input type="email" class="text large" id="email" name="email"> '.(empty($_GET['add']) && $error=='email'?'<span class="red">'.$lang['srv_added_false'].'</span>':'');
            echo '</div>';

            echo '<div class="setting_holder horizontal">';
            echo '<div class="setting_holder">';
            echo '<p>'.$lang['password'].':</p>';
            echo '<input type="password" class="text large" id="password" name="password"> '.(empty($_GET['add']) && $error=='pass'?'<span class="red">'.$lang['pass_doesnt_match'].'</span>':'');
            echo '</div>';

            echo '<div class="setting_holder">';
            echo '<p>'.$lang['password_confirm'].':</p>';
            echo '<input type="password" class="text large" id="password2" name="password2">';
            echo '</div>';
            echo '</div>';

            echo '<div class="button_holder top16 bottom0">';
            echo '  <button class="medium blue" onClick="document.manager_add_user.submit();">'.$lang['create_add'].'</button>';
            echo '</div>';

            echo '</form>';
            
            echo '</div>';


            // Dodajanje obstojecih uporabnikov pod pregled
            echo '<div id="add_user_option_3" class="add_user_option displayNone">';

            echo '<form class="manager_add_user" name="admin_add_dostop" action="ajax.php?t=dostop&a=admin_add_user" method="post">';

            echo '<div class="setting_holder">';
            echo '  <p class="bottom4">'.$lang['srv_manager_add_user_label'].':</p>';
            echo '  <select name="uid" class="js-obstojeci-uporabniki-admin-ajax drowpdown large" style="width: 460px;"></select>';
            echo '</div>';

            echo '<div class="button_holder top16 bottom0">';
            echo '  <button class="medium blue" onCclick="document.admin_add_dostop.submit();">'.$lang['add'].'</button>';
            echo '</div>';

            echo '</form>';

            echo '</div>';
    
        
            echo '</fieldset>';
        }		
    }

	/**
	 * Seznam vseh uporabnikov znotraj 1ke
	 */
	public function allUsersList(){
		global $lang;
		global $admin_type;

		echo '<table id="all_users_list" class="usersTable">';
        
        echo '<thead><tr>';
        echo '<th>'.$lang['edit2'].'</th>';
        echo '<th>' . $lang['srv_survey_list_users_name'] . '</th>';
        echo '<th>' . $lang['srv_survey_list_users_email'] . '</th>';
        echo '<th>' . $lang['admin_type'] . '</th>';
        echo '<th>' . $lang['lang'] . '</th>';
        echo '<th>' . $lang['srv_survey_list_users_aai'] . '</th>';
        echo '<th>' . $lang['srv_survey_list_users_survey_count'] . '</th>';
        echo '<th>' . $lang['srv_survey_list_users_survey_archive_count'] . '</th>';
        echo '<th>' . $lang['srv_manager_count'] . '</th>';
        echo '<th>' . $lang['srv_manager_count_manager'] . '</th>';
        echo '<th>' . $lang['users_gdpr_title'] . '</th>';
        echo '<th>' . $lang['srv_survey_list_users_registred'] . '</th>';
        echo '<th>' . $lang['srv_survey_list_users_last_login'] . '</th>';
        echo '</tr></thead>';
        
        echo '</table>';
    }
    
    /**
	 * Osnovni pregled uporabnikov za managerje in admine
	 */
	public function assignedUsersList(){
		global $lang;
		global $admin_type;
		global $global_user_id;
        
        $sqlU = sisplet_query("SELECT name, surname, email FROM users WHERE id='".$global_user_id."'");
        $rowU = mysqli_fetch_array($sqlU);

		//echo "Ime: ".$rowU['name'];

		// PREGLED NAD UPORABNIKI - TABELA
        echo '<div class="my_users_list_holder">';

		// Naslov
        /*echo '<h2 style="margin-bottom:30px;">';

        if($admin_type == 0)
            echo $lang['administrator'];
        elseif($admin_type == 1)
            echo $lang['manager'];
        else
            echo $lang['user'];

        echo ': '.$rowU['name'].' '.$rowU['surname'].' ('.$rowU['email'].')';

        echo ' <a href="#" onclick="edit_user(\''.$global_user_id.'\'); return false;" title="Uredi"><i class="fa fa-pencil-alt link-moder"></i></a>';

        echo '</h2>'; */

		echo '<p class="bottom16 caps semi-bold">'.$lang['srv_users_assigned_title'].'</p>';

        // Tabela
        echo '<table id="my_users_list" class="usersTable">';

        echo '<thead><tr>';
        echo '<th>'.$lang['edit2'].'</th>';
        echo '<th>' . $lang['srv_survey_list_users_name'] . '</th>';
        echo '<th>' . $lang['srv_survey_list_users_email'] . '</th>';
        echo '<th>' . $lang['admin_type'] . '</th>';
        echo '<th>' . $lang['lang'] . '</th>';
        echo '<th>' . $lang['srv_survey_list_users_aai'] . '</th>';
        echo '<th>' . $lang['srv_survey_list_users_survey_count'] . '</th>';
        echo '<th>' . $lang['srv_survey_list_users_survey_archive_count'] . '</th>';
        echo '<th>' . $lang['users_gdpr_title'] . '</th>';
        echo '<th>' . $lang['srv_survey_list_users_registred'] . '</th>';
        echo '<th>' . $lang['srv_survey_list_users_last_login'] . '</th>';
        echo '</tr></thead>';

        echo '</table>';

        echo '</div>';
	}

	/**
	 * Seznam vseh izbrisanih uporabnikov znotraj 1ke
	 */
	public function deletedUsersList(){
	    global  $lang; 

	    	echo '<table id="deleted_users_list" class="usersTable">';
                echo '<thead><tr>';
                    echo '<th>'.$lang['srv_survey_list_users_name'].'</th>';
                    echo '<th>'.$lang['srv_survey_list_users_email'].'</th>';
                    echo '<th>'.$lang['admin_type'].'</th>';
			        echo '<th>'.$lang['lang'].'</th>';
                    echo '<th>'.$lang['registered'].'</th>';
                echo '</tr></thead>';
			echo '</table>';

    }

	/**
	 * Seznam vseh odjavljenih uporabnikov
     * V bazi vsi uporabniki, ki so odjavljeni samo pridobijo status 0
	 */
    public function unsignedUsersList(){
	    global  $lang;

	    echo '<table id="unsigned_users_list" class="usersTable">';
	    echo '<thead><tr>';
	    echo '<th>'.$lang['srv_survey_list_users_name'].'</th>';
	    echo '<th>'.$lang['srv_survey_list_users_email'].'</th>';
	    echo '<th>'.$lang['admin_type'].'</th>';
	    echo '<th>'.$lang['lang'].'</th>';
	    echo '<th>'.$lang['registered'].'</th>';
	    echo '</tr></thead>';
	    echo '</table>';
    }

	/**
	 * Seznam vseh uporabnikov, ki so prejeli email in ga niso potrdili
	 */
    public function unconfirmedMailUsersList(){
	    global  $lang;

	    echo '<table id="unconfirmed_mail_user_list" class="dataTable">';
	    echo '<thead><tr>';
	    echo '<th>'.$lang['edit2'].'</th>';
	    echo '<th>'.$lang['srv_survey_list_users_name'].'</th>';
	    echo '<th>'.$lang['srv_survey_list_users_email'].'</th>';
	    echo '<th>'.$lang['admin_type'].'</th>';
	    echo '<th>'.$lang['lang'].'</th>';
	    echo '<th>'.$lang['registered'].'</th>';
	    echo '</tr></thead>';
	    echo '</table>';
    }

	/**
	 * Seznam uporabnikov, ki imajo dostop do SA modula
	 */
	public function SAuserListIndex(){
		global $lang, $global_user_id, $admin_type;

		if($admin_type > 0)
			return false;

		$sql_uporabniki = sisplet_query("SELECT id, u.name, u.surname, u.email, d.ustanova, d.aai_email, DATE_FORMAT(d.created_at, '%d.%m.%Y - %H:%i') AS created_at, d.updated_at FROM srv_hierarhija_dostop AS d LEFT JOIN users AS u ON u.id=d.user_id ORDER BY u.name", "obj");

		echo '<a href="#" onclick="dodeliSAdostopUporabniku()">Dodaj uporabniku SA dostop</a><br /><br />';

		if(empty($sql_uporabniki)){
			echo $lang['srv_hierarchy_users_access_no_data'];
			return false;
		}


		if(!empty($sql_uporabniki->name)) {
					$uporabniki[0] = $sql_uporabniki;
        }else{
		    $uporabniki = $sql_uporabniki;
        }

		echo '<table class="datatables" id="sa-users-table">';
		echo '<tr>';
		echo '<th class="text-left">'.$lang['srv_hierarchy_users_name'].'</th>';
		echo '<th>'.$lang['srv_hierarchy_users_email'].'</th>';
		echo '<th>'.$lang['srv_hierarchy_users_organization'].'</th>';
		echo '<th class="text-right">'.$lang['srv_hierarchy_users_created'].'</th>';
		echo '<th></th>';
		echo '</tr>';


		foreach($uporabniki as $uporabnik) {
			echo '<tr>';
			echo '<td class="text-left">'.$uporabnik->name .' '. $uporabnik->surname.'</td>';
			echo '<td>'.$uporabnik->email.'</td>';
			echo '<td>'.$uporabnik->ustanova .'</td>';
			echo '<td class="text-right">'.$uporabnik->created_at.'</td>';
            echo '<td class="akcija">';
                echo '<div>';
                    echo '<a href="#" onclick="preveriSAuporabnika(\''.$uporabnik->id.'\')">'.$lang['srv_dataIcons_quick_view'].'</a>';
                    echo '<a href="#"  onclick="urediSAuporabnika(\''.$uporabnik->id.'\')">'.$lang['srv_recode_edit'].'</a>';
                    echo '<a href="#"  onclick="izbrisiSAuporabnika(\''.$uporabnik->id.'\')">'.$lang['srv_recode_remove'].'</a>';
                echo '</div>';
			echo '</td>';
			echo '</tr>';
		}
		echo '</table>';

	}

	// Prikaz naprednih modulov - NOVO (v urejanje->nastavitve)
	function showAdvancedModules(){
		global $lang, $site_url, $global_user_id, $admin_type;

		# preberemo osnovne nastavitve 
		$row = SurveyInfo::getInstance()->getSurveyRow();
		
		# preberemo vklopljene module
		$modules = SurveyInfo::getSurveyModules();
		
		$disabled = '';
		$css_disabled = '';
		if (isset($modules['slideshow'])){
			$disabled = ' disabled="disabled"';
			$css_disabled = ' gray';
		}

		if ($_GET['a'] == 'uporabnost'){
		
			echo '<fieldset><legend>'.$lang['srv_uporabnost'].Help::display('srv_evalvacija_strani').'</legend>';

			echo '<p class="bottom16">'.$lang['srv_vrsta_survey_note_4_3'].'</p>';

			echo '<div class="setting_holder">';
			echo '<input type="checkbox" id="advanced_module_uporabnost" name="uporabnost" value="1"'. (isset($modules['uporabnost']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'uporabnost\');" />';
			echo '<label for="advanced_module_uporabnost">'.$lang['srv_vrsta_survey_note_4_3a'] . '</label>';
			echo '</div>';

			echo '</fieldset>';
			
			echo '<div id="globalSettingsInner">';
			if(isset($modules['uporabnost'])){
				$this->uporabnost();
			}
			echo '</div>';
		}
		elseif (($_GET['a'] == A_HIERARHIJA_SUPERADMIN) && Hierarhija\HierarhijaHelper::preveriDostop($this->anketa)){

			// Blok za vklop in izklop hierarhije skrijemo, če je hierarhija aktivna
			if(!SurveyInfo::getInstance()->checkSurveyModule('hierarhija')) {
				echo '<fieldset><legend>' . $lang['srv_hierarchy'] . '</legend>';
				echo '<i class="' . $css_disabled . '">' . $lang['srv_hierarchy_description'] . '</i>';
				echo '<label class="strong' . $css_disabled . '"><input type="checkbox" id="advanced_module_hierarhija" name="hierarhija" value="1"' . (isset($modules['hierarhija']) ? ' checked="checked"' : '') . $disabled . ' onChange="preveriAnketoZaHierarhijo('.$this->anketa.');" />';
				echo $lang['srv_hierarchy_on'] . '</label>';

				echo '<div id="hierarhija-opcije-vklopa">';
				    echo '<h4>'.$lang['srv_hierarchy_intro_select_title'].':</h4>';
				    echo '<div id="error"></div>';
                    echo '<label class="strong' . $css_disabled . '"><input type="radio" id="obstojeca-anketa" name="izberi-anketo" value="obstojeca" checked="checked"/>'.$lang['srv_hierarchy_intro_option_current'].'</label>';
                    echo '<label class="strong' . $css_disabled . '"><input type="radio" id="prevzeta-anketa" name="izberi-anketo" onclick="pridobiKnjiznicoZaHierarhijo(\'privzeta\')" value="prevzeta" />'.$lang['srv_hierarchy_intro_option_default'].' <span id="hierarhija-prevzeta"></span></label>';
                    echo '<label class="strong' . $css_disabled . '"><input type="radio" name="izberi-anketo" value="knjiznica" onclick="pridobiKnjiznicoZaHierarhijo(\'vse\')"/>'.$lang['srv_hierarchy_intro_option_library'].'</label>';
                    echo '<div id="hierarhija-knjiznica">';
                    echo '</div>';
                    echo '<span class="floatLeft spaceRight" style="padding:15px 0;"><div class="buttonwrapper">';
                        echo '<a class="ovalbutton ovalbutton_orange" href="#" onclick="potrdiIzbiroAnkete(); return false;" style="padding-right: 5px;>
                                <span style="color:#fff;">Vklopi modul</span>
                              </a>';
                    echo '</div></span>';
                echo '</div>';
				echo '</fieldset>';
				echo '<div id="globalSettingsInner" style="padding-top: 15px;">';
				echo '</div>';
			}else {

				$hierarhija = new \Hierarhija\Hierarhija($this->anketa);


				echo '<div id="hierarhija-container">';
                    echo '<div style="width:586px;">';
                        $hierarhija->displayHierarhijaNavigationSuperAdmin();
                    echo '</div>';

                    echo '<div id="globalSettingsInner" style="padding-top: 15px;">';

                    if($_GET['m'] == M_ADMIN_UREDI_SIFRANTE){

                        $hierarhija->hierarhijaSuperadminSifranti();

                    }elseif($_GET['m'] == M_ADMIN_UVOZ_SIFRANTOV){

                        $hierarhija->hierarhijaSuperadminUvoz();

                    }elseif($_GET['m'] == M_ADMIN_UPLOAD_LOGO){

                        $hierarhija->hierarhijaSuperadminUploadLogo();

                    }elseif($_GET['m'] == M_ADMIN_IZVOZ_SIFRANTOV){

                        $hierarhija->izvozSifrantov();

                    }elseif($_GET['m'] == M_ANALIZE){

                        if($_GET['r'] == 'custom'){
                            $HC = new \Hierarhija\HierarhijaPorocilaClass($this->anketa);
                            $HC->izvoz();
                        }else {
                            $HA = new HierarhijaAnalysis($this->anketa);
                            $HA->Display();
                        }

                    }elseif($_GET['m'] == M_HIERARHIJA_STATUS){

                        if($_GET['izvoz'] == 'status'){
                          // Izvoz tabele status
                          \Hierarhija\HierarhijaIzvoz::getInstance($this->anketa)->csvIzvozStatusa();
                        }else {
                          $hierarhija->statistikaHierjearhije();
                        }

                    }elseif($_GET['m'] == M_ADMIN_AKTIVACIJA){

                        $hierarhija->aktivacijaHierarhijeInAnkete();

                    }elseif($_GET['m'] == M_ADMIN_KOPIRANJE){

                        $hierarhija->kopiranjeHierarhijeInAnkete();

                    }elseif($_GET['m'] ==  M_UREDI_UPORABNIKE && $_GET['izvoz'] == 1) {
                        // za vse ostalo je ure uredi uporabnike - M_UREDI_UPORABNIKE
                        \Hierarhija\HierarhijaIzvoz::getInstance($this->anketa)->csvIzvozVsehUporabnikov();
                    }elseif($_GET['m'] ==  M_UREDI_UPORABNIKE && $_GET['izvoz'] == 'struktura-analiz') {
                        // za vse ostalo je uredi uporabnike - M_UREDI_UPORABNIKE
                        if(!empty($_GET['n']) && $_GET['n'] == 1){
                            \Hierarhija\HierarhijaIzvoz::getInstance($this->anketa)->csvIzvozStruktureZaObdelavo(false, true);
                        }else {
                            \Hierarhija\HierarhijaIzvoz::getInstance($this->anketa)->csvIzvozStruktureZaObdelavo();
                        }
                    }else{
                        $hierarhija->izberiDodajanjeUporabnikovNaHierarhijo();
                    }


                    echo '</div>';
				echo '</div>';
			}
		}
		elseif ($_GET['a'] == 'kviz'){
			
			echo '<fieldset><legend>'.$lang['srv_kviz'].'</legend>';

			echo '<p class="bottom16">'.$lang['srv_vrsta_survey_note_6_4'].'</p>';
			echo '<div class="setting_holder">';
			echo '  <input type="checkbox" id="advanced_module_quiz" name="quiz" value="1" '. (isset($modules['quiz']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'quiz\');" />';
			echo '  <label for="advanced_module_quiz">'.$lang['srv_vrsta_survey_note_6_4a'].'</label>';
			echo '</div>'; 
			
			echo '</fieldset>';
			
			echo '<div id="globalSettingsInner">';
			if(isset($modules['quiz'])){
				$sq = new SurveyQuiz($this->anketa);
				$sq->displaySettings();
			}
			echo '</div>';
		} 
        elseif ($_GET['a'] == 'voting'){
			
            // Ce so vabila ze vklopljena ne pustimo vklopa
            if(isset($modules['voting']) || (!isset($modules['voting']) && SurveyInfo::getInstance()->checkSurveyModule('email'))){
                $disabled = ' disabled="disabled"';
                $css_disabled = ' gray';
            }

			echo '<fieldset><legend>'.$lang['srv_voting'].'</legend>';
			
            echo '<p class="bottom16">'.$lang['srv_voting_info'].'</p>';
            echo '<p class="bottom16">'.$lang['srv_voting_info_link'].'</p>';
            echo '<p class="bottom16">'.$lang['srv_voting_info_warning'].'</p>';

            // Opozorilo, da so vabila ze vklopljena in zato modula ni mogoce vklopiti
            if(!isset($modules['voting']) && SurveyInfo::getInstance()->checkSurveyModule('email')){
                echo '<p class="bottom16"><i class="red bold">'.$lang['srv_voting_info_error'].'</i></p>';
            }

			echo '<div class="setting_holder '.$css_disabled.'">';
			echo '  <input type="checkbox" id="advanced_module_voting" name="voting" value="1" '. (isset($modules['voting']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'voting\');" />';
			echo '  <label for="advanced_module_voting">'.$lang['srv_voting_on'].'</label>';
			echo '</div>';     
			
            echo '</fieldset>';
					
			echo '<div id="globalSettingsInner">';
			if(isset($modules['voting'])){
				$sv = new SurveyVoting($this->anketa);
				$sv->displaySettings();
			}
			echo '</div>';
		}
		elseif ($_GET['a'] == 'advanced_paradata'){
			
			echo '<fieldset><legend>'.$lang['srv_advanced_paradata'].'</legend>';

            echo '<div class="setting_holder '.$css_disabled.'">';
			echo '  <input type="checkbox" id="advanced_module_advanced_paradata" name="advanced_paradata" value="1" '. (isset($modules['advanced_paradata']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'advanced_paradata\');" />';
			echo '  <label for="advanced_module_advanced_paradata">'.$lang['srv_vrsta_survey_type_16_on'].'</label>';
			echo '</div>';  

            echo '</fieldset>';
					
			echo '<div id="globalSettingsInner">';
			if(isset($modules['advanced_paradata'])){
				$sap = new SurveyAdvancedParadata($this->anketa);
				$sap->displaySettings();
			}
			echo '</div>';
		}
		elseif ($_GET['a'] == 'advanced_timestamps'){
			
			echo '<fieldset><legend>'.$lang['srv_advanced_timestamps'].'</legend>';

            echo '<div class="setting_holder '.$css_disabled.'">';
			echo '  <input type="checkbox" id="advanced_module_advanced_timestamps" name="advanced_timestamps" value="1" '. (isset($modules['advanced_timestamps']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'advanced_timestamps\');" />';
			echo '  <label for="advanced_module_advanced_timestamps">'.$lang['srv_vrsta_survey_type_20_on'].'</label>';
			echo '</div>';  

            echo '</fieldset>';
					
			echo '<div id="globalSettingsInner">';
			if(isset($modules['advanced_timestamps'])){
				$sat = new SurveyAdvancedTimestamps($this->anketa);
				$sat->displaySettings();
			}
			echo '</div>';
		}
		elseif ($_GET['a'] == 'json_survey_export'){
			
			echo '<fieldset><legend>'.$lang['srv_json_survey_export'].'</legend>';
			echo '<label class="strong'.$css_disabled.'"><input type="checkbox" id="advanced_module_srv_json_survey_export" name="srv_json_survey_export" value="1" '. (isset($modules['srv_json_survey_export']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'srv_json_survey_export\');" />';
			echo $lang['srv_vrsta_survey_type_17'] . '</label>';			
			echo '</fieldset>';
			
			echo '<br />';
		
			echo '<div id="globalSettingsInner">';
			
			if(isset($modules['srv_json_survey_export'])){				
				$sjs = new SurveyJsonSurveyData($this->anketa);
				$sjs->displaySettings();
			}
			
			echo '</div>';
		} 		
		elseif ($_GET['a'] == 'slideshow'){
		
			echo '<fieldset><legend>'.$lang['srv_slideshow_fieldset_label'].'</legend>';
			echo '<label class="strong"><input type="checkbox" id="advanced_module_slideshow" name="slideshow" value="1" '. (isset($modules['slideshow']) ? ' checked="checked"' : '').' onChange="toggleAdvancedModule(\'slideshow\');" />';
			echo $lang['srv_vrsta_survey_type_9'] . '</label>';
			echo '<br/><i>'.$lang['srv_vrsta_survey_note_9_2'].'</i>';
			echo '</fieldset>';
			
			echo '<br />';
			
			echo '<div id="globalSettingsInner">';
			if(isset($modules['slideshow'])){
				$ss = new SurveySlideshow($this->anketa);
				$ss->ShowSlideshowSetings();
			}
			echo '</div>';
		} 
		elseif ($_GET['a'] == 'vnos') {
			
			echo '<fieldset><legend>'.$lang['srv_vnos'].'</legend>';
			echo '<label class="strong'.$css_disabled.'"><input type="checkbox" id="advanced_module_user_from_cms" name="user_from_cms" value="2" '. (($row['user_from_cms'] == 2 && $row['cookie'] == -1) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'user_from_cms\');" />';
			echo $lang['srv_vrsta_survey_type_5'] . '</label>';
			echo '<br/><i class="'.$css_disabled.'">'.$lang['srv_vrsta_survey_note_5_3'].'</i>';
			echo '</fieldset>';
			
			echo '<br />';
		
			echo '<div id="globalSettingsInner">';
			if($row['user_from_cms'] == 2 && $row['cookie'] == -1){
				$this->vnos();
			}
			echo '</div>';
		} 
		elseif ($_GET['a'] == A_TELEPHONE){

			echo '<div class="phone_settings_top" id="globalSettingsInner">';
			if(isset($modules['phone'])){
				$ST = new SurveyTelephone($this->anketa);
				$ST->action($_GET['m']);
			}
			echo '</div>';

			$hide_tel_fieldset = '';
			if ($_GET['m'] == 'recipients_lists') {
				$hide_tel_fieldset = 'displayNone';
			}
		
			// Ce je anketar ne vidi teh nastavitev
			$isAnketar = Common::isUserAnketar($this->anketa, $global_user_id);
			if(!$isAnketar){
				
				if(isset($modules['phone'])){
					$sqlT = sisplet_query("SELECT count(*) AS cnt FROM srv_invitations_recipients WHERE ank_id='$this->anketa' AND deleted='0' AND phone!=''");
					$rowT = mysqli_fetch_array($sqlT);
					
					// Ce se nimamo nobene stevilke v bazi, pustimo da se lahko ugasne
					if($rowT['cnt'] == 0){
						echo '<fieldset class="'.$hide_tel_fieldset.'"><legend>'.$lang['srv_vrsta_survey_type_7'].' '.Help::display('srv_telephone_help').'</legend>';

						echo '<div class="setting_holder">';
						echo '<input type="checkbox" id="advanced_module_phone" name="phone" value="1" '. (isset($modules['phone']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'phone\');" />';
						echo '<label for="advanced_module_phone" class="'.$css_disabled.'">'.$lang['srv_vrsta_survey_type_7'] . '</label>';
						echo '</div>';

						echo '<p class="top16 '.$css_disabled.'">'.$lang['srv_vrsta_survey_note_7_3'].'</p>';
						echo '</fieldset>';
					}
					/*else{
						echo '<span class="blue" style="font-size:14px; font-weight:600;">'.$lang['srv_vrsta_survey_type_7'].'</span> '.Help::display('srv_telephone_help');
						echo '<br />';
					}*/
				}
				else{					
					echo '<fieldset class="'.$hide_tel_fieldset.'"><legend>'.$lang['srv_vrsta_survey_type_7'].' '.Help::display('srv_telephone_help').'</legend>';
					echo '<div class="setting_holder">';
					echo '<input type="checkbox" id="advanced_module_phone" name="phone" value="1" '. (isset($modules['phone']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'phone\');" />';
					echo '<label for="advanced_module_phone" class="'.$css_disabled.'">'.$lang['srv_vrsta_survey_type_7'] . '</label>';
					echo '</div>';

					echo '<p class="top16 '.$css_disabled.'">'.$lang['srv_vrsta_survey_note_7_3'].'</p>';
					echo '</fieldset>';
				}
			}
		}
		elseif ($_GET['a'] == A_CHAT){
			global $site_path;
			
			echo '<fieldset><legend>'.$lang['srv_vrsta_survey_type_14'].'</legend>';

			echo '<p class="bottom16">'.$lang['srv_vrsta_survey_note_14_1'].'</p>';
			echo '<div class="setting_holder">';
			echo '<input type="checkbox" id="advanced_module_chat" name="chat" value="1" '. (isset($modules['chat']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'chat\');" />';
			echo '<label for="advanced_module_chat">'.$lang['srv_vrsta_survey_note_14_1b'].'</label>';
			echo '</div>';

			echo '</fieldset>';
			
			echo '<div id="globalSettingsInner">';
			if(isset($modules['chat'])){
				$sc = new SurveyChat($this->anketa);
				$sc->displaySettings();
			}
			echo '</div>';
		}
		elseif ($_GET['a'] == A_PANEL){
			global $site_path;
			
			echo '<fieldset><legend>'.$lang['srv_vrsta_survey_type_15'].'</legend>';
			echo '<p>'.$lang['srv_vrsta_survey_note_15_1'].'</p>';
			echo '<p class="top16 bottom16">'.$lang['srv_vrsta_survey_note_15_1a'].'</p>';
			echo '<div class="setting_holder">';
			echo '<input type="checkbox" id="advanced_module_panel" name="panel" value="1" '. (isset($modules['panel']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'panel\');" />';
			echo '<label for="advanced_module_panel">'.$lang['srv_vrsta_survey_note_15_1b'].'</label>';
			echo '</div>'; 

			echo '</fieldset>';
			
			echo '<div id="globalSettingsInner">';
			if(isset($modules['panel'])){
				$sp = new SurveyPanel($this->anketa);
				$sp->displaySettings();
			}
			echo '</div>';
		}
		elseif ($_GET['a'] == A_EMAIL_ACCESS){
			global $site_path;
			
			echo '<fieldset><legend>'.$lang['srv_vrsta_survey_type_19'].'</legend>';
			echo '<p class="bottom16">'.$lang['srv_vrsta_survey_note_19_1'].'</p>';

			echo '<div class="setting_holder">';
			echo '<input type="checkbox" id="advanced_module_email_access" name="email_access" value="1" '. (isset($modules['email_access']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'email_access\');" />';
			echo '<label for="advanced_module_email_access">'.$lang['srv_vrsta_survey_note_19_2'].'</label>';
			echo '</div>'; 

			echo '</fieldset>';
			
			echo '<div id="globalSettingsInner">';
			if(isset($modules['email_access'])){
				$sea = new SurveyEmailAccess($this->anketa);
				$sea->displaySettings();
			}
			echo '</div>';
		}
		elseif ($_GET['a'] == A_FIELDWORK){
			global $site_path;
			
			// tole bom dopolnil po potrebi
			// 
			// Ce je anketar ne vidi teh nastavitev
			$isAnketar = Common::isUserAnketar($this->anketa, $global_user_id);
			if(!$isAnketar){
				// tole bom dopo
			}

			echo '<div id="globalSettingsInner">';

			$ST = new SurveyFieldwork($this->anketa);
			$ST->action($_GET['m']);
                        
			echo '</div>';
		}
		elseif ($_GET['a'] == A_MAZA){
			global $site_path;
                        
                        echo '<fieldset><legend>'.$lang['srv_maza'].'</legend>';
			echo '<label class="strong'.$css_disabled.'"><input type="checkbox" id="advanced_module_maza" name="maza" value="1" '. (isset($modules['maza']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'maza\');maza_on_off();" />';
			echo $lang['srv_maza'] . '</label>';
			echo '<br/><i class="'.$css_disabled.'">'.$lang['srv_maza_note'].'</i>';
			echo '</fieldset>';
			
			echo '<br />';

			echo '<div id="globalSettingsInner">';

			if(isset($modules['maza'])){
				$MS = new MAZA($this->anketa);
				$MS ->display();
			}
                        
			echo '</div>';
		}
		elseif ($_GET['a'] == A_WPN){
			global $site_path;
                        
                        echo '<fieldset><legend>'.$lang['srv_wpn'].'</legend>';
			echo '<label class="strong'.$css_disabled.'"><input type="checkbox" id="advanced_module_wpn" name="wpn" value="1" '. (isset($modules['wpn']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'wpn\');" />';
			echo $lang['srv_wpn'] . '</label>';
			echo '<br/><i class="'.$css_disabled.'">'.$lang['srv_wpn_note'].'</i>';
			echo '</fieldset>';
			
			echo '<br />';

			echo '<div id="globalSettingsInner">';

			if(isset($modules['wpn'])){
				$MS = new WPN($this->anketa);
				$MS ->display();
			}
                        
			echo '</div>';
		}
		elseif ($_GET['a'] == 'social_network'){
			if ($_GET['m'] == 'respondenti' || $_GET['m'] == "") {
				
				echo '<fieldset><legend>'.$lang['srv_vrsta_survey_type_8'].'</legend>';

				echo '<p class="bottom16">'.$lang['srv_vrsta_survey_note_8_3'].'</p>';
				echo '<div class="setting_holder bottom0">';
				echo '<input type="checkbox" id="advanced_module_social_network" name="social_network" value="1" '. (isset($modules['social_network']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'social_network\');" />';
				echo '<label for="advanced_module_social_network">'.$lang['srv_vrsta_survey_note_8_3a'].'</label>';
				echo '</div>';
				
				echo '<div id="globalSettingsInner">';
				if(isset($modules['social_network'])){
					$this->SN_Settings();
				}
				echo '</div>';

				echo '</fieldset>';
			}
		}
		elseif ($_GET['a'] == A_360){
				
			echo '<fieldset><legend>'.$lang['srv_vrsta_survey_type_11'].'</legend>';
			echo '<label class="strong'.$css_disabled.'"><input type="checkbox" id="advanced_module_360_stopinj" name="360_stopinj" value="1" '. (isset($modules['360_stopinj']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'360_stopinj\');" />';
			echo $lang['srv_vrsta_survey_type_11'] . '</label>';
			echo '<br/><i class="'.$css_disabled.'">'.$lang['srv_vrsta_survey_note_11_1'].'</i>';
			echo '</fieldset>';
		
			echo '<br />';
				
			echo '<div id="globalSettingsInner">';
			// urejanje respondentov
			if(isset($modules['360_stopinj'])){
				$S360 = new Survey360($this->anketa);
				$S360->displaySettings();
			}
			echo '</div>';
		}
		elseif ($_GET['a'] == A_360_1KA){
				
			echo '<fieldset><legend>'.$lang['srv_vrsta_survey_type_12'].'</legend>';
			echo '<label class="strong'.$css_disabled.'"><input type="checkbox" id="advanced_module_360_stopinj_1ka" name="360_stopinj_1ka" value="1" '. (isset($modules['360_stopinj_1ka']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'360_stopinj_1ka\');" />';
			echo $lang['srv_vrsta_survey_type_12'] . '</label>';
			echo '<br/><i class="'.$css_disabled.'">'.$lang['srv_vrsta_survey_note_12_1'].'</i>';
			echo '</fieldset>';
		
			echo '<br />';
				
			echo '<div id="globalSettingsInner">';
			// urejanje respondentov
			if(isset($modules['360_stopinj_1ka'])){
				$S360 = new Survey3601ka($this->anketa);
				$S360->displaySettings();
			}
			echo '</div>';
		}
		elseif ($_GET['a'] == 'evoli'){
				
			echo '<fieldset><legend>Evoli</legend>';

			echo '<p class="bottom16">Napredna poročila Evoli</p>';
			echo '<div class="setting_holder bottom0">';
			echo '<input type="checkbox" id="advanced_module_evoli" name="evoli" value="1" '. (isset($modules['evoli']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'evoli\');" />';
			echo '<label for="advanced_module_evoli">Evoli</label>';
			echo '</div>';

			echo '</fieldset>';

			echo '<div id="globalSettingsInner"></div>';
		}
		elseif ($_GET['a'] == 'evoli_teammeter'){
				
			echo '<fieldset><legend>Evoli team meter</legend>';

			echo '<p class="bottom16">Napredna poročila Evoli team meter</p>';
			echo '<div class="setting_holder bottom0">';
			echo '<input type="checkbox" id="advanced_module_evoli_teammeter" name="evoli_teammeter" value="1" '. (isset($modules['evoli_teammeter']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'evoli_teammeter\');" />';
			echo '<label for="advanced_module_evoli_teammeter">Evoli team meter</label>';
			echo '</div>';

			echo '</fieldset>';
			
			echo '<div id="globalSettingsInner">';
			// urejanje respondentov
			if(isset($modules['evoli_teammeter'])){
				$evoliTM = new SurveyTeamMeter($this->anketa);
				$evoliTM->displaySettings();
			}
			echo '</div>';
        }
        elseif ($_GET['a'] == 'evoli_quality_climate'){
				
			echo '<fieldset><legend>Evoli quality climate</legend>';

			echo '<p class="bottom16">Napredna poročila Evoli quality climate</p>';
			echo '<div class="setting_holder bottom0">';
			echo '<input type="checkbox" id="advanced_module_evoli_quality_climate" name="evoli_quality_climate" value="1" '. (isset($modules['evoli_quality_climate']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'evoli_quality_climate\');" />';
			echo '<label for="advanced_module_evoli_quality_climate">Evoli quality climate</label>';
			echo '</div>';

			echo '</fieldset>';
			
			echo '<div id="globalSettingsInner">';
			// urejanje respondentov
			if(isset($modules['evoli_quality_climate'])){
				$evoliTM = new SurveyTeamMeter($this->anketa);
				$evoliTM->displaySettings();
			}
			echo '</div>';
        }
        elseif ($_GET['a'] == 'evoli_teamship_meter'){
				
			echo '<fieldset><legend>Evoli teamship meter</legend>';

			echo '<p class="bottom16">Napredna poročila Evoli teamship meter</p>';
			echo '<div class="setting_holder bottom0">';
			echo '<input type="checkbox" id="advanced_module_evoli_teamship_meter" name="evoli_evoli_teamship_meter" value="1" '. (isset($modules['evoli_teamship_meter']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'evoli_teamship_meter\');" />';
			echo '<label for="advanced_module_evoli_teamship_meter">Evoli teamship meter</label>';
			echo '</div>';

			echo '</fieldset>';
			
			echo '<div id="globalSettingsInner">';
			// urejanje respondentov
			if(isset($modules['evoli_teamship_meter'])){
				$evoliTM = new SurveyTeamMeter($this->anketa);
				$evoliTM->displaySettings();
			}
			echo '</div>';
        }
        elseif ($_GET['a'] == 'evoli_organizational_employeeship_meter'){
				
			echo '<fieldset><legend>Evoli organizational employeeship meter</legend>';

			echo '<p class="bottom16">Napredna poročila Evoli organizational employeeship meter</p>';
			echo '<div class="setting_holder bottom0">';
			echo '<input type="checkbox" id="advanced_module_evoli_organizational_employeeship_meter" name="evoli_evoli_organizational_employeeship_meter" value="1" '. (isset($modules['evoli_organizational_employeeship_meter']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'evoli_organizational_employeeship_meter\');" />';
			echo '<label for="advanced_module_evoli_organizational_employeeship_meter">Evoli organizational employeeship meter</label>';
			echo '</div>';

			echo '</fieldset>';
						
			echo '<div id="globalSettingsInner">';
			// urejanje respondentov
			if(isset($modules['evoli_organizational_employeeship_meter'])){
				$evoliTM = new SurveyTeamMeter($this->anketa);
				$evoliTM->displaySettings();
			}
			echo '</div>';
        }
        elseif ($_GET['a'] == 'evoli_employmeter'){
				
			echo '<fieldset><legend>Evoli employ meter</legend>';

			echo '<p class="bottom16">Napredna poročila Evoli employeeship meter</p>';
			echo '<div class="setting_holder bottom0">';
			echo '<input type="checkbox" id="advanced_module_evoli_employmeter" name="evoli_evoli_employmeter" value="1" '. (isset($modules['evoli_employmeter']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'evoli_employmeter\');" />';
			echo '<label for="advanced_module_evoli_employmeter">Evoli employeeship meter</label>';
			echo '</div>';

			echo '</fieldset>';

			echo '<div id="globalSettingsInner"></div>';
		}
		elseif ($_GET['a'] == 'mfdps'){
				
			echo '<fieldset><legend>MFDPŠ</legend>';

			echo '<p class="bottom16">Napredni izvozi MFDPŠ</p>';
			echo '<div class="setting_holder bottom0">';
			echo '<input type="checkbox" id="advanced_module_mfdps" name="mfdps" value="1" '. (isset($modules['mfdps']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'mfdps\');" />';
			echo '<label for="advanced_module_mfdps">MFDPŠ</label>';
			echo '</div>';

			echo '</fieldset>';

			echo '<div id="globalSettingsInner"></div>';
		}
		elseif ($_GET['a'] == 'borza'){
				
			echo '<fieldset><legend>Borza</legend>';

			echo '<p class="bottom16">Napredni izvozi Borza</p>';
			echo '<div class="setting_holder bottom0">';
			echo '<input type="checkbox" id="advanced_module_borza" name="borza" value="1" '. (isset($modules['borza']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'borza\');" />';
			echo '<label for="advanced_module_borza">Borza</label>';
			echo '</div>';

			echo '</fieldset>';

			echo '<div id="globalSettingsInner"></div>';
		}
		elseif ($_GET['a'] == 'mju'){
				
			echo '<fieldset><legend>MJU</legend>';

			echo '<p class="bottom16">Napredni izvozi MJU</p>';
			echo '<div class="setting_holder bottom0">';
			echo '<input type="checkbox" id="advanced_module_mju" name="mju" value="1" '. (isset($modules['mju']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'mju\');" />';
			echo '<label for="advanced_module_mju">MJU</label>';
			echo '</div>';

			echo '</fieldset>';
						
			
			echo '<div id="globalSettingsInner">';
			if(isset($modules['mju'])){
				$sme = new SurveyMJUEnote($this->anketa);
				$sme->displaySettings();
			}
			echo '</div>';
		}
		elseif ($_GET['a'] == 'excell_matrix'){
				
			echo '<fieldset><legend>Excelleration matrix</legend>';

			echo '<p class="bottom16">'.$lang['srv_vrsta_survey_note_16_1'].'</p>';
			echo '<div class="setting_holder bottom0">';
			echo '<input type="checkbox" id="advanced_module_excell_matrix" name="excell_matrix" value="1" '. (isset($modules['excell_matrix']) ? ' checked="checked"' : '').$disabled.' onChange="toggleAdvancedModule(\'excell_matrix\');" />';
			echo '<label for="advanced_module_excell_matrix">Excelleration matrix</label>';
			echo '</div>';

			echo '</fieldset>';

			echo '<div id="globalSettingsInner"></div>';
		}
	}
	
	function formatNumber ($value, $digit = 0, $form=null) {
		# Kako izpisujemo decimalke in tisočice
		$default_seperators = array(	0=>array('decimal_point'=>'.', 'thousands'=>','),
										1=>array('decimal_point'=>',', 'thousands'=>'.'));		
	
		if (is_array($form) && isset($form['decimal_point'])&& isset($form['thousands'])) {
			$decimal_point = $form['decimal_point'];
			$thousands = $form['thousands'];
		} else {
			$decimal_point = $default_seperators['decimal_point'];
			$thousands = $default_seperators['thousands'];
		}
	
		if ($value <> 0 && $value != null)
			$result = round($value, $digit);
		else
			$result = "0";
			
		$result = number_format($result, $digit, $decimal_point, $thousands);
	
		return $result;
	}
	
	function globalUserSettings () {
		global $lang, $global_user_id, $admin_type, $site_url;

		$preklici_url = ltrim(str_replace("&s=1","",$_SERVER['REQUEST_URI']),"/");
		$preklici_url = "'". $site_url . $preklici_url . "'";
		
		# polovimo nastavitve uporabnika
		UserSetting::getInstance()->Init($global_user_id);
		
		# ali zaklepamo anketo ob aktivaciji
		$lockSurvey = UserSetting::getInstance()->getUserSetting('lockSurvey');
		# ali je anketa aktivna ob aktivaciji
		$autoActiveSurvey = UserSetting::getInstance()->getUserSetting('autoActiveSurvey');
		# ali imamo star napreden vmesnik za moje ankete
		$advancedMySurveys = UserSetting::getInstance()->getUserSetting('advancedMySurveys');
		# ali imamo gumb za enklik kreiranje ankete
		$oneclickCreateMySurveys = UserSetting::getInstance()->getUserSetting('oneclickCreateMySurveys');
		
		# ali so komentarji aktivirani ob kreaciji ankete
		$activeComments = UserSetting::getInstance()->getUserSetting('activeComments');
		
		# uvod ob aktivaciji
		$showIntro = UserSetting::getInstance()->getUserSetting('showIntro');
		# zakljucek ob aktivaciji
		$showConcl = UserSetting::getInstance()->getUserSetting('showConcl');
		# ime za respondente ob aktivaciji
		$showSurveyTitle = UserSetting::getInstance()->getUserSetting('showSurveyTitle');

		# Prikaži bližnico za jezik v statusni vrstici
        $showLanguageShortcut = UserSetting::getInstance()->getUserSetting('showLanguageShortcut');
		
		
		echo '<div id="anketa_edit">';
		
		echo '<form name="settingsanketa" action="ajax.php?a=editanketasettings&m=global_user_settings" method="post" autocomplete="off">' . "\n\r";
		//echo '          <input type="hidden" name="anketa" value="' . $this->anketa . '" />' . "\n\r";
		//echo '          <input type="hidden" name="grupa" value="' . $this->grupa . '" />' . "\n\r";
		echo '          <input type="hidden" name="location" value="' . $_GET['a'] . '" />' . "\n\r";
		echo '          <input type="hidden" name="submited" value="1" />' . "\n\r";
		
		echo '<fieldset><legend>'.$lang['srv_interface_settings'].'</legend>';
		
		// Jezik vmesnika
		$sql = sisplet_query("SELECT lang FROM users WHERE id = '$global_user_id'");
		$row = mysqli_fetch_array($sql);
		$lang_admin = $row['lang'];

		echo '<div class="setting_holder">';
		echo '<label for="language">'.$lang['lang'] . ':</label><select class="dropdown large" name="language" id="language">';
		echo '<option value="1"'.($lang_admin == 1?' selected':'').'>Slovenščina</option>';
		echo '<option value="2"'.($lang_admin == 2?' selected':'').'>English</option>';
		echo '</select>';
		echo '</div>';

		echo '<div class="setting_holder">';
		echo '<span class="setting_title">'.$lang['srv_settings_interfaceshow'] . ':</span>';

        // Prikaži ikono za jezik v navigacijski vrstici ankete, desno zgoraj - TE NASTAVITVE NE UPOSTEVAMO VEC
		/*echo '<div class="setting_item">';
		echo '<input name="showLanguageShortcut" type="hidden" value="0">';
        echo '<input id="showLanguageShortcut" name="showLanguageShortcut" type="checkbox" value="1" '.($showLanguageShortcut == 1?' checked="checked"':'').'>';
		echo '<label for="showLanguageShortcut">'.$lang['srv_settings_language_shortcut'].'</label>';
		echo '</div>';*/

		// Opcija enklik ustvarjanja ankete (v mojih anketah)
		echo '<div class="setting_item">';
		echo '<input name="oneclickCreateMySurveys" type="hidden" value="0">';
		echo '<input id="oneclickCreateMySurveys" name="oneclickCreateMySurveys" type="checkbox" value="1" '.($oneclickCreateMySurveys == 1?' checked="checked"':'').'>';
		echo '<label for="oneclickCreateMySurveys">'.$lang['srv_settings_oneClickCreate'].Help::display('srv_nastavitve_enklik').'</label>';
		echo '</div>';

		// Možnost prikaza SA ikone pri vseh anketah
        if($admin_type < 3) {
            $showSAicon = UserSetting::getInstance()->getUserSetting('showSAicon');

			echo '<div class="setting_item">';
			echo '<input name="showSAicon" type="hidden" value="0">';
            echo '<input id="showSAicon" name="showSAicon" type="checkbox" value="1" ' . ($showSAicon == 1 ? ' checked="checked"' : '') . '>';
            echo '<label for="showSAicon">' . $lang['srv_settings_showSAicon'] .'</label>';
			echo '</div>';
        }

		echo '</div>';
		
		echo '</fieldset>';
		
		
		echo '<fieldset><legend>'.$lang['srv_survey_settings'].'</legend>';

		echo '<div class="setting_holder">';
		echo '<span class="setting_title">'.$lang['srv_settings_oncreation'] . ':</span>';

		// Aktivna anketa ob aktivaciji - TO PUSTIMO SAMO ADMINOM ZARADI GDPR OPOZORILA OB AKTIVACIJI
		if($admin_type == '0'){
			echo '<div class="setting_item">';
			echo '<input name="autoActiveSurvey" type="hidden" value="0">';
			echo '<input id="autoActiveSurvey" name="autoActiveSurvey" type="checkbox" value="1" '.($autoActiveSurvey == 1?' checked="checked"':'').'>';
			echo '<label for="autoActiveSurvey">'.$lang['srv_settings_autoActiveSurvey'].'</label>';
			echo '</div>';
		}
		
		// Komentarji aktivirani ob kreaciji ankete
		echo '<div class="setting_item">';
		echo '<input name="activeComments" type="hidden" value="0">';
		echo '<input id="activeComments" name="activeComments" type="checkbox" value="1" '.($activeComments == 1?' checked="checked"':'').'>';
		echo '<label for="activeComments">'.$lang['srv_settings_activeComments'].'</label>';
		echo '</div>';

		// Uvod ob aktivaciji prikazan
		echo '<div class="setting_item">';
		echo '<input name="showIntro" type="hidden" value="0">';
		echo '<input id="showIntro" name="showIntro" type="checkbox" value="1" '.($showIntro == 1?' checked="checked"':'').'>';
		echo '<label for="showIntro">'.$lang['srv_create_show_intro'].'</label>';
		echo '</div>';

		// Zakljucek ob aktivaciji prikazan
		echo '<div class="setting_item">';
		echo '<input name="showConcl" type="hidden" value="0">';
		echo '<input id="showConcl" name="showConcl" type="checkbox" value="1" '.($showConcl == 1?' checked="checked"':'').'>';
		echo '<label for="showConcl">'.$lang['srv_create_show_concl'].'</label>';
		echo '</div>';

		// Ime ob aktivaciji prikazano za respondente
		echo '<div class="setting_item">';
		echo '<input name="showSurveyTitle" type="hidden" value="0">';
		echo '<input id="showSurveyTitle" name="showSurveyTitle" type="checkbox" value="1" '.($showSurveyTitle == 1?' checked="checked"':'').'>';
		echo '<label for="showSurveyTitle">'.$lang['srv_create_show_title'].'</label>';
		echo '</div>';

		echo '</div>';

		echo '<div class="setting_holder">';
		echo '<span class="setting_title">'.$lang['srv_settings_onactivation'] . ':</span>';

		// Zaklep urejanja ob aktivaciji
		echo '<div class="setting_item">';
		echo '<input name="lockSurvey" type="hidden" value="0">';
        echo '<input name="lockSurvey" id="lockSurvey" type="checkbox" value="1" '.($lockSurvey == 1?' checked="checked"':'').'>';
        echo '<label for="lockSurvey">'.$lang['srv_lock_survey_when_activate'].Help::display('srv_nastavitve_zakleni').'</label>';
		echo '</div>';

		echo '</div>';

		echo '</fieldset>';
		
		echo '</form>';

		// save gumb
		echo '<div class="button_holder">';
		echo '<button class="medium white-blue" onClick="window.location='.$preklici_url.';return false;">'.$lang['edit1338'].'</button>';				
		echo '<button class="medium blue" onclick="document.settingsanketa.submit();">'.$lang['edit1337'].'</button>';
		echo '</div>';
		
		// div za prikaz uspešnosti shranjevanja
		if (isset($_GET['s']) && $_GET['s'] == '1') {
			echo '<div id="success_save"></div>';
			echo '<script type="text/javascript">$(document).ready(function() {actionNotePopup(\'success_save\', \'success\');});</script>';
		}

		
		// API avtentikacija
		echo '<fieldset><legend>'.$lang['srv_api'].Help::display('srv_nastavitve_API').'</legend>';

		echo '<p class="bottom16">'.$lang['srv_api_auth'].':</p>';

		echo '<div class="button_holder bottom16">';
		echo '<button type="button" class="medium white-black" onClick="generate_API_key(); return false;">'.$lang['srv_api_auth2'].'</button>';
		echo '</div>';

        echo $lang['additional_info_api'];
		
		echo '</fieldset>';


		// Izvoz vseh anket v paket
		echo '<fieldset class="top32"><legend>'.$lang['srv_archive_surveys'].Help::display('archive_surveys').'</legend>';

		echo '<p class="bottom16">'.$lang['srv_archive_surveys_text1'].'</p>';
		echo '<p class="bottom16">'.$lang['srv_archive_surveys_text2'].'</p>';
		echo '<p class="bottom32 italic">'.$lang['srv_archive_surveys_text3'].'</p>';

		echo '<div class="button_holder bottom0">';
		echo '	<button type="button" class="medium white-black" onClick="archive_surveys_display_export(); return false;">'.$lang['srv_archive_surveys_export'].'</button>';
		echo '	<button type="button" class="medium white-black" onClick="archive_surveys_display_import(); return false;">'.$lang['srv_archive_surveys_import'].'</button>';
		echo '</div>';
		
		echo '</fieldset>';
		
				
		echo '</div>';
	}
	
	function globalUserMyProfile () {
		global $lang, $global_user_id, $admin_type, $site_domain, $site_url;

		$preklici_url = ltrim(str_replace("&s=1","",$_SERVER['REQUEST_URI']),"/");
		$preklici_url = "'". $site_url . $preklici_url . "'";
		
		// podatki prijavljenega uporabnika
		$sql = sisplet_query("SELECT id, name, surname, email, type, gdpr_agree, last_login FROM users WHERE id = '$global_user_id'");
		$row = mysqli_fetch_array($sql);
		
		echo '<div id="anketa_edit">';
		
		echo '	<form name="settingsanketa" id="form_profile_user_settings" action="ajax.php?a=editanketasettings&m=global_user_myProfile" method="post" autocomplete="off">' . "\n\r";
		echo '		<input type="hidden" name="location" value="' . $_GET['a'] . '" />' . "\n\r";
		echo '		<input type="hidden" name="submited" value="1" />' . "\n\r";
		
		echo '<fieldset><legend>'.$lang['edit_data'].'</legend>';

		//Prijavljeni kot, status, zadnja prijava, aktivni paket
		echo '<div id="user_info_segment" class="bottom0">';

		if($row['type'] == '0')
			$type = $lang['admin_admin'];
		elseif($row['type'] == '1')
			$type = $lang['admin_manager'];
		else
			$type = $lang['admin_narocnik'];

		echo '<div class="user_info text">';
		echo '<div class="setting_holder">'.$lang['logged_in_as']. ':</div>';
		echo '<div class="setting_holder">'.$lang['your_status']. ':</div>';
		echo '<div class="setting_holder">'.$lang['srv_last_login'].':</div>';
		if(AppSettings::getInstance()->getSetting('app_settings-commercial_packages') === true)
			echo '<div class="setting_holder">'.$lang['srv_access_package'].':</div>';
		echo '</div>';
		
		echo '<div class="user_info data bold">';
		echo '<div class="setting_holder">'.$row['name'].' '.$row['surname'].'</div>';
		echo '<div class="setting_holder">'.$type.'</div>';
		echo '<div class="setting_holder">'.date('j.n.Y', strtotime($row['last_login'])).' '.$lang['ob'].' '.date('H:i', strtotime($row['last_login'])).'</div>';
		
		// Trenutni paket funkcionalnosti
        if(AppSettings::getInstance()->getSetting('app_settings-commercial_packages') === true){

            $sqlA = sisplet_query("SELECT ua.time_activate, ua.time_expire, uap.id AS package_id, uap.name AS package_name
                                    FROM user_access ua, user_access_paket uap 
                                    WHERE ua.usr_id='$global_user_id' AND uap.id=ua.package_id
                                ");

            $drupal_url = ($lang['id'] == '2') ? $site_url.'d/en/' : $site_url.'d/';

			

            // Ni nobenega paketa
			if(mysqli_num_rows($sqlA) == 0){
                $package_string = $lang['paket_opis_1ka'].' ('.$lang['srv_access_package_free'].')</div><div class="setting_holder"><button class="medium yellow" type="button" onClick="window.location.href=\''.$drupal_url.''.$lang['srv_narocila_buyurl'].'\';">'.$lang['srv_narocila_buy'].'</button>';
            }
            else{
                $rowA = mysqli_fetch_array($sqlA);
				$userAccess = UserAccess::getInstance($global_user_id);
				$package_name = $userAccess->getPackageName($rowA['package_id']);

                // Ce ima paket 2 ali 3
				if($rowA['package_id'] == '2' || $rowA['package_id'] == '3'){

                    // Ce je paket ze potekel
					if(strtotime($rowA['time_expire']) < time()){

                        $package_string = '<span>';
                        $package_string .= $package_name;
                        $package_string .= ' ('.$lang['srv_access_package_expire'].' '.date("d.m.Y", strtotime($rowA['time_expire'])).')';
                        $package_string .= '</span>';
                        
						$package_string .= '</div><div class="setting_holder"><button class="medium yellow" type="button" onClick="window.location.href=\''.$drupal_url.'izvedi-nakup/'.$rowA['package_id'].'/podatki/\';">'.$lang['srv_narocila_extend'].'</button>';
                    }
                    else{
                        $package_string = $package_name;
                        $package_string .= ' ('.$lang['srv_access_package_valid'].' '.date("d.m.Y", strtotime($rowA['time_expire'])).')';

						$package_string .= '</div><div class="setting_holder"><button class="medium yellow" type="button" onClick="window.location.href=\''.$drupal_url.'izvedi-nakup/'.$rowA['package_id'].'/podatki/\';">'.$lang['srv_narocila_extend'].'</button>';
                    }
                }
                else{
                    $package_string = $package_name;
                    $package_string .= ' ('.$lang['srv_access_package_free'].')';
                    
                    $package_string .= '</div><div class="setting_holder"><button class="medium yellow" type="button" onClick="window.location.href=\''.$drupal_url.''.$lang['srv_narocila_buyurl'].'\';">'.$lang['srv_narocila_buy'].'</button>';
                }
            }
			echo '<div class="setting_holder">'.$package_string.'</div>';
        }
		
		echo '</div>';

		echo '</div>';
		
        // AAI nima moznosti spreminjanja imena, priimka, emaila, gesla...
        if(isAAI()){
            echo '<span class="italic">'.$lang['srv_profil_aai_warning'].'</span>';
        }
        else{

			echo '<p class="bottom16">'.$lang['srv_profil_settings_text'].'</p>';

			echo '<div class="setting_horizontal_wrapper">';
			
			echo '<div class="setting_holder">';
			echo '<label for="ime">'.$lang['name'].':</label>';
			echo '<input class="large" id="ime" name="ime" placeholder="Ime" value="'.$row['name'].'" type="text">';
			echo '</div>';
			
			echo '<div class="setting_holder">';
			echo '<label for="priimek">'.$lang['surname'].':</label>';
			echo '<input class="large" id="priimek" name="priimek" placeholder="Priimek" value="'.$row['surname'].'" type="text">';
			echo '</div>';

			echo '</div>';

			$alternativni_emaili = User::getInstance()->allEmails('brez primarnega');

			echo '<div class="setting_holder">';
			echo '<label>'.$lang['email'].':</label>';
			echo '<div class="icon_holder">';
			echo '<input class="large disabled" value="'.$row['email'].'" type="text" name="email_main">';
			if(!empty($alternativni_emaili))
				echo '<input name="active_email" value="master" id="active-master" type="radio" '.(User::getInstance()->primaryEmail() == $row['email'] ? 'checked="checked"' : '').'><label for="active-master">'.$lang['login_email_subscription'].'</label>';
			echo '</div>';
			echo '</div>';

			//Alternativni maili
            if(!empty($alternativni_emaili)){
                foreach($alternativni_emaili as $email) {
                    echo '<div class="setting_holder">';
                    echo '<label>'.$lang['altemail'].':</label>';
                    echo '<div class="icon_holder">';
                    echo '<input class="large disabled" value="'.$email->email.'" name="email_alter'.($email->id).'" type="text">';
                    echo '<div class="blue pointer altmail" onclick="izbrisiAlternativniEmail(\''.$email->id.'\', \''.$email->email.'\')"><span class="faicon trash empty"></span></div>';
                    echo '<input value="'.($email->id).'" id="altemail'.($email->id).'" name="active_email" type="radio" '.($email->active == 1 ? 'checked="checked"' : '').'> <label for="altemail'.($email->id).'">'.$lang['login_email_subscription'].'</label>';
                    echo '</div>';
                    echo '</div>';						
                }
            }

			echo '<div class="setting_holder">';
			echo '<div class="blue pointer" id="klik-dodaj-email"><span class="faicon add"></span>'.$lang['srv_profil_settings_addemail'].'</div>';
			echo '</div>';

            echo '<div class="setting_holder dodaj-alternativni-email" style="display: none;">';
			echo '<div class="setting_item">';
			echo '<label for="alternativni-email">'.$lang['login_alternative_emails'].':</label>';
					echo '<div class="vnos button_holder inline bottom0">';
                    echo '<input class="large" id="alternativni-email" value="" type="text">';
                    echo '<button type="button" class="medium blue" onclick="dodajAlternativniEmail()">'.$lang['srv_inv_btn_add_recipients_add'].'</button>';
					echo '</div>';
			echo '</div>';
            
			echo '<div class="setting_item">';
			echo '<div id="alternativno-obvestilo" class="italic"></div>';
			echo '</div>';
            
			echo '</div>';

            // Ce je vklopljen modul gorenje, preverimo ce ima se default geslo in izpisemo opozorilo
            if (Common::checkModule('gorenje')){
                if(SurveyGorenje::checkGorenjePassword())
                    echo '<p class="red bold">'.$lang['gorenje_password_warning'].'</p>';
            }

            // Geslo

			echo '<div class="setting_horizontal_wrapper">';
			
			echo '<div class="setting_holder">';
			echo '<label for="p1">'.$lang['password'].':</label>';
            echo '<input class="large" name="geslo" placeholder="'.$lang['password'].'" id="p1" value="PRIMERZELODOLGEGAGESLA" onclick="document.getElementById(\'p1\').value=\'\';" type="password">';
			echo '</div>';
			
			echo '<div class="setting_holder">';
			echo '<label for="p2">'.$lang['password_confirm'].':</label>';
            echo '<input class="large" name="geslo2" placeholder="'.$lang['password'].'" id="p2" value="PRIMERZELODOLGEGAGESLA" onclick="document.getElementById(\'p2\').value=\'\';" type="password">';
			echo '</div>';

			echo '</div>';
                    
            // Prejemanje obvestil
            
			echo '<div class="setting_holder">';
			echo '<span class="setting_title">'.$lang['srv_gdpr_user_options'].' '.Help::display('srv_gdpr_user_options').'</span>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="gdpr_agree" id="gdpr-agree-yes" value="1" '.($row['gdpr_agree'] == 1 ? 'checked="checked"' : null).'/>';
            echo '<label for="gdpr-agree-yes">'.$lang['yes'].'</label>';
			echo '</div>';
			echo '<div class="setting_item">';
			echo '<input type="radio" name="gdpr_agree" id="gdpr-agree-no" value="0" '.($row['gdpr_agree'] == 0 ? 'checked="checked"' : null).'/>';
            echo '<label for="gdpr-agree-no">'.$lang['no1'].'</label>';
			echo '</div>';
			
			echo '</div>';

            // Google 2 FA
            $user_option = User::option($global_user_id, 'google-2fa-secret');
            $user_option_validate = User::option($global_user_id, 'google-2fa-validation');
			echo '<div class="setting_holder">';
			echo '<input type="checkbox" name="google-2fa" id="google-2fa" value="1" '.(! empty($user_option) ? 'checked="checked"' : '').' onclick="prikaziGoogle2faKodo()"/>';
			echo '<label for="google-2fa">'.$lang['google_2fa'].' '.Help::display('srv_google_2fa_options').'</label>';
			echo '</div>';

            if (empty($user_option)) {
                $google2fa = new \Sonata\GoogleAuthenticator\GoogleAuthenticator();
                $googleSecret = $google2fa->generateSecret();

                $googleLink = \Sonata\GoogleAuthenticator\GoogleQrUrl::generate($row['email'], $googleSecret, $site_domain);

                //Prikaži QR kodo
                echo '<div class="settings-2fa-code" id="2fa-display" style="display: none;">';
                echo '<div>'.$lang['google_2fa_admin_enabled'].'<b>'.$googleSecret.'</b></div>';
                echo '<div style="padding-top: 10px;">'.$lang['google_2fa_admin_enabled_2'].'</div>';
                echo '<input type="hidden" name="google-2fa-secret" value="'.$googleSecret.'">';
                echo '<div><img class="twoFAQRcode" src="'.$googleLink.'"/></div>';
                echo '<div>'.$lang['google_2fa_admin_save_note'].'</div>';
                echo '</div>';
            } 
			elseif (! empty($user_option) && $user_option_validate == 'NOT') {
                echo '<div class="google-2fa-validate setting_holder">';

                echo '<div>'.$lang['google_2fa_admin_validate'].'</div>';
                echo '<div><input type="text" class="medium bottom8" name="google-2fa-validate"></div>';

				echo '<div class="bottom8">';
                echo '	<button type="button" class="small white-blue right8" onclick="ponastaviGoogle2fa()">'.$lang['google_2fa_admin_test_code_reset'].'</a></button>';
                echo '	<button type="button" class="small blue" onclick="aktivirajGoogle2fa()">'.$lang['google_2fa_admin_test_code'].'</a></button>';
				echo '</div>';

                echo '<div id="google-2fa-bvestilo" style="font-style: italic; display: none;">'.$lang['google_2fa_user_error_code'].'</div>';

                echo '</div>';
            } 
			else {
                echo '<div class="google-2fa-deactivate setting_holder" id="2fa-display" style="display: none;">';
                
				echo '<div>'.$lang['google_2fa_admin_deactivate'].'</div>';
                echo '<div><input type="text" class="medium bottom8" name="google-2fa-deactivate"></div>';

				echo '<div class="bottom8">';
                echo '	<button type="button" class="small blue" onclick="deaktivirajGoogle2fa()">'.$lang['google_2fa_admin_deactivat_code'].'</a></button>';
                echo '</div>';

				echo '<div id="google-2fa-bvestilo" style="font-style: italic; display: none;">'.$lang['google_2fa_user_error_code'].'</div>';
                
				echo '</div>';
            }

			echo '<div class="button_holder below">';
			echo '	<button type="button" class="medium red" onClick="izbrisi1kaRacun();">'.$lang['delete_account'].'</button>';				
			echo '</div>';
        }
		
        echo '</fieldset>';

		// Save gumb - ce ni AAI
        if(!isAAI()){

			echo '<div class="button_holder">';
			echo '	<button type="button" class="medium white-blue" onClick="window.location='.$preklici_url.';return false;">'.$lang['edit1338'].'</button>';				
			echo '	<button class="medium blue" onclick="save1kaRacunSettings();">'.$lang['edit1337'].'</button>';
			echo '</div>';
            
            // div za prikaz uspešnosti shranjevanja
            if (isset($_GET['s']) && $_GET['s'] == '1') {
                echo '<div id="success_save"></div>';
                echo '<script type="text/javascript">$(document).ready(function() {actionNotePopup(\'success_save\', \'success\');});</script>';
            }
        }

		echo '</form>';
                
		echo '</div>';
	}

	function globalUserChangeArchive () {
		global $lang, $global_user_id, $admin_type, $site_domain, $site_url;
		
		// podatki prijavljenega uporabnika
		$sql = sisplet_query("SELECT id, name, surname, email, type, gdpr_agree, last_login FROM users WHERE id = '$global_user_id'");
		$row = mysqli_fetch_array($sql);
		
		echo '<div id="anketa_edit">';
		
		echo '	<form name="settingsanketa" id="form_profile_user_settings" action="ajax.php?a=editanketasettings&m=global_user_ChangeArchive" method="post" autocomplete="off">' . "\n\r";
		echo '		<input type="hidden" name="location" value="' . $_GET['a'] . '" />' . "\n\r";
		echo '		<input type="hidden" name="submited" value="1" />' . "\n\r";
		

		// Tabela aktivnosti (prijav)
		echo '<fieldset><legend>'.$lang['srv_login_tracking_title'].'</legend>';
		
		echo '<div class="table-horizontal-scroll-wrapper1">';
        echo '<div class="table-horizontal-scroll-wrapper2">';
		
		echo '<table class="login_tracking">';
		echo '<tr><th>IP</th><th>'.$lang['srv_login_tracking_date'].'</th></tr>';
		
		$result = sisplet_query ("SELECT IP, kdaj FROM user_login_tracker WHERE uid='".$global_user_id."' ORDER BY kdaj desc");
		if (mysqli_num_rows($result) == 0) {
			echo $lang['srv_login_tracking_noData'];
		}
		else {
			$count = 0;
			while ($row = mysqli_fetch_array ($result)) {
					echo '<tr '.($count >= 5 ? ' class="displayNone"' : '').($count == 4 ? ' class="show_as_last_row"' : '').'>';
					echo '<td>'.$row['IP'].'</td>';
					echo '<td>'.date('j.n.Y', strtotime($row['kdaj'])).' '.date('H:i:s', strtotime($row['kdaj'])).'</td>';
					echo '</tr>';
					
					$count++;
			}
		}
		echo '</table>';

		echo '</div>';
        echo '</div>';
		
		//Gumb za izpis vseh prijav v tabeli - update v 2. fazi @TG
		/**if(mysqli_num_rows($result) > 25){
			echo '<span class="login_tracking_more bold" onClick="$(\'table.login_tracking tr\').removeClass(\'displayNone\'); $(\'.login_tracking_more\').addClass(\'displayNone\');">'.$lang['srv_invitation_nonActivated_more'].'</span>';
		}**/
		
		echo '</fieldset>';	
                
        UserTrackingClass::init()->userTrackingDisplay();
		
		echo '</div>';
	}
	
	
	function setGlobalUserSetting() {
		global $lang, $global_user_id;
		
		# polovimo nastavitve uporabnika
		UserSetting::getInstance()->Init($global_user_id);
		if (isset($_REQUEST['name']) && isset($_REQUEST['value'])) {
			
			$name = $_REQUEST['name'];
			$value = $_REQUEST['value'];
			
			UserSetting::getInstance()->setUserSetting($name, $value);
			UserSetting::getInstance()->saveUserSetting();
			
		} else {echo 'napaka';}
	}
	
	function showLockSurvey() {
		global $lang, $global_user_id, $admin_type;
        
        UserSetting::getInstance()->Init($global_user_id);
        
        # ali zaklepamo anketo ob aktivaciji
		$lockSurvey = (int)UserSetting::getInstance()->getUserSetting('lockSurvey');
		
		if ($admin_type == '0' || $admin_type == '1') {
			echo '<div class="setting_holder">';
			echo '  <input type="checkbox" id="change_survey_lock" onclick="changeSurveyLock(this)" '.($lockSurvey == 1 ? ' checked="checekd"' : '').'>';
			echo '  <label for="change_survey_lock">';
			echo        $lang['srv_survey_lock_note'];
			echo '  </label>';
			echo '</div>';
        }
        
		if ((int)$lockSurvey > 0) {
			echo '<p class="top16">'.$lang['srv_unlock_popup'].'</p>';
		}
	}
	
	function ajax_showTestSurveySMTP(){
        ob_start();
        
		global $lang, $global_user_id;
		global $admin_type;

		$error = false;
		$msg = null;
		$email_msg = $lang['srv_mail_test_smtp_test_success'];
		$email_subject = $lang['srv_mail_test_smtp_test'];

		$mailMode = $_REQUEST['SMTPMailMode'];
		
		$MA = new MailAdapter($this->anketa);
		$settings = $MA->getSettingsFromRequest($_REQUEST);
		$SmailMode = $mailMode === 1 ? 'google' : 'smtp';
		$settings_saved = $MA->getSettings($SmailMode);

		if ($mailMode == 1 || $mailMode == 2){
			$email = $settings['SMTPFrom'];
		}
		else if (isset ($_COOKIE['uid'])) {
			$email = base64_decode ($_COOKIE['uid']);
        } 
        else {
			$error = true;
			$msg = $lang['srv_mail_test_smtp_mail_detect_error'];
		}

		$settings['SMTPPassword'] = empty($_REQUEST['SMTPPassword']) ? $settings_saved['SMTPPassword'] : $_REQUEST['SMTPPassword'];

		if (validEmail($email)){
			
			// preverimo password - ne sme bit prazen
			if (($mailMode == 1 || $mailMode == 2) && empty($settings['SMTPPassword'])){ # password 
				$error = true;
				$msg = $lang['srv_mail_test_smtp_password_error'];
			}
        } 
        else {
			$error = true;
			$msg = $lang['srv_mail_test_smtp_mail_detect_error'];	
        }
        
        // preverjanje je ok.. poizkusimo poslat testni email
		if ($error == false){ 
            
            $MA->addRecipients($email);
            
            $result = $MA->sendMailTest($email_msg, $email_subject, $mailMode, $settings);
            
            if ($result == false){
				$error = true;
				$msg = $lang['srv_mail_test_smtp_not_possible'];
			}
		}

		echo '<div id="divSurveySmtp">';
		
		echo '<h2>'.$lang['srv_mail_test_smtp'].'</h2>';
		echo '<div class="popup_close"><a href="#" onClick="popupClose(); return false;">✕</a></div>';

		// če imamo napake jo izpišemo
		if ($error == true){
			echo $lang['srv_mail_test_smtp_error'].': '. $msg;
		}
		else{
			echo $lang['srv_mail_test_smtp_sent'].': '.$email;
		}

		#vsebino shranimo v buffer
		$content = ob_get_clean();

		echo $content;

		echo '<div class="button_holder">';
        echo '<button class="medium blue" onClick="popupClose(); return false;">'.$lang['srv_zapri'].'</button>';
        echo '</div>';
		
		echo '</div>';
	}
}
?>