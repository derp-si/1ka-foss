<?php

ob_start();

session_start();

include_once 'definition.php';
include_once '../../function.php';
include_once '../../vendor/autoload.php';

# error reporting
if (isDebug()){
#	error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT);
	error_reporting(E_ALL ^ E_NOTICE);
	ini_set('display_errors', '1');
}
else{
    error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT);
    ini_set('display_errors', '0');
}

Common::start();

sisplet_query("BEGIN");

global $global_user_id;

$surveySkin = 0;


// Naložimo jezikovno datoteko
$anketa = $_REQUEST['anketa'] ?? null;
$lang_admin = 0;
if ($anketa > 0) {
	$sql = sisplet_query("SELECT lang_admin FROM srv_anketa WHERE id = '$anketa'");
	$row = @mysqli_fetch_array($sql);
	$lang_admin = $row['lang_admin'];
}
if ($lang_admin == 0) {
	$sql = sisplet_query("SELECT lang FROM users WHERE id = '$global_user_id'");
	$row = @mysqli_fetch_array($sql);
	$lang_admin = $row['lang'];
}
if ($lang_admin == 0) {
	$sql = sisplet_query("SELECT value FROM misc WHERE what = 'SurveyLang_admin'");
	$row = @mysqli_fetch_array($sql);
	$lang_admin = $row['value'];
}
if ($lang_admin == 0) $lang_admin = 2; // za vsak slucaj, ce ni v bazi

$file = '../../lang/'.$lang_admin.'.php';
include($file);
$_SESSION['langX'] = $site_url .'lang/'.$lang_admin.'.php';


$t = (isset($_GET['t'])) ? $_GET['t'] : '';
$a = (isset($_GET['a'])) ? $_GET['a'] : '';
$m = (isset($_GET['m'])) ? $_GET['m'] : '';
$d = (isset($_GET['d'])) ? $_GET['d'] : '';


// preverimo dostop
$result = sisplet_query ("SELECT value FROM misc WHERE what='SurveyDostop'");
list ($SurveyDostop) = mysqli_fetch_row ($result);

if ( (( ($admin_type <= $SurveyDostop && $SurveyDostop<3) || ($SurveyDostop==3) ) && ($admin_type>=0)) || $a == 'comment_manage' ) {
	// ok, bo slo naprej...
} 
else {
	header ('location: ' .$site_url .'admin/survey/index.php');
	die();
}


// Tracking za katero skupino funkcionalnosti gre (urejanje ankete, podatki, analize...)
$tracking_status = -1;


/**************** UREJANJE ****************/
if ($t == 'branching') {
    $tracking_status = 0;

	$b = new BranchingAjax($anketa);
	$b->ajax();
} 
elseif ($t == 'quota') {
    $tracking_status = 0;

	$SQ = new SurveyQuotas($anketa);
	$SQ->ajax();  
} 
elseif ($t == 'vprasanje') {
    $tracking_status = 0;

	$v = new Vprasanje();
	$v->ajax();
} 
elseif ($t == 'vprasanjeinline') {
    $tracking_status = 0;

	$v = new VprasanjeInline();
	$v->ajax();
} 
elseif ($t == 'vprasanjeDeleted') {
    $tracking_status = 0;

	$vd = new VprasanjeDeleted($anketa);
	$vd->ajax();
} 
elseif ($t == 'prevajanje') {
    $tracking_status = 0;

	$p = new Prevajanje();
	$p->ajax();	
} 
elseif ($t == 'glasovanje') {
    $tracking_status = 0;

	$gl = new Glasovanje($anketa);
	$gl->ajax(); 	
} 
elseif ($t == 'missingValues') {
    $tracking_status = 0;

	$smv = new SurveyMissingValues($anketa);
	$smv->ajax();
} 
elseif ($t == 'quiz') {
    $tracking_status = 0;

	$sq = new SurveyQuiz($anketa);
	$sq -> ajax();
} 
elseif ($t == 'theme') {
    $tracking_status = 0;

	$diplayIframe = ($_POST['a'] == 'previewThemeIframe' ? false : true);		
	$st = new SurveyTheme($anketa,$diplayIframe); 
	$st->Ajax();
} 
elseif ($t == 'heatmapRadij') {
    $tracking_status = 0;

	$hmr = new SurveyHeatMapRadij();
	$hmr->ajax() ;
} 
elseif ($t == 'getheatmapradij') {
    $tracking_status = 0;

	$hmgr = new SurveyGetHeatMapRadij();
	$hmgr->ajax() ;
}
elseif ($t == 'getheatmapexporticons') {
    $tracking_status = 0;
    
	$hmei = new SurveyHeatMapExportIcons();
	$hmei->ajax() ;
}
elseif ($t == 'saveHeatmapImage') {
    
        $tracking_status = 0;
	$hmis = new SurveyHeatMapImageSave();
	$hmis->ajax() ;	

} 
elseif ($t == 'themeEditor') {
    $tracking_status = 0;

	$te = new SurveyThemeEditor($anketa, true);
	$te->ajax();
	
} 
elseif ($t == 'changeSurveyLock') {
    $tracking_status = 0;

	if (isset($_POST['name']) && $_POST['name'] == 'lockSurvey') {
		$locked = (int)$_POST['value'];
		$lockSurvey = sisplet_query("UPDATE srv_anketa SET locked='$locked' WHERE id = '$anketa'");
		UserSetting :: getInstance()->Init($global_user_id);
		UserSetting::getInstance()->setUserSetting('lockSurvey', $locked);
		UserSetting::getInstance()->saveUserSetting();
    }
    
	$sas = new SurveyAdminSettings();
	$sas->showLockSurvey();
} 
elseif ($t == 'SurveyConnect') {
    $tracking_status = 0;

	$sc = new SurveyConnect();
	$sc->ajax();
} 
elseif ($t == 'surveyCondition') {
    $tracking_status = 0;

	$scp = new SurveyCondition($anketa);
	$scp->ajax();
} 
elseif($t == 'checboxChangeTheme'){
    $tracking_status = 0;

    $checkbox = new SurveyTheme($anketa);
    $checkbox->Ajax();
} 
elseif($t == 'gdpr'){
    $tracking_status = 0;

	$gdpr = new GDPR();
	$gdpr->ajax();
} 
elseif ($t == 'skupine') {
    $tracking_status = 0;

	$ss = new SurveySkupine($anketa);
	$ss->ajax();
} 
/**************** UREJANJE - END ****************/


/**************** ANALIZE ****************/	
elseif ($t == A_ANALYSIS) {
    $tracking_status = 2;

	$a = new SurveyAnalysis();
	$a->Init($anketa);
	$a->ajax();    
} 
elseif ($t == 'crosstab') {
    $tracking_status = 2;

	$sc = new SurveyCrosstabs();
	$sc -> Init($anketa);
	$sc -> ajax();
} 
elseif ($t == 'multicrosstabs') {
    $tracking_status = 2;

    $smc = new SurveyMultiCrosstabs($anketa);
	$smc -> ajax();
} 
elseif ($t == 'means') {
    $tracking_status = 2;

	$sm = new SurveyMeans($anketa);
	$sm -> ajax();
}
elseif ($t == 'ttest') {
    $tracking_status = 2;

	$stt = new SurveyTTest($anketa);
	$stt -> ajax();
} 
elseif ($t == 'table_chart') {
    $tracking_status = 2;

	$stc = new SurveyTableChart($anketa);
	$stc -> ajax();
} 
elseif ($t == 'charts') {
    $tracking_status = 2;

	$sc = new SurveyChart();
	$sc -> Init($anketa);
	$sc -> ajax();
} 
elseif ($t == 'zoom') {
    $tracking_status = 2;

	$sz = new SurveyZoom($anketa);
	$sz -> ajax();  
}
elseif ($t == 'break') {
    $tracking_status = 2;

	$sb = new SurveyBreak($anketa);
	$sb->ajax() ;
} 
elseif($t == 'analysisGorenje'){
    $tracking_status = 2;

	$SAG = new SurveyAnalysisGorenje($anketa);
	$SAG->ajax();
} 
elseif ($t == 'ParaAnalysis') {
    $tracking_status = 2;

	$spa = new SurveyParaAnalysis($anketa);
	$spa->ajax();
}
elseif ($t == 'custom_report') {
    $tracking_status = 2;
    
	$SCR = new SurveyCustomReport($anketa);
	$SCR -> ajax();
}	
/**************** ANALIZE - END ****************/


/**************** STATUS ****************/
elseif ($t == 'dashboard') {
    $tracking_status = 3;

	$ss = new SurveyStatistic();
	$ss -> Init($anketa);
	$ss -> ajax();
} 
/**************** STATUS - END ****************/


/**************** PODATKI ****************/
elseif ($t == 'postprocess') {
    $tracking_status = 4;

	$spp = new SurveyPostProcess($anketa);
	$spp->ajax();
} 
elseif ($t == 'advanced_paradata') {
    $tracking_status = 4;

	$sq = new SurveyAdvancedParadata($anketa);
	$sq -> ajax();
} 
elseif ($t == 'recode') {
    $tracking_status = 4;

	$SR = new SurveyRecoding($anketa);
	$SR -> Ajax();
} 
elseif ($t == 'displayData') {
    $tracking_status = 4;

	$dd = new SurveyDataDisplay($anketa);
	$dd->ajax() ;
} 
elseif ($t == 'dataFile') {
    $tracking_status = 4;
    
    $SDF = SurveyDataFile::get_instance();
    $SDF->init($anketa);
    $SDF->ajax();
} 
elseif ($t == 'mapData') {
    $tracking_status = 4;

	$md = new SurveyMapData();
	$md->mapData() ; 
} 
elseif ($t == 'mapDataAll') {
    $tracking_status = 4;

    $md = new SurveyMapData();
    $md->mapDataAll() ;
} 
elseif ($t == 'heatmapData') {
    $tracking_status = 4;

	$hmd = new SurveyHeatMap();
	$hmd->ajax() ;
} 
elseif ($t == 'heatmapBackgroundData') {
    $tracking_status = 4;

	$hmbd = new SurveyHeatMapBackground();
	$hmbd->ajax() ;
} 
elseif ($t == 'setDataView') {
    $tracking_status = 4;

	if ($_POST['what'] != null && trim($_POST['what']) != '' && is_string($_POST['what'])){
		
		# v nastavitev data_view_settings dodamo podnastavitev
		SurveySession::sessionStart($anketa);
		SurveySession::append('data_view_settings',$_POST['what'],$_POST['value']);
		#bolše bi bilo z user sessionom
		
		// Ce nastavljamo stevilo vprasanj ali stevilo spremenljivk resetiramo tudi stran na 1
		if($_POST['what'] == 'spr_limit')
			SurveySession::append('data_view_settings','spr_page','1');
		elseif($_POST['what'] == 'rec_on_page')	
			SurveySession::append('data_view_settings','cur_rec_page','1');
	}
} 
elseif ($t == 'surveyUsableResp') {
    $tracking_status = 4;

	$sur = new SurveyUsableResp($anketa);
	$sur->ajax();
} 
elseif($t == 'aaporCalculation'){
    $tracking_status = 4;

	$ss = new SurveyStatistic();
    $ss -> Init($anketa);

    if($m == ''){
        $ss->DisplayAaporFullCalculation();
    }
    else if($m == 'priblizek'){
        $ss->DisplayAaporPriblizek();
    }
}
elseif ($t == 'appendMerge') {
    $tracking_status = 4;

	$am = new SurveyAppendMerge($anketa);
	$am->ajax();
} 
/**************** PODATKKI - END ****************/


/**************** OBJAVA ****************/
elseif ($t == 'telephone') {
    $tracking_status = 5;

    $tp = new SurveyTelephone($anketa);
    $tp->ajax();
} 
elseif ($t == 'invitations') {
    $tracking_status = 5;

	$SIN = new SurveyInvitationsNew($anketa);
	$SIN -> ajax();
} 
elseif ($t == 'notifications') {
    $tracking_status = 5;

	$NO = new Notifications();
	$NO->ajax();			
} 
elseif ($t == 'simpleMailInvitation') {
    $tracking_status = 5;

	$SSMI = new SurveySimpleMailInvitation($anketa);
	$SSMI -> ajax();
} 
elseif ($t == 'surveyBaseSetting') {
    $tracking_status = 5;

	$SBS = new SurveyBaseSetting($anketa);
	$SBS -> ajax();
}
elseif ($t == 'getSiteUrl') {
    $tracking_status = 5;

	$hmii = new GetSiteUrl();
	$hmii->ajax() ;	
} 
elseif($t == 'WPN'){
    if($a == 'wpn_send_notification'){
        $tracking_status = 5;

        $WPN = new WPN($anketa);
        $WPN -> sendWebPushNotificationsToAll();
    }
} 
elseif ($t == 'SurveyUrlLinks') {
    $tracking_status = 5;

	$sul = new SurveyUrlLinks($anketa);
	$sul->ajax();
}	
/**************** OBJAVA - END ****************/

        
/**************** HIERARHIJA ****************/  
elseif ($t == 'hierarhy-means') {
    $tracking_status = 6;

	$sm = new HierarhijaAnalysis($anketa);
	$sm -> ajax();
        
} 
elseif($t == 'hierarhija-ajax'){
    $tracking_status = 6;

	$hierarhija = new \Hierarhija\HierarhijaAjax($anketa);
	$hierarhija->ajax();
} 
elseif ($t == 'sa-uporabniki') {
    $tracking_status = 6;

	if(!class_exists('Hierarhija\Ajax\AjaxHierarhijaDostopUporabnikovClass'))
		return redirect('/admin/survey/');

	$hierarhija_dostop = new Hierarhija\Ajax\AjaxHierarhijaDostopUporabnikovClass();

	if($a == 'add') {
		$hierarhija_dostop->save();
    }
    elseif($a == 'check'){
        $hierarhija_dostop->checkUserEmail();
    }
    elseif($a == 'delete'){
		$hierarhija_dostop->delete();
    }
    elseif($a == 'edit') {
		$user_id = (!empty($_POST['id']) ? $_POST['id'] : null);
		$hierarhija_dostop->popupNew($user_id);
    }
    elseif($a == 'update') {
		$hierarhija_dostop->update();
    }
    elseif($a == 'show') {
		$hierarhija_dostop->show();
    }
    else {
		$hierarhija_dostop->popupNew();
	}
}
/**************** HIERARHIJA - END ****************/


/**************** UPORABNIK ****************/
elseif ($t == 'surveyList') {
	$SL = new SurveyList();
	$SL->Ajax();    
} 
elseif ($t == 'surveyListFolders') {
	$SL = new SurveyListFolders();
	$SL->ajax();    
} 
elseif ($t == 'library') {
    $l = new Library();
    $l->ajax();
} 
elseif ($t == 'libraryBranching') {
    $lb = new LibraryBranching($anketa);
    $lb->ajax();
} 
elseif ($t == 'help') {
    $h = new Help();
    $h->ajax();
} 
elseif ($t == 'globalUserSettings') {
	$sas = new SurveyAdminSettings();
	$sas->setGlobalUserSetting();
} 
elseif($t == 'userAccess'){
    $ua = UserAccess::getInstance($global_user_id);
	$ua->ajax();
} 
elseif($t == 'userNarocila'){
    $UN = new UserNarocila();
    $UN->ajax();
} 
elseif($t == 'userPlacila'){
    $UP = new UserPlacila();
    $UP->ajax();
} 
elseif($t == 'domainChange'){
    $usr_id = (isset($_POST['usr_id']) ? $_POST['usr_id'] : $global_user_id);

    $dc = DomainChange::getInstance($usr_id);
	$dc->ajax();
} 
elseif ($t == 'newSurvey') {
	$ns = new NewSurvey();
	$ns->ajax();
} 
elseif ($a == 'user_tracking') {

	if($d == 'download'){
        return UserTrackingClass::init()->csvExport();
    }
} 
elseif ($t == 'archiveSurveys') {
	$as = new ArchiveSurveys();
	$as->ajax();
} 
/**************** UPORABNIK - END ****************/


/**************** UNKNOWN, NAPREDNI MODULI ****************/
elseif ($t == 'profileManager') {
    $tracking_status = -1;

    $spm = new SurveyProfileManager();
    $spm->ajax();
} 
elseif ($t == 'SurveyReminderTracking') {
    $tracking_status = -1;

	$sur = new SurveyReminderTracking($anketa);
	$sur->ajax();
} 
elseif ($t == 'inspect') {
    $tracking_status = -1;

	$SI = new SurveyInspect($anketa);
	$SI -> ajax();
} 
elseif ($t == 'dostop') {
    $tracking_status = -1;

	$d = new Dostop();
	$d->ajax();
} 
elseif ($t == 'missingProfiles') {
    $tracking_status = -1;

	$smp = new SurveyMissingProfiles();
	$smp->Init($anketa);
	$smp->ajax();
} 
elseif ($t == 'statusProfile') {
    $tracking_status = -1;

	$ssp = new SurveyStatusProfiles();
	$ssp -> Init($anketa);
	$ssp->ajax();
} 
elseif ($t == 'timeProfile') {
    global $global_user_id;
    
    $tracking_status = -1;

	$tp = new SurveyTimeProfiles();
	$tp -> Init($anketa,$global_user_id);
	$tp -> ajax();
} 
elseif ($t == 'dataSettingProfile') {
    global $global_user_id;
    
    $tracking_status = -1;

	$dsp = new SurveyDataSettingProfiles();
	$dsp -> Init($anketa,$global_user_id);
	$dsp -> ajax();
} 
elseif ($t == 'variableProfile') {
    $tracking_status = -1;

	$svp = new SurveyVariablesProfiles();
	$svp -> Init($anketa,$global_user_id);
	$svp->ajax();
} 
elseif ($t == 'export') {
    $tracking_status = -1;

	$se = new SurveyExport();
	$se -> Init($anketa);
	$se -> ajax();
} 
elseif ($t == 'conditionProfile') {
    $tracking_status = -1;

	$scp = new SurveyConditionProfiles();
	$scp -> Init($anketa,$global_user_id);
	$scp -> ajax();
} 
elseif ($t == 'zankaProfile') {
    $tracking_status = -1;

	$szp = new SurveyZankaProfiles();
	$szp -> Init($anketa,$global_user_id);
	$szp -> ajax();
} 
elseif ($t == 'slideshow') {
    $tracking_status = -1;

	$ss = new SurveySlideshow($anketa);
	$ss -> ajax();
} 
elseif ($t == 'chat') {
    $tracking_status = -1;

	$sc = new SurveyChat($anketa);
	$sc -> ajax();
} 
elseif ($t == 'panel') {
    $tracking_status = -1;

	$sp = new SurveyPanel($anketa);
	$sp -> ajax();
}
elseif ($t == 'email_access') {
    $tracking_status = -1;

	$sea = new SurveyEmailAccess($anketa);
	$sea -> ajax();
}
elseif ($t == 'showTestSurveySMTP') {
    $tracking_status = -1;

	$sas = new SurveyAdminSettings();
	$sas->ajax_showTestSurveySMTP();
} 
elseif ($t == 'evalvacija') {
    $tracking_status = -1;

	// UL EVALVACIJA
	$eval = new Evalvacija($anketa);
	$eval->ajax();
} 
elseif ($t == 'evoliTM') {
    $tracking_status = -1;

	// Evoli TeamMeter
	$evoliTM = new SurveyTeamMeter($anketa);
	$evoliTM->ajax();
} 
elseif ($t == 'kolektor') {
    $tracking_status = -1;

	// Modul za kolektor
	$kolektor = new Kolektor();
	$kolektor->ajax();
} 
/**************** UNKNOWN, NAPREDNI MODULI - END ****************/


/**************** MAZA ****************/
elseif ($t == 'MAZA') {

    if(isset($a)){

        $anketa = $anketa;

        if($a == 'maza_send_notification'){
            $tracking_status = 5;
            $maza = new MAZA($anketa);
            $maza->ajax_sendNotification();
        }

        if($a == 'maza_send_notification_pwa'){
            $tracking_status = 5;
            $WPN = new WPN($anketa);
            $WPN -> sendWebPushNotificationsToAll();
        }
        else if($a == 'maza_on_off'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->ajax_maza_on_off();
        }
        else if($a == 'maza_cancel_alarm'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->ajax_maza_cancel_alarm();
        }
        else if($a == 'maza_generate_users'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->ajax_maza_generate_users();
        }
        else if($a == 'maza_survey_description'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->ajax_maza_survey_description();
        }
        else if($a == 'changeRepeatBy'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->ajax_changeRepeatBy();
        }
        else if($a == 'changeTimeInDay'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->ajax_changeTimeInDay();
        }
        else if($a == 'changeDayInWeek'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->ajax_changeDayInWeek();
        }
        else if($a == 'changeEveryWhichDay'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->ajax_changeEveryWhichDay();
        }
        else if($a == 'maza_save_repeater'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->ajax_saveRepeater();
        }
        else if($a == 'cancelRepeater'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->maza_cancel_repeater();
        }
        else if($a == 'insert_geofence'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->ajax_insert_geofence();
        }
        else if($a == 'update_geofence'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->ajax_update_geofence();
        }
        else if($a == 'update_geofence_name'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->ajax_update_geofence_name();
        }
        else if($a == 'delete_geofence'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->ajax_delete_geofence();
        }
        else if($a == 'get_all_geofences'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->ajax_get_all_geofences();
        }
        else if($a == 'maza_cancel_geofencing'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->maza_cancel_geofencing();
        }
        else if($a == 'maza_run_geofences'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->maza_run_geofences();
        }
        else if($a == 'maza_cancel_entry'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->maza_cancel_entry();
        }
        else if($a == 'maza_run_entry'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->maza_run_entry();
        }
        else if($a == 'maza_run_activity'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->maza_run_activity();
        }
        else if($a == 'maza_cancel_activity'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->maza_cancel_activity();
        }
        else if($a == 'maza_run_tracking'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->maza_run_tracking();
        }
        else if($a == 'maza_cancel_tracking'){
            $tracking_status = 0;
            $maza = new MAZA($anketa);
            $maza->maza_cancel_tracking();
        }
    }
} 
/**************** MAZA -END ****************/


/**************** DEFAULT ****************/
else {
    $tracking_status = -1;
	$s = new SurveyAdminAjax();
	$s->ajax();
}


// Shranimo tracking
if($anketa != null && $anketa > 0){
    TrackingClass::update($anketa, $tracking_status);
}
//nismo vezani na anketo, tracking uporabnika
else{
    TrackingClass::update_user();
}


// izpisemo buffer pred zapisovanjem vSSPVUCT tracking in commitom (da je kao hitrejse)
ob_flush();

Common::stop();

Common::checkStruktura();

// TODO neko globalno preverjanje za errorje. da se ob napaki naredi rollback in ohranimo konsistentnost
if (true)
	sisplet_query("COMMIT");
else
	sisplet_query("ROLLBACK");


?>
