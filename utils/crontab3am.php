<?php
    
    // CRONJOB - klice se vsako noc okoli treh
	ob_start();
	set_time_limit(2400); # 30 minut

	include_once('../admin/survey/definition.php');
	include_once('../function.php');
	include_once('../vendor/autoload.php');

  
	// Nov nacin za izvajanje cronjobov (razred utils/1kaCron/class.CronJobs.php)
    $cj = new CronJobs();
    

    /*** BRISANJE ***/

    // Brisanje pobrisanih anket (starejsih od 3 meseca) (prej utils/cronetab_send_survey_alert.php)
    $cj->executeAction('deleteSurveyFromDB');

    // Brisanje pobrisanih vprasanj
	$cj->executeAction('deleteVprasanjeFromDB');

    // Brisanje pobrisanih podatkov anket (starejsi od 3 mesecev)
    $cj->executeAction('deleteResponseFromDB');

    // Brisanje podatkov anket, ki so boti (pri neaktivnih anketah starejsih od 3 mesecev)
    $cj->executeAction('deleteResponseBotFromDB');

    // Brisanje pingdom podatkov anket
    $cj->executeAction('deleteResponsePingdomFromDB');

    // Generiranje grafov - zanekrat samo čistimo stare grafe (starejše od 30 dni) in ne generiramo ničesar
    $cj->executeAction('cacheCharts');

    // Brisanje emailov pri dostopu do ankete z emailom - SAMO NA INSTALACIJI MJU www.gov-ankete.si
    global $site_domain;
    if($site_domain == 'gov-ankete.si'){
        $cj->executeAction('deleteEmailAccessEmails');
    }

    /*** BRISANJE KONEC ***/



    /*** KESIRANJE ***/

    // Generiranje podatkovnih datotek
	$cj->executeAction('cacheData');
        
    // Za modul MAZA - javljanje aplikacijam, ce je anketa potekla (deaktivirana)
    if(Common::checkModule('maza')){
	    $cj->executeAction('mazaCheckExpiredSurveys');
    }

    /*** KESIRANJE KONEC ***/
    


    /*** OBVESTILA ***/

    // Posiljanje obvestil o poteku anket (prej utils/cronetab_send_survey_alert.php) - TODO
    //$SL->addMessage(SurveyLog::INFO, "CRONJOB 3am - posiljanje obvestil o poteku anket");
	//$cj->executeAction('surveySendAlert');
    
    // Posiljanje obvestila adminom za reset gesla vsakih 6 mesecev
    //$SL->addMessage(SurveyLog::INFO, "CRONJOB 3am - posiljanje obvestila adminom za reset gesla vsakih 6 mesecev");
    //$cj->executeAction('adminResetPassword');
    
    // Posiljanje obvestila metaadminom in avtorju ankete 5 let po aktivaciji, da naj pobrisejo podatke
    //$SL->addMessage(SurveyLog::INFO, "CRONJOB 3am - posiljanje gdpr obvestil po 5 letih za brisanje podatkov");
    //$cj->executeAction('gdprMailExpired');

    /*** OBVESTILA KONEC ***/
       

	
	ob_get_clean();
?>