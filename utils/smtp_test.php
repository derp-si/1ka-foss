<?php

/**
* skripta za testiranje smtp-ja
*/

include('../function.php');
include_once '../vendor/autoload.php';


$mail = new PHPMailer\PHPMailer\PHPMailer();

try {

	$mail->SMTPDebug = 2;

    // Server settings
    $mail->isSMTP();


	// Preveri in dopolni podatke!
    $mail->Host       = ''; // SMTP server
    $mail->SMTPAuth   = false; // Enable SMTP authentication
    $mail->Username   = ''; // SMTP username
    $mail->Password   = ''; // SMTP password
    $mail->SMTPSecure = ''; // tls, ssl...
    $mail->Port       = 25; // TCP port



    // Recipients
    $mail->setFrom('xxx@xxx.si', 'SMTP tester');
    $mail->addAddress('xxx@xxx.si');

    // Content
    $mail->isHTML(true);
    $mail->Subject = 'SMTP test';
    $mail->Body    = 'Testiranje 1ka emailov.';

    // Send the email
    $response = $mail->send();

	if($mail->ErrorInfo != '')
		echo '<br><br>'.$mail->ErrorInfo.'</br>';
} 
catch (Exception $e) {
    echo "Error: {$mail->ErrorInfo}";
}





?>