<?php 
	
	session_start();
	
	//phpinfo();
	error_reporting(0);
	//ini_set('display_errors', 'On');
    

    include_once('classes/DisplayController.php');
        

    // Preverimo ce imamo uvozeno bazo in ce ne ponudbimo uvoz
	$import_db = new ImportDB();
	if($import_db->checkDBEmpty()){
		global $site_url;
		header('Location: '.$site_url.'frontend/install');
    }


    // Preprecimo vkljucevanje v iframe
    header('X-Frame-Options: SAMEORIGIN');
    
	
	// Inicializiramo razred za prikaz
	$dc = new DisplayController();
	
    
    echo '<!doctype html>';
    echo '<html lang="en">';
    

    /********** HEAD **********/
    echo '<head>';
    $dc->displayHead();
    echo '</head>';
    /********** HEAD - END **********/
	
	
    /********** BODY **********/
    if(isset($_GET['a']) && $_GET['a'] != '' && in_array($_GET['a'], array('landing_page', 'login', 'register', 'login_noEmail', 'reset_password', 'gdpr')))
        $body_class = $_GET['a'];
    else
        $body_class = 'landing_page body-2';
    echo '<body class="'.$body_class.'">';
    
    echo '<div id="content" '.(isAAI() ? 'class="aai"' : '').'>';

	// Glava
	echo '<header>';
	$dc->displayHeader();	
	echo '</header>';
    
    
    // Vsebina strani
    echo '<div id="main">';

    echo '<div class="main_content">';
	$dc->displayMain();	
    echo '</div>';
    
	echo '</div>';
	
	
    // Footer
    echo '<footer>';

    echo '<div class="footer_content">';
    $dc->displayFooter();	
    echo '</div>';

    echo '</footer>';


    // Cookie notice  
    $dc->displayCookieNotice();	
    
    
    echo '</div>';

    echo '</body>';
    /********** BODY - END **********/
    
    
	echo '</html>';
?>