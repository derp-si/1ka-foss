<?php

/**
 *
 *  Class ki skrbi za prenos domene www.1ka.si na oneclicksurvey.com oz. enklikanketa.si
 *
 */

class DomainChange{


    private static $instance = false;
    
    private $usr_id;
    private $user_switch_status;    // Status preklopa uporabnika (0 - ni preklopil, 1 - je zavrnil preklop in ostal na 1ka.si, 2 - je preklopil na oneclick)

    public static $domain_original = 'www.1ka.si';
    //public static $domain_original = 'localhost';
    public static $domain_new = 'www.oneclicksurvey.com';
    //public static $domain_new = 'localhost';

	
    public static function getInstance($usr_id){
        
        if (!self::$instance)
			self::$instance = new DomainChange($usr_id);
			
		return self::$instance;
	}

    private function __construct($usr_id){

        // Ce nimamo usr_id-ja ga poskusimo pridobiti iz id-ja ankete
        if(!isset($usr_id) || $usr_id < 1 || $usr_id == ''){
            echo 'Napaka! Manjka ID uporabnika.';
            die();
        }

        $this->usr_id = $usr_id;

        // Dobimo status preklopa za userja
        $sqlUser = sisplet_query("SELECT switch_status FROM 1ka_domain_change_user WHERE usr_id='".$this->usr_id."'");

        // Uporabnik se ni naredil nic
        if(mysqli_num_rows($sqlUser) == 0)
            $this->user_switch_status = 0;

        $rowUser = mysqli_fetch_array($sqlUser);
        $this->user_switch_status = $rowUser['switch_status'];
    }


    // Preverimo ce izvajamo na aplikaciji preklop domene
    public static function check1kaDomainChange(){
       global $site_domain;

        // ZAENKRAT JE CEL MODUL UGASNJEN
        return false;
        
        // Ce smo na www.1ka.si, oneclicksurvey.com ali enklikanketa.si
        //if(in_array($site_domain, array('www.1ka.si', 'www.oneclicksurvey.com', 'www.enklikanketa.si'))){
        //if(in_array($site_domain, array(self::$domain_original, self::$domain_new)))
        if($site_domain == self::$domain_original)
            return true;
        else
            return false;
    }


    // Ali prikazujemo kreiranje, kopiranje ankete
    public function domainChangeLimits(){
        global $site_domain;

        // Skrivamo samo na originalni domeni (www.1ka.si) in se ni naredil preklopa na oneclicksurvey.com
        if($site_domain == self::$domain_original && $this->user_switch_status != '2'){
            
            // Preverimo aktiven paket - ce nima placanega paketa, mu vse skrijemo
            $userAccess = UserAccess::getInstance($this->usr_id);
            $user_package = $userAccess->getPackage();
            if ($user_package != '2' && $user_package != "3"){
                return true;
            }        
        }

        return false;
    }

    // Ali prikazujemo kreiranje, kopiranje ankete
    public function visibleSwitchPopup(){
        global $site_domain;

        if($site_domain != self::$domain_original)
            return false;
        
        if($this->user_switch_status != 0)
            return false;

        return true;
    }


    // Izpisemo popup obvestilo, da je funkcionalnost onemogocena in naj kupi paket
    private function displaySwitchPopup(){
        global $lang;
        global $site_url;

        echo '<h2>'.$lang['domain_switch_popup_title'].'</h2>';

        echo '<div class="popup_close"><a href="#" onclick="popupUserAccess_close();">✕</a></div>';

        echo '<p>'.$lang['domain_switch_popup_text'].'</p>';

        echo '<div class="button_holder">';
        echo '  <button class="medium white-blue" href="#" onClick="popupSwitch_save(\'1\');">'.$lang['domain_switch_popup_no'].'</button>';
        echo '  <button class="medium blue" href="#" onClick="popupSwitch_save(\'2\');">'.$lang['domain_switch_popup_yes'].'</button>';
        echo '</div>';
    }


    // Ajax klici
    public function ajax(){
        global $site_url;


        // Prikazemo popup z izbiro preklopa
        if($_GET['a'] == 'displaySwitchPopup') {
            $this->displaySwitchPopup($what);
        }

        // Prikazemo popup z izbiro preklopa
        elseif($_GET['a'] == 'setSwitchStatus') {

            $switch_status = isset($_POST['switch_status']) ? $_POST['switch_status'] : '';

            if($switch_status != ''){

                /*$sqlUser = sisplet_query("INSERT INTO 1ka_domain_change_user 
                                                (usr_id, switch_time, switch_status)
                                            VALUES
                                                ('".$this->usr_id."', NOW(), '".$switch_status."')
                                            ON DUPLICATE KEY UPDATE
                                                switch_status='".$switch_status."'
                                        ");*/

                // Izvede se prenos na oneclicksurvey.com - userja se odjavi na 1ka.si in prijavi na oneclicksurvey.com
                if($switch_status == '2'){
                    $return_data = array(
                        'action' => 'switch_domain',
                        'url'   => $site_url.'/domain-change.php?action=logout'
                    );

                    echo json_encode($return_data);
                }
            }
        }

    }


    // Pri preklopu domene odjavimo userja iz stare domene
    public static function domainChangeLogoutOld($secret_key){
        global $cookie_domain;

        // Dobmo piskotek za post na domeno oneclicksurvey.com (tam uporabnika prijavimo)
        $cookie = array(
            'secret'    => $_COOKIE['secret'],
            'uid'       => $_COOKIE['uid'],
            'unam'      => $_COOKIE['unam']
        );

        if (isset ($_COOKIE['g2fa']) && !empty($_COOKIE['g2fa']))
            $cookie['g2fa'] = $_COOKIE['g2fa'];

        $cookie_string = json_encode($cookie);

        $cipher = "AES-256-CBC";
        $encrypted_cookie_string = openssl_encrypt($cookie_string, $cipher, $secret_key);


        // Pobrisemo piskotek in uporabnika odjavimo
        setcookie('uid', '', time() - 3600, '/', $cookie_domain);
        setcookie('unam', '', time() - 3600, '/', $cookie_domain);
        setcookie('secret', '', time() - 3600, '/', $cookie_domain);
        setcookie('ME', '', time() - 3600, '/', $cookie_domain);
        setcookie('P', '', time() - 3600, '/', $cookie_domain);
        setcookie("AN", '', time() - 3600, '/', $cookie_domain);
        setcookie("AS", '', time() - 3600, '/', $cookie_domain);
        setcookie("AT", '', time() - 3600, '/', $cookie_domain);

        setcookie("DP", $p, time() - 3600 * 24 * 365, "/", $cookie_domain);
        setcookie("DC", $p, time() - 3600 * 24 * 365, "/", $cookie_domain);
        setcookie("DI", $p, time() - 3600 * 24 * 365, "/", $cookie_domain);
        setcookie("SO", $p, time() - 3600 * 24 * 365, "/", $cookie_domain);
        setcookie("SPO", $p, time() - 3600 * 24 * 365, "/", $cookie_domain);
        setcookie("SL", $p, time() - 3600 * 24 * 365, "/", $cookie_domain);

        // pobrisi se naddomeno! (www.1ka.si naj pobrise se 1ka.si)
        if (substr_count($cookie_domain, ".") > 1) {
            $nd = substr($cookie_domain, strpos($cookie_domain, ".") + 1);

            setcookie('uid', '', time() - 3600, '/', $nd);
            setcookie('unam', '', time() - 3600, '/', $nd);
            setcookie('secret', '', time() - 3600, '/', $nd);
            setcookie('ME', '', time() - 3600, '/', $nd);
            setcookie('P', '', time() - 3600, '/', $nd);
            setcookie("AN", '', time() - 3600, '/', $nd);
            setcookie("AS", '', time() - 3600, '/', $nd);
            setcookie("AT", '', time() - 3600, '/', $nd);

            setcookie("DP", $p, time() - 3600 * 24 * 365, "/", $nd);
            setcookie("DC", $p, time() - 3600 * 24 * 365, "/", $nd);
            setcookie("DI", $p, time() - 3600 * 24 * 365, "/", $nd);
            setcookie("SO", $p, time() - 3600 * 24 * 365, "/", $nd);
            setcookie("SPO", $p, time() - 3600 * 24 * 365, "/", $nd);
            setcookie("SL", $p, time() - 3600 * 24 * 365, "/", $nd);
        }
        

        // Preusmerimo na oneclicksurvey s parametri za prijavo
        header('Location: http://'.self::$domain_new.'/domain-change.php?action=login&c='.urlencode($encrypted_cookie_string));
    }

    // Pri preklopu domene prijavimo userja v novo stran
    public static function domainChangeLoginNew($secret_key){
        global $cookie_domain;

        // Check if the request is coming from an allowed domain
        $referer = $_SERVER['HTTP_REFERER'];
        $domain = parse_url($referer, PHP_URL_HOST);

        if ($domain != self::$domain_original) {
            header("HTTP/1.0 403 Forbidden");
            die("Forbidden");
        }


        // Get cookies from parameter
        if(!isset($_GET['c']) || $_GET['c'] == ''){
            die();
        }

        $encrypted_cookie_string = $_GET['c'];

        // Decrypt cookies
        $cipher = "AES-256-CBC";
        $cookie_string = openssl_decrypt($encrypted_cookie_string, $cipher, $secret_key);
        $cookie = json_decode($cookie_string);

        $LifeTime = 43200;

        // Set cookies on new domain
        setcookie("uid", $cookie->uid, time() + $LifeTime, '/', $cookie_domain);
        setcookie("unam", $cookie->unam, time() + $LifeTime, '/', $cookie_domain);
        setcookie("secret", $cookie->secret, time() + $LifeTime, '/', $cookie_domain);

        header('Location: http://'.self::$domain_new);
    }
}