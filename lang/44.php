<?php
// _/ _/ _/  za primerjavo z 1.php uporabi: /utils/checkLanguageKeys.php _/ _/ _/
// Language file

$lang_add = array (
	"useful_translation"	=>		"0",
	"id"			=>		"44",							// ID te jezikovne datoteke (ID.php)
	"lang_short"	=>		"nn",							// si - slovenian, en - english
	"language"		=> 	"New Norwegian",

	// tukaj so zbrani vsi teksti za respondentov vmesnik ankete
	// anketa -> urejanje -> nastavitve -> standardne besede
    "srv_survey_non_active" => "Undersøkinga er avslutta.",
    "srv_survey_non_active_notStarted" => "Undersøkinga er ikkje aktiv. Valet startar:",
    "srv_survey_non_active_expired" => "Undersøkinga er ikkje aktiv. Valet er avslutta:",
    "srv_survey_non_active_voteLimit" => "Undersøkinga har nådd maksimalt antal svar.",
    "srv_previewalert" => "Du viser akkurat no ei førehandsvising av undersøkinga. Svara dine vil ikkje bli lagra!",
    "srv_recognized" => "Svar på undersøkinga som",
    "srv_ranking_avaliable_categories" => "Eksisterande kategoriar",
    "srv_ranking_ranked_categories" => "Rangeringskategoriar",
    "srv_add_field" => "Legg til nytt felt",
    "vote_sex_selection" => "Vel kjønn",
    "vote_sex_man" => "Mann",
    "vote_gender_female" => "Kvinne",
    "srv_remind_sum_hard" => "Du har overskride maksimumssummen!",
    "srv_remind_sum_soft" => "Du har overskride maksimumssummen. Vil du fortsetje?",
    "srv_remind_num_hard" => "Du har overskride den tillatne verdien!",
    "srv_remind_num_soft" => "Du har overskride den tillatne verdien. Vil du fortsetje?",
    "srv_remind_hard" => "Ver venleg og svar på alle naudsynte spørsmål.",
    "srv_remind_soft" => "Du har ikkje svara på alle dei naudsynte spørsmåla. Vil du fortsetje?",
    "srv_confirm" => "Stadfest",
    "srv_lastpage" => "Siste side",
    "srv_nextpage" => "Neste side",
    "srv_nextpage_uvod" => "Neste side",
    "srv_prevpage" => "Førre side",
    "results" => "Resultat",
    "vote_count" => "Antal ",
    "vote_time" => "Røysting er mogleg frå",
    "vote_time_end" => "til",
    "hour_all" => "Alle",
    "female_female_voting" => "Kvinne",
    "srv_intro" => "Bruk litt tid på å gjennomføre denne undersøkinga ved å klikke på \" Neste side \"",
    "srv_basecode" => "Skriv inn passordet ditt",
    "srv_end" => "Du er ferdig med undersøkinga. Takk.",
    "srv_back_edit" => "Tilbake til redigering",
    "srv_nextins" => "Neste oppfølging",
    "srv_insend" => "Avslutt",
    "srv_back_edit" => "Tilbake til redigering",
    "srv_alert_msg" => "fullførte undersøkinga",
    "srv_alert_subject" => "Undersøkinga er fullført",
    "srv_remind_captcha_hard" => "Talet du skreiv inn, er ikkje det same som i biletet!",
    "srv_remind_captcha_soft" => "Talet du skreiv inn, er ikkje det same som i biletet! Vil du fortsetje?",
    "srv_alert_number_exists" => "Åtvaring: talet finst allereie!",
    "srv_alert_number_toobig" => "Åtvaring: Talet er for stort!",
    "srv_forma_send" => "Send",
    "srv_end" => "Slutt",
    "srv_dropdown_select" => "Velje",
);

include(dirname(__FILE__).'/2.php');

// povozimo angleski jezik s prevodi
foreach ($lang_add AS $key => $val) {
	$lang[$key] = $val;
}

?>