<?php
// _/ _/ _/  za primerjavo z 1.php uporabi: /utils/checkLanguageKeys.php _/ _/ _/
// Language file

$lang_add = array (
	"id"			=>		"45",							// ID te jezikovne datoteke (ID.php)
	"lang_short"	=>		"ne",							// si - slovenian, en - english
	"language"		=> 	"Nepali",
    
    
	// Translation texts
    "srv_survey_non_active" => "सर्वेक्षण बन्द",
    "srv_survey_non_active_notStarted" => "सर्वेक्षण सक्रिय छैन। मतदान सुरु:",
    "srv_survey_non_active_expired" => "Survey is not active. Poll has expired:",
    "srv_survey_non_active_voteLimit" => "सर्वेक्षण सक्रिय छैन। मतदानको म्याद सकिएको छ:",
    "srv_previewalert" =>  "तपाईं हाल सर्वेक्षणको पूर्वावलोकन हेर्दै हुनुहुन्छ। तपाईंका जवाफहरू बचत हुने छैनन्!",
    "srv_recognized" => "सर्वेक्षणको रूपमा जवाफ दिनुहोस्",
    "srv_ranking_avaliable_categories" => "अवस्थित कोटीहरू",
    "srv_ranking_ranked_categories" => "श्रेणी वर्गहरू",
    "srv_add_field" =>  "नयाँ फिल्ड थप्नुहोस्",
    "vote_sex_selection" => " लिङ्ग चयन गर्नुहोस्",
    "vote_sex_man" =>  "पुरुष",
    "vote_gender_female" => "महिला",
    "srv_remind_sum_hard" => "तपाईंले अधिकतम योगफल नाघेको छ!",
    "srv_remind_sum_soft" => "तपाईंले अधिकतम योगफल नाघेको छ। के तपाइँ जारी राख्न चाहनुहुन्छ?",
    "srv_remind_num_hard" => "तपाईंले अनुमति दिएको मान नाघ्नुभयो!",
    "srv_remind_num_soft" => "तपाईंले अनुमति दिएको मान नाघ्नुभयो। के तपाइँ जारी राख्न चाहनुहुन्छ?",
    "srv_remind_hard" => "कृपया सबै आवश्यक प्रश्नहरूको जवाफ दिनुहोस्।",
    "srv_remind_soft" => "तपाईंले सबै आवश्यक प्रश्नहरूको जवाफ दिनुभएन। के तपाइँ जारी राख्न चाहनुहुन्छ?",
    "srv_confirm" => "पुष्टि गर्नुहोस्",
    "srv_lastpage" => "अन्तिम पाना",
    "srv_nextpage" => "अर्को पाना",
    "srv_nextpage_uvod" => "अर्को पाना",
    "srv_prevpage" => "अघिल्लो पृष्ठ",
    "results" => "परिणामहरू",
    "vote_count" => "गणना गणना",
    "vote_time" => "बाट भोट गर्न सकिनेछ",
    "vote_time_end" => "को",
    "hour_all" => "सबै",
    "female_female_voting" => "महिला",
    "srv_intro" => "कृपया क्लिक गरेर यो सर्वेक्षण पूरा गर्न एक क्षण लिनुहोस् \"अर्को पाना\"",
    "srv_basecode" => "आफ्नो पासवर्ड प्रविष्ट गर्नुहोस्",
    "srv_end" => "तपाईंले सर्वेक्षण पूरा गर्नुभयो। धन्यवाद।",
    "srv_back_edit" => "सम्पादन मा फर्कनुहोस्",
    "srv_nextins" => "अर्को प्रविष्टि",
    "srv_insend" => "समाप्त गर्नुहोस्",
    "srv_back_edit" => "सम्पादन मा फर्कनुहोस्",
    "srv_alert_msg" => "सर्वेक्षण पूरा गरे",
    "srv_alert_subject" => "सर्वेक्षण सम्पन्न भएको छ",
    "srv_remind_captcha_hard" => "तपाईंले प्रविष्ट गर्नुभएको नम्बर छविमा जस्तै छैन!",
    "srv_remind_captcha_soft" => "तपाईंले प्रविष्ट गर्नुभएको नम्बर छविमा जस्तै छैन! के तपाइँ जारी राख्न चाहनुहुन्छ?",
    "srv_alert_number_exists" => "चेतावनी: नम्बर पहिले नै अवस्थित छ!",
    "srv_alert_number_toobig" => "चेतावनी: संख्या धेरै ठूलो!",
    "srv_forma_send" => "पठाउनुहोस्",
    "srv_end" => "अन्त्य",
    "srv_dropdown_select" => "चयन गर्नुहोस्",
);

include(dirname(__FILE__).'/2.php');

// povozimo angleski jezik s prevodi
foreach ($lang_add AS $key => $val) {
	$lang[$key] = $val;
}

?>